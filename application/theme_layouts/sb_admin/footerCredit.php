<footer class="footer-admin mt-auto footer-light">
  <div class="container-xl px-4">
    <div class="row">
      <div class="col-md-6 small">
        © Darul Arqam Muhammadiyah Garut | <?= $app->app_name ?>
      </div>
      <div class="col-md-6 text-md-end small">
        Version <?= $app->app_version ?>
      </div>
    </div>
  </div>
</footer>