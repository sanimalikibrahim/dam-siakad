<footer class="footer hidden-xs-down">
  <p>© <?php echo $app->app_name ?>. Version <?php echo $app->app_version ?></p>

  <ul class="nav footer__nav">
    <a class="nav-link" href="https://darularqamgarut.sch.id/" target="_blank">Pondok Pesantren Darul Arqam Muhammadiyah Garut</a>
  </ul>
</footer>