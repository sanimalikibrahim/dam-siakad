<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . 'controllers/AppBackend.php');

class Dashboard extends AppBackend
{
	function __construct()
	{
		parent::__construct();
		$this->load->model([
			'NotificationModel',
			'JadwalshalatModel',
			'PsbModel'
		]);
	}

	public function index()
	{
		$role = $this->session->userdata('user')['role'];
		$lastDayOfMonth = cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y'));
		$paramStartDate = $this->input->get('start_date');
		$paramStartDate = (!is_null($paramStartDate)) ? $paramStartDate : date('Y-m') . '-01';
		$paramEndDate = $this->input->get('end_date');
		$paramEndDate = (!is_null($paramEndDate)) ? $paramEndDate : date('Y-m') . '-' . $lastDayOfMonth;
		$paramStatus = $this->input->get('status');

		if (strtolower($role) === 'calon santri') {
			redirect(base_url('profile/calon-santri'));
			exit;
		};

		$pendaftaranTanggal = $this->PsbModel->getCountByYear($paramStartDate, $paramEndDate, $paramStatus);
		$pendaftaranKota = $this->PsbModel->getCountByKota($paramStartDate, $paramEndDate, $paramStatus);
		$pendaftaranGender = $this->PsbModel->getCountByGender($paramStartDate, $paramEndDate, $paramStatus);

		$data = array(
			'app' => $this->app(),
			'main_js' => $this->load_main_js('dashboard', false, array(
				'data_chart_pendaftaran' => json_encode($pendaftaranTanggal),
				'data_chart_pendaftaran_kota' => json_encode($pendaftaranKota),
				'data_chart_pendaftaran_gender' => json_encode($pendaftaranGender),
				'filter_start_date' => $paramStartDate,
				'filter_end_date' => $paramEndDate,
				'filter_status' => $paramStatus,
			)),
			'page_title' => 'ٱلسَّلَامُ عَلَيْكُمْ‎',
			'page_subTitle' => 'Welcome to ' . $this->app()->app_name . ' v' . $this->app()->app_version,
			'data_jadwal_shalat' => $this->JadwalshalatModel->getAll_list(),
			'data_pendaftaran_kota_count' => count($pendaftaranKota),
			'filter_start_date' => $paramStartDate,
			'filter_end_date' => $paramEndDate,
			'filter_status' => $paramStatus,
		);

		$this->template->set('title', $data['app']->app_name, TRUE);
		$this->template->load_view('index', $data, TRUE);
		$this->template->render();
	}
}
