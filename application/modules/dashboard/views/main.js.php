<script type="text/javascript">
  $(document).ready(function() {

    var _filterStartDate = "<?= $filter_start_date ?>";
    var _filterEndDate = "<?= $filter_end_date ?>";
    var _filterStatus = "<?= $filter_status ?>";
    var _dataTotalPendaftaran = <?= $data_chart_pendaftaran ?>;
    var _dataTotalPendaftaranKota = <?= $data_chart_pendaftaran_kota ?>;
    var _dataTotalPendaftaranGender = <?= $data_chart_pendaftaran_gender ?>;

    // Disabled right click on home image
    $(".img-home").each(function() {
      $(this)[0].oncontextmenu = function() {
        return false;
      };
    });

    // Handle ajax start
    $(document).ajaxStart(function() {
      $(document).find(".body-loading").fadeOut("fast", function() {
        $(this).show();
      });
    });

    // Handle ajax stop
    $(document).ajaxStop(function() {
      $(document).find(".body-loading").fadeOut("fast", function() {
        $(this).hide();
        document.body.style.overflow = "auto";
      });
    });

    // Handle filter
    $(document).on("click", ".page-action-filter", function() {
      var baseUrl = "<?= base_url('dashboard/') ?>";
      var startDate = $(".filter-start_date").val();
      var endDate = $(".filter-end_date").val();
      var status = $(".filter-status").val();

      if (startDate != "" && endDate != "" && status != "" && status != null) {
        var params = `?start_date=${startDate}&end_date=${endDate}&status=${status}`;
        var url = baseUrl + params;

        $(document).find(".body-loading").fadeOut("fast", function() {
          $(this).show();
        });

        // Reload page
        window.location.href = url;
      } else {
        notify("Please fill all filter field!", "danger");
      };
    });
    // END ## Handle filter

    // Chart: Total Pendaftaran by Tanggal
    if ($("#chart-pendaftaran")[0]) {
      var _dataTotalPendaftaran_labels = [];
      var _dataTotalPendaftaran_datasets = [];
      var _dataTotalPendaftaran_total = 0;

      if (_dataTotalPendaftaran.length > 0) {
        _dataTotalPendaftaran.map((item, index) => {
          _dataTotalPendaftaran_labels.push(item.tanggal);
          _dataTotalPendaftaran_datasets.push(item.jumlah);
          _dataTotalPendaftaran_total = parseInt(_dataTotalPendaftaran_total) + parseInt(item.jumlah);
        });
      };

      var chartPendaftaran = new Chart(document.getElementById("chart-pendaftaran"), {
        type: "line",
        data: {
          labels: _dataTotalPendaftaran_labels,
          datasets: [{
            data: _dataTotalPendaftaran_datasets,
            label: "Jumlah",
            borderColor: "#3e95cd",
            fill: false
          }]
        },
        options: {
          title: {
            display: true,
            text: [
              `Pendaftaran Santri Baru`,
              `${moment(_filterStartDate).format("DD MMM YYYY")} s/d ${moment(_filterEndDate).format("DD MMM YYYY")}`,
              `Total: ${_dataTotalPendaftaran_total} ${getStatusPsb(_filterStatus)}`
            ],
            fontSize: 15,
            padding: 15
          },
          legend: {
            display: false
          },
          plugins: {
            datalabels: {
              font: {
                weight: "bold"
              },
              color: "#333",
              backgroundColor: "#fff",
              borderColor: "#3e95cd",
              borderWidth: 1,
              borderRadius: 3,
              opacity: 0.8,
            },
          },
          responsive: true,
        },
      });
    };

    $(".btn-download-chart-pendaftaran").on("click", function() {
      var link = document.createElement('a');
      link.href = chartPendaftaran.toBase64Image();
      link.download = 'chart-pendaftaran-by-tanggal.png';
      link.click();
    });
    // END ## Chart: Total Pendaftaran by Tanggal

    // Chart: Total Pendaftaran by Kota
    if ($("#chart-pendaftaran-kota")[0]) {
      var _dataTotalPendaftaranKota_labels = [];
      var _dataTotalPendaftaranKota_datasetsPutra = [];
      var _dataTotalPendaftaranKota_datasetsPutri = [];
      var _dataTotalPendaftaranKota_totalPutra = 0;
      var _dataTotalPendaftaranKota_totalPutri = 0;
      var _dataTotalPendaftaranKota_totalKota = 0;

      if (_dataTotalPendaftaranKota.length > 0) {
        _dataTotalPendaftaranKota.map((item, index) => {
          _dataTotalPendaftaranKota_labels.push(item.kota);
          _dataTotalPendaftaranKota_datasetsPutra.push(item.laki_laki);
          _dataTotalPendaftaranKota_datasetsPutri.push(item.perempuan);

          _dataTotalPendaftaranKota_totalKota = parseInt(_dataTotalPendaftaranKota_totalKota) + 1;
          _dataTotalPendaftaranKota_totalPutra = parseInt(_dataTotalPendaftaranKota_totalPutra) + parseInt(item.laki_laki);
          _dataTotalPendaftaranKota_totalPutri = parseInt(_dataTotalPendaftaranKota_totalPutri) + parseInt(item.perempuan);
        });
      };

      var chartPendaftaranKota = new Chart(document.getElementById("chart-pendaftaran-kota"), {
        type: "horizontalBar",
        data: {
          labels: _dataTotalPendaftaranKota_labels,
          datasets: [{
            data: _dataTotalPendaftaranKota_datasetsPutra,
            label: "Putra",
            backgroundColor: "#3e95cd",
            borderColor: "#3e95cd",
          }, {
            data: _dataTotalPendaftaranKota_datasetsPutri,
            label: "Putri",
            backgroundColor: "#ff6b68",
            borderColor: "#ff6b68",
          }]
        },
        options: {
          title: {
            display: true,
            text: [
              "Pendaftaran Santri Baru",
              "Kota: " + _dataTotalPendaftaranKota_totalKota + " / Putra: " + _dataTotalPendaftaranKota_totalPutra + " / Putri: " + _dataTotalPendaftaranKota_totalPutri
            ],
            fontSize: 15,
          },
          title: {
            display: true,
            text: [
              `Pendaftaran Santri Baru`,
              `${moment(_filterStartDate).format("DD MMM YYYY")} s/d ${moment(_filterEndDate).format("DD MMM YYYY")}`,
              `Kota: ${_dataTotalPendaftaranKota_totalKota} / Putra: ${_dataTotalPendaftaranKota_totalPutra} / Putri: ${_dataTotalPendaftaranKota_totalPutri} ${getStatusPsb(_filterStatus)}`
            ],
            fontSize: 15,
            padding: 15
          },
          legend: {
            display: false
          },
          plugins: {
            datalabels: {
              font: {
                weight: "bold",
              },
              color: "#333",
              opacity: 0.8,
              anchor: "end",
              align: "end",
              clamp: true,
              color: function(ctx) {
                return ctx.dataset.borderColor
              }
            }
          },
          elements: {
            bar: {
              borderWidth: 2,
            }
          },
          responsive: true,
          maintainAspectRatio: true,
          scales: {
            yAxes: [{
              categoryPercentage: 0.7,
              barPercentage: 0.8,
            }],
          },
        },
      });
    };

    $(".btn-download-chart-pendaftaran-kota").on("click", function() {
      var link = document.createElement('a');
      link.href = chartPendaftaranKota.toBase64Image();
      link.download = 'chart-pendaftaran-by-kota.png';
      link.click();
    });
    // END ## Chart: Total Pendaftaran by Kota

    // Chart: Total Pendaftaran by Gender
    if ($("#chart-pendaftaran-gender")[0]) {
      var _dataTotalPendaftaranGender_datasets = [];
      var _dataTotalPendaftaranGender_total = 0;

      var putriTerverifikasi = _dataTotalPendaftaranGender.putri_terverifikasi;
      var putriBelumTerverifikasi = _dataTotalPendaftaranGender.putri_belum_terverifikasi;
      var putraTerverifikasi = _dataTotalPendaftaranGender.putra_terverifikasi;
      var putraBelumTerverifikasi = _dataTotalPendaftaranGender.putra_belum_terverifikasi;

      _dataTotalPendaftaranGender_datasets = [putriTerverifikasi, putriBelumTerverifikasi, putraTerverifikasi, putraBelumTerverifikasi];
      _dataTotalPendaftaranGender_total = parseInt(putriTerverifikasi) + parseInt(putriBelumTerverifikasi) + parseInt(putraTerverifikasi) + parseInt(putraBelumTerverifikasi);

      var chartPendaftaranGender = new Chart(document.getElementById("chart-pendaftaran-gender"), {
        type: "pie",
        data: {
          labels: [
            "Putri Terverifikasi",
            "Putri Belum Terverifikasi",
            "Putra Terverifikasi",
            "Putra Belum Terverifikasi"
          ],
          datasets: [{
            label: "Jumlah",
            data: _dataTotalPendaftaranGender_datasets,
            backgroundColor: [
              'rgba(204,0,1,255)',
              'rgba(224,102,101,255)',
              'rgba(12,83,149,255)',
              'rgba(111,168,221,255)'
            ],
            hoverOffset: 4
          }]
        },
        options: {
          title: {
            display: true,
            text: [
              `Diagram Pendaftar`,
              `${moment(_filterStartDate).format("DD MMM YYYY")} s/d ${moment(_filterEndDate).format("DD MMM YYYY")}`,
              `Total ${_dataTotalPendaftaranGender_total} Peserta`
            ],
            fontSize: 15,
            padding: 15
          },
          plugins: {
            datalabels: {
              font: {
                weight: "bold"
              },
              color: "#333",
              backgroundColor: "#fff",
              borderColor: "#3e95cd",
              borderWidth: 1,
              borderRadius: 3,
              opacity: 0.8,
            },
          },
          responsive: true,
          legend: {
            position: 'top',
          }
        },
      });
    };

    $(".btn-download-chart-pendaftaran-gender").on("click", function() {
      var link = document.createElement('a');
      link.href = chartPendaftaranGender.toBase64Image();
      link.download = 'chart-pendaftaran-by-gender.png';
      link.click();
    });
    // END ## Chart: Total Pendaftaran by Gender

    // Handle status
    function getStatusPsb(status) {
      switch (parseInt(status)) {
        case -1:
          return "";
          break;
        case 0:
          return "/ Belum Terverifikasi";
          break;
        case 1:
          return "/ Sudah Terverifikasi";
          break;
        case 2:
          return "/ Tidak Lulus Ujian";
          break;
        case 3:
          return "/ Lulus Ujian (Belum Jadi Santri)";
          break;
        case 4:
          return "/ Lulus Ujian (Sudah Jadi Santri)";
          break;
        default:
          return "";
          break;
      };
    };

  });
</script>