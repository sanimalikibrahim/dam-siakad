<style type="text/css">
    .img-home {
        margin-bottom: 2rem;
        width: <?= isset($app->dashboard_image_width) ? $app->dashboard_image_width . '%' : '100%' ?>;
        max-height: <?= isset($app->dashboard_image_max_height) ? $app->dashboard_image_max_height . 'px' : '450px' ?>;
        object-fit: <?= isset($app->dashboard_image_object_fit) ? $app->dashboard_image_object_fit : 'cover' ?>;
        object-position: <?= isset($app->dashboard_image_object_position) ? $app->dashboard_image_object_position : 'center' ?>;
        box-shadow: <?= (isset($app->dashboard_image_box_shadow) && $app->dashboard_image_box_shadow === '1') ? '0 1px 2px rgba(0, 0, 0, 0.1)' : 'none' ?>;
    }

    .text-small {
        font-size: 1rem;
        display: block;
        color: rgba(255, 255, 255, .8);
        font-weight: 600;
    }

    .flot-chart--xs {
        color: rgba(255, 255, 255, 0.6);
        font-size: 1.2rem;
        text-align: center;
        text-shadow: 0px 1px rgba(1, 1, 1, 0.1);
        font-weight: 500;
    }

    .stats__info h2 {
        font-size: 1.1rem;
        font-weight: 300;
    }

    @media only screen and (max-width: 768px) {
        .img-home {
            margin-bottom: 2rem;
            width: 100%;
            max-height: <?= isset($app->dashboard_image_max_height) ? $app->dashboard_image_max_height . 'px' : '450px' ?>;
            object-fit: <?= isset($app->dashboard_image_object_fit) ? $app->dashboard_image_object_fit : 'cover' ?>;
            object-position: <?= isset($app->dashboard_image_object_position) ? $app->dashboard_image_object_position : 'center' ?>;
            box-shadow: <?= (isset($app->dashboard_image_box_shadow) && $app->dashboard_image_box_shadow === '1') ? '0 1px 2px rgba(0, 0, 0, 0.1)' : 'none' ?>;
        }
    }
</style>

<div class="body-loading">
    <div class="body-loading-content">
        <div class="card">
            <div class="card-body">
                <i class="zmdi zmdi-spinner zmdi-hc-spin"></i>
                Please wait...
                <div class="mb-2"></div>
                <span style="color: #9c9c9c; font-size: 1rem;">Don't close this tab activity!</span>
            </div>
        </div>
    </div>
</div>

<!-- Dashboard image -->
<?php if (isset($app->dashboard_image_source) && !empty($app->dashboard_image_source)) : ?>
    <div class="row">
        <div class="col">
            <center>
                <img src="<?php echo base_url($app->dashboard_image_source) ?>" class="img-home" />
            </center>
        </div>
    </div>
<?php endif; ?>
<!-- END ## Dashboard image -->

<!-- Panitia PSB: Chart -->
<?php if (strtolower($this->session->userdata('user')['role']) === strtolower('Panitia PSB')) : ?>
    <div class="card">
        <div class="card-header">
            <!-- Filter -->
            <div class="filter-action">
                <div class="buttons">
                    <div class="input-group mb-0">
                        <div class="input-group-prepend">
                            <label class="input-group-text" style="height: 34.13px; background: #f2f2f2;">Filter</label>
                        </div>
                        <input type="text" name="filter_start_date" class="custom-select flatpickr-date bg-white filter-start_date" placeholder="Start Date" style="height: 34.13px; max-width: 150px;" value="<?= isset($filter_start_date) ? $filter_start_date : '' ?>" />
                        <input type="text" name="filter_end_date" class="custom-select flatpickr-date bg-white filter-end_date" placeholder="End Date" style="height: 34.13px; max-width: 150px;" value="<?= isset($filter_end_date) ? $filter_end_date : '' ?>" />
                        <select class="custom-select bg-white filter-status" style="height: 34.13px; max-width: 240px;">
                            <option disabled selected>Status &#8595;</option>
                            <option value="-1" <?= (isset($filter_status) && $filter_status == '-1') ? 'selected' : '' ?>>Semua</option>
                            <option value="0" <?= (isset($filter_status) && $filter_status == '0') ? 'selected' : '' ?>>Belum Terverifikasi</option>
                            <option value="1" <?= (isset($filter_status) && $filter_status == '1') ? 'selected' : '' ?>>Sudah Terverifikasi</option>
                            <option value="2" <?= (isset($filter_status) && $filter_status == '2') ? 'selected' : '' ?>>Tidak Lulus Ujian</option>
                            <option value="3" <?= (isset($filter_status) && $filter_status == '3') ? 'selected' : '' ?>>Lulus Ujian (Belum Jadi Santri)</option>
                            <option value="4" <?= (isset($filter_status) && $filter_status == '4') ? 'selected' : '' ?>>Lulus Ujian (Sudah Jadi Santri)</option>
                        </select>
                        <div class="input-group-apend">
                            <button class="btn btn--raised btn-primary btn--icon-text page-action-filter" style="height: 34.13px;">
                                <i class="zmdi zmdi-filter-list"></i> Apply
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-body">
            <nav>
                <ul class="nav nav-tabs nav-responsive-2" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="nav-tanggal-tab" data-toggle="tab" href="#nav-tanggal" role="tab" aria-controls="nav-tanggal" aria-selected="true">Tanggal</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="nav-kota-tab" data-toggle="tab" href="#nav-kota" role="tab" aria-controls="nav-kota" aria-selected="false">Kota</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="nav-gender-tab" data-toggle="tab" href="#nav-gender" role="tab" aria-controls="nav-gender" aria-selected="false">Jenis Kelamin</a>
                    </li>
                </ul>
            </nav>
            <div class="tab-content pb-0" id="nav-tabContent">
                <!-- By tanggal -->
                <div class="tab-pane fade show active" id="nav-tanggal" role="tabpanel" aria-labelledby="nav-tanggal-tab">
                    <div class="float-right">
                        <button class="btn btn-light btn-download-chart-pendaftaran">
                            <i class="zmdi zmdi-download"></i> Download
                        </button>
                    </div>
                    <canvas id="chart-pendaftaran" height="80"></canvas>
                </div>
                <!-- By kota -->
                <div class="tab-pane fade" id="nav-kota" role="tabpanel" aria-labelledby="nav-kota-tab">
                    <div class="float-right">
                        <button class="btn btn-light btn-download-chart-pendaftaran-kota">
                            <i class="zmdi zmdi-download"></i> Download
                        </button>
                    </div>
                    <canvas id="chart-pendaftaran-kota" height="<?= ($data_pendaftaran_kota_count != 0) ? $data_pendaftaran_kota_count * 12 : 60 ?>"></canvas>
                </div>
                <!-- By gender -->
                <div class="tab-pane fade" id="nav-gender" role="tabpanel" aria-labelledby="nav-gender-tab">
                    <div class="float-right">
                        <button class="btn btn-light btn-download-chart-pendaftaran-gender">
                            <i class="zmdi zmdi-download"></i> Download
                        </button>
                    </div>
                    <canvas id="chart-pendaftaran-gender" height="80"></canvas>
                </div>
            </div>
        </div>
    </div>
<?php endif ?>
<!-- END ## Panitia PSB: Chart -->

<!-- Jadwal Shalat -->
<div class="row stats">
    <div class="<?= ($app->is_mobile) ? 'col-6' : 'col' ?>">
        <div class="stats__item">
            <div class="stats__chart bg-blue-grey">
                <div class="flot-chart flot-chart--xs stats-chart-1"><?= $data_jadwal_shalat->Shubuh->mulai ?></div>
            </div>

            <div class="stats__info">
                <div>
                    <h2>Shubuh</h2>
                </div>
            </div>
        </div>
    </div>
    <div class="<?= ($app->is_mobile) ? 'col-6' : 'col' ?>">
        <div class="stats__item">
            <div class="stats__chart bg-cyan">
                <div class="flot-chart flot-chart--xs stats-chart-2"><?= $data_jadwal_shalat->Dzuhur->mulai ?></div>
            </div>

            <div class="stats__info">
                <div>
                    <h2>Dzuhur</h2>
                </div>
            </div>
        </div>
    </div>
    <div class="<?= ($app->is_mobile) ? 'col-6' : 'col' ?>">
        <div class="stats__item">
            <div class="stats__chart bg-teal">
                <div class="flot-chart flot-chart--xs stats-chart-3"><?= $data_jadwal_shalat->Ashar->mulai ?></div>
            </div>

            <div class="stats__info">
                <div>
                    <h2>Ashar</h2>
                </div>
            </div>
        </div>
    </div>
    <div class="<?= ($app->is_mobile) ? 'col-6' : 'col' ?>">
        <div class="stats__item">
            <div class="stats__chart bg-indigo">
                <div class="flot-chart flot-chart--xs stats-chart-4"><?= $data_jadwal_shalat->Magrib->mulai ?></div>
            </div>

            <div class="stats__info">
                <div>
                    <h2>Magrib</h2>
                </div>
            </div>
        </div>
    </div>
    <div class="<?= ($app->is_mobile) ? 'col-12' : 'col' ?>">
        <div class="stats__item">
            <div class="stats__chart bg-brown">
                <div class="flot-chart flot-chart--xs stats-chart-5"><?= $data_jadwal_shalat->Isya->mulai ?></div>
            </div>

            <div class="stats__info">
                <div>
                    <h2>Isya</h2>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END ## Jadwal Shalat -->