<div class="modal fade" id="modal-form-quransurah" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title pull-left">Quran Surah</h5>
      </div>
      <div class="spinner">
        <div class="lds-hourglass"></div>
      </div>
      <div class="modal-body">
        <form id="form-quransurah" autocomplete="off">
          <!-- CSRF -->
          <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />

          <div class="form-group">
            <label>Juzz</label>
            <input type="number" name="juzz" class="form-control mask-number quransurah-juzz" maxlength="5" placeholder="Juzz" />
            <i class="form-group__bar"></i>
          </div>

          <div class="form-group">
            <label required>Nama</label>
            <input type="text" name="nama" class="form-control quransurah-nama" maxlength="50" placeholder="Nama" required />
            <i class="form-group__bar"></i>
          </div>

          <div class="form-group">
            <label>Arti Nama</label>
            <input type="text" name="arti_nama" class="form-control quransurah-arti_nama" maxlength="200" placeholder="Arti Nama" />
            <i class="form-group__bar"></i>
          </div>

          <div class="form-group">
            <label>Jumlah Ayat</label>
            <input type="number" name="jumlah_ayat" class="form-control mask-number quransurah-jumlah_ayat" maxlength="5" placeholder="Jumlah Ayat" />
            <i class="form-group__bar"></i>
          </div>

          <small class="form-text text-muted">
            Fields with red stars (<label required></label>) are required.
          </small>

        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success btn--icon-text quransurah-action-save">
          <i class="zmdi zmdi-save"></i> Save
        </button>
        <button type="button" class="btn btn-light btn--icon-text quransurah-action-cancel" data-dismiss="modal">
          Cancel
        </button>
      </div>
    </div>
  </div>
</div>