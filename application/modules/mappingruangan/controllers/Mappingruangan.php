<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . 'controllers/AppBackend.php');

use alhimik1986\PhpExcelTemplator\PhpExcelTemplator;

class Mappingruangan extends AppBackend
{
  private $_nomorPeserta_male;
  private $_nomorPeserta_female;

  function __construct()
  {
    parent::__construct();
    $this->load->model([
      'AppModel',
      'PsbModel',
      'RuanganModel',
      'RuanganTipeModel',
      'MappingRuanganModel'
    ]);

    $this->_nomorPeserta_male = 1;
    $this->_nomorPeserta_female = 1;
  }

  private function _assignUserNomorPeserta($users = array(), $gender = null)
  {
    $result = array();

    if (count($users) > 0) {
      foreach ($users as $index => $item) {
        $nomorPeserta = str_pad(($gender === 'male') ? $this->_nomorPeserta_male : $this->_nomorPeserta_female, 3, '0', STR_PAD_LEFT);
        $nomorPeserta = ($gender === 'male') ? '1' . $nomorPeserta : '2' . $nomorPeserta;

        $result[] = (object) array(
          'id' => $item->id,
          'nomor_peserta' => $nomorPeserta
        );

        if ($gender === 'male') {
          $this->_nomorPeserta_male = $this->_nomorPeserta_male + 1;
        } else {
          $this->_nomorPeserta_female = $this->_nomorPeserta_female + 1;
        };
      };
    };

    return $result;
  }

  private function _assignUserToRuangan($users = array(), $ruangan = array(), $year = null)
  {
    $result = array();
    $lastIndex = 0;
    $isStopLoop = false;

    if (count($users) > 0 && count($ruangan) > 0) {
      foreach ($ruangan as $index => $item) {
        $kapasitas = (int) $item->kapasitas + (int) $lastIndex;

        if ($isStopLoop === false) {
          for ($i = $lastIndex; $i < $kapasitas; $i++) {
            if (isset($users[$i])) {
              $result[] = array(
                'ruangan_id' => $item->id,
                'psb_id' => $users[$i]->id,
                'nomor_peserta' => $users[$i]->nomor_peserta,
                'tahun' => $year,
                'created_by' => $this->session->userdata('user')['id'],
              );
            } else {
              $isStopLoop = true;
            };
          };
          $lastIndex = $i;
        };
      };
    };

    return $result;
  }

  private function _deleteDirectory($dir)
  {
    $files = array_diff(scandir($dir), array('.', '..'));

    foreach ($files as $file) {
      (is_dir("$dir/$file")) ? $this->_deleteDirectory("$dir/$file") : unlink("$dir/$file");
    };

    return @rmdir($dir);
  }

  private function _generateZip($source, $fileName)
  {
    if (!extension_loaded('zip') || !file_exists($source)) {
      return false;
    };

    $zip = new ZipArchive();
    if (!$zip->open($fileName, ZIPARCHIVE::CREATE)) {
      return false;
    };

    $source = str_replace('\\', '/', realpath($source));

    if (is_dir($source) === true) {
      $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source), RecursiveIteratorIterator::SELF_FIRST);

      foreach ($files as $file) {
        $file = str_replace('\\', '/', $file);

        if (in_array(substr($file, strrpos($file, '/') + 1), array('.', '..'))) continue;

        $file = realpath($file);

        if (is_dir($file) === true) {
          $zip->addEmptyDir(str_replace($source . '/', '', $file . '/'));
        } else if (is_file($file) === true) {
          $zip->addFromString(str_replace($source . '/', '', $file), file_get_contents($file));
        };
      };
    } else if (is_file($source) === true) {
      $zip->addFromString(basename($source), file_get_contents($source));
    };

    return $zip->close();
  }

  public function index()
  {
    $year = $this->input->get('year');

    $ruanganTipe = $this->RuanganTipeModel->getAll(array('use_for_test' => 1, 'is_active' => 1));
    $ruanganTipeIds = $this->RuanganTipeModel->getListId(array('use_for_test' => 1, 'is_active' => 1));
    $ruanganList = array();

    if (count($ruanganTipe) > 0) {
      foreach ($ruanganTipe as $index => $item) {
        $ruanganTemp = $this->MappingRuanganModel->getAllHead(strtolower($item->nama), $year);
        $ruanganList[$item->id] = $ruanganTemp;
      };
    };

    $data = array(
      'app' => $this->app(),
      'main_js' => $this->load_main_js('mappingruangan', false, array(
        'list_ruangan_tipe_id' => json_encode($ruanganTipeIds),
        'filter_year' => $year,
      )),
      'card_title' => (!is_null($year)) ? 'Mapping Ruangan : ' . $year : 'Mapping Ruangan',
      'filter_year' => $year,
      'list_tahun' => $this->init_list($this->PsbModel->getTahunList(), 'tahun', 'tahun', $year),
      'list_ruangan_tipe' => $ruanganTipe,
      'list_ruangan' => $ruanganList,
      'last_updated' => $this->MappingRuanganModel->getLastUpdated($year),
    );
    $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
    $this->template->load_view('index', $data, TRUE);
    $this->template->render();
  }

  public function detail()
  {
    $this->handle_ajax_request();
    $ruanganId = $this->input->get('key');
    $year = $this->input->get('year');

    $data = $this->MappingRuanganModel->getDetailByRuangan($ruanganId, $year);
    $ruangan = $this->RuanganModel->getDetail(array('id' => $ruanganId));

    echo json_encode(array(
      'data_count' => count($data),
      'data_table' => $this->load->view('ruangan_detail', array(
        'data' => $data,
        'ruangan' => $ruangan
      ), true),
    ));
  }

  public function ajax_generate()
  {
    $this->handle_ajax_request();
    $app = $this->app();
    $psbTestType = (isset($app->psb_test_type)) ? $app->psb_test_type : 'offline';
    $year = $this->input->get('year');

    $response = array('status' => false, 'data' => 'No operation.');
    $userInRuangan = array();

    $calonSantri_maleTemp = $this->PsbModel->getAllRandom($year, 'laki-laki');
    $calonSantri_male = $this->_assignUserNomorPeserta($calonSantri_maleTemp, 'male');
    $calonSantri_femaleTemp = $this->PsbModel->getAllRandom($year, 'perempuan');
    $calonSantri_female = $this->_assignUserNomorPeserta($calonSantri_femaleTemp, 'female');
    $ruanganTipe = $this->RuanganTipeModel->getAll(array('use_for_test' => 1, 'is_active' => 1));

    if (count($ruanganTipe) > 0) {
      foreach ($ruanganTipe as $index => $item) {
        // Putra
        $ruanganList_male = $this->RuanganModel->getAll(array('LOWER(tipe)' => strtolower($item->nama), 'LOWER(jenis)' => strtolower('putra')), null, null, false);
        $userInRuangan_male = $this->_assignUserToRuangan($calonSantri_male, $ruanganList_male, $year, 'male');
        // Putri
        $ruanganList_female = $this->RuanganModel->getAll(array('LOWER(tipe)' => strtolower($item->nama), 'LOWER(jenis)' => strtolower('putri')), null, null, false);
        $userInRuangan_female = $this->_assignUserToRuangan($calonSantri_female, $ruanganList_female, $year, 'female');

        $userInRuangan[$item->nama] = array_merge($userInRuangan_male, $userInRuangan_female);
      };
    };

    if (count($userInRuangan) > 0) {
      // Delete when exists
      $this->MappingRuanganModel->truncate();
      // Insert new items
      foreach ($userInRuangan as $index => $item) {
        $response = $this->MappingRuanganModel->insertBatch($item);

        // Update nomor format when online test
        if ($response['status'] === true && strtolower($psbTestType) === 'online') {
          $this->MappingRuanganModel->updateNomorPeserta_online($year);
        };
      };
    };

    echo json_encode($response);
  }

  public function excel()
  {
    try {
      $ruanganId = $this->input->get('ref');
      $year = $this->input->get('year');

      $fileTemplate = FCPATH . 'directory/templates/template-ruangan_peserta_list.xlsx';
      $callbacks = array();

      $dataItem = $this->MappingRuanganModel->getDetailByRuangan_toUpper($ruanganId, $year);

      if (count($dataItem) > 0) {
        $tipe = $this->cleanSpace($dataItem[0]->tipe);
        $namaRuangan = $this->cleanSpace($dataItem[0]->nama_ruangan);
        $jenis = $this->cleanSpace($dataItem[0]->jenis);

        $outputFileName = 'peserta_ruangan-' . $tipe . '_' . $namaRuangan . '_' . $jenis . '_' . $year . '.xlsx';
        $dataMaster = (object) array(
          'ruangan_tipe_nama' => $tipe,
          'ruangan_nama' => $namaRuangan,
          'ruangan_jenis' => $jenis,
          'tahun' => $year
        );

        $payloadMaster = $this->arrayToSetterSimple($dataMaster);
        $payload = $this->arrayToSetter($dataItem);
        $payload = array_merge($payload, $payloadMaster);

        PhpExcelTemplator::outputToFile($fileTemplate, $outputFileName, $payload, $callbacks);
      } else {
        show_404();
      };
    } catch (\Throwable $th) {
      show_error('Terjadi kesalahan ketika membuat file.', 500);
    };
  }

  public function bulk_excel()
  {
    $year = $this->input->get('year');
    $type = $this->input->get('type');
    $page = $this->input->get('page');

    $limitOffset = 30; // Data to display per page, please set in the main.js.php too
    $limitStart = ($page * $limitOffset) - $limitOffset;

    $fileTemplate = FCPATH . 'directory/templates/template-ruangan_peserta_list.xlsx';
    $pathTemp = FCPATH . 'directory/templates/ruangan_peserta_temp/';
    $pathZipTemp = FCPATH . 'directory/templates/ruangan_peserta_zip_temp/';
    $isCreateDestination = true;
    $isCreateDestination2 = true;
    $callbacks = array();

    if (!extension_loaded('zip')) {
      show_error('ZIP extension cannot be load, please contact your administrator!', 500, 'Error');
      exit;
    };

    // Create destination folder
    if (!file_exists($pathTemp)) {
      $isCreateDestination = mkdir($pathTemp, 0777, true);
    };

    // Create destination folder
    if (!file_exists($pathZipTemp)) {
      $isCreateDestination2 = mkdir($pathZipTemp, 0777, true);
    };

    if ($isCreateDestination === true && $isCreateDestination2 === true) {
      // Set memory limit
      ini_set('post_max_size', '99500M');
      ini_set('memory_limit', '-1'); // Unlimited memory
      ini_set('max_execution_time', '300');

      $model = $this->RuanganModel->getAll(array('ruangan_tipe_id' => $type), $limitStart, $limitOffset);

      if (count($model) > 0) {
        foreach ($model as $index => $ruangan) {
          $dataItem = $this->MappingRuanganModel->getDetailByRuangan_toUpper($ruangan->id, $year);

          if (count($dataItem) > 0) {
            // Generate excel
            $tipeTemp = $this->cleanSpace($dataItem[0]->tipe);
            $namaRuanganTemp = $this->cleanSpace($dataItem[0]->nama_ruangan);
            $jenisTemp = $this->cleanSpace($dataItem[0]->jenis);

            $outputFileName = $pathTemp . 'peserta_ruangan-' . $tipeTemp . '_' . $namaRuanganTemp . '_' . $jenisTemp . '_' . $year . '.xlsx';
            $dataMaster = (object) array(
              'ruangan_tipe_nama' => $tipeTemp,
              'ruangan_nama' => $namaRuanganTemp,
              'ruangan_jenis' => $jenisTemp,
              'tahun' => $year
            );

            $payloadMaster = $this->arrayToSetterSimple($dataMaster);
            $payload = $this->arrayToSetter($dataItem);
            $payload = array_merge($payload, $payloadMaster);

            PhpExcelTemplator::saveToFile($fileTemplate, $outputFileName, $payload, $callbacks);
            // END ## Generate excel
          };
        };

        // Convert excel to zip
        $fileNameZip = $page . '_peserta_ruangan-' . $tipeTemp . '_' . $year . '.zip';
        $fileNameZipPath = $pathZipTemp . '/' . $fileNameZip;
        $generateZip = $this->_generateZip($pathTemp, $fileNameZipPath);

        if ($generateZip === true) {
          // Stream the file to the client
          if (file_exists($fileNameZipPath) === true) {
            header("Content-Description: DAM SIAKAD | PSB - Peserta Ruangan Tes");
            header("Content-Type: application/zip");
            header("Content-Disposition: attachment; filename=\"$fileNameZip\"");
            header("Content-Transfer-Encoding: binary");
            header("Content-Length: " . filesize($fileNameZipPath));
            header("Pragma: no-cache");
            header("Expires: 0");
            header("Cache-Control: no-cache");
            header("Set-Cookie: fileDownload=true; path=/");
            ob_end_flush();
            ob_end_clean();
            @readfile($fileNameZipPath);
            @unlink($fileNameZipPath);

            $this->_deleteDirectory($pathTemp);
            $this->_deleteDirectory($pathZipTemp);
          } else {
            show_error('Failed while generating zip file, source file does not exist.', 500, 'Error');
          };
        } else {
          show_error('Failed while generating zip file.', 500, 'Error');
        };
        // END ## Convert to zip
      } else {
        show_404();
      };
    } else {
      show_error('Failed to create destination folder, please contact your administrator!', 500, 'Error');
    };
  }
}
