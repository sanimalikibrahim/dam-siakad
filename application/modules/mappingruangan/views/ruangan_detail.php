<?php $year =  (count($data) > 0) ? $data[0]->tahun : null ?>

<div class="detail-info">
  <div class="table-responsive">
    <table class="table table-sm table-bordered">
      <tr>
        <td width="100" class="bg-light">Ruangan</td>
        <td><?= (isset($ruangan)) ? $ruangan->nama_ruangan : '-' ?></td>
        <td rowspan="2" width="170" align="center">
          <a href="<?= base_url('mappingruangan/excel/?ref=' . $ruangan->id . '&year=' . $year) ?>" class="btn btn-secondary btn-block">
            <span class="zmdi zmdi-download mr-1"></span> Export to Excel
          </a>
        </td>
      </tr>
      <tr>
        <td width="100" class="bg-light">Jenis</td>
        <td><?= (isset($ruangan)) ? $ruangan->tipe . ' / ' . $ruangan->jenis : '-' ?></td>
      </tr>
    </table>
  </div>
</div>

<div class="table-responsive">
  <table id="table-ruangan-detail" class="table table-bordered table-sm mb-0">
    <thead>
      <tr>
        <th width="80">No</th>
        <th>Nomor Peserta</th>
        <th>NISN</th>
        <th>Nama Lengkap</th>
        <th>Jenis Kelamin</th>
      </tr>
    </thead>
    <tbody>
      <?php if (count($data) > 0) : ?>
        <?php $no = 1 ?>
        <?php foreach ($data as $index => $item) : ?>
          <tr>
            <td><?= $no++ ?></td>
            <td><?= $item->nomor_peserta ?></td>
            <td><?= $item->nisn ?></td>
            <td><?= $item->nama_lengkap ?></td>
            <td><?= $item->jenis_kelamin ?></td>
          </tr>
        <?php endforeach ?>
      <?php else : ?>
        <tr>
          <td colspan="5">Tidak ditemukan data</td>
        </tr>
      <?php endif ?>
    </tbody>
  </table>
</div>