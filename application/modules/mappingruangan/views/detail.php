<div class="modal fade" id="modal-ruangan-detail" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-dialog-centered modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title pull-left">Ruangan Detail</h5>
      </div>
      <div class="modal-body">
        <form id="form-ruangan" autocomplete="off">
          <!-- CSRF -->
          <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />

          <div class="content-partial"></div>

        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-light btn--icon-text" data-dismiss="modal">
          Close
        </button>
      </div>
    </div>
  </div>
</div>