<script type="text/javascript">
  $(document).ready(function() {

    var _section = "mappingruangan";
    var _modalDetail = "modal-ruangan-detail";
    var _listRuanganTipeId = <?= $list_ruangan_tipe_id ?>;
    var _limitExportOffset = 30; // Data to display per page, please set in the controller too
    var _countExportList = 0;

    initDatatables();

    // Handle ajax start
    $(document).ajaxStart(function() {
      $(document).find(".body-loading").fadeOut("fast", function() {
        $(this).show();
      });
    });

    // Handle ajax stop
    $(document).ajaxStop(function() {
      $(document).find(".body-loading").fadeOut("fast", function() {
        $(this).hide();
        document.body.style.overflow = "auto";
      });
    });

    // Handle submit filter
    $("#" + _section).on("click", "button.page-action-filter", function() {
      var filter_year = $(".filter-year").val();

      if (filter_year != null) {
        $(document).find(".body-loading").fadeOut("fast", function() {
          $(this).show();
        });

        // Reload page
        window.location.href = "<?php echo base_url('mappingruangan/?year=') ?>" + filter_year;
      };
    });

    // Handle data generate
    $("#" + _section).on("click", "button.page-action-generate", function() {
      var filter_year = $(".filter-year").val();

      swal({
        title: "Calon santri akan dibagi ketiap ruangan, lanjutkan?",
        text: "Pada saat generate, data yang sebelumnya akan dihapus.",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false
      }).then((result) => {
        if (result.value) {
          $.ajax({
            type: "delete",
            url: "<?php echo base_url('mappingruangan/ajax_generate/?year=') ?>" + filter_year,
            dataType: "json",
            success: function(response) {
              if (response.status) {
                notify(response.data, "success");
                window.location.href = "<?php echo base_url('mappingruangan/?year=') ?>" + filter_year;
              } else {
                notify(response.data, "danger");
              };
            }
          });
        };
      });
    });

    // Handle data detail
    $("#" + _section).on("click", "a.action-data-detail", function(e) {
      e.preventDefault();
      var url = $(this).attr("href");
      var contentPartial = $("#" + _modalDetail + " .content-partial");

      $.ajax({
        url: url,
        type: "get",
        dataType: "json",
        success: function(response) {
          $("#" + _modalDetail).modal("show");
          contentPartial.html(response.data_table);

          if (response.data_count > 0) {
            // Init datatables
            $("#table-ruangan-detail").dataTable({
              autoWidth: !1,
              responsive: {
                details: {
                  renderer: function(api, rowIdx, columns) {
                    var hideColumn = [];
                    var data = $.map(columns, function(col, i) {
                      return ($.inArray(col.columnIndex, hideColumn)) ?
                        '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                        '<td class="dt-details-td">' + col.title + ':' + '</td> ' +
                        '<td class="dt-details-td">' + col.data + '</td>' +
                        '</tr>' :
                        '';
                    }).join('');

                    return data ? $('<table/>').append(data) : false;
                  },
                  type: "inline",
                  target: 'tr',
                }
              },
              columnDefs: [{
                className: 'desktop',
                targets: [0, 1, 2, 3, 4]
              }, {
                className: 'tablet',
                targets: [0, 1, 3]
              }, {
                className: 'mobile',
                targets: [0, 3]
              }, {
                responsivePriority: 0,
                targets: -1
              }],
              language: {
                searchPlaceholder: "Search...",
              },
            });
          };
        }
      });
    });

    // Handle export download
    const handleExportDownload = async function(params, page, pageCount) {
      var spinnerContent = '<h5 class="m-0" style="color: var(--white);">Generating files (' + page + '/' + pageCount + ')...</h5>';
      spinnerContent += '<span style="color: var(--white);">You cannot cancel this process, dont close this tab activity!</span>';

      await $.fileDownload("<?php echo base_url('mappingruangan/bulk_excel/') ?>" + params, {
        prepareCallback: function() {
          $(".body-spinner").css("display", "flex");
          $(".body-spinner .body-spinner-content").html(spinnerContent);
        },
        successCallback: function() {
          $(".body-spinner").hide();
        },
        failCallback: function() {
          $(".body-spinner").hide();
          swal({
            title: "Oops",
            text: "Failed to generate your file, try again later.",
            type: "error",
            showCancelButton: false,
            confirmButtonColor: '#39bbb0',
            confirmButtonText: "OK",
            closeOnConfirm: false
          });
        },
        abortCallback: function() {
          $(".body-spinner").hide();
        }
      });
    };

    // Handle export
    $("#" + _section + " .page-action-export").on("click", function(e) {
      var isDisabled = (typeof $(this).attr("disabled") !== 'undefined') ? true : false;
      var filter_year = "<?= $filter_year ?>";
      var filter_type = $(this).attr("data-ruangan-tipe_id");
      var count_data = $(this).attr("data-ruangan_list_count");

      if (isDisabled === false) {
        _countExportList = parseInt(count_data);
        var pageCount = Math.ceil(_countExportList / _limitExportOffset);

        (async () => {
          for (page = 1; page <= pageCount; page++) {
            var params = "?year=" + filter_year;
            params += "&type=" + filter_type;
            params += "&page=" + page;

            await handleExportDownload(params, page, pageCount);
          };
        })();
      };
    });

    // Init datatables
    function initDatatables() {
      if (_listRuanganTipeId.length > 0) {
        _listRuanganTipeId.map((item, index) => {
          $("#table-ruangan-" + item).dataTable({
            pageLength: 25,
            autoWidth: !1,
            responsive: {
              details: {
                renderer: function(api, rowIdx, columns) {
                  var hideColumn = [];
                  var data = $.map(columns, function(col, i) {
                    return ($.inArray(col.columnIndex, hideColumn)) ?
                      '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                      '<td class="dt-details-td">' + col.title + ':' + '</td> ' +
                      '<td class="dt-details-td">' + col.data + '</td>' +
                      '</tr>' :
                      '';
                  }).join('');

                  return data ? $('<table/>').append(data) : false;
                },
                type: "inline",
                target: 'tr',
              }
            },
            columnDefs: [{
              className: 'desktop',
              targets: [0, 1, 2, 3, 4, 5]
            }, {
              className: 'tablet',
              targets: [0, 1, 2, 3]
            }, {
              className: 'mobile',
              targets: [0, 1]
            }, {
              responsivePriority: 2,
              targets: -1
            }],
            language: {
              searchPlaceholder: "Search...",
            },
            footerCallback: function(row, data, start, end, display) {
              var api = this.api(),
                data;

              // Remove the formatting to get integer data for summation
              var intVal = function(i) {
                return typeof i === 'string' ?
                  i.replace(/[\$,]/g, '') * 1 :
                  typeof i === 'number' ?
                  i : 0;
              };

              // Total over all pages
              total = api
                .column(4)
                .data()
                .reduce(function(a, b) {
                  return intVal(a) + intVal(b);
                }, 0);


              // Update footer
              $(api.column(4).footer()).html(total + " Orang");
            }
          });
        });
      };
    };

  });
</script>