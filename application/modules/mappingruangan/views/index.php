<style type="text/css">
    .filter-info {
        text-align: center;
        padding: 10rem 0rem;
    }

    .filter-info p {
        line-height: .75rem;
        font-size: 1.075rem;
        color: #999;
        font-weight: 400;
    }

    .filter-info .zmdi {
        font-size: 3.5rem;
        margin-bottom: 1rem;
        color: #c5c5c5;
    }

    .dataTables_wrapper {
        margin-top: 0;
    }
</style>

<div class="body-loading">
    <div class="body-loading-content">
        <div class="card">
            <div class="card-body">
                <i class="zmdi zmdi-spinner zmdi-hc-spin"></i>
                Please wait...
                <div class="mb-2"></div>
                <span style="color: #9c9c9c; font-size: 1rem;">Don't close this tab activity!</span>
            </div>
        </div>
    </div>
</div>

<?php include_once('detail.php') ?>

<section id="mappingruangan">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">
                <?php echo (isset($card_title)) ? $card_title : '' ?>
            </h4>
            <h6 class="card-subtitle"><?php echo (isset($card_subTitle)) ? $card_subTitle : '' ?></h6>

            <div class="action-buttons border-bottom border-danger pb-2 mb-2">
                <div class="table-action row">
                    <div class="buttons col">
                        <div class="input-group mb-0">
                            <div class="input-group-prepend">
                                <label class="input-group-text" style="height: 34.13px; background: #f2f2f2;">Filter</label>
                            </div>
                            <select class="custom-select filter-year" style="height: 34.13px; max-width: 150px;">
                                <?= $list_tahun ?>
                            </select>
                            <div class="input-group-apend">
                                <button class="btn btn--raised btn-primary btn--icon-text page-action-filter" style="height: 34.13px;">
                                    <i class="zmdi zmdi-filter-list"></i> Apply
                                </button>
                                <?php if (!is_null($filter_year)) : ?>
                                    <button class="btn btn--raised btn-success btn--icon-text page-action-generate" style="height: 34.13px;">
                                        <i class="zmdi zmdi-group"></i> Generate
                                    </button>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php if (count($list_ruangan_tipe) > 0 && !is_null($filter_year)) : ?>
                <div class="alert alert-light text-dark mt-4 mb-2">
                    <i class="zmdi zmdi-info"></i>
                    Terakhir diperbaharui : <?= $last_updated ?>
                    &nbsp; / &nbsp;
                    PSB Tipe Tes : <?= isset($app->psb_test_type) ? strtoupper($app->psb_test_type) : '-' ?>
                </div>
                <div class="tab-container">
                    <ul class="nav nav-tabs nav-responsive" role="tablist">
                        <?php foreach ($list_ruangan_tipe as $index => $item) : ?>
                            <li class="nav-item">
                                <a class="nav-link ruangan-nav-<?= $item->id ?> <?= ($index === 0) ? 'active' : '' ?>" data-toggle="tab" href="#ruangan-<?= $item->id ?>" role="tab"><?= $item->nama ?></a>
                            </li>
                        <?php endforeach ?>
                    </ul>
                    <div class="tab-content clear-tab-content">
                        <!-- Tables -->
                        <?php foreach ($list_ruangan_tipe as $index => $item) : ?>
                            <div class="tab-pane fade show <?= ($index === 0) ? 'active' : '' ?>" id="ruangan-<?= $item->id ?>" role="tabpanel">
                                <!-- Export button -->
                                <button class="btn btn-secondary mb-3 page-action-export" data-ruangan-tipe_id="<?= $item->id ?>" data-ruangan_list_count="<?= count($list_ruangan[$item->id]) ?>">
                                    <span class="zmdi zmdi-download mr-1"></span> Export <?= $item->nama ?>
                                </button>

                                <div class="table-responsive">
                                    <table id="table-ruangan-<?= $item->id ?>" class="table table-bordered table-sm table-hover">
                                        <thead class="thead-default">
                                            <tr>
                                                <th width="100">No</th>
                                                <th>Nama Ruangan</th>
                                                <th>Jenis</th>
                                                <th>Kapasitas</th>
                                                <th>Jumlah Peserta</th>
                                                <th width="120">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if (isset($list_ruangan[$item->id]) && count($list_ruangan[$item->id])) : ?>
                                                <?php $no = 1 ?>
                                                <?php foreach ($list_ruangan[$item->id] as $indekRuangan => $ruangan) : ?>
                                                    <tr>
                                                        <td><?= $no++ ?></td>
                                                        <td><?= $ruangan->nama_ruangan ?></td>
                                                        <td><?= $ruangan->jenis ?></td>
                                                        <td><?= number_format($ruangan->kapasitas) ?></td>
                                                        <td><?= number_format($ruangan->user_assigned) ?></td>
                                                        <td>
                                                            <div class="action">
                                                                <a href="<?= base_url('mappingruangan/detail/?key=' . $ruangan->id . '&year=' . $filter_year) ?>" class="btn btn-sm btn-light btn-table-action action-data-detail">
                                                                    <i class="zmdi zmdi-eye"></i> View
                                                                </a>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                <?php endforeach ?>
                                            <?php else : ?>
                                                <tr>
                                                    <td colspan="5">Tidak ditemukan data</td>
                                                </tr>
                                            <?php endif ?>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th colspan="4" style="text-align:right">Total Peserta :</th>
                                                <th></th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        <?php endforeach ?>
                    </div>
                </div>
            <?php else : ?>
                <div class="filter-info filter-no-data">
                    <i class="zmdi zmdi-filter-frames"></i>
                    <p>Tidak ditemukan data</p>
                    <p>Silahkan lakukan filter terlebih dahulu</p>
                </div>
            <?php endif ?>
        </div>
    </div>
</section>