<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . 'controllers/AppBackend.php');
require_once(FCPATH . 'vendor/autoload.php');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Laporan extends AppBackend
{
  function __construct()
  {
    parent::__construct();
    $this->load->model([
      'AppModel',
      'PengajuanModel',
      'PengajuanitemModel',
      'KelasModel',
      'PsbModel',
      'PsbprestasiModel'
    ]);
  }

  private function _getReportBuku($id, $renderAsData = false)
  {
    $pengajuan = $this->PengajuanModel->getDetail(['id' => $id]);
    $pengajuanItem = $this->PengajuanitemModel->getAll(['pengajuan_id' => $id]);
    $pengajuan_ttd = $this->_getTtd($id);

    if ($pengajuan) {
      return $this->load->view('report_pembelian', array(
        'controller' => $this,
        'pengajuan' => $pengajuan,
        'pengajuan_item' => $pengajuanItem,
        'pengajuan_ttd' => $pengajuan_ttd
      ), $renderAsData);
    } else {
      show_404();
    };
  }

  private function _getTtd($id)
  {
    $staff_perpustakaan =  $this->PengajuanModel->checkTtd($id, 'Sent', 'Staff Perpustakaan');
    $kabid =  $this->PengajuanModel->checkTtd($id, 'Approve', 'Kabid');
    $staff_bendahara =  $this->PengajuanModel->checkTtd($id, 'Approve', 'Staff Bendahara');

    $result = array(
      'staff_perpustakaan' => $staff_perpustakaan,
      'kabid' => $kabid,
      'staff_bendahara' => $staff_bendahara,
    );

    return (object) $result;
  }

  private function _generateZip($source, $fileName)
  {
    if (!extension_loaded('zip') || !file_exists($source)) {
      return false;
    };

    $zip = new ZipArchive();
    if (!$zip->open($fileName, ZIPARCHIVE::CREATE)) {
      return false;
    };

    $source = str_replace('\\', '/', realpath($source));

    if (is_dir($source) === true) {
      $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source), RecursiveIteratorIterator::SELF_FIRST);

      foreach ($files as $file) {
        $file = str_replace('\\', '/', $file);

        if (in_array(substr($file, strrpos($file, '/') + 1), array('.', '..'))) continue;

        $file = realpath($file);

        if (is_dir($file) === true) {
          $zip->addEmptyDir(str_replace($source . '/', '', $file . '/'));
        } else if (is_file($file) === true) {
          $zip->addFromString(str_replace($source . '/', '', $file), file_get_contents($file));
        };
      };
    } else if (is_file($source) === true) {
      $zip->addFromString(basename($source), file_get_contents($source));
    };

    return $zip->close();
  }

  private function _deleteDirectory($dir)
  {
    $files = array_diff(scandir($dir), array('.', '..'));

    foreach ($files as $file) {
      (is_dir("$dir/$file")) ? $this->_deleteDirectory("$dir/$file") : unlink("$dir/$file");
    };

    return @rmdir($dir);
  }

  public function index()
  {
    show_404();
  }

  // Buku
  public function buku()
  {
    $data = array(
      'app' => $this->app(),
      'main_js' => $this->load_main_js('laporan'),
      'card_title' => 'Laporan › Buku Perpustakaan'
    );
    $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
    $this->template->load_view('index', $data, TRUE);
    $this->template->render();
  }

  public function ajax_get_all_buku()
  {
    $this->handle_ajax_request();

    $role = $this->session->userdata('user')['role'];
    $defaultCondition = array(
      'jenis' => 'Buku Perpustakaan',
      'status' => 6,
    );
    $creatorCondition = array('created_by !=' => null);

    if (!in_array($role, array('Administrator', 'Kabid', 'Staff Bendahara', 'Staff Keuangan'))) {
      $creatorCondition = array('created_by' => $this->session->userdata('user')['id']);
    };

    $staticConditional = array_merge($defaultCondition, $creatorCondition);

    $dtAjax_config = array(
      'table_name' => 'pengajuan',
      'order_column' => 4,
      'order_column_dir' => 'desc',
      'static_conditional' => $staticConditional
    );
    $response = $this->AppModel->getData_dtAjax($dtAjax_config);

    echo json_encode($response);
  }

  public function ajax_get_report_buku($id = null)
  {
    $this->handle_ajax_request();

    if (!is_null($id)) {
      if (!$this->app()->is_mobile) {
        $this->_getReportBuku($id);
      } else {
        echo '<center><i>The preview is not available for mobile, but you can download it as a PDF to see this.</i></center>';
      };
    } else {
      show_404();
    };
  }

  public function download_pdf_report_buku($id = null)
  {
    libxml_use_internal_errors(true);

    try {
      $pengajuan = $this->PengajuanModel->getDetail(['id' => $id]);
      $content =  $this->_getReportBuku($id, true);
      $file_name = 'Laporan_Pembelian_Buku-' . $pengajuan->nomor . '.pdf';

      $mpdf = new \Mpdf\Mpdf();

      header("Content-type:application/pdf");
      header("Content-Disposition:attachment;filename=$file_name");

      $mpdf->WriteHTML($content);
      $mpdf->Output($file_name, 'D');
    } catch (\Throwable $th) {
      echo 'An error occurred while creating the file.';
    };
  }

  public function download_xls_report_buku($id = null)
  {
    libxml_use_internal_errors(true);

    try {
      $pengajuan = $this->PengajuanModel->getDetail(['id' => $id]);
      $content =  $this->_getReportBuku($id, true);
      $file_name = 'Laporan_Pembelian_Buku-' . $pengajuan->nomor . '.xls';

      $reader = new \PhpOffice\PhpSpreadsheet\Reader\Html();
      $spreadsheet = $reader->loadFromString($content);
      $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xls');
      $writer->save($file_name);
    } catch (\Throwable $e) {
      echo 'An error occurred while creating the file.';
    };
  }
  // END ## Buku

  // Santri Baru
  public function santri_baru()
  {
    $data = array(
      'app' => $this->app(),
      'main_js' => $this->load_main_js('laporan/views/santribaru/main.js.php', true),
      'card_title' => 'Laporan › Santri Baru',
      'list_tahun' => $this->init_list($this->PsbModel->getTahunList(), 'tahun', 'tahun')
    );
    $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
    $this->template->load_view('santribaru/index', $data, TRUE);
    $this->template->render();
  }

  public function ajax_get_santri_baru($tahun = null)
  {
    $this->handle_ajax_request();

    try {
      if (!is_null($tahun) && !empty($tahun) && $tahun !== 'null') {
        $model = $this->PsbModel->getAll(['status' => 4, 'tahun' => $tahun]);
        $response = array('status' => true, 'data' => count($model));
      } else {
        $response = array('status' => false, 'data' => 'Tahun is required.');
      }
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to get the data, try again later.');
    };

    echo json_encode($response);
  }

  public function download_excel_santri_baru()
  {
    $tahun = $this->input->get('tahun');
    $status = 4;
    $fileTemplate = FCPATH . 'directory/templates/template-santri_baru.xlsx';
    $pathTemp = FCPATH . 'directory/templates/santri_baru_temp/';
    $isCreateDestination = true;

    if (!extension_loaded('zip')) {
      show_error('ZIP extension cannot be load, please contact your administrator!', 500, 'Error');
      exit;
    };

    // Create destination folder
    if (!file_exists($pathTemp)) {
      $isCreateDestination = mkdir($pathTemp, 0777, true);
    };

    if ($isCreateDestination === true) {
      // Get santri data
      $model = $this->PsbModel->getAll(['status' => $status, 'tahun' => $tahun]);

      foreach ($model as $index => $item) {
        $fileName = 'Laporan-Santri_Baru_Tahun_' . $tahun . '-' . $item->nisn . '.xlsx';

        $ayah_tanggal_lahir = (!is_null($item->ayah_tanggal_lahir) && $item->ayah_tanggal_lahir != '0000-00-00') ? date('d/m/Y', strtotime($item->ayah_tanggal_lahir)) : '';
        $ibu_tanggal_lahir = (!is_null($item->ibu_tanggal_lahir) && $item->ibu_tanggal_lahir != '0000-00-00') ? date('d/m/Y', strtotime($item->ibu_tanggal_lahir)) : '';
        $wali_tanggal_lahir = (!is_null($item->wali_tanggal_lahir) && $item->wali_tanggal_lahir != '0000-00-00') ? date('d/m/Y', strtotime($item->wali_tanggal_lahir)) : '';

        // Write excel
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        $spreadsheet = $reader->load($fileTemplate);

        $spreadsheet->setActiveSheetIndex(0)
          // Santri
          ->setCellValue('C8', $item->nama_lengkap)
          ->setCellValue('C13', $item->tempat_lahir)
          ->setCellValue('S13', date('d/m/Y', strtotime($item->tanggal_lahir)))
          ->setCellValue('C15', $item->jenis_kelamin)
          ->setCellValue('C17', $item->urutan_anak)
          ->setCellValue('I17', $item->jumlah_saudara)
          ->setCellValue('C19', $item->golongan_darah)
          ->setCellValue('C21', $item->nik)
          ->setCellValue('C23', $item->npsn)
          ->setCellValue('C25', $item->nim)
          ->setCellValue('C27', $item->nisn)
          ->setCellValue('C29', $item->alamat_santri)
          ->setCellValue('C31', $item->kelurahan)
          ->setCellValue('C33', $item->kecamatan)
          ->setCellValue('C35', $item->kota)
          ->setCellValue('C37', $item->provinsi)
          ->setCellValue('E40', $item->telepon)
          ->setCellValue('Q40', $item->handphone)
          ->setCellValue('C42', $item->tempat_tinggal)
          // Ayah
          ->setCellValue('C50', $item->ayah_no_kk)
          ->setCellValue('C52', $item->ayah_nama_lengkap)
          ->setCellValue('C54', $item->ayah_tempat_lahir)
          ->setCellValue('S54', $ayah_tanggal_lahir)
          ->setCellValue('C57', $item->ayah_nik)
          ->setCellValue('C59', $item->ayah_status)
          ->setCellValue('C61', $item->ayah_pendidikan)
          ->setCellValue('C63', $item->ayah_pekerjaan)
          ->setCellValue('C67', $item->ayah_pekerjaan_jenis)
          ->setCellValue('C69', $item->ayah_pekerjaan_pangkat)
          ->setCellValue('C73', $item->ayah_alamat)
          ->setCellValue('C75', $item->ayah_kelurahan)
          ->setCellValue('C77', $item->ayah_kecamatan)
          ->setCellValue('C79', $item->ayah_kota)
          ->setCellValue('C81', $item->ayah_provinsi)
          ->setCellValue('E83', $item->ayah_telepon)
          ->setCellValue('Q83', $item->ayah_handphone)
          ->setCellValue('C85', $item->ayah_penghasilan)
          // Ibu
          ->setCellValue('C95', $item->ibu_nama_lengkap)
          ->setCellValue('C97', $item->ibu_tempat_lahir)
          ->setCellValue('S97', $ibu_tanggal_lahir)
          ->setCellValue('C99', $item->ibu_status)
          ->setCellValue('C101', $item->ibu_nik)
          ->setCellValue('C103', $item->ibu_pendidikan)
          ->setCellValue('C105', $item->ibu_pekerjaan)
          ->setCellValue('C109', $item->ibu_pekerjaan_jenis)
          ->setCellValue('C111', $item->ibu_pekerjaan_pangkat)
          ->setCellValue('C113', $item->ibu_alamat)
          ->setCellValue('C115', $item->ibu_kelurahan)
          ->setCellValue('C117', $item->ibu_kecamatan)
          ->setCellValue('C119', $item->ibu_kota)
          ->setCellValue('C121', $item->ibu_provinsi)
          ->setCellValue('E123', $item->ibu_telepon)
          ->setCellValue('Q123', $item->ibu_handphone)
          ->setCellValue('C125', $item->ibu_penghasilan)
          // Wali
          ->setCellValue('C136', $item->wali_no_kk)
          ->setCellValue('C138', $item->wali_nama_lengkap)
          ->setCellValue('C140', $item->wali_tempat_lahir)
          ->setCellValue('S140', $wali_tanggal_lahir)
          ->setCellValue('C142', $item->wali_status)
          ->setCellValue('C144', $item->wali_nik)
          ->setCellValue('C146', $item->wali_pendidikan)
          ->setCellValue('C148', $item->wali_pekerjaan)
          ->setCellValue('C152', $item->wali_pekerjaan_jenis)
          ->setCellValue('C154', $item->wali_pekerjaan_pangkat)
          ->setCellValue('C156', $item->wali_alamat)
          ->setCellValue('C158', $item->wali_kelurahan)
          ->setCellValue('C160', $item->wali_kecamatan)
          ->setCellValue('C162', $item->wali_kota)
          ->setCellValue('C164', $item->wali_provinsi)
          ->setCellValue('E166', $item->wali_telepon)
          ->setCellValue('Q166', $item->wali_handphone)
          ->setCellValue('C168', $item->wali_penghasilan)
          // Komitmen
          ->setCellValue('C183', $item->komitmen)
          ->setCellValue('C185', $item->komitmen_nominal)
          // TTD
          ->setCellValue('K190', $item->ayah_nama_lengkap);

        // Prestasi
        $prestasiData = $this->PsbprestasiModel->getAll(['psb_id' => $item->id]);

        if (count($prestasiData) > 0) {
          $prestasiStart = 176;
          $prestasiMax = 4;
          $prestasiCount = 1;

          foreach ($prestasiData as $prestasiIndex => $prestasi) {
            if ($prestasiCount <= $prestasiMax) {
              $spreadsheet->setActiveSheetIndex(0)
                ->setCellValue('A' . $prestasiStart, $prestasi->nama_lomba)
                ->setCellValue('B' . $prestasiStart, $prestasi->jenis_lomba)
                ->setCellValue('H' . $prestasiStart, $prestasi->penyelenggara)
                ->setCellValue('N' . $prestasiStart, $prestasi->tingkat)
                ->setCellValue('T' . $prestasiStart, $prestasi->peringkat)
                ->setCellValue('Y' . $prestasiStart, $prestasi->tahun);

              $prestasiStart++;
              $prestasiCount++;
            };
          };
        };
        // END ## Prestasi

        $writer = new Xlsx($spreadsheet);
        $writer->save($pathTemp . $fileName);
        // END ## Write excel
      };

      // Convert to zip
      $fileNameZip = 'Laporan-Santri_Baru_Tahun_' . $tahun . '.zip';
      $fileNameZipPath = $pathTemp . '/' . $fileNameZip;
      $generateZip = $this->_generateZip($pathTemp, $fileNameZipPath);

      if ($generateZip === true) {
        // Stream the file to the client
        if (file_exists($fileNameZipPath) === true) {
          header("Content-Description: DAM SIAKAD | PSB - Laporan Santri Baru");
          header("Content-Type: application/zip");
          header("Content-Disposition: attachment; filename=\"$fileNameZip\"");
          header("Content-Transfer-Encoding: binary");
          header("Content-Length: " . filesize($fileNameZipPath));
          header("Pragma: no-cache");
          header("Expires: 0");
          header("Cache-Control: no-cache");
          ob_end_flush();
          @readfile($fileNameZipPath);
          @unlink($fileNameZipPath);

          $this->_deleteDirectory($pathTemp);
        } else {
          show_error('Failed while generating zip file, source file does not exist.', 500, 'Error');
        };
      } else {
        show_error('Failed while generating zip file.', 500, 'Error');
      };
      // END ## Convert to zip
    } else {
      show_error('Failed to create destination folder, please contact your administrator!', 500, 'Error');
    };
  }
  // END ## Santri Baru

  // Calon Santri
  public function calon_santri()
  {
    $data = array(
      'app' => $this->app(),
      'main_js' => $this->load_main_js('laporan/views/calonsantri/main.js.php', true),
      'card_title' => 'Laporan › Calon Santri',
      'list_tahun' => $this->init_list($this->PsbModel->getTahunList(), 'tahun', 'tahun')
    );
    $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
    $this->template->load_view('calonsantri/index', $data, TRUE);
    $this->template->render();
  }

  public function ajax_get_calon_santri()
  {
    $this->handle_ajax_request();

    try {
      $tahun = $this->input->get('tahun');
      $status = $this->input->get('status');
      $gender = $this->input->get('gender');
      $status = (is_null($status) || $status === 'null') ? -1 : $status;

      if (!is_null($tahun) && !empty($tahun) && $tahun !== 'null') {
        if (in_array($status, [0, 1, 2, 3, 4])) {
          if (!is_null($gender) && !empty($gender) && $gender !== 'null') {
            if ($gender == 'All') {
              $modelFilter = ['status' => $status, 'tahun' => $tahun];
            } else {
              $modelFilter = ['status' => $status, 'tahun' => $tahun, 'jenis_kelamin' => $gender];
            };

            $model = $this->PsbModel->getAll($modelFilter);
            $response = array('status' => true, 'data' => count($model));
          } else {
            $response = array('status' => false, 'data' => 'Jenis Kelamin is required.');
          };
        } else {
          $response = array('status' => false, 'data' => 'Status is required.');
        };
      } else {
        $response = array('status' => false, 'data' => 'Tahun is required.');
      }
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to get the data, try again later.');
    };

    echo json_encode($response);
  }

  public function download_excel_calon_santri()
  {
    $tahun = $this->input->get('tahun');
    $status = $this->input->get('status');
    $gender = $this->input->get('gender');
    $page = $this->input->get('page');
    $status = (is_null($status) || $status === 'null') ? -1 : (int) $status;
    $fileTemplate = FCPATH . 'directory/templates/template-calon_santri.xlsx';
    $pathTemp = FCPATH . 'directory/templates/calon_santri_temp/';
    $pathZipTemp = FCPATH . 'directory/templates/calon_santri_zip_temp/';
    $isCreateDestination = true;
    $isCreateDestination2 = true;

    if (!extension_loaded('zip')) {
      show_error('ZIP extension cannot be load, please contact your administrator!', 500, 'Error');
      exit;
    };

    switch ($status) {
      case 0:
        $statusName = 'Belum_Bayar';
        break;
      case 1:
        $statusName = 'Sudah_Bayar';
        break;
      case 2:
        $statusName = 'Tidak_Lulus';
        break;
      case 3:
        $statusName = 'Lulus';
        break;
      case 4:
        $statusName = 'Jadi_Santri';
        break;
      default:
        $statusName = 'Undefined';
        break;
    };

    // Create destination folder
    if (!file_exists($pathTemp)) {
      $isCreateDestination = mkdir($pathTemp, 0777, true);
    };

    // Create destination folder
    if (!file_exists($pathZipTemp)) {
      $isCreateDestination2 = mkdir($pathZipTemp, 0777, true);
    };

    if ($isCreateDestination === true && $isCreateDestination2 === true) {
      // Set memory limit
      ini_set('post_max_size', '99500M');
      ini_set('memory_limit', '-1'); // Unlimited memory
      ini_set('max_execution_time', '300');

      // Get santri data
      if ($gender == 'All') {
        $modelFilter = ['status' => $status, 'tahun' => $tahun];
      } else {
        $modelFilter = ['status' => $status, 'tahun' => $tahun, 'jenis_kelamin' => $gender];
      };

      $limitOffset = 30; // Data to display per page, please set in the main.js.php too

      // for ($i = $pageStart; $i <= $pageCount; $i++) {
      $limitStart = ($page * $limitOffset) - $limitOffset;
      $model = $this->PsbModel->getAll($modelFilter, $limitStart, $limitOffset);

      if (count($model) > 0) {
        // Generate excel
        foreach ($model as $index => $item) {
          $fileName = 'Laporan-Calon_Santri-' . $statusName . '-Tahun_' . $tahun . '-' . $item->nisn . '.xlsx';

          $ayah_tanggal_lahir = (!is_null($item->ayah_tanggal_lahir) && $item->ayah_tanggal_lahir != '0000-00-00') ? date('d/m/Y', strtotime($item->ayah_tanggal_lahir)) : '';
          $ibu_tanggal_lahir = (!is_null($item->ibu_tanggal_lahir) && $item->ibu_tanggal_lahir != '0000-00-00') ? date('d/m/Y', strtotime($item->ibu_tanggal_lahir)) : '';
          $wali_tanggal_lahir = (!is_null($item->wali_tanggal_lahir) && $item->wali_tanggal_lahir != '0000-00-00') ? date('d/m/Y', strtotime($item->wali_tanggal_lahir)) : '';

          // Write excel
          $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
          $spreadsheet = $reader->load($fileTemplate);

          $spreadsheet->setActiveSheetIndex(0)
            // Santri
            ->setCellValue('C8', $item->nama_lengkap)
            ->setCellValue('C13', $item->tempat_lahir)
            ->setCellValue('S13', date('d/m/Y', strtotime($item->tanggal_lahir)))
            ->setCellValue('C15', $item->jenis_kelamin)
            ->setCellValue('C17', $item->urutan_anak)
            ->setCellValue('I17', $item->jumlah_saudara)
            ->setCellValue('C19', $item->golongan_darah)
            ->setCellValue('C21', $item->nik)
            ->setCellValue('C23', $item->npsn)
            ->setCellValue('C25', $item->nim)
            ->setCellValue('C27', $item->nisn)
            ->setCellValue('C29', $item->alamat_santri)
            ->setCellValue('C31', $item->kelurahan)
            ->setCellValue('C33', $item->kecamatan)
            ->setCellValue('C35', $item->kota)
            ->setCellValue('C37', $item->provinsi)
            ->setCellValue('E40', $item->telepon)
            ->setCellValue('Q40', $item->handphone)
            ->setCellValue('C42', $item->tempat_tinggal)
            ->setCellValue('C44', $item->sekolah_asal)
            ->setCellValue('C46', $item->sekolah_asal_alamat)
            // Ayah
            ->setCellValue('C53', $item->ayah_no_kk)
            ->setCellValue('C55', $item->ayah_nama_lengkap)
            ->setCellValue('C57', $item->ayah_tempat_lahir)
            ->setCellValue('S57', $ayah_tanggal_lahir)
            ->setCellValue('C60', $item->ayah_nik)
            ->setCellValue('C62', $item->ayah_status)
            ->setCellValue('C64', $item->ayah_pendidikan)
            ->setCellValue('C66', $item->ayah_pekerjaan)
            ->setCellValue('C70', $item->ayah_pekerjaan_jenis)
            ->setCellValue('C72', $item->ayah_pekerjaan_pangkat)
            ->setCellValue('C76', $item->ayah_alamat)
            ->setCellValue('C78', $item->ayah_kelurahan)
            ->setCellValue('C80', $item->ayah_kecamatan)
            ->setCellValue('C82', $item->ayah_kota)
            ->setCellValue('C84', $item->ayah_provinsi)
            ->setCellValue('E86', $item->ayah_telepon)
            ->setCellValue('Q86', $item->ayah_handphone)
            ->setCellValue('C88', $item->ayah_penghasilan)
            // Ibu
            ->setCellValue('C98', $item->ibu_nama_lengkap)
            ->setCellValue('C100', $item->ibu_tempat_lahir)
            ->setCellValue('S100', $ibu_tanggal_lahir)
            ->setCellValue('C102', $item->ibu_status)
            ->setCellValue('C104', $item->ibu_nik)
            ->setCellValue('C106', $item->ibu_pendidikan)
            ->setCellValue('C108', $item->ibu_pekerjaan)
            ->setCellValue('C112', $item->ibu_pekerjaan_jenis)
            ->setCellValue('C114', $item->ibu_pekerjaan_pangkat)
            ->setCellValue('C116', $item->ibu_alamat)
            ->setCellValue('C118', $item->ibu_kelurahan)
            ->setCellValue('C120', $item->ibu_kecamatan)
            ->setCellValue('C122', $item->ibu_kota)
            ->setCellValue('C124', $item->ibu_provinsi)
            ->setCellValue('E126', $item->ibu_telepon)
            ->setCellValue('Q126', $item->ibu_handphone)
            ->setCellValue('C128', $item->ibu_penghasilan)
            // Wali
            ->setCellValue('C139', $item->wali_no_kk)
            ->setCellValue('C141', $item->wali_nama_lengkap)
            ->setCellValue('C143', $item->wali_tempat_lahir)
            ->setCellValue('S143', $wali_tanggal_lahir)
            ->setCellValue('C145', $item->wali_status)
            ->setCellValue('C147', $item->wali_nik)
            ->setCellValue('C149', $item->wali_pendidikan)
            ->setCellValue('C151', $item->wali_pekerjaan)
            ->setCellValue('C155', $item->wali_pekerjaan_jenis)
            ->setCellValue('C157', $item->wali_pekerjaan_pangkat)
            ->setCellValue('C159', $item->wali_alamat)
            ->setCellValue('C161', $item->wali_kelurahan)
            ->setCellValue('C163', $item->wali_kecamatan)
            ->setCellValue('C165', $item->wali_kota)
            ->setCellValue('C167', $item->wali_provinsi)
            ->setCellValue('E169', $item->wali_telepon)
            ->setCellValue('Q169', $item->wali_handphone)
            ->setCellValue('C171', $item->wali_penghasilan)
            // Komitmen
            ->setCellValue('C186', $item->komitmen)
            ->setCellValue('C188', $item->komitmen_nominal)
            // TTD
            ->setCellValue('K193', $item->ayah_nama_lengkap);

          // Prestasi
          $prestasiData = $this->PsbprestasiModel->getAll(['psb_id' => $item->id]);

          if (count($prestasiData) > 0) {
            $prestasiStart = 179;
            $prestasiMax = 4;
            $prestasiCount = 1;

            foreach ($prestasiData as $prestasiIndex => $prestasi) {
              if ($prestasiCount <= $prestasiMax) {
                $spreadsheet->setActiveSheetIndex(0)
                  ->setCellValue('A' . $prestasiStart, $prestasi->nama_lomba)
                  ->setCellValue('B' . $prestasiStart, $prestasi->jenis_lomba)
                  ->setCellValue('H' . $prestasiStart, $prestasi->penyelenggara)
                  ->setCellValue('N' . $prestasiStart, $prestasi->tingkat)
                  ->setCellValue('T' . $prestasiStart, $prestasi->peringkat)
                  ->setCellValue('Y' . $prestasiStart, $prestasi->tahun);

                $prestasiStart++;
                $prestasiCount++;
              };
            };
          };
          // END ## Prestasi

          $writer = new Xlsx($spreadsheet);
          $writer->save($pathTemp . $fileName);
          // END ## Write excel
        };
      };

      // Convert excel to zip
      $fileNameZip = $page . '-Laporan-Calon_Santri-' . $statusName . '-Tahun_' . $tahun . '_' . $gender . '.zip';
      $fileNameZipPath = $pathZipTemp . '/' . $fileNameZip;
      $generateZip = $this->_generateZip($pathTemp, $fileNameZipPath);

      if ($generateZip === true) {
        // Stream the file to the client
        if (file_exists($fileNameZipPath) === true) {
          header("Content-Description: DAM SIAKAD | PSB - Laporan Calon Santri");
          header("Content-Type: application/zip");
          header("Content-Disposition: attachment; filename=\"$fileNameZip\"");
          header("Content-Transfer-Encoding: binary");
          header("Content-Length: " . filesize($fileNameZipPath));
          header("Pragma: no-cache");
          header("Expires: 0");
          header("Cache-Control: no-cache");
          header("Set-Cookie: fileDownload=true; path=/");
          ob_end_flush();
          @readfile($fileNameZipPath);
          @unlink($fileNameZipPath);

          $this->_deleteDirectory($pathTemp);
          $this->_deleteDirectory($pathZipTemp);
        } else {
          show_error('Failed while generating zip file, source file does not exist.', 500, 'Error');
        };
      } else {
        show_error('Failed while generating zip file.', 500, 'Error');
      };
      // END ## Convert to zip
    } else {
      show_error('Failed to create destination folder, please contact your administrator!', 500, 'Error');
    };
  }
  // END ## Calon Santri
}
