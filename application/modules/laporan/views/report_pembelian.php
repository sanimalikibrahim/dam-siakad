<style type="text/css">
  @page {
    margin: 20px;
  }

  .report-wrapper {
    margin: 0 auto;
  }

  .report-logo {
    width: 45px;
  }

  .report-form-name {
    border: 1px solid #999;
    padding: 8px;
  }

  .report-wrapper .report {
    border: 1px solid #333;
    padding: 2rem;
  }

  .table-border {
    border-collapse: collapse;
    font-size: 1rem;
  }

  .table-border th {
    border: 1px solid #999;
    padding: 8px 8px;
  }

  .table-border td {
    border: 1px solid #999;
    padding: 4px 8px;
  }

  .ttd-input {
    width: 100%;
    height: 70px;
  }

  .item-value {
    border: none;
    padding: 0;
    margin: 0;
    width: 100%;
  }

  .item-value td {
    border: none;
    padding: 0;
    margin: 0;
  }

  .text-center {
    text-align: center;
  }

  .text-right {
    text-align: right;
  }

  .pull-left {
    float: left;
    text-align: left;
  }

  .pull-right {
    float: right;
    text-align: right
  }
</style>

<div class="report-wrapper">
  <div class="report">
    <table style="width: 100%;">
      <tr>
        <th style="width: 15%; text-align: left; background: xxx;">
          <img src="<?= base_url('themes/_public/img/logo/logo-white.png') ?>" class="report-logo" />
        </th>
        <th style="font-size: 13pt; font-weight: bold; text-align: center; background: xxx;">
          LAPORAN KEUANGAN PERPUSTAKAAN
        </th>
        <th style="width: 15%; text-align: right; background: xxx;">
          <span class="report-form-name" style="font-size: .90rem; font-weight: normal;">Form B.6</span>
        </th>
      </tr>
      <tr>
        <td colspan="3">
          <table class="item-value" style="margin-top: 2rem;">
            <tr>
              <td style="width: 120px;">NOMOR</td>
              <td style="width: 10px;">:</td>
              <td><?= $pengajuan->nomor ?></td>
            </tr>
            <tr>
              <td style="width: 120px;">TANGGAL</td>
              <td style="width: 10px;">:</td>
              <td>
                <?php
                $date = $pengajuan->tanggal;
                $day = date('d', strtotime($date));
                $month = $controller->get_month(date('m', strtotime($date)));
                $year = date('Y', strtotime($date));

                echo $day . ' ' . $month . ' ' . $year;
                ?>
              </td>
            </tr>
            <tr>
              <td style="width: 120px;">NAMA BIDANG</td>
              <td style="width: 10px;">:</td>
              <td>Perpustakaan</td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td colspan="3">
          <table class="table-border" style="width: 100%; margin-top: 2rem;">
            <thead>
              <tr>
                <th class="text-center" style="width: 50px;">No</th>
                <th class="text-center">Uraian Jenis Kebutuhan</th>
                <th class="text-center" style="width: 160px;">Jumlah Kebutuhan</th>
                <th class="text-center" style="width: 120px;">Kode Satuan</th>
                <th class="text-center" style="width: 160px;">Harga Satuan</th>
                <th class="text-center" style="width: 160px;">Total Harga</th>
              </tr>
            </thead>
            <tbody>
              <?php if (count($pengajuan_item) > 0) : ?>
                <?php $no = 1; ?>
                <?php $jumlah = 0; ?>
                <?php foreach ($pengajuan_item as $key => $item) : ?>
                  <?php $jumlah = (float) $jumlah + (!is_null($item->amount) && !empty($item->amount) ? (float) $item->amount : 0) ?>
                  <tr>
                    <td class="text-center"><?= $no++ ?></td>
                    <td><?= $item->item ?></td>
                    <td class="text-center"><?= $item->quantity ?></td>
                    <td class="text-center"><?= $item->unit ?></td>
                    <td>
                      <table class="item-value">
                        <tr>
                          <td>Rp.</td>
                          <td class="text-right"><?= number_format($item->unit_price) ?></td>
                        </tr>
                      </table>
                    </td>
                    <td>
                      <table class="item-value">
                        <tr>
                          <td>Rp.</td>
                          <td class="text-right"><?= number_format($item->amount) ?></td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                <?php endforeach; ?>
              <?php endif; ?>
            </tbody>
            <tfoot>
              <tr>
                <th colspan="5" class="text-right">Jumlah</th>
                <th>
                  <table class="item-value">
                    <tr>
                      <td style="font-weight: bold;">Rp.</td>
                      <td style="font-weight: bold;" class="text-right"><?= number_format($jumlah) ?></td>
                    </tr>
                  </table>
                </th>
              </tr>
            </tfoot>
          </table>
        </td>
      </tr>
      <tr>
        <td colspan="3">
          <table class="table-border" style="width: 100%; margin-top: 2rem; text-align: center;">
            <!-- <td class="text-center">
              <p>&nbsp;</p>
              Disetujui,
              <div class="ttd-input"></div>
              <?= $pengajuan_ttd->staff_bendahara ?>
              <hr class="ttd-hr" />
              NKTAM : -
            </td>
            <td class="text-center">
              <p>Garut, <?= date('d') . ' ' . $controller->get_month(date('m')) . ' ' . date('Y') ?></p>
              Pemohon,
              <div class="ttd-input"></div>
              <?= $pengajuan_ttd->staff_perpustakaan ?>
              <hr class="ttd-hr" />
              NKTAM : -
            </td> -->
            <tr>
              <td colspan="2" class="text-right" style="padding: 0; padding-bottom: 1rem; border: none;">
                Garut, <?= date('d') . ' ' . $controller->get_month(date('m')) . ' ' . date('Y') ?>
              </td>
            </tr>
            <tr>
              <td><b>Pemohon</b></td>
              <td><b>Disetujui</b></td>
            </tr>
            <tr>
              <td style="height: 90px;">
                <?= (!is_null($pengajuan_ttd->staff_perpustakaan)) ? $pengajuan_ttd->staff_perpustakaan : '-' ?> <br />
                NKTAM : -
              </td>
              <td style="height: 90px;">
                <?= (!is_null($pengajuan_ttd->staff_bendahara)) ? $pengajuan_ttd->staff_bendahara : '-' ?> <br />
                NKTAM : -
              </td>
            </tr>
            <tr>
              <td><b>Staff Perpustakaan</b></td>
              <td><b>Staff Bendahara</b></td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </div>
</div>