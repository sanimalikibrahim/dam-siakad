<script type="text/javascript">
  $(document).ready(function() {

    var _key = "";
    var _section = "laporanbuku";
    var _table = "table-laporanbuku";
    var _modal_partial = "modal-partial";

    // Initialize DataTables: Index
    if ($("#" + _table)[0]) {
      var table_laporanbuku = $("#" + _table).DataTable({
        processing: true,
        serverSide: true,
        ajax: {
          url: "<?php echo base_url('laporan/ajax_get_all_buku/') ?>",
          type: "get"
        },
        columns: [{
            data: null,
            render: function(data, type, row, meta) {
              var nomor = meta.row + meta.settings._iDisplayStart + 1;

              if ($.inArray(row.status, ["2", "4"]) !== -1) {
                return '<span style="color: #ff6b68;">' + nomor + '</span>';
              } else if ($.inArray(row.status, ["5"]) !== -1) {
                return '<span style="color: #FF9800;">' + nomor + '</span>';
              } else {
                return '<span>' + nomor + '</span>';
              };
            }
          },
          {
            data: "nomor",
            render: function(data, type, row, meta) {
              if ($.inArray(row.status, ["2", "4"]) !== -1) {
                return '<span style="color: #ff6b68;">' + data + '</span>';
              } else if ($.inArray(row.status, ["5"]) !== -1) {
                return '<span style="color: #FF9800;">' + data + '</span>';
              } else {
                return '<span>' + data + '</span>';
              };
            }
          },
          {
            data: "tanggal",
            render: function(data, type, row, meta) {
              if ($.inArray(row.status, ["2", "4"]) !== -1) {
                return '<span style="color: #ff6b68;">' + data + '</span>';
              } else if ($.inArray(row.status, ["5"]) !== -1) {
                return '<span style="color: #FF9800;">' + data + '</span>';
              } else {
                return '<span>' + data + '</span>';
              };
            }
          },
          {
            data: "status",
            render: function(data, type, row, meta) {
              var status = getStatus(data);

              if ($.inArray(data, ["2", "4"]) !== -1) {
                return '<span style="color: #ff6b68;">' + status + '</span>';
              } else if ($.inArray(row.status, ["5"]) !== -1) {
                return '<span style="color: #FF9800;">' + status + '</span>';
              } else {
                return '<span>' + status + '</span>';
              };
            }
          },
          {
            data: "finish_at",
            render: function(data, type, row, meta) {
              if ($.inArray(row.status, ["2", "4"]) !== -1) {
                return '<span style="color: #ff6b68;">' + data + '</span>';
              } else if ($.inArray(row.status, ["5"]) !== -1) {
                return '<span style="color: #FF9800;">' + data + '</span>';
              } else {
                return '<span>' + data + '</span>';
              };
            }
          },
          {
            data: null,
            className: "center",
            render: function(data, type, row, meta) {
              var output = '';

              output += '<div class="action">';
              output += '&nbsp;<a href="javascript:;" class="btn btn-sm btn-light btn-table-action action-detail-report" title="Send Report" data-toggle="modal" data-target="#' + _modal_partial + '"><i class="zmdi zmdi-eye"></i> Detail</a>';
              output += '</div>';

              return output;
            }
          }
        ],
        autoWidth: !1,
        responsive: {
          details: {
            renderer: function(api, rowIdx, columns) {
              var hideColumn = [];
              var data = $.map(columns, function(col, i) {
                return ($.inArray(col.columnIndex, hideColumn)) ?
                  '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                  '<td class="dt-details-td">' + col.title + ':' + '</td> ' +
                  '<td class="dt-details-td">' + col.data + '</td>' +
                  '</tr>' :
                  '';
              }).join('');

              return data ? $('<table/>').append(data) : false;
            },
            type: "inline",
            target: 'tr',
          }
        },
        columnDefs: [{
          className: 'desktop',
          targets: [0, 1, 2, 3, 4, 5]
        }, {
          className: 'tablet',
          targets: [0, 1, 2, 3]
        }, {
          className: 'mobile',
          targets: [0, 1]
        }, {
          responsivePriority: 2,
          targets: -1
        }],
        pageLength: 15,
        language: {
          searchPlaceholder: "Search...",
          sProcessing: '<div style="text-align: center;"><div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div></div>'
        },
        sDom: '<"dataTables_ct"><"dataTables__top"fb>rt<"dataTables__bottom"ip><"clear">',
        buttons: [{
          extend: "excelHtml5",
          title: "Export Result"
        }, {
          extend: "print",
          title: "Export Result"
        }],
        initComplete: function(a, b) {
          $(this).closest(".dataTables_wrapper").find(".dataTables__top").prepend(
            '<div class="dataTables_buttons hidden-sm-down actions">' +
            '<span class="actions__item zmdi zmdi-refresh" data-table-action="reload" title="Reload" />' +
            '</div>'
          );
        },
      });

      $(".dataTables_filter input[type=search]").focus(function() {
        $(this).closest(".dataTables_filter").addClass("dataTables_filter--toggled")
      });

      $(".dataTables_filter input[type=search]").blur(function() {
        $(this).closest(".dataTables_filter").removeClass("dataTables_filter--toggled")
      });

      $("body").on("click", "[data-table-action]", function(a) {
        a.preventDefault();
        var b = $(this).data("table-action");
        if ("reload" === b) {
          $("#" + _table).DataTable().ajax.reload(null, false);
        };
      });
    };

    // Handle open report
    $("#" + _table).on("click", "a.action-detail-report", function(e) {
      e.preventDefault();
      var temp = table_laporanbuku.row($(this).closest('tr')).data();

      // Set key for update params, important!
      _key = temp.id;

      $("#" + _modal_partial + " .partial-title").html("Laporan");
      $("#" + _modal_partial + " .partial-content").html("");
      $("#" + _modal_partial + " .partial-footer").html("");

      // Fetch history
      $.ajax({
        url: "<?= base_url('laporan/ajax_get_report_buku/') ?>" + _key,
        type: "get",
        success: function(response) {
          var button = '';
          button += '<a href="<?= base_url('laporan/d-pdf/pembelian-buku/') ?>' + _key + '" class="btn btn-success"><i class="zmdi zmdi-download"></i> Export to PDF</a>';

          $("#" + _modal_partial + " .partial-content").html(response);
          $("#" + _modal_partial + " .partial-footer").html(button);
        }
      });
    });

    // Handle get status
    getStatus = (status) => {
      var output = 'Undefined';

      switch (status) {
        case "0":
          output = "Draft";
          break;
        case "1":
          output = "Menunggu Persetujuan Kabid";
          break;
        case "2":
          output = "Ditolak Kabid";
          break;
        case "3":
          output = "Menunggu Persetujuan Bendahara";
          break;
        case "4":
          output = "Ditolak Bendahara";
          break;
        case "5":
          output = "Proses Pembelian Buku";
          break;
        case "6":
          output = "Selesai";
          break;
        default:
          break;
      };

      return output;
    };

    // Handle number format
    formatNumber = (num) => {
      return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
    };

  });
</script>