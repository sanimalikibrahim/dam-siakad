<style type="text/css">
    .form-control[readonly] {
        opacity: 1;
    }

    .form-control-label {
        width: 100%;
        padding: .375rem 0;
        border-bottom: 1px solid #eceff1;
    }

    .laporanbuku-hitory .alert {
        padding: 10px;
        font-size: 9.5pt;
    }

    #table-laporanbuku-item_wrapper {
        margin-top: 0;
    }

    #table-laporanbuku-detail-item_wrapper {
        margin-top: 0;
    }

    .dropdown-item {
        padding: .5rem 1rem !important;
        font-size: 9.5pt !important;
    }

    #table-laporanbuku_wrapper {
        margin-top: 0;
    }
</style>

<section id="laporanbuku">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title"><?php echo (isset($card_title)) ? $card_title : '' ?></h4>
            <h6 class="card-subtitle"><?php echo (isset($card_subTitle)) ? $card_subTitle : '' ?></h6>

            <?php include_once('partial.php') ?>

            <div class="table-responsive">
                <table id="table-laporanbuku" class="table table-bordered">
                    <thead class="thead-default">
                        <tr>
                            <th width="100">No</th>
                            <th>Nomor</th>
                            <th>Tanggal</th>
                            <th>Status</th>
                            <th>Created</th>
                            <th width="130">Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</section>