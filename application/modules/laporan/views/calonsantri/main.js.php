<script type="text/javascript">
  $(document).ready(function() {

    var _section = "laporan";
    var _limitOffset = 30; // Data to display per page, please set in the controller too
    var _calonSantriCount = 0;

    // Handle ajax loader
    $(document).ajaxStart(function() {
      $(".loader").show();
      $("#" + _section + " .page-action-export").attr("disabled", true);
    });
    $(document).ajaxStop(function() {
      $(".loader").hide();
    });

    // Handle filter submit
    $("#" + _section + " .page-action-filter").on("click", function() {
      var filter_year = $(".filter-year").val();
      var filter_status = $(".filter-status").val();
      var filter_gender = $(".filter-gender").val();

      var params = "?tahun=" + filter_year;
      params += "&status=" + filter_status;
      params += "&gender=" + filter_gender;

      var statusName = '-';

      switch (parseInt(filter_status)) {
        case 0:
          statusName = 'Belum Bayar & Nonaktif';
          break;
        case 1:
          statusName = 'Sudah Bayar & Aktif';
          break;
        case 2:
          statusName = 'Tidak Lulus Ujian';
          break;
        case 3:
          statusName = 'Lulus Ujian (Belum Jadi Santri)';
          break;
          break;
        case 4:
          statusName = 'Lulus Ujian (Sudah Jadi Santri)';
          break;
        default:
          statusName = 'Undefined';
          break;
      };

      $.ajax({
        type: "get",
        url: "<?php echo base_url('laporan/ajax_get_calon_santri/') ?>" + params,
        dataType: "json",
        success: function(response) {
          if (response.status === true) {
            _calonSantriCount = response.data;

            $("#" + _section + " .filter-no-data").hide();
            $("#" + _section + " .filter-result").show();
            $("#" + _section + " .filter-result .santri-status").html(statusName);
            $("#" + _section + " .filter-result .santri-count").html(response.data);

            if (response.data == '0') {
              $("#" + _section + " .page-action-export").attr("disabled", true);
            } else {
              $("#" + _section + " .page-action-export").removeAttr("disabled");
            };
          } else {
            notify(response.data, "danger");
          };
        }
      });
    });

    // Handle export download
    const handleExportDownload = async function(params, page, pageCount) {
      var spinnerContent = '<h5 class="m-0" style="color: var(--white);">Generating files (' + page + '/' + pageCount + ')...</h5>';
      spinnerContent += '<span style="color: var(--white);">You cannot cancel this process, dont close this tab activity!</span>';

      await $.fileDownload("<?php echo base_url('laporan/d-xlsx/calon-santri/') ?>" + params, {
        prepareCallback: function() {
          $(".body-spinner").css("display", "flex");
          $(".body-spinner .body-spinner-content").html(spinnerContent);
        },
        successCallback: function() {
          $(".body-spinner").hide();
        },
        failCallback: function() {
          $(".body-spinner").hide();
          swal({
            title: "Oops",
            text: "Failed to generate your file, try again later.",
            type: "error",
            showCancelButton: false,
            confirmButtonColor: '#39bbb0',
            confirmButtonText: "OK",
            closeOnConfirm: false
          });
        },
        abortCallback: function() {
          $(".body-spinner").hide();
        }
      });
    };

    // Handle export
    $("#" + _section + " .page-action-export").on("click", function(e) {
      var isDisabled = (typeof $(this).attr("disabled") !== 'undefined') ? true : false;
      var filter_year = $(".filter-year").val();
      var filter_status = $(".filter-status").val();
      var filter_gender = $(".filter-gender").val();

      if (isDisabled === false) {
        _calonSantriCount = parseInt(_calonSantriCount);
        var pageCount = Math.ceil(_calonSantriCount / _limitOffset);

        (async () => {
          for (page = 1; page <= pageCount; page++) {
            var params = "?tahun=" + filter_year;
            params += "&status=" + filter_status;
            params += "&gender=" + filter_gender;
            params += "&page=" + page;

            await handleExportDownload(params, page, pageCount);
          };
        })();
      };
    });

  });
</script>