<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . 'controllers/AppBackend.php');

class Kegiatan extends AppBackend
{
  function __construct()
  {
    parent::__construct();
    $this->load->model([
      'AppModel',
      'KegiatanModel',
      'KegiatanitemModel',
      'JadwalshalatModel'
    ]);
    $this->load->library('form_validation');
  }

  public function index()
  {
    $data = array(
      'app' => $this->app(),
      'main_js' => $this->load_main_js('kegiatan'),
      'card_title' => 'Kegiatan'
    );
    $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
    $this->template->load_view('index', $data, TRUE);
    $this->template->render();
  }

  public function ajax_get_all()
  {
    $this->handle_ajax_request();
    $dtAjax_config = array(
      'table_name' => 'kegiatan',
      'order_column' => 3,
      'order_column_dir' => 'desc'
    );
    $response = $this->AppModel->getData_dtAjax($dtAjax_config);
    echo json_encode($response);
  }

  public function ajax_save($id = null)
  {
    $this->handle_ajax_request();
    $this->form_validation->set_rules($this->KegiatanModel->rules($id));

    if ($this->form_validation->run() === true) {
      if (is_null($id)) {
        echo json_encode($this->KegiatanModel->insert());
      } else {
        echo json_encode($this->KegiatanModel->update($id));
      };
    } else {
      $errors = validation_errors('<div>- ', '</div>');
      echo json_encode(array('status' => false, 'data' => $errors));
    };
  }

  public function ajax_delete($id)
  {
    $this->handle_ajax_request();
    echo json_encode($this->KegiatanModel->delete($id));
  }

  // Kegiatan Item
  public function item($kegiatan_id = null)
  {
    if (!is_null($kegiatan_id)) {
      $kegiatan = $this->KegiatanModel->getDetail(['id' => $kegiatan_id]);
      $jadwal_shalat = $this->JadwalshalatModel->getAll();

      if (!is_null($kegiatan)) {
        $data = array(
          'app' => $this->app(),
          'main_js' => $this->load_main_js('kegiatan'),
          'card_title' => 'Kegiatan › Item',
          'card_subTitle' => $kegiatan->judul,
          'data_kegiatan' => $kegiatan,
          'jadwal_shalat_list' => $this->init_list($jadwal_shalat, 'nama', 'nama')
        );
        $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
        $this->template->load_view('index_item', $data, TRUE);
        $this->template->render();
      } else {
        redirect(base_url('kegiatan'));
      };
    } else {
      redirect(base_url('kegiatan'));
    };
  }

  public function ajax_get_all_item($kegiatan_id = null)
  {
    $this->handle_ajax_request();
    $dtAjax_config = array(
      'table_name' => 'view_kegiatan_item',
      'order_column' => 5,
      'order_column_dir' => 'asc',
      'static_conditional' => ['kegiatan_id' => $kegiatan_id]
    );
    $response = $this->AppModel->getData_dtAjax($dtAjax_config);
    echo json_encode($response);
  }

  public function ajax_save_item($id = null)
  {
    $this->handle_ajax_request();
    $this->form_validation->set_rules($this->KegiatanitemModel->rules($id));

    if ($this->form_validation->run() === true) {
      if (is_null($id)) {
        echo json_encode($this->KegiatanitemModel->insert());
      } else {
        echo json_encode($this->KegiatanitemModel->update($id));
      };
    } else {
      $errors = validation_errors('<div>- ', '</div>');
      echo json_encode(array('status' => false, 'data' => $errors));
    };
  }

  public function ajax_delete_item($id)
  {
    $this->handle_ajax_request();
    echo json_encode($this->KegiatanitemModel->delete($id));
  }
  // END ## Kegiatan Item
}
