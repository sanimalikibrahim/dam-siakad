<div class="modal fade" id="modal-form-kegiatan_item" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title pull-left">Kegiatan Item</h5>
      </div>
      <div class="spinner">
        <div class="lds-hourglass"></div>
      </div>
      <div class="modal-body">
        <form id="form-kegiatan_item" autocomplete="off">
          <!-- CSRF -->
          <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />

          <input type="hidden" name="kegiatan_id" class="kegiatan_item-kegiatan_id" readonly />
          <input type="hidden" name="slug" class="kegiatan_item-slug" maxlength="100" readonly />

          <div class="form-group">
            <label required>Nama</label>
            <input type="text" name="nama" class="form-control kegiatan_item-nama" maxlength="255" placeholder="Nama" required />
            <i class="form-group__bar"></i>
          </div>

          <div class="form-group">
            <label required>Tipe Jawaban</label>
            <div class="select">
              <select name="tipe_jawaban" class="form-control kegiatan_item-tipe_jawaban" data-placeholder="Select &#8595;">
                <option disabled selected>Select &#8595;</option>
                <option value="Pilihan Ganda Single">Pilihan Ganda Single</option>
                <option value="Isian Bebas">Isian Bebas</option>
                <option value="File">File</option>
              </select>
              <i class="form-group__bar"></i>
            </div>
          </div>

          <div class="form-group">
            <label required>Aktif</label>
            <div class="select">
              <select name="aktif" class="form-control kegiatan_item-aktif" data-placeholder="Select &#8595;">
                <?= $jadwal_shalat_list ?>
                <option value="Kapanpun">Kapanpun</option>
              </select>
              <i class="form-group__bar"></i>
            </div>
          </div>

          <div class="kegiatan_item-options" style="display: none;">
            <div class="form-group">
              <label required>Option 1</label>
              <input type="text" name="option1" class="form-control kegiatan_item-option1" maxlength="255" placeholder="Option 1" required />
              <i class="form-group__bar"></i>
            </div>

            <div class="form-group">
              <label required>Option 2</label>
              <input type="text" name="option2" class="form-control kegiatan_item-option2" maxlength="255" placeholder="Option 2" required />
              <i class="form-group__bar"></i>
            </div>

            <div class="form-group">
              <label>Option 3</label>
              <input type="text" name="option3" class="form-control kegiatan_item-option3" maxlength="255" placeholder="Option 3" />
              <i class="form-group__bar"></i>
            </div>

            <div class="form-group">
              <label>Option 4</label>
              <input type="text" name="option4" class="form-control kegiatan_item-option4" maxlength="255" placeholder="Option 4" />
              <i class="form-group__bar"></i>
            </div>

            <div class="form-group">
              <label>Option 5</label>
              <input type="text" name="option5" class="form-control kegiatan_item-option5" maxlength="255" placeholder="Option 5" />
              <i class="form-group__bar"></i>
            </div>
          </div>

          <small class="form-text text-muted">
            Fields with red stars (<label required></label>) are required.
          </small>

        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success btn--icon-text kegiatan_item-action-save">
          <i class="zmdi zmdi-save"></i> Save
        </button>
        <button type="button" class="btn btn-light btn--icon-text kegiatan_item-action-cancel" data-dismiss="modal">
          Cancel
        </button>
      </div>
    </div>
  </div>
</div>