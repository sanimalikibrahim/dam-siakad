<section id="kegiatan_item">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title"><?php echo (isset($card_title)) ? $card_title : '' ?></h4>
            <h6 class="card-subtitle"><?php echo (isset($card_subTitle)) ? $card_subTitle : '' ?></h6>

            <div class="table-action">
                <div class="buttons">
                    <a href="<?php echo base_url('kegiatan') ?>" class="btn btn-secondary">
                        <i class="zmdi zmdi-arrow-left"></i>
                    </a>
                    <button class="btn btn--raised btn-primary btn--icon-text kegiatan_item-action-add" data-toggle="modal" data-target="#modal-form-kegiatan_item">
                        <i class="zmdi zmdi-plus-circle"></i> Add New
                    </button>
                </div>
            </div>

            <!-- Temporary -->
            <input type="hidden" class="kegiatan-id" value="<?= (isset($data_kegiatan)) ? $data_kegiatan->id : null ?>" readonly />
            <input type="hidden" class="kegiatan-slug" value="<?= (isset($data_kegiatan)) ? $data_kegiatan->slug : null ?>" readonly />

            <?php include_once('form_item.php') ?>

            <div class="table-responsive">
                <table id="table-kegiatan_item" class="table table-bordered">
                    <thead class="thead-default">
                        <tr>
                            <th width="100">No</th>
                            <th>Nama</th>
                            <th>Tipe Jawaban</th>
                            <th>Options</th>
                            <th>Aktif</th>
                            <th>Created</th>
                            <th width="170">Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</section>