<script type="text/javascript">
  $(document).ready(function() {

    var _key = "";
    var _section = "kegiatan";
    var _table = "table-kegiatan";
    var _modal = "modal-form-kegiatan";
    var _form = "form-kegiatan";
    // Kegiatan Item
    var _section_item = "kegiatan_item";
    var _table_item = "table-kegiatan_item";
    var _modal_item = "modal-form-kegiatan_item";
    var _form_item = "form-kegiatan_item";
    var _temp_kegiatan_id = $("#" + _section_item + " .kegiatan-id").val();
    var _temp_kegiatan_slug = $("#" + _section_item + " .kegiatan-slug").val();

    // Initialize DataTables: Index
    if ($("#" + _table)[0]) {
      var table_kegiatan = $("#" + _table).DataTable({
        processing: true,
        serverSide: true,
        ajax: {
          url: "<?php echo base_url('kegiatan/ajax_get_all/') ?>",
          type: "get"
        },
        columns: [{
            data: null,
            render: function(data, type, row, meta) {
              return meta.row + meta.settings._iDisplayStart + 1;
            }
          },
          {
            data: "judul"
          },
          {
            data: "slug"
          },
          {
            data: "created_at"
          }, {
            data: null,
            className: "center",
            render: function(data, type, row, meta) {
              return '<div class="action">' +
                '<a href="<?= base_url('kegiatan/item/') ?>' + row.id + '" class="btn btn-sm btn-success btn-table-action action-kegiatan-item">Item</a>&nbsp;' +
                '<a href="javascript:;" class="btn btn-sm btn-light btn-table-action action-edit" data-toggle="modal" data-target="#' + _modal + '"><i class="zmdi zmdi-edit"></i> Edit</a>&nbsp;' +
                '<a href="javascript:;" class="btn btn-sm btn-light btn-table-action action-delete"><i class="zmdi zmdi-delete"></i> Delete</a>' +
                '</div>';
            }
          }
        ],
        autoWidth: !1,
        responsive: {
          details: {
            renderer: function(api, rowIdx, columns) {
              var hideColumn = [];
              var data = $.map(columns, function(col, i) {
                return ($.inArray(col.columnIndex, hideColumn)) ?
                  '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                  '<td class="dt-details-td">' + col.title + ':' + '</td> ' +
                  '<td class="dt-details-td">' + col.data + '</td>' +
                  '</tr>' :
                  '';
              }).join('');

              return data ? $('<table/>').append(data) : false;
            },
            type: "inline",
            target: 'tr',
          }
        },
        columnDefs: [{
          className: 'desktop',
          targets: [0, 1, 2, 3, 4]
        }, {
          className: 'tablet',
          targets: [0, 1, 2, 3]
        }, {
          className: 'mobile',
          targets: [0, 1]
        }, {
          responsivePriority: 2,
          targets: -1
        }],
        pageLength: 15,
        language: {
          searchPlaceholder: "Search...",
          sProcessing: '<div style="text-align: center;"><div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div></div>'
        },
        sDom: '<"dataTables_ct"><"dataTables__top"fb>rt<"dataTables__bottom"ip><"clear">',
        buttons: [{
          extend: "excelHtml5",
          title: "Export Result"
        }, {
          extend: "print",
          title: "Export Result"
        }],
        initComplete: function(a, b) {
          $(this).closest(".dataTables_wrapper").find(".dataTables__top").prepend(
            '<div class="dataTables_buttons hidden-sm-down actions">' +
            '<span class="actions__item zmdi zmdi-refresh" data-table-action="reload" title="Reload" />' +
            '</div>'
          );
        },
      });

      $(".dataTables_filter input[type=search]").focus(function() {
        $(this).closest(".dataTables_filter").addClass("dataTables_filter--toggled")
      });

      $(".dataTables_filter input[type=search]").blur(function() {
        $(this).closest(".dataTables_filter").removeClass("dataTables_filter--toggled")
      });

      $("body").on("click", "[data-table-action]", function(a) {
        a.preventDefault();
        var b = $(this).data("table-action");
        if ("reload" === b) {
          $("#" + _table).DataTable().ajax.reload(null, false);
        };
      });
    };

    // Handle data add
    $("#" + _section).on("click", "button." + _section + "-action-add", function(e) {
      e.preventDefault();
      resetForm();
    });

    // Handle data edit
    $("#" + _table).on("click", "a.action-edit", function(e) {
      e.preventDefault();
      resetForm();
      var temp = table_kegiatan.row($(this).closest('tr')).data();

      // Set key for update params, important!
      _key = temp.id;

      $("#" + _form + " ." + _section + "-judul").val(temp.judul).trigger("input");
      $("#" + _form + " ." + _section + "-slug").val(temp.slug).trigger("input");
      $("#" + _form + " ." + _section + "-keterangan").val(temp.keterangan.replace(/<br\s*[\/]?>/gi, "\r\n")).trigger("input");

      // Handle textarea height
      setTimeout(function() {
        setTextareHeight();
      }, 500);
    });

    // Handle data submit
    $("#" + _modal + " ." + _section + "-action-save").on("click", function(e) {
      e.preventDefault();
      $.ajax({
        type: "post",
        url: "<?php echo base_url('kegiatan/ajax_save/') ?>" + _key,
        data: $("#" + _form).serialize(),
        success: function(response) {
          var response = JSON.parse(response);
          if (response.status === true) {
            resetForm();
            $("#" + _modal).modal("hide");
            $("#" + _table).DataTable().ajax.reload(null, false);
            notify(response.data, "success");
          } else {
            notify(response.data, "danger");
          };
        }
      });
    });

    // Handle data delete
    $("#" + _table).on("click", "a.action-delete", function(e) {
      e.preventDefault();
      var temp = table_kegiatan.row($(this).closest('tr')).data();

      swal({
        title: "Are you sure to delete?",
        text: "Once deleted, you will not be able to recover this data!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false
      }).then((result) => {
        if (result.value) {
          $.ajax({
            type: "delete",
            url: "<?php echo base_url('kegiatan/ajax_delete/') ?>" + temp.id,
            dataType: "json",
            success: function(response) {
              if (response.status) {
                $("#" + _table).DataTable().ajax.reload(null, false);
                notify(response.data, "success");
              } else {
                notify(response.data, "danger");
              };
            }
          });
        };
      });
    });

    // Handle form reset
    resetForm = () => {
      _key = "";
      $("#" + _form).trigger("reset");
      setTextareHeight(true);
    };

    // Handle textarea height
    setTextareHeight = (isReset = false) => {
      if (isReset === true) {
        $("#" + _form + " ." + _section + "-keterangan").css("height", "31px").keyup();
      } else {
        $("#" + _form + " ." + _section + "-keterangan").height($("#" + _form + " ." + _section + "-keterangan")[0].scrollHeight).keyup();
      };
    };

    // Handle slug based on judul
    $("#" + _form + " ." + _section + "-judul").on("keyup", function() {
      var text = $(this).val().trim();
      var result = text.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '-');

      $("#" + _form + " ." + _section + "-slug").val(result.toLowerCase());
    });

    // Kegiatan Item
    // Initialize DataTables: Index
    if ($("#" + _table_item)[0]) {
      var table_kegiatan_item = $("#" + _table_item).DataTable({
        processing: true,
        serverSide: true,
        ajax: {
          url: "<?php echo base_url('kegiatan/ajax_get_all_item/') ?>" + _temp_kegiatan_id,
          type: "get"
        },
        columns: [{
            data: null,
            render: function(data, type, row, meta) {
              return meta.row + meta.settings._iDisplayStart + 1;
            }
          },
          {
            data: "nama"
          },
          {
            data: "tipe_jawaban"
          },
          {
            data: null,
            render: function(data, type, row, meta) {
              var options = '';

              if (row.tipe_jawaban == "Pilihan Ganda Single") {
                options += (row.option1 != "" && row.option1 != null) ? '<li>' + row.option1 + '</li>' : '';
                options += (row.option2 != "" && row.option2 != null) ? '<li>' + row.option2 + '</li>' : '';
                options += (row.option3 != "" && row.option3 != null) ? '<li>' + row.option3 + '</li>' : '';
                options += (row.option4 != "" && row.option4 != null) ? '<li>' + row.option4 + '</li>' : '';
                options += (row.option5 != "" && row.option5 != null) ? '<li>' + row.option5 + '</li>' : '';
              };

              return options;
            }
          },
          {
            data: "aktif"
          },
          {
            data: "created_at"
          }, {
            data: null,
            className: "center",
            render: function(data, type, row, meta) {
              return '<div class="action">' +
                '<a href="javascript:;" class="btn btn-sm btn-light btn-table-action action-edit" data-toggle="modal" data-target="#' + _modal_item + '"><i class="zmdi zmdi-edit"></i> Edit</a>&nbsp;' +
                '<a href="javascript:;" class="btn btn-sm btn-light btn-table-action action-delete"><i class="zmdi zmdi-delete"></i> Delete</a>' +
                '</div>';
            }
          }
        ],
        autoWidth: !1,
        responsive: {
          details: {
            renderer: function(api, rowIdx, columns) {
              var hideColumn = [];
              var data = $.map(columns, function(col, i) {
                return ($.inArray(col.columnIndex, hideColumn)) ?
                  '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                  '<td class="dt-details-td">' + col.title + ':' + '</td> ' +
                  '<td class="dt-details-td">' + col.data + '</td>' +
                  '</tr>' :
                  '';
              }).join('');

              return data ? $('<table/>').append(data) : false;
            },
            type: "inline",
            target: 'tr',
          }
        },
        columnDefs: [{
          className: 'desktop',
          targets: [0, 1, 2, 3, 4, 5, 6]
        }, {
          className: 'tablet',
          targets: [0, 1, 2, 4]
        }, {
          className: 'mobile',
          targets: [0, 1]
        }, {
          responsivePriority: 2,
          targets: -1
        }],
        pageLength: 15,
        language: {
          searchPlaceholder: "Search...",
          sProcessing: '<div style="text-align: center;"><div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div></div>'
        },
        sDom: '<"dataTables_ct"><"dataTables__top"fb>rt<"dataTables__bottom"ip><"clear">',
        buttons: [{
          extend: "excelHtml5",
          title: "Export Result"
        }, {
          extend: "print",
          title: "Export Result"
        }],
        initComplete: function(a, b) {
          $(this).closest(".dataTables_wrapper").find(".dataTables__top").prepend(
            '<div class="dataTables_buttons hidden-sm-down actions">' +
            '<span class="actions__item zmdi zmdi-refresh" data-table-action="reload" title="Reload" />' +
            '</div>'
          );
        },
      });

      $(".dataTables_filter input[type=search]").focus(function() {
        $(this).closest(".dataTables_filter").addClass("dataTables_filter--toggled")
      });

      $(".dataTables_filter input[type=search]").blur(function() {
        $(this).closest(".dataTables_filter").removeClass("dataTables_filter--toggled")
      });

      $("body").on("click", "[data-table-action]", function(a) {
        a.preventDefault();
        var b = $(this).data("table-action");
        if ("reload" === b) {
          $("#" + _table_item).DataTable().ajax.reload(null, false);
        };
      });
    };

    // Handle data add
    $("#" + _section_item).on("click", "button." + _section_item + "-action-add", function(e) {
      e.preventDefault();
      resetForm_item();
    });

    // Handle data edit
    $("#" + _table_item).on("click", "a.action-edit", function(e) {
      e.preventDefault();
      resetForm_item();
      var temp = table_kegiatan_item.row($(this).closest('tr')).data();

      // Set key for update params, important!
      _key = temp.id;

      $("#" + _form_item + " ." + _section_item + "-nama").val(temp.nama).trigger("input");
      $("#" + _form_item + " ." + _section_item + "-tipe_jawaban").val(temp.tipe_jawaban).trigger("change");
      $("#" + _form_item + " ." + _section_item + "-aktif").val(temp.aktif).trigger("change");
      $("#" + _form_item + " ." + _section_item + "-option1").val(temp.option1).trigger("input");
      $("#" + _form_item + " ." + _section_item + "-option2").val(temp.option2).trigger("input");
      $("#" + _form_item + " ." + _section_item + "-option3").val(temp.option3).trigger("input");
      $("#" + _form_item + " ." + _section_item + "-option4").val(temp.option4).trigger("input");
      $("#" + _form_item + " ." + _section_item + "-option5").val(temp.option5).trigger("input");
    });

    // Handle data submit
    $("#" + _modal_item + " ." + _section_item + "-action-save").on("click", function(e) {
      e.preventDefault();
      $.ajax({
        type: "post",
        url: "<?php echo base_url('kegiatan/ajax_save_item/') ?>" + _key,
        data: $("#" + _form_item).serialize(),
        success: function(response) {
          var response = JSON.parse(response);
          if (response.status === true) {
            resetForm();
            $("#" + _modal_item).modal("hide");
            $("#" + _table_item).DataTable().ajax.reload(null, false);
            notify(response.data, "success");
          } else {
            notify(response.data, "danger");
          };
        }
      });
    });

    // Handle data delete
    $("#" + _table_item).on("click", "a.action-delete", function(e) {
      e.preventDefault();
      var temp = table_kegiatan_item.row($(this).closest('tr')).data();

      swal({
        title: "Are you sure to delete?",
        text: "Once deleted, you will not be able to recover this data!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false
      }).then((result) => {
        if (result.value) {
          $.ajax({
            type: "delete",
            url: "<?php echo base_url('kegiatan/ajax_delete_item/') ?>" + temp.id,
            dataType: "json",
            success: function(response) {
              if (response.status) {
                $("#" + _table_item).DataTable().ajax.reload(null, false);
                notify(response.data, "success");
              } else {
                notify(response.data, "danger");
              };
            }
          });
        };
      });
    });

    // Handle tipe_jawaban
    $("#" + _form_item + " ." + _section_item + "-tipe_jawaban").on("change", function() {
      var val = $(this).val();

      if (val == "Pilihan Ganda Single") {
        $("#" + _form_item + " ." + _section_item + "-options").show();
      } else {
        $("#" + _form_item + " ." + _section_item + "-options").hide();
      };
    });

    // Handle form reset
    resetForm_item = () => {
      _key = "";
      $("#" + _form_item).trigger("reset");
      $("#" + _form_item + " ." + _section_item + "-kegiatan_id").val(_temp_kegiatan_id).trigger("input");
      $("#" + _form_item + " ." + _section_item + "-slug").val(_temp_kegiatan_slug).trigger("input");
      $("#" + _form_item + " ." + _section_item + "-tipe_jawaban").trigger("change");
      $("#" + _form_item + " ." + _section_item + "-aktif").trigger("change");
    };
    // END ## Kegiatan Item

  });
</script>