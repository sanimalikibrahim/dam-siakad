<div class="modal fade" id="modal-form-kegiatan" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title pull-left">Kegiatan</h5>
      </div>
      <div class="spinner">
        <div class="lds-hourglass"></div>
      </div>
      <div class="modal-body">
        <form id="form-kegiatan" autocomplete="off">
          <!-- CSRF -->
          <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />

          <div class="form-group">
            <label required>Judul</label>
            <input type="text" name="judul" class="form-control kegiatan-judul" maxlength="255" placeholder="Judul" required />
            <i class="form-group__bar"></i>
          </div>

          <div class="form-group">
            <label required>Slug</label>
            <div class="position-relative">
              <input type="text" name="slug" class="form-control mask-slug kegiatan-slug" placeholder="Slug" required />
              <i class="form-group__bar"></i>
            </div>
            <small class="form-text text-muted">
              Use alpha-numeric or with dash.
            </small>
          </div>

          <div class="form-group" style="margin-bottom: 3rem;">
            <label>Keterangan</label>
            <textarea name="keterangan" class="form-control textarea-autosize text-counter kegiatan-keterangan" rows="1" data-max-length="250" placeholder="Keterangan" style="overflow: hidden; overflow-wrap: break-word; height: 31px;"></textarea>
            <i class="form-group__bar"></i>
          </div>

          <small class="form-text text-muted">
            Fields with red stars (<label required></label>) are required.
          </small>

        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success btn--icon-text kegiatan-action-save">
          <i class="zmdi zmdi-save"></i> Save
        </button>
        <button type="button" class="btn btn-light btn--icon-text kegiatan-action-cancel" data-dismiss="modal">
          Cancel
        </button>
      </div>
    </div>
  </div>
</div>