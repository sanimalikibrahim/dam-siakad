<section id="timeline_item">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title"><?php echo (isset($card_title)) ? $card_title : '' ?></h4>
            <h6 class="card-subtitle"><?php echo (isset($card_subTitle)) ? $card_subTitle : '' ?></h6>

            <div class="table-action">
                <div class="buttons">
                    <a href="<?php echo base_url('post/timeline') ?>" class="btn btn-secondary">
                        <i class="zmdi zmdi-arrow-left"></i>
                    </a>
                    <button class="btn btn--raised btn-primary btn--icon-text timeline_item-action-add" data-toggle="modal" data-target="#modal-form-timeline_item">
                        <i class="zmdi zmdi-plus-circle"></i> Add New
                    </button>
                </div>
            </div>

            <!-- Temporary -->
            <input type="hidden" class="timeline-id" value="<?= (isset($timeline)) ? $timeline->id : null ?>" readonly />
            <input type="hidden" class="timeline-url" value="<?= (isset($timeline)) ? $timeline->url : null ?>" readonly />

            <?php include_once('form_timeline_item.php') ?>

            <div class="table-responsive">
                <table id="table-timeline_item" class="table table-bordered">
                    <thead class="thead-default">
                        <tr>
                            <th width="100">No</th>
                            <th>Judul</th>
                            <th>Mulai</th>
                            <th>Selesai</th>
                            <th>Icon</th>
                            <th>Created</th>
                            <th width="170">Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</section>