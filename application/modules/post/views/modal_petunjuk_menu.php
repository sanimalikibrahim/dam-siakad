<!-- Modal menu setting -->
<div class="modal fade" id="modal-menu-setting">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title pull-left">Petunjuk</h5>
      </div>
      <div class="modal-body">
        <p>Agar dapat di akses oleh user lain, silahkan tambahkan menu untuk setiap post yang ingin di tampilkan. Ikuti langkah-langkah berikut :</p>
        <ol>
          <li class="mb-2">Buka Pengaturan › Konfigurasi Menu</li>
          <li class="mb-2">
            Buat menu parent (<b>Lewati bagian ini jika sudah ada</b>)
            <ul>
              <li>Klik tombol <b style="color: var(--success)">Add New</b></li>
              <li>Pilih field Parent sebagai <b style="color: var(--success)">(Empty)</b></li>
              <li>Isikan field Name sesuai keinginan Anda</li>
              <li>Unchecklist baseurl pada field Link</li>
              <li>
                Isikan field Link dengan :
                <div class="text-dark text-center mt-1 mb-1" style="background: var(--light); padding: 8px; border-radius: 2px;">
                  <b style="color: var(--success)">post, s, t</span></b>
                </div>
              </li>
              <li>Pilih field Role PIC</li>
              <li>Klik tombol <b style="color: var(--success)">Save</b></li>
            </ul>
          </li>
          <li class="mb-2">
            Buat menu child
            <ul>
              <li>Klik tombol <b style="color: var(--success)">Add New</b></li>
              <li>Pilih field Parent (Menu parent)</li>
              <li>Isikan field Name sesuai keinginan Anda</li>
              <li>Checklist baseurl pada field Link</li>
              <li>
                Isikan field Link dengan :
                <div class="text-dark text-center mt-1 mb-1" style="background: var(--light); padding: 8px; border-radius: 2px;">
                  <b style="color: var(--success)"><span class="modal-menu-setting-url"></span></b>
                </div>
              </li>
              <li>Pilih field Role PIC</li>
              <li>Klik tombol <b style="color: var(--success)">Save</b></li>
            </ul>
          </li>
          <li>Muat ulang halaman, selesai</li>
        </ol>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>