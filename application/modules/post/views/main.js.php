<script type="text/javascript">
  $(document).ready(function() {

    var _key = "";
    var _role = $(".user-role").val();
    //------- Static
    var _section_static = "static";
    var _table_static = "table-static";
    var _modal_static = "modal-form-static";
    var _form_static = "form-static";
    //------- Timeline
    var _section = "timeline";
    var _table_timeline = "table-timeline";
    var _modal_timeline = "modal-form-timeline";
    var _form_timeline = "form-timeline";
    //------- Timeline Item
    var _section_item = "timeline_item";
    var _table_item = "table-timeline_item";
    var _modal_item = "modal-form-timeline_item";
    var _form_item = "form-timeline_item";
    var _temp_timeline_id = $("#" + _section_item + " .timeline-id").val();
    var _temp_timeline_url = $("#" + _section_item + " .timeline-url").val();
    var _modalDetail = $("#modal-detail:first");
    
    // Inject state
    _isLockDaftarUlang = false;

    //------- Static

    // Initialize DataTables: Index
    if ($("#" + _table_static)[0]) {
      var table_static = $("#" + _table_static).DataTable({
        processing: true,
        serverSide: true,
        ajax: {
          url: "<?php echo base_url('post/ajax_get_all_static/') ?>",
          type: "get"
        },
        columns: [{
            data: null,
            render: function(data, type, row, meta) {
              return meta.row + meta.settings._iDisplayStart + 1;
            }
          },
          {
            data: "judul"
          },
          {
            data: "url",
            render: function(data, type, row, meta) {
              var _btnExtra = '<a href="#" class="btn btn-sm btn-dark action-menu-setting mr-2" style="padding: 0px 5px;" data-toggle="modal" data-target="#modal-menu-setting"><i class="zmdi zmdi-wrench"></i></a>';
              var _link = '<a href="<?= base_url('post/s/read/') ?>' + data + '">post/s/read/' + data + '</a>';

              if (_role === 'Administrator') {
                return _btnExtra + _link;
              } else {
                return _link;
              };
            }
          },
          {
            data: "status",
            render: function(data, type, row, meta) {
              if (parseInt(data) === 1) {
                return "Aktif";
              } else {
                return "Nonaktif";
              };
            }
          },
          {
            data: "created_at"
          }, {
            data: null,
            className: "center",
            render: function(data, type, row, meta) {
              let action = '';

              action += '<div class="action">';
              action += '<a href="javascript:;" class="btn btn-sm btn-light btn-table-action action-edit" data-toggle="modal" data-target="#' + _modal_static + '"><i class="zmdi zmdi-edit"></i> Edit</a>&nbsp;';
              action += (_role === 'Administrator') ? '<a href="javascript:;" class="btn btn-sm btn-light btn-table-action action-delete"><i class="zmdi zmdi-delete"></i> Delete</a>' : '';
              action += '</div>';

              return action;
            }
          }
        ],
        autoWidth: !1,
        responsive: {
          details: {
            renderer: function(api, rowIdx, columns) {
              var hideColumn = [];
              var data = $.map(columns, function(col, i) {
                return ($.inArray(col.columnIndex, hideColumn)) ?
                  '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                  '<td class="dt-details-td">' + col.title + ':' + '</td> ' +
                  '<td class="dt-details-td">' + col.data + '</td>' +
                  '</tr>' :
                  '';
              }).join('');

              return data ? $('<table/>').append(data) : false;
            },
            type: "inline",
            target: 'tr',
          }
        },
        columnDefs: [{
          className: 'desktop',
          targets: [0, 1, 2, 3, 4, 5]
        }, {
          className: 'tablet',
          targets: [0, 1, 2, 3]
        }, {
          className: 'mobile',
          targets: [0, 1]
        }, {
          responsivePriority: 2,
          targets: -1
        }],
        pageLength: 15,
        language: {
          searchPlaceholder: "Search...",
          sProcessing: '<div style="text-align: center;"><div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div></div>'
        },
        sDom: '<"dataTables_ct"><"dataTables__top"fb>rt<"dataTables__bottom"ip><"clear">',
        buttons: [{
          extend: "excelHtml5",
          title: "Export Result"
        }, {
          extend: "print",
          title: "Export Result"
        }],
        initComplete: function(a, b) {
          $(this).closest(".dataTables_wrapper").find(".dataTables__top").prepend(
            '<div class="dataTables_buttons hidden-sm-down actions">' +
            '<span class="actions__item zmdi zmdi-refresh" data-table-action="reload" title="Reload" />' +
            '</div>'
          );
        },
      });

      $(".dataTables_filter input[type=search]").focus(function() {
        $(this).closest(".dataTables_filter").addClass("dataTables_filter--toggled")
      });

      $(".dataTables_filter input[type=search]").blur(function() {
        $(this).closest(".dataTables_filter").removeClass("dataTables_filter--toggled")
      });

      $("body").on("click", "[data-table-action]", function(a) {
        a.preventDefault();
        var b = $(this).data("table-action");
        if ("reload" === b) {
          $("#" + _table_static).DataTable().ajax.reload(null, false);
        };
      });
    };

    // Handle menu setting
    $("#" + _table_static).on("click", "a.action-menu-setting", function(e) {
      e.preventDefault();
      var temp = table_static.row($(this).closest('tr')).data();

      $("#modal-menu-setting .modal-menu-setting-url").html("post/s/read/" + temp.url);
    });

    // Handle data add
    $("#" + _section_static).on("click", "button." + _section_static + "-action-add", function(e) {
      e.preventDefault();
      resetForm_static();
    });

    // Handle data edit
    $("#" + _table_static).on("click", "a.action-edit", function(e) {
      e.preventDefault();
      resetForm_static();
      var temp = table_static.row($(this).closest('tr')).data();

      // Set key for update params, important!
      _key = temp.id;

      $("#" + _form_static + " ." + _section_static + "-judul").val(temp.judul).trigger("input");
      tinyMCE.activeEditor.setContent(temp.konten);
      $("#" + _form_static + " ." + _section_static + "-url").val(temp.url).trigger("input");
      $("#" + _form_static + " ." + _section_static + "-status").val(temp.status).trigger("change");
    });

    // Handle data submit
    $("#" + _modal_static + " ." + _section_static + "-action-save").on("click", function(e) {
      e.preventDefault();
      tinyMCE.triggerSave();

      $.ajax({
        type: "post",
        url: "<?php echo base_url('post/ajax_save_static/') ?>" + _key,
        data: $("#" + _form_static).serialize(),
        success: function(response) {
          var response = JSON.parse(response);
          if (response.status === true) {
            resetForm();
            $("#" + _modal_static).modal("hide");
            $("#" + _table_static).DataTable().ajax.reload(null, false);
            notify(response.data, "success");
          } else {
            notify(response.data, "danger");
          };
        }
      });
    });

    // Handle data delete
    $("#" + _table_static).on("click", "a.action-delete", function(e) {
      e.preventDefault();
      var temp = table_static.row($(this).closest('tr')).data();

      swal({
        title: "Are you sure to delete?",
        text: "Once deleted, you will not be able to recover this data!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false
      }).then((result) => {
        if (result.value) {
          $.ajax({
            type: "delete",
            url: "<?php echo base_url('post/ajax_delete_static/') ?>" + temp.id,
            dataType: "json",
            success: function(response) {
              if (response.status) {
                $("#" + _table_static).DataTable().ajax.reload(null, false);
                notify(response.data, "success");
              } else {
                notify(response.data, "danger");
              };
            }
          });
        };
      });
    });

    // Handle form reset
    resetForm_static = () => {
      _key = "";
      $("#" + _form_static).trigger("reset");
    };

    // Handle url based on judul
    $("#" + _form_static + " ." + _section_static + "-judul").on("keyup", function() {
      var text = $(this).val().trim();
      var result = text.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '-');

      $("#" + _form_static + " ." + _section_static + "-url").val(result.toLowerCase());
    });

    //------- END ## Static

    //------- Timeline

    // Initialize DataTables: Index
    if ($("#" + _table_timeline)[0]) {
      var table_timeline = $("#" + _table_timeline).DataTable({
        processing: true,
        serverSide: true,
        ajax: {
          url: "<?php echo base_url('post/ajax_get_all_timeline/') ?>",
          type: "get"
        },
        columns: [{
            data: null,
            render: function(data, type, row, meta) {
              return meta.row + meta.settings._iDisplayStart + 1;
            }
          },
          {
            data: "judul"
          },
          {
            data: "url",
            render: function(data, type, row, meta) {
              var _btnExtra = '<a href="#" class="btn btn-sm btn-dark action-menu-setting mr-2" style="padding: 0px 5px;" data-toggle="modal" data-target="#modal-menu-setting"><i class="zmdi zmdi-wrench"></i></a>';
              var _link = '<a href="<?= base_url('post/t/read/') ?>' + data + '">post/t/read/' + data + '</a>';

              if (_role === 'Administrator') {
                return _btnExtra + _link;
              } else {
                return _link;
              };
            }
          },
          {
            data: "is_active",
            render: function(data, type, row, meta) {
              if (parseInt(data) === 1) {
                return "Aktif";
              } else {
                return "Nonaktif";
              };
            }
          },
          {
            data: "created_at"
          }, {
            data: null,
            className: "center",
            render: function(data, type, row, meta) {
              let action = '';

              action += '<div class="action">';
              action += '<a href="<?= base_url('post/timeline/item/') ?>' + row.id + '" class="btn btn-sm btn-success btn-table-action action-timeline-item">Item</a>&nbsp;';
              action += '<a href="javascript:;" class="btn btn-sm btn-light btn-table-action action-edit" data-toggle="modal" data-target="#' + _modal_timeline + '"><i class="zmdi zmdi-edit"></i> Edit</a>&nbsp;';
              action += (_role === 'Administrator') ? '<a href="javascript:;" class="btn btn-sm btn-light btn-table-action action-delete"><i class="zmdi zmdi-delete"></i> Delete</a>' : '';
              action += '</div>';

              return action;
            }
          }
        ],
        autoWidth: !1,
        responsive: {
          details: {
            renderer: function(api, rowIdx, columns) {
              var hideColumn = [];
              var data = $.map(columns, function(col, i) {
                return ($.inArray(col.columnIndex, hideColumn)) ?
                  '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                  '<td class="dt-details-td">' + col.title + ':' + '</td> ' +
                  '<td class="dt-details-td">' + col.data + '</td>' +
                  '</tr>' :
                  '';
              }).join('');

              return data ? $('<table/>').append(data) : false;
            },
            type: "inline",
            target: 'tr',
          }
        },
        columnDefs: [{
          className: 'desktop',
          targets: [0, 1, 2, 3, 4, 5]
        }, {
          className: 'tablet',
          targets: [0, 1, 2, 3]
        }, {
          className: 'mobile',
          targets: [0, 1]
        }, {
          responsivePriority: 2,
          targets: -1
        }],
        pageLength: 15,
        language: {
          searchPlaceholder: "Search...",
          sProcessing: '<div style="text-align: center;"><div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div></div>'
        },
        sDom: '<"dataTables_ct"><"dataTables__top"fb>rt<"dataTables__bottom"ip><"clear">',
        buttons: [{
          extend: "excelHtml5",
          title: "Export Result"
        }, {
          extend: "print",
          title: "Export Result"
        }],
        initComplete: function(a, b) {
          $(this).closest(".dataTables_wrapper").find(".dataTables__top").prepend(
            '<div class="dataTables_buttons hidden-sm-down actions">' +
            '<span class="actions__item zmdi zmdi-refresh" data-table-action="reload" title="Reload" />' +
            '</div>'
          );
        },
      });

      $(".dataTables_filter input[type=search]").focus(function() {
        $(this).closest(".dataTables_filter").addClass("dataTables_filter--toggled")
      });

      $(".dataTables_filter input[type=search]").blur(function() {
        $(this).closest(".dataTables_filter").removeClass("dataTables_filter--toggled")
      });

      $("body").on("click", "[data-table-action]", function(a) {
        a.preventDefault();
        var b = $(this).data("table-action");
        if ("reload" === b) {
          $("#" + _table_timeline).DataTable().ajax.reload(null, false);
        };
      });
    };

    // Handle menu setting
    $("#" + _table_timeline).on("click", "a.action-menu-setting", function(e) {
      e.preventDefault();
      var temp = table_timeline.row($(this).closest('tr')).data();

      $("#modal-menu-setting .modal-menu-setting-url").html("post/t/read/" + temp.url);
    });

    // Handle data add
    $("#" + _section).on("click", "button." + _section + "-action-add", function(e) {
      e.preventDefault();
      resetForm();
    });

    // Handle data edit
    $("#" + _table_timeline).on("click", "a.action-edit", function(e) {
      e.preventDefault();
      resetForm();
      var temp = table_timeline.row($(this).closest('tr')).data();

      // Set key for update params, important!
      _key = temp.id;

      $("#" + _form_timeline + " ." + _section + "-judul").val(temp.judul).trigger("input");
      $("#" + _form_timeline + " ." + _section + "-url").val(temp.url).trigger("input");
      $("#" + _form_timeline + " ." + _section + "-is_active").val(temp.is_active).trigger("change");
    });

    // Handle data submit
    $("#" + _modal_timeline + " ." + _section + "-action-save").on("click", function(e) {
      e.preventDefault();
      $.ajax({
        type: "post",
        url: "<?php echo base_url('post/ajax_save_timeline/') ?>" + _key,
        data: $("#" + _form_timeline).serialize(),
        success: function(response) {
          var response = JSON.parse(response);
          if (response.status === true) {
            resetForm();
            $("#" + _modal_timeline).modal("hide");
            $("#" + _table_timeline).DataTable().ajax.reload(null, false);
            notify(response.data, "success");
          } else {
            notify(response.data, "danger");
          };
        }
      });
    });

    // Handle data delete
    $("#" + _table_timeline).on("click", "a.action-delete", function(e) {
      e.preventDefault();
      var temp = table_timeline.row($(this).closest('tr')).data();

      swal({
        title: "Are you sure to delete?",
        text: "Once deleted, you will not be able to recover this data!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false
      }).then((result) => {
        if (result.value) {
          $.ajax({
            type: "delete",
            url: "<?php echo base_url('post/ajax_delete_timeline/') ?>" + temp.id,
            dataType: "json",
            success: function(response) {
              if (response.status) {
                $("#" + _table_timeline).DataTable().ajax.reload(null, false);
                notify(response.data, "success");
              } else {
                notify(response.data, "danger");
              };
            }
          });
        };
      });
    });

    // Handle form reset
    resetForm = () => {
      _key = "";
      $("#" + _form_timeline).trigger("reset");
    };

    // Handle url based on judul
    $("#" + _form_timeline + " ." + _section + "-judul").on("keyup", function() {
      var text = $(this).val().trim();
      var result = text.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '-');

      $("#" + _form_timeline + " ." + _section + "-url").val(result.toLowerCase());
    });

    //------- END ## Timeline

    //------- Timeline Item

    // Initialize DataTables: Index
    if ($("#" + _table_item)[0]) {
      var table_item = $("#" + _table_item).DataTable({
        processing: true,
        serverSide: true,
        ajax: {
          url: "<?php echo base_url('post/ajax_get_all_timeline_item/') ?>" + _temp_timeline_id,
          type: "get"
        },
        columns: [{
            data: null,
            render: function(data, type, row, meta) {
              return meta.row + meta.settings._iDisplayStart + 1;
            }
          },
          {
            data: "judul"
          },
          {
            data: "tanggal_mulai"
          },
          {
            data: "tanggal_selesai"
          },
          {
            data: "icon",
            render: function(data, type, row, meta) {
              return '<i class="' + data + '"></i> &nbsp; (<span style="color: ' + row.icon_bg_color + '">' + row.icon_bg_color + '</span>)';
            }
          },
          {
            data: "created_at"
          }, {
            data: null,
            className: "center",
            render: function(data, type, row, meta) {
              return '<div class="action">' +
                '<a href="javascript:;" class="btn btn-sm btn-light btn-table-action action-edit" data-toggle="modal" data-target="#' + _modal_item + '"><i class="zmdi zmdi-edit"></i> Edit</a>&nbsp;' +
                '<a href="javascript:;" class="btn btn-sm btn-light btn-table-action action-delete"><i class="zmdi zmdi-delete"></i> Delete</a>' +
                '</div>';
            }
          }
        ],
        autoWidth: !1,
        responsive: {
          details: {
            renderer: function(api, rowIdx, columns) {
              var hideColumn = [];
              var data = $.map(columns, function(col, i) {
                return ($.inArray(col.columnIndex, hideColumn)) ?
                  '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                  '<td class="dt-details-td">' + col.title + ':' + '</td> ' +
                  '<td class="dt-details-td">' + col.data + '</td>' +
                  '</tr>' :
                  '';
              }).join('');

              return data ? $('<table/>').append(data) : false;
            },
            type: "inline",
            target: 'tr',
          }
        },
        columnDefs: [{
          className: 'desktop',
          targets: [0, 1, 2, 3, 4, 5, 6]
        }, {
          className: 'tablet',
          targets: [0, 1, 2, 3]
        }, {
          className: 'mobile',
          targets: [0, 1]
        }, {
          responsivePriority: 2,
          targets: -1
        }],
        pageLength: 15,
        language: {
          searchPlaceholder: "Search...",
          sProcessing: '<div style="text-align: center;"><div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div></div>'
        },
        sDom: '<"dataTables_ct"><"dataTables__top"fb>rt<"dataTables__bottom"ip><"clear">',
        buttons: [{
          extend: "excelHtml5",
          title: "Export Result"
        }, {
          extend: "print",
          title: "Export Result"
        }],
        initComplete: function(a, b) {
          $(this).closest(".dataTables_wrapper").find(".dataTables__top").prepend(
            '<div class="dataTables_buttons hidden-sm-down actions">' +
            '<span class="actions__item zmdi zmdi-refresh" data-table-action="reload" title="Reload" />' +
            '</div>'
          );
        },
      });

      $(".dataTables_filter input[type=search]").focus(function() {
        $(this).closest(".dataTables_filter").addClass("dataTables_filter--toggled")
      });

      $(".dataTables_filter input[type=search]").blur(function() {
        $(this).closest(".dataTables_filter").removeClass("dataTables_filter--toggled")
      });

      $("body").on("click", "[data-table-action]", function(a) {
        a.preventDefault();
        var b = $(this).data("table-action");
        if ("reload" === b) {
          $("#" + _table_item).DataTable().ajax.reload(null, false);
        };
      });
    };

    // Handle data add
    $("#" + _section_item).on("click", "button." + _section_item + "-action-add", function(e) {
      e.preventDefault();
      resetForm_item();
    });

    // Handle data edit
    $("#" + _table_item).on("click", "a.action-edit", function(e) {
      e.preventDefault();
      resetForm_item();
      var temp = table_item.row($(this).closest('tr')).data();

      // Set key for update params, important!
      _key = temp.id;

      $("#" + _form_item + " ." + _section_item + "-judul").val(temp.judul).trigger("input");
      tinyMCE.activeEditor.setContent(temp.deskripsi);
      $("#" + _form_item + " ." + _section_item + "-tanggal_mulai").val(temp.tanggal_mulai).trigger("input");
      $("#" + _form_item + " ." + _section_item + "-tanggal_selesai").val(temp.tanggal_selesai).trigger("input");
      $("#" + _form_item + " ." + _section_item + "-icon").val(temp.icon).trigger("input");
      $("#" + _form_item + " ." + _section_item + "-icon_bg_color").val(temp.icon_bg_color).trigger("input");
    });

    // Handle data submit
    $("#" + _modal_item + " ." + _section_item + "-action-save").on("click", function(e) {
      e.preventDefault();
      tinyMCE.triggerSave();

      $.ajax({
        type: "post",
        url: "<?php echo base_url('post/ajax_save_timeline_item/') ?>" + _key,
        data: $("#" + _form_item).serialize(),
        success: function(response) {
          var response = JSON.parse(response);
          if (response.status === true) {
            resetForm();
            $("#" + _modal_item).modal("hide");
            $("#" + _table_item).DataTable().ajax.reload(null, false);
            notify(response.data, "success");
          } else {
            notify(response.data, "danger");
          };
        }
      });
    });

    // Handle data delete
    $("#" + _table_item).on("click", "a.action-delete", function(e) {
      e.preventDefault();
      var temp = table_item.row($(this).closest('tr')).data();

      swal({
        title: "Are you sure to delete?",
        text: "Once deleted, you will not be able to recover this data!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false
      }).then((result) => {
        if (result.value) {
          $.ajax({
            type: "delete",
            url: "<?php echo base_url('post/ajax_delete_timeline_item/') ?>" + temp.id,
            dataType: "json",
            success: function(response) {
              if (response.status) {
                $("#" + _table_item).DataTable().ajax.reload(null, false);
                notify(response.data, "success");
              } else {
                notify(response.data, "danger");
              };
            }
          });
        };
      });
    });

    // Handle form reset
    resetForm_item = () => {
      _key = "";
      $("#" + _form_item).trigger("reset");
      $("#" + _form_item + " ." + _section_item + "-timeline_id").val(_temp_timeline_id).trigger("input");
    };

    // Handle detail timeline
    $(document).on("click", ".btn-action-detail", function(e) {
      e.preventDefault();
      var key = $(this).attr("data-key");
      var content = $(".timeline-detail-item-" + key);

      _modalDetail.find(".modal-body").html("");
      _modalDetail.find(".modal-body").html(content.html());
      _modalDetail.modal("show");
    });

    //------- END ## Timeline Item

  });
</script>