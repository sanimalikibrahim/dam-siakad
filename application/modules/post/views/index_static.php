<section id="static">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title"><?php echo (isset($card_title)) ? $card_title : '' ?></h4>
            <h6 class="card-subtitle"><?php echo (isset($card_subTitle)) ? $card_subTitle : '' ?></h6>

            <?php if ($role === 'Administrator') : ?>
                <div class="table-action">
                    <div class="buttons">
                        <button class="btn btn--raised btn-primary btn--icon-text static-action-add" data-toggle="modal" data-target="#modal-form-static">
                            <i class="zmdi zmdi-plus-circle"></i> Add New
                        </button>
                    </div>
                </div>
            <?php endif ?>

            <!-- Temporary -->
            <input type="hidden" class="user-role" value="<?= $role ?>" />

            <?php include_once('form_static.php') ?>

            <div class="table-responsive">
                <table id="table-static" class="table table-bordered">
                    <thead class="thead-default">
                        <tr>
                            <th width="100">No</th>
                            <th>Judul</th>
                            <th>URL</th>
                            <th>Status</th>
                            <th>Created</th>
                            <th width="<?= ($role === 'Administrator') ? '170' : '100' ?>">Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</section>

<?php include_once('modal_petunjuk_menu.php') ?>