<!-- Finish Message -->
<div>
  <div class="card">
    <div class="card-body">
      <?php
      if (!is_null($denied_dom)) {
        echo $denied_dom;
      } else {
        $content = $post->konten;
        $content = str_replace('{base_url}', base_url(), $content);

        echo $content;
      };
      ?>
    </div>
  </div>
</div>