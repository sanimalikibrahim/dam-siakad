<!-- Agenda / Timeline PSB -->
<div class="card">
  <div class="card-body" style="background-color: hsl(205, 38%, 93.45%);">
    <section class="cd-timeline js-cd-timeline">
      <div class="container max-width-lg cd-timeline__container">

        <?php if (count($timeline) > 0) : ?>
          <?php foreach ($timeline as $key => $item) : ?>
            <div class="cd-timeline__block">
              <div class="cd-timeline__img cd-timeline__img--picture" style="background: <?= $item->icon_bg_color ?> !important;">
                <i class="<?= $item->icon ?> text-white" style="font-size: 1.5rem;"></i>
              </div> <!-- cd-timeline__img -->
              <div class="cd-timeline__content text-component">
                <h2 class="mb-2"><?= $item->judul ?></h2>
                <p class="color-contrast-medium">
                  <?= (strlen(strip_tags($item->deskripsi)) > 250) ? substr(strip_tags($item->deskripsi), 0, 250) . '...' : strip_tags($item->deskripsi) ?>
                </p>
                <div class="timeline-detail-item-<?= $key ?>" style="display: none;">
                  <h5 class="mb-3"><?= $item->judul ?></h5>
                  <?= $item->deskripsi ?>
                </div>
                <div class="flex justify-between items-center">
                  <span class="cd-timeline__date">
                    <?= ($item->tanggal_mulai !== $item->tanggal_selesai) ? $item->tanggal_mulai . '<br/>s/d<br/>' . $item->tanggal_selesai : $item->tanggal_mulai ?>
                  </span>
                  <a href="#" class="btn btn-light btn-action-detail <?= ($app->is_mobile) ? 'btn-block mt-2' : '' ?>" data-key="<?= $key ?>">Read more</a>
                </div>
              </div> <!-- cd-timeline__content -->
            </div> <!-- cd-timeline__block -->
          <?php endforeach ?>
        <?php endif ?>

      </div>
    </section> <!-- cd-timeline -->
  </div>
</div>

<!-- Modal detail -->
<div class="modal fade" id="modal-detail">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="spinner">
        <div class="lds-hourglass"></div>
      </div>
      <div class="modal-body">
        <!-- Content in here -->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>