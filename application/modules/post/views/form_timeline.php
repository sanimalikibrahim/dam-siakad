<div class="modal fade" id="modal-form-timeline" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title pull-left">Timeline</h5>
      </div>
      <div class="spinner">
        <div class="lds-hourglass"></div>
      </div>
      <div class="modal-body">
        <form id="form-timeline" autocomplete="off">
          <!-- CSRF -->
          <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />

          <div class="form-group">
            <label required>Judul</label>
            <input type="text" name="judul" class="form-control timeline-judul" maxlength="200" placeholder="Judul" required />
            <i class="form-group__bar"></i>
          </div>

          <div class="form-group">
            <label required>URL</label>
            <div class="position-relative">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    post/t/read/
                  </span>
                </div>
                <input type="text" name="url" class="form-control mask-slug pr-0 timeline-url" maxlength="100" placeholder="URL" required />
                <i class="form-group__bar"></i>
              </div>
            </div>
            <small class="form-text text-muted">
              Use alpha-numeric or with dash.
            </small>
          </div>

          <div class="form-group">
            <label required>Status</label>
            <div class="select">
              <select name="is_active" class="form-control timeline-is_active" data-placeholder="Select &#8595;" required>
                <option disabled selected>Select &#8595;</option>
                <option value="1">Aktif</option>
                <option value="0">Nonaktif</option>
              </select>
              <i class="form-group__bar"></i>
            </div>
          </div>

          <small class="form-text text-muted">
            Fields with red stars (<label required></label>) are required.
          </small>

        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success btn--icon-text timeline-action-save">
          <i class="zmdi zmdi-save"></i> Save
        </button>
        <button type="button" class="btn btn-light btn--icon-text timeline-action-cancel" data-dismiss="modal">
          Cancel
        </button>
      </div>
    </div>
  </div>
</div>