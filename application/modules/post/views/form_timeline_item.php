<div class="modal fade" id="modal-form-timeline_item" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-dialog-centered modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title pull-left">Timeline Item</h5>
      </div>
      <div class="spinner">
        <div class="lds-hourglass"></div>
      </div>
      <div class="modal-body">
        <form id="form-timeline_item" autocomplete="off">
          <!-- CSRF -->
          <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
          <input type="hidden" name="timeline_id" class="timeline_item-timeline_id" readonly />

          <div class="row">
            <div class="col-md-6 col-12">
              <div class="form-group">
                <label required>Tanggal Mulai</label>
                <input type="text" name="tanggal_mulai" class="form-control flatpickr-date timeline_item-tanggal_mulai" placeholder="Tanggal Mulai" required />
                <i class="form-group__bar"></i>
              </div>
            </div>
            <div class="col-md-6 col-12">
              <div class="form-group">
                <label required>Tanggal Selesai</label>
                <input type="text" name="tanggal_selesai" class="form-control flatpickr-date timeline_item-tanggal_selesai" placeholder="Tanggal Selesai" required />
                <i class="form-group__bar"></i>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-6 col-12">
              <div class="form-group">
                <label>Icon</label>
                <div class="position-relative">
                  <input type="text" name="icon" class="form-control timeline_item-icon" maxlength="50" placeholder="zmdi zmdi-calendar">
                  <i class="form-group__bar"></i>
                </div>
                <small class="form-text text-muted">
                  See more icon from <a href="https://zavoloklom.github.io/material-design-iconic-font/icons.html" target="_blank">Material Design Iconic Font</a>
                </small>
              </div>
            </div>
            <div class="col-md-6 col-12">
              <div class="form-group">
                <label>Icon Background Color</label>
                <input type="text" name="icon_bg_color" class="form-control timeline_item-icon_bg_color" maxlength="30" placeholder="#75cd65" />
                <i class="form-group__bar"></i>
              </div>
            </div>
          </div>

          <div class="form-group">
            <label required>Judul</label>
            <input type="text" name="judul" class="form-control timeline_item-judul" maxlength="200" placeholder="Judul" required />
            <i class="form-group__bar"></i>
          </div>

          <div class="form-group">
            <label required>Deskripsi</label>
            <textarea name="deskripsi" class="form-control tinymce-init timeline_item-deskripsi" placeholder="Deskripsi" data-height="300" required></textarea>
            <i class="form-group__bar"></i>
          </div>

          <small class="form-text text-muted">
            Fields with red stars (<label required></label>) are required.
          </small>

        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success btn--icon-text timeline_item-action-save">
          <i class="zmdi zmdi-save"></i> Save
        </button>
        <button type="button" class="btn btn-light btn--icon-text timeline_item-action-cancel" data-dismiss="modal">
          Cancel
        </button>
      </div>
    </div>
  </div>
</div>