<div class="modal fade" id="modal-form-static" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-dialog-centered modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title pull-left">Post Static</h5>
      </div>
      <div class="spinner">
        <div class="lds-hourglass"></div>
      </div>
      <div class="modal-body">
        <form id="form-static" autocomplete="off">
          <!-- CSRF -->
          <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />

          <div class="form-group">
            <label required>Judul</label>
            <input type="text" name="judul" class="form-control static-judul" maxlength="200" placeholder="Judul" required />
            <i class="form-group__bar"></i>
          </div>

          <div class="form-group mb-2">
            <label required>Konten</label>
            <textarea name="konten" class="form-control tinymce-init static-konten" placeholder="Konten" data-height="300" required></textarea>
            <i class="form-group__bar"></i>
          </div>
          <div class="mb-4" style="border: 1px solid #eceff1; padding: 8px 10px;">
            Parameter :
            <table class="table table-sm table-bordered mt-3 mb-0">
              <tr>
                <td>
                  <b class="text-primary">{base_url}</b>
                  <p class="m-0">
                    Digunakan untuk menampilkan base url (<?= base_url() ?>) pada url link.<br />
                    <small class="text-muted">
                      Contoh: {base_url}/post/test <br />
                      Jika link tersebut diklik, akan arahkan ke : <?= base_url('post/test') ?>
                    </small>
                  </p>
                </td>
              </tr>
            </table>
          </div>

          <div class="form-group">
            <label required>URL</label>
            <div class="position-relative">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    post/s/read/
                  </span>
                </div>
                <input type="text" name="url" class="form-control mask-slug pr-0 static-url" maxlength="100" placeholder="URL" required />
                <i class="form-group__bar"></i>
              </div>
            </div>
            <small class="form-text text-muted">
              Use alpha-numeric or with dash.
            </small>
          </div>

          <div class="form-group">
            <label required>Status</label>
            <div class="select">
              <select name="status" class="form-control static-status" data-placeholder="Select &#8595;" required>
                <option disabled selected>Select &#8595;</option>
                <option value="1">Aktif</option>
                <option value="0">Nonaktif</option>
              </select>
              <i class="form-group__bar"></i>
            </div>
          </div>

          <small class="form-text text-muted">
            Fields with red stars (<label required></label>) are required.
          </small>

        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success btn--icon-text static-action-save">
          <i class="zmdi zmdi-save"></i> Save
        </button>
        <button type="button" class="btn btn-light btn--icon-text static-action-cancel" data-dismiss="modal">
          Cancel
        </button>
      </div>
    </div>
  </div>
</div>