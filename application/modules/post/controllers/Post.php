<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . 'controllers/AppBackend.php');

class Post extends AppBackend
{
  function __construct()
  {
    parent::__construct();
    $this->load->model([
      'AppModel',
      'PostModel',
      'TimelineModel',
      'TimelineitemModel',
      'PsbModel'
    ]);
    $this->load->library('form_validation');
  }

  public function index()
  {
    show_404();
  }

  //------- Static

  public function detail($id = null)
  {
    if (!is_null($id)) {
      $post = $this->PostModel->getDetail(['id' => $id, 'status' => 1]);

      if (!is_null($post)) {
        redirect(base_url('post/s/read/' . $post->url));
      } else {
        show_404();
      };
    } else {
      show_404();
    };
  }

  public function static_read($url = null)
  {
    if (!is_null($url)) {
      $role = $this->session->userdata('user')['role'];
      $userId = $this->session->userdata('user')['id'];
      $psbStatus = null;
      $deniedDom = null;
      $postConditional = (in_array($role, ['Administrator', 'Panitia PSB'])) ? ['url' => $url] : ['url' => $url, 'status' => 1];
      $post = $this->PostModel->getDetail($postConditional);

      if (!is_null($post)) {
        if ($role === 'Calon Santri' && in_array((int) $post->id, [1, 4])) {
          $calonSantri = $this->PsbModel->getDetail(['id' => $userId]);
          $psbStatus = $this->PsbModel->getStatus((int) $calonSantri->status);

          if ((int) $calonSantri->status !== 3 && (int) $post->id === 1) {
            $deniedDom = '
              <span style="font-size: 12pt; font-weight: 500;">Maaf!</span>
              <p class="mt-1">
                Anda hanya dapat melihat informasi ini setelah / sudah dinyatakan
                <strong>Lulus</strong>,
                status Anda saat ini
                <strong>' . $psbStatus . '</strong>.
              </p>
              <hr />
              <i>Terima kasih</i>
            ';
          } else if ((int) $calonSantri->status === 0 && (int) $post->id === 4) {
            $deniedDom = '
              <span style="font-size: 12pt; font-weight: 500;">Maaf!</span>
              <p class="mt-1 mb-0">
                Anda hanya dapat melihat informasi ini setelah / sudah melakukan <strong>Pembayaran Administrasi Pendaftaran</strong>,
                status Anda saat ini
                <strong>' . $psbStatus . '</strong>.
              </p>
              <p class="mt-1">
                <a href="' . base_url('post/detail/3') . '">Klik disini</a> untuk melihat petunjuk pembayaran.
              </p>
              <hr />
              <i>Terima kasih</i>
            ';
          };
        };

        $data = array(
          'app' => $this->app(),
          'main_js' => $this->load_main_js('post'),
          'page_title' => $post->judul,
          'post' => $post,
          'psb_status' => $psbStatus,
          'denied_dom' => $deniedDom
        );
        $this->template->set('title', $data['page_title'] . ' | ' . $data['app']->app_name, TRUE);
        $this->template->load_view('read_static', $data, TRUE);
        $this->template->render();
      } else {
        show_404();
      };
    } else {
      show_404();
    };
  }

  public function static()
  {
    $data = array(
      'app' => $this->app(),
      'main_js' => $this->load_main_js('post'),
      'card_title' => 'Post › Static',
      'role' => $this->session->userdata('user')['role']
    );
    $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
    $this->template->load_view('index_static', $data, TRUE);
    $this->template->render();
  }

  public function ajax_get_all_static()
  {
    $this->handle_ajax_request();
    $role = $this->session->userdata('user')['role'];

    switch ($role) {
      case 'Administrator':
        $conditionalInKey = null;
        $conditionalInValue = [];
        break;
      case 'Panitia PSB':
        $conditionalInKey = 'id';
        $conditionalInValue = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13];
        break;
      default:
        $conditionalInKey = 'created_by';
        $conditionalInValue = [$this->session->userdata('user')['id']];
        break;
    };

    $dtAjax_config = array(
      'table_name' => 'post',
      'order_column' => 4,
      'order_column_dir' => 'desc',
      'static_conditional_in_key' => $conditionalInKey,
      'static_conditional_in' => $conditionalInValue
    );
    $response = $this->AppModel->getData_dtAjax($dtAjax_config);
    echo json_encode($response);
  }

  public function ajax_save_static($id = null)
  {
    $this->handle_ajax_request();
    $this->form_validation->set_rules($this->PostModel->rules($id));

    if ($this->form_validation->run() === true) {
      if (is_null($id)) {
        echo json_encode($this->PostModel->insert());
      } else {
        echo json_encode($this->PostModel->update($id));
      };
    } else {
      $errors = validation_errors('<div>- ', '</div>');
      echo json_encode(array('status' => false, 'data' => $errors));
    };
  }

  public function ajax_delete_static($id)
  {
    $this->handle_ajax_request();
    echo json_encode($this->PostModel->delete($id));
  }

  //------- END ## Static

  //------- Timeline

  public function t_detail($id = null)
  {
    if (!is_null($id)) {
      $post = $this->TimelineModel->getDetail(['id' => $id, 'is_active' => 1]);

      if (!is_null($post)) {
        redirect(base_url('post/t/read/' . $post->url));
      } else {
        show_404();
      };
    } else {
      show_404();
    };
  }

  public function timeline_read($url = null)
  {
    if (!is_null($url)) {
      $post = $this->TimelineModel->getDetail(['url' => $url, 'is_active' => 1]);

      if (!is_null($post)) {
        $timeline = $this->TimelineitemModel->getAll(['timeline_id' => $post->id]);
        $data = array(
          'app' => $this->app(),
          'main_js' => $this->load_main_js('post'),
          'page_title' => $post->judul,
          'post' => $post,
          'timeline' => $timeline
        );
        $this->template->set('title', $data['page_title'] . ' | ' . $data['app']->app_name, TRUE);
        $this->template->load_view('read_timeline', $data, TRUE);
        $this->template->render();
      } else {
        show_404();
      };
    } else {
      show_404();
    };
  }

  public function timeline()
  {
    $data = array(
      'app' => $this->app(),
      'main_js' => $this->load_main_js('post'),
      'card_title' => 'Post › Timeline',
      'role' => $this->session->userdata('user')['role']
    );
    $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
    $this->template->load_view('index_timeline', $data, TRUE);
    $this->template->render();
  }

  public function ajax_get_all_timeline()
  {
    $this->handle_ajax_request();
    $role = $this->session->userdata('user')['role'];

    switch ($role) {
      case 'Administrator':
        $conditionalInKey = null;
        $conditionalInValue = [];
        break;
      case 'Panitia PSB':
        $conditionalInKey = 'id';
        $conditionalInValue = [1];
        break;
      default:
        $conditionalInKey = 'created_by';
        $conditionalInValue = [$this->session->userdata('user')['id']];
        break;
    };

    $dtAjax_config = array(
      'table_name' => 'timeline',
      'order_column' => 4,
      'order_column_dir' => 'desc',
      'static_conditional_in_key' => $conditionalInKey,
      'static_conditional_in' => $conditionalInValue
    );
    $response = $this->AppModel->getData_dtAjax($dtAjax_config);
    echo json_encode($response);
  }

  public function ajax_save_timeline($id = null)
  {
    $this->handle_ajax_request();
    $this->form_validation->set_rules($this->TimelineModel->rules($id));

    if ($this->form_validation->run() === true) {
      if (is_null($id)) {
        echo json_encode($this->TimelineModel->insert());
      } else {
        echo json_encode($this->TimelineModel->update($id));
      };
    } else {
      $errors = validation_errors('<div>- ', '</div>');
      echo json_encode(array('status' => false, 'data' => $errors));
    };
  }

  public function ajax_delete_timeline($id)
  {
    $this->handle_ajax_request();
    echo json_encode($this->TimelineModel->delete($id));
  }

  //------- END ## Timeline

  //------- Timeline Item

  public function timeline_item($timeline_id = null)
  {
    if (!is_null($timeline_id)) {
      $timeline = $this->TimelineModel->getDetail(['id' => $timeline_id]);

      if (!is_null($timeline)) {
        $data = array(
          'app' => $this->app(),
          'main_js' => $this->load_main_js('post'),
          'card_title' => 'Post › Timeline › Item',
          'card_subTitle' => $timeline->judul,
          'timeline' => $timeline
        );
        $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
        $this->template->load_view('index_timeline_item', $data, TRUE);
        $this->template->render();
      } else {
        show_404();
      };
    } else {
      show_404();
    };
  }

  public function ajax_get_all_timeline_item($timeline_id = null)
  {
    $this->handle_ajax_request();
    $dtAjax_config = array(
      'table_name' => 'timeline_item',
      'order_column' => 5,
      'order_column_dir' => 'asc',
      'static_conditional' => ['timeline_id' => $timeline_id]
    );
    $response = $this->AppModel->getData_dtAjax($dtAjax_config);
    echo json_encode($response);
  }

  public function ajax_save_timeline_item($id = null)
  {
    $this->handle_ajax_request();
    $this->form_validation->set_rules($this->TimelineitemModel->rules($id));

    if ($this->form_validation->run() === true) {
      if (is_null($id)) {
        echo json_encode($this->TimelineitemModel->insert());
      } else {
        echo json_encode($this->TimelineitemModel->update($id));
      };
    } else {
      $errors = validation_errors('<div>- ', '</div>');
      echo json_encode(array('status' => false, 'data' => $errors));
    };
  }

  public function ajax_delete_item($id)
  {
    $this->handle_ajax_request();
    echo json_encode($this->TimelineitemModel->delete($id));
  }

  //------- END ## Timeline Item
}
