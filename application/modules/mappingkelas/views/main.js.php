<script type="text/javascript">
  $(document).ready(function() {

    var _key = "";
    var _section = "mappingkelas";
    var _table = "table-mappingkelas";
    var _table_item = "table-mappingkelas-item";
    var _modal = "modal-form-mappingkelas";
    var _form = "form-mappingkelas";
    var _santriSelected = [];
    var _isForceSubmit = false;
    var _isFetchGroup = false;

    // Initialize DataTables: Index
    if ($("#" + _table)[0]) {
      var table_mappingkelas = $("#" + _table).DataTable({
        processing: true,
        serverSide: true,
        ajax: {
          url: "<?php echo base_url('mappingkelas/ajax_get_all/') ?>",
          type: "get"
        },
        columns: [{
            data: null,
            render: function(data, type, row, meta) {
              return meta.row + meta.settings._iDisplayStart + 1;
            }
          },
          {
            data: "kelas_nama",
            render: function(data, type, row, meta) {
              return data + " " + row.sub_kelas_nama;
            }
          },
          {
            data: "learndash_group_name",
          },
          {
            data: "jumlah_santri",
            render: function(data) {
              return data + " Orang";
            }
          },
          {
            data: "status",
            render: function(data) {
              if (data === "Draft") {
                return `<span class="text-red">${data}</span>`;
              } else {
                return data;
              };
            }
          },
          {
            data: null,
            className: "center",
            render: function(data, type, row, meta) {
              var buttons = `
                <div class="action">
                  <a href="javascript:;" class="btn btn-sm btn-light btn-table-action action-edit" data-toggle="modal" data-target="#${_modal}"><i class="zmdi zmdi-edit"></i> Edit</a>&nbsp;
                  <a href="javascript:;" class="btn btn-sm btn-light btn-table-action action-delete"><i class="zmdi zmdi-delete"></i> Delete</a>
                `;

              if (row.status === "Draft") {
                buttons += `&nbsp;<a href="javascript:;" class="btn btn-sm btn-success btn-table-action action-publish"><i class="zmdi zmdi-play"></i> Publish</a>`;
              };

              buttons += `</div>`;
              return buttons;
            },
          }
        ],
        autoWidth: !1,
        responsive: {
          details: {
            renderer: function(api, rowIdx, columns) {
              var hideColumn = [];
              var data = $.map(columns, function(col, i) {
                return ($.inArray(col.columnIndex, hideColumn)) ?
                  '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                  '<td class="dt-details-td">' + col.title + ':' + '</td> ' +
                  '<td class="dt-details-td">' + col.data + '</td>' +
                  '</tr>' :
                  '';
              }).join('');

              return data ? $('<table/>').append(data) : false;
            },
            type: "inline",
            target: 'tr',
          }
        },
        columnDefs: [{
          className: 'desktop',
          targets: [0, 1, 2, 3, 4, 5]
        }, {
          className: 'tablet',
          targets: [0, 1, 2, 3]
        }, {
          className: 'mobile',
          targets: [0, 1]
        }, {
          responsivePriority: 2,
          targets: -1
        }],
        pageLength: 15,
        language: {
          searchPlaceholder: "Search...",
          sProcessing: '<div style="text-align: center;"><div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div></div>'
        },
        sDom: '<"dataTables_ct"><"dataTables__top"fb>rt<"dataTables__bottom"ip><"clear">',
        buttons: [{
          extend: "excelHtml5",
          title: "Export Result"
        }, {
          extend: "print",
          title: "Export Result"
        }],
        initComplete: function(a, b) {
          $(this).closest(".dataTables_wrapper").find(".dataTables__top").prepend(
            '<div class="dataTables_buttons hidden-sm-down actions">' +
            '<span class="actions__item zmdi zmdi-refresh" data-table-action="reload" title="Reload" />' +
            '</div>'
          );
        },
      });

      $(".dataTables_filter input[type=search]").focus(function() {
        $(this).closest(".dataTables_filter").addClass("dataTables_filter--toggled")
      });

      $(".dataTables_filter input[type=search]").blur(function() {
        $(this).closest(".dataTables_filter").removeClass("dataTables_filter--toggled")
      });

      $("body").on("click", "[data-table-action]", function(a) {
        a.preventDefault();
        var b = $(this).data("table-action");
        if ("reload" === b) {
          $("#" + _table).DataTable().ajax.reload(null, false);
        };
      });
    };

    // Initialize DataTables: Item
    if ($("#" + _table_item)[0]) {
      var table_mappingkelas_item = $("#" + _table_item).DataTable();
    };

    // Handle data add
    $("#" + _section).on("click", "button." + _section + "-action-add", function(e) {
      e.preventDefault();
      resetForm();

      if (_isFetchGroup === false) {
        fetchGroupList();
      };

      // Reinitialize select2 kelas
      $("#" + _form + " ." + _section + "-kelas option:not(:first)").prop("disabled", false);
      $("#" + _form + " ." + _section + "-kelas").select2();
    });

    // Handle data edit
    $("#" + _table).on("click", "a.action-edit", function(e) {
      e.preventDefault();
      resetForm();

      var temp = table_mappingkelas.row($(this).closest('tr')).data();
      var kelasSelected = temp.kelas_id + "#" + temp.sub_kelas_id;
      var tempData = [];

      // Set key for update params, important!
      _key = temp.id;

      // Get existing item
      $.ajax({
        type: "get",
        url: "<?php echo base_url('mappingkelas/ajax_get_item/') ?>" + temp.kelas_id + "/" + temp.sub_kelas_id,
        dataType: "json",
        success: function(response) {
          if (response.length > 0) {
            response.map(function(item) {
              addRowToList(item.santri_id, item.nama_lengkap, item.nisn, item.nil, item.asrama);
            });
          };
        }
      });


      $("#" + _form + " ." + _section + "-kelas").val(kelasSelected).trigger("change");
      $("#" + _form + " ." + _section + "-learndash_group_name").val(temp.learndash_group_name).trigger('input');

      if (_isFetchGroup === true) {
        $("#" + _form + " ." + _section + "-learndash_group_id").val(temp.learndash_group_id).trigger('change');
      } else {
        fetchGroupList().then(() => {
          $("#" + _form + " ." + _section + "-learndash_group_id").val(temp.learndash_group_id).trigger('change');
        });
      };

      // Reinitialize select2 kelas
      $("#" + _form + " ." + _section + "-kelas option[value='" + kelasSelected + "']").prop("disabled", false);
      $("#" + _form + " ." + _section + "-kelas option:not(:selected)").prop("disabled", true);
      $("#" + _form + " ." + _section + "-kelas").select2();
    });

    // Handle data submit
    $("#" + _modal + " ." + _section + "-action-save").on("click", function(e) {
      e.preventDefault();
      $.ajax({
        type: "post",
        url: "<?php echo base_url('mappingkelas/ajax_save/') ?>" + _isForceSubmit,
        data: $("#" + _form).serialize(),
        success: function(response) {
          var response = JSON.parse(response);
          if (response.status === true) {
            resetForm();
            $("#" + _modal).modal("hide");
            $("#" + _table).DataTable().ajax.reload(null, false);
            notify(response.data, "success");
          } else {
            if (response.isExists === true) {
              var kelasSelected = $("#" + _form + " ." + _section + "-kelas option:selected").text();

              swal({
                title: `Kelas "${kelasSelected}" already has data, update it?`,
                text: "Once updated, you will not be able to recover this data!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: "Yes",
                cancelButtonText: "No",
                closeOnConfirm: false
              }).then((result) => {
                if (result.value) {
                  _isForceSubmit = true;
                  $("#" + _modal + " ." + _section + "-action-save").click();
                };
              });
            } else {
              notify(response.data, "danger");
            };
          };
        }
      });
    });

    // Handle data delete
    $("#" + _table).on("click", "a.action-delete", function(e) {
      e.preventDefault();
      var temp = table_mappingkelas.row($(this).closest('tr')).data();

      swal({
        title: "Are you sure to delete?",
        text: "Once deleted, you will not be able to recover this data!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false
      }).then((result) => {
        if (result.value) {
          $.ajax({
            type: "delete",
            url: "<?php echo base_url('mappingkelas/ajax_delete/') ?>" + temp.kelas_id + "/" + temp.sub_kelas_id,
            dataType: "json",
            success: function(response) {
              if (response.status) {
                $("#" + _table).DataTable().ajax.reload(null, false);
                notify(response.data, "success");
              } else {
                notify(response.data, "danger");
              };
            }
          });
        };
      });
    });

    // Handle data publish
    $("#" + _table).on("click", "a.action-publish", function(e) {
      e.preventDefault();
      var temp = table_mappingkelas.row($(this).closest('tr')).data();

      swal({
        title: "Are you sure to publish?",
        text: "Once published, you will not be able to recover this data!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false
      }).then((result) => {
        if (result.value) {
          $.ajax({
            type: "get",
            url: "<?php echo base_url('mappingkelas/ajax_publish/') ?>" + temp.kelas_id + "/" + temp.sub_kelas_id,
            dataType: "json",
            success: function(response) {
              if (response.status) {
                $("#" + _table).DataTable().ajax.reload(null, false);
                notify(response.data, "success");
              } else {
                notify(response.data, "danger");
              };
            }
          });
        };
      });
    });

    // Handle form reset
    resetForm = () => {
      _key = "";
      table_mappingkelas_item.clear().draw();
      _santriSelected = [];
      _isForceSubmit = false;

      $("#" + _form).trigger("reset");
      $("#" + _form + " ." + _section + "-kelas").val("-1").trigger("change");
      $("#" + _form + " ." + _section + "-santri").val("-1").trigger("change");
      $("#" + _form + " ." + _section + "-kelas").select2();
      $("#" + _form + " ." + _section + "-kelas").select2("destroy");
      $("#" + _form + " ." + _section + "-learndash_group_id").val(null).trigger("change");
    };

    // Handle add item
    $("#" + _modal + " ." + _section + "-add-item").on("click", function() {
      var santri = $("#" + _modal + " ." + _section + "-santri");
      var santriData = santri.select2().find(":selected").data();
      var santriId = santriData.data.id;
      var santriName = santriData.fullname;
      var santriNisn = santriData.nisn;
      var santriNil = santriData.nil;
      var santriAsrama = santriData.asrama;

      addRowToList(santriId, santriName, santriNisn, santriNil, santriAsrama);
    });

    // Handle remove item
    $("#" + _table_item + " tbody").on("click", "a.maapingkelas-remove-item", function(e) {
      e.preventDefault();
      var temp = table_mappingkelas_item.row($(this).closest('tr')).data();

      table_mappingkelas_item.row($(this).parents('tr')).remove().draw();
      _santriSelected = $.grep(_santriSelected, function(value) {
        return value != temp[0].toString();
      });
    });

    // Handle learndash ID select
    $("." + _section + "-learndash_group_id").on("select2:select", function(e) {
      var data = $("." + _section + "-learndash_group_id").select2('data');
      $("." + _section + "-learndash_group_name").val(data[0].text);
    });

    // Handle add item to grid
    function addRowToList(santriId, santriName, santriNisn, santriNil, santriAsrama) {
      if (santriId !== undefined && santriId !== null && santriId !== "-1") {
        if ($.inArray(santriNisn.toString(), _santriSelected) === -1) {
          var newRecord = `
          <tr>
            <td>${santriNisn}</td>
            <td>
              ${santriName}
              <input type="hidden" name="santri_id[]" class="mappingkelas-santri_id" value="${santriId}" readonly />
            </td>
            <td>
              <div class="form-group m-0">
                <input type="text" name="nil[]" class="mappingkelas-nil form-control" value="${santriNil}" />
                <i class="form-group__bar"></i>
              </div>
            </td>
            <td>
              <div class="form-group m-0">
                <input type="text" name="asrama[]" class="mappingkelas-asrama form-control" value="${santriAsrama}" />
                <i class="form-group__bar"></i>
              </div>
            </td>
            <td class="text-center">
              <a href="javascript:;" class="btn btn-secondary btn-sm btn-warning maapingkelas-remove-item">
                <i class="zmdi zmdi-delete"></i>
              </a>
            </td>
          </tr>
        `;

          table_mappingkelas_item.row.add($(newRecord)).draw();
          _santriSelected.push(santriNisn.toString());
          $("#" + _form + " ." + _section + "-santri").val("-1").trigger("change");
        } else {
          notify(`Santri "${santriNisn} | ${santriName}" already exist!`, "warning");
        };
      } else {
        notify("Please select the santri first!", "warning");
      };
    };

    // Handle fetch groups
    async function fetchGroupList() {
      await $.ajax({
        type: "get",
        url: "<?php echo base_url('mappingkelas/ajax_get_group/') ?>",
        success: function(response) {
          var response = JSON.parse(response);
          if (response.status === true) {
            var data = response.data;

            if (data.length > 0) {
              data.map((value, index) => {
                var newOption = new Option(value.id_nama, value.id, false, false);
                $("." + _section + "-learndash_group_id").append(newOption).trigger("change");
              });

              $("." + _section + "-learndash_group_id").val(null).trigger("change");
              _isFetchGroup = true;
            };
          } else {
            notify(response.data, "danger");
          };
        }
      });
    };

  });
</script>