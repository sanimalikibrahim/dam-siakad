<div class="modal fade" id="modal-form-mappingkelas" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-dialog-centered modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title pull-left">Mapping Kelas</h5>
      </div>
      <div class="spinner">
        <div class="lds-hourglass"></div>
      </div>
      <div class="modal-body">
        <form id="form-mappingkelas" autocomplete="off">
          <!-- CSRF -->
          <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />

          <div class="row">
            <div class="col-sm-10 col-md-9 col-lg-10">
              <div class="row">
                <div class="col-sm-5 col-md-5 col-lg-4">
                  <div class="form-group">
                    <label required>Kelas</label>
                    <div class="select">
                      <select name="kelas" class="form-control select2 mappingkelas-kelas" data-placeholder="Select &#8595;" style="width: 100%;" required>
                        <?= $list_kelas ?>
                      </select>
                      <i class="form-group__bar"></i>
                    </div>
                  </div>
                </div>
                <div class="col-sm-7 col-md-7 col-lg-8">
                  <div class="form-group">
                    <label>Learndash ID</label>
                    <div class="select">
                      <select name="learndash_group_id" class="form-control select2 mappingkelas-learndash_group_id" data-placeholder="Select &#8595;" style="width: 100%;">
                        <!-- ajax load -->
                      </select>
                      <input type="hidden" name="learndash_group_name" class="mappingkelas-learndash_group_name" readonly />
                      <i class="form-group__bar"></i>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label required>Santri</label>
                    <div class="select">
                      <select name="santri" class="form-control select2 mappingkelas-santri" search-placeholder="Search by NISN or Nama Lengkap" required>
                        <?= $list_santri ?>
                      </select>
                      <i class="form-group__bar"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-sm-2 col-md-3 col-lg-2">
              <div class="form-group">
                <label class="d-none d-sm-block">&nbsp;</label>
                <a href="javascript:;" class="btn btn-secondary btn-sm btn-block mappingkelas-add-item <?= (!$app->is_mobile) ? 'pt-4 pb-4' : '' ?>">
                  <i class="zmdi zmdi-plus-circle mr-1"></i>
                  Add to List
                </a>
              </div>
            </div>
          </div>

          <div class="table-responsive">
            <table id="table-mappingkelas-item" class="table table-bordered table-sm">
              <thead>
                <tr>
                  <th>NISN</th>
                  <th>Nama Lengkap</th>
                  <th>Nomor Induk Lokal</th>
                  <th>Asrama</th>
                  <th width="120">Action</th>
                </tr>
              </thead>
              <tbody class="table-mappingkelas-item-lists"></tbody>
            </table>
          </div>

        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success btn--icon-text mappingkelas-action-save">
          <i class="zmdi zmdi-save"></i> Save
        </button>
        <button type="button" class="btn btn-light btn--icon-text mappingkelas-action-cancel" data-dismiss="modal">
          Cancel
        </button>
      </div>
    </div>
  </div>
</div>

<style type="text/css">
  #table-mappingkelas-item_wrapper {
    margin: 0 !important;
  }
</style>