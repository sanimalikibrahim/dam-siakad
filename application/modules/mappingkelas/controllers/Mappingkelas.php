<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . 'controllers/AppBackend.php');
require_once(APPPATH . 'controllers/ApiClient.php');

class Mappingkelas extends AppBackend
{
  function __construct()
  {
    parent::__construct();
    $this->load->model([
      'AppModel',
      'MappingKelasModel',
      'MappingKelasLmsModel',
    ]);
    $this->load->library('form_validation');
  }

  public function index()
  {
    $data = array(
      'app' => $this->app(),
      'main_js' => $this->load_main_js('mappingkelas'),
      'card_title' => 'Mapping Kelas',
      'list_kelas' => $this->init_list_kelas_id_value(),
      'list_santri' => $this->init_list_santri(),
    );
    $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
    $this->template->load_view('index', $data, TRUE);
    $this->template->render();
  }

  public function ajax_get_all()
  {
    $this->handle_ajax_request();

    $dtAjax_config = array(
      'table_name' => 'view_mapping_kelas_head',
      'order_column' => 1,
      'order_column_dir' => 'asc'
    );
    $response = $this->AppModel->getData_dtAjax($dtAjax_config);
    echo json_encode($response);
  }

  public function ajax_get_item($kelas = null, $subKelas = null)
  {
    $this->handle_ajax_request();

    echo json_encode($this->MappingKelasModel->getAll(array(
      'kelas_id' => $kelas,
      'sub_kelas_id' => $subKelas
    )));
  }

  public function ajax_save($isForce = false)
  {
    $this->handle_ajax_request();

    $isForce = filter_var($isForce, FILTER_VALIDATE_BOOLEAN);
    $kelasId = null;
    $subKelasId = null;
    $kelas = $this->input->post('kelas');
    $listSantri = $this->input->post('santri_id');
    $listNil = $this->input->post('nil');
    $listAsrama = $this->input->post('asrama');

    if (!is_null($kelas)) {
      $kelasSplit = explode('#', $kelas);
      $kelasId = $kelasSplit[0];
      $subKelasId = $kelasSplit[1];
    };

    if (!is_null($kelasId) && !is_null($subKelasId)) {
      if (!is_null($listSantri) && count($listSantri) > 0) {
        $isErrorNil = in_array('', $listNil);
        $isErroAsrama = in_array('', $listAsrama);

        if ($isErrorNil === false && $isErroAsrama === false) {
          $isExists = $this->MappingKelasModel->getAll(array('kelas_id' => $kelasId, 'sub_kelas_id' => $subKelasId));

          if (count($isExists) > 0 && $isForce === false) {
            echo json_encode(array('status' => false, 'data' => 'Kelas already exist!', 'isExists' => true));
          } else {
            $tempData = array();

            foreach ($listSantri as $key => $santriId) {
              $tempData[] = array(
                'kelas_id' => $kelasId,
                'sub_kelas_id' => $subKelasId,
                'santri_id' => $santriId,
                'nil' => $listNil[$key],
                'asrama' => $listAsrama[$key],
                'created_by' => $this->session->userdata('user')['id']
              );
            };

            if ($isForce === true) {
              $this->MappingKelasModel->deleteByKelas($kelasId, $subKelasId);
            };

            $transaction = $this->MappingKelasModel->insertBatch($tempData);

            if ($transaction['status'] === true) {
              $_POST['kelas_id'] = $kelasId;
              $_POST['sub_kelas_id'] = $subKelasId;

              $this->MappingKelasLmsModel->insert();
            };

            echo json_encode($transaction);
          };
        } else {
          echo json_encode(array('status' => false, 'data' => 'Please fill all fields!'));
        };
      } else {
        echo json_encode(array('status' => false, 'data' => 'Please add Santri first!'));
      };
    } else {
      echo json_encode(array('status' => false, 'data' => 'The Kelas field is required.'));
    };
  }

  public function ajax_delete($kelas = null, $subKelas = null)
  {
    $this->handle_ajax_request();
    echo json_encode($this->MappingKelasModel->deleteByKelas($kelas, $subKelas));
  }

  public function ajax_publish($kelas = null, $subKelas = null)
  {
    $this->handle_ajax_request();
    echo json_encode($this->MappingKelasModel->publishByKelas($kelas, $subKelas));
  }

  public function ajax_get_group()
  {
    $this->handle_ajax_request();

    $apiClient = new ApiClient();
    $response = $apiClient->getListGroup(100);

    if ($response['status'] === true) {
      $temp = $response['data'];
      $data = [];

      if (count($temp) > 0) {
        foreach ($temp as $index => $item) {
          $data[] = (object) [
            'id' => $item->id,
            'nama' => $item->title->rendered,
            'id_nama' => $item->id . ' | ' . $item->title->rendered
          ];
        };
      };

      $response = array('status' => true, 'data' => $data);
    } else {
      $response = array('status' => false, 'data' => 'Terjadi kesalahan ketika mengambil data group ke LMS.');
    };

    echo json_encode($response);
  }
}
