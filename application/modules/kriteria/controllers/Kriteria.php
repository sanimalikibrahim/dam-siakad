<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . 'controllers/AppBackend.php');

class Kriteria extends AppBackend
{
  function __construct()
  {
    parent::__construct();
    $this->load->model([
      'AppModel',
      'KriteriaModel'
    ]);
    $this->load->library('form_validation');
  }

  public function index()
  {
    $data = array(
      'app' => $this->app(),
      'main_js' => $this->load_main_js('kriteria'),
      'card_title' => 'Kriteria'
    );
    $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
    $this->template->load_view('index', $data, TRUE);
    $this->template->render();
  }

  public function ajax_get_all()
  {
    $this->handle_ajax_request();
    $dtAjax_config = array(
      'table_name' => 'kriteria',
      'order_column' => 4,
      'order_column_dir' => 'asc'
    );
    $response = $this->AppModel->getData_dtAjax($dtAjax_config);
    echo json_encode($response);
  }

  public function ajax_save($id = null)
  {
    $this->handle_ajax_request();
    $this->form_validation->set_rules($this->KriteriaModel->rules());

    if ($this->form_validation->run() === true) {
      if (is_null($id)) {
        echo json_encode($this->KriteriaModel->insert());
      } else {
        echo json_encode($this->KriteriaModel->update($id));
      };
    } else {
      $errors = validation_errors('<div>- ', '</div>');
      echo json_encode(array('status' => false, 'data' => $errors));
    };
  }

  public function ajax_set_bobot()
  {
    $this->handle_ajax_request();

    $data = $this->input->post('data');
    $hasErrors = [];

    if (!is_null($data)) {
      foreach ($data as $index => $item) {
        $transaction = $this->KriteriaModel->updateBobot($item['id'], $item['bobot']);

        if ($transaction['status'] === false) {
          $hasErrors[] = [
            'id' => $item['id'],
            'bobot' => $item['bobot']
          ];
        };
      };
    };

    if (count($hasErrors) === 0) {
      $response = array('status' => true, 'data' => 'Data has been saved.');
    } else {
      $response = array('status' => false, 'data' => 'Some data failed to save, try again later.', 'error' => $hasErrors);
    };

    echo json_encode($response);
  }

  public function ajax_reset_bobot()
  {
    $this->handle_ajax_request();
    echo json_encode($this->KriteriaModel->resetBobot());
  }

  public function ajax_delete($id)
  {
    $this->handle_ajax_request();
    echo json_encode($this->KriteriaModel->delete($id));
  }
}
