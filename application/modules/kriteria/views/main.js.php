<script type="text/javascript">
  $(document).ready(function() {

    var _key = "";
    var _section = "kriteria";
    var _table = "table-kriteria";
    var _modal = "modal-form-kriteria";
    var _form = "form-kriteria";

    // Initialize DataTables: Index
    if ($("#" + _table)[0]) {
      var table_kriteria = $("#" + _table).DataTable({
        processing: true,
        serverSide: true,
        ajax: {
          url: "<?php echo base_url('kriteria/ajax_get_all/') ?>",
          type: "get"
        },
        columns: [{
            data: null,
            render: function(data, type, row, meta) {
              return meta.row + meta.settings._iDisplayStart + 1;
            }
          },
          {
            data: "nama"
          },
          {
            data: "bobot",
            className: "text-center",
            render: function(data, type, row, meta) {
              let input = '';
              input += '<div class="input-group m-0 p-0">';
              input += '<input type="number" name="kriteria-bobot[]" class="form-control mask-number kriteria-bobot pl-0" data-id="' + row.id + '" maxlength="3" min="0" max="100" placeholder="Input" />';
              input += '<div class="input-group-append"><span class="input-group-text">%</span></div>';
              input += '</div>';
              return input;
            }
          },
          {
            data: "bobot",
            className: "text-center",
            render: function(data, type, row, meta) {
              return (parseFloat(data) * 100) + " %";
            }
          },
          {
            data: "created_at"
          },
          {
            data: "updated_at"
          },
          {
            data: null,
            className: "center",
            defaultContent: '<div class="action">' +
              '<a href="javascript:;" class="btn btn-sm btn-light btn-table-action action-edit" data-toggle="modal" data-target="#' + _modal + '"><i class="zmdi zmdi-edit"></i> Edit</a>&nbsp;' +
              '<a href="javascript:;" class="btn btn-sm btn-light btn-table-action action-delete"><i class="zmdi zmdi-delete"></i> Delete</a>' +
              '</div>'
          }
        ],
        autoWidth: !1,
        responsive: {
          details: {
            renderer: function(api, rowIdx, columns) {
              var hideColumn = [];
              var data = $.map(columns, function(col, i) {
                return ($.inArray(col.columnIndex, hideColumn)) ?
                  '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                  '<td class="dt-details-td">' + col.title + ':' + '</td> ' +
                  '<td class="dt-details-td">' + col.data + '</td>' +
                  '</tr>' :
                  '';
              }).join('');

              return data ? $('<table/>').append(data) : false;
            },
            type: "inline",
            target: 'tr',
          }
        },
        columnDefs: [{
          className: 'desktop',
          targets: [0, 1, 2, 3, 4, 5, 6]
        }, {
          className: 'tablet',
          targets: [0, 1, 2, 3]
        }, {
          className: 'mobile',
          targets: [0, 1]
        }, {
          responsivePriority: 2,
          targets: -1
        }],
        pageLength: 15,
        language: {
          searchPlaceholder: "Search...",
          sProcessing: '<div style="text-align: center;"><div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div></div>'
        },
        sDom: '<"dataTables_ct"><"dataTables__top"fb>rt<"dataTables__bottom"ip><"clear">',
        buttons: [{
          extend: "excelHtml5",
          title: "Export Result"
        }, {
          extend: "print",
          title: "Export Result"
        }],
        initComplete: function(a, b) {
          $(this).closest(".dataTables_wrapper").find(".dataTables__top").prepend(
            '<div class="dataTables_buttons hidden-sm-down actions">' +
            '<span class="actions__item zmdi zmdi-refresh" data-table-action="reload" title="Reload" />' +
            '</div>'
          );
        },
        drawCallback: function() {
          sumDataOnTable();
        }
      });

      $(".dataTables_filter input[type=search]").focus(function() {
        $(this).closest(".dataTables_filter").addClass("dataTables_filter--toggled")
      });

      $(".dataTables_filter input[type=search]").blur(function() {
        $(this).closest(".dataTables_filter").removeClass("dataTables_filter--toggled")
      });

      $("body").on("click", "[data-table-action]", function(a) {
        a.preventDefault();
        var b = $(this).data("table-action");
        if ("reload" === b) {
          $("#" + _table).DataTable().ajax.reload(null, false);
        };
      });
    };

    // Handle data add
    $("#" + _section).on("click", "button." + _section + "-action-add", function(e) {
      e.preventDefault();
      resetForm();

      $("#" + _section + " .kriteria-alert").show();
    });

    // Handle data edit
    $("#" + _table).on("click", "a.action-edit", function(e) {
      e.preventDefault();
      resetForm();
      var temp = table_kriteria.row($(this).closest('tr')).data();

      // Set key for update params, important!
      _key = temp.id;

      $("#" + _form + " ." + _section + "-nama").val(temp.nama).trigger('input');
      $("#" + _section + " .kriteria-alert").hide();
    });

    // Handle data submit
    $("#" + _modal + " ." + _section + "-action-save").on("click", function(e) {
      e.preventDefault();
      $.ajax({
        type: "post",
        url: "<?php echo base_url('kriteria/ajax_save/') ?>" + _key,
        data: $("#" + _form).serialize(),
        success: function(response) {
          var response = JSON.parse(response);
          if (response.status === true) {
            resetForm();
            $("#" + _modal).modal("hide");
            $("#" + _table).DataTable().ajax.reload(null, false);
            notify(response.data, "success");
          } else {
            notify(response.data, "danger");
          };
        }
      });
    });

    // Handle set bobot
    $("#" + _section).on("click", "button." + _section + "-action-set-bobot", function(e) {
      e.preventDefault();

      swal({
        title: "Are you sure to set bobot?",
        text: "Once submitted, you will not be able to recover this data!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false
      }).then((result) => {
        if (result.value) {
          // Collect bobot
          var bobots = [];

          $('input[type="number"].kriteria-bobot').each(function() {
            var id = $(this).attr("data-id");
            var value = $(this).val();
            var data = {
              id: id,
              bobot: value
            };

            bobots.push(data);
          });
          // END ## Collect bobot

          $.ajax({
            type: "post",
            url: "<?php echo base_url('kriteria/ajax_set_bobot') ?>",
            data: {
              "data": bobots,
              "<?php echo $this->security->get_csrf_token_name(); ?>": "<?php echo $this->security->get_csrf_hash(); ?>"
            },
            dataType: "json",
            success: function(response) {
              if (response.status) {
                $("#" + _section + " .kriteria-action-set-bobot").attr("disabled", true);
                $("#" + _section + " .kriteria-bobot-total").html("0");
                $("#" + _table).DataTable().ajax.reload(null, false);
                notify(response.data, "success");
              } else {
                notify(response.data, "danger");
              };
            }
          });
        };
      });
    });

    // Handle reset bobot
    $("#" + _section).on("click", "button." + _section + "-action-reset-bobot", function(e) {
      e.preventDefault();

      swal({
        title: "Are you sure to reset?",
        text: "Once reseted, you will not be able to recover this data!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false
      }).then((result) => {
        if (result.value) {
          $.ajax({
            type: "post",
            url: "<?php echo base_url('kriteria/ajax_reset_bobot/') ?>",
            dataType: "json",
            success: function(response) {
              if (response.status) {
                $("#" + _table).DataTable().ajax.reload(null, false);
                notify(response.data, "success");
              } else {
                notify(response.data, "danger");
              };
            }
          });
        };
      });
    });

    // Handle data delete
    $("#" + _table).on("click", "a.action-delete", function(e) {
      e.preventDefault();
      var temp = table_kriteria.row($(this).closest('tr')).data();

      swal({
        title: "Are you sure to delete?",
        text: "Once deleted, you will not be able to recover this data!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false
      }).then((result) => {
        if (result.value) {
          $.ajax({
            type: "delete",
            url: "<?php echo base_url('kriteria/ajax_delete/') ?>" + temp.id,
            dataType: "json",
            success: function(response) {
              if (response.status) {
                $("#" + _table).DataTable().ajax.reload(null, false);
                notify(response.data, "success");
              } else {
                notify(response.data, "danger");
              };
            }
          });
        };
      });
    });

    // Handle input bobot
    $("#" + _section).on("keyup click", ".kriteria-bobot", function(e) {
      var isEmpty = 0;
      var bobotCount = 0;

      $('input[type="number"].kriteria-bobot').each(function() {
        var value = $(this).val();

        if (value.trim() === "") {
          isEmpty = isEmpty + 1;
        } else {
          bobotCount = bobotCount + parseInt(value);
        };
      });

      if (isEmpty === 0 && bobotCount <= 100) {
        $("#" + _section + " .kriteria-action-set-bobot").removeAttr("disabled");
      } else {
        $("#" + _section + " .kriteria-action-set-bobot").attr("disabled", true);
      };

      if (bobotCount > 100) {
        $("#" + _section + " .kriteria-alert-max").show();
      } else {
        $("#" + _section + " .kriteria-alert-max").hide();
      };

      $("#" + _section + " .kriteria-bobot-total").html(bobotCount);
    });

    // Handle sum on table
    sumDataOnTable = () => {
      table_kriteria.columns(".sum").every(function() {
        if (this.data().length > 0) {
          var total = this.data().reduce(function(a, b) {
            var x = parseFloat(a) || 0;
            var y = parseFloat(b) || 0;

            return x + y;
          });
          total = total * 100;

          $("#" + _section + " .kriteria-bobot-total-now").html(total);
        };
      });
    };

    // Handle form reset
    resetForm = () => {
      _key = "";
      $("#" + _form).trigger("reset");
    };

  });
</script>