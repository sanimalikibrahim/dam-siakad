<style type="text/css">
    .table-action .buttons {
        padding-top: 10px;
        display: flex;
        align-items: center;
    }

    .vertical-line {
        width: 1px;
        height: 30px;
        background: var(--cd-color-2);
    }

    .hidden {
        display: none;
    }
</style>

<section id="kriteria">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title"><?php echo (isset($card_title)) ? $card_title : '' ?></h4>
            <h6 class="card-subtitle"><?php echo (isset($card_subTitle)) ? $card_subTitle : '' ?></h6>

            <div class="table-action">
                <div class="buttons">
                    <button class="btn btn--raised btn-primary btn--icon-text kriteria-action-add" data-toggle="modal" data-target="#modal-form-kriteria">
                        <i class="zmdi zmdi-plus-circle"></i> Add New
                    </button>
                    <div class="vertical-line ml-2 mr-2"></div>
                    <button class="btn btn-success btn--icon-text kriteria-action-set-bobot mr-2" disabled>
                        Set Bobot
                    </button>
                    <button class="btn btn-danger btn--icon-text kriteria-action-reset-bobot">
                        Reset Bobot
                    </button>
                </div>
            </div>

            <?php include_once('form.php') ?>

            <div class="alert alert-danger kriteria-alert-max mt-4 mb-0 hidden">
                <i class="zmdi zmdi-alert-circle"></i>
                Total nilai bobot tidak boleh lebih dari 100%
            </div>

            <div class="table-responsive">
                <table id="table-kriteria" class="table table-bordered">
                    <thead class="thead-default">
                        <tr>
                            <th width="100">No</th>
                            <th>Nama</th>
                            <th width="220">Bobot</th>
                            <th width="170" class="sum">Bobot Saat Ini</th>
                            <th width="170">Created</th>
                            <th width="170">Updated</th>
                            <th width="170">Action</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th colspan="2"></th>
                            <th><span class="kriteria-bobot-total">0</span> %</th>
                            <th><span class="kriteria-bobot-total-now">0</span> %</th>
                            <th colspan="3"></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</section>