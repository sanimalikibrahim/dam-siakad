<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . 'controllers/AppBackend.php');

class Bukudigital extends AppBackend
{
  private $_list_per_page = 16;

  function __construct()
  {
    parent::__construct();
    $this->load->model([
      'AppModel',
      'BukukategoriModel',
      'BukuModel',
      'DocumentModel'
    ]);
  }

  public function index()
  {
    $data = array(
      'app' => $this->app(),
      'main_js' => $this->load_main_js('bukudigital'),
      'page_title' => 'Buku Digital',
      'data_kategori_buku' => $this->BukukategoriModel->getAll([], 'nama', 'asc')
    );
    $this->template->set('title', $data['page_title'] . ' | ' . $data['app']->app_name, TRUE);
    $this->template->load_view('index', $data, TRUE);
    $this->template->render();
  }

  public function book($kategori_id = null)
  {
    $kategori = $this->BukukategoriModel->getDetail(['id' => $kategori_id]);

    if (!empty($kategori) && !is_null($kategori)) {
      $data_buku = $this->BukuModel->getAll_list(['jenis' => 'Digital', 'status' => 'Publish', 'buku_kategori_id' => $kategori_id], $this->_list_per_page, 0);
      $data_buku_count = $this->BukuModel->getAll_list_count(['jenis' => 'Digital', 'status' => 'Publish', 'buku_kategori_id' => $kategori_id]);
      $data = array(
        'app' => $this->app(),
        'main_js' => $this->load_main_js('bukudigital'),
        'page_title' => 'Buku Digital › ' . $kategori->nama,
        'kategori_id' => $kategori_id,
        'data_buku' => $data_buku,
        'data_buku_count' => $data_buku_count
      );
      $this->template->set('title', $data['page_title'] . ' | ' . $data['app']->app_name, TRUE);
      $this->template->load_view('book', $data, TRUE);
      $this->template->render();
    } else {
      show_404();
    };
  }

  public function view($id = null)
  {
    $buku = $this->BukuModel->getDetail(['id' => $id, 'jenis' => 'Digital', 'status' => 'Publish']);

    if (!empty($buku) && !is_null($buku)) {
      $data_document = $this->DocumentModel->getAll_list(['ref' => 'bukudigital', 'ref_id' => $id], $this->_list_per_page, 0);
      $data_document_count = $this->DocumentModel->getAll_list_count(['ref' => 'bukudigital', 'ref_id' => $id]);
      $data = array(
        'app' => $this->app(),
        'main_js' => $this->load_main_js('bukudigital'),
        'page_title' => 'Buku Digital › ' . $buku->kategori_nama,
        'data_buku' => $buku,
        'data_document' => $data_document,
        'data_document_count' => $data_document_count
      );
      $this->template->set('title', $data['page_title'] . ' | ' . $data['app']->app_name, TRUE);
      $this->template->load_view('detail', $data, TRUE);
      $this->template->render();
    } else {
      show_404();
    };
  }

  public function ajax_load_more_book($kategori_id = null, $offset = null)
  {
    $this->handle_ajax_request();
    $app = $this->app();

    if ((!is_null($kategori_id) && !empty($kategori_id)) && !is_null($offset) && !empty($offset)) {
      $data_buku = $this->BukuModel->getAll_list(['jenis' => 'Digital', 'status' => 'Publish', 'buku_kategori_id' => $kategori_id], $this->_list_per_page, $offset);
      $strlenMax = ($app->is_mobile) ? 40 : 55;

      // Response DOM
      $dom = '';
      if (count($data_buku) > 0) {
        foreach ($data_buku as $key => $item) {
          $item_title = (strlen($item->judul) > $strlenMax) ? substr($item->judul, 0, $strlenMax) . '...' : $item->judul;
          $item_desc_book = '';

          if (!$app->is_mobile) {
            $item_desc_book_pengarang = (strlen($item->pengarang) > 32) ? substr($item->pengarang, 0, 32) . '...' : $item->pengarang;
            $item_desc_book_penerbit = (strlen($item->penerbit) > 35) ? substr($item->penerbit, 0, 35) . '...' : $item->penerbit;
            $item_desc_book = '
              <p>
                  <span>Pengarang: ' . $item_desc_book_pengarang . '</span> <br />
                  <span>Penerbit: ' . $item_desc_book_penerbit . '</span> <br />
                  <span>Tahun Terbit: ' . $item->tahun_terbit . '</span>
              </p>
            ';
          };

          $dom .= '
            <div class="col-md-6">
              <a href="' . base_url('bukudigital/view/' . $item->id . '?ref=' . md5($item->id . date('YmdHis'))) . '" title="' . $item->judul . '">
                <div class="card item-link">
                  <div class="card-body item-body">
                    <div class="item-cover">
                      <img src="' . base_url($item->cover_img) . '" />
                    </div>
                    <div class="item-desc">
                      <p class="item-desc-title">' . $item_title . '</p>
                      ' . $item_desc_book . '
                    </div>
                  </div>
                </div>
              </a>
            </div>
          ';
        };
      };

      echo json_encode(['status' => true, 'data' => $dom, 'count' => count($data_buku)]);
    } else {
      echo json_encode(['status' => false, 'data' => 'Failed to fetch more data.', 'count' => 0]);
    };
  }

  public function ajax_load_more_doc($id = null, $offset = null)
  {
    $this->handle_ajax_request();
    $app = $this->app();

    if ((!is_null($id) && !empty($id)) && !is_null($offset) && !empty($offset)) {
      $data_document = $this->DocumentModel->getAll_list(['ref' => 'bukudigital', 'ref_id' => $id], $this->_list_per_page, $offset);

      // Response DOM
      $dom = '';
      if (count($data_document) > 0) {
        foreach ($data_document as $key => $item) {
          $isNewTab = (!$app->is_mobile) ? 'target="_blank"' : '';
          $dom .= '
            <div class="item-doc">
              <i class="zmdi zmdi-collection-pdf item-doc-icon"></i>
              <div class="item-doc-desc">
                <a href="' . base_url($item->file_name) . '" ' . $isNewTab . '>
                  ' . $item->description . '
                </a>
                <br />
                <small>' . $item->file_size . ' KB</small>
              </div>
            </div>
          ';
        };
      };

      echo json_encode(['status' => true, 'data' => $dom, 'count' => count($data_document)]);
    } else {
      echo json_encode(['status' => false, 'data' => 'Failed to fetch more data.', 'count' => 0]);
    };
  }
}
