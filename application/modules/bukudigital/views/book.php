<style type="text/css">
    .item-link {
        transition: 0.2s;
        margin-bottom: <?= ($app->is_mobile) ? '1rem' : '2.3rem' ?>;
    }

    .item-link:hover {
        box-shadow: 0 1px 15px rgba(0, 0, 0, .075);
        transition: 0.2s;
    }

    .item-body {
        color: #747a80;
        padding: 0;
        display: flex;
        align-items: <?= ($app->is_mobile) ? 'center' : 'unset' ?>;
    }

    .item-body .item-icon {
        font-size: <?= $app->is_mobile ? '25pt' : '30pt' ?>;
        margin-right: 1rem;
    }

    .item-body .item-cover {
        min-width: <?= ($app->is_mobile) ? '64px' : '130px' ?>;
        max-width: <?= ($app->is_mobile) ? '64px' : '130px' ?>;
        height: <?= ($app->is_mobile) ? '64px' : '150px' ?>;
        background-color: #fff;
    }

    .item-body .item-cover img {
        width: 100%;
        height: 100%;
        object-fit: cover;
    }

    .item-body .item-desc {
        padding: <?= ($app->is_mobile) ? '10px' : '15px' ?>;
        font-size: 10pt;
        font-weight: 300;
    }

    .item-body .item-desc .item-desc-title {
        font-size: 10.5pt;
        font-weight: <?= ($app->is_mobile) ? '300' : '500' ?>;
        margin-bottom: <?= ($app->is_mobile) ? '0' : '6px' ?>;
    }

    .item-info {
        text-align: center;
        padding: 10rem 0rem;
    }

    .item-info p {
        line-height: .75rem;
        font-size: 1.075rem;
        color: #999;
        font-weight: 400;
    }

    .item-info .zmdi {
        font-size: 3.5rem;
        margin-bottom: 1rem;
        color: #c5c5c5;
    }
</style>

<?php $strlenMax = ($app->is_mobile) ? 40 : 55 ?>

<input type="hidden" class="kategori-id" value="<?= $kategori_id ?>" readonly />

<div id="bukudigital" class="data-lists row">
    <?php if (count($data_buku) > 0) : ?>
        <?php foreach ($data_buku as $index => $item) : ?>
            <div class="col-md-6">
                <a href="<?= base_url('bukudigital/view/' . $item->id . '?ref=' . md5($item->id . date('YmdHis'))) ?>" title="<?= $item->judul ?>">
                    <div class="card item-link">
                        <div class="card-body item-body">
                            <div class="item-cover">
                                <img src="<?= base_url($item->cover_img) ?>" />
                            </div>
                            <div class="item-desc">
                                <p class="item-desc-title"><?= (strlen($item->judul) > $strlenMax) ? substr($item->judul, 0, $strlenMax) . '...' : $item->judul ?></p>
                                <?php if (!$app->is_mobile) : ?>
                                    <p>
                                        <span>Pengarang: <?= (strlen($item->pengarang) > 32) ? substr($item->pengarang, 0, 32) . '...' : $item->pengarang ?></span> <br />
                                        <span>Penerbit: <?= (strlen($item->penerbit) > 35) ? substr($item->penerbit, 0, 35) . '...' : $item->penerbit ?></span> <br />
                                        <span>Tahun Terbit: <?= $item->tahun_terbit ?></span>
                                    </p>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        <?php endforeach; ?>
    <?php else : ?>
        <div class="col">
            <div class="card">
                <div class="card-body">
                    <div class="item-info item-no-data">
                        <i class="zmdi zmdi-folder-outline"></i>
                        <p>Tidak ditemukan data</p>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
</div>

<?php if ($data_buku_count > 16) : ?>
    <div class="col-md-12 loadmore-data-wrap text-center" style="margin-top: 1rem; margin-bottom: 1rem;">
        <a href="javascript:;" id="loadmore-book" class="btn btn-success spinner-action-button" style="padding-left: 30px; padding-right: 30px;">Load More</a>
    </div>
<?php endif; ?>