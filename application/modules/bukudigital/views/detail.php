<style type="text/css">
    .item-body {
        color: #747a80;
        display: flex;
        padding: 15px;
    }

    .item-body .item-icon {
        font-size: <?= $app->is_mobile ? '25pt' : '30pt' ?>;
        margin-right: 1rem;
    }

    .item-body .item-cover {
        min-width: <?= ($app->is_mobile) ? '64px' : '100px' ?>;
        max-width: <?= ($app->is_mobile) ? '64px' : '100px' ?>;
        height: <?= ($app->is_mobile) ? '64px' : '120px' ?>;
        background-color: #fff;
    }

    .item-body .item-cover img {
        width: 100%;
        height: 100%;
        object-fit: cover;
    }

    .item-body .item-desc {
        padding: <?= ($app->is_mobile) ? '10px' : '15px' ?>;
        padding-top: 0;
        font-size: 10pt;
        font-weight: 300;
    }

    .item-body .item-desc .item-desc-title {
        font-size: 10.5pt;
        font-weight: 500;
        margin-bottom: 6px;
    }

    .item-body-doc {
        color: #747a80;
        padding: 15px;
    }

    .item-body-doc .item-doc {
        display: flex;
        align-items: flex-start;
        margin-bottom: 0.5rem;
        padding-bottom: 0.5rem;
        border-bottom: 1px solid #f2f2f2;
    }

    .item-body-doc .item-doc .item-doc-icon {
        margin-top: 4px;
        font-size: 15pt;
    }

    .item-body-doc .item-doc .item-doc-desc {
        padding-left: 10px;
    }

    .item-body-doc .item-doc .item-doc-desc a {
        color: #747a80;
        font-weight: 500;
    }

    .item-info {
        text-align: center;
        padding: 10rem 0rem;
    }

    .item-info p {
        line-height: .75rem;
        font-size: 1.075rem;
        color: #999;
        font-weight: 400;
    }

    .item-info .zmdi {
        font-size: 3.5rem;
        margin-bottom: 1rem;
        color: #c5c5c5;
    }
</style>


<div id="bukudigital" class="row">
    <?php if (!is_null($data_buku)) : ?>
        <div class="col-md-12">
            <div class="card" style="margin-bottom: 1rem;">
                <div class="card-body item-body">
                    <input type="hidden" class="buku-id" value="<?= $data_buku->id ?>" readonly />
                    <div class="item-cover">
                        <img class="img img-thumbnail" src="<?= base_url($data_buku->cover_img) ?>" />
                    </div>
                    <div class="item-desc">
                        <p class="item-desc-title"><?= $data_buku->judul ?></p>
                        <p>
                            <span>Pengarang: <?= $data_buku->pengarang ?></span> <br />
                            <span>Penerbit: <?= $data_buku->penerbit ?></span> <br />
                            <span>Tahun Terbit: <?= $data_buku->tahun_terbit ?></span>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <?php if (count($data_document) > 0) : ?>
                <div class="card">
                    <div id="data-lists" class="card-body item-body-doc">
                        <?php foreach ($data_document as $index => $item) : ?>
                            <div class="item-doc">
                                <i class="zmdi zmdi-collection-pdf item-doc-icon"></i>
                                <div class="item-doc-desc">
                                    <a href="<?= base_url($item->file_name) ?>" <?= (!$app->is_mobile) ? 'target="_blank"' : '' ?>>
                                        <?= $item->description ?>
                                    </a>
                                    <br />
                                    <small><?= $item->file_size ?> KB</small>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
                <?php if ($data_document_count > 16) : ?>
                    <div class="col-md-12 loadmore-data-wrap text-center" style="margin-top: 1rem; margin-bottom: 1rem;">
                        <a href="javascript:;" id="loadmore" class="btn btn-success spinner-action-button" style="padding-left: 30px; padding-right: 30px;">Load More</a>
                    </div>
                <?php endif; ?>
            <?php else : ?>
                <div class="card">
                    <div class="card-body">
                        <div class="item-info item-no-data">
                            <i class="zmdi zmdi-folder-outline"></i>
                            <p>Tidak ditemukan data</p>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    <?php else : ?>
        <div class="col">
            <div class="card">
                <div class="card-body">
                    <div class="item-info item-no-data">
                        <i class="zmdi zmdi-folder-outline"></i>
                        <p>Tidak ditemukan data</p>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
</div>