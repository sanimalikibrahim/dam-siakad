<style type="text/css">
    .item-link {
        transition: 0.2s;
        margin-bottom: <?= ($app->is_mobile) ? '1rem' : '2.3rem' ?>;
    }

    .item-link:hover {
        box-shadow: 0 1px 15px rgba(0, 0, 0, .075);
        transition: 0.2s;
    }

    .item-body {
        color: #747a80;
        font-size: 10.5pt;
        font-weight: 400;
        padding: <?= $app->is_mobile ? '1.2rem 2.2rem' : '2.2rem' ?>;
        display: flex;
        align-items: center;
        height: <?= $app->is_mobile ? '80px' : '80px' ?>;
    }

    .item-body .item-icon {
        font-size: <?= $app->is_mobile ? '25pt' : '30pt' ?>;
        margin-right: 1rem;
    }

    .item-info {
        text-align: center;
        padding: 10rem 0rem;
    }

    .item-info p {
        line-height: .75rem;
        font-size: 1.075rem;
        color: #999;
        font-weight: 400;
    }

    .item-info .zmdi {
        font-size: 3.5rem;
        margin-bottom: 1rem;
        color: #c5c5c5;
    }
</style>

<div id="bukudigital" class="row">
    <?php if (count($data_kategori_buku) > 0) : ?>
        <?php foreach ($data_kategori_buku as $index => $item) : ?>
            <div class="col-md-3">
                <a href="<?= base_url('bukudigital/' . $item->id . '?ref=' . md5($item->id . date('YmdHis'))) ?>" title="<?= $item->nama ?>">
                    <div class="card item-link">
                        <div class="card-body item-body">
                            <i class="zmdi zmdi-folder item-icon"></i>
                            <span style="flex: 1;"><?= (strlen($item->nama) > 30) ? substr($item->nama, 0, 30) . '...' : $item->nama ?></span>
                            <span class="badge badge-light rounded-circle" style="margin-left: 10px; font-weight: 300;"><?= number_format($item->jumlah_buku_digital) ?></span>
                        </div>
                    </div>
                </a>
            </div>
        <?php endforeach; ?>
    <?php else : ?>
        <div class="col">
            <div class="card">
                <div class="card-body">
                    <div class="item-info item-no-data">
                        <i class="zmdi zmdi-folder-outline"></i>
                        <p>Tidak ditemukan data</p>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
</div>