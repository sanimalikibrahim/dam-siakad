<script type="text/javascript">
  $(document).ready(function() {

    var _offset = 0;
    var _per_page = 16;

    // Handle load more: Book
    $("#loadmore-book").on("click", function() {
      var kategoriId = $(".kategori-id").val();
      _offset = _offset + _per_page;

      $.ajax({
        type: "get",
        url: "<?php echo base_url('bukudigital/ajax_load_more_book/') ?>" + kategoriId + "/" + _offset,
        dataType: "json",
        success: function(response) {
          if (response.status === true) {
            if (response.count > 0) {
              $(".data-lists").append(response.data);
            } else {
              $(".loadmore-data-wrap").hide();
            };
          } else {
            notify(response.data, "danger");
          };
        }
      });
    });

    // Handle load more: Document
    $("#loadmore").on("click", function() {
      var bukuId = $(".buku-id").val();
      _offset = _offset + _per_page;

      $.ajax({
        type: "get",
        url: "<?php echo base_url('bukudigital/ajax_load_more_doc/') ?>" + bukuId + "/" + _offset,
        dataType: "json",
        success: function(response) {
          if (response.status === true) {
            if (response.count > 0) {
              $("#data-lists").append(response.data);
            } else {
              $(".loadmore-data-wrap").hide();
            };
          } else {
            notify(response.data, "danger");
          };
        }
      });
    });

  });
</script>