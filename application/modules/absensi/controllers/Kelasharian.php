<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . 'controllers/AppBackend.php');

class Kelasharian extends AppBackend
{
  function __construct()
  {
    parent::__construct();
    $this->load->model([
      'AppModel',
      'AbsenHarianModel',
      'MataPelajaranModel',
    ]);
    $this->load->library('form_validation');
  }

  public function index()
  {
    $user_id = $this->session->userdata('user')['id'];
    $user_role = $this->session->userdata('user')['role'];

    if ($user_role === 'Guru') {
      $list_kelas = $this->init_list_kelas_by_guru($user_id);
      $list_mapel = $this->init_list_mapel_by_guru($user_id);
    } else {
      $list_kelas = $this->init_list_kelas();
      $list_mapel = $this->init_list_mapel_by_guru(-1);
    };

    $data = array(
      'app' => $this->app(),
      'main_js' => $this->load_main_js('absensi/views/kelasharian/main.js.php', true),
      'card_title' => 'Absensi › Kelas Harian',
      'list_kelas' => $list_kelas,
      'list_mapel' => $list_mapel,
    );
    $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
    $this->template->load_view('kelasharian/index_activity', $data, TRUE);
    $this->template->render();
  }

  public function ajax_get_activity()
  {
    $this->handle_ajax_request();

    $filter_date = $this->input->post('filter_date');
    $filter_kelas = $this->input->post('filter_kelas');
    $filter_mapel = $this->input->post('filter_mapel');
    $kelas = null;
    $sub_kelas = null;

    if (!empty($filter_date) && !empty($filter_kelas) && !empty($filter_mapel)) {
      $filter_kelas_ex = explode('#', $filter_kelas);
      $kelas = (isset($filter_kelas_ex[0])) ? $filter_kelas_ex[0] : null;
      $sub_kelas = (isset($filter_kelas_ex[1])) ? $filter_kelas_ex[1] : null;

      $model = $this->AbsenHarianModel->getAll_withSantri($kelas, $sub_kelas, $filter_date, $filter_mapel);

      if (count($model) > 0) {
        $data = array(
          'app' => $this,
          'is_today' => true,
          'data_santri' => $model,
        );
        $this->load->view('kelasharian/grid_activity', $data);
      } else {
        echo json_encode(['status' => false, 'data' => 'Data santri tidak ditemukan.']);
      };
    } else {
      echo json_encode(['status' => false, 'data' => 'Pastikan Tanggal, Kelas & Mata Pelajaran sudah dipilih.']);
    };
  }

  public function ajax_save_activity()
  {
    $this->handle_ajax_request();

    $filter_date = $this->input->post('filter_date');
    $filter_kelas = $this->input->post('filter_kelas');
    $filter_mapel = $this->input->post('filter_mapel');
    $absenItem = $this->input->post('absen');
    $data = array();

    if (!empty($filter_date) && !empty($filter_kelas) && !empty($filter_mapel)) {
      $date = date('Y-m-d', strtotime($filter_date));

      if (!is_null($absenItem) && count($absenItem) > 0) {
        // Delete existing
        $this->AbsenHarianModel->delete([
          'tanggal' => $date,
          'kelas' => $filter_kelas,
          'mata_pelajaran_id' => $filter_mapel
        ]);

        foreach ($absenItem as $santri_id => $item) {
          // Store data POST
          if (isset($item['status'])) {
            $data[] = array(
              'kelas' => $filter_kelas,
              'santri_id' => $santri_id,
              'mata_pelajaran_id' => $filter_mapel,
              'pengajar_id' => $this->session->userdata('user')['id'],
              'status' => $item['status'],
              'catatan' => $item['catatan'],
              'tanggal' => $date,
              'created_by' => $this->session->userdata('user')['id']
            );
          };
        };

        if (count($data) > 0) {
          // Insert new data
          echo json_encode($this->AbsenHarianModel->insertBatch($data));
        } else {
          echo json_encode(array('status' => false, 'data' => 'No data to submit.'));
        };
      } else {
        echo json_encode(array('status' => false, 'data' => 'No data to submit.'));
      };
    } else {
      echo json_encode(array('status' => false, 'data' => 'Pastikan Tanggal, Kelas & Mata Pelajaran sudah dipilih.'));
    };
  }

  public function ajax_save_activity_item()
  {
    $this->handle_ajax_request();

    $filter_date = $this->input->post('filter_date');
    $filter_kelas = $this->input->post('filter_kelas');
    $filter_mapel = $this->input->post('filter_mapel');
    $santri_id = $this->input->post('santri_id');

    if (!empty($filter_date) && !empty($filter_kelas) && !empty($filter_mapel)) {
      $date = date('Y-m-d', strtotime($filter_date));

      // Delete existing
      $this->AbsenHarianModel->delete([
        'tanggal' => $date,
        'kelas' => $filter_kelas,
        'mata_pelajaran_id' => $filter_mapel,
        'santri_id' => $santri_id,
      ]);

      $payload[] = array(
        'kelas' => $filter_kelas,
        'santri_id' => $santri_id,
        'mata_pelajaran_id' => $filter_mapel,
        'pengajar_id' => $this->session->userdata('user')['id'],
        'status' => 'Hadir',
        'catatan' => '',
        'tanggal' => $date,
        'created_by' => $this->session->userdata('user')['id']
      );

      echo json_encode($this->AbsenHarianModel->insertBatch($payload));
    } else {
      echo json_encode(array('status' => false, 'data' => 'Pastikan Tanggal, Kelas & Mata Pelajaran sudah dipilih.'));
    };
  }

  public function export_harian()
  {
    $tanggal = $this->input->get('tanggal');
    $kelas = $this->input->get('kelas');
    $mapel = $this->input->get('mapel');

    if (!is_null($tanggal) && !is_null($kelas) && !is_null($mapel)) {
      $tanggalRgx = date('Ymd', strtotime($tanggal));
      $tanggalHariName = $this->get_day(date('D', strtotime($tanggal)));
      $tanggalHari = date('d', strtotime($tanggal));
      $tanggalBulanName = $this->get_month(date('m'), strtotime($tanggal));
      $tanggalTahun = date('Y', strtotime($tanggal));
      $tanggalFormated = $tanggalHariName . ', ' . $tanggalHari . ' ' . $tanggalBulanName . ' ' . $tanggalTahun;
      $kelas = str_replace('@', '#', $kelas);
      $kelasClean = str_replace('#', ' ', $kelas);
      $startAttributeRow = 5;
      $startDataRow = 6;
      $fileName = 'absen_harian_kelas-' . $tanggalRgx . '.xlsx';
      $payload = array();

      $fileTemplate = FCPATH . 'directory/templates/template-absen_kelas_harian.xlsx';
      $model = $this->AbsenHarianModel->getAll(['tanggal' => $tanggal, 'kelas' => $kelas, 'mata_pelajaran_id' => $mapel]);
      $mapelData = $this->MataPelajaranModel->getDetail(['id' => $mapel]);

      if (count($model) > 0) {
        // Modified payload
        foreach ($model as $index => $item) {
          $payload[] = (object) array(
            'id' => $item->id,
            'kelas' => $item->kelas,
            'santri_id' => $item->santri_id,
            'nomor_induk_lokal' => $item->nomor_induk_lokal,
            'nisn' => $item->nisn,
            'nama_santri' => $item->nama_santri,
            'mata_pelajaran_id' => $item->mata_pelajaran_id,
            'nama_mapel' => $item->nama_mapel,
            'pengajar_id' => $item->pengajar_id,
            'nama_pengajar' => $item->nama_pengajar,
            'status' => substr(strtoupper($item->status), 0, 1),
            'catatan' => $item->catatan,
            'tanggal' => $item->tanggal,
            'created_at' => $item->created_at,
            'created_by' => $item->created_by
          );
        };

        $inject  = '$a1 = $sheet->getCell("A1")->getFormattedValue();';
        $inject .= '$a3 = $sheet->getCell("A3")->getFormattedValue();';
        $inject .= '$a1 = str_replace(\'${kelas}\', "' . $kelasClean . '", $a1);'; // Kelas
        $inject .= '$a3 = str_replace(\'${tanggal}\', "' . $tanggalFormated . '", $a3);'; // Tanggal
        $inject .= '$a3 = str_replace(\'${mapel}\', "' . $mapelData->nama . '", $a3);'; // Mapel
        $inject .= '$sheet->setCellValue("A3", strtoupper($a3));';
        $inject .= '$sheet->setCellValue("A1", strtoupper($a1));';

        $this->generateExcelByTemplate($fileTemplate, $startAttributeRow, $startDataRow, $payload, $fileName, $inject);
      } else {
        echo 'Tidak ditemukan data.';
      };
    } else {
      echo 'Terjadi kesalahan ketika membuat file.';
    };
  }
}
