<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . 'controllers/AppBackend.php');

class Absensi extends AppBackend
{
  function __construct()
  {
    parent::__construct();
    $this->load->model([
      'AppModel',
      'KelasModel',
      'SubkelasModel',
      'SantriModel',
      'KegiatanModel',
      'KegiatanresultModel'
    ]);
    $this->load->library('form_validation');
  }

  public function index()
  {
    redirect(base_url('absensi/activity'));
  }

  // Kegiatan Harian
  public function activity()
  {
    $user_id = $this->session->userdata('user')['id'];
    $user_role = $this->session->userdata('user')['role'];

    if ($user_role === 'Pembina') {
      $kelas = $this->SantriModel->getKelasByPembina($user_id);
      $sub_kelas = $this->SantriModel->getSubKelasByPembina($user_id);
    } else {
      $kelas = $this->KelasModel->getAll();
      $sub_kelas = $this->SubkelasModel->getAll();
    };

    $data = array(
      'app' => $this->app(),
      'main_js' => $this->load_main_js('absensi'),
      'card_title' => 'Absensi › Kegiatan Harian',
      'data_kelas' => $kelas,
      'data_sub_kelas' => $sub_kelas
    );
    $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
    $this->template->load_view('index_activity', $data, TRUE);
    $this->template->render();
  }

  public function ajax_get_activity()
  {
    $this->handle_ajax_request();

    $filter_date = $this->input->post('filter_date');
    $filter_kelas = $this->input->post('filter_kelas');
    $kelas = null;
    $sub_kelas = null;

    if (!empty($filter_kelas)) {
      $filter_kelas_ex = explode('#', $filter_kelas);
      $kelas = (isset($filter_kelas_ex[0])) ? $filter_kelas_ex[0] : null;
      $sub_kelas = (isset($filter_kelas_ex[1])) ? $filter_kelas_ex[1] : null;
    };

    if (!empty($filter_date) && !empty($filter_kelas)) {
      $santri = $this->SantriModel->getAll(['kelas' => $kelas, 'sub_kelas' => $sub_kelas]);

      if (count($santri) > 0) {
        $kegiatan_slug = ['shalat-wajib', 'shalat-sunnah', 'shaum-sunnah', 'tadarus'];
        $kegiatan = $this->KegiatanModel->getAll_fullItem($filter_date, 0, $kegiatan_slug);
        $kegiatan_santri = $this->KegiatanModel->getAll_fullItemWithSantri($filter_date, $kelas, $sub_kelas, $kegiatan_slug);
        $isToday = ($filter_date === date('Y-m-d')) ? true : false;

        $data = array(
          'app' => $this,
          'data_kegiatan' => $kegiatan,
          'data_kegiatan_santri' => $kegiatan_santri,
          'is_today' => true,
        );
        $this->load->view('grid_activity', $data);
      } else {
        echo json_encode(['status' => false, 'data' => 'Data santri tidak ditemukan.']);
      };
    } else {
      echo json_encode(['status' => false, 'data' => 'Tanggal & Kelas is required.']);
    };
  }

  public function ajax_save_activity()
  {
    $this->handle_ajax_request();

    $date = $this->input->get('date');
    $kegiatan_item = $this->input->post('kegiatan_item');
    $data = [];

    if (!is_null($date)) {
      $date = date('Y-m-d', strtotime($date));
      $dateTime = $date . ' ' . date('H:i:s');

      if (!is_null($kegiatan_item) && count($kegiatan_item) > 0) {
        foreach ($kegiatan_item as $santri_id => $kegiatan_santri) {
          // Delete existing
          $this->KegiatanresultModel->delete(['santri_id' => $santri_id, 'DATE(created_at)' => $date]);

          // Store data POST
          if (!is_null($kegiatan_santri) && count($kegiatan_santri) > 0) {
            foreach ($kegiatan_santri as $key => $item) {
              if (!empty($item)) {
                $data[] = array(
                  'kegiatan_item_id' => $key,
                  'santri_id' => $santri_id,
                  'jawaban' => $item,
                  'created_at' => $dateTime,
                  'created_by' => $santri_id
                );
              };
            };
          };
        };

        if (count($data) > 0) {
          // Insert new data
          echo json_encode($this->KegiatanresultModel->insertBatch($data));
        } else {
          echo json_encode(array('status' => false, 'data' => 'No data to submit.'));
        };
      } else {
        echo json_encode(array('status' => false, 'data' => 'No data to submit.'));
      };
    } else {
      echo json_encode(array('status' => false, 'data' => 'Tanggal tidak diketahui.'));
    };
  }
  // END ## Kegiatan Harian
}
