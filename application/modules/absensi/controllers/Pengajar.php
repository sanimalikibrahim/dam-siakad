<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . 'controllers/AppBackend.php');

class Pengajar extends AppBackend
{
  function __construct()
  {
    parent::__construct();
    $this->load->model([
      'AppModel',
      'AbsenHarianGuruModel',
    ]);
    $this->load->library('form_validation');
  }

  public function index()
  {
    $data = array(
      'app' => $this->app(),
      'main_js' => $this->load_main_js('absensi/views/pengajar/main.js.php', true),
      'card_title' => 'Absensi › Pengajar',
    );
    $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
    $this->template->load_view('pengajar/index_activity', $data, TRUE);
    $this->template->render();
  }

  public function ajax_get_activity()
  {
    $this->handle_ajax_request();
    $filter_date = $this->input->post('filter_date');

    if (!empty($filter_date)) {
      $model = $this->AbsenHarianGuruModel->getAll_withPengajar($filter_date);
      $list_kelas = $this->init_list_kelas(null, null, true);
      $list_mapel = $this->init_list_mapel_by_guru(-1, null, null, true);

      if (count($model) > 0) {
        $data = array(
          'app' => $this,
          'is_today' => true,
          'data_pengajar' => $model,
          'list_kelas' => $list_kelas,
          'list_mapel' => $list_mapel,
        );
        $this->load->view('pengajar/grid_activity', $data);
      } else {
        echo json_encode(['status' => false, 'data' => 'Data pengajar tidak ditemukan.']);
      };
    } else {
      echo json_encode(['status' => false, 'data' => 'Pastikan tanggal sudah dipilih.']);
    };
  }

  public function ajax_save_activity()
  {
    $this->handle_ajax_request();

    $filter_date = $this->input->post('filter_date');
    $absenItem = $this->input->post('absen');
    $payload = array();

    if (!empty($filter_date)) {
      $date = date('Y-m-d', strtotime($filter_date));

      if (!is_null($absenItem) && count($absenItem) > 0) {
        // Delete existing
        $this->AbsenHarianGuruModel->delete(['tanggal' => $date]);

        foreach ($absenItem as $index => $item) {
          // Store data POST
          if (isset($item['status'])) {
            if (isset($item['pengajar_id']) && isset($item['status'])) {
              $payload[] = array(
                'pengajar_id' => $item['pengajar_id'],
                'kelas' => isset($item['kelas']) ? $item['kelas'] : null,
                'mata_pelajaran_id' => isset($item['mata_pelajaran_id']) ? $item['mata_pelajaran_id'] : null,
                'status' => $item['status'],
                'catatan' => $item['catatan'],
                'tanggal' => $date,
                'created_by' => $this->session->userdata('user')['id']
              );
            };
          };
        };

        if (count($payload) > 0) {
          // Insert new data
          echo json_encode($this->AbsenHarianGuruModel->insertBatch($payload));
        } else {
          echo json_encode(array('status' => false, 'data' => 'No data to submit.'));
        };
      } else {
        echo json_encode(array('status' => false, 'data' => 'No data to submit.'));
      };
    } else {
      echo json_encode(array('status' => false, 'data' => 'Pastikan tanggal sudah dipilih.'));
    };
  }

  public function ajax_save_activity_item()
  {
    $this->handle_ajax_request();

    $filter_date = $this->input->post('filter_date');
    $pengajar_id = $this->input->post('pengajar_id');
    $kelas = $this->input->post('kelas');
    $mata_pelajaran_id = $this->input->post('mata_pelajaran_id');

    if (!empty($filter_date) && !is_null($pengajar_id) && !is_null($kelas) && !is_null($mata_pelajaran_id)) {
      $date = date('Y-m-d', strtotime($filter_date));

      // Delete existing
      $this->AbsenHarianGuruModel->delete([
        'tanggal' => $date,
        'pengajar_id' => $pengajar_id,
        'kelas' => $kelas,
        'mata_pelajaran_id' => $mata_pelajaran_id
      ]);

      $payload[] = array(
        'pengajar_id' => $pengajar_id,
        'kelas' => $kelas,
        'mata_pelajaran_id' => $mata_pelajaran_id,
        'status' => 'Hadir',
        'catatan' => '',
        'tanggal' => $date,
        'created_by' => $this->session->userdata('user')['id']
      );

      echo json_encode($this->AbsenHarianGuruModel->insertBatch($payload));
    } else {
      echo json_encode(array('status' => false, 'data' => 'Tanggal atau format field tidak diketahui.'));
    };
  }
}
