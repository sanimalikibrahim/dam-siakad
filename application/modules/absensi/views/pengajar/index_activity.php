<?php include_once('scan_qr.php') ?>

<style type="text/css">
    .filter-result {
        margin-top: 1rem;
    }

    .filter-info {
        text-align: center;
        padding: 10rem 0rem;
    }

    .filter-info p {
        line-height: .75rem;
        font-size: 1.075rem;
        color: #999;
        font-weight: 400;
    }

    .filter-info .zmdi {
        font-size: 3.5rem;
        margin-bottom: 1rem;
        color: #c5c5c5;
    }

    .table td,
    .table th {
        padding: .5rem .85rem;
        font-size: .9rem;
    }
</style>

<section id="absensi">
    <div class="card" style="z-index: 0;">
        <div class="card-body">
            <h4 class="card-title"><?php echo (isset($card_title)) ? $card_title : '' ?></h4>
            <h6 class="card-subtitle"><?php echo (isset($card_subTitle)) ? $card_subTitle : '' ?></h6>

            <div class="table-action row">
                <div class="buttons col">
                    <div class="input-group mb-0">
                        <div class="input-group-prepend">
                            <label class="input-group-text" style="height: 34.13px; background: #f2f2f2;">Tanggal</label>
                        </div>
                        <input type="text" class="custom-select filter-date flatpickr-datemax-today" placeholder="Select" style="height: 34.13px; max-width: 130px;" value="<?= date('Y-m-d') ?>" />
                        <div class="input-group-apend">
                            <button class="btn btn--raised btn-primary btn--icon-text page-action-filter" style="height: 34.13px;">
                                <i class="zmdi zmdi-filter-list"></i> Apply
                            </button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="loader" style="display: none;">
                <div style="text-align: center;">
                    <div class="lds-ellipsis">
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                    </div>
                </div>
            </div>

            <div class="filter-result"></div>

            <div class="filter-info filter-no-data">
                <i class="zmdi zmdi-filter-frames"></i>
                <p>Tidak ditemukan data</p>
                <p>Silahkan lakukan filter terlebih dahulu</p>
            </div>
        </div>
    </div>
</section>