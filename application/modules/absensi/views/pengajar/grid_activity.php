<style type="text/css">
  .table-scroll {
    position: relative;
    overflow: hidden;
    border: 1px solid #ececec;
    border-radius: 2px;
    z-index: 0;
  }

  .table-wrap {
    width: 100%;
    overflow: auto;
    padding: .5rem;
  }

  .table-scroll table {
    width: 100%;
    margin: auto;
    border-collapse: collapse;
  }

  .table-scroll tr th {
    background-color: #fafafa;
  }

  .table-scroll th,
  .table-scroll td {
    padding: 4px 8px;
    border: 1px solid #ececec;
    white-space: nowrap;
    vertical-align: top;
    text-align: center;
  }

  .table-scroll tbody tr:hover {
    background: #f2f2f2;
  }

  .table-scroll thead,
  .table-scroll tfoot {
    background: #f9f9f9;
  }

  /* Input style */
  input[type=radio] {
    -moz-appearance: none;
    -webkit-appearance: none;
    -o-appearance: none;
    outline: none;
    content: none;
  }

  input[type=radio]:before {
    content: "     ";
    white-space: pre;
    font-size: 11px;
    color: transparent !important;
    background: #fff;
    width: 25px;
    height: 25px;
    border: 1px dotted #ccc;
    border-radius: 4px;
  }

  input[type=radio]:checked:before {
    color: #2dad76 !important;
    content: " ✓ ";
    width: 25px;
    height: 25px;
  }

  .input-text {
    border: 1px dotted #ccc;
    border-radius: 4px;
    font-size: 11px;
    color: #333;
  }

  .dataTables_wrapper,
  .dataTables_wrapper .table {
    margin: 0;
  }

  .page-action {
    background-color: #fafafa;
    padding: 15px;
    margin-top: 1rem;
    box-shadow: 0 1px 2px rgba(0, 0, 0, .075);
  }
</style>

<form id="form-absensi-pengajar" method="post" enctype="multipart/form-data" autocomplete="off">
  <!-- CSRF -->
  <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />

  <div id="table-scroll" class="table-scroll">
    <div class="action-buttons p-2 border-bottom border-danger">
      <a href="javascript:;" class="btn btn-danger btn--icon-text absensi-scanqr" data-toggle="modal" data-target="#modal-scanqr">
        <i class="zmdi zmdi-center-focus-strong"></i> Scan QR-Code
      </a>
    </div>
    <div class="table-wrap">
      <table id="table-absensi-pengajar" class="table table-hover hover">
        <thead>
          <tr>
            <th rowspan="2" style="width: 70px;">No</th>
            <th rowspan="2">Nama Lengkap</th>
            <th rowspan="2" style="width: 400px;">Kelas</th>
            <th colspan="4">Status</th>
            <th rowspan="2">Catatan</th>
          </tr>
          <tr>
            <th style="width: 80px;">Hadir</th>
            <th style="width: 80px;">Izin</th>
            <th style="width: 80px;">Sakit</th>
            <th style="width: 80px;">Alpa</th>
          </tr>
        </thead>
        <tbody>
          <?php if (count($data_pengajar) > 0) : ?>
            <?php $no = 1 ?>
            <?php foreach ($data_pengajar as $index => $item) : ?>
              <tr>
                <td><?= $no++ ?></td>
                <td style="text-align: left;">
                  <?= $item->nama_lengkap ?>
                  <input type="hidden" name="absen[<?= $index ?>][pengajar_id]" value="<?= $item->id ?>" />
                </td>
                <td style="text-align: left;">
                  <select class="input-text" name="absen[<?= $index ?>][kelas]" style="width: 200px;">
                    <?php
                    if (count($list_kelas) > 0) {
                      $kelasCurrent = (isset($item->kelas)) ? $item->kelas : null;
                      $kelasOptions = '<option value="-1" disabled selected>Kelas &#8595;</option>';

                      foreach ($list_kelas as $indexKelas => $kelas) {
                        $kelasSelected = ($kelas['value'] == $kelasCurrent) ? 'selected' : '';
                        $kelasOptions .= '<option value="' . $kelas['value'] . '" ' . $kelasSelected . '>' . $kelas['label'] . '</option>';
                      };

                      echo $kelasOptions;
                    } else {
                      echo '<option disabled selected>(Kelas is not available)</option>';
                    };
                    ?>
                  </select>
                  <select class="input-text" name="absen[<?= $index ?>][mata_pelajaran_id]" style="width: 200px;">
                    <?php
                    if (count($list_mapel) > 0) {
                      $mapelCurrent = (isset($item->mata_pelajaran_id)) ? $item->mata_pelajaran_id : null;
                      $mapelOptions = '<option value="-1" disabled selected>Mata Pelajaran &#8595;</option>';

                      foreach ($list_mapel as $indexMapel => $mapel) {
                        $mapelSelected = ($mapel['value'] == $mapelCurrent) ? 'selected' : '';
                        $mapelOptions .= '<option value="' . $mapel['value'] . '" ' . $mapelSelected . '>' . $mapel['label'] . '</option>';
                      };

                      echo $mapelOptions;
                    } else {
                      echo '<option disabled selected>(Mapel is not available)</option>';
                    };
                    ?>
                  </select>
                </td>
                <td>
                  <input type="radio" name="absen[<?= $index ?>][status]" value="Hadir" <?= ($item->status === 'Hadir') ? 'checked' : '' ?> />
                </td>
                <td>
                  <input type="radio" name="absen[<?= $index ?>][status]" value="Izin" <?= ($item->status === 'Izin') ? 'checked' : '' ?> />
                </td>
                <td>
                  <input type="radio" name="absen[<?= $index ?>][status]" value="Sakit" <?= ($item->status === 'Sakit') ? 'checked' : '' ?> />
                </td>
                <td>
                  <input type="radio" name="absen[<?= $index ?>][status]" value="Alpa" <?= ($item->status === 'Alpa') ? 'checked' : '' ?> />
                </td>
                <td>
                  <input type="text" name="absen[<?= $index ?>][catatan]" class="input-text" maxlength="255" style="width: 100%;" value="<?= $item->catatan ?>" />
                </td>
              </tr>
            <?php endforeach; ?>
          <?php else : ?>
            <tr>
              <td colspan="8">Tidak ditemukan data</td>
            </tr>
          <?php endif; ?>
        </tbody>
      </table>
    </div>
  </div>

  <div class="page-action">
    <div class="row">
      <div class="col text-right">
        <button type="submit" class="btn btn--raised btn-success btn--icon-text page-action-save spinner-action-button">
          <i class="zmdi zmdi-save"></i>
          Save Changes
        </button>
      </div>
    </div>
  </div>
</form>