<script type="text/javascript">
  $(document).ready(function() {

    var _key = "";
    var _section = "absensi";
    var _form_absensi = "form-absensi-pengajar";
    var _modal_scanqr = "modal-scanqr";
    var _activeDate = null;
    var _isReadyScan = true;

    // Initialize WebCodeCamJS
    var WebCodeCamJS_Args = {
      brightness: 15,
      resultFunction: function(result) {
        const code = result.code.split("##");

        if (code.length === 4) {
          if (_isReadyScan === true) {
            _isReadyScan = false;

            const pengajarId = code[0];
            const pengajarNama = code[1];
            const kelas = code[2];
            const mapelId = code[3];

            $("#" + _modal_scanqr + " #cam-error").html("").hide();
            $("#" + _modal_scanqr + " .scan-results").show();
            $("#" + _modal_scanqr + " .scan-results-info").show();
            $("#" + _modal_scanqr + " .scan-results-status").html("HADIR");
            $("#" + _modal_scanqr + " .scan-results-info .scan-results-info-nama").html(pengajarNama);
            $("#" + _modal_scanqr + " .scan-results-info .scan-results-info-kelas").html(kelas.replace("#", " "));

            storeAbsenByScan(pengajarId, kelas, mapelId);
          };
        } else {
          $("#" + _modal_scanqr + " #cam-error").html("Code detected, but format is not valid!").show();
          $("#" + _modal_scanqr + " .scan-results").hide();
        };
      },
      cameraSuccess: function(stream) {
        $("#" + _modal_scanqr + " #cam-error").html("").hide();
        $("#" + _modal_scanqr + " .scan-results").show();
        $("#" + _modal_scanqr + " .scan-results-info").hide();
        $("#" + _modal_scanqr + " .scan-results-status").html("Ready to scan...");
      },
      cameraError: function(error) {
        $("#" + _modal_scanqr + " #cam-error").html("Failed to open camera.").show();
        $("#" + _modal_scanqr + " .scan-results").hide();
      },
      getDevicesError: function(error) {
        $("#" + _modal_scanqr + " #cam-error").html("Failed to open camera.").show();
        $("#" + _modal_scanqr + " .scan-results").hide();
      },
      getUserMediaError: function(error) {
        $("#" + _modal_scanqr + " #cam-error").html("Failed to open camera.").show();
        $("#" + _modal_scanqr + " .scan-results").hide();
      },
      canPlayFunction: function() {
        $("#" + _modal_scanqr + " #cam-error").html("Can't execute play function.").show();
        $("#" + _modal_scanqr + " .scan-results").hide();
      },
    };
    var WebCodeCamJS_Decoder = $("#" + _modal_scanqr + " #cam-canvas").WebCodeCamJQuery(WebCodeCamJS_Args).data().plugin_WebCodeCamJQuery;
    WebCodeCamJS_Decoder.buildSelectMenu("#cam-source");

    // Handle ajax loader
    $(document).ajaxStart(function() {
      $(".loader").show();
    });
    $(document).ajaxStop(function() {
      $(".loader").hide();
    });

    // Handle scan barcode: Start
    $(document).on("click", ".absensi-scanqr", function(e) {
      e.preventDefault();
      $("#" + _modal_scanqr + " .scan-results").hide();
      WebCodeCamJS_Decoder.play();
    });

    // handle scan barcode: Stop
    $("#" + _modal_scanqr).on("click", "button.scanqr-action-close", function() {
      WebCodeCamJS_Decoder.stop();
    });

    // Handle scan barcode: Change source
    $("#" + _modal_scanqr + " #cam-source").on("change", function() {
      WebCodeCamJS_Decoder.stop().play();
    });

    // Handle filter submit
    $("#" + _section + " .page-action-filter").on("click", function() {
      var filter_result = $(".filter-result");
      var filter_no_data = $(".filter-no-data");
      var filter_date = $(".filter-date").val();
      var data = {
        filter_date
      };
      _activeDate = filter_date;

      $.ajax({
        type: "post",
        url: "<?php echo base_url('absensi/pengajar/ajax_get_activity/') ?>",
        data: data,
        success: function(response) {
          var result = isJson(response);

          if (result !== false) {
            if (result.status === false) {
              filter_result.html("");
              filter_no_data.show();
              notify(result.data, "danger");
            } else {
              notify("Error undefined.", "danger");
            };
          } else {
            filter_result.html("");
            filter_result.html(response);
            filter_no_data.hide();

            // Initialize dataTables
            initTableAbsensiKelasHarian();
          };

          // Handle submit button
          $(".page-action-save").show();
        }
      });
    });

    // Handle data submit
    $(document).on("click", "#" + _form_absensi + " .page-action-save", function(e) {
      e.preventDefault();

      swal({
        title: "Konfirmasi",
        text: "Anda yakin semua data yang akan disubmit sudah benar?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#39bbb0',
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false
      }).then((result) => {
        if (result.value) {
          var filter_date = $(".filter-date").val();

          var form = $("#" + _form_absensi)[0];
          var data = new FormData(form);
          data.append('filter_date', filter_date);

          $.ajax({
            type: "post",
            url: "<?php echo base_url('absensi/pengajar/ajax_save_activity/') ?>",
            data: data,
            dataType: "json",
            enctype: "multipart/form-data",
            processData: false,
            contentType: false,
            cache: false,
            success: function(response) {
              if (response.status === true) {
                notify(response.data, "success");
              } else {
                notify(response.data, "danger");
              };
            }
          });
        };
      });
    });

    function storeAbsenByScan(pengajarId, kelas, mapelId) {
      if (_isReadyScan === false) {
        var filter_date = $(".filter-date").val();

        var data = new FormData();
        data.append('filter_date', filter_date);
        data.append('pengajar_id', pengajarId);
        data.append('kelas', kelas);
        data.append('mata_pelajaran_id', mapelId);

        $.ajax({
          type: "post",
          url: "<?php echo base_url('absensi/pengajar/ajax_save_activity_item/') ?>",
          data: data,
          dataType: "json",
          enctype: "multipart/form-data",
          processData: false,
          contentType: false,
          cache: false,
          success: function(response) {
            if (response.status === true) {
              notify(response.data, "success");
              setTimeout(
                function() {
                  _isReadyScan = true;
                  $("#" + _modal_scanqr + " #cam-error").html("").hide();
                  $("#" + _modal_scanqr + " .scan-results").show();
                  $("#" + _modal_scanqr + " .scan-results-info").hide();
                  $("#" + _modal_scanqr + " .scan-results-status").html("Ready to scan...");
                  $("#" + _section + " .page-action-filter").click();
                }, 5000);
            } else {
              _isReadyScan = true;
              notify(response.data, "danger");
              $("#" + _modal_scanqr + " #cam-error").html(response.data).show();
              $("#" + _modal_scanqr + " .scan-results").hide();
            };
          }
        });
      };
    };

    function initTableAbsensiKelasHarian() {
      $("#table-absensi-pengajar").DataTable({
        paging: false,
        sorting: false,
        ordering: false,
        info: false,
        pageLength: 100,
        language: {
          searchPlaceholder: "Search..."
        }
      });
    };

    function isJson(object) {
      try {
        return $.parseJSON(object);
      } catch (err) {
        return false;
      };
    };

  });
</script>