<script type="text/javascript">
  $(document).ready(function() {

    var _key = "";
    var _section = "absensi";
    var _form_absensi_kegiatan = "form-absensi-kegiatan";
    var _activeDate = null;

    // Handle ajax loader
    $(document).ajaxStart(function() {
      $(".loader").show();
    });
    $(document).ajaxStop(function() {
      $(".loader").hide();
    });

    // Handle filter submit
    $("#" + _section + " .page-action-filter").on("click", function() {
      var filter_result = $(".filter-result");
      var filter_no_data = $(".filter-no-data");
      var filter_date = $(".filter-date").val();
      var filter_kelas = $(".filter-kelas").val();
      var data = {
        filter_date,
        filter_kelas
      };
      _activeDate = filter_date;

      $.ajax({
        type: "post",
        url: "<?php echo base_url('absensi/ajax_get_activity/') ?>",
        data: data,
        success: function(response) {
          var result = isJson(response);

          if (result !== false) {
            if (result.status === false) {
              filter_result.html("");
              filter_no_data.show();
              notify(result.data, "danger");
            } else {
              notify("Error undefined.", "danger");
            };
          } else {
            filter_result.html("");
            filter_result.html(response);
            filter_no_data.hide();

            // Initialize dataTables
            initTableAbsensiKegiatan();
          };

          // Handle submit button
          $(".page-action-save").show();
        }
      });
    });

    // Handle data submit
    $(document).on("click", "#" + _form_absensi_kegiatan + " .page-action-save", function(e) {
      e.preventDefault();

      swal({
        title: "Konfirmasi",
        text: "Anda yakin semua data yang akan disubmit sudah benar?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#39bbb0',
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false
      }).then((result) => {
        if (result.value) {
          var form = $("#" + _form_absensi_kegiatan)[0];
          var data = new FormData(form);

          $.ajax({
            type: "post",
            url: "<?php echo base_url('absensi/ajax_save_activity/?date=') ?>" + _activeDate,
            data: data,
            dataType: "json",
            enctype: "multipart/form-data",
            processData: false,
            contentType: false,
            cache: false,
            success: function(response) {
              if (response.status === true) {
                notify(response.data, "success");
              } else {
                notify(response.data, "danger");
              };
            }
          });
        };
      });
    });

    function initTableAbsensiKegiatan() {
      $("#table-absensi-kegiatan").DataTable({
        scrollX: true,
        scrollCollapse: true,
        paging: false,
        sorting: false,
        ordering: false,
        info: false,
        fixedColumns: {
          leftColumns: 2
        },
        pageLength: 100,
        language: {
          searchPlaceholder: "Search..."
        }
      });
    };

    function isJson(object) {
      try {
        return $.parseJSON(object);
      } catch (err) {
        return false;
      };
    };

  });
</script>