<style type="text/css">
  .table-scroll {
    position: relative;
    overflow: hidden;
    border: 1px solid #ececec;
    border-radius: 2px;
    z-index: 0;
  }

  .table-wrap {
    width: 100%;
    overflow: auto;
    padding: .5rem;
    padding-bottom: 0;
  }

  .table-scroll table {
    width: 100%;
    margin: auto;
    border-collapse: collapse;
  }

  .table-scroll tr th {
    background-color: #fafafa;
  }

  .table-scroll th,
  .table-scroll td {
    padding: 4px 8px;
    border: 1px solid #ececec;
    background: #fff;
    white-space: nowrap;
    vertical-align: top;
    text-align: center;
  }

  .table-scroll thead,
  .table-scroll tfoot {
    background: #f9f9f9;
  }

  /* Input style */
  input[type=radio] {
    -moz-appearance: none;
    -webkit-appearance: none;
    -o-appearance: none;
    outline: none;
    content: none;
  }

  input[type=radio]:before {
    content: "     ";
    white-space: pre;
    font-size: 11px;
    color: transparent !important;
    background: #fff;
    width: 25px;
    height: 25px;
    border: 1px dotted #ccc;
    border-radius: 4px;
  }

  input[type=radio]:checked:before {
    color: #2dad76 !important;
    content: " ✓ ";
    width: 25px;
    height: 25px;
  }

  .input-text {
    border: 1px dotted #ccc;
    border-radius: 4px;
    font-size: 11px;
    color: #333;
  }

  .dataTables_wrapper,
  .dataTables_wrapper .table {
    margin: 0;
  }

  .page-action {
    background-color: #fafafa;
    padding: 15px;
    margin-top: 1rem;
    box-shadow: 0 1px 2px rgba(0, 0, 0, .075);
  }
</style>

<form id="form-absensi-kegiatan" method="post" enctype="multipart/form-data" autocomplete="off">
  <!-- CSRF -->
  <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />

  <div id="table-scroll" class="table-scroll">
    <div class="table-wrap">
      <table id="table-absensi-kegiatan" class="table main-table existent">
        <thead>
          <tr>
            <th rowspan="3" class="fixed-side" scope="col" style="text-align: center; vertical-align: bottom; width: 50px;">NO</th>
            <th rowspan="3" class="fixed-side" scope="col" style="text-align: center; vertical-align: bottom;">NAMA</th>
            <!-- Header level 1 -->
            <?php $colspan = 0 ?>
            <?php foreach ($data_kegiatan as $kegiatan_key => $kegiatan) : ?>
              <?php
              foreach ($kegiatan->kegiatan_item as $kegiatan_item_key => $kegiatan_item) {
                if ($kegiatan_item->tipe_jawaban === 'Pilihan Ganda Single') {
                  if (!empty($kegiatan_item->option1)) {
                    $colspan = $colspan + 1;
                  };
                  if (!empty($kegiatan_item->option2)) {
                    $colspan = $colspan + 1;
                  };
                  if (!empty($kegiatan_item->option3)) {
                    $colspan = $colspan + 1;
                  };
                  if (!empty($kegiatan_item->option4)) {
                    $colspan = $colspan + 1;
                  };
                  if (!empty($kegiatan_item->option5)) {
                    $colspan = $colspan + 1;
                  };
                } else {
                  $colspan = $colspan + 1;
                };
              };
              ?>
              <th colspan="<?= $colspan ?>" scope="col"><?= $kegiatan->judul ?></th>
              <?php $colspan = 0 ?>
            <?php endforeach; ?>
          </tr>
          <tr>
            <!-- Header level 2 -->
            <?php foreach ($data_kegiatan as $kegiatan_key => $kegiatan) : ?>
              <?php $colspan = 0 ?>
              <?php $rowspan = 1 ?>
              <?php foreach ($kegiatan->kegiatan_item as $kegiatan_item_key => $kegiatan_item) : ?>
                <?php
                if ($kegiatan_item->tipe_jawaban === 'Pilihan Ganda Single') {
                  if (!empty($kegiatan_item->option1)) {
                    $colspan = $colspan + 1;
                  };
                  if (!empty($kegiatan_item->option2)) {
                    $colspan = $colspan + 1;
                  };
                  if (!empty($kegiatan_item->option3)) {
                    $colspan = $colspan + 1;
                  };
                  if (!empty($kegiatan_item->option4)) {
                    $colspan = $colspan + 1;
                  };
                  if (!empty($kegiatan_item->option5)) {
                    $colspan = $colspan + 1;
                  };
                } else {
                  $rowspan = 2;
                };
                ?>
                <th colspan="<?= $colspan ?>" rowspan="<?= $rowspan ?>"><?= $kegiatan_item->nama ?></th>
                <?php $colspan = 0 ?>
              <?php endforeach; ?>
            <?php endforeach; ?>
          </tr>
          <!-- Header level 3 -->
          <tr>
            <?php foreach ($data_kegiatan as $kegiatan_key => $kegiatan) : ?>
              <?php foreach ($kegiatan->kegiatan_item as $kegiatan_item_key => $kegiatan_item) : ?>
                <?php if ($kegiatan_item->tipe_jawaban === 'Pilihan Ganda Single') : ?>
                  <?php if (!empty($kegiatan_item->option1)) : ?>
                    <th><?= (in_array($kegiatan_item->slug, ['shalat-wajib', 'shalat-sunnah', 'membantu-pekerjaan-rumah'])) ? substr($kegiatan_item->option1, 0, 1) : $kegiatan_item->option1 ?></th>
                  <?php endif; ?>
                  <?php if (!empty($kegiatan_item->option2)) : ?>
                    <th><?= (in_array($kegiatan_item->slug, ['shalat-wajib', 'shalat-sunnah', 'membantu-pekerjaan-rumah'])) ? substr($kegiatan_item->option2, 0, 1) : $kegiatan_item->option2 ?></th>
                  <?php endif; ?>
                  <?php if (!empty($kegiatan_item->option3)) : ?>
                    <th><?= (in_array($kegiatan_item->slug, ['shalat-wajib', 'shalat-sunnah', 'membantu-pekerjaan-rumah'])) ? substr($kegiatan_item->option3, 0, 1) : $kegiatan_item->option3 ?></th>
                  <?php endif; ?>
                  <?php if (!empty($kegiatan_item->option4)) : ?>
                    <th><?= (in_array($kegiatan_item->slug, ['shalat-wajib', 'shalat-sunnah', 'membantu-pekerjaan-rumah'])) ? substr($kegiatan_item->option4, 0, 1) : $kegiatan_item->option4 ?></th>
                  <?php endif; ?>
                  <?php if (!empty($kegiatan_item->option5)) : ?>
                    <th><?= (in_array($kegiatan_item->slug, ['shalat-wajib', 'shalat-sunnah', 'membantu-pekerjaan-rumah'])) ? substr($kegiatan_item->option5, 0, 1) : $kegiatan_item->option5 ?></th>
                  <?php endif; ?>
                <?php endif; ?>
              <?php endforeach; ?>
            <?php endforeach; ?>
          </tr>
        </thead>
        <tbody>
          <!-- Answer -->
          <?php $no = 1 ?>
          <?php foreach ($data_kegiatan_santri as $santri_key => $santri) : ?>
            <tr>
              <th class="fixed-side text-center" style="width: 44px;"><?= $no++ ?></th>
              <th class="fixed-side"><?= $santri->nama_lengkap ?></th>
              <?php foreach ($santri->kegiatan as $kegiatan_key => $kegiatan) :  ?>
                <?php foreach ($kegiatan->kegiatan_item as $kegiatan_item_key => $kegiatan_item) : ?>
                  <?php $uniqueName = 'kegiatan_item[' . $santri->santri_id . '][' . $kegiatan_item->id . ']' ?>
                  <?php $uniqueId = 'kegiatan_item-' . $santri->santri_id . '-' . $kegiatan_item->id ?>
                  <?php $currentAnswer = $kegiatan_item->jawaban ?>
                  <?php if ($kegiatan_item->tipe_jawaban === 'Pilihan Ganda Single') : ?>
                    <!-- Pilihan Ganda Single -->
                    <?php if (!empty($kegiatan_item->option1)) : ?>
                      <td><input name="<?= $uniqueName ?>" id="<?= $uniqueId . '-option1' ?>" type="radio" value="<?= $kegiatan_item->option1 ?>" <?= ($currentAnswer == $kegiatan_item->option1) ? 'checked' : '' ?> <?= (!$is_today) ? 'onclick="javascript: return false;"' : '' ?> /></td>
                    <?php endif; ?>
                    <?php if (!empty($kegiatan_item->option2)) : ?>
                      <td><input name="<?= $uniqueName ?>" id="<?= $uniqueId . '-option2' ?>" type="radio" value="<?= $kegiatan_item->option2 ?>" <?= ($currentAnswer == $kegiatan_item->option2) ? 'checked' : '' ?> <?= (!$is_today) ? 'onclick="javascript: return false;"' : '' ?> /></td>
                    <?php endif; ?>
                    <?php if (!empty($kegiatan_item->option3)) : ?>
                      <td><input name="<?= $uniqueName ?>" id="<?= $uniqueId . '-option3' ?>" type="radio" value="<?= $kegiatan_item->option3 ?>" <?= ($currentAnswer == $kegiatan_item->option3) ? 'checked' : '' ?> <?= (!$is_today) ? 'onclick="javascript: return false;"' : '' ?> /></td>
                    <?php endif; ?>
                    <?php if (!empty($kegiatan_item->option4)) : ?>
                      <td><input name="<?= $uniqueName ?>" id="<?= $uniqueId . '-option4' ?>" type="radio" value="<?= $kegiatan_item->option4 ?>" <?= ($currentAnswer == $kegiatan_item->option4) ? 'checked' : '' ?> <?= (!$is_today) ? 'onclick="javascript: return false;"' : '' ?> /></td>
                    <?php endif; ?>
                    <?php if (!empty($kegiatan_item->option5)) : ?>
                      <td><input name="<?= $uniqueName ?>" id="<?= $uniqueId . '-option5' ?>" type="radio" value="<?= $kegiatan_item->option5 ?>" <?= ($currentAnswer == $kegiatan_item->option5) ? 'checked' : '' ?> <?= (!$is_today) ? 'onclick="javascript: return false;"' : '' ?> /></td>
                    <?php endif; ?>
                  <?php elseif ($kegiatan_item->tipe_jawaban === 'Isian Bebas') : ?>
                    <!-- isian Bebas -->
                    <td><input name="<?= $uniqueName ?>" type="text" class="input-text" value="<?= $currentAnswer  ?>" <?= (!$is_today) ? 'readonly' : '' ?> /></td>
                  <?php else : ?>
                    <!-- Default -->
                    <td>-</td>
                  <?php endif; ?>
                <?php endforeach; ?>
              <?php endforeach; ?>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
    </div>
  </div>

  <div class="page-action">
    <div class="row">
      <!-- <div class="col" style="display: flex; align-items: center;">
        <i class="zmdi zmdi-info"></i>&nbsp;
        Data yang sudah disimpan hanya dapat diubah pada hari ini saja.
      </div> -->
      <div class="col text-right">
        <button type="submit" class="btn btn--raised btn-success btn--icon-text page-action-save spinner-action-button">
          <i class="zmdi zmdi-save"></i>
          Save Changes
        </button>
      </div>
    </div>
  </div>
</form>