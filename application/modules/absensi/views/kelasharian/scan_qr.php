<div class="modal fade" id="modal-scanqr" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title pull-left">Scan QR-Code</h5>
      </div>
      <div class="spinner">
        <div class="lds-hourglass"></div>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <label>Source</label>
          <div class="select">
            <select id="cam-source" class="form-control"></select>
            <i class="form-group__bar"></i>
          </div>
        </div>
        <canvas id="cam-canvas" class="border border-light" style="width: 100%;"></canvas>
        <div id="cam-error" class="alert alert-danger mt-2 mb-0" style="display: none;"></div>
        <div class="card card-body p-2 mt-2 mb-0 border-success scan-results" style="display: none;">
          <span class="badge badge-success scan-results-status">
            Ready to scan...
          </span>
          <div class="scan-results-info" style="display: none;">
            <p class="mt-2 mb-2">
              NISN : <br>
              <b class="scan-results-info-nisn">-</b>
            </p>
            <p class="mb-2">
              Nama Lengkap : <br>
              <b class="scan-results-info-nama">-</b>
            </p>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-light btn--icon-text scanqr-action-close" data-dismiss="modal">
          Close
        </button>
      </div>
    </div>
  </div>
</div>