<style type="text/css">
  .table-scroll {
    position: relative;
    overflow: hidden;
    border: 1px solid #ececec;
    border-radius: 2px;
    z-index: 0;
  }

  .table-wrap {
    width: 100%;
    overflow: auto;
    padding: .5rem;
  }

  .table-scroll table {
    width: 100%;
    margin: auto;
    border-collapse: collapse;
  }

  .table-scroll tr th {
    background-color: #fafafa;
  }

  .table-scroll th,
  .table-scroll td {
    padding: 4px 8px;
    border: 1px solid #ececec;
    white-space: nowrap;
    vertical-align: top;
    text-align: center;
  }

  .table-scroll tbody tr:hover {
    background: #f2f2f2;
  }

  .table-scroll thead,
  .table-scroll tfoot {
    background: #f9f9f9;
  }

  /* Input style */
  input[type=radio] {
    -moz-appearance: none;
    -webkit-appearance: none;
    -o-appearance: none;
    outline: none;
    content: none;
  }

  input[type=radio]:before {
    content: "     ";
    white-space: pre;
    font-size: 11px;
    color: transparent !important;
    background: #fff;
    width: 25px;
    height: 25px;
    border: 1px dotted #ccc;
    border-radius: 4px;
  }

  input[type=radio]:checked:before {
    color: #2dad76 !important;
    content: " ✓ ";
    width: 25px;
    height: 25px;
  }

  .input-text {
    border: 1px dotted #ccc;
    border-radius: 4px;
    font-size: 11px;
    color: #333;
  }

  .dataTables_wrapper,
  .dataTables_wrapper .table {
    margin: 0;
  }

  .page-action {
    background-color: #fafafa;
    padding: 15px;
    margin-top: 1rem;
    box-shadow: 0 1px 2px rgba(0, 0, 0, .075);
  }
</style>

<form id="form-absensi-kelasharian" method="post" enctype="multipart/form-data" autocomplete="off">
  <!-- CSRF -->
  <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />

  <div id="table-scroll" class="table-scroll">
    <div class="action-buttons p-2 border-bottom border-danger">
      <a href="javascript:;" class="btn btn-danger btn--icon-text absensi-scanqr" data-toggle="modal" data-target="#modal-scanqr">
        <i class="zmdi zmdi-center-focus-strong"></i> Scan QR-Code
      </a>
      <a href="javascript:;" class="btn btn-secondary btn--icon-text absensi-export-excel">
        <i class="zmdi zmdi-download"></i> Export to Excel
      </a>
    </div>
    <div class="table-wrap">
      <table id="table-absensi-kelasharian" class="table table-hover hover">
        <thead>
          <tr>
            <th rowspan="2" style="width: 70px;">No</th>
            <th rowspan="2" style="width: 120px">NIL</th>
            <th rowspan="2">Nama</th>
            <th colspan="4">Status</th>
            <th rowspan="2">Catatan</th>
          </tr>
          <tr>
            <th style="width: 80px;">Hadir</th>
            <th style="width: 80px;">Izin</th>
            <th style="width: 80px;">Sakit</th>
            <th style="width: 80px;">Alpa</th>
          </tr>
        </thead>
        <tbody>
          <?php if (count($data_santri) > 0) : ?>
            <?php $no = 1 ?>
            <?php foreach ($data_santri as $index => $item) : ?>
              <tr>
                <td><?= $no++ ?></td>
                <td><?= $item->nomor_induk_lokal ?></td>
                <td style="text-align: left;"><?= $item->nama_lengkap ?></td>
                <td>
                  <input type="radio" name="absen[<?= $item->id ?>][status]" value="Hadir" <?= ($item->status === 'Hadir') ? 'checked' : '' ?> />
                </td>
                <td>
                  <input type="radio" name="absen[<?= $item->id ?>][status]" value="Izin" <?= ($item->status === 'Izin') ? 'checked' : '' ?> />
                </td>
                <td>
                  <input type="radio" name="absen[<?= $item->id ?>][status]" value="Sakit" <?= ($item->status === 'Sakit') ? 'checked' : '' ?> />
                </td>
                <td>
                  <input type="radio" name="absen[<?= $item->id ?>][status]" value="Alpa" <?= ($item->status === 'Alpa') ? 'checked' : '' ?> />
                </td>
                <td>
                  <input type="text" name="absen[<?= $item->id ?>][catatan]" class="input-text" maxlength="255" style="width: 100%;" value="<?= $item->catatan ?>" />
                </td>
              </tr>
            <?php endforeach; ?>
          <?php else : ?>
            <tr>
              <td colspan="7">Tidak ditemukan data</td>
            </tr>
          <?php endif; ?>
        </tbody>
      </table>
    </div>
  </div>

  <div class="page-action">
    <div class="row">
      <div class="col text-right">
        <button type="submit" class="btn btn--raised btn-success btn--icon-text page-action-save spinner-action-button">
          <i class="zmdi zmdi-save"></i>
          Save Changes
        </button>
      </div>
    </div>
  </div>
</form>