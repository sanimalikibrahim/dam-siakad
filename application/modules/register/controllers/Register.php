<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . 'controllers/AppBackend.php');

class Register extends AppBackend
{
  function __construct()
  {
    parent::__construct();
    $this->load->model([
      'RegisterModel',
      'KelasModel',
      'SubkelasModel',
      'UserModel'
    ]);
    $this->load->library('form_validation');
  }

  public function index()
  {
    $app = $this->app();

    if (isset($app->psb_status) && $app->psb_status == 0) {
      $data = array(
        'app' => $app,
        'main_js' => $this->load_main_js('register'),
        'page_title' => 'Register | ' . $this->app()->app_name,
        'list_kelas' => $this->init_list($this->KelasModel->getAll(), 'nama', 'nama'),
        'list_sub_kelas' => $this->init_list($this->SubkelasModel->getAll(), 'nama', 'nama'),
        'list_pembina' => $this->init_list($this->UserModel->getAll(['role' => 'Pembina']), 'id', 'nama_lengkap')
      );
      $this->load->view('index', $data);
    } else {
      show_404();
    };
  }

  public function ajax_submit()
  {
    $this->handle_ajax_request();
    $this->form_validation->set_rules($this->RegisterModel->rules_santri());

    if ($this->form_validation->run() === true) {
      echo json_encode($this->RegisterModel->insert_santri());
    } else {
      $errors = validation_errors('<div>- ', '</div>');
      echo json_encode(array('status' => false, 'data' => $errors));
    };
  }
}
