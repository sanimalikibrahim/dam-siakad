<script type="text/javascript">
  $(document).ready(function() {

    var _form = "form-register";

    // Handle ajax start
    $(document).ajaxStart(function() {
      $(".spinner-action-button").attr("disabled", true);
    });

    // Handle ajax stop
    $(document).ajaxStop(function() {
      $(".spinner-action-button").removeAttr("disabled");
    });

    // Handle data submit
    $("#" + _form + " .page-action-register").on("click", function(e) {
      e.preventDefault();

      var form = $("#" + _form)[0];
      var data = new FormData(form);

      $.ajax({
        type: "post",
        url: "<?php echo base_url('register/ajax_submit/') ?>",
        data: data,
        dataType: "json",
        enctype: "multipart/form-data",
        processData: false,
        contentType: false,
        cache: false,
        success: function(response) {
          if (response.status === true) {
            $("#" + _form).trigger("reset");
            $("#" + _form + " .register-form-wrapper").hide();
            $("#" + _form + " .register-message").show();
            $("#" + _form + " .register-message-email").html(response.email);
          } else {
            notify(response.data, "danger");
          };
        }
      });
      return false;
    });

    // Handle visibility password event
    $(".visibility-password").html('<i class="zmdi zmdi-eye"></i>').css("width", "30px");
    $(".visibility-password").on("click", function() {
      var obj = $(this);
      var input = obj.attr("data-input");

      if (obj.html() === '<i class="zmdi zmdi-eye"></i>') {
        $(input).prop("type", "text");
        obj.html('<i class="zmdi zmdi-eye-off"></i>');
      } else {
        $(input).prop("type", "password");
        obj.html('<i class="zmdi zmdi-eye"></i>');
      };
    });

  });
</script>