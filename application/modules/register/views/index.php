<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title><?php echo $page_title ?></title>
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Favicons -->
  <link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url('themes/_public/') ?>img/favicon/apple-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url('themes/_public/') ?>img/favicon/apple-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url('themes/_public/') ?>img/favicon/apple-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url('themes/_public/') ?>img/favicon/apple-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url('themes/_public/') ?>img/favicon/apple-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url('themes/_public/') ?>img/favicon/apple-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url('themes/_public/') ?>img/favicon/apple-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url('themes/_public/') ?>img/favicon/apple-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url('themes/_public/') ?>img/favicon/apple-icon-180x180.png">
  <link rel="icon" type="image/png" sizes="192x192" href="i<?php echo base_url('themes/_public/') ?>mg/favicon/android-icon-192x192.png">
  <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url('themes/_public/') ?>img/favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url('themes/_public/') ?>img/favicon/favicon-96x96.png">
  <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('themes/_public/') ?>img/favicon/favicon-16x16.png">
  <link rel="manifest" href="<?php echo base_url('themes/_public/') ?>img/favicon/manifest.json">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-TileImage" content="<?php echo base_url('themes/_public/') ?>img/favicon/ms-icon-144x144.png">
  <meta name="theme-color" content="#ffffff">

  <!-- Vendor styles -->
  <link rel="stylesheet" href="<?php echo base_url('themes/material_admin/vendors/material-design-iconic-font/css/material-design-iconic-font.min.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('themes/material_admin/vendors/animate.css/animate.min.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('themes/material_admin/vendors/select2/css/select2.min.css') ?>">

  <!-- App styles -->
  <link rel="stylesheet" href="<?php echo base_url('themes/material_admin/css/app.min.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('themes/_public/css/public.main.css') ?>">

  <style type="text/css">
    .login__block {
      text-align: left;
      max-width: 450px;
      margin: 1rem;
    }

    .login__block__header {
      text-align: center;
    }

    .clear {
      height: 1.2rem;
    }
  </style>
</head>

<body data-ma-theme="<?php echo $app->theme_color ?>">
  <form id="form-register" autocomplete="off">
    <!-- CSRF -->
    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />

    <div class="login">
      <div class="login__block active">
        <div class="login__block__header">
          <img src="<?php echo base_url('themes/_public/img/logo/logo.png') ?>" />
          <span style="font-weight: 500;">Darul Arqam Muhammadiyah</span>
          <br />
          <small>Create Santri Account</small>
        </div>

        <div class="clear"></div>

        <div class="register-form-wrapper" style="display: block;">
          <div class="login__block__body">
            <div class="form-group">
              <label required>NISN</label>
              <input type="number" name="nisn" class="form-control mask-number register-nisn" placeholder="Nomor Induk Siswa Nasional" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==10) return false;" required />
              <i class="form-group__bar"></i>
            </div>

            <div class="form-group">
              <label required>Nama Lengkap</label>
              <input type="text" name="nama_lengkap" class="form-control register-nama_lengkap" placeholder="Nama Lengkap" required>
              <i class="form-group__bar"></i>
            </div>

            <div class="form-group">
              <label required>Kelas</label>
              <div class="buttons">
                <div class="input-group mb-0">
                  <div class="select">
                    <select name="kelas" class="form-control no-padding-l register-kelas" data-placeholder="Select &#8595;" required>
                      <?= $list_kelas ?>
                    </select>
                    <i class="form-group__bar"></i>
                  </div>
                  <div class="select" style="flex: 1;">
                    <select name="sub_kelas" class="form-control no-padding-l register-sub_kelas" data-placeholder="Select &#8595;" required>
                      <?= $list_sub_kelas ?>
                    </select>
                    <i class="form-group__bar"></i>
                  </div>
                </div>
              </div>
            </div>

            <div class="form-group">
              <label required>Pembina</label>
              <div class="select">
                <select name="pembina" class="form-control select2 register-pembina" data-placeholder="Select &#8595;" required>
                  <?= $list_pembina ?>
                </select>
                <i class="form-group__bar"></i>
              </div>
            </div>

            <div class="form-group">
              <label>Asrama</label>
              <input type="text" name="asrama" class="form-control register-asrama" placeholder="Nomor Asrama" />
              <i class="form-group__bar"></i>
            </div>

            <div class="form-group">
              <label required>Email</label>
              <input type="email" name="email" class="form-control register-email" placeholder="Email" required>
              <i class="form-group__bar"></i>
            </div>

            <div class="form-group">
              <label required>Password</label>
              <div class="input-group">
                <input type="password" name="password" class="form-control no-padding-l register-password" placeholder="Password" autocomplete="new-password" required>
                <i class="form-group__bar"></i>
                <div class="input-group-append">
                  <span class="input-group-text">
                    <a href="javascript:;" class="visibility-password" data-input=".register-password"></a>
                  </span>
                </div>
              </div>
            </div>

            <div class="form-group">
              <label required>Confirm Password</label>
              <div class="input-group">
                <input type="password" name="password_confirm" class="form-control no-padding-l register-password_confirm" placeholder="Type your password again" autocomplete="new-password" required>
                <i class="form-group__bar"></i>
                <div class="input-group-append">
                  <span class="input-group-text">
                    <a href="javascript:;" class="visibility-password" data-input=".register-password_confirm"></a>
                  </span>
                </div>
              </div>
            </div>

            <div class="text-center">
              <button class="btn btn--icon login__block__btn page-action-register spinner-action-button"><i class="zmdi zmdi-long-arrow-right"></i></button>
            </div>
          </div>
          <div style="border-top: 1px solid #eceff1; padding-top: 15px; margin-top: 20px; font-size: 9pt; color: #999; text-align: center;">
            <a href="<?php echo base_url('login') ?>" style="color: #999;">Login to existing account</a>
          </div>
        </div>

        <div class="register-message" style="display: none;">
          <div class="alert alert-success" role="alert">
            <h4 class="alert-heading">Well Done!</h4>
            Email has been sent to "<span class='register-message-email'>{email}</span>", please check your inbox or spam for confirmation account.
          </div>

          <div style="text-align: center; margin-top: 2rem; margin-bottom: 1rem;">
            <a href="<?= base_url('login') ?>" class="btn btn-primary">Login to your account</a>
          </div>
        </div>
      </div>
    </div>
  </form>

  <!-- Javascript -->
  <script src="<?php echo base_url('themes/material_admin/vendors/jquery/jquery.min.js') ?>"></script>
  <script src="<?php echo base_url('themes/material_admin/vendors/popper.js/popper.min.js') ?>"></script>
  <script src="<?php echo base_url('themes/material_admin/vendors/bootstrap/js/bootstrap.min.js') ?>"></script>
  <script src="<?php echo base_url('themes/material_admin/vendors/bootstrap-notify/bootstrap-notify.min.js') ?>"></script>
  <script src="<?php echo base_url('themes/material_admin/vendors/select2/js/select2.full.min.js') ?>"></script>

  <!-- App functions and actions -->
  <script src="<?php echo base_url('themes/material_admin/js/app.min.js') ?>"></script>

  <script type="text/javascript">
    function notify(nMessage, nType) {
      $.notify({
        message: nMessage
      }, {
        type: nType,
        z_index: 9999,
        delay: 2500,
        timer: 500,
        placement: {
          from: "top",
          align: "center"
        },
        template: '<div data-notify="container" class="alert alert-dismissible alert-{0} alert--notify" role="alert">' +
          '<span data-notify="message">{2}</span>' +
          '<button type="button" aria-hidden="true" data-notify="dismiss" class="alert--notify__close">Close</button>' +
          '</div>'
      });
    };

    // Handle CSRF
    $.ajaxPrefilter(function(options, originalOptions, jqXHR) {
      if (originalOptions.data instanceof FormData) {
        originalOptions.data.append("<?= $this->security->get_csrf_token_name(); ?>", "<?= $this->security->get_csrf_hash(); ?>");
      };
    });
  </script>

  <?php echo (isset($main_js)) ? $main_js : '' ?>
</body>

</html>