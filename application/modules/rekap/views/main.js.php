<script type="text/javascript">
  $(document).ready(function() {

    var _key = "";
    var _section = "rekap";
    var _table_berjamaah = "table-berjamaah";

    // Handle ajax loader
    $(document).ajaxStart(function() {
      $(".loader").show();
    });
    $(document).ajaxStop(function() {
      $(".loader").hide();
    });

    // Handle filter component
    $("#" + _section + " .filter-by").on("change", function() {
      filterComponent_by();
    });
    $("#" + _section + " .filter-kelas").on("change", function() {
      filterComponent_kelas();
    });

    // Handle fullscreen
    $(".page-action-fullscreen").on("click", function() {
      var c = $(this).closest(".card");
      c.hasClass("card--fullscreen") ? (c.removeClass("card--fullscreen"), $("body").removeClass("data-table-toggled")) : (c.addClass("card--fullscreen"), $("body").addClass("data-table-toggled"));
    });

    // Handle filter submit
    $("#" + _section + " .page-action-filter").on("click", function() {
      var filter_result = $(".filter-result");
      var filter_no_data = $(".filter-no-data");
      var filter_by = $(".filter-by").val();
      var filter_start_date = $(".filter-start_date").val();
      var filter_end_date = $(".filter-end_date").val();
      var filter_year = $(".filter-year").val();
      var filter_kelas = $(".filter-kelas").val();
      var filter_santri = $(".filter-santri").val();
      var export_button = $(".page-action-export");
      var data = {
        filter_by,
        filter_start_date,
        filter_end_date,
        filter_year,
        filter_kelas,
        filter_santri
      };

      if (filter_santri !== null) {
        $.ajax({
          type: "post",
          url: "<?php echo base_url('rekap/ajax_get_berjamaah/') ?>",
          data: data,
          success: function(response) {
            var result = isJson(response);

            if (result !== false) {
              if (result.status === false) {
                filter_result.html("");
                filter_no_data.show();
                export_button.hide();
                notify(result.data, "danger");
              } else {
                notify("Error undefined.", "danger");
              };
            } else {
              filter_result.html("");
              filter_result.html(response);
              filter_no_data.hide();
              export_button.show();

              if (filter_by == 'by_date') {
                // Initialize dataTables: Shalat Berjamaah
                initTableShalatBerjamaah();
              };
            };
          }
        });
      } else {
        filter_result.html("");
        filter_no_data.show();
        export_button.hide();
        notify(`Data santri untuk kelas yang dipilih tidak tersedia.`, "danger");
      };
    });

    // Handle export
    $("#" + _section + " .page-action-export").on("click", function() {
      var filter_by = $(".filter-by").val();
      var filter_start_date = $(".filter-start_date").val();
      var filter_end_date = $(".filter-end_date").val();
      var filter_year = $(".filter-year").val();
      var filter_kelas = $(".filter-kelas").val();
      var filter_santri = $(".filter-santri").val();

      if (filter_santri !== null) {
        var params = '?filter_by=' + filter_by;
        params += '&filter_start_date=' + filter_start_date;
        params += '&filter_end_date=' + filter_end_date;
        params += '&filter_year=' + filter_year;
        params += '&filter_kelas=' + filter_kelas.replace(/\#/g, "$");
        params += '&filter_santri=' + filter_santri;

        window.location.href = "<?php echo base_url('rekap/d-pdf/berjamaah/') ?>" + params;
      } else {
        notify(`Data santri untuk kelas yang dipilih tidak tersedia.`, "danger");
      };
    });

    function initTableShalatBerjamaah() {
      var groupColumn = 2;
      $('#table-shalat-berjamaah').DataTable({
        "columnDefs": [{
          targets: 0,
          render: function(data, type, row, meta) {
            return meta.row + meta.settings._iDisplayStart + 1;
          }
        }, {
          visible: false,
          targets: 1
        }, {
          visible: false,
          targets: groupColumn
        }],
        "order": [
          [1, 'asc']
        ],
        "displayLength": 50,
        sDom: '<"dataTables__top"lfB>rt<"dataTables__bottom"ip><"clear">',
        initComplete: function(a, b) {
          $(this).closest(".dataTables_wrapper").find(".dataTables__top").prepend('<div class="dataTables_buttons actions"><div class="dropdown actions__item"><i data-toggle="dropdown" class="zmdi zmdi-download" /><ul class="dropdown-menu dropdown-menu-right"><a href="javascript:;" class="dropdown-item table-shalat-berjamaah-excel" data-table-action="excel">Excel</a><a href="javascript:;" class="dropdown-item table-shalat-berjamaah-pdf" data-table-action="pdf">PDF</a></ul></div></div>')
        },
        "drawCallback": function(settings) {
          var api = this.api();
          var rows = api.rows({
            page: 'current'
          }).nodes();
          var last = null;

          api.column(groupColumn, {
            page: 'current'
          }).data().each(function(group, i) {
            if (last !== group) {
              $(rows).eq(i).before(
                '<tr class="group"><td colspan="37">' + group + '</td></tr>'
              );
              last = group;
            };
          });
        }
      });
    };

    function filterComponent_by() {
      var filter_by = $(".filter-by");
      var filter_start_date = $(".filter-start_date");
      var filter_end_date = $(".filter-end_date");
      var filter_year = $(".filter-year");
      var filter_kelas = $(".filter-kelas");
      var filter_santri = $(".filter-santri");

      if (filter_by.val() == "by_date") {
        // Berdasarkan Tanggal
        filter_start_date.show().val("").trigger("change");
        filter_end_date.show().val("").trigger("change");
        filter_year.hide().val($(".filter-year option:first").val()).trigger("change");
        filter_kelas.show().val($(".filter-kelas option:first").val()).trigger("change");
        filter_santri.hide().val($(".filter-santri option:first").val()).trigger("change");
      } else {
        // Tahunan
        filter_start_date.hide().val("").trigger("change");
        filter_end_date.hide().val("").trigger("change");
        filter_year.show().val($(".filter-year option:first").val()).trigger("change");
        filter_kelas.show().val($(".filter-kelas option:first").val()).trigger("change");
        filter_santri.hide().val($(".filter-santri option:first").val()).trigger("change");
      };
    };

    function filterComponent_kelas() {
      var filter_kelas = $(".filter-kelas");
      var filter_santri = $(".filter-santri");

      if (filter_kelas.val() != "all") {
        filter_santri.show().val($(".filter-santri option:first").val()).trigger("change");
      } else {
        filter_santri.hide().val($(".filter-santri option:first").val()).trigger("change");
      };

      // Get santri
      $.ajax({
        type: "post",
        url: "<?= base_url('rekap/ajax_get_santri/') ?>",
        data: {
          kelas: filter_kelas.val()
        },
        dataType: "json",
        success: function(response) {
          filter_santri.html(response);
        }
      });
    };

    function isJson(object) {
      try {
        return $.parseJSON(object);
      } catch (err) {
        return false;
      };
    };

  });
</script>