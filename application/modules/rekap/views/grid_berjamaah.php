<style type="text/css">
  @page {
    margin: 20px;
  }

  #table-berjamaah {
    font-size: 13px;
    border-collapse: collapse;
    width: 100%;
  }

  #table-berjamaah th {
    text-align: center;
    border: 1px solid #ccc;
    padding: 4px 8px;
  }

  #table-berjamaah td {
    border: 1px solid #ccc;
    padding: 4px 4px;
  }

  .result-wrapper {
    background: #f2f2f2;
    margin-top: 1rem;
    padding: 6px;
  }

  .result-content {
    background: #fff;
    padding: 8px;
  }

  .table-header {
    padding: 15px;
    text-align: center;
    font-size: 1.1rem;
    font-weight: bold;
    color: #555;
  }

  .table-subheader {
    padding-top: 10px;
    padding-bottom: 10px;
    font-weight: 500;
  }

  .text-center {
    text-align: center;
  }
</style>

<div class="result-wrapper">
  <div class="result-content">
    <div class="table-header">
      REKAPITULASI SHALAT BERJAMAAH SANTRI <BR />
      KELAS <?= $kelas ?>
    </div>
    <div class="table-subheader">
      <?= $tanggal ?>
    </div>
    <div class="table-responsive">
      <table id="table-berjamaah" class="table table-bordered table-hover table-condensed" style="margin: 0;">
        <thead>
          <tr>
            <th rowspan="2">NO</th>
            <th rowspan="2">NAMA</th>
            <th rowspan="2">Standar Kehadiran</th>
            <th colspan="3">Ketidakhadiran</th>
            <th colspan="4">JUMLAH</th>
            <th colspan="3">Jumlah Ketidakhadiran<br />Per Hari</th>
          </tr>
          <tr>
            <!-- Ketidakhadiran -->
            <th>T</th>
            <th>M</th>
            <th>H</th>
            <!-- Jumlah -->
            <th>Ketidakhadiran</th>
            <th>PKH</th>
            <th>Kehadiran</th>
            <th>PH</th>
            <!-- Jumlah Ketidakhadiran Per Hari -->
            <th>Total</th>
            <th colspan="2">Rata-Rata</th>
          </tr>
        </thead>
        <tbody>
          <?php if (count($data) > 0) : ?>
            <?php
            // Temporary
            $no = 1;
            $standar_kehadiran = 0;
            $total_ketidakhadiran_t = 0;
            $total_ketidakhadiran_m = 0;
            $total_ketidakhadiran_h = 0;
            $total_ketidakhadiran = 0;
            $total_pkh = 0;
            $total_kehadiran = 0;
            $total_ph = 0;
            $total_ketidakhadiran_per_hari = 0;
            ?>
            <?php foreach ($data as $key => $item) : ?>
              <?php
              // Total
              $standar_kehadiran = $item->standar_kehadiran;
              $total_ketidakhadiran_t = $total_ketidakhadiran_t + $item->ketidakhadiran_t;
              $total_ketidakhadiran_m = $total_ketidakhadiran_m + $item->ketidakhadiran_m;
              $total_ketidakhadiran_h = $total_ketidakhadiran_h + $item->ketidakhadiran_h;
              $total_ketidakhadiran = $total_ketidakhadiran + $item->ketidakhadiran;
              $total_pkh = $total_pkh + $item->pkh;
              $total_kehadiran = $total_kehadiran + $item->kehadiran;
              $total_ph = $total_ph + $item->ph;
              $total_ketidakhadiran_per_hari = $total_ketidakhadiran_per_hari + $item->ketidakhadiran_per_hari;
              ?>
              <tr>
                <td valign="top" style="width: 30px" class="text-center"><?= $no++ ?></td>
                <td valign="top" style="min-width: 200px;"><?= $item->nama_lengkap ?></td>
                <td valign="top" style="width: auto;" class="text-center"><?= $item->standar_kehadiran ?></td>
                <td valign="top" style="width: 50px;" class="text-center"><?= $item->ketidakhadiran_t ?></td>
                <td valign="top" style="width: 50px;" class="text-center"><?= $item->ketidakhadiran_m ?></td>
                <td valign="top" style="width: 50px;" class="text-center"><?= $item->ketidakhadiran_h ?></td>
                <td valign="top" style="width: 120px;" class="text-center"><?= $item->ketidakhadiran ?></td>
                <td valign="top" style="width: 120px;" class="text-center"><?= $item->pkh . '%' ?></td>
                <td valign="top" style="width: 120px;" class="text-center"><?= $item->kehadiran ?></td>
                <td valign="top" style="width: 120px;" class="text-center"><?= $item->ph . '%' ?></td>
                <td valign="top" style="width: 70px;" class="text-center"><?= $item->ketidakhadiran_per_hari ?></td>
                <td valign="top" style="width: 50px;" class="text-center">Hari</td>
                <td valign="top" style="width: 50px; background: #ddd9c4;" class="text-center"></td>
              </tr>
            <?php endforeach; ?>
          <?php endif; ?>
        </tbody>
        <tfoot>
          <tr>
            <th colspan="3" style="text-align: left; font-weight: bold; padding-left: 100px;"><b>TOTAL</b></th>
            <th><?= $total_ketidakhadiran_t ?></th>
            <th><?= $total_ketidakhadiran_m ?></th>
            <th><?= $total_ketidakhadiran_h ?></th>
            <th><?= $total_ketidakhadiran ?></th>
            <th><?= $total_pkh . '%' ?></th>
            <th><?= $total_kehadiran ?></th>
            <th><?= $total_ph . '%' ?></th>
            <th rowspan="3" style="vertical-align: middle;"><?= $total_ketidakhadiran_per_hari ?></th>
            <th rowspan="3" style="background: #ddd9c4; vertical-align: middle;"><?= number_format(($total_ketidakhadiran_per_hari / count($data)), 0) ?></th>
            <th rowspan="3" style="background: #ddd9c4; vertical-align: middle;">Hari</th>
          </tr>
          <tr>
            <th colspan="6" style="text-align: left; font-weight: bold; padding-left: 40px;">PROSENTASE KESELURUHAN</th>
            <th colspan="2"><?= number_format(($total_ketidakhadiran / ($standar_kehadiran * count($data)) * 100), 1) . '%' ?></th>
            <th colspan="2"><?= number_format(($total_kehadiran / ($standar_kehadiran * count($data)) * 100), 1) . '%' ?></th>
          </tr>
          <tr>
            <th colspan="10" style="background: #ddd9c4; font-weight: bold;">RATA RATA KETIDAKHADIRAN SHALAT BERJAMAAH</th>
          </tr>
        </tfoot>
      </table>
    </div>
  </div>
</div>