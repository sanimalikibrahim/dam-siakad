<style type="text/css">
    .filter-info {
        text-align: center;
        padding: 10rem 0rem;
    }

    .filter-info p {
        line-height: .75rem;
        font-size: 1.075rem;
        color: #999;
        font-weight: 400;
    }

    .filter-info .zmdi {
        font-size: 3.5rem;
        margin-bottom: 1rem;
        color: #c5c5c5;
    }
</style>

<section id="rekap">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title"><?php echo (isset($card_title)) ? $card_title : '' ?></h4>
            <h6 class="card-subtitle"><?php echo (isset($card_subTitle)) ? $card_subTitle : '' ?></h6>

            <div class="table-action row">
                <div class="buttons col">
                    <div class="input-group mb-0">
                        <div class="input-group-prepend">
                            <label class="input-group-text" style="height: 34.13px; background: #f2f2f2;">Filter</label>
                        </div>
                        <select class="custom-select filter-by" style="height: 34.13px; max-width: 130px;">
                            <option value="by_compact">Compact</option>
                            <option value="by_month">Bulanan</option>
                        </select>
                        <select class="custom-select filter-kelas" style="height: 34.13px; max-width: 200px;">
                            <?= $list_kelas ?>
                        </select>
                        <!-- Filter Compact -->
                        <input type="text" class="custom-select filter-start_date flatpickr-date" placeholder="Start Date" style="height: 34.13px; max-width: 130px;" />
                        <input type="text" class="custom-select filter-end_date flatpickr-date" placeholder="End Date" style="height: 34.13px; max-width: 130px;" />
                        <!-- END ## Filter Compact -->
                        <!-- Filter Bulanan -->
                        <select class="custom-select filter-year" style="height: 34.13px; max-width: 130px; display: none;">
                            <?php for ($year = date('Y') - 2; $year <= date('Y'); $year++) : ?>
                                <option value="<?= $year ?>" <?= ($year == date('Y')) ? 'selected' : '' ?>><?= $year ?></option>
                            <?php endfor ?>
                        </select>
                        <select class="custom-select filter-month" style="height: 34.13px; max-width: 130px; display: none;">
                            <option value="01" <?= (date('m') == '01') ? 'selected' : '' ?>>Januari</option>
                            <option value="02" <?= (date('m') == '02') ? 'selected' : '' ?>>Februari</option>
                            <option value="03" <?= (date('m') == '03') ? 'selected' : '' ?>>Maret</option>
                            <option value="04" <?= (date('m') == '04') ? 'selected' : '' ?>>April</option>
                            <option value="05" <?= (date('m') == '05') ? 'selected' : '' ?>>Mei</option>
                            <option value="06" <?= (date('m') == '06') ? 'selected' : '' ?>>Juni</option>
                            <option value="07" <?= (date('m') == '07') ? 'selected' : '' ?>>Juli</option>
                            <option value="08" <?= (date('m') == '08') ? 'selected' : '' ?>>Agustus</option>
                            <option value="09" <?= (date('m') == '09') ? 'selected' : '' ?>>September</option>
                            <option value="10" <?= (date('m') == '10') ? 'selected' : '' ?>>Oktober</option>
                            <option value="11" <?= (date('m') == '11') ? 'selected' : '' ?>>November</option>
                            <option value="12" <?= (date('m') == '12') ? 'selected' : '' ?>>Desember</option>
                        </select>
                        <!-- END ## Filter Bulanan -->
                        <div class="input-group-apend">
                            <button class="btn btn--raised btn-primary btn--icon-text page-action-filter" style="height: 34.13px;">
                                <i class="zmdi zmdi-filter-list"></i> Apply
                            </button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="loader" style="display: none;">
                <div style="text-align: center;">
                    <div class="lds-ellipsis">
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                    </div>
                </div>
            </div>

            <div class="filter-result pt-4" style="display: none;"></div>

            <div class="filter-info filter-no-data">
                <i class="zmdi zmdi-filter-frames"></i>
                <p>Tidak ditemukan data</p>
                <p>Silahkan lakukan filter terlebih dahulu</p>
            </div>
        </div>
    </div>
</section>