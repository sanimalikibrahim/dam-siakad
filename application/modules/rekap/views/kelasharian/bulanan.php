<?php if ($is_export === false) : ?>
  <div class="action-buttons pb-2 border-bottom border-danger">
    <a href="<?= base_url('rekap/d-xlsx/absen-kelas-month/?param=' . $url_param) ?>" class="btn btn-secondary btn--icon-text btn-action-export-excel">
      <i class="zmdi zmdi-download"></i> Export to Excel
    </a>
  </div>
<?php endif ?>

<div id="print-area" class="mt-4" style="font-size: 0.90rem !important;">
  <h4 class="card-title text-center">
    REKAPITULASI KEHADIRAN SANTRI KELAS <?= strtoupper($filter_kelas_name . ' ' . $filter_kelas_subName) ?>
  </h4>

  <div class="table-responsive">
    <table class="table table-sm table-bordered table-hover m-0">
      <thead class="text-center">
        <tr>
          <th rowspan="2" width="50">NO</th>
          <th rowspan="2" style="min-width: 200px;">N &nbsp; A &nbsp; M &nbsp; A</th>
          <th colspan="<?= $day_count ?>"><?= strtoupper($month_name) ?></th>
          <th colspan="2">HADIR</th>
          <th rowspan="2">SAKIT</th>
          <th rowspan="2">IZIN</th>
          <th rowspan="2">ALPA</th>
        </tr>
        <tr>
          <!-- Days -->
          <?php foreach ($payload as $index => $item) : ?>
            <th style="<?= ($item->day_name === 'Jumat') ? 'background: #ccc;' : '' ?>"><?= $item->day ?></th>
          <?php endforeach ?>
          <!-- END ## Days -->
          <!-- HADIR -->
          <th style="min-width: 90px;">JML SESI</th>
          <th style="min-width: 90px;">%</th>
        </tr>
      </thead>
      <tbody>
        <?php $no = 1 ?>
        <?php if (count($santri) > 0) : ?>
          <?php foreach ($santri as $index => $santri) : ?>
            <?php
            $jumlahHadir = 0;
            $jumlahSakit = 0;
            $jumlahIzin = 0;
            $jumlahAlpa = 0;
            $jumlahMapel = 0;
            ?>
            <tr>
              <td class="text-center"><?= $no++ ?></td>
              <td><?= $santri->nama_lengkap ?></td>
              <!-- Absen -->
              <?php foreach ($payload as $index => $item) : ?>
                <?php
                if ($item->day_name !== 'Jumat') {
                  $jumlahHadir = $jumlahHadir + $item->santri_absen[$santri->id]->jumlah_hadir;
                  $jumlahSakit = $jumlahSakit + $item->santri_absen[$santri->id]->jumlah_sakit;
                  $jumlahIzin = $jumlahIzin + $item->santri_absen[$santri->id]->jumlah_izin;
                  $jumlahAlpa = $jumlahAlpa + $item->santri_absen[$santri->id]->jumlah_alpa;
                  $jumlahMapel = $jumlahMapel + $item->santri_absen[$santri->id]->jumlah_mapel;
                };
                ?>
                <?php if ($item->day_name === 'Jumat') : ?>
                  <td class="text-center" style="background: #ccc;">-</td>
                <?php else : ?>
                  <td class="text-center"><?= $item->santri_absen[$santri->id]->jumlah_hadir ?></td>
                <?php endif ?>
              <?php endforeach ?>
              <!-- END ## Absen -->
              <td class="text-center"><?= $jumlahHadir ?></td>
              <td class="text-center"><?= number_format(((float) $jumlahHadir / (float) $jumlahMapel) * 100, 0) ?>%</td>
              <td class="text-center"><?= $jumlahSakit ?></td>
              <td class="text-center"><?= $jumlahIzin ?></td>
              <td class="text-center"><?= $jumlahAlpa ?></td>
            </tr>
          <?php endforeach ?>
        <?php else : ?>
          <tr>
            <td colspan="99">Data santri tidak ditemukan</td>
          </tr>
        <?php endif ?>
      </tbody>
    </table>
  </div>
</div>