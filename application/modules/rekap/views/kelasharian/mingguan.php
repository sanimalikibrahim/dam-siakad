<?php if ($is_export === false) : ?>
  <div class="action-buttons pb-2 border-bottom border-danger">
    <a href="<?= base_url('rekap/d-xlsx/absen-kelas-compact/?param=' . $url_param) ?>" class="btn btn-secondary btn--icon-text btn-action-export-excel">
      <i class="zmdi zmdi-download"></i> Export to Excel
    </a>
  </div>
<?php endif ?>

<div id="print-area" class="mt-4" style="font-size: 0.90rem !important;">
  <h4 class="card-title text-center">
    DAFTAR HADIR KELAS <?= strtoupper($filter_kelas_name . ' ' . $filter_kelas_subName) ?>
  </h4>

  <div class="table-responsive">
    <table class="table table-sm table-bordered table-hover m-0">
      <thead class="text-center">
        <tr>
          <th rowspan="3" width="50">NO</th>
          <th rowspan="3" style="min-width: 200px;">N &nbsp; A &nbsp; M &nbsp; A</th>
          <!-- Hari -->
          <?php foreach ($date_range as $index => $item) : ?>
            <th colspan="<?= $item->mapel_count ?>"><?= strtoupper($item->day_name) ?></th>
          <?php endforeach ?>
          <!-- END ## Hari -->
          <th colspan="4">JUMLAH</th>
        </tr>
        <tr>
          <!-- Tanggal -->
          <?php foreach ($date_range as $index => $item) : ?>
            <th colspan="<?= $item->mapel_count ?>" style="min-width: 100px;"><?= $item->date ?></th>
          <?php endforeach ?>
          <!-- END ## Tanggal -->
          <th colspan="4">ABSENSI</th>
        </tr>
        <tr>
          <!-- Mapel -->
          <?php foreach ($date_range as $index => $item) : ?>
            <?php if ((int) $item->mapel_count > 0) : ?>
              <?php foreach ($item->mapel as $indexMapel => $mapel) : ?>
                <th><?= strtoupper($mapel['mata_pelajaran_kode']) ?></th>
              <?php endforeach ?>
            <?php else : ?>
              <th>-</th>
            <?php endif ?>
          <?php endforeach ?>
          <!-- END ## Mapel -->
          <!-- JUMLAH -->
          <th>H</th>
          <th>S</th>
          <th>I</th>
          <th>TK</th>
        </tr>
      </thead>
      <tbody>
        <?php $no = 1 ?>
        <?php if (count($santri) > 0) : ?>
          <?php foreach ($santri as $index => $santri) : ?>
            <?php
            $jumlahHadir = 0;
            $jumlahSakit = 0;
            $jumlahIzin = 0;
            $jumlahTanpaKeterangan = 0;
            ?>
            <tr>
              <td class="text-center"><?= $no++ ?></td>
              <td><?= $santri->nama_lengkap ?></td>
              <!-- Absen -->
              <?php foreach ($date_range as $index => $item) : ?>
                <?php foreach ($item->mapel as $indexMapel => $mapel) : ?>
                  <?php
                  $status = isset($mapel['santri_absen'][$santri->id]) ? $mapel['santri_absen'][$santri->id]->status : null;
                  $statusSimple = (!is_null($status)) ? strtoupper(substr($status, 0, 1)) : '<span class="text-muted">-</span>';

                  $jumlahHadir = ($statusSimple === 'H') ? $jumlahHadir + 1 : $jumlahHadir;
                  $jumlahSakit = ($statusSimple === 'S') ? $jumlahSakit + 1 : $jumlahSakit;
                  $jumlahIzin = ($statusSimple === 'I') ? $jumlahIzin + 1 : $jumlahIzin;
                  $jumlahTanpaKeterangan = (is_null($status)) ? $jumlahTanpaKeterangan + 1 : $jumlahTanpaKeterangan;
                  ?>
                  <td class="text-center"><?= $statusSimple ?></td>
                <?php endforeach ?>
              <?php endforeach ?>
              <!-- END ## Absen -->
              <td class="text-center"><?= $jumlahHadir ?></td>
              <td class="text-center"><?= $jumlahSakit ?></td>
              <td class="text-center"><?= $jumlahIzin ?></td>
              <td class="text-center"><?= $jumlahTanpaKeterangan ?></td>
            </tr>
          <?php endforeach ?>
        <?php else : ?>
          <tr>
            <td colspan="99">Data santri tidak ditemukan</td>
          </tr>
        <?php endif ?>
      </tbody>
    </table>
  </div>
</div>