<div class="table-responsive" style="padding-bottom: 1.5rem;">
  <table id="table-shalat-berjamaah" class="table table-bordered table-hover table-condensed">
    <thead>
      <tr>
        <th rowspan="2" class="text-center">NO</th>
        <th rowspan="2" class="text-center">DIBUAT</th>
        <th rowspan="2" class="text-center">TANGGAL</th>
        <th rowspan="2" class="text-center">NAMA</th>
        <th colspan="5" class="text-center">SABTU</th>
        <th colspan="5" class="text-center">AHAD</th>
        <th colspan="5" class="text-center">SENIN</th>
        <th colspan="5" class="text-center">SELASA</th>
        <th colspan="5" class="text-center">RABU</th>
        <th colspan="5" class="text-center">KAMIS</th>
        <th colspan="5" class="text-center">JUMAT</th>
      </tr>
      <tr>
        <!-- Ahad -->
        <th>S</th>
        <th>D</th>
        <th>A</th>
        <th>M</th>
        <th>I</th>
        <!-- Senin -->
        <th>S</th>
        <th>D</th>
        <th>A</th>
        <th>M</th>
        <th>I</th>
        <!-- Selasa -->
        <th>S</th>
        <th>D</th>
        <th>A</th>
        <th>M</th>
        <th>I</th>
        <!-- Rabu -->
        <th>S</th>
        <th>D</th>
        <th>A</th>
        <th>M</th>
        <th>I</th>
        <!-- Kamis -->
        <th>S</th>
        <th>D</th>
        <th>A</th>
        <th>M</th>
        <th>I</th>
        <!-- Jumat -->
        <th>S</th>
        <th>D</th>
        <th>A</th>
        <th>M</th>
        <th>I</th>
        <!-- Sabtu -->
        <th>S</th>
        <th>D</th>
        <th>A</th>
        <th>M</th>
        <th>I</th>
      </tr>
    </thead>
    <tbody>
      <?php if (count($activity) > 0) : ?>
        <?php foreach ($activity as $key => $item) : ?>
          <?php
          $created_at = strtotime($item->created_at);
          $day = date('d', $created_at);
          $day_name = $app->get_day(date('D', $created_at));
          $month = $app->get_month(date('m', $created_at));
          $year = date('Y', $created_at);
          ?>
          <tr>
            <td valign="top" style="width: 30px" class="text-center"></td>
            <td valign="top" style="min-width: 200px;"><?= $item->created_at ?></td>
            <td valign="top" style="min-width: 200px;"><?= $day_name . ', ' . $day . ' ' . $month . ' ' . $year ?></td>
            <td valign="top" style="min-width: 200px;"><?= $item->nama_lengkap ?></td>
            <!-- Sabtu -->
            <td valign="top" class="text-center"><?= ($item->sabtu_shubuh) ? '✓' : '' ?></td>
            <td valign="top" class="text-center"><?= ($item->sabtu_dzuhur) ? '✓' : '' ?></td>
            <td valign="top" class="text-center"><?= ($item->sabtu_ashar) ? '✓' : '' ?></td>
            <td valign="top" class="text-center"><?= ($item->sabtu_magrib) ? '✓' : '' ?></td>
            <td valign="top" class="text-center"><?= ($item->sabtu_isya) ? '✓' : '' ?></td>
            <!-- Ahad -->
            <td valign="top" class="text-center"><?= ($item->minggu_shubuh) ? '✓' : '' ?></td>
            <td valign="top" class="text-center"><?= ($item->minggu_dzuhur) ? '✓' : '' ?></td>
            <td valign="top" class="text-center"><?= ($item->minggu_ashar) ? '✓' : '' ?></td>
            <td valign="top" class="text-center"><?= ($item->minggu_magrib) ? '✓' : '' ?></td>
            <td valign="top" class="text-center"><?= ($item->minggu_isya) ? '✓' : '' ?></td>
            <!-- Senin -->
            <td valign="top" class="text-center"><?= ($item->senin_shubuh) ? '✓' : '' ?></td>
            <td valign="top" class="text-center"><?= ($item->senin_dzuhur) ? '✓' : '' ?></td>
            <td valign="top" class="text-center"><?= ($item->senin_ashar) ? '✓' : '' ?></td>
            <td valign="top" class="text-center"><?= ($item->senin_magrib) ? '✓' : '' ?></td>
            <td valign="top" class="text-center"><?= ($item->senin_isya) ? '✓' : '' ?></td>
            <!-- Selasa -->
            <td valign="top" class="text-center"><?= ($item->selasa_shubuh) ? '✓' : '' ?></td>
            <td valign="top" class="text-center"><?= ($item->selasa_dzuhur) ? '✓' : '' ?></td>
            <td valign="top" class="text-center"><?= ($item->selasa_ashar) ? '✓' : '' ?></td>
            <td valign="top" class="text-center"><?= ($item->selasa_magrib) ? '✓' : '' ?></td>
            <td valign="top" class="text-center"><?= ($item->selasa_isya) ? '✓' : '' ?></td>
            <!-- Rabu -->
            <td valign="top" class="text-center"><?= ($item->rabu_shubuh) ? '✓' : '' ?></td>
            <td valign="top" class="text-center"><?= ($item->rabu_dzuhur) ? '✓' : '' ?></td>
            <td valign="top" class="text-center"><?= ($item->rabu_ashar) ? '✓' : '' ?></td>
            <td valign="top" class="text-center"><?= ($item->rabu_magrib) ? '✓' : '' ?></td>
            <td valign="top" class="text-center"><?= ($item->rabu_isya) ? '✓' : '' ?></td>
            <!-- Kamis -->
            <td valign="top" class="text-center"><?= ($item->kamis_shubuh) ? '✓' : '' ?></td>
            <td valign="top" class="text-center"><?= ($item->kamis_dzuhur) ? '✓' : '' ?></td>
            <td valign="top" class="text-center"><?= ($item->kamis_ashar) ? '✓' : '' ?></td>
            <td valign="top" class="text-center"><?= ($item->kamis_magrib) ? '✓' : '' ?></td>
            <td valign="top" class="text-center"><?= ($item->kamis_isya) ? '✓' : '' ?></td>
            <!-- Jumat -->
            <td valign="top" class="text-center"><?= ($item->jumat_shubuh) ? '✓' : '' ?></td>
            <td valign="top" class="text-center"><?= ($item->jumat_dzuhur) ? '✓' : '' ?></td>
            <td valign="top" class="text-center"><?= ($item->jumat_ashar) ? '✓' : '' ?></td>
            <td valign="top" class="text-center"><?= ($item->jumat_magrib) ? '✓' : '' ?></td>
            <td valign="top" class="text-center"><?= ($item->jumat_isya) ? '✓' : '' ?></td>
          </tr>
        <?php endforeach; ?>
      <?php endif; ?>
    </tbody>
  </table>
</div>