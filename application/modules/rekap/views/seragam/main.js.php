<script type="text/javascript">
  $(document).ready(function() {

    var _section = "rekap";

    // Handle ajax loader
    $(document).ajaxStart(function() {
      $(".loader").show();
    });
    $(document).ajaxStop(function() {
      $(".loader").hide();
    });

    // Handle filter submit
    $("#" + _section + " .page-action-filter").on("click", function() {
      var filter_year = $(".filter-year").val();

      $.ajax({
        type: "get",
        url: "<?php echo base_url('rekap/rekapseragam/ajax_get/') ?>" + filter_year,
        dataType: "json",
        success: function(response) {
          if (response.status === true) {
            $("#" + _section + " .filter-no-data").hide();
            $("#" + _section + " .filter-result").show();
            $("#" + _section + " .filter-result .santri-count").html(response.data);
            $("#" + _section + " .filter-result .santri-2-count").html(response.data2);

            if (response.data == '0' && response.data2 == '0') {
              $("#" + _section + " .page-action-export").attr("disabled", true);
            } else {
              $("#" + _section + " .page-action-export").removeAttr("disabled");
            };
          } else {
            notify(response.data, "danger");
          };
        }
      });
    });

    // Handle export
    $("#" + _section + " .page-action-export").on("click", function() {
      var isDisabled = (typeof $(this).attr("disabled") !== 'undefined') ? true : false;
      var filter_year = $(".filter-year").val();
      var params = "?tahun=" + filter_year;

      if (isDisabled === false) {
        window.location.href = "<?php echo base_url('rekap/d-xlsx/seragam/') ?>" + params;
      };
    });

  });
</script>