<?php if ($is_export === false) : ?>
  <div class="action-buttons pb-2 border-bottom border-danger">
    <a href="<?= base_url('rekap/d-xlsx/absen-pengajar-compact/?param=' . $url_param) ?>" class="btn btn-secondary btn--icon-text btn-action-export-excel">
      <i class="zmdi zmdi-download"></i> Export to Excel
    </a>
  </div>
<?php endif ?>

<div id="print-area" class="mt-4" style="font-size: 0.90rem !important;">
  <h4 class="card-title text-center">
    REKAPITULASI KEHADIRAN GURU KELAS <?= strtoupper($filter_kelas_name . ' ' . $filter_kelas_subName) ?>
  </h4>

  <div class="table-responsive">
    <table class="table table-sm table-bordered table-hover m-0">
      <thead class="text-center">
        <tr>
          <th rowspan="2" style="width: 150px;">NO</th>
          <!-- Hari -->
          <?php foreach ($periode_list as $index => $item) : ?>
            <th><?= strtoupper($item->day_name) ?></th>
          <?php endforeach ?>
          <!-- END ## Hari -->
        </tr>
        <tr>
          <!-- Tanggal -->
          <?php foreach ($periode_list as $index => $item) : ?>
            <th style="min-width: 100px;"><?= $item->date ?></th>
          <?php endforeach ?>
          <!-- END ## Tanggal -->
        </tr>
      </thead>
      <tbody>
        <?php if (count($payload) > 0) : ?>
          <?php foreach ($payload as $index => $item) : ?>
            <tr>
              <td><?= $item->sesi ?></td>
              <!-- Absen -->
              <?php foreach ($item->data as $indexData => $data) : ?>
                <td><?= (!is_null($data->pengajar_name)) ? $data->pengajar_name : '<span class="text-muted">-</span>' ?></td>
              <?php endforeach ?>
              <!-- END ## Absen -->
            </tr>
          <?php endforeach ?>
        <?php else : ?>
          <tr>
            <td colspan="99">Data sesi tidak ditemukan</td>
          </tr>
        <?php endif ?>
      </tbody>
    </table>
  </div>
</div>