<?php if ($is_export === false) : ?>
  <div class="action-buttons pb-2 border-bottom border-danger">
    <a href="<?= base_url('rekap/d-xlsx/absen-pengajar-month/?param=' . $url_param) ?>" class="btn btn-secondary btn--icon-text btn-action-export-excel">
      <i class="zmdi zmdi-download"></i> Export to Excel
    </a>
  </div>
<?php endif ?>

<div id="print-area" class="mt-4" style="font-size: 0.90rem !important;">
  <h4 class="card-title text-center">
    REKAPITULASI KEHADIRAN GURU KELAS <?= strtoupper($filter_kelas_name . ' ' . $filter_kelas_subName) ?>
    <br>
    <b><?= strtoupper($month_name) . ' ' . $year ?></b>
  </h4>

  <div class="table-responsive">
    <table class="table table-sm table-bordered table-hover m-0">
      <thead class="text-center">
        <tr>
          <th rowspan="2" width="50">NO</th>
          <th rowspan="2" style="min-width: 200px;">NAMA GURU</th>
          <th rowspan="2">MAPEL</th>
          <th rowspan="2">JTM</th>
          <th colspan="2">KEHADIRAN</th>
          <th colspan="4">KETIDAKHADIRAN</th>
        </tr>
        <tr>
          <!-- Kehadiran -->
          <th style="min-width: 90px;">H</th>
          <th style="min-width: 90px;">%</th>
          <!-- Ketidakhadiran -->
          <th style="min-width: 90px;">SAKIT</th>
          <th style="min-width: 90px;">IZIN</th>
          <th style="min-width: 90px;">TK</th>
          <th style="min-width: 90px;">%</th>
        </tr>
      </thead>
      <tbody>
        <?php if (count($payload) > 0) : ?>
          <?php $no = 1 ?>
          <?php foreach ($payload as $index => $item) : ?>
            <tr>
              <td class="text-center"><?= $no++ ?></td>
              <td><?= $item->pengajar_nama_lengkap ?></td>
              <td><?= $item->mata_pelajaran_nama ?></td>
              <td class="text-center"><?= number_format($item->jtm) ?></td>
              <td class="text-center"><?= number_format($item->kehadiran_jumlah) ?></td>
              <td class="text-center"><?= number_format($item->kehadiran_persen) ?>%</td>
              <td class="text-center"><?= number_format($item->sakit) ?></td>
              <td class="text-center"><?= number_format($item->izin) ?></td>
              <td class="text-center"><?= number_format($item->tanpa_keterangan) ?></td>
              <td class="text-center"><?= number_format($item->ketidakhadiran_persen) ?>%</td>
            </tr>
          <?php endforeach ?>
        <?php else : ?>
          <tr>
            <td colspan="10">Tidak ditemukan data</td>
          </tr>
        <?php endif ?>
      </tbody>
    </table>
  </div>
</div>