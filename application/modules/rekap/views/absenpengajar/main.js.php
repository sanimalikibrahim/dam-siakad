<script type="text/javascript">
  $(document).ready(function() {

    var _section = "rekap";
    var _filterType = "by_compact";

    // Handle ajax loader
    $(document).ajaxStart(function() {
      $(".loader").show();
    });
    $(document).ajaxStop(function() {
      $(".loader").hide();
    });

    // Handle filter submit
    $("#" + _section + " .page-action-filter").on("click", function() {
      var filter_kelas = $(".filter-kelas").val();
      var filter_dateStart = $(".filter-start_date").val();
      var filter_dateEnd = $(".filter-end_date").val();
      var filter_year = $(".filter-year").val();
      var filter_month = $(".filter-month").val();
      var url = "undefined";

      switch (_filterType) {
        case "by_compact":
          url = "<?= base_url('rekap/rekapabsenpengajar/ajax_get_compact/') ?>";
          break;
        case "by_month":
          url = "<?= base_url('rekap/rekapabsenpengajar/ajax_get_month/') ?>";
          break;
        default:
          break;
      };

      $.ajax({
        type: "POST",
        url: url,
        data: {
          kelas: filter_kelas,
          start_date: filter_dateStart,
          end_date: filter_dateEnd,
          year: filter_year,
          month: filter_month
        },
        success: function(response) {
          if (response !== '404') {
            $("#" + _section + " .filter-no-data").hide();
            $("#" + _section + " .filter-result").html(response).show();
          } else {
            $("#" + _section + " .filter-no-data").show();
            $("#" + _section + " .filter-result").html("").hide();
          };
        }
      });
    });

    // Handle filter type
    $(document).on("change", ".filter-by", function() {
      _filterType = $(this).val();

      $("#" + _section + " .filter-no-data").show();
      $("#" + _section + " .filter-result").html("").hide();

      if (_filterType === "by_compact") {
        $(".filter-start_date").show();
        $(".filter-end_date").show();
        $(".filter-year").hide();
        $(".filter-month").hide();
      } else {
        $(".filter-start_date").hide();
        $(".filter-end_date").hide();
        $(".filter-year").show();
        $(".filter-month").show();
      };
    });

  });
</script>