<style type="text/css">
    .filter-info {
        text-align: center;
        padding: 10rem 0rem;
    }

    .filter-info p {
        line-height: .75rem;
        font-size: 1.075rem;
        color: #999;
        font-weight: 400;
    }

    .filter-info .zmdi {
        font-size: 3.5rem;
        margin-bottom: 1rem;
        color: #c5c5c5;
    }
</style>

<section id="rekap">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title"><?php echo (isset($card_title)) ? $card_title : '' ?></h4>
            <h6 class="card-subtitle"><?php echo (isset($card_subTitle)) ? $card_subTitle : '' ?></h6>

            <div class="table-action row">
                <div class="buttons col">
                    <div class="input-group mb-0">
                        <div class="input-group-prepend">
                            <label class="input-group-text" style="height: 34.13px; background: #f2f2f2;">Filter</label>
                        </div>
                        <select class="custom-select filter-year" style="height: 34.13px; max-width: 150px;">
                            <?= $list_tahun ?>
                        </select>
                        <select class="custom-select filter-status" style="height: 34.13px; max-width: 250px;">
                            <option disabled selected>Select &#8595;</option>
                            <option value="0">Belum Bayar & Nonaktif</option>
                            <option value="1">Sudah Bayar & Aktif</option>
                            <option value="2">Tidak Lulus Ujian</option>
                            <option value="3">Lulus Ujian (Belum Jadi Santri)</option>
                        </select>
                        <select class="custom-select filter-gender" style="height: 34.13px; max-width: 250px;">
                            <option disabled selected>Select &#8595;</option>
                            <option value="All">All</option>
                            <option value="Laki-laki">Laki-laki</option>
                            <option value="Perempuan">Perempuan</option>
                        </select>
                        <div class="input-group-apend">
                            <button class="btn btn--raised btn-primary btn--icon-text page-action-filter" style="height: 34.13px;">
                                <i class="zmdi zmdi-filter-list"></i> Apply
                            </button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="loader" style="display: none;">
                <div style="text-align: center;">
                    <div class="lds-ellipsis">
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                    </div>
                </div>
            </div>

            <div class="filter-result" style="display: none;">
                <div class="mt-4" style="font-size: 1rem; border: 1px solid #eee; padding: 1.5rem;">
                    <p class="mb-3">
                        <i class="zmdi zmdi-info"></i>
                        Jumlah calon santri <span class="santri-status">-</span> : <span class="santri-count" style="font-weight: 500;">0</span> orang
                    </p>
                    <button class="btn btn--raised btn-success btn--icon-text page-action-export">
                        <i class="zmdi zmdi-download"></i> Export to Excel
                    </button>
                </div>
            </div>

            <div class="filter-info filter-no-data">
                <i class="zmdi zmdi-filter-frames"></i>
                <p>Tidak ditemukan data</p>
                <p>Silahkan lakukan filter terlebih dahulu</p>
            </div>
        </div>
    </div>
</section>