<script type="text/javascript">
  $(document).ready(function() {

    var _section = "rekap";

    // Handle ajax loader
    $(document).ajaxStart(function() {
      $(".loader").show();
    });
    $(document).ajaxStop(function() {
      $(".loader").hide();
    });

    // Handle filter submit
    $("#" + _section + " .page-action-filter").on("click", function() {
      var filter_year = $(".filter-year").val();
      var filter_status = $(".filter-status").val();
      var filter_gender = $(".filter-gender").val();

      var params = "?tahun=" + filter_year;
      params += "&status=" + filter_status;
      params += "&gender=" + filter_gender;

      var statusName = '-';

      switch (parseInt(filter_status)) {
        case 0:
          statusName = 'Belum Bayar & Nonaktif';
          break;
        case 1:
          statusName = 'Sudah Bayar & Aktif';
          break;
        case 2:
          statusName = 'Tidak Lulus Ujian';
          break;
        case 3:
          statusName = 'Lulus Ujian (Belum Jadi Santri)';
          break;
        default:
          statusName = 'Undefined';
          break;
      };

      $.ajax({
        type: "get",
        url: "<?php echo base_url('rekap/ajax_get_calon_santri/') ?>" + params,
        dataType: "json",
        success: function(response) {
          if (response.status === true) {
            $("#" + _section + " .filter-no-data").hide();
            $("#" + _section + " .filter-result").show();
            $("#" + _section + " .filter-result .santri-status").html(statusName);
            $("#" + _section + " .filter-result .santri-count").html(response.data);

            if (response.data == '0') {
              $("#" + _section + " .page-action-export").attr("disabled", true);
            } else {
              $("#" + _section + " .page-action-export").removeAttr("disabled");
            };
          } else {
            notify(response.data, "danger");
          };
        }
      });
    });

    // Handle export
    $("#" + _section + " .page-action-export").on("click", function() {
      var isDisabled = (typeof $(this).attr("disabled") !== 'undefined') ? true : false;
      var filter_year = $(".filter-year").val();
      var filter_status = $(".filter-status").val();
      var filter_gender = $(".filter-gender").val();
      var params = "?tahun=" + filter_year;
      params += "&status=" + filter_status;
      params += "&gender=" + filter_gender;

      if (isDisabled === false) {
        window.location.href = "<?php echo base_url('rekap/d-xlsx/calon-santri/') ?>" + params;
      };
    });

  });
</script>