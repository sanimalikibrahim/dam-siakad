<style type="text/css">
    .filter-info {
        text-align: center;
        padding: 10rem 0rem;
    }

    .filter-info p {
        line-height: .75rem;
        font-size: 1.075rem;
        color: #999;
        font-weight: 400;
    }

    .filter-info .zmdi {
        font-size: 3.5rem;
        margin-bottom: 1rem;
        color: #c5c5c5;
    }

    .table td,
    .table th {
        padding: .5rem .85rem;
        font-size: .9rem;
    }

    .table tbody tr:hover {
        background-color: #fffde7;
    }

    .dataTables_wrapper {
        margin-top: 1rem;
    }
</style>

<section id="rekap">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title"><?php echo (isset($card_title)) ? $card_title : '' ?></h4>
            <h6 class="card-subtitle"><?php echo (isset($card_subTitle)) ? $card_subTitle : '' ?></h6>

            <div class="table-action row">
                <div class="buttons col">
                    <div class="input-group mb-0">
                        <div class="input-group-prepend">
                            <label class="input-group-text" style="height: 34.13px; background: #f2f2f2;">Filter</label>
                        </div>
                        <select class="custom-select filter-by" style="height: 34.13px; max-width: 130px;">
                            <option value="by_date">Compact</option>
                            <option value="by_year">Tahunan</option>
                        </select>
                        <input type="text" class="custom-select filter-start_date flatpickr-date" placeholder="Start Date" style="height: 34.13px; max-width: 130px;" />
                        <input type="text" class="custom-select filter-end_date flatpickr-date" placeholder="End Date" style="height: 34.13px; max-width: 130px;" />
                        <select class="custom-select filter-year" style="height: 34.13px; max-width: 130px; display: none;">
                            <option value="" selected disabled>Tahun</option>
                            <?php
                            $firstYear = (int) date('Y');
                            $lastYear = $firstYear - 4;
                            for ($i = $firstYear; $i >= $lastYear; $i--) {
                                echo '<option value=' . $i . '>' . $i . '</option>';
                            };
                            ?>
                        </select>
                        <select class="custom-select filter-kelas" style="height: 34.13px; max-width: 130px;">
                            <option value="" selected disabled>Kelas</option>
                            <?php if (count($data_kelas) > 0 && count($data_sub_kelas) > 0) : ?>
                                <?php foreach ($data_kelas as $key1 => $kelas) : ?>
                                    <?php foreach ($data_sub_kelas as $key2 => $sub_kelas) : ?>
                                        <option value="<?= $kelas->nama . '#' . $sub_kelas->nama ?>">
                                            <?= $kelas->nama . ' ' . $sub_kelas->nama ?>
                                        </option>
                                    <?php endforeach; ?>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                        <select class="custom-select filter-santri" style="height: 34.13px; display: none;">
                            <option value="all">Semua Santri</option>
                        </select>
                        <div class="input-group-apend">
                            <button class="btn btn--raised btn-primary btn--icon-text page-action-filter" style="height: 34.13px;">
                                <i class="zmdi zmdi-filter-list"></i> Apply
                            </button>
                            <button class="btn btn--raised btn-success btn--icon-text page-action-export" style="height: 34.13px; display: none;">
                                <i class="zmdi zmdi-download"></i> Export to PDF
                            </button>
                            <button class="btn btn--raised btn-secondary page-action-fullscreen" style="height: 34.13px;">
                                <i class="zmdi zmdi-fullscreen"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="loader" style="display: none;">
                <div style="text-align: center;">
                    <div class="lds-ellipsis">
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                    </div>
                </div>
            </div>

            <div class="filter-result"></div>

            <div class="filter-info filter-no-data">
                <i class="zmdi zmdi-filter-frames"></i>
                <p>Tidak ditemukan data</p>
                <p>Silahkan lakukan filter terlebih dahulu</p>
            </div>
        </div>
    </div>
</section>