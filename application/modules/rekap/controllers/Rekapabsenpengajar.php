<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . 'controllers/AppBackend.php');

class Rekapabsenpengajar extends AppBackend
{
  function __construct()
  {
    parent::__construct();
    $this->load->model([
      'AppModel',
      'JadwalPelajaranItemModel',
      'AbsenHarianGuruModel',
    ]);
    $this->load->library('form_validation');
  }

  public function index()
  {
    $list_kelas = $this->init_list_kelas();
    $data = array(
      'app' => $this->app(),
      'main_js' => $this->load_main_js('rekap/views/absenpengajar/main.js.php', true),
      'card_title' => 'Rekap › Absen Pengajar',
      'list_kelas' => $list_kelas,
    );
    $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
    $this->template->load_view('absenpengajar/index', $data, TRUE);
    $this->template->render();
  }

  public function ajax_get_compact()
  {
    $this->handle_ajax_request();

    $filterKelas = $this->input->post('kelas');
    $filterStartDate = $this->input->post('start_date');
    $filterEndDate = $this->input->post('end_date');

    $this->generate_pivot_compact($filterKelas, $filterStartDate, $filterEndDate);
  }

  public function ajax_get_month()
  {
    $this->handle_ajax_request();

    $filterKelas = $this->input->post('kelas');
    $filterYear = $this->input->post('year');
    $filterMonth = $this->input->post('month');

    $this->generate_pivot_month($filterKelas, $filterYear, $filterMonth);
  }

  public function export_excel_compact()
  {
    $param = $this->input->get('param');
    $filterKelas = null;
    $filterStartDate = null;
    $filterEndDate = null;

    // Extract params
    foreach (explode('&', $param) as $chunk) {
      $param = explode('=', $chunk);

      if ($param) {
        $filterKelas = (urldecode($param[0]) === 'kelas') ? urldecode($param[1]) : $filterKelas;
        $filterStartDate = (urldecode($param[0]) === 'start_date') ? urldecode($param[1]) : $filterStartDate;
        $filterEndDate = (urldecode($param[0]) === 'end_date') ? urldecode($param[1]) : $filterEndDate;
      };
    };

    $generateView = $this->generate_pivot_compact($filterKelas, $filterStartDate, $filterEndDate, true, true);
    $excelChar = $this->excelChar();
    $headerCellCount = $excelChar[(int) $generateView['header_cell_count']];
    $content = $generateView['content'];

    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Html();
    $spreadsheet = $reader->loadFromString($content);

    // Set style
    $spreadsheet->getActiveSheet()->mergeCells('A1:' . $headerCellCount . '1');
    $spreadsheet->getActiveSheet()->getStyle('A2:' . $headerCellCount . '3')->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
    $spreadsheet->getActiveSheet()->getStyle('A1:' . $headerCellCount . '3')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
    $spreadsheet->getActiveSheet()->getStyle('A1:' . $headerCellCount . '3')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
    $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(20);
    $spreadsheet->getActiveSheet()->getRowDimension(1)->setRowHeight(40);

    $outputFileName = 'rekap-absen_pengajar-' . $filterKelas . '-' . strtotime(date('YmdHis')) . '.xlsx';

    // Output stream
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="' . $outputFileName . '"');
    header('Cache-Control: max-age=0');
    header('Cache-Control: max-age=1');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
    header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header('Pragma: public'); // HTTP/1.0

    $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
    $writer->save('php://output');
    exit;
  }

  public function export_excel_month()
  {
    $param = $this->input->get('param');
    $filterKelas = null;
    $filterYear = null;
    $filterMonth = null;

    // Extract params
    foreach (explode('&', $param) as $chunk) {
      $param = explode('=', $chunk);

      if ($param) {
        $filterKelas = (urldecode($param[0]) === 'kelas') ? urldecode($param[1]) : $filterKelas;
        $filterYear = (urldecode($param[0]) === 'year') ? urldecode($param[1]) : $filterYear;
        $filterMonth = (urldecode($param[0]) === 'month') ? urldecode($param[1]) : $filterMonth;
      };
    };

    $generateView = $this->generate_pivot_month($filterKelas, $filterYear, $filterMonth, true, true);
    $content = $generateView['content'];

    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Html();
    $spreadsheet = $reader->loadFromString($content);

    // Set style
    $spreadsheet->getActiveSheet()->mergeCells('A1:J1');
    $spreadsheet->getActiveSheet()->mergeCells('A2:J2');
    $spreadsheet->getActiveSheet()->getStyle('A3:J4')->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
    $spreadsheet->getActiveSheet()->getStyle('A1:J4')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
    $spreadsheet->getActiveSheet()->getStyle('A1:J4')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
    $spreadsheet->getActiveSheet()->getStyle('A5:A300')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
    $spreadsheet->getActiveSheet()->getStyle('A5:A300')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
    $spreadsheet->getActiveSheet()->getStyle('D5:J300')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
    $spreadsheet->getActiveSheet()->getStyle('D5:J300')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
    $spreadsheet->getActiveSheet()->getStyle('A1')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_BOTTOM);
    $spreadsheet->getActiveSheet()->getStyle('A2')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);
    $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(5);
    $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(35);
    $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(35);
    $spreadsheet->getActiveSheet()->getRowDimension(1)->setRowHeight(40);
    $spreadsheet->getActiveSheet()->getRowDimension(2)->setRowHeight(30);

    $outputFileName = 'rekap-absen_pengajar-' . $filterKelas . '-' . $filterYear . '_' . $filterMonth . '-' . strtotime(date('YmdHis')) . '.xlsx';

    // Output stream
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="' . $outputFileName . '"');
    header('Cache-Control: max-age=0');
    header('Cache-Control: max-age=1');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
    header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header('Pragma: public'); // HTTP/1.0

    $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
    $writer->save('php://output');
    exit;
  }

  private function generate_pivot_compact($filterKelas, $filterStartDate, $filterEndDate, $returnAsData = false, $isExport = false)
  {
    if (!empty($filterKelas) && !empty($filterStartDate) && !empty($filterEndDate)) {
      $filterKelasExplode = explode('#', $filterKelas);
      $filterKelas_name = null;
      $filterKelas_subName = null;

      if (count($filterKelasExplode) === 2) {
        $filterKelas_name = $filterKelasExplode[0];
        $filterKelas_subName = $filterKelasExplode[1];
      };

      $begin = new DateTime($filterStartDate);
      $end = new DateTime($filterEndDate);
      $end = $end->modify('+1 day');
      $interval = DateInterval::createFromDateString('1 day');
      $period = new DatePeriod($begin, $interval, $end);
      $payload = array();
      $headerCellCount = 0;

      // Generate periode list
      foreach ($period as $indexPeriod => $item) {
        $dayName = $this->get_day($item->format('D'));

        if ($dayName !== 'Jumat') {
          $headerCellCount = $headerCellCount + 1;
          $periodeList[] = (object) array(
            'day_name' => $dayName,
            'date' => $item->format('d/m/y')
          );
        };
      };

      // Generate absen guru
      $sesiList = $this->JadwalPelajaranItemModel->getAll_sesi($filterKelas);

      if (count($sesiList) > 0) {
        foreach ($sesiList as $index => $sesi) {
          $sesiNo = $sesi->sesi;
          $sesiName = 'Sesi ' . $sesi->sesi;
          $data = array();

          foreach ($period as $indexPeriod => $item) {
            $dayName = $this->get_day($item->format('D'));

            if ($dayName !== 'Jumat') {
              $data[$indexPeriod] = (object) array(
                'day_name' => $dayName,
                'date' => $item->format('d/m/y'),
                'pengajar_name' => null
              );

              // Get absen pengajar
              $absenPengajar = $this->AbsenHarianGuruModel->getAll_rekap($filterKelas, $item->format('Y-m-d'), $sesiNo);

              if (!is_null($absenPengajar)) {
                $data[$indexPeriod]->pengajar_name = $absenPengajar->nama_pengajar;
              };
            };
          };

          $payload[] = (object) array(
            'sesi' => $sesiName,
            'data' => $data
          );
        };

        $urlParam = '?ref=cxsmi&kelas=' . str_replace('#', '#', $filterKelas) . '&start_date=' . $filterStartDate . '&end_date=' . $filterEndDate;
        $urlParam = urlencode($urlParam);

        $loadContent = $this->load->view('absenpengajar/mingguan', array(
          'controller' => $this,
          'filter_kelas' => $filterKelas,
          'filter_kelas_name' => $filterKelas_name,
          'filter_kelas_subName' => $filterKelas_subName,
          'start_date' => $filterStartDate,
          'end_date' => $filterEndDate,
          'periode_list' => $periodeList,
          'payload' => $payload,
          'is_export' => $isExport,
          'url_param' => $urlParam
        ), $returnAsData);

        if ($isExport === true) {
          return array(
            'header_cell_count' => $headerCellCount,
            'content' => $loadContent
          );
        } else {
          return $loadContent;
        };
      } else {
        echo '404';
      };
    } else {
      echo '404';
    };
  }

  private function generate_pivot_month($filterKelas, $filterYear, $filterMonth, $returnAsData = false, $isExport = false)
  {
    if (!empty($filterKelas) && !empty($filterYear) && !empty($filterMonth)) {
      $startDay = 1;
      $endDay = cal_days_in_month(0, $filterMonth, $filterYear);
      $monthName = $this->get_month($filterMonth);
      $startDate = $filterYear . '-' . $filterMonth . '-' . $startDay;
      $endDate = $filterYear . '-' . $filterMonth . '-' . $endDay;

      // Extract kelas
      $filterKelasExplode = explode('#', $filterKelas);
      $filterKelas_name = null;
      $filterKelas_subName = null;

      if (count($filterKelasExplode) === 2) {
        $filterKelas_name = $filterKelasExplode[0];
        $filterKelas_subName = $filterKelasExplode[1];
      };

      $payload = $this->AbsenHarianGuruModel->getAll_rekapByDate($filterKelas, $startDate, $endDate);

      $urlParam = '?ref=cxsmi&kelas=' . str_replace('#', '#', $filterKelas) . '&year=' . $filterYear . '&month=' . $filterMonth;
      $urlParam = urlencode($urlParam);

      $loadContent = $this->load->view('absenpengajar/bulanan', array(
        'controller' => $this,
        'filter_kelas' => $filterKelas,
        'filter_kelas_name' => $filterKelas_name,
        'filter_kelas_subName' => $filterKelas_subName,
        'year' => $filterYear,
        'month' => $filterMonth,
        'month_name' => $monthName,
        'payload' => $payload,
        'is_export' => $isExport,
        'url_param' => $urlParam
      ), $returnAsData);

      if ($isExport === true) {
        return array(
          'content' => $loadContent
        );
      } else {
        return $loadContent;
      };
    } else {
      echo '404';
    };
  }
}
