<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . 'controllers/AppBackend.php');
require_once(FCPATH . 'vendor/autoload.php');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Rekap extends AppBackend
{
  function __construct()
  {
    parent::__construct();
    $this->load->model([
      'AppModel',
      'KelasModel',
      'SubkelasModel',
      'SantriModel',
      'RekapModel',
      'PsbModel',
      'MappingOrtuModel',
    ]);
    $this->load->library('form_validation');
  }

  public function index()
  {
    show_404();
  }

  // Shalat Berjamaah
  public function berjamaah()
  {
    $user_id = $this->session->userdata('user')['id'];
    $user_role = $this->session->userdata('user')['role'];

    if ($user_role === 'Pembina') {
      $kelas = $this->SantriModel->getKelasByPembina($user_id);
      $sub_kelas = $this->SantriModel->getSubKelasByPembina($user_id);
    } else {
      $kelas = $this->KelasModel->getAll();
      $sub_kelas = $this->SubkelasModel->getAll();
    };

    $data = array(
      'app' => $this->app(),
      'main_js' => $this->load_main_js('rekap'),
      'card_title' => 'Rekap › Shalat Berjamaah',
      'data_kelas' => $kelas,
      'data_sub_kelas' => $sub_kelas
    );
    $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
    $this->template->load_view('index_berjamaah', $data, TRUE);
    $this->template->render();
  }

  public function ajax_get_berjamaah()
  {
    $this->handle_ajax_request();

    $filter_by = $this->input->post('filter_by');
    $filter_start_date = $this->input->post('filter_start_date');
    $filter_end_date = $this->input->post('filter_end_date');
    $filter_year = $this->input->post('filter_year');
    $filter_kelas = $this->input->post('filter_kelas');
    $filter_santri = $this->input->post('filter_santri');
    $kelas = null;
    $sub_kelas = null;

    if (!empty($filter_kelas)) {
      $filter_kelas_ex = explode('#', $filter_kelas);
      $kelas = (isset($filter_kelas_ex[0])) ? $filter_kelas_ex[0] : null;
      $sub_kelas = (isset($filter_kelas_ex[1])) ? $filter_kelas_ex[1] : null;
    };

    if ($filter_by === 'by_date') {
      // Compact
      if (!empty($filter_start_date) && !empty($filter_end_date) && !empty($filter_kelas)) {
        $santri = $this->SantriModel->getAll(['kelas' => $kelas, 'sub_kelas' => $sub_kelas]);

        if (count($santri) > 0) {
          $this->_getBerjamaahCompact($filter_by, $filter_santri, $filter_start_date, $filter_end_date, $kelas, $sub_kelas);
        } else {
          echo json_encode(['status' => false, 'data' => 'Data santri tidak ditemukan.']);
        };
      } else {
        echo json_encode(['status' => false, 'data' => 'Start Date, End Date & Kelas is required.']);
      };
    } else {
      // Tahunan
      if (!empty($filter_year) && !empty($filter_kelas)) {
        $santri = $this->SantriModel->getAll(['kelas' => $kelas, 'sub_kelas' => $sub_kelas]);

        if (count($santri) > 0) {
          $this->_getBerjamaahTahunan($filter_by, $filter_santri, $filter_start_date, $filter_end_date, $filter_year, $kelas, $sub_kelas);
        } else {
          echo json_encode(['status' => false, 'data' => 'Data santri tidak ditemukan.']);
        };
      } else {
        echo json_encode(['status' => false, 'data' => 'Tahun & Kelas is required.']);
      };
    };
  }

  public function download_pdf_berjamaah()
  {
    try {
      $filter_by = $this->input->get('filter_by');
      $filter_start_date = $this->input->get('filter_start_date');
      $filter_end_date = $this->input->get('filter_end_date');
      $filter_year = $this->input->get('filter_year');
      $filter_kelas = $this->input->get('filter_kelas');
      $filter_santri = $this->input->get('filter_santri');
      $kelas = null;
      $sub_kelas = null;

      if (!empty($filter_kelas)) {
        $filter_kelas_ex = explode('$', $filter_kelas);
        $kelas = (isset($filter_kelas_ex[0])) ? $filter_kelas_ex[0] : null;
        $sub_kelas = (isset($filter_kelas_ex[1])) ? $filter_kelas_ex[1] : null;
      };

      if ($filter_by === 'by_date') {
        // Compact
        $content = $this->_getBerjamaahCompact($filter_by, $filter_santri, $filter_start_date, $filter_end_date, $kelas, $sub_kelas, true);
      } else {
        // Tahunan
        $content = $this->_getBerjamaahTahunan($filter_by, $filter_santri, $filter_start_date, $filter_end_date, $filter_year, $kelas, $sub_kelas, true);
      };

      $file_name = 'Rekap_Berjamaah-' . date('YmdHis') . '.pdf';

      $mpdf = new \Mpdf\Mpdf(['orientation' => 'L']);

      header("Content-type:application/pdf");
      header("Content-Disposition:attachment;filename=$file_name");

      $mpdf->WriteHTML($content);
      $mpdf->Output($file_name, 'D');
    } catch (\Throwable $th) {
      echo 'An error occurred while creating the file.';
    };
  }

  private function _getBerjamaahCompact($filter_by, $filter_santri, $filter_start_date, $filter_end_date, $kelas, $sub_kelas, $renderAsData = false)
  {
    if (empty($filter_santri) || $filter_santri === 'all') {
      $rekap = $this->RekapModel->getBerjamaah($filter_start_date, $filter_end_date, $kelas, $sub_kelas, 30);
    } else {
      $rekap = $this->RekapModel->getBerjamaahBySantri($filter_start_date, $filter_end_date, $kelas, $sub_kelas, 30, $filter_santri);
    };

    $data = array(
      'app' => $this,
      'data' => $rekap,
      'kelas' => strtoupper($kelas . ' ' . $sub_kelas),
      'tanggal' => $this->_getTanggalRekap($filter_by, $filter_start_date, $filter_end_date)
    );
    return $this->load->view('grid_berjamaah', $data, $renderAsData);
  }

  private function _getBerjamaahTahunan($filter_by, $filter_santri, $filter_start_date, $filter_end_date, $filter_year, $kelas, $sub_kelas, $renderAsData = false)
  {
    $filter_start_date = $filter_year . '-01-01';
    $filter_end_date = $filter_year . '-12-31';

    if (empty($filter_santri) || $filter_santri === 'all') {
      $rekap = $this->RekapModel->getBerjamaah($filter_start_date, $filter_end_date, $kelas, $sub_kelas, 30);
    } else {
      $rekap = $this->RekapModel->getBerjamaahBySantri($filter_start_date, $filter_end_date, $kelas, $sub_kelas, 30, $filter_santri);
    };

    $data = array(
      'app' => $this,
      'data' => $rekap,
      'kelas' => strtoupper($kelas . ' ' . $sub_kelas),
      'tanggal' => $this->_getTanggalRekap($filter_by, $filter_start_date, $filter_end_date)
    );
    return $this->load->view('grid_berjamaah', $data, $renderAsData);
  }

  private function _getTanggalRekap($type, $start, $end)
  {
    $start_day = date('d', strtotime($start));
    $start_month = date('m', strtotime($start));
    $start_month_name = strtoupper($this->get_month($start_month));
    $start_year = date('Y', strtotime($start));
    $end_day = date('d', strtotime($end));
    $end_month = date('m', strtotime($end));
    $end_month_name = strtoupper($this->get_month($end_month));
    $end_year = date('Y', strtotime($end));
    $tanggal = 'TANGGAL : undefined';

    if ($type === 'by_date') {
      if (($start_day === $end_day) && ($start_month === $end_month) && ($start_year === $end_year)) {
        $tanggal = 'TANGGAL : ' . $start_day . ' ' . $start_month_name . ' ' . $start_year;
      } else if (($start_month === $end_month) && ($start_year === $end_year)) {
        $tanggal = 'TANGGAL : ' . $start_day . ' - ' . $end_day . ' ' . $start_month_name . ' ' . $start_year;
      } else {
        $tanggal = 'TANGGAL: ' . $start_day . ' ' . $start_month_name . ' ' . $start_year . ' - ' . $end_day . ' ' . $end_month_name . ' ' . $end_year;
      };
    } else {
      $tanggal = 'TAHUN : ' . $start_year;
    };

    return $tanggal;
  }
  // END ## Shalat Berjamaah

  // Santri Baru
  public function santri_baru()
  {
    $tahunList = $this->init_list($this->PsbModel->getTahunList(), 'tahun', 'tahun');

    $data = array(
      'app' => $this->app(),
      'main_js' => $this->load_main_js('rekap/views/santribaru/main.js.php', true),
      'card_title' => 'Rekap › Santri Baru',
      'list_tahun' => $tahunList
    );
    $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
    $this->template->load_view('santribaru/index', $data, TRUE);
    $this->template->render();
  }

  public function ajax_get_santri_baru($tahun = null)
  {
    $this->handle_ajax_request();

    try {
      if (!is_null($tahun) && !empty($tahun) && $tahun !== 'null') {
        $model = $this->PsbModel->getAll(['status' => 4, 'tahun' => $tahun]);
        $response = array('status' => true, 'data' => count($model));
      } else {
        $response = array('status' => false, 'data' => 'Tahun is required.');
      }
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to get the data, try again later.');
    };

    echo json_encode($response);
  }

  public function download_excel_santri_baru()
  {
    $tahun = $this->input->get('tahun');

    $model = $this->PsbModel->getAll(['status' => 4, 'tahun' => $tahun]);
    $fileTemplate = FCPATH . 'directory/templates/template-santri_baru_list.xlsx';

    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
    $spreadsheet = $reader->load($fileTemplate);

    $activeRow = 4;

    foreach ($model as $index => $item) {
      $ayah_tanggal_lahir = (!is_null($item->ayah_tanggal_lahir) && $item->ayah_tanggal_lahir != '0000-00-00') ? ', ' . date('dmY', strtotime($item->ayah_tanggal_lahir)) : ', -';
      $ibu_tanggal_lahir = (!is_null($item->ibu_tanggal_lahir) && $item->ibu_tanggal_lahir != '0000-00-00') ? ', ' . date('dmY', strtotime($item->ibu_tanggal_lahir)) : ', -';
      $wali_tanggal_lahir = (!is_null($item->wali_tanggal_lahir) && $item->wali_tanggal_lahir != '0000-00-00') ? ', ' . date('dmY', strtotime($item->wali_tanggal_lahir)) : ', -';

      // Alamat orang tua / wali
      if (!is_null($item->ayah_alamat) && !empty($item->ayah_alamat)) {
        $orang_tua_alamat = $item->ayah_alamat;
        $orang_tua_provinsi = $item->ayah_provinsi;
        $orang_tua_kota = $item->ayah_kota;
        $orang_tua_kecamatan = $item->ayah_kecamatan;
        $orang_tua_kelurahan = $item->ayah_kelurahan;
      } else if (!is_null($item->ibu_alamat) && !empty($item->ibu_alamat)) {
        $orang_tua_alamat = $item->ibu_alamat;
        $orang_tua_provinsi = $item->ibu_provinsi;
        $orang_tua_kota = $item->ibu_kota;
        $orang_tua_kecamatan = $item->ibu_kecamatan;
        $orang_tua_kelurahan = $item->ibu_kelurahan;
      } else {
        $orang_tua_alamat = $item->wali_alamat;
        $orang_tua_provinsi = $item->wali_provinsi;
        $orang_tua_kota = $item->wali_kota;
        $orang_tua_kecamatan = $item->wali_kecamatan;
        $orang_tua_kelurahan = $item->wali_kelurahan;
      };

      $spreadsheet->setActiveSheetIndex(0)
        ->setCellValue('A' . $activeRow, $item->nisn)
        ->setCellValue('B' . $activeRow, $item->nik)
        ->setCellValue('C' . $activeRow, $item->nama_lengkap)
        ->setCellValue('D' . $activeRow, $item->tempat_lahir)
        ->setCellValue('E' . $activeRow, date('d/m/Y', strtotime($item->tanggal_lahir)))
        ->setCellValue('F' . $activeRow, $item->jenis_kelamin)
        ->setCellValue('G' . $activeRow, $item->golongan_darah)
        ->setCellValue('H' . $activeRow, '-') // Hobi
        ->setCellValue('I' . $activeRow, '-') // Cita-cita
        ->setCellValue('J' . $activeRow, $item->urutan_anak)
        ->setCellValue('K' . $activeRow, $item->jumlah_saudara)
        ->setCellValue('S' . $activeRow, $orang_tua_alamat)
        ->setCellValue('T' . $activeRow, $orang_tua_provinsi)
        ->setCellValue('U' . $activeRow, $orang_tua_kota)
        ->setCellValue('V' . $activeRow, $orang_tua_kecamatan)
        ->setCellValue('W' . $activeRow, $orang_tua_kelurahan)
        ->setCellValue('X' . $activeRow, '-') // Wali: Kode Pos
        ->setCellValue('Y' . $activeRow, '-') // Wali: Tempat Tinggal
        ->setCellValue('AI' . $activeRow, $item->ayah_no_kk)
        ->setCellValue('AJ' . $activeRow, $item->ayah_nama_lengkap)
        ->setCellValue('AK' . $activeRow, $item->ayah_nama_lengkap)
        ->setCellValue('AL' . $activeRow, (!empty($item->ayah_tempat_lahir)) ? $item->ayah_tempat_lahir : '-' . $ayah_tanggal_lahir)
        ->setCellValue('AM' . $activeRow, $item->ayah_nik)
        ->setCellValue('AN' . $activeRow, $item->ayah_status)
        ->setCellValue('AO' . $activeRow, $item->ayah_pendidikan)
        ->setCellValue('AP' . $activeRow, $item->ayah_pekerjaan)
        ->setCellValue('AQ' . $activeRow, (!empty($item->ayah_telepon)) ? $item->ayah_telepon : $item->ayah_handphone)
        ->setCellValue('AR' . $activeRow, $item->ibu_nama_lengkap)
        ->setCellValue('AS' . $activeRow, (!empty($item->ibu_tempat_lahir)) ? $item->ibu_tempat_lahir : '-' . $ibu_tanggal_lahir)
        ->setCellValue('AT' . $activeRow, $item->ibu_nik)
        ->setCellValue('AU' . $activeRow, $item->ibu_status)
        ->setCellValue('AV' . $activeRow, $item->ibu_pendidikan)
        ->setCellValue('AW' . $activeRow, $item->ibu_pekerjaan)
        ->setCellValue('AX' . $activeRow, (!empty($item->ibu_telepon)) ? $item->ibu_telepon : $item->ibu_handphone)
        ->setCellValue('AZ' . $activeRow, $item->wali_nama_lengkap)
        ->setCellValue('BA' . $activeRow, (!empty($item->wali_tempat_lahir)) ? $item->wali_tempat_lahir : '-' . $wali_tanggal_lahir)
        ->setCellValue('BB' . $activeRow, $item->wali_nik)
        ->setCellValue('BC' . $activeRow, $item->wali_status)
        ->setCellValue('BD' . $activeRow, $item->wali_pendidikan)
        ->setCellValue('BE' . $activeRow, $item->wali_pekerjaan)
        ->setCellValue('BF' . $activeRow, (!empty($item->wali_telepon)) ? $item->wali_telepon : $item->wali_handphone);

      $activeRow++;
    };

    $styleArray = array(
      'borders' => array(
        'allBorders' => array(
          'style' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
          // 'color' => array('argb' => '000000'),
        ),
      ),
    );

    $spreadsheet->getActiveSheet()->getStyle('A1:' .
      $spreadsheet->getActiveSheet()->getHighestColumn() .
      $spreadsheet->getActiveSheet()->getHighestRow())->applyFromArray($styleArray);

    $writer = new Xlsx($spreadsheet);

    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="Rekap-Santri_Baru_Tahun_' . $tahun . '.xlsx"');
    header('Cache-Control: max-age=0');

    $writer->save('php://output');
  }
  // END ## Santri Baru

  // Calon Santri
  public function calon_santri()
  {
    $tahunList = $this->init_list($this->PsbModel->getTahunList(), 'tahun', 'tahun');

    $data = array(
      'app' => $this->app(),
      'main_js' => $this->load_main_js('rekap/views/calonsantri/main.js.php', true),
      'card_title' => 'Rekap › Calon Santri',
      'list_tahun' => $tahunList
    );
    $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
    $this->template->load_view('calonsantri/index', $data, TRUE);
    $this->template->render();
  }

  public function ajax_get_calon_santri()
  {
    $this->handle_ajax_request();

    try {
      $tahun = $this->input->get('tahun');
      $status = $this->input->get('status');
      $gender = $this->input->get('gender');
      $status = (is_null($status) || $status === 'null') ? -1 : $status;

      if (!is_null($tahun) && !empty($tahun) && $tahun !== 'null') {
        if (in_array($status, [0, 1, 2, 3])) {
          if (!is_null($gender) && !empty($gender) && $gender !== 'null') {
            if ($gender == 'All') {
              $modelFilter = ['status' => $status, 'tahun' => $tahun];
            } else {
              $modelFilter = ['status' => $status, 'tahun' => $tahun, 'jenis_kelamin' => $gender];
            };

            $model = $this->PsbModel->getAll($modelFilter);
            $response = array('status' => true, 'data' => count($model));
          } else {
            $response = array('status' => false, 'data' => 'Jenis Kelamin is required.');
          };
        } else {
          $response = array('status' => false, 'data' => 'Status is required.');
        };
      } else {
        $response = array('status' => false, 'data' => 'Tahun is required.');
      }
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to get the data, try again later.');
    };

    echo json_encode($response);
  }

  public function download_excel_calon_santri()
  {
    $tahun = $this->input->get('tahun');
    $status = $this->input->get('status');
    $gender = $this->input->get('gender');
    $status = (is_null($status) || $status === 'null') ? -1 : $status;

    switch ((int) $status) {
      case 0:
        $statusName = 'Belum_Bayar_Nonaktif';
        break;
      case 1:
        $statusName = 'Sudah_Bayar_Aktif';
        break;
      case 2:
        $statusName = 'Tidak_Lulus_Ujian';
        break;
      case 3:
        $statusName = 'Lulus_Ujian_(Belum_Jadi_Santri)';
        break;
      default:
        $statusName = 'Undefined';
        break;
    };

    if ($gender == 'All') {
      $modelFilter = ['status' => $status, 'tahun' => $tahun];
    } else {
      $modelFilter = ['status' => $status, 'tahun' => $tahun, 'jenis_kelamin' => $gender];
    };
    $model = $this->PsbModel->getAll($modelFilter);

    $fileTemplate = FCPATH . 'directory/templates/template-calon_santri_list.xlsx';

    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
    $spreadsheet = $reader->load($fileTemplate);

    $activeRow = 4;

    foreach ($model as $index => $item) {
      $ayah_tanggal_lahir = (!is_null($item->ayah_tanggal_lahir) && $item->ayah_tanggal_lahir != '0000-00-00') ? ', ' . date('dmY', strtotime($item->ayah_tanggal_lahir)) : ', -';
      $ibu_tanggal_lahir = (!is_null($item->ibu_tanggal_lahir) && $item->ibu_tanggal_lahir != '0000-00-00') ? ', ' . date('dmY', strtotime($item->ibu_tanggal_lahir)) : ', -';
      $wali_tanggal_lahir = (!is_null($item->wali_tanggal_lahir) && $item->wali_tanggal_lahir != '0000-00-00') ? ', ' . date('dmY', strtotime($item->wali_tanggal_lahir)) : ', -';

      // Alamat orang tua / wali
      if (!is_null($item->ayah_alamat) && !empty($item->ayah_alamat)) {
        $orang_tua_alamat = $item->ayah_alamat;
        $orang_tua_provinsi = $item->ayah_provinsi;
        $orang_tua_kota = $item->ayah_kota;
        $orang_tua_kecamatan = $item->ayah_kecamatan;
        $orang_tua_kelurahan = $item->ayah_kelurahan;
      } else if (!is_null($item->ibu_alamat) && !empty($item->ibu_alamat)) {
        $orang_tua_alamat = $item->ibu_alamat;
        $orang_tua_provinsi = $item->ibu_provinsi;
        $orang_tua_kota = $item->ibu_kota;
        $orang_tua_kecamatan = $item->ibu_kecamatan;
        $orang_tua_kelurahan = $item->ibu_kelurahan;
      } else {
        $orang_tua_alamat = $item->wali_alamat;
        $orang_tua_provinsi = $item->wali_provinsi;
        $orang_tua_kota = $item->wali_kota;
        $orang_tua_kecamatan = $item->wali_kecamatan;
        $orang_tua_kelurahan = $item->wali_kelurahan;
      };

      $spreadsheet->setActiveSheetIndex(0)
        ->setCellValue('A' . $activeRow, $item->nisn)
        ->setCellValue('B' . $activeRow, $item->nik)
        ->setCellValue('C' . $activeRow, $item->nama_lengkap)
        ->setCellValue('D' . $activeRow, $item->tempat_lahir)
        ->setCellValue('E' . $activeRow, date('d/m/Y', strtotime($item->tanggal_lahir)))
        ->setCellValue('F' . $activeRow, $item->jenis_kelamin)
        ->setCellValue('G' . $activeRow, $item->golongan_darah)
        ->setCellValue('J' . $activeRow, $item->urutan_anak)
        ->setCellValue('K' . $activeRow, $item->jumlah_saudara)
        ->setCellValue('L' . $activeRow, $item->alamat_santri)
        ->setCellValue('M' . $activeRow, $item->kota)
        ->setCellValue('N' . $activeRow, $item->provinsi)
        ->setCellValue('Q' . $activeRow, $item->sekolah_asal)
        ->setCellValue('T' . $activeRow, $item->sekolah_asal_alamat)
        ->setCellValue('V' . $activeRow, $orang_tua_alamat)
        ->setCellValue('W' . $activeRow, $orang_tua_provinsi)
        ->setCellValue('X' . $activeRow, $orang_tua_kota)
        ->setCellValue('Y' . $activeRow, $orang_tua_kecamatan)
        ->setCellValue('Z' . $activeRow, $orang_tua_kelurahan)
        ->setCellValue('AL' . $activeRow, $item->ayah_no_kk)
        ->setCellValue('AM' . $activeRow, $item->ayah_nama_lengkap)
        ->setCellValue('AN' . $activeRow, $item->ayah_nama_lengkap)
        ->setCellValue('AO' . $activeRow, (!empty($item->ayah_tempat_lahir)) ? $item->ayah_tempat_lahir : '-' . $ayah_tanggal_lahir)
        ->setCellValue('AP' . $activeRow, $item->ayah_nik)
        ->setCellValue('AQ' . $activeRow, $item->ayah_status)
        ->setCellValue('AR' . $activeRow, $item->ayah_pendidikan)
        ->setCellValue('AS' . $activeRow, $item->ayah_pekerjaan)
        ->setCellValue('AT' . $activeRow, (!empty($item->ayah_telepon)) ? $item->ayah_telepon : $item->ayah_handphone)
        ->setCellValue('AU' . $activeRow, $item->ibu_nama_lengkap)
        ->setCellValue('AV' . $activeRow, (!empty($item->ibu_tempat_lahir)) ? $item->ibu_tempat_lahir : '-' . $ibu_tanggal_lahir)
        ->setCellValue('AW' . $activeRow, $item->ibu_nik)
        ->setCellValue('AX' . $activeRow, $item->ibu_status)
        ->setCellValue('AY' . $activeRow, $item->ibu_pendidikan)
        ->setCellValue('AZ' . $activeRow, $item->ibu_pekerjaan)
        ->setCellValue('BA' . $activeRow, (!empty($item->ibu_telepon)) ? $item->ibu_telepon : $item->ibu_handphone)
        ->setCellValue('BC' . $activeRow, $item->wali_nama_lengkap)
        ->setCellValue('BD' . $activeRow, (!empty($item->wali_tempat_lahir)) ? $item->wali_tempat_lahir : '-' . $wali_tanggal_lahir)
        ->setCellValue('BE' . $activeRow, $item->wali_nik)
        ->setCellValue('BF' . $activeRow, $item->wali_pendidikan)
        ->setCellValue('BG' . $activeRow, $item->wali_pekerjaan)
        ->setCellValue('BH' . $activeRow, (!empty($item->wali_telepon)) ? $item->wali_telepon : $item->wali_handphone);

      $activeRow++;
    };

    $styleArray = array(
      'borders' => array(
        'allBorders' => array(
          'style' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
          // 'color' => array('argb' => '000000'),
        ),
      ),
    );

    $spreadsheet->getActiveSheet()->getStyle('A1:' .
      $spreadsheet->getActiveSheet()->getHighestColumn() .
      $spreadsheet->getActiveSheet()->getHighestRow())->applyFromArray($styleArray);

    $writer = new Xlsx($spreadsheet);

    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="Rekap-Calon_Santri_Tahun_' . $tahun . '_' . $statusName . '_' . $gender . '.xlsx"');
    header('Cache-Control: max-age=0');

    $writer->save('php://output');
  }
  // END ## Calon Santri

  public function ajax_get_santri()
  {
    $user_id = $this->session->userdata('user')['id'];
    $user_role = $this->session->userdata('user')['role'];
    $filter_kelas = $this->input->post('kelas');
    $santriList = array();
    $static = '<option value="all">Semua Santri</option>';

    if (!empty($filter_kelas)) {
      $filter_kelas_ex = explode('#', $filter_kelas);
      $kelas = (isset($filter_kelas_ex[0])) ? $filter_kelas_ex[0] : null;
      $sub_kelas = (isset($filter_kelas_ex[1])) ? $filter_kelas_ex[1] : null;

      if ($user_role === 'Orang Tua') {
        $santriList = $this->MappingOrtuModel->getSantriIdFromOrtu($user_id);
        $static = null;
      };

      $santri = $this->SantriModel->getAll(['kelas' => $kelas, 'sub_kelas' => $sub_kelas], $santriList);
      $options = $this->init_list($santri, 'id', 'nama_lengkap', 'all', $static);

      echo json_encode($options);
    } else {
      echo json_encode(array());
    };
  }

  private function _activityConcated($santri, $activity)
  {
    $result = [];

    foreach ($santri as $key => $santri) {
      $is_exist = $this->searchInArrayObj($activity, 'nisn', $santri->nisn);

      if (count($is_exist) > 0) {
        $result[] = $is_exist;
      } else {
        $result[] = (object) [
          'santri_id' => $santri->id,
          'nisn' => $santri->nisn,
          'nama_lengkap' => $santri->nama_lengkap,
          'kelas' => $santri->kelas . ' ' . $santri->sub_kelas,
          'created_at' => null,
          'sabtu_shubuh' => 0,
          'sabtu_dzuhur' => 0,
          'sabtu_ashar' => 0,
          'sabtu_magrib' => 0,
          'sabtu_isya' => 0,
          'minggu_shubuh' => 0,
          'minggu_dzuhur' => 0,
          'minggu_ashar' => 0,
          'minggu_magrib' => 0,
          'minggu_isya' => 0,
          'senin_shubuh' => 0,
          'senin_dzuhur' => 0,
          'senin_ashar' => 0,
          'senin_magrib' => 0,
          'senin_isya' => 0,
          'selasa_shubuh' => 0,
          'selasa_dzuhur' => 0,
          'selasa_ashar' => 0,
          'selasa_magrib' => 0,
          'selasa_isya' => 0,
          'rabu_shubuh' => 0,
          'rabu_dzuhur' => 0,
          'rabu_ashar' => 0,
          'rabu_magrib' => 0,
          'rabu_isya' => 0,
          'kamis_shubuh' => 0,
          'kamis_dzuhur' => 0,
          'kamis_ashar' => 0,
          'kamis_magrib' => 0,
          'kamis_isya' => 0,
          'jumat_shubuh' => 0,
          'jumat_dzuhur' => 0,
          'jumat_ashar' => 0,
          'jumat_magrib' => 0,
          'jumat_isya' => 0
        ];
      };
    };

    return $result;
  }
}
