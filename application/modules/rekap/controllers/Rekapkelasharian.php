<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . 'controllers/AppBackend.php');

class Rekapkelasharian extends AppBackend
{
  function __construct()
  {
    parent::__construct();
    $this->load->model([
      'AppModel',
      'JadwalPelajaranItemModel',
      'SantriModel',
      'AbsenHarianModel',
    ]);
    $this->load->library('form_validation');
  }

  public function index()
  {
    $list_kelas = $this->init_list_kelas();
    $data = array(
      'app' => $this->app(),
      'main_js' => $this->load_main_js('rekap/views/kelasharian/main.js.php', true),
      'card_title' => 'Rekap › Absen Kelas',
      'list_kelas' => $list_kelas,
    );
    $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
    $this->template->load_view('kelasharian/index', $data, TRUE);
    $this->template->render();
  }

  public function ajax_get_compact()
  {
    $this->handle_ajax_request();

    $filterKelas = $this->input->post('kelas');
    $filterStartDate = $this->input->post('start_date');
    $filterEndDate = $this->input->post('end_date');

    $this->generate_pivot_compact($filterKelas, $filterStartDate, $filterEndDate);
  }

  public function ajax_get_month()
  {
    $this->handle_ajax_request();

    $filterKelas = $this->input->post('kelas');
    $filterYear = $this->input->post('year');
    $filterMonth = $this->input->post('month');

    $this->generate_pivot_month($filterKelas, $filterYear, $filterMonth);
  }

  public function export_excel_compact()
  {
    $param = $this->input->get('param');
    $filterKelas = null;
    $filterStartDate = null;
    $filterEndDate = null;

    // Extract params
    foreach (explode('&', $param) as $chunk) {
      $param = explode('=', $chunk);

      if ($param) {
        $filterKelas = (urldecode($param[0]) === 'kelas') ? urldecode($param[1]) : $filterKelas;
        $filterStartDate = (urldecode($param[0]) === 'start_date') ? urldecode($param[1]) : $filterStartDate;
        $filterEndDate = (urldecode($param[0]) === 'end_date') ? urldecode($param[1]) : $filterEndDate;
      };
    };

    $generateView = $this->generate_pivot_compact($filterKelas, $filterStartDate, $filterEndDate, true, true);
    $excelChar = $this->excelChar();
    $headerCellCount = $excelChar[(int) $generateView['header_cell_count'] - 1];
    $content = $generateView['content'];

    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Html();
    $spreadsheet = $reader->loadFromString($content);

    // Set style
    $spreadsheet->getActiveSheet()->mergeCells('A1:' . $headerCellCount . '1');
    $spreadsheet->getActiveSheet()->getStyle('A2:' . $headerCellCount . '4')->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
    $spreadsheet->getActiveSheet()->getStyle('A1:' . $headerCellCount . '4')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
    $spreadsheet->getActiveSheet()->getStyle('A1:' . $headerCellCount . '4')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
    $spreadsheet->getActiveSheet()->getStyle('A5:A300')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
    $spreadsheet->getActiveSheet()->getStyle('A5:A300')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
    $spreadsheet->getActiveSheet()->getStyle('C5:' . $headerCellCount . '300')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
    $spreadsheet->getActiveSheet()->getStyle('C5:' . $headerCellCount . '300')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
    $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(5);
    $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(35);
    $spreadsheet->getActiveSheet()->getRowDimension(1)->setRowHeight(40);

    $outputFileName = 'rekap-absen_kelas-' . $filterKelas . '-' . strtotime(date('YmdHis')) . '.xlsx';

    // Output stream
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="' . $outputFileName . '"');
    header('Cache-Control: max-age=0');
    header('Cache-Control: max-age=1');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
    header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header('Pragma: public'); // HTTP/1.0

    $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
    $writer->save('php://output');
    exit;
  }

  public function export_excel_month()
  {
    $param = $this->input->get('param');
    $filterKelas = null;
    $filterYear = null;
    $filterMonth = null;

    // Extract params
    foreach (explode('&', $param) as $chunk) {
      $param = explode('=', $chunk);

      if ($param) {
        $filterKelas = (urldecode($param[0]) === 'kelas') ? urldecode($param[1]) : $filterKelas;
        $filterYear = (urldecode($param[0]) === 'year') ? urldecode($param[1]) : $filterYear;
        $filterMonth = (urldecode($param[0]) === 'month') ? urldecode($param[1]) : $filterMonth;
      };
    };

    $generateView = $this->generate_pivot_month($filterKelas, $filterYear, $filterMonth, true, true);

    $excelChar = $this->excelChar();
    $headerCellCount = $excelChar[(int) $generateView['header_cell_count'] + 6];
    $headerCellCountContent = $excelChar[(int) $generateView['header_cell_count'] + 1];
    $content = $generateView['content'];

    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Html();
    $spreadsheet = $reader->loadFromString($content);

    // Set style
    $spreadsheet->getActiveSheet()->mergeCells('A1:' . $headerCellCount . '1');
    $spreadsheet->getActiveSheet()->getStyle('A2:' . $headerCellCount . '3')->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
    $spreadsheet->getActiveSheet()->getStyle('A1:' . $headerCellCount . '3')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
    $spreadsheet->getActiveSheet()->getStyle('A1:' . $headerCellCount . '3')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
    $spreadsheet->getActiveSheet()->getStyle('A4:A300')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
    $spreadsheet->getActiveSheet()->getStyle('A4:A300')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
    $spreadsheet->getActiveSheet()->getStyle('C4:' . $headerCellCount . '300')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
    $spreadsheet->getActiveSheet()->getStyle('C4:' . $headerCellCount . '300')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
    $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(5);
    $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(35);
    $spreadsheet->getActiveSheet()->getRowDimension(1)->setRowHeight(40);

    $outputFileName = 'rekap-absen_kelas-' . $filterKelas . '-' . $filterYear . '_' . $filterMonth . '-' . strtotime(date('YmdHis')) . '.xlsx';

    // Output stream
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="' . $outputFileName . '"');
    header('Cache-Control: max-age=0');
    header('Cache-Control: max-age=1');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
    header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header('Pragma: public'); // HTTP/1.0

    $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
    $writer->save('php://output');
    exit;
  }

  private function generate_pivot_compact($filterKelas, $filterStartDate, $filterEndDate, $returnAsData = false, $isExport = false)
  {
    if (!empty($filterKelas) && !empty($filterStartDate) && !empty($filterEndDate)) {
      $filterKelasExplode = explode('#', $filterKelas);
      $filterKelas_name = null;
      $filterKelas_subName = null;

      if (count($filterKelasExplode) === 2) {
        $filterKelas_name = $filterKelasExplode[0];
        $filterKelas_subName = $filterKelasExplode[1];
      };

      $begin = new DateTime($filterStartDate);
      $end = new DateTime($filterEndDate);
      $end = $end->modify('+1 day');
      $interval = DateInterval::createFromDateString('1 day');
      $period = new DatePeriod($begin, $interval, $end);
      $dateRange = array();
      $headerCellCount = 0;

      // Get santri
      $santriList = $this->SantriModel->getAll(array(
        'kelas' => $filterKelas_name,
        'sub_kelas' => $filterKelas_subName
      ));

      if (count($santriList) > 0) {
        foreach ($period as $item) {
          $dayName = $this->get_day($item->format('D'));

          if ($dayName !== 'Jumat') {
            // Get mapel
            $mapelList = $this->JadwalPelajaranItemModel->getAll(array(
              'jadwal_pelajaran_kelas' => '["' . $filterKelas . '"]',
              'hari' => $dayName
            ));
            $mapelTemp = array();

            if (count($mapelList) > 0) {
              foreach ($mapelList as $indexMapel => $mapel) {
                $santriAbsen = array();
                $headerCellCount = $headerCellCount + 1;

                // Get santri nilai
                foreach ($santriList as $indexSantri => $santri) {
                  $absenHarian = $this->AbsenHarianModel->getAll_rekap($filterKelas, $santri->id, $mapel->mata_pelajaran_id, $item->format('Y-m-d'));
                  $absenHarian_temp = (object) array(
                    'santri_id' => $santri->id,
                    'status' => null,
                    'catatan' => null,
                  );

                  if (!is_null($absenHarian)) {
                    $absenHarian_temp->status = $absenHarian->status;
                    $absenHarian_temp->catatan = $absenHarian->catatan;
                  };

                  $santriAbsen[$santri->id] = $absenHarian_temp;
                };

                $mapelTemp[] = array(
                  'id' => $mapel->id,
                  'mata_pelajaran_id' => $mapel->mata_pelajaran_id,
                  'mata_pelajaran_kode' => $mapel->kode,
                  'mata_pelajaran_jenis' => $mapel->mata_pelajaran_jenis,
                  'mata_pelajaran_nama' => $mapel->mata_pelajaran_nama,
                  'santri_absen' => $santriAbsen
                );
              };
            } else {
              // Default
              $santriAbsen = array();
              $headerCellCount = $headerCellCount + 1;

              foreach ($santriList as $indexSantri => $santri) {
                $santriAbsen[$santri->id] = (object) array(
                  'santri_id' => $santri->id,
                  'status' => null,
                  'catatan' => null,
                );
              };

              $mapelTemp[] = array(
                'id' => null,
                'mata_pelajaran_id' => null,
                'mata_pelajaran_kode' => null,
                'mata_pelajaran_jenis' => null,
                'mata_pelajaran_nama' => null,
                'santri_absen' => $santriAbsen
              );
            };

            $dateRange[] = (object) array(
              'day_name' => $dayName,
              'date' => $item->format('d/m/y'),
              'mapel' => $mapelTemp,
              'mapel_count' => count($mapelList),
            );
          };
        };

        $headerCellCount = $headerCellCount + 6;
        $urlParam = '?ref=cxsmi&kelas=' . str_replace('#', '#', $filterKelas) . '&start_date=' . $filterStartDate . '&end_date=' . $filterEndDate;
        $urlParam = urlencode($urlParam);

        $loadContent = $this->load->view('kelasharian/mingguan', array(
          'controller' => $this,
          'filter_kelas' => $filterKelas,
          'filter_kelas_name' => $filterKelas_name,
          'filter_kelas_subName' => $filterKelas_subName,
          'start_date' => $filterStartDate,
          'end_date' => $filterEndDate,
          'date_range' => $dateRange,
          'santri' => $santriList,
          'is_export' => $isExport,
          'url_param' => $urlParam
        ), $returnAsData);

        if ($isExport === true) {
          return array(
            'header_cell_count' => $headerCellCount,
            'content' => $loadContent
          );
        } else {
          return $loadContent;
        };
      } else {
        echo '404';
      };
    } else {
      echo '404';
    };
  }

  private function generate_pivot_month($filterKelas, $filterYear, $filterMonth, $returnAsData = false, $isExport = false)
  {
    if (!empty($filterKelas) && !empty($filterYear) && !empty($filterMonth)) {
      $startDay = 1;
      $endDay = cal_days_in_month(0, $filterMonth, $filterYear);
      $days = array();
      $monthName = $this->get_month($filterMonth);

      // Collect day of month
      for ($day = $startDay; $day <= $endDay; $day++) {
        $days[] = $day;
      };

      // Extract kelas
      $filterKelasExplode = explode('#', $filterKelas);
      $filterKelas_name = null;
      $filterKelas_subName = null;

      if (count($filterKelasExplode) === 2) {
        $filterKelas_name = $filterKelasExplode[0];
        $filterKelas_subName = $filterKelasExplode[1];
      };

      $payload = array();
      $headerCellCount = 0;

      // Get santri
      $santriList = $this->SantriModel->getAll(array(
        'kelas' => $filterKelas_name,
        'sub_kelas' => $filterKelas_subName
      ));

      if (count($santriList) > 0) {
        foreach ($days as $day) {
          $currentDate = $filterYear . '-' . $filterMonth . '-' . $day;
          $currentDate = date('Y-m-d', strtotime($currentDate));
          $dayName = date('D', strtotime($currentDate));
          $dayName = $this->get_day($dayName);

          $santriAbsen = array();
          $headerCellCount = $headerCellCount + 1;

          // Get santri nilai
          foreach ($santriList as $indexSantri => $santri) {
            $absenHarian = $this->AbsenHarianModel->getAll_rekapByDate($filterKelas, $santri->id, $currentDate);
            $absenHarian_temp = (object) array(
              'santri_id' => $santri->id,
              'jumlah_sesi' => 0,
              'jumlah_hadir' => 0,
              'jumlah_izin' => 0,
              'jumlah_sakit' => 0,
              'jumlah_alpa' => 0,
              'jumlah_mapel' => 0
            );

            if (!is_null($absenHarian)) {
              $absenHarian_temp->jumlah_sesi = $absenHarian->jumlah_sesi;
              $absenHarian_temp->jumlah_hadir = $absenHarian->jumlah_hadir;
              $absenHarian_temp->jumlah_izin = $absenHarian->jumlah_izin;
              $absenHarian_temp->jumlah_sakit = $absenHarian->jumlah_sakit;
              $absenHarian_temp->jumlah_alpa = $absenHarian->jumlah_alpa;
              $absenHarian_temp->jumlah_mapel = $absenHarian->jumlah_mapel;
            };

            $santriAbsen[$santri->id] = $absenHarian_temp;
          };

          $payload[] = (object) array(
            'day_name' => $dayName,
            'day' => $day,
            'date' => $currentDate,
            'santri_absen' => $santriAbsen
          );
        };

        $urlParam = '?ref=cxsmi&kelas=' . str_replace('#', '#', $filterKelas) . '&year=' . $filterYear . '&month=' . $filterMonth;
        $urlParam = urlencode($urlParam);

        $loadContent = $this->load->view('kelasharian/bulanan', array(
          'controller' => $this,
          'filter_kelas' => $filterKelas,
          'filter_kelas_name' => $filterKelas_name,
          'filter_kelas_subName' => $filterKelas_subName,
          'year' => $filterYear,
          'month' => $filterMonth,
          'month_name' => $monthName,
          'payload' => $payload,
          'santri' => $santriList,
          'day_count' => count($days),
          'is_export' => $isExport,
          'url_param' => $urlParam
        ), $returnAsData);

        if ($isExport === true) {
          return array(
            'header_cell_count' => $headerCellCount,
            'content' => $loadContent
          );
        } else {
          return $loadContent;
        };
      } else {
        echo '404';
      };
    } else {
      echo '404';
    };
  }
}
