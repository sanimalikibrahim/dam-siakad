<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . 'controllers/AppBackend.php');

class Rekapseragam extends AppBackend
{
  function __construct()
  {
    parent::__construct();
    $this->load->model([
      'AppModel',
      'PsbModel',
    ]);
    $this->load->library('form_validation');
  }

  public function index()
  {
    $tahunList = $this->init_list($this->PsbModel->getTahunList(), 'tahun', 'tahun');

    $data = array(
      'app' => $this->app(),
      'main_js' => $this->load_main_js('rekap/views/seragam/main.js.php', true),
      'card_title' => 'Rekap › Seragam',
      'list_tahun' => $tahunList
    );
    $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
    $this->template->load_view('seragam/index', $data, TRUE);
    $this->template->render();
  }

  public function ajax_get($tahun = null)
  {
    $this->handle_ajax_request();

    try {
      if (!is_null($tahun) && !empty($tahun) && $tahun !== 'null') {
        $model = $this->PsbModel->getAll(['status' => 4, 'tahun' => $tahun, 'ukuran_baju IS NOT NULL' => NULL, 'ukuran_celana IS NOT NULL' => NULL]);
        $model2 = $this->PsbModel->getAll(['status' => 4, 'tahun' => $tahun, 'ukuran_baju IS NULL' => NULL, 'ukuran_celana IS NULL' => NULL]);
        $response = array(
          'status' => true,
          'data' => count($model),
          'data2' => count($model2)
        );
      } else {
        $response = array('status' => false, 'data' => 'Tahun is required.');
      }
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to get the data, try again later.');
    };

    echo json_encode($response);
  }

  public function export_excel()
  {
    $tahun = $this->input->get('tahun');

    if (!is_null($tahun)) {
      $startAttributeRow = 6;
      $startDataRow = 7;
      $fileName = 'rekap_seragam_santri_baru_' . $tahun . '-' . date('YmdHis') . '.xlsx';
      $payload = array();
      $tahunAjaran = $tahun . '/' . ((int) $tahun + 1);

      $fileTemplate = FCPATH . 'directory/templates/template-santri_baru_seragam.xlsx';
      $model = $this->PsbModel->getAll(['status' => 4, 'tahun' => $tahun]);

      if (count($model) > 0) {
        // Modified payload
        foreach ($model as $index => $item) {
          $payload[$index] = $item;

          if (strtolower($item->jenis_kelamin) === 'laki-laki') {
            $payload[$index]->ukuran_baju_putra = $item->ukuran_baju;
            $payload[$index]->ukuran_celana_putra = $item->ukuran_celana;
            $payload[$index]->ukuran_baju_putri = null;
            $payload[$index]->ukuran_celana_putri = null;
          } else {
            $payload[$index]->ukuran_baju_putra = null;
            $payload[$index]->ukuran_celana_putra = null;
            $payload[$index]->ukuran_baju_putri = $item->ukuran_baju;
            $payload[$index]->ukuran_celana_putri = $item->ukuran_celana;
          };
        };

        $inject  = '$a3 = $sheet->getCell("A3")->getFormattedValue();';
        $inject .= '$a3 = str_replace(\'${tahun_ajaran}\', "' . $tahunAjaran . '", $a3);'; // Tahun Ajaran
        $inject .= '$sheet->setCellValue("A3", $a3);';

        $this->generateExcelByTemplate($fileTemplate, $startAttributeRow, $startDataRow, $payload, $fileName, $inject);
      } else {
        echo 'Tidak ditemukan data.';
      };
    } else {
      echo 'Terjadi kesalahan ketika membuat file.';
    };
  }
}
