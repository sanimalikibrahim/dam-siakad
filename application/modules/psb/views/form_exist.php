<style type="text/css">
  .cx__form-control {
    width: 100%;
    padding: 0.375 rem 0;
    font-size: 1rem;
    line-height: 1.5;
    color: #495057;
    border-bottom: 1px solid #eceff1;
  }
</style>

<div class="modal fade" id="modal-form-exist" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="spinner">
        <div class="lds-hourglass"></div>
      </div>
      <div class="modal-body">
        <div class="alert alert-info">
          <h4 class="alert-heading">NISN Sudah Terdaftar!</h4>
          <p class="m-0">Jika data dibawah ini sesuai dengan identitas ananda silahkan klik <b>Ini Saya</b>, namun jika bukan ananda silahkan klik <b>Bukan Saya</b>.</p>
        </div>
        <!-- DATA CALON SANTRI -->
        <div class="row">
          <div class="col-xs-12 col-lg-3">
            <img class="img-thumbnail mb-4 psb-exist-foto" src="#!" alt="Foto Calon Santri" />
          </div>
          <div class="col-xs-12 col-lg-9">
            <div class="form-group mb-3">
              <label>Nama Lengkap</label>
              <div class="cx__form-control psb-exist-nama_lengkap">-</div>
            </div>
            <div class="form-group mb-3">
              <label>Nomor Induk Siswa Nasional (NISN)</label>
              <div class="cx__form-control psb-exist-nisn">-</div>
            </div>
            <div class="form-group mb-3">
              <label>Tempat, Tanggal Lahir</label>
              <div class="cx__form-control psb-exist-ttgl">-</div>
            </div>
            <div class="form-group mb-3">
              <label>Jenis Kelamin</label>
              <div class="cx__form-control psb-exist-jenis_kelamin">-</div>
            </div>
            <div class="form-group mb-3">
              <label>Telepon</label>
              <div class="cx__form-control psb-exist-telepon">-</div>
            </div>
            <div class="form-group mb-3">
              <label>Alamat Tempat Tinggal</label>
              <div class="cx__form-control psb-exist-alamat_santri">-</div>
            </div>
            <div class="form-group mb-3">
              <label>Kota / Kabupaten</label>
              <div class="cx__form-control psb-exist-kota">-</div>
            </div>
            <div class="form-group mb-0">
              <label>Provinsi</label>
              <div class="cx__form-control psb-exist-provinsi">-</div>
            </div>
          </div>
        </div>
        <!-- END ## DATA CALON SANTRI -->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success btn--icon-text psb-action-exist-next">
          <i class="zmdi zmdi-save"></i> Ini Saya
        </button>
        <button type="button" class="btn btn-light btn--icon-text psb-action-exist-cancel" data-dismiss="modal">
          Bukan Saya
        </button>
      </div>
    </div>
  </div>
</div>