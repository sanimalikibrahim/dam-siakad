<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title><?php echo $page_title ?></title>
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Favicons -->
  <link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url('themes/_public/') ?>img/favicon/apple-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url('themes/_public/') ?>img/favicon/apple-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url('themes/_public/') ?>img/favicon/apple-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url('themes/_public/') ?>img/favicon/apple-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url('themes/_public/') ?>img/favicon/apple-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url('themes/_public/') ?>img/favicon/apple-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url('themes/_public/') ?>img/favicon/apple-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url('themes/_public/') ?>img/favicon/apple-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url('themes/_public/') ?>img/favicon/apple-icon-180x180.png">
  <link rel="icon" type="image/png" sizes="192x192" href="i<?php echo base_url('themes/_public/') ?>mg/favicon/android-icon-192x192.png">
  <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url('themes/_public/') ?>img/favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url('themes/_public/') ?>img/favicon/favicon-96x96.png">
  <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('themes/_public/') ?>img/favicon/favicon-16x16.png">
  <link rel="manifest" href="<?php echo base_url('themes/_public/') ?>img/favicon/manifest.json">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-TileImage" content="<?php echo base_url('themes/_public/') ?>img/favicon/ms-icon-144x144.png">
  <meta name="theme-color" content="#ffffff">

  <!-- Vendor styles -->
  <link rel="stylesheet" href="<?php echo base_url('themes/material_admin/vendors/material-design-iconic-font/css/material-design-iconic-font.min.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('themes/material_admin/vendors/animate.css/animate.min.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('themes/material_admin/vendors/select2/css/select2.min.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('themes/material_admin/vendors/flatpickr/flatpickr.min.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('themes/material_admin/vendors/sweetalert2/sweetalert2.min.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('themes/_public/vendors/responsive-tabs/css/responsive-tabs.css') ?>" />

  <!-- App styles -->
  <link rel="stylesheet" href="<?php echo base_url('themes/material_admin/css/app.min.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('themes/_public/css/material-effect.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('themes/_public/css/public.main.css') ?>">

  <style type="text/css">
    html {
      font-size: 15px;
    }

    .login__block {
      text-align: left;
      max-width: 900px;
      min-height: 100vh;
      margin: 1rem;
      line-height: 150%;
    }

    .login__block__header {
      text-align: center;
    }

    .clear {
      height: 1.2rem;
    }

    .page-title {
      font-size: 14pt;
    }

    .hidden {
      display: none;
    }

    #form-psb {
      text-align: center;
      position: relative;
      margin-top: 20px;
    }

    #form-psb fieldset {
      background: white;
      border: 0 none;
      border-radius: 0.5rem;
      box-sizing: border-box;
      width: 100%;
      margin: 0;
      padding-bottom: 20px;
      position: relative;
    }

    #form-psb fieldset:not(:first-of-type) {
      display: none
    }

    #form-psb fieldset .form-card {
      text-align: left;
      color: #9E9E9E
    }

    #form-psb .action-button {
      width: 100px;
      background: skyblue;
      font-weight: bold;
      color: white;
      border: 0 none;
      border-radius: 0px;
      cursor: pointer;
      padding: 10px 5px;
      margin: 10px 5px
    }

    #form-psb .action-button:hover,
    #form-psb .action-button:focus {
      box-shadow: 0 0 0 2px white, 0 0 0 3px skyblue
    }

    #form-psb .action-button-previous {
      width: 100px;
      background: #616161;
      font-weight: bold;
      color: white;
      border: 0 none;
      border-radius: 0px;
      cursor: pointer;
      padding: 10px 5px;
      margin: 10px 5px
    }

    #form-psb .action-button-previous:hover,
    #form-psb .action-button-previous:focus {
      box-shadow: 0 0 0 2px white, 0 0 0 3px #616161
    }

    select.list-dt {
      border: none;
      outline: 0;
      border-bottom: 1px solid #ccc;
      padding: 2px 5px 3px 5px;
      margin: 2px
    }

    select.list-dt:focus {
      border-bottom: 2px solid skyblue
    }

    .fs-title {
      font-size: 25px;
      color: #2C3E50;
      margin-bottom: 10px;
      font-weight: bold;
      text-align: left
    }

    #progressbar {
      margin-bottom: 30px;
      overflow: hidden;
      color: lightgrey
    }

    #progressbar .active {
      color: #444;
    }

    #progressbar li {
      list-style-type: none;
      font-size: 12px;
      width: 20%;
      float: left;
      position: relative;
      text-align: center;
    }

    #progressbar #data-akun:before,
    #data-santri:before,
    #data-orang-tua:before,
    #data-prestasi:before,
    #data-komitmen:before {
      font-family: 'Material-Design-Iconic-Font';
      content: '\f107';
    }

    #progressbar #data-akun:before {
      content: '\f183';
    }

    #progressbar #data-santri:before {
      content: '\f207';
    }

    #progressbar #data-orang-tua:before {
      content: '\f20d';
    }

    #progressbar #data-prestasi:before {
      content: '\f27d';
    }

    #progressbar #data-komitmen:before {
      content: '\f1de';
    }

    #progressbar li:before {
      width: 50px;
      height: 50px;
      line-height: 45px;
      display: block;
      font-size: 18px;
      color: #ffffff;
      background: lightgray;
      border-radius: 50%;
      margin: 0 auto 10px auto;
      padding: 2px;
      -webkit-transition: all 0.3s ease;
      -moz-transition: all 0.3s ease;
      -o-transition: all 0.3s ease;
      transition: all 0.3s ease;
    }

    #progressbar li:after {
      content: '';
      width: 100%;
      height: 2px;
      background: lightgray;
      position: absolute;
      left: 0;
      top: 25px;
      z-index: -1;
      -webkit-transition: all 0.3s ease;
      -moz-transition: all 0.3s ease;
      -o-transition: all 0.3s ease;
      transition: all 0.3s ease;
    }

    #progressbar li.active:before,
    #progressbar li.active:after {
      background: #2196F3;
      -webkit-transition: all 0.3s ease;
      -moz-transition: all 0.3s ease;
      -o-transition: all 0.3s ease;
      transition: all 0.3s ease;
    }

    .pull-right {
      float: right;
    }

    .floating-action {
      position: fixed;
      width: 100%;
      background: transparent;
      left: 0;
      bottom: 0;
      z-index: 9999;
    }

    .floating-action-body {
      max-width: 900px;
      padding: 20px 30px;
      background: #fff;
      margin: 0 auto;
      box-shadow: 0px 1px 30px rgba(0, 0, 0, 0.1);
      text-align: center;
      height: 75px;
    }

    .floating-action-body a {
      width: 135px;
      margin: 0 2px;
      border-radius: 24px;
    }

    .opacity-0 {
      opacity: 0 !important;
    }

    .panel-bordered {
      border: 1px solid #eee;
    }

    .panel-bordered .panel-bordered-header {
      background: #f9f9f9;
      padding: 15px;
      font-size: 12pt;
      font-weight: 500;
      border-bottom: 1px solid #eee;
    }

    .panel-bordered .panel-bordered-body {
      background: #fff;
      padding: 15px;
    }

    .text-bold {
      font-weight: bold;
    }

    .alert-warning {
      background-color: var(--orange);
    }
  </style>
</head>

<body data-ma-theme="<?php echo $app->theme_color ?>">
  <!-- Loading Indicator -->
  <div class="spinner" style="margin-top: -20px; position: fixed; flex-direction: column; justify-content: center; align-items: center;">
    <h3 style="color: var(--white);">Please wait</h3>
    <div class="lds-hourglass"></div>
  </div>
  <!-- END ## Loading Indicator -->

  <?php include_once('form_exist.php') ?>

  <form id="form-psb" autocomplete="off">
    <!-- CSRF -->
    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />

    <div class="login">
      <div class="login__block active">
        <div class="login__block__header">
          <img src="<?php echo base_url('themes/_public/img/logo/logo.png') ?>" />
          <span style="font-weight: 500;">Darul Arqam Muhammadiyah Garut</span>
          <br />
          <small>Pendaftaran Santri Baru Tahun Ajaran <?= $periode ?></small>
        </div>

        <div class="clear"></div>

        <?php if ((int) $app->psb_status === 1) : ?>
          <?php if (!is_null($persyaratan_daftar_ulang) && !is_null($komitmen)) : ?>
            <div class="psb-form-wrapper" style="display: block;">
              <div class="login__block__body">
                <div class="psb-input-wrapper">
                  <div class="summary mb-4">
                    <!-- <h1 class="card-title mb-3">Biodata Santri Baru</h1> -->
                    <div class="alert alert-warning">
                      Silahkan isi data-data berikut dengan data yang sebenar-benarnya, identitas calon santri harus sesuai dengan Akte Kelahiran dan/atau Ijazah SD/MI/Sederajat yang dimiliki.
                    </div>
                  </div>

                  <!-- Wizard Form -->
                  <!-- progressbar -->
                  <ul id="progressbar" class="p-0 <?= ($app->is_mobile) ? 'hidden' : '' ?>">
                    <li id="data-akun" class="active"><strong>Akun</strong></li>
                    <li id="data-santri"><strong>Personal</strong></li>
                    <li id="data-orang-tua"><strong>Orang Tua</strong></li>
                    <li id="data-prestasi"><strong>Prestasi</strong></li>
                    <li id="data-komitmen"><strong>Komitmen</strong></li>
                  </ul>
                  <?php include_once('form_akun.php') ?>
                  <?php include_once('form_santri.php') ?>
                  <?php include_once('form_orang_tua.php') ?>
                  <?php include_once('form_prestasi.php') ?>
                  <?php include_once('form_komitmen.php') ?>
                  <!-- END ## Wizard Form -->
                </div>
                <div class="psb-response-dom hidden"></div>
              </div>
            </div>
          <?php else : ?>
            <div class="text-center mt-5 mb-5">
              <p>
                <b>Terjadi kesalahan ketika mengambil data</b> <br />
                Coba lagi nanti atau hubungi Administrator untuk memberitahu kesalahan.
              </p>
              <small>
                No content to load. (Error code: 404) <br />
                <?= md5('PSB#POST/T/READ/{1}&#POST/T/READ/{2}') ?>
              </small>
              <hr />
              <small>Page rendered in {elapsed_time} seconds.</small>
            </div>
          <?php endif ?>
        <?php else : ?>
          <div class="text-center mt-5 mb-5">
            <p>
              <b>Pendaftaran santri baru telah ditutup</b> <br />
              Sudah memiliki akun? <a href="<?= base_url('login') ?>">Klik Disini</a> untuk masuk ke aplikasi.
            </p>
            <hr />
            <small>Page rendered in {elapsed_time} seconds.</small>
          </div>
        <?php endif ?>

        <!-- Success message -->
        <div class="psb-message" style="display: none;">
          <div class="alert alert-success" role="alert">
            <h4 class="alert-heading">Selesai!</h4>
            Email has been sent to "<span class='psb-message-email'>{email}</span>", please check your inbox or spam for confirmation account.
          </div>

          <div style="text-align: center; margin-top: 2rem; margin-bottom: 2rem;">
            <a href="<?= base_url('login') ?>" class="btn btn-primary">Login to your account</a>
          </div>
        </div>
      </div>
    </div>
  </form>

  <?php if (((int) $app->psb_status === 1) && !is_null($persyaratan_daftar_ulang) && !is_null($komitmen)) : ?>
    <!-- Floating button -->
    <div class="floating-action">
      <div class="floating-action-body">
        <div class="floating-buttons">
          <a href="javascript:;" class="btn btn--raised btn-dark floating-previous hidden">
            <i class="zmdi zmdi-arrow-left"></i> Sebelumnya
          </a>
          <a href="javascript:;" class="btn btn--raised btn-primary floating-next">
            Lanjut <i class="zmdi zmdi-arrow-right"></i>
          </a>
        </div>
      </div>
    </div>
  <?php endif ?>

  <!-- Javascript -->
  <script src="<?php echo base_url('themes/material_admin/vendors/jquery/jquery.min.js') ?>"></script>
  <script src="<?php echo base_url('themes/material_admin/vendors/popper.js/popper.min.js') ?>"></script>
  <script src="<?php echo base_url('themes/material_admin/vendors/bootstrap/js/bootstrap.min.js') ?>"></script>
  <script src="<?php echo base_url('themes/material_admin/vendors/bootstrap-notify/bootstrap-notify.min.js') ?>"></script>
  <script src="<?php echo base_url('themes/material_admin/vendors/select2/js/select2.full.min.js') ?>"></script>
  <script src="<?php echo base_url('themes/material_admin/vendors/tinymce/tinymce.min.js') ?>"></script>
  <script src="<?php echo base_url('themes/material_admin/vendors/jquery-mask-plugin/jquery.mask.min.js') ?>"></script>
  <script src="<?php echo base_url('themes/material_admin/vendors/flatpickr/flatpickr.min.js') ?>"></script>
  <script src="<?php echo base_url('themes/material_admin/vendors/sweetalert2/sweetalert2.min.js') ?>"></script>
  <script src="<?php echo base_url('themes/_public/vendors/responsive-tabs/js/responsive-tabs.js') ?>"></script>

  <!-- App functions and actions -->
  <script src="<?php echo base_url('themes/material_admin/js/app.min.js') ?>"></script>
  <script src="<?php echo base_url('themes/_public/js/material-effect.js') ?>"></script>
  <script src="<?php echo base_url('themes/_public/js/public.main.js') ?>"></script>

  <script type="text/javascript">
    // Handle CSRF
    $.ajaxPrefilter(function(options, originalOptions, jqXHR) {
      if (originalOptions.data instanceof FormData) {
        originalOptions.data.append("<?= $this->security->get_csrf_token_name(); ?>", "<?= $this->security->get_csrf_hash(); ?>");
      };
    });
  </script>

  <?php echo (isset($main_js)) ? $main_js : '' ?>
</body>

</html>