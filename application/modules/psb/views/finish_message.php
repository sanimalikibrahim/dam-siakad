<!-- Finish Message -->
<fieldset>
  <div class="panel-akun panel-active mb-5">
    <div class="alert alert-success mb-3 text-center">
      <i class="zmdi zmdi-check-circle"></i>
      Terima kasih, data Anda berhasil di kirim.
    </div>

    <div>
      <?php
      $content = $data->konten;
      $content = str_replace('{base_url}', base_url(), $content);

      echo $content;
      ?>
    </div>

    <div class="alert alert-light mt-4 mb-0 text-dark text-center">
      <p class="mb-2">Informasi diatas dikirim juga ke email Anda, silahkan cek pesan masuk / spam. Anda juga dapat melihat kembali didalam aplikasi.</p>
      <p class="mb-0">Untuk dapat masuk ke aplikasi, silahkan gunakan Email / NISN dan juga Password yang Anda masukan sebelumnya.</p>
      <p class="mb-0">
        <a href="<?= base_url('login') ?>">Klik disini untuk masuk</a>
      </p>
    </div>
  </div>
</fieldset>