<!-- Data Prestasi -->
<fieldset>
  <div class="panel-prestasi panel-active" panel-key="prestasi">
    <div class="card">
      <div class="card-header border-info">
        <h5 class="card-title">Data Prestasi</h5>
        <h6 class="card-subtitle text-muted text-black">Disertakan sertifikat / piagam sebagai bukti</h6>
      </div>
      <div class="card-body">
        <div class="alert alert-light text-dark mb-3 text-center">
          <i class="zmdi zmdi-info"></i>
          Tidak perlu diisi jika memang tidak ada sertifikat / piagam
        </div>
        <!-- Button -->
        <div class="table-action">
          <div class="buttons" style="display: flex; align-items: center;">
            <button class="btn btn-success btn--icon-text prestasi-action-add">
              <i class="zmdi zmdi-plus-circle"></i> Tambah Data
            </button>
            <div class="form-prestasi-item-count text-bold ml-3 hidden" style="color: var(--green);">
              Jumlah Prestasi: 0
            </div>
          </div>
        </div>
        <!-- Temporary Form -->
        <div class="form-prestasi"></div>
      </div>
    </div>
  </div>
  <!-- Buttons -->
  <a href="javascript:;" class="btn btn-dark previous disabled opacity-0" button-type="btn-dark">
    <i class="zmdi zmdi-arrow-left"></i> Sebelumnya
  </a>
  <a href="javascript:;" class="btn btn-primary pull-right next disabled opacity-0" button-type="btn-primary">
    Lanjut <i class="zmdi zmdi-arrow-right"></i>
  </a>
</fieldset>