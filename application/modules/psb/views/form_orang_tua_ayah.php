<!-- Data Orang Tua : Ayah -->
<!-- <div class="row">
  <div class="col-md-6 col-12">
    <div class="form-group">
      <label>Nomor Kartu Keluarga (KK)</label>
      <input type="number" name="ayah_no_kk" class="form-control mask-number psb-ayah_no_kk" maxlength="16" placeholder="-">
      <i class="form-group__bar"></i>
    </div>
  </div>
  <div class="col-md-6 col-12">
    <div class="form-group">
      <label>Nomor Induk Keluarga (NIK)</label>
      <input type="number" name="ayah_nik" class="form-control mask-number psb-ayah_nik" maxlength="16" placeholder="-">
      <i class="form-group__bar"></i>
    </div>
  </div>
</div> -->
<div class="form-group">
  <label required>Nama Ayah Kandung</label>
  <input type="text" name="ayah_nama_lengkap" class="form-control psb-ayah_nama_lengkap" placeholder="-" required>
  <i class="form-group__bar"></i>
</div>
<!-- <div class="row">
  <div class="col-md-8 col-12">
    <div class="form-group">
      <label>Tempat Lahir</label>
      <input type="text" name="ayah_tempat_lahir" class="form-control psb-ayah_tempat_lahir" placeholder="-">
      <i class="form-group__bar"></i>
    </div>
  </div>
  <div class="col-md-4 col-12">
    <div class="form-group">
      <label>Tanggal Lahir</label>
      <input type="text" name="ayah_tanggal_lahir" class="form-control flatpickr-date psb-ayah_tanggal_lahir" placeholder="-">
      <i class="form-group__bar"></i>
    </div>
  </div>
</div> -->
<div class="form-group">
  <label required>Status</label>
  <div class="pt-1">
    <div class="form-check form-check-inline">
      <input class="form-check-input" type="radio" name="ayah_status" id="ayah_status-hidup" value="Masih Hidup">
      <label class="form-check-label" for="ayah_status-hidup">Masih Hidup</label>
    </div>
    <div class="form-check form-check-inline">
      <input class="form-check-input" type="radio" name="ayah_status" id="ayah_status-sudah_meninggal" value="Sudah Meninggal">
      <label class="form-check-label" for="ayah_status-sudah_meninggal">Sudah Meninggal</label>
    </div>
  </div>
</div>
<!-- <div class="form-group">
  <label>Pendidikan Terakhir</label>
  <div class="select">
    <select name="ayah_pendidikan" class="form-control psb-ayah_pendidikan" data-placeholder="Select &#8595;">
      <option disabled selected>Select &#8595;</option>
      <option value="Tidak tamat SD/MI/Sederajat">Tidak tamat SD/MI/Sederajat</option>
      <option value="SD/MI/Sederajat">SD/MI/Sederajat</option>
      <option value="SMP/MTs/Sederajat">SMP/MTs/Sederajat</option>
      <option value="SMA/MA/Sederajat">SMA/MA/Sederajat</option>
      <option value="Diploma">Diploma</option>
      <option value="Sarjana (S1)">Sarjana (S1)</option>
      <option value="Magister (S2)">Magister (S2)</option>
      <option value="Doktor (S3)">Doktor (S3)</option>
    </select>
    <i class="form-group__bar"></i>
  </div>
</div> -->
<div class="form-group">
  <label required>Pekerjaan Utama</label>
  <div class="select">
    <select name="ayah_pekerjaan" class="form-control psb-ayah_pekerjaan" data-placeholder="Select &#8595;" required>
      <option disabled selected>Select &#8595;</option>
      <option value="Tidak Bekerja">Tidak Bekerja</option>
      <option value="Pensiunan">Pensiunan</option>
      <option value="Karyawan Swasta">Karyawan Swasta</option>
      <option value="TNI/POLRI">TNI/POLRI</option>
      <option value="Pengajar Honorer">Pengajar Honorer</option>
      <option value="Buruh Tani">Buruh Tani</option>
      <option value="Pengusaha">Pengusaha</option>
      <option value="Pedagang Eceran">Pedagang Eceran</option>
      <option value="Sopir/Kondektur">Sopir/Kondektur</option>
      <option value="Nelayan">Nelayan</option>
      <option value="Pekerja Pabrik">Pekerja Pabrik</option>
      <option value="Tukang Bangunan">Tukang Bangunan</option>
      <option value="PNS">PNS</option>
      <option value="Lainnya">Lainnya</option>
    </select>
    <i class="form-group__bar"></i>
  </div>
</div>
<div class="row control-psb-ayah_pekerjaan hidden">
  <div class="col-md-6 col-12">
    <div class="form-group">
      <label>Pekerjaan Utama: Jenis Profesi</label>
      <div class="select">
        <select name="ayah_pekerjaan_jenis" class="form-control psb-ayah_pekerjaan_jenis" data-placeholder="Select &#8595;">
          <option disabled selected>Select &#8595;</option>
          <option value="Pegawai Kementerian">Pegawai Kementerian</option>
          <option value="TNI/POLRI">TNI/POLRI</option>
          <option value="Guru/Dosen">Guru/Dosen</option>
          <option value="TU.Sekolah/Madrasah">TU.Sekolah/Madrasah</option>
          <option value="Pegawai Pemda">Pegawai Pemda</option>
          <option value="Dokter/Perawat">Dokter/Perawat</option>
          <option value="Lainnya">Lainnya</option>
        </select>
        <i class="form-group__bar"></i>
      </div>
    </div>
  </div>
  <div class="col-md-6 col-12">
    <div class="form-group">
      <label>Pekerjaan Utama: Pangkat/Golongan</label>
      <div class="select">
        <select name="ayah_pekerjaan_pangkat" class="form-control psb-ayah_pekerjaan_pangkat" data-placeholder="Select &#8595;">
          <option disabled selected>Select &#8595;</option>
          <option value="Golongan I/II">Golongan I/II</option>
          <option value="Golongan III">Golongan III</option>
          <option value="Golongan IV">Golongan IV</option>
          <option value="Tamtama">Tamtama</option>
          <option value="Bintara">Bintara</option>
          <option value="Perwira">Perwira</option>
        </select>
        <i class="form-group__bar"></i>
      </div>
    </div>
  </div>
</div>
<!-- <div class="form-group">
  <label>Alamat Tempat Tinggal Ayah</label>
  <input type="text" name="ayah_alamat" class="form-control psb-ayah_alamat" placeholder="-">
  <i class="form-group__bar"></i>
</div>
<div class="row">
  <div class="col-md-6 col-12">
    <div class="form-group">
      <label>Kampung/Desa/Kelurahan</label>
      <input type="text" name="ayah_kelurahan" class="form-control psb-ayah_kelurahan" placeholder="-">
      <i class="form-group__bar"></i>
    </div>
  </div>
  <div class="col-md-6 col-12">
    <div class="form-group">
      <label>Kecamatan</label>
      <input type="text" name="ayah_kecamatan" class="form-control psb-ayah_kecamatan" placeholder="-">
      <i class="form-group__bar"></i>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-6 col-12">
    <div class="form-group">
      <label>Kota/Kabupaten</label>
      <input type="text" name="ayah_kota" class="form-control psb-ayah_kota" placeholder="-">
      <i class="form-group__bar"></i>
    </div>
  </div>
  <div class="col-md-6 col-12">
    <div class="form-group">
      <label>Provinsi</label>
      <input type="text" name="ayah_provinsi" class="form-control psb-ayah_provinsi" placeholder="-">
      <i class="form-group__bar"></i>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-6 col-12">
    <div class="form-group">
      <label>Telepon</label>
      <input type="number" name="ayah_telepon" class="form-control mask-number psb-ayah_telepon" maxlength="15" placeholder="-">
      <i class="form-group__bar"></i>
    </div>
  </div>
  <div class="col-md-6 col-12">
    <div class="form-group">
      <label>Handphone</label>
      <input type="number" name="ayah_handphone" class="form-control mask-number psb-ayah_handphone" maxlength="15" placeholder="-">
      <i class="form-group__bar"></i>
    </div>
  </div>
</div> -->
<!-- <div class="form-group">
  <label>Penghasilan Ayah per Bulan</label>
  <div class="select">
    <select name="ayah_penghasilan" class="form-control psb-ayah_penghasilan" data-placeholder="Select &#8595;">
      <option disabled selected>Select &#8595;</option>
      <option value="< Rp.500.000,-">
        < Rp.500.000,-</option> <option value="Rp. 500.000,- s.d. Rp.1.000.000,-">Rp. 500.000,- s.d. Rp.1.000.000,-
      </option>
      <option value="Rp.1.000.000,- s.d. Rp.2.000.000,-">Rp.1.000.000,- s.d. Rp.2.000.000,-</option>
      <option value="Rp.2.000.000,- s.d. Rp.3.000.000,-">Rp.2.000.000,- s.d. Rp.3.000.000,-</option>
      <option value="Rp.3.000.000,- s.d. Rp.5.000.000,-">Rp.3.000.000,- s.d. Rp.5.000.000,-</option>
      <option value="> Rp.5.000.000,-">> Rp.5.000.000,-</option>
    </select>
    <i class="form-group__bar"></i>
  </div>
</div> -->