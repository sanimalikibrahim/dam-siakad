<!-- Data Akun -->
<fieldset>
  <div class="panel-akun panel-active" panel-key="akun">
    <div class="card">
      <div class="card-header border-info">
        <h5 class="card-title">Data Akun</h5>
        <h6 class="card-subtitle text-muted text-black">Digunakan untuk masuk ke aplikasi</h6>
      </div>
      <div class="card-body pb-0">
        <div class="form-group">
          <label required>Nomor Induk Siswa Nasional (NISN)</label>
          <input type="number" name="nisn" class="form-control mask-number psb-nisn" maxlength="10" placeholder="-" required>
          <i class="form-group__bar"></i>
        </div>
        <div class="form-group">
          <label required>Email</label>
          <div class="position-relative">
            <input type="email" name="email" class="form-control psb-email" placeholder="-" required>
            <i class="form-group__bar"></i>
          </div>
          <small class="form-text text-muted">
            Silakan isi dengan alamat email aktif yang nantinya dapat dipergunakan oleh calon santri
          </small>
        </div>
        <div class="row">
          <div class="col-md-6 col-12">
            <div class="form-group">
              <label required>Password</label>
              <div class="position-relative">
                <div class="input-group">
                  <input type="password" name="password" class="form-control no-padding-l psb-password" placeholder="-" autocomplete="new-password" minlength="8" required>
                  <i class="form-group__bar"></i>
                  <div class="input-group-append">
                    <span class="input-group-text">
                      <a href="javascript:;" class="visibility-password" data-input=".psb-password"></a>
                    </span>
                  </div>
                </div>
              </div>
              <small class="form-text text-muted">
                Masukan kata sandi
              </small>
            </div>
          </div>
          <div class="col-md-6 col-12">
            <div class="form-group">
              <label required>Confirm Password</label>
              <div class="position-relative">

                <div class="input-group">
                  <input type="password" name="password_confirm" class="form-control no-padding-l psb-password_confirm" placeholder="-" autocomplete="new-password" minlength="8" required>
                  <i class="form-group__bar"></i>
                  <div class="input-group-append">
                    <span class="input-group-text">
                      <a href="javascript:;" class="visibility-password" data-input=".psb-password_confirm"></a>
                    </span>
                  </div>
                </div>
              </div>
              <small class="form-text text-muted">
                Masukan ulang kata sandi
              </small>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Buttons -->
  <a href="javascript:;" class="btn btn-primary pull-right next disabled opacity-0" button-type="btn-primary">
    Lanjut <i class="zmdi zmdi-arrow-right"></i>
  </a>
</fieldset>