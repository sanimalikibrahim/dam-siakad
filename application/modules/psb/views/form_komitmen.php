<!-- Data Komitmen -->
<fieldset>
  <div class="panel-komitmen panel-active">
    <div class="card">
      <div class="card-header border-info">
        <h5 class="card-title"><?= $komitmen->judul ?></h5>
      </div>
      <div class="card-body">
        <?= $komitmen->konten ?>

        <div class="komitmen-form mt-4 p-4 bg-light" style="border-radius: 2px;">
          <div class="form-group mb-0">
            <label required><b>Komitmen Anda</b></label>
            <div class="pt-1">
              <div class="form-check form-check-inline">
                <input class="form-check-input psb-komitmen" type="radio" name="komitmen" id="komitmen-bersedia" value="Bersedia">
                <label class="form-check-label" for="komitmen-bersedia">Bersedia</label>
              </div>
              <div class="form-check form-check-inline">
                <input class="form-check-input psb-komitmen" type="radio" name="komitmen" id="komitmen-tidak_bersedia" value="Tidak Bersedia">
                <label class="form-check-label" for="komitmen-tidak_bersedia">Tidak Bersedia</label>
              </div>
            </div>
          </div>
          <div class="form-group mt-3 mb-0 control-psb-komitmen hidden">
            <label required>Nominal</label>
            <div class="position-relative">
              <div class="input-group">
                <div class="input-group-prepend" style="width: 35px;">
                  <span class="input-group-text">Rp</span>
                </div>
                <input type="text" name="komitmen_nominal" class="form-control mask-money input-white psb-komitmen_nominal" placeholder="-" required>
                <i class="form-group__bar"></i>
              </div>
            </div>
            <small class="form-text text-muted">
              Komitmen infaq & shadaqah anda
            </small>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Buttons -->
  <a href="javascript:;" class="btn btn-dark previous disabled opacity-0" button-type="btn-dark">
    <i class="zmdi zmdi-arrow-left"></i> Sebelumnya
  </a>
  <a href="javascript:;" class="btn btn-success pull-right next disabled opacity-0" button-type="btn-success" is-submit="true">
    <i class="zmdi zmdi-check-circle"></i> Kirim
  </a>
</fieldset>