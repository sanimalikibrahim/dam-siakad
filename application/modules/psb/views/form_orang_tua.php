<!-- Data Orang Tua -->
<fieldset>
  <div class="panel-orang_tua panel-active" panel-key="orang_tua">
    <div class="card">
      <div class="card-header border-info">
        <h5 class="card-title">Ayah Kandung</h5>
      </div>
      <div class="card-body pb-0">
        <?php include_once('form_orang_tua_ayah.php') ?>
      </div>
    </div>
    <div class="card">
      <div class="card-header border-info">
        <h5 class="card-title">Ibu Kandung</h5>
      </div>
      <div class="card-body pb-0">
        <?php include_once('form_orang_tua_ibu.php') ?>
      </div>
    </div>
    <div class="card">
      <div class="card-header border-info">
        <h5 class="card-title">Wali</h5>
      </div>
      <div class="card-body pb-0">
        <?php include_once('form_orang_tua_wali.php') ?>
      </div>
    </div>
  </div>
  <!-- Buttons -->
  <a href="javascript:;" class="btn btn-dark previous disabled opacity-0" button-type="btn-dark">
    <i class="zmdi zmdi-arrow-left"></i> Sebelumnya
  </a>
  <a href="javascript:;" class="btn btn-primary pull-right next disabled opacity-0" button-type="btn-primary">
    Lanjut <i class="zmdi zmdi-arrow-right"></i>
  </a>
</fieldset>