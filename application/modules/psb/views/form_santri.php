<!-- Data Santri -->
<fieldset>
  <div class="panel-santri panel-active" panel-key="santri">
    <!-- Personal -->
    <div class="card">
      <div class="card-header border-info">
        <h5 class="card-title">Data Personal</h5>
        <h6 class="card-subtitle text-muted text-black">Pengisian Identitas Harus Sesuai Dengan Ijazah SD/MI/Sederajat</h6>
      </div>
      <div class="card-body pb-0">
        <div class="row">
          <div class="col-md-12 col-12">
            <div class="form-group">
              <label required>Nama Lengkap Santri</label>
              <div class="position-relative">
                <input type="text" name="nama_lengkap" class="form-control psb-nama_lengkap" placeholder="-" required>
                <i class="form-group__bar"></i>
              </div>
              <small class="form-text text-muted">
                Sesuai dengan ijazah / Akta Kelahiran
              </small>
            </div>
          </div>
          <!-- <div class="col-md-6 col-12">
            <div class="form-group">
              <label required>Nomor Induk Siswa Nasional (NISN)</label>
              <input type="number" name="nisn" class="form-control mask-number psb-nisn" maxlength="10" placeholder="-" required>
              <i class="form-group__bar"></i>
            </div>
          </div> -->
        </div>
        <div class="row">
          <div class="col-md-6 col-12">
            <div class="form-group">
              <label required>Tempat Lahir</label>
              <input type="text" name="tempat_lahir" class="form-control psb-tempat_lahir" placeholder="-" required>
              <i class="form-group__bar"></i>
            </div>
          </div>
          <div class="col-md-6 col-12">
            <div class="form-group">
              <label required>Tanggal Lahir</label>
              <input type="text" name="tanggal_lahir" class="form-control flatpickr-datemax-today-id mask-date-id psb-tanggal_lahir" placeholder="DD-MM-YYYY" required>
              <i class="form-group__bar"></i>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6 col-12">
            <div class="form-group">
              <label required>Jenis Kelamin</label>
              <div class="pt-1">
                <div class="form-check form-check-inline">
                  <input class="form-check-input" type="radio" name="jenis_kelamin" id="jenis_kelamin-laki_laki" value="Laki-laki">
                  <label class="form-check-label" for="jenis_kelamin-laki_laki">Laki-laki</label>
                </div>
                <div class="form-check form-check-inline">
                  <input class="form-check-input" type="radio" name="jenis_kelamin" id="jenis_kelamin-perempuan" value="Perempuan">
                  <label class="form-check-label" for="jenis_kelamin-perempuan">Perempuan</label>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-12">
            <div class="form-group">
              <label required>Telepon</label>
              <input type="number" name="telepon" class="form-control mask-number psb-telepon" maxlength="15" placeholder="-" required>
              <i class="form-group__bar"></i>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6 col-12">
            <div class="form-group">
              <label required>Urutan Dalam Keluarga Anak Ke</label>
              <input type="number" name="urutan_anak" class="form-control mask-number psb-urutan_anak" maxlength="2" placeholder="-" required>
              <i class="form-group__bar"></i>
            </div>
          </div>
          <div class="col-md-6 col-12">
            <div class="form-group">
              <label required>Jumlah Saudara</label>
              <input type="number" name="jumlah_saudara" class="form-control mask-number psb-jumlah_saudara" maxlength="2" placeholder="-" required>
              <i class="form-group__bar"></i>
            </div>
          </div>
        </div>
        <!-- <div class="form-group">
          <label>Golongan Darah</label>
          <div class="pt-1">
            <div class="form-check form-check-inline">
              <input class="form-check-input" type="radio" name="golongan_darah" id="golongan_darah-a" value="A">
              <label class="form-check-label" for="golongan_darah-a">A</label>
            </div>
            <div class="form-check form-check-inline">
              <input class="form-check-input" type="radio" name="golongan_darah" id="golongan_darah-b" value="B">
              <label class="form-check-label" for="golongan_darah-b">B</label>
            </div>
            <div class="form-check form-check-inline">
              <input class="form-check-input" type="radio" name="golongan_darah" id="golongan_darah-o" value="O">
              <label class="form-check-label" for="golongan_darah-o">O</label>
            </div>
            <div class="form-check form-check-inline">
              <input class="form-check-input" type="radio" name="golongan_darah" id="golongan_darah-ab" value="AB">
              <label class="form-check-label" for="golongan_darah-ab">AB</label>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6 col-12">
            <div class="form-group">
              <label>Nomor Induk Keluarga (NIK)</label>
              <input type="number" name="nik" class="form-control mask-number psb-nik" maxlength="16" placeholder="-">
              <i class="form-group__bar"></i>
            </div>
          </div>
          <div class="col-md-6 col-12">
            <div class="form-group">
              <label>Nomor Pokok Sekolah Nasional (NPSN)</label>
              <input type="number" name="npsn" class="form-control mask-number psb-npsn" maxlength="8" placeholder="-">
              <i class="form-group__bar"></i>
            </div>
          </div>
        </div> -->
        <!-- <div class="row">
          <div class="col-md-6 col-12">
            <div class="form-group">
              <label>Nomor Induk Madrasah (NIM)</label>
              <input type="number" name="nim" class="form-control mask-number psb-nim" maxlength="18" placeholder="-">
              <i class="form-group__bar"></i>
            </div>
          </div>
          <div class="col-md-6 col-12">
            <div class="form-group">
              <label required>Nomor Induk Siswa Nasional (NISN)</label>
              <input type="number" name="nisn" class="form-control mask-number psb-nisn" maxlength="10" placeholder="-" required>
              <i class="form-group__bar"></i>
            </div>
          </div>
        </div> -->
        <div class="form-group">
          <label required>Alamat Tempat Tinggal Santri</label>
          <input type="text" name="alamat_santri" class="form-control psb-alamat_santri" placeholder="-" required>
          <i class="form-group__bar"></i>
        </div>
        <!-- <div class="row">
          <div class="col-md-6 col-12">
            <div class="form-group">
              <label>Kampung/Desa/Kelurahan</label>
              <input type="text" name="kelurahan" class="form-control psb-kelurahan" placeholder="-">
              <i class="form-group__bar"></i>
            </div>
          </div>
          <div class="col-md-6 col-12">
            <div class="form-group">
              <label>Kecamatan</label>
              <input type="text" name="kecamatan" class="form-control psb-kecamatan" placeholder="-">
              <i class="form-group__bar"></i>
            </div>
          </div>
        </div> -->
        <div class="row">
          <div class="col-md-6 col-12">
            <div class="form-group">
              <label required>Kota/Kabupaten</label>
              <div class="select">
                <select name="kota" class="form-control select2 psb-kota" data-placeholder="Select &#8595;">
                  <?= $list_regencies ?>
                </select>
                <i class="form-group__bar"></i>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-12">
            <div class="form-group">
              <label required>Provinsi</label>
              <div class="select">
                <select name="provinsi" class="form-control select2 psb-provinsi" data-placeholder="Select &#8595;">
                  <?= $list_provinces ?>
                </select>
                <i class="form-group__bar"></i>
              </div>
            </div>
          </div>
        </div>
        <!-- <div class="row">
          <div class="col-md-4 col-12">
            <div class="form-group">
              <label>Kode Pos</label>
              <input type="number" name="kode_pos" class="form-control mask-number psb-kode_pos" maxlength="5" placeholder="-">
              <i class="form-group__bar"></i>
            </div>
          </div>
          <div class="col-md-4 col-12">
            <div class="form-group">
              <label required>Telepon</label>
              <input type="number" name="telepon" class="form-control mask-number psb-telepon" maxlength="15" placeholder="-" required>
              <i class="form-group__bar"></i>
            </div>
          </div>
          <div class="col-md-4 col-12">
            <div class="form-group">
              <label>Handphone</label>
              <input type="number" name="handphone" class="form-control mask-number psb-handphone" maxlength="15" placeholder="-">
              <i class="form-group__bar"></i>
            </div>
          </div>
        </div> -->
        <div class="row">
          <div class="col-md-6 col-12">
            <div class="form-group">
              <label required>Tempat Tinggal Santri</label>
              <div class="select">
                <select name="tempat_tinggal" class="form-control psb-tempat_tinggal" data-placeholder="Select &#8595;">
                  <option disabled selected>Select &#8595;</option>
                  <option value="Rumah Orang Tua Kandung">Rumah Orang Tua Kandung</option>
                  <option value="Rumah Kakek Nenek">Rumah Kakek Nenek</option>
                  <option value="Rumah Kontrak">Rumah Kontrak (Kontrak/Sewa)</option>
                  <option value="Ikut Saudara">Ikut Saudara/Kerabat</option>
                  <option value="Asrama">Asrama</option>
                  <option value="Wali">Wali (Orang Tua Asuh)</option>
                  <option value="Lainnya">Lainnya</option>
                </select>
                <i class="form-group__bar"></i>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-12 control-psb-tempat_tinggal_lainnya hidden">
            <div class="form-group">
              <label required>Tempat Tinggal Santri: Lainnya</label>
              <input type="text" name="tempat_tinggal_lainnya" class="form-control psb-tempat_tinggal_lainnya" placeholder="-" required>
              <i class="form-group__bar"></i>
            </div>
          </div>
          <div class="col-md-6 col-12">
            <div class="form-group">
              <label required>Foto Terbaru</label>
              <div class="upload-inline">
                <div class="upload-button">
                  <input type="file" name="profile_photo" class="upload-pure-button psb-profile_photo" accept="image/jpg,image/jpeg,image/png,image/gif" />
                </div>
                <div class="upload-preview"></div>
              </div>
              <small class="form-text text-muted">
                Use file with format .JPG, .JPEG, .PNG or .GIF
              </small>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Sekolah Asal -->
    <div class="card">
      <div class="card-header border-info">
        <h5 class="card-title">Asal Sekolah</h5>
      </div>
      <div class="card-body pb-0">
        <div class="row">
          <div class="col-md-6 col-12">
            <div class="form-group">
              <label required>Nama Sekolah</label>
              <input type="text" name="sekolah_asal" class="form-control psb-sekolah_asal" placeholder="-" style="text-transform: uppercase;" required>
              <i class="form-group__bar"></i>
            </div>
          </div>
          <div class="col-md-6 col-12">
            <div class="form-group">
              <label required>Kota/Kabupaten Sekolah</label>
              <div class="select">
                <select name="sekolah_asal_alamat" class="form-control select2 psb-sekolah_asal_alamat" data-placeholder="Select &#8595;">
                  <?= $list_regencies ?>
                </select>
                <i class="form-group__bar"></i>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Surat Keterangan -->
    <div class="card">
      <div class="card-header border-info">
        <h5 class="card-title">Surat Keterangan</h5>
      </div>
      <div class="card-body pb-0">
        <div class="alert alert-light text-dark mb-4 text-center">
          <i class="zmdi zmdi-info"></i>
          Anda dapat mengunggahnya sekarang atau kosongkan jika ingin melakukannya nanti
        </div>
        <div class="row">
          <div class="col-md-6 col-12">
            <label>
              Surat Keterangan dari SD/MI sederajat yang menyatakan bahwa Calon Santri
              masih duduk di kelas VI dan akan mengikuti Ujian Akhir atau Ijazah SD/MI maksimal 2 tahun dari tahun kelulusan
            </label>
          </div>
          <div class="col-md-6 col-12">
            <div class="form-group">
              <div class="upload-inline-xs">
                <div class="upload-button">
                  <input type="file" name="surat_1" class="upload-pure-button psb-surat_1" accept="image/jpg,image/jpeg,image/png,image/gif" data-preview="surat_1" />
                </div>
                <div class="upload-preview data-preview-surat_1"></div>
              </div>
              <small class="form-text text-muted">
                Use file with format .JPG, .JPEG, .PNG or .GIF
              </small>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6 col-12">
            <label>
              Surat Keterangan Berkelakuan Baik dari SD/MI sederajat
            </label>
          </div>
          <div class="col-md-6 col-12">
            <div class="form-group">
              <div class="upload-inline-xs">
                <div class="upload-button">
                  <input type="file" name="surat_2" class="upload-pure-button psb-surat_2" accept="image/jpg,image/jpeg,image/png,image/gif" data-preview="surat_2" />
                </div>
                <div class="upload-preview data-preview-surat_2"></div>
              </div>
              <small class="form-text text-muted">
                Use file with format .JPG, .JPEG, .PNG or .GIF
              </small>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Buttons -->
  <a href="javascript:;" class="btn btn-dark previous disabled opacity-0" button-type="btn-dark">
    <i class="zmdi zmdi-arrow-left"></i> Sebelumnya
  </a>
  <a href="javascript:;" class="btn btn-primary pull-right next disabled opacity-0" button-type="btn-primary">
    Lanjut <i class="zmdi zmdi-arrow-right"></i>
  </a>
</fieldset>