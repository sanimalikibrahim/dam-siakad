<script type="text/javascript">
  $(document).ready(function() {

    var current_fs, next_fs, previous_fs; //fieldsets
    var opacity;
    var _form = "form-psb";
    var _modalExist = "modal-form-exist";
    var _modalSeragam = "modal-ref-seragam";
    var _prestasiCount = 0;
    var _isExistUser = false;
    var _isExistUserTemp = null;

    // Handle ajax start
    $(document).ajaxStart(function() {
      $(".spinner").css("display", "flex");
    });

    // Handle ajax stop
    $(document).ajaxStop(function() {
      $(".spinner").hide();
    });

    // Handle hide middle string
    function hideMiddleString(string, prefixLength, suffixLength) {
      var re = new RegExp('^(\\+?\\d{' + prefixLength + '})(\\d+)(\\d{' + suffixLength + '})$');

      return string.replace(re, function(match, prefix, middle, suffix) {
        return prefix + '*'.repeat(middle.length) + suffix;
      });
    };

    // Handle visibility floating button
    handleVisibilityFloatingButton = () => {
      // Set visibilty floating button: Next
      if ($(".next:visible").length > 0) {
        var isSubmit = $(".next:visible").attr("is-submit");

        $(".floating-next").show("fast");
        $(".floating-next").html($(".next:visible").html());
        $(".floating-next").attr("class", "btn btn--raised floating-next");
        $(".floating-next").addClass($(".next:visible").attr("button-type"));
        $(".floating-next").removeAttr("is-submit");

        if (isSubmit == "true") {
          $(".floating-next").attr("is-submit", isSubmit);
        };
      } else {
        $(".floating-next").hide("fast");
        $(".floating-next").html('Lanjut <i class="zmdi zmdi-arrow-right"></i>');
        $(".floating-next").attr("class", "btn btn--raised btn-primary floating-next");
      };

      // Set visibilty floating button: Previous
      if ($(".previous:visible").length > 0) {
        $(".floating-previous").show("fast");
        $(".floating-previous").html($(".previous:visible").html());
        $(".floating-previous").attr("class", "btn btn--raised floating-previous");
        $(".floating-previous").addClass($(".previous:visible").attr("button-type"));
      } else {
        $(".floating-previous").hide("fast");
        $(".floating-previous").html('<i class="zmdi zmdi-arrow-left"></i> Sebelumnya');
        $(".floating-previous").attr("class", "btn btn--raised btn-dark floating-previous");
      };
    };

    // Handle seragam modal
    $(document).on("click", ".btn-ref-seragam", function(e) {
      e.preventDefault();
      $("#" + _modalSeragam).modal("show");
      $(".floating-action").hide();
    });

    // Handle exist modal on close
    $("#" + _modalSeragam).on('hidden.bs.modal', function() {
      $(".floating-action").show();
    });

    // Handle exist accept
    $("#" + _modalExist + " .psb-action-exist-next").on("click", function() {
      _isExistUser = true;

      // Fill personal form
      $(".psb-nama_lengkap").val(_isExistUserTemp.nama_lengkap).trigger("input");
      $(".psb-tempat_lahir").val(_isExistUserTemp.tempat_lahir).trigger("input");
      $(".psb-tanggal_lahir").val(_isExistUserTemp.tanggal_lahir).trigger("input");
      $('input:radio[name="jenis_kelamin"][value="' + _isExistUserTemp.jenis_kelamin + '"]').attr("checked", "checked");
      $(".psb-telepon").val(_isExistUserTemp.telepon).trigger("input");
      $(".psb-urutan_anak").val(_isExistUserTemp.urutan_anak).trigger("input");
      $(".psb-jumlah_saudara").val(_isExistUserTemp.jumlah_saudara).trigger("input");
      $(".psb-alamat_santri").val(_isExistUserTemp.alamat_santri).trigger("input");
      $(".psb-kota").val(_isExistUserTemp.kota).trigger("change");
      $(".psb-provinsi").val(_isExistUserTemp.provinsi).trigger("change");
      $(".psb-tempat_tinggal").val(_isExistUserTemp.tempat_tinggal).trigger("change");
      $(".psb-tempat_tinggal_lainnya").val(_isExistUserTemp.tempat_tinggal_lainnya).trigger("input");
      // END ## Fill personal form

      // Fill orang tua form
      // Ayah
      $(".psb-ayah_nama_lengkap").val(_isExistUserTemp.ayah_nama_lengkap).trigger("input");
      $('input:radio[name="ayah_status"][value="' + _isExistUserTemp.ayah_status + '"]').attr("checked", "checked");
      $(".psb-ayah_pekerjaan").val(_isExistUserTemp.ayah_pekerjaan).trigger("change");
      $(".psb-ayah_pekerjaan_jenis").val(_isExistUserTemp.ayah_pekerjaan_jenis).trigger("change");
      $(".psb-ayah_pekerjaan_pangkat").val(_isExistUserTemp.ayah_pekerjaan_pangkat).trigger("change");
      // Ibu
      $(".psb-ibu_nama_lengkap").val(_isExistUserTemp.ibu_nama_lengkap).trigger("input");
      $('input:radio[name="ibu_status"][value="' + _isExistUserTemp.ibu_status + '"]').attr("checked", "checked");
      $(".psb-ibu_pekerjaan").val(_isExistUserTemp.ibu_pekerjaan).trigger("change");
      $(".psb-ibu_pekerjaan_jenis").val(_isExistUserTemp.ibu_pekerjaan_jenis).trigger("change");
      $(".psb-ibu_pekerjaan_pangkat").val(_isExistUserTemp.ibu_pekerjaan_pangkat).trigger("change");
      // Wali
      $(".psb-wali_nama_lengkap").val(_isExistUserTemp.wali_nama_lengkap).trigger("input");
      $('input:radio[name="wali_status"][value="' + _isExistUserTemp.wali_status + '"]').attr("checked", "checked");
      $(".psb-wali_pekerjaan").val(_isExistUserTemp.wali_pekerjaan).trigger("change");
      $(".psb-wali_pekerjaan_jenis").val(_isExistUserTemp.wali_pekerjaan_jenis).trigger("change");
      $(".psb-wali_pekerjaan_pangkat").val(_isExistUserTemp.wali_pekerjaan_pangkat).trigger("change");
      // END ## Fill orang tua form

      $("#" + _modalExist).modal("hide");
      $(".next:visible").click();
    });

    // Handle floating form next
    $(".floating-next").click(function(e) {
      var isSubmit = $(this).attr("is-submit");

      if (isSubmit == "true") {
        submitPsb(e);
      } else {
        var panelActive = $(".panel-active:visible").attr("panel-key");
        panelActive = (typeof panelActive !== "undefined") ? panelActive : "";

        // Validate form input
        var form = $("#" + _form)[0];
        var data = new FormData(form);

        $.ajax({
          type: "post",
          url: "<?php echo base_url('psb/validate_form/') ?>" + panelActive,
          data: data,
          dataType: "json",
          enctype: "multipart/form-data",
          processData: false,
          contentType: false,
          cache: false,
          success: function(response) {
            if (response.status === true) {
              $(".next:visible").click();
            } else {
              // Handle data exists
              if (response.panel == "akun") {
                if (response.existing != null) {
                  const telepon = (response.existing.telepon) ? hideMiddleString(response.existing.telepon, 2, 3) : "-";
                  _isExistUserTemp = response.existing;
                  _isExistUser = false;

                  $("#" + _modalExist + " .psb-exist-foto").attr("src", response.existing_foto);
                  $("#" + _modalExist + " .psb-exist-nama_lengkap").html(response.existing.nama_lengkap.toUpperCase());
                  $("#" + _modalExist + " .psb-exist-nisn").html(response.existing.nisn);
                  $("#" + _modalExist + " .psb-exist-ttgl").html(response.existing.tempat_lahir.toUpperCase() + ", " + response.existing.tanggal_lahir);
                  $("#" + _modalExist + " .psb-exist-jenis_kelamin").html(response.existing.jenis_kelamin.toUpperCase());
                  $("#" + _modalExist + " .psb-exist-telepon").html(telepon);
                  $("#" + _modalExist + " .psb-exist-alamat_santri").html(response.existing.alamat_santri.toUpperCase());
                  $("#" + _modalExist + " .psb-exist-kota").html(response.existing.kota.toUpperCase());
                  $("#" + _modalExist + " .psb-exist-provinsi").html(response.existing.provinsi.toUpperCase());
                  $("#" + _modalExist).modal("show");
                  $(".floating-action").hide();

                  return false;
                };
              };

              notify(response.data, "danger");
              return false;
            };
          }
        });
      };
    });

    // Handle floating form previous
    $(".floating-previous").click(function() {
      $(".previous:visible").click();
    });

    // Handle form next
    $(".next").click(function() {
      current_fs = $(this).parent();
      next_fs = $(this).parent().next();

      //Add Class Active
      $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

      //hide the current fieldset with style
      current_fs.animate({
        opacity: 0
      }, {
        step: function(now) {
          // for making fielset appear animation
          opacity = 1 - now;

          current_fs.css({
            'display': 'none',
            'position': 'relative'
          });
          next_fs.css({
            'opacity': opacity
          });
        },
        duration: 500
      });

      //show the next fieldset
      next_fs.show("fast", function() {
        handleVisibilityFloatingButton();

        // Scroll to top
        $([document.documentElement, document.body]).animate({
          scrollTop: $(".login__block__header").offset().top
        }, 500);
      });
    });

    // Handle form previous
    $(".previous").click(function() {
      current_fs = $(this).parent();
      previous_fs = $(this).parent().prev();

      //Remove class active
      $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

      //hide the current fieldset with style
      current_fs.animate({
        opacity: 0
      }, {
        step: function(now) {
          // for making fielset appear animation
          opacity = 1 - now;

          current_fs.css({
            'display': 'none',
            'position': 'relative'
          });
          previous_fs.css({
            'opacity': opacity
          });
        },
        duration: 500
      });

      //show the previous fieldset
      previous_fs.show("fast", function() {
        handleVisibilityFloatingButton();

        // Scroll to top
        $([document.documentElement, document.body]).animate({
          scrollTop: $(".login__block__header").offset().top
        }, 500);
      });
    });

    // Handle exist modal on close
    $("#" + _modalExist).on('hidden.bs.modal', function() {
      $(".floating-action").show();
    });

    // Handle Tempat Tinggal Santri
    $(".psb-tempat_tinggal").on("change", function() {
      var value = $(this).val();
      var control = $(".control-psb-tempat_tinggal_lainnya");

      if (value === "Lainnya") {
        control.show();
      } else {
        control.hide();
      };
    });

    // Handle Ayah: Pekerjaan Utama
    $(".psb-ayah_pekerjaan").on("change", function() {
      var value = $(this).val();
      var control = $(".control-psb-ayah_pekerjaan");

      if (value === "TNI/POLRI" || value === "PNS") {
        control.css("display", "flex");
      } else {
        control.hide();
      };
    });

    // Handle Ibu: Pekerjaan Utama
    $(".psb-ibu_pekerjaan").on("change", function() {
      var value = $(this).val();
      var control = $(".control-psb-ibu_pekerjaan");

      if (value === "TNI/POLRI" || value === "PNS") {
        control.css("display", "flex");
      } else {
        control.hide();
      };
    });

    // Handle Wali: Pekerjaan Utama
    $(".psb-wali_pekerjaan").on("change", function() {
      var value = $(this).val();
      var control = $(".control-psb-wali_pekerjaan");

      if (value === "TNI/POLRI" || value === "PNS") {
        control.css("display", "flex");
      } else {
        control.hide();
      };
    });

    // Handle prestasi: Add
    $(".prestasi-action-add").on("click", function(e) {
      e.preventDefault();

      // Increment form count
      _prestasiCount = parseInt(_prestasiCount) + 1;

      var wrapper = $(".form-prestasi");
      var formTemplate = '' +
        '<!-- Form Items -->' +
        '<div class="panel-bordered mt-3 form-prestasi-item form-prestasi-item-' + _prestasiCount + '">' +
        '<div class="panel-bordered-header">' +
        'Prestasi' +
        '<a href="javascript:;" class="pull-right prestasi-action-delete" style="color: var(--red);" title="Hapus" data-prestasi-key="' + _prestasiCount + '">' +
        '<i class="zmdi zmdi-close"></i>' +
        '</a>' +
        '</div>' +
        '<div class="panel-bordered-body">' +
        '<div class="row">' +
        '<div class="col-md-6 col-12">' +
        '<div class="form-group">' +
        '<label required>Nama Lomba</label>' +
        '<input type="text" name="prestasi[nama_lomba][]" class="form-control prestasi-nama_lomba" placeholder="-" required />' +
        '<i class="form-group__bar"></i>' +
        '</div>' +
        '</div>' +
        '<div class="col-md-6 col-12">' +
        '<div class="form-group">' +
        '<label>Jenis Lomba</label>' +
        '<input type="text" name="prestasi[jenis_lomba][]" class="form-control prestasi-jenis_lomba" placeholder="-" />' +
        '<i class="form-group__bar"></i>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '<div class="row">' +
        '<div class="col-md-6 col-12">' +
        '<div class="form-group">' +
        '<label>Penyelenggara</label>' +
        '<input type="text" name="prestasi[penyelenggara][]" class="form-control prestasi-penyelenggara" placeholder="-" />' +
        '<i class="form-group__bar"></i>' +
        '</div>' +
        '</div>' +
        '<div class="col-md-6 col-12">' +
        '<div class="form-group">' +
        '<label>Tingkat</label>' +
        '<input type="text" name="prestasi[tingkat][]" class="form-control prestasi-tingkat" placeholder="-" />' +
        '<i class="form-group__bar"></i>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '<div class="row">' +
        '<div class="col-md-6 col-12">' +
        '<div class="form-group">' +
        '<label required>Peringkat</label>' +
        '<input type="text" name="prestasi[peringkat][]" class="form-control prestasi-peringkat" placeholder="-" required />' +
        '<i class="form-group__bar"></i>' +
        '</div>' +
        '</div>' +
        '<div class="col-md-6 col-12">' +
        '<div class="form-group">' +
        '<label required>Tahun</label>' +
        '<input type="number" name="prestasi[tahun][]" class="form-control mask-number prestasi-tahun" maxlength="4" placeholder="-" required />' +
        '<i class="form-group__bar"></i>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '<div class="form-group mb-0">' +
        '<label required>Foto Sertifikat / Piagam</label>' +
        '<div class="position-relative">' +
        '<input type="file" name="prestasi[sertifikat][]" class="form-control prestasi-sertifikat" accept="image/jpeg,image/gif,image/png,application/pdf" required />' +
        '<i class="form-group__bar"></i>' +
        '</div>' +
        '<small class="form-text text-muted">' +
        'Use file with format .JPG, .JPEG, .PNG, .GIF or .PDF' +
        '</small>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '<!-- END ## Form Items -->' +
        '';

      wrapper.append(formTemplate).children(':last').hide().fadeIn("fast");
      handlePrestasiCount();
    });

    // Handle prestasi: Delete
    $(document).on("click", "a.prestasi-action-delete", function(e) {
      e.preventDefault();

      var key = $(this).attr("data-prestasi-key");
      var form = $(".form-prestasi-item-" + key);

      form.fadeOut("fast", function() {
        $(this).remove();
        handlePrestasiCount();
      });
    });

    // Handle prestasi: Data Count
    handlePrestasiCount = () => {
      var countItem = $(".form-prestasi-item").length;

      if (countItem > 0) {
        $(".form-prestasi-item-count").show().html("Jumlah Prestasi: " + countItem);
      } else {
        $(".form-prestasi-item-count").hide().html("Jumlah Prestasi: " + countItem);
      };
    };

    // Handle Komitmen: Bersedia
    $(".psb-komitmen").on("change", function() {
      var value = $(this).val();
      var nominal = $(".psb-komitmen_nominal");
      var control = $(".control-psb-komitmen");

      if (value === "Bersedia") {
        control.css("display", "block");
      } else {
        nominal.val("");
        control.hide();
      };
    });

    // Handle data submit
    submitPsb = (e) => {
      e.preventDefault();

      var form = $("#" + _form)[0];
      var data = new FormData(form);
      var key = null;

      if (_isExistUser === true) {
        key = (_isExistUserTemp.id) ? _isExistUserTemp.id : null;
      };

      $.ajax({
        type: "post",
        url: "<?php echo base_url('psb/ajax_submit/') ?>" + key,
        data: data,
        dataType: "json",
        enctype: "multipart/form-data",
        processData: false,
        contentType: false,
        cache: false,
        success: function(response) {
          if (response.status === true) {
            $("#" + _form).trigger("reset");
            $("#" + _form + " .psb-input-wrapper").hide("fast");
            $("#" + _form + " .psb-response-dom").show("fast", function() {
              $("#" + _form + " .psb-response-dom").html(response.html_dom);
              $(".floating-action").slideUp(function() {
                $(this).remove();
                $(".panel-active").removeClass("mb-5");
              });
            });
          } else {
            notify(response.data, "danger");
          };
        }
      });

      return false;
    };

    // Handle upload
    $(document.body).on("change", ".psb-profile_photo", function() {
      readUploadInlineURL(this);
    });
    $(document.body).on("change", ".psb-surat_1", function() {
      readUploadMultipleDocURLXs(this);
    });
    $(document.body).on("change", ".psb-surat_2", function() {
      readUploadMultipleDocURLXs(this);
    });

  });
</script>