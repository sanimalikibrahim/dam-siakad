<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . 'controllers/AppBackend.php');

class Psb extends AppBackend
{
  function __construct()
  {
    parent::__construct();
    $this->load->model([
      'PsbModel',
      'PsbprestasiModel',
      'PostModel',
      'ProvincesModel',
      'RegenciesModel',
      'UserModel'
    ]);
    $this->load->library('form_validation');
  }

  public function index()
  {
    $app = $this->app();
    $persyaratanDaftarUlang = $this->PostModel->getDetail(['id' => 3]);
    $komitmen = $this->PostModel->getDetail(['id' => 2]);
    $periodeMulai = isset($app->psb_tahun_mulai) ? $app->psb_tahun_mulai : 'undefined';
    $periodeSelesai = isset($app->psb_tahun_selesai) ? $app->psb_tahun_selesai : 'undefined';
    $periode = $periodeMulai . ' - ' . $periodeSelesai;

    $data = array(
      'app' => $app,
      'main_js' => $this->load_main_js('psb'),
      'page_title' => 'Pendaftaran Santri Baru Tahun Ajaran ' . $periode . ' | ' . $this->app()->app_name,
      'periode' => $periode,
      'persyaratan_daftar_ulang' => $persyaratanDaftarUlang,
      'komitmen' => $komitmen,
      'list_provinces' => $this->init_list($this->ProvincesModel->getAll(), 'name', 'name'),
      'list_regencies' => $this->init_list($this->RegenciesModel->getAll(), 'name', 'name'),
    );
    $this->load->view('index', $data);
  }

  public function validate_form($type = null)
  {
    $this->handle_ajax_request();

    if ((!is_null($type) && !empty($type)) && in_array($type, ['akun', 'santri', 'orang_tua', 'prestasi'])) {
      $app = $this->app();
      $nisn = $this->input->post('nisn');
      $password = $this->input->post('password');
      $passwordConfirm = $this->input->post('password_confirm');
      $existing = null;
      $existingFoto = null;

      if ((int) $app->psb_status === 1) {
        if ($passwordConfirm === $password) {
          // Get existing user
          $existingUser = $this->UserModel->getDetail(['username' => trim($nisn)]);
          if (!is_null($existingUser)) {
            $existingFoto = $existingUser->profile_photo;
          };
          // END ## Get existing user

          switch ($type) {
            case 'akun':
              $ruleType = $this->PsbModel->rulesAkun(null);
              $existing = $this->PsbModel->getDetail(['nisn' => trim($nisn)]);
              break;
            case 'santri':
              $ruleType = $this->PsbModel->rulesSantri(null);
              break;
            case 'orang_tua':
              $ruleType = $this->PsbModel->rulesOrangTua(null);
              break;
            case 'prestasi':
              $ruleType = $this->PsbModel->rulesPestasi(null);
              break;
            default:
              $ruleType = null;
              break;
          };

          $this->form_validation->set_rules($ruleType);

          if ($this->form_validation->run() === true) {
            if ($type === 'santri') {
              $profile_photo = $_FILES['profile_photo']['name'];

              if (is_null($profile_photo) || empty($profile_photo)) {
                echo json_encode(array('status' => false, 'data' => 'The Foto field is required.', 'panel' => $type, 'existing' => $existing, 'existing_foto' => $existingFoto));
                exit;
              };
            };

            echo json_encode(array('status' => true, 'data' => 'Data has been validated.', 'panel' => $type, 'existing' => $existing, 'existing_foto' => $existingFoto));
          } else {
            $errors = validation_errors('<div>- ', '</div>');
            echo json_encode(array('status' => false, 'data' => $errors, 'panel' => $type, 'existing' => $existing, 'existing_foto' => $existingFoto));
          };
        } else {
          echo json_encode(array('status' => false, 'data' => 'Confirm Password tidak sama.', 'panel' => $type, 'existing' => $existing, 'existing_foto' => $existingFoto));
        };
      } else {
        echo json_encode(array('status' => false, 'data' => 'Maaf! Pendaftaran santri baru telah ditutup.'));
      };
    } else {
      echo json_encode(array('status' => false, 'data' => 'Missing parameter! Please reload and try again later.'));
    };
  }

  public function ajax_submit($id = null)
  {
    $this->handle_ajax_request();
    $app = $this->app();

    if ((int) $app->psb_status === 1) {
      $this->form_validation->set_rules($this->PsbModel->rules($id));

      if ($this->form_validation->run() === true) {
        // Upload foto prifile
        if (!empty($_FILES['profile_photo']['name'])) {
          $cpUpload = new CpUpload();
          $upload = $cpUpload->run('profile_photo', 'profile', true, true, 'jpg|jpeg|png|gif');

          $_POST['profile_photo'] = '';

          if ($upload->status === true) {
            $_POST['profile_photo'] = $upload->data->base_path;
          };
        };

        // Upload surat 1
        if (!empty($_FILES['surat_1']['name'])) {
          $cpUpload = new CpUpload();
          $upload = $cpUpload->run('surat_1', 'psb_surat', true, true, 'jpg|jpeg|png|gif');

          $_POST['surat_1'] = '';

          if ($upload->status === true) {
            $_POST['surat_1'] = $upload->data->base_path;
          };
        };

        // Upload surat 2
        if (!empty($_FILES['surat_2']['name'])) {
          $cpUpload = new CpUpload();
          $upload = $cpUpload->run('surat_2', 'psb_surat', true, true, 'jpg|jpeg|png|gif');

          $_POST['surat_2'] = '';

          if ($upload->status === true) {
            $_POST['surat_2'] = $upload->data->base_path;
          };
        };

        // Set tahun by config
        $_POST['active_tahun'] = isset($app->psb_tahun_mulai) ? $app->psb_tahun_mulai : 0;

        if (is_null($id) || $id == 'null') {
          $transaction = $this->PsbModel->insert();
        } else {
          $transaction = $this->PsbModel->update($id, true);
        };
        $response = null;

        if ($transaction['status'] === true) {
          // Save prestasi
          $transactionPrestasi = $this->_save_prestasi($transaction['psb_id']);

          if ($transactionPrestasi['status'] === true) {
            // Initialize success message
            $persyaratanDaftarUlang = $this->PostModel->getDetail(['id' => 3]);
            $successResposne = $this->load->view('finish_message', array(
              'controller' => $this,
              'data' => $persyaratanDaftarUlang
            ), true);

            // Send mail
            $sendMail = $this->_sendMail($this->input->post('email'), $successResposne);

            if ($sendMail['status'] === true) {
              $response = array_merge($transaction, ['html_dom' => $successResposne]);
            } else {
              // Delete current data when email error
              $this->PsbModel->delete($transaction['psb_id']);

              $response = $sendMail;
            };
          } else {
            $response = $transactionPrestasi;
          };
        } else {
          $response = $transaction;
        };

        echo json_encode($response);
      } else {
        $errors = validation_errors('<div>- ', '</div>');
        echo json_encode(array('status' => false, 'data' => $errors));
      };
    } else {
      echo json_encode(array('status' => false, 'data' => 'Maaf! Pendaftaran santri baru telah ditutup.'));
    };
  }

  private function _save_prestasi($psb_id = null)
  {
    $prestasi = $this->input->post('prestasi');

    if (!empty($psb_id) && !is_null($psb_id) && !is_null($prestasi)) {
      $cpUpload = new CpUpload();

      $prestasi = $cpUpload->re_arrange($prestasi);
      $prestasiFile = $_FILES['prestasi'];
      $prestasiFile = $cpUpload->re_arrange($prestasiFile);
      $prestasiFile = $prestasiFile['sertifikat'];
      $prestasiFile = $cpUpload->re_arrange($prestasiFile);

      $directory = 'psb';
      $post = array();
      $error = '';

      foreach ($prestasiFile as $index => $item) {
        if (!empty($item['name'])) {
          $upload = $cpUpload->run($item, $directory, true, true, 'jpg|jpeg|png|gif|pdf', true);

          if ($upload->status === true) {
            $post[] = array(
              'psb_id' => $psb_id,
              'nama_lomba' => $prestasi[$index]['nama_lomba'],
              'jenis_lomba' => $prestasi[$index]['jenis_lomba'],
              'penyelenggara' => $prestasi[$index]['penyelenggara'],
              'tingkat' => $prestasi[$index]['tingkat'],
              'peringkat' => $prestasi[$index]['peringkat'],
              'tahun' => $prestasi[$index]['tahun'],
              'file_raw_name' => $upload->data->raw_name . $upload->data->file_ext,
              'file_raw_name_thumb' => ($upload->data->is_image) ? $upload->data->raw_name . '_thumb' . $upload->data->file_ext : null,
              'file_name' => $upload->data->base_path,
              'file_name_thumb' => ($upload->data->is_image) ? 'directory/' . $directory . '/' . $upload->data->raw_name . '_thumb' . $upload->data->file_ext : null,
              'file_size' => $upload->data->file_size
            );
          } else {
            $error .= $upload->data;
          };
        };
      };

      if (empty($error) && count($post) > 0) {
        return $this->PsbprestasiModel->insertBatch($post);
      } else {
        return array('status' => false, 'data' => $error);
      };
    } else {
      return array('status' => true, 'data' => 'No file to upload.');
    };
  }

  private function _sendMail($email = null, $template = '')
  {
    $app = $this->app();
    $mailParams = array(
      'receiver' => $email,
      'subject' => 'Pendaftaran Santri Baru | ' . $app->app_name,
      'message' => $template
    );

    return $this->sendMail($mailParams);
  }
}
