<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . 'controllers/AppBackend.php');

class Notice extends AppBackend
{
  function __construct()
  {
    parent::__construct();
  }

  public function index()
  {
    show_404();
  }

  public function persyaratan_psb()
  {
    $data = array(
      'app' => $this->app(),
      'main_js' => $this->load_main_js('notice'),
      'page_title' => 'Persyaratan Daftar Ulang'
    );
    $this->template->set('title', $data['page_title'] . ' | ' . $data['app']->app_name, TRUE);
    $this->template->load_view('finish_message', $data, TRUE);
    $this->template->render();
  }

  public function timeline()
  {
    $data = array(
      'app' => $this->app(),
      'main_js' => $this->load_main_js('notice'),
      'page_title' => 'Timeline'
    );
    $this->template->set('title', $data['page_title'] . ' | ' . $data['app']->app_name, TRUE);
    $this->template->load_view('timeline', $data, TRUE);
    $this->template->render();
  }
}
