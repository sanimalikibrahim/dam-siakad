<!-- Finish Message -->
<div>
  <div class="card">
    <div class="card-body">
      <div class="alert alert-info text-center mb-4">
        Abaikan jika syarat sudah terpenuhi!
      </div>
      <div class="mb-3">
        Silakan lengkapi Persyaratan Daftar Ulang lainnya (Mohon perhatikan dan catat hal-hal penting berikut) :
      </div>
      <h6>Persyaratan Daftar Ulang</h6>
      <ol style="padding-left: 2rem;">
        <li class="mb-3">
          Membayar Keuangan Santri Baru melalui :
          <div class="alert alert-light text-dark text-center mt-3 mb-3 pt-4 pb-4">
            <img src="<?= base_url('themes/_public/img/logo/logo-bank-mandiri-syariah.png') ?>" class="bg-white p-3 mb-4" style="width: 150px; border-radius: 12px;" />
            <p class="mb-2">
              Jumlah Yang Harus Dibayar : <br />
              <b class="text-green" style="font-size: 11pt;">Rp. 13.000.000</b>
            </p>
            <p class="mb-1">
              Nama Bank : <br />
              <b class="text-primary">Bank Syariah Mandiri</b>
            </p>
            <p class="mb-0">
              Nomor Rekening : <br />
              <b class="text-primary">7096700576</b> <br />
              <span class="text-primary">(a.n. PONPES DARUL ARQAM)</span>
            </p>
          </div>
          Segera konfirmasi melalui Whatsapp ke nomor :
          <p class="mt-1 mb-0">
            <b>081312768146 (Drs. Nasrun Hermansyah), 082118637055 (Asti Sumiati, A.Ma)</b>
            atau
            <b>081220102943 (Dodi Ahmad Mudakit, S.Pd)</b>
          </p>
        </li>
        <li class="mb-3">
          Cek Kesehatan dilaksanakan pada Waktu Kegiatan Ta’aruf, di Klinik Ponpes Darul Arqam Muhammadiyah Daerah Garut
        </li>
        <li>
          Melengkapi Administrasi (Diserahkan ketika santri masuk) :
          <ul class="mt-2" style="padding-left: 2rem;">
            <li class="mb-1">5 Lembar Photo Kopi Ijazah SD/MI yang telah dilegalisir</li>
            <li class="mb-1">5 Lembar Photo Kopi SKHUN SD/MI yang telah dilegalisir</li>
            <li class="mb-1">1 Exemplar Photo Kopi Buku Laporan Pendidikan SD/MI yang telah dilegalisir</li>
            <li class="mb-1">5 Lembar Photo Kopi Kartu NISN (Nomor Induk Siswa Nasional)</li>
            <li class="mb-1">5 Lembar Photo Kopi Akte Kelahiran</li>
            <li class="mb-1">5 Lembar Photo Kopo Kartu Keluarga</li>
            <li class="mb-1">Menyerahkan KIP (Kartu Indonesia Pintar) Asli dan Photo Kopinya, bagi yang memiliki</li>
            <li class="mb-1">
              Surat Kesediaan Mentaati Peraturan dan Tata Tertib Pondok 3 lembar Diatas Materai 6.000 (<b class="text-red">Draft Surat</b> <a href="https://psb.darularqamgarut.sch.id/paths/2020/SURAT-PERNYATAAN-SANTRI-BARU.docx" target="_blank"><b>Download di Sini</b></a>)
            </li>
            <li>
              Surat Pernyataan Orang Tua 3 lembar Diatas Materai 6.000 (<b class="text-red">Draft Surat</b> <a href="https://psb.darularqamgarut.sch.id/paths/2020/SURAT-PERNYATAAN-ORANG-TUA.docx" target="_blank"><b>Download di Sini</b></a>)
            </li>
          </ul>
        </li>
      </ol>
    </div>
  </div>
</div>