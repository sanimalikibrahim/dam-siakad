<!-- Agenda / Timeline PSB -->
<div class="card">
  <div class="card-body" style="background-color: hsl(205, 38%, 93.45%);">
    <section class="cd-timeline js-cd-timeline">
      <div class="container max-width-lg cd-timeline__container">

        <div class="cd-timeline__block">
          <div class="cd-timeline__img cd-timeline__img--picture">
            <i class="zmdi zmdi-n-1-square text-white" style="font-size: 1.5rem;"></i>
          </div> <!-- cd-timeline__img -->
          <div class="cd-timeline__content text-component">
            <h2 class="mb-2">TEST LISAN DAN TULISAN</h2>
            <p class="color-contrast-medium">Calon Santri melakukan ujian lisan dan tulisan di Pondok Pesantren Darul Arqam Muhammadiyah Garut</p>
            <div class="flex justify-between items-center">
              <span class="cd-timeline__date">07 Maret 2020</span>
            </div>
          </div> <!-- cd-timeline__content -->
        </div> <!-- cd-timeline__block -->

        <div class="cd-timeline__block">
          <div class="cd-timeline__img cd-timeline__img--picture">
            <i class="zmdi zmdi-n-2-square text-white" style="font-size: 1.5rem;"></i>
          </div> <!-- cd-timeline__img -->
          <div class="cd-timeline__content text-component">
            <h2 class="mb-2">PENGUMUMAN KELULUSAN</h2>
            <p class="color-contrast-medium">Pengumuman kelulusan yang akan disampaikan melalui: Email Surat Website PSB Pesantren Darul Arqam Muhammadiyah Daerah Garut</p>
            <div class="flex justify-between items-center">
              <span class="cd-timeline__date">15 Maret 2020 <br /> s/d <br /> 21 Maret 2020</span>
            </div>
          </div> <!-- cd-timeline__content -->
        </div> <!-- cd-timeline__block -->

        <div class="cd-timeline__block">
          <div class="cd-timeline__img cd-timeline__img--picture">
            <i class="zmdi zmdi-n-3-square text-white" style="font-size: 1.5rem;"></i>
          </div> <!-- cd-timeline__img -->
          <div class="cd-timeline__content text-component">
            <h2 class="mb-2">DAFTAR ULANG SANTRI BARU</h2>
            <p class="color-contrast-medium">
              Calon santri yang telah dinyatakan lulus melakukan daftar ulang dengan menyertakan kelengkapan persyaratan dokumen dan administrasi. Kelengkapan Dokumen 5 Lembar Photo Kopi Ijazah SD/MI yang telah dilegalisir 5 Lembar Photo Kopi SKHUN SD/MI yang tela...
            </p>
            <div class="timeline-detail-item-0" style="display: none;">
              <p>Calon santri yang telah dinyatakan lulus melakukan daftar ulang dengan menyertakan kelengkapan persyaratan dokumen dan administrasi.</p>
              <p><b>Kelengkapan Dokumen</b></p>
              <ol>
                <li>5 Lembar Photo Kopi Ijazah SD/MI yang telah dilegalisir</li>
                <li>5 Lembar Photo Kopi SKHUN SD/MI yang telah dilegalisir</li>
                <li>1 Exemplar Photo Kopi Buku Laporan Pendidikan SD/MI yang telah dilegalisir</li>
                <li>5 Lembar Photo Kopi Kartu NISN (Nomor Induk Siswa Nasional)</li>
                <li>5 Lembar Photo Kopi Akte Kelahiran</li>
                <li>5 Lembar Photo Kopo Kartu Keluarga</li>
                <li>&nbsp;3 Set Data Pribadi Santri (Formulir disediakan Panitia PSB Pesantren Darul Arqam)</li>
                <li>3 Lembar Surat Kesediaan Mentaati Peraturan dan Tata Tertib Pondok (Surat disediakan Panitia PSB Pesantren Darul Arqam)</li>
                <li>Menyerahkan KIP (Kartu Indonesia Pintar) Asli dan Photo Kopinya, bagi yang memiliki</li>
              </ol>
              <p><b>Kelengkapan Administrasi</b></p>
              <ol>
                <li>Keuangan Santri Baru <span style="color: #0000ff;"><b>Rp. 13.000.000</b></span> (Tiga Belas Juta Rupiah)</li>
              </ol>
              <p><b>Calon Santri yang tidak melakukan Daftar Ulang tepat pada waktu yang telah ditentukan, dianggap mengundurkan diri</b></p>
            </div>
            <div class="flex justify-between items-center">
              <span class="cd-timeline__date">04 April 2020 <br /> s/d <br /> 06 April 2020</span>
              <a href="#" class="btn btn-light btn-action-detail <?= ($app->is_mobile) ? 'btn-block mt-2' : '' ?>" data-key="0">Read more</a>
            </div>
          </div> <!-- cd-timeline__content -->
        </div> <!-- cd-timeline__block -->

        <div class="cd-timeline__block">
          <div class="cd-timeline__img cd-timeline__img--picture">
            <i class="zmdi zmdi-n-4-square text-white" style="font-size: 1.5rem;"></i>
          </div> <!-- cd-timeline__img -->
          <div class="cd-timeline__content text-component">
            <h2 class="mb-2">TA’ARUF</h2>
            <p class="color-contrast-medium">Santri Baru mulai masuk Pondok untuk mengikuti serangkaian kegiatan Ta’aruf (Pengenalan Pondok beserta apartur & entitasnya)</p>
            <div class="flex justify-between items-center">
              <span class="cd-timeline__date">01 Juni 2020</span>
            </div>
          </div> <!-- cd-timeline__content -->
        </div> <!-- cd-timeline__block -->

      </div>
    </section> <!-- cd-timeline -->
  </div>
</div>

<!-- Modal detail -->
<div class="modal fade" id="modal-detail">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title pull-left">Detail</h5>
      </div>
      <div class="spinner">
        <div class="lds-hourglass"></div>
      </div>
      <div class="modal-body">
        <!-- Content in here -->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>