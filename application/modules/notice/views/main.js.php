<script type="text/javascript">
  $(document).ready(function() {
    var _modalDetail = $("#modal-detail:first");

    $(document).on("click", ".btn-action-detail", function(e) {
      e.preventDefault();
      var key = $(this).attr("data-key");
      var content = $(".timeline-detail-item-" + key);

      _modalDetail.find(".modal-body").html("");
      _modalDetail.find(".modal-body").html(content.html());
      _modalDetail.modal("show");
    });
  });
</script>