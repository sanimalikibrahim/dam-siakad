<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . 'controllers/AppBackend.php');

class Myqrcode extends AppBackend
{
  function __construct()
  {
    parent::__construct();
  }

  public function index()
  {
    $userId = $this->session->userdata('user')['id'];
    $userRole = $this->session->userdata('user')['role'];

    if ($userRole === 'Guru') {
      $list_kelas = $this->init_list_kelas_by_guru($userId);
      $list_mapel = $this->init_list_mapel_by_guru($userId);
    } else {
      $list_kelas = array();
      $list_mapel = array();
    };

    $data = array(
      'app' => $this->app(),
      'main_js' => $this->load_main_js('myqrcode'),
      'card_title' => 'My QR-Code',
      'list_kelas' => $list_kelas,
      'list_mapel' => $list_mapel,
    );
    $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
    $this->template->load_view('index', $data, TRUE);
    $this->template->render();
  }

  public function generate()
  {
    $userId = $this->session->userdata('user')['id'];
    $userRole = $this->session->userdata('user')['role'];
    $fullName = $this->session->userdata('user')['nama_lengkap'];

    if ($userRole === 'Guru') {
      $kelas = $this->input->get('kelas');
      $mapel = $this->input->get('mapel');

      if (!is_null($kelas) && !is_null($mapel)) {
        $kelas = str_replace('$', '#', $kelas);
        $this->generateQrCodeAsImage($userId . '##' . $fullName . '##' . $kelas . '##' . $mapel);
      } else {
        echo '<h4 style="color: #ccc;">Silahkan lakukan generate terlebih dahulu!</h4>';
      };
    } else {
      $this->generateQrCodeAsImage($userId . '##' . $fullName);
    };
  }
}
