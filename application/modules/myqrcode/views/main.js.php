<script type="text/javascript">
  $(document).ready(function() {
    var _section = "myqrcode";
    var iframe = document.getElementById("qrcode-frame");

    if (iframe) {
      iframe.onload = function() {
        // Handle iframe content style
        $("#qrcode-frame").contents().find("body").css("display", "flex");
        $("#qrcode-frame").contents().find("body").css("align-items", "center");
        $("#qrcode-frame").contents().find("body").css("justify-content", "center");
        $("#qrcode-frame").contents().find("img").css("width", "100%");
        $("#qrcode-frame").contents().find("img").css("max-width", "300px");

        $(".loader").hide();
      };
    };

    // Handle generate submit
    $("#" + _section + " .page-action-filter").on("click", function() {
      var filter_kelas = $(".filter-kelas").val();
      var filter_mapel = $(".filter-mapel").val();
      var filter_mapelText = $(".filter-mapel option:selected").text();

      if (filter_kelas !== null && filter_mapel !== null) {
        if (iframe) {
          $(".loader").show();

          filter_kelas = filter_kelas.replace("#", "$");
          $(iframe).attr("src", "<?= base_url('myqrcode/generate/') ?>" + "?kelas=" + filter_kelas + "&mapel=" + filter_mapel);
          $(".qrcode-result").show();
          $(".qrcode-result .qrcode-result-kelas").html(filter_kelas.replace("$", " "));
          $(".qrcode-result .qrcode-result-mapel").html(filter_mapelText);
        } else {
          notify("Object is not found!", "danger");
        };
      } else {
        notify("Kelas & mata pelajaran tidak boleh kosong!", "danger");
      };
    });
  });
</script>