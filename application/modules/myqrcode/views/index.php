<style type="text/css">
    .iframe {
        border: 0;
        padding: 0;
        margin: 0;
        width: 100%;
        height: 500px;
        background-color: #000;
    }

    .content-center {
        display: flex;
        align-items: center;
        justify-content: center;
    }
</style>

<section id="myqrcode">
    <div class="row">
        <div class="col-xs-10 col-md-10">
            <h4 class="card-title"><?php echo (isset($card_title)) ? $card_title : '' ?></h4>
            <h6 class="card-subtitle"><?php echo (isset($card_subTitle)) ? $card_subTitle : '' ?></h6>
            <div class="clear-card"></div>
        </div>
    </div>
    <?php if ($this->session->userdata('user')['role'] === 'Guru') : ?>
        <div class="card mb-0">
            <div class="card-body">
                <div class="table-action row">
                    <div class="buttons col pt-0">
                        <div class="input-group mb-0">
                            <div class="input-group-prepend">
                                <label class="input-group-text" style="height: 34.13px; background: #f2f2f2;">Kelas</label>
                            </div>
                            <select class="custom-select filter-kelas" style="height: 34.13px; max-width: 230px;">
                                <?= $list_kelas ?>
                            </select>
                            <select class="custom-select filter-mapel" style="height: 34.13px; max-width: 250px;">
                                <?= $list_mapel ?>
                            </select>
                            <div class="input-group-apend">
                                <button class="btn btn--raised btn-primary btn--icon-text page-action-filter" style="height: 34.13px;">
                                    <i class="zmdi zmdi-filter-list"></i> Generate
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="loader" style="display: none;">
            <div style="text-align: center;">
                <div class="lds-ellipsis">
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
            </div>
        </div>
    <?php endif ?>
    <div class="card bg-black">
        <?php if ($this->session->userdata('user')['role'] === 'Guru') : ?>
            <div class="alert alert-success qrcode-result" style="display: none;">
                <i class="zmdi zmdi-info"></i>
                QR-Code digenerate utuk kelas <b><span class="qrcode-result-kelas">NULL</span></b> (<span class="qrcode-result-mapel">NULL</span>)
            </div>
        <?php endif ?>
        <div class="card-body">
            <iframe src="<?= base_url('myqrcode/generate') ?>" id="qrcode-frame" class="iframe"></iframe>
        </div>
    </div>
</section>