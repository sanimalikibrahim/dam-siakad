<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . 'controllers/AppBackend.php');
require_once(APPPATH . 'controllers/ApiClient.php');
require_once(FCPATH . 'vendor/autoload.php');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Calonsantri extends AppBackend
{
  function __construct()
  {
    parent::__construct();
    $this->load->model([
      'AppModel',
      'UserModel',
      'PsbModel',
      'PsbprestasiModel',
      'PsbdokumenModel',
      'ProvincesModel',
      'RegenciesModel',
      'PostModel',
      'MappingRuanganModel',
    ]);
    $this->load->library('form_validation');
  }

  private function _sendMail($psb, $notifDesc, $notifSubject)
  {
    $app = $this->app();

    $template = '
      <div style="background: #fff; padding: 1.5rem; border: 4px solid #f2f2f2;">
        <h4>Assalamualaikum, ' . $psb->nama_lengkap . '</h4>
        <p>' . $notifDesc . '</p>
        <hr />
        <i>Panitia PSB</i> <br/>
        <i>Pondok Pesantren Darul Arqam Muhammadiyah Garut</i>
      </div>
    ';

    $mailParams = array(
      'receiver' => $psb->email,
      'subject' => $notifSubject . ' | ' . $app->app_name,
      'message' => $template
    );

    return $this->sendMail($mailParams);
  }

  private function _sendNotifMail($id, $status, $psb)
  {
    // Informasi Lulus Ujian
    $info_lulusUjian = $this->PostModel->getDetail(['id' => 7]);
    $info_lulusUjian = !is_null($info_lulusUjian) ? $info_lulusUjian->konten : '';
    $info_lulusUjian = str_replace('{base_url}', base_url(), $info_lulusUjian);

    // Informasi Tidak Lulus Ujian
    $info_tidakLulusUjian = $this->PostModel->getDetail(['id' => 8]);
    $info_tidakLulusUjian = !is_null($info_tidakLulusUjian) ? $info_tidakLulusUjian->konten : '';
    $info_tidakLulusUjian = str_replace('{base_url}', base_url(), $info_tidakLulusUjian);

    // Ucapan Selamat Untuk Santri Baru
    $info_ucapanSelamat = $this->PostModel->getDetail(['id' => 9]);
    $info_ucapanSelamat = !is_null($info_ucapanSelamat) ? $info_ucapanSelamat->konten : '';
    $info_ucapanSelamat = str_replace('{base_url}', base_url(), $info_ucapanSelamat);

    switch ((int) $status) {
      case 0:
        $notifSubject = 'Info Pembayaran';
        $notifRef = 'psb_belum_bayar';
        $notifDesc = 'Untuk melengkapi persyaratan, silahkan lakukan pembayaran administrasi pendaftaran.';
        $notifDescMail = 'Untuk melengkapi persyaratan, silahkan lakukan pembayaran administrasi pendaftaran. <a href="' . base_url('post/detail/3') . '">Klik disini</a> untuk informasi lebih lanjut.';
        $notifLink = 'post/detail/3';
        break;
      case 1:
        $notifSubject = 'Pembayaran Pendaftaran Diterima';
        $notifRef = 'psb_buktibayar_pendaftaran';
        $notifDesc = 'Terima kasih, pembayaran pendaftaran ananda telah kami terima.';
        $notifDescMail = $notifDesc;
        $notifLink = 'profile/calon-santri';
        break;
      case 2:
        $notifSubject = 'Informasi Kelulusan';
        $notifRef = 'psb_tidak_lulus';
        $notifDesc = 'Maaf! Berdasarkan hasil tes, ananda dinyatakan Tidak Lulus.';
        $notifDescMail = $info_tidakLulusUjian;
        $notifLink = 'profile/calon-santri';
        break;
      case 3:
        $notifSubject = 'Informasi Kelulusan';
        $notifRef = 'psb_lulus';
        $notifDesc = 'Selamat! Berdasarkan hasil tes, ananda dinyatakan Lulus.';
        $notifDescMail = $info_lulusUjian;
        $notifLink = 'post/detail/1';
        break;
      case 4:
        $notifSubject = 'Pembayaran Daftar Ulang Diterima';
        $notifRef = 'psb_buktibayar_daftarulang';
        $notifDesc = 'Selamat! Ananda saat ini sudah menjadi Santri.';
        $notifDescMail = $info_ucapanSelamat;
        $notifLink = '/';
        break;
      default:
        $notifSubject = 'Panitia PSB';
        $notifRef = 'psb';
        $notifDesc = 'Undefined';
        $notifDescMail = $notifDesc;
        $notifLink = 'profile/calon-santri';
        break;
    };

    // Send mail
    $this->_sendMail($psb, $notifDescMail, $notifSubject);

    // Notification
    $notifParams = array(
      'user_from' => $this->session->userdata('user')['id'],
      'user_to' => $id,
      'ref' => $notifRef,
      'ref_id' => $id,
      'description' => $notifDesc,
      'link' => $notifLink
    );
    $this->set_notification($notifParams);
    // END ## Notification
  }

  private function _storeToLearndash($user = null)
  {
    $apiClient = new ApiClient();
    $response = $apiClient->storeUserToLearndash($user->learndash_user_id, [
      'username' => $user->username,
      'password' => (!empty($user->learndash_user_ref) && !is_null($user->learndash_user_ref)) ? $user->learndash_user_ref : $user->username,
      'email' => $user->email,
      'name' => $user->nama_lengkap,
      'first_name' => $user->nama_lengkap,
      'last_name' => ''
    ]);

    return $response;
  }

  private function _deleteFromLearndash($calonSantri = null)
  {
    $apiClient = new ApiClient();
    $response = $apiClient->deleteUser($calonSantri->learndash_user_id);

    return $response;
  }

  public function getStatus($status = 0)
  {
    return $this->PsbModel->getStatus($status);
  }

  public function index()
  {
    $data = array(
      'app' => $this->app(),
      'main_js' => $this->load_main_js('calonsantri', false, [
        'app' => $this->app()
      ]),
      'card_title' => 'Calon Santri',
      'card_subTitle' => 'Pendaftaran Santri Baru',
      'list_tahun' => $this->init_list($this->PsbModel->getTahunList(), 'tahun', 'tahun'),
    );
    $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
    $this->template->load_view('index', $data, TRUE);
    $this->template->render();
  }

  public function update($id = null)
  {
    $role = $this->session->userdata('user')['role'];
    $id = ($role === 'Calon Santri') ? $this->session->userdata('user')['id'] : $id;
    $psb = $this->PsbModel->getDetail(['id' => $id]);

    if (!is_null($psb)) {
      $psbPrestasi = $this->PsbprestasiModel->getAll(['psb_id' => $id]);
      $psbDokumen = $this->PsbdokumenModel->getDetail(['psb_id' => $id]);
      $komitmen = $this->PostModel->getDetail(['id' => 2]);
      $seragamPutra = $this->PostModel->getDetail(['id' => 12]);
      $seragamPutri = $this->PostModel->getDetail(['id' => 13]);

      $data = array(
        'controller' => $this,
        'app' => $this->app(),
        'main_js' => $this->load_main_js('calonsantri'),
        'card_title' => ($role === 'Calon Santri') ? 'Profil Saya' : 'Calon Santri',
        'card_subTitle' => 'Untuk merubah data akun silahkan masuk ke pengaturan akun',
        'psb' => $psb,
        'psb_prestasi' => $psbPrestasi,
        'psb_dokumen' => $psbDokumen,
        'komitmen' => $komitmen,
        'seragam_putra' => $seragamPutra,
        'seragam_putri' => $seragamPutri,
        'list_provinces' => $this->init_list($this->ProvincesModel->getAll(), 'name', 'name', $psb->provinsi),
        'list_regencies' => $this->init_list($this->RegenciesModel->getAll(), 'name', 'name', $psb->kota),
        'list_regencies_sekolah_asal' => $this->init_list($this->RegenciesModel->getAll(), 'name', 'name', $psb->sekolah_asal_alamat),
      );
      $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
      $this->template->load_view('form', $data, TRUE);
      $this->template->render();
    } else {
      show_404();
    };
  }

  public function ajax_get_all($year = null)
  {
    $this->handle_ajax_request();

    $dtAjax_config = array(
      'table_name' => 'view_psb',
      'order_column' => 7,
      'order_column_dir' => 'asc',
      'static_conditional' => array(
        'tahun' => $year
      ),
    );

    $response = $this->AppModel->getData_dtAjax($dtAjax_config);
    echo json_encode($response);
  }

  public function ajax_save()
  {
    $this->handle_ajax_request();

    $id = $this->input->post('id');

    if (!is_null($id)) {
      $this->form_validation->set_rules($this->PsbModel->rules($id));

      if ($this->form_validation->run() === true) {
        // Upload surat 1
        if (!empty($_FILES['surat_1']['name'])) {
          $cpUpload = new CpUpload();
          $upload = $cpUpload->run('surat_1', 'psb_surat', true, true, 'jpg|jpeg|png|gif');

          $_POST['surat_1'] = '';

          if ($upload->status === true) {
            $_POST['surat_1'] = $upload->data->base_path;
          };
        };

        // Upload surat 2
        if (!empty($_FILES['surat_2']['name'])) {
          $cpUpload = new CpUpload();
          $upload = $cpUpload->run('surat_2', 'psb_surat', true, true, 'jpg|jpeg|png|gif');

          $_POST['surat_2'] = '';

          if ($upload->status === true) {
            $_POST['surat_2'] = $upload->data->base_path;
          };
        };

        $transaction = $this->PsbModel->update($id);
        $response = null;

        if ($transaction['status'] === true) {
          // Save prestasi
          $transactionPrestasi = $this->_save_prestasi($transaction['psb_id']);

          if ($transactionPrestasi['status'] === true) {
            // Save kelengkapan dokumen
            if ((int) $transaction['psb_status'] === 3) {
              $transactionDokumen = $this->_save_kelengkapan_dokumen($transaction['psb_id']);

              if ($transactionDokumen['status'] === true) {
                $response = $transaction;
              } else {
                $response = $transactionDokumen;
              };
            } else {
              $response = $transaction;
            };
          } else {
            $response = $transactionPrestasi;
          };
        } else {
          $response = $transaction;
        };

        echo json_encode($response);
      } else {
        $errors = validation_errors('<div>- ', '</div>');
        echo json_encode(array('status' => false, 'data' => $errors));
      };
    } else {
      echo json_encode(array('status' => false, 'data' => 'The ID field is required.'));
    };
  }

  private function _save_kelengkapan_dokumen($psb_id = null)
  {
    if (!is_null($psb_id) && !empty($psb_id)) {
      try {
        $currentData = $this->PsbdokumenModel->getDetail(['psb_id' => $psb_id]);
        $directory = 'psb_dokumen';
        $nisn = null;
        $nisn_current = (!is_null($currentData)) ? $currentData->nisn : null;
        $akteKelahiran = null;
        $akteKelahiran_current = (!is_null($currentData)) ? $currentData->akte_kelahiran : null;
        $kartuKeluarga = null;
        $kartuKeluarga_current = (!is_null($currentData)) ? $currentData->kartu_keluarga : null;
        $ijazahSD = null;
        $skhun = null;
        $bukuLaporan = null;
        $kip = null;
        $acceptPeraturan = $this->input->post('accept_aturan');
        $acceptOrangTua = $this->input->post('accept_ortu');
        $error = '';

        // Validate
        if (empty($_FILES['nisn']['name']) && is_null($nisn_current)) {
          $error .= '<div>- Kartu NISN (Nomor Induk Siswa Nasional) is required.</div>';
        };
        if (empty($_FILES['akte_kelahiran']['name']) && is_null($akteKelahiran_current)) {
          $error .= '<div>- Akte Kelahiran is required.</div>';
        };
        if (empty($_FILES['kartu_keluarga']['name']) && is_null($kartuKeluarga_current)) {
          $error .= '<div>- Kartu Keluarga is required.</div>';
        };
        if (is_null($acceptPeraturan)) {
          $error .= '<div>- Ananda diwajibkan untuk mentaati peraturan dan tata terbit pondok.</div>';
        };
        if (is_null($acceptOrangTua)) {
          $error .= '<div>- Orang tua diwajibkan untuk memenuhi perjanjian/ketentuan yang berlaku.</div>';
        };

        if (empty($error)) {
          // NISN (mandatori)
          if (!empty($_FILES['nisn']['name'])) {
            $cpUpload = new CpUpload();
            $upload = $cpUpload->run('nisn', $directory, true, true, 'jpg|jpeg|png|gif');

            if ($upload->status === true) {
              $nisn = $upload->data->base_path;
            } else {
              $error .= '<div>- Kartu NISN (Nomor Induk Siswa Nasional): ' . $upload->data . '</div>';
            };
          };

          // Akte Kelahiran (mandatori)
          if (!empty($_FILES['akte_kelahiran']['name'])) {
            $cpUpload = new CpUpload();
            $upload = $cpUpload->run('akte_kelahiran', $directory, true, true, 'jpg|jpeg|png|gif');

            if ($upload->status === true) {
              $akteKelahiran = $upload->data->base_path;
            } else {
              $error .= '<div>- Akte Kelahiran: ' . $upload->data . '</div>';
            };
          };

          // Kartu Keluarga (mandatori)
          if (!empty($_FILES['kartu_keluarga']['name'])) {
            $cpUpload = new CpUpload();
            $upload = $cpUpload->run('kartu_keluarga', $directory, true, true, 'jpg|jpeg|png|gif');

            if ($upload->status === true) {
              $kartuKeluarga = $upload->data->base_path;
            } else {
              $error .= '<div>- Kartu Keluarga: ' . $upload->data . '</div>';
            };
          };

          // Ijazah SD/MI
          if (!empty($_FILES['ijazah_sd']['name'])) {
            $cpUpload = new CpUpload();
            $upload = $cpUpload->run('ijazah_sd', $directory, true, true, 'jpg|jpeg|png|gif');

            if ($upload->status === true) {
              $ijazahSD = $upload->data->base_path;
            } else {
              $error .= '<div>- Ijazah SD/MI: ' . $upload->data . '</div>';
            };
          };

          // SKHUN
          if (!empty($_FILES['skhun']['name'])) {
            $cpUpload = new CpUpload();
            $upload = $cpUpload->run('skhun', $directory, true, true, 'jpg|jpeg|png|gif');

            if ($upload->status === true) {
              $skhun = $upload->data->base_path;
            } else {
              $error .= '<div>- SKHUN SD/MI: ' . $upload->data . '</div>';
            };
          };

          // Buku Laporan Pendidikan SD/MI
          if (!empty($_FILES['buku_laporan']['name'])) {
            $cpUpload = new CpUpload();
            $upload = $cpUpload->run('buku_laporan', $directory, true, true, 'pdf|docx');

            if ($upload->status === true) {
              $bukuLaporan = $upload->data->base_path;
            } else {
              $error .= '<div>- Buku Laporan Pendidikan SD/MI: ' . $upload->data . '</div>';
            };
          };

          // KIP
          if (!empty($_FILES['kip']['name'])) {
            $cpUpload = new CpUpload();
            $upload = $cpUpload->run('kip', $directory, true, true, 'jpg|jpeg|png|gif');

            if ($upload->status === true) {
              $kip = $upload->data->base_path;
            } else {
              $error .= '<div>- KIP (Kartu Indonesia Pintar): ' . $upload->data . '</div>';
            };
          };

          if (empty($error)) {
            $post = array(
              'psb_id' => $psb_id,
              'nisn' => $nisn,
              'akte_kelahiran' => $akteKelahiran,
              'kartu_keluarga' => $kartuKeluarga,
              'ijazah_sd' => $ijazahSD,
              'skhun' => $skhun,
              'buku_laporan' => $bukuLaporan,
              'kip' => $kip,
              'accept_aturan' => 1,
              'accept_ortu' => 1
            );

            if (is_null($currentData)) {
              return $this->PsbdokumenModel->insert($post);
            } else {
              return $this->PsbdokumenModel->update($post);
            };
          } else {
            return array('status' => false, 'data' => $error);
          };
        } else {
          return array('status' => false, 'data' => $error);
        };
      } catch (\Throwable $th) {
        return array('status' => false, 'data' => 'An error while uploading kelengkapan dokumen.');
      };
    } else {
      return array('status' => true, 'data' => 'No file to upload.');
    };
  }

  private function _save_prestasi($psb_id = null)
  {
    $prestasi = $this->input->post('prestasi');

    if (!empty($psb_id) && !is_null($psb_id) && !is_null($prestasi)) {
      $cpUpload = new CpUpload();

      $prestasi = $cpUpload->re_arrange($prestasi);
      $prestasiFile = $_FILES['prestasi'];
      $prestasiFile = $cpUpload->re_arrange($prestasiFile);
      $prestasiFile = $prestasiFile['sertifikat'];
      $prestasiFile = $cpUpload->re_arrange($prestasiFile);

      $directory = 'psb';
      $post = array();
      $error = '';

      foreach ($prestasiFile as $index => $item) {
        if (!empty($item['name'])) {
          $upload = $cpUpload->run($item, $directory, true, true, 'jpg|jpeg|png|gif|pdf', true);

          if ($upload->status === true) {
            $post[] = array(
              'psb_id' => $psb_id,
              'nama_lomba' => $prestasi[$index]['nama_lomba'],
              'jenis_lomba' => $prestasi[$index]['jenis_lomba'],
              'penyelenggara' => $prestasi[$index]['penyelenggara'],
              'tingkat' => $prestasi[$index]['tingkat'],
              'peringkat' => $prestasi[$index]['peringkat'],
              'tahun' => $prestasi[$index]['tahun'],
              'file_raw_name' => $upload->data->raw_name . $upload->data->file_ext,
              'file_raw_name_thumb' => ($upload->data->is_image) ? $upload->data->raw_name . '_thumb' . $upload->data->file_ext : null,
              'file_name' => $upload->data->base_path,
              'file_name_thumb' => ($upload->data->is_image) ? 'directory/' . $directory . '/' . $upload->data->raw_name . '_thumb' . $upload->data->file_ext : null,
              'file_size' => $upload->data->file_size
            );
          } else {
            $error .= $upload->data;
          };
        };
      };

      if (empty($error) && count($post) > 0) {
        return $this->PsbprestasiModel->insertBatch($post);
      } else {
        return array('status' => false, 'data' => $error);
      };
    } else {
      return array('status' => true, 'data' => 'No file to upload.');
    };
  }

  public function ajax_set_status_old($id, $status)
  {
    $this->handle_ajax_request();
    echo json_encode($this->PsbModel->setStatus($id, $status));
  }

  public function ajax_set_status($id, $status)
  {
    $this->handle_ajax_request();
    $psb = $this->PsbModel->getDetail(['id' => $id]);
    $user = $this->UserModel->getDetail(['id' => $id]);

    if (!is_null($psb)) {
      $syncStatus = true;

      // Delete user from learndash-wp if exist
      if ((int) $status === 0) {
        $this->_deleteFromLearndash($user);
      };

      // Store to learndash-wp if payment has been received
      if ((int) $status === 1) {
        $storeToLearndash = $this->_storeToLearndash($user);

        if ($storeToLearndash['status'] === true) {
          $syncStatus = true;

          // Update meta in user
          $learndashUserId = (isset($storeToLearndash['data']) && isset($storeToLearndash['data']->id)) ? $storeToLearndash['data']->id : null;
          $this->UserModel->setLearndashId($id, $learndashUserId);
        } else {
          $syncStatus = false;
          $response = $storeToLearndash;
        };
      };

      if ($syncStatus === true) {
        $setStatus = $this->PsbModel->setStatus($id, $status);

        if ($setStatus['status'] === true) {
          $this->_sendNotifMail($id, $status, $psb);
          $response = $setStatus;
        } else {
          $response = $setStatus;
        };
      };
    } else {
      $response = ['status' => false, 'data' => 'Calon santri dengan ID: ' . $id . ' tidak ditemukan.'];
    };

    echo json_encode($response);
  }

  public function ajax_delete($id)
  {
    $this->handle_ajax_request();
    echo json_encode($this->PsbModel->delete($id));
  }

  public function ajax_delete_prestasi($id)
  {
    $this->handle_ajax_request();
    echo json_encode($this->PsbprestasiModel->delete($id));
  }

  public function ajax_send_notif($id)
  {
    $this->handle_ajax_request();
    $psb = $this->PsbModel->getDetail(['id' => $id]);

    if (!is_null($psb)) {
      try {
        $this->_sendNotifMail($id, 0, $psb);

        $response = ['status' => true, 'data' => 'The notification was successfully sent to ' . $psb->nama_lengkap . '.'];
      } catch (\Throwable $th) {
        $response = ['status' => false, 'data' => 'Terjadi kesalahan ketika mengirim email, coba lagi nanti atau hubungi Administrator.'];
      };
    } else {
      $response = ['status' => false, 'data' => 'Calon santri dengan ID: ' . $id . ' tidak ditemukan.'];
    };

    echo json_encode($response);
  }

  public function ajax_generate_idcard($id = null)
  {
    $this->handle_ajax_request();
    $psb = $this->PsbModel->getDetail(['id' => $id]);
    $mappingRuangan = $this->MappingRuanganModel->getRuanganByPsb($id);

    if (!is_null($psb)) {
      try {
        $data = array(
          'app' => $this->app(),
          'idcard' => $this->_intializeIdCardStyle(),
          'psb' => $psb,
          'ruangan' => $mappingRuangan,
        );
        $dom = $this->load->view('generate_idcard', $data, true);

        echo json_encode(['status' => true, 'data' => 'ID Card berhasil dibuat.', 'dom' => $dom]);
      } catch (\Throwable $th) {
        echo json_encode(['status' => false, 'data' => 'Terjadi kesalahan ketika membuat ID Card.']);
      };
    } else {
      echo json_encode(['status' => false, 'data' => 'Calon santri dengan ID: ' . $id . ' tidak ditemukan.']);
    };
  }

  private function _intializeIdCardStyle()
  {
    $app = $this->app();
    $idcard = array(
      'idcard_psb_background' => (isset($app->idcard_psb_background)) ? $app->idcard_psb_background : '',
      'idcard_psb_nomor_top' => (isset($app->idcard_psb_nomor_top)) ? $app->idcard_psb_nomor_top . 'px' : '',
      'idcard_psb_nomor_left' => (isset($app->idcard_psb_nomor_left)) ? $app->idcard_psb_nomor_left . 'px' : '',
      'idcard_psb_nomor_font_size' => (isset($app->idcard_psb_nomor_font_size)) ? $app->idcard_psb_nomor_font_size . 'px' : '',
      'idcard_psb_nomor_color' => (isset($app->idcard_psb_nomor_color)) ? $app->idcard_psb_nomor_color : '',
      'idcard_psb_nama_top' => (isset($app->idcard_psb_nama_top)) ? $app->idcard_psb_nama_top . 'px' : '',
      'idcard_psb_nama_left' => (isset($app->idcard_psb_nama_left)) ? $app->idcard_psb_nama_left . 'px' : '',
      'idcard_psb_nama_font_size' => (isset($app->idcard_psb_nama_font_size)) ? $app->idcard_psb_nama_font_size . 'px' : '',
      'idcard_psb_nama_color' => (isset($app->idcard_psb_nama_color)) ? $app->idcard_psb_nama_color : '',
      'idcard_psb_alamat_top' => (isset($app->idcard_psb_alamat_top)) ? $app->idcard_psb_alamat_top . 'px' : '',
      'idcard_psb_alamat_left' => (isset($app->idcard_psb_alamat_left)) ? $app->idcard_psb_alamat_left . 'px' : '',
      'idcard_psb_alamat_font_size' => (isset($app->idcard_psb_alamat_font_size)) ? $app->idcard_psb_alamat_font_size . 'px' : '',
      'idcard_psb_alamat_color' => (isset($app->idcard_psb_alamat_color)) ? $app->idcard_psb_alamat_color : '',
      'idcard_psb_ruangan1_top' => (isset($app->idcard_psb_ruangan1_top)) ? $app->idcard_psb_ruangan1_top . 'px' : '',
      'idcard_psb_ruangan1_left' => (isset($app->idcard_psb_ruangan1_left)) ? $app->idcard_psb_ruangan1_left . 'px' : '',
      'idcard_psb_ruangan1_font_size' => (isset($app->idcard_psb_ruangan1_font_size)) ? $app->idcard_psb_ruangan1_font_size . 'px' : '',
      'idcard_psb_ruangan1_color' => (isset($app->idcard_psb_ruangan1_color)) ? $app->idcard_psb_ruangan1_color : '',
      'idcard_psb_ruangan2_top' => (isset($app->idcard_psb_ruangan2_top)) ? $app->idcard_psb_ruangan2_top . 'px' : '',
      'idcard_psb_ruangan2_left' => (isset($app->idcard_psb_ruangan2_left)) ? $app->idcard_psb_ruangan2_left . 'px' : '',
      'idcard_psb_ruangan2_font_size' => (isset($app->idcard_psb_ruangan2_font_size)) ? $app->idcard_psb_ruangan2_font_size . 'px' : '',
      'idcard_psb_ruangan2_color' => (isset($app->idcard_psb_ruangan2_color)) ? $app->idcard_psb_ruangan2_color : '',
      'idcard_psb_foto_top' => (isset($app->idcard_psb_foto_top)) ? $app->idcard_psb_foto_top . 'px' : '',
      'idcard_psb_foto_left' => (isset($app->idcard_psb_foto_left)) ? $app->idcard_psb_foto_left . 'px' : '',
      'idcard_psb_foto_show' => (isset($app->idcard_psb_foto_show)) ? $app->idcard_psb_foto_show : 'none',
      'idcard_psb_foto_width' => (isset($app->idcard_psb_foto_width)) ? $app->idcard_psb_foto_width . 'px' : '',
      'idcard_psb_foto_height' => (isset($app->idcard_psb_foto_height)) ? $app->idcard_psb_foto_height . 'px' : '',
    );

    return (object) $idcard;
  }

  public function download_excel($id = null)
  {
    if (!is_null($id)) {
      $calonSantri = $this->PsbModel->getDetail(array('id' => $id));

      if (!is_null($calonSantri)) {
        $fileTemplate = FCPATH . 'directory/templates/template-calon_santri.xlsx';
        $fileName = 'Calon_Santri-' . $calonSantri->nisn . '.xlsx';

        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        $spreadsheet = $reader->load($fileTemplate);

        $ayah_tanggal_lahir = (!is_null($calonSantri->ayah_tanggal_lahir) && $calonSantri->ayah_tanggal_lahir != '0000-00-00') ? date('d/m/Y', strtotime($calonSantri->ayah_tanggal_lahir)) : '';
        $ibu_tanggal_lahir = (!is_null($calonSantri->ibu_tanggal_lahir) && $calonSantri->ibu_tanggal_lahir != '0000-00-00') ? date('d/m/Y', strtotime($calonSantri->ibu_tanggal_lahir)) : '';
        $wali_tanggal_lahir = (!is_null($calonSantri->wali_tanggal_lahir) && $calonSantri->wali_tanggal_lahir != '0000-00-00') ? date('d/m/Y', strtotime($calonSantri->wali_tanggal_lahir)) : '';

        $spreadsheet->setActiveSheetIndex(0)
          // Santri
          ->setCellValue('C8', $calonSantri->nama_lengkap)
          ->setCellValue('C13', $calonSantri->tempat_lahir)
          ->setCellValue('S13', date('d/m/Y', strtotime($calonSantri->tanggal_lahir)))
          ->setCellValue('C15', $calonSantri->jenis_kelamin)
          ->setCellValue('C17', $calonSantri->urutan_anak)
          ->setCellValue('I17', $calonSantri->jumlah_saudara)
          ->setCellValue('C19', $calonSantri->golongan_darah)
          ->setCellValue('C21', $calonSantri->nik)
          ->setCellValue('C23', $calonSantri->npsn)
          ->setCellValue('C25', $calonSantri->nim)
          ->setCellValue('C27', $calonSantri->nisn)
          ->setCellValue('C29', $calonSantri->alamat_santri)
          ->setCellValue('C31', $calonSantri->kelurahan)
          ->setCellValue('C33', $calonSantri->kecamatan)
          ->setCellValue('C35', $calonSantri->kota)
          ->setCellValue('C37', $calonSantri->provinsi)
          ->setCellValue('E40', $calonSantri->telepon)
          ->setCellValue('Q40', $calonSantri->handphone)
          ->setCellValue('C42', $calonSantri->tempat_tinggal)
          ->setCellValue('C44', $calonSantri->sekolah_asal)
          ->setCellValue('C46', $calonSantri->sekolah_asal_alamat)
          // Ayah
          ->setCellValue('C53', $calonSantri->ayah_no_kk)
          ->setCellValue('C55', $calonSantri->ayah_nama_lengkap)
          ->setCellValue('C57', $calonSantri->ayah_tempat_lahir)
          ->setCellValue('S57', $ayah_tanggal_lahir)
          ->setCellValue('C60', $calonSantri->ayah_nik)
          ->setCellValue('C62', $calonSantri->ayah_status)
          ->setCellValue('C64', $calonSantri->ayah_pendidikan)
          ->setCellValue('C66', $calonSantri->ayah_pekerjaan)
          ->setCellValue('C70', $calonSantri->ayah_pekerjaan_jenis)
          ->setCellValue('C72', $calonSantri->ayah_pekerjaan_pangkat)
          ->setCellValue('C76', $calonSantri->ayah_alamat)
          ->setCellValue('C78', $calonSantri->ayah_kelurahan)
          ->setCellValue('C80', $calonSantri->ayah_kecamatan)
          ->setCellValue('C82', $calonSantri->ayah_kota)
          ->setCellValue('C84', $calonSantri->ayah_provinsi)
          ->setCellValue('E86', $calonSantri->ayah_telepon)
          ->setCellValue('Q86', $calonSantri->ayah_handphone)
          ->setCellValue('C88', $calonSantri->ayah_penghasilan)
          // Ibu
          ->setCellValue('C98', $calonSantri->ibu_nama_lengkap)
          ->setCellValue('C100', $calonSantri->ibu_tempat_lahir)
          ->setCellValue('S100', $ibu_tanggal_lahir)
          ->setCellValue('C102', $calonSantri->ibu_status)
          ->setCellValue('C104', $calonSantri->ibu_nik)
          ->setCellValue('C106', $calonSantri->ibu_pendidikan)
          ->setCellValue('C108', $calonSantri->ibu_pekerjaan)
          ->setCellValue('C112', $calonSantri->ibu_pekerjaan_jenis)
          ->setCellValue('C114', $calonSantri->ibu_pekerjaan_pangkat)
          ->setCellValue('C116', $calonSantri->ibu_alamat)
          ->setCellValue('C118', $calonSantri->ibu_kelurahan)
          ->setCellValue('C120', $calonSantri->ibu_kecamatan)
          ->setCellValue('C122', $calonSantri->ibu_kota)
          ->setCellValue('C124', $calonSantri->ibu_provinsi)
          ->setCellValue('E126', $calonSantri->ibu_telepon)
          ->setCellValue('Q126', $calonSantri->ibu_handphone)
          ->setCellValue('C128', $calonSantri->ibu_penghasilan)
          // Wali
          ->setCellValue('C139', $calonSantri->wali_no_kk)
          ->setCellValue('C141', $calonSantri->wali_nama_lengkap)
          ->setCellValue('C143', $calonSantri->wali_tempat_lahir)
          ->setCellValue('S143', $wali_tanggal_lahir)
          ->setCellValue('C145', $calonSantri->wali_status)
          ->setCellValue('C147', $calonSantri->wali_nik)
          ->setCellValue('C149', $calonSantri->wali_pendidikan)
          ->setCellValue('C151', $calonSantri->wali_pekerjaan)
          ->setCellValue('C155', $calonSantri->wali_pekerjaan_jenis)
          ->setCellValue('C157', $calonSantri->wali_pekerjaan_pangkat)
          ->setCellValue('C159', $calonSantri->wali_alamat)
          ->setCellValue('C161', $calonSantri->wali_kelurahan)
          ->setCellValue('C163', $calonSantri->wali_kecamatan)
          ->setCellValue('C165', $calonSantri->wali_kota)
          ->setCellValue('C167', $calonSantri->wali_provinsi)
          ->setCellValue('E169', $calonSantri->wali_telepon)
          ->setCellValue('Q169', $calonSantri->wali_handphone)
          ->setCellValue('C171', $calonSantri->wali_penghasilan)
          // Komitmen
          ->setCellValue('C186', $calonSantri->komitmen)
          ->setCellValue('C188', $calonSantri->komitmen_nominal)
          // TTD
          ->setCellValue('K193', $calonSantri->ayah_nama_lengkap);

        // Prestasi
        $prestasiData = $this->PsbprestasiModel->getAll(['psb_id' => $calonSantri->id]);

        if (count($prestasiData) > 0) {
          $prestasiStart = 179;
          $prestasiMax = 4;
          $prestasiCount = 1;

          foreach ($prestasiData as $prestasiIndex => $prestasi) {
            if ($prestasiCount <= $prestasiMax) {
              $spreadsheet->setActiveSheetIndex(0)
                ->setCellValue('A' . $prestasiStart, $prestasi->nama_lomba)
                ->setCellValue('B' . $prestasiStart, $prestasi->jenis_lomba)
                ->setCellValue('H' . $prestasiStart, $prestasi->penyelenggara)
                ->setCellValue('N' . $prestasiStart, $prestasi->tingkat)
                ->setCellValue('T' . $prestasiStart, $prestasi->peringkat)
                ->setCellValue('Y' . $prestasiStart, $prestasi->tahun);

              $prestasiStart++;
              $prestasiCount++;
            };
          };
        };
        // END ## Prestasi

        $styleArray = array(
          'borders' => array(
            'allBorders' => array(
              'style' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
              // 'color' => array('argb' => '000000'),
            ),
          ),
        );

        $spreadsheet->getActiveSheet()->getStyle('A1:' .
          $spreadsheet->getActiveSheet()->getHighestColumn() .
          $spreadsheet->getActiveSheet()->getHighestRow())->applyFromArray($styleArray);

        $writer = new Xlsx($spreadsheet);

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $fileName . '"');
        header('Cache-Control: max-age=0');

        $writer->save('php://output');
      } else {
        show_404();
      };
    } else {
      show_404();
    };
  }
}
