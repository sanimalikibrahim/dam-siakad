<!-- Prestasi -->
<fieldset>
  <div class="panel-prestasi panel-active">
    <div class="alert alert-light text-dark mb-3 text-center">
      <i class="zmdi zmdi-info"></i>
      Tidak perlu diisi jika memang tidak ada sertifikat / piagam
    </div>
    <!-- Button -->
    <div class="table-action">
      <div class="buttons" style="display: flex; align-items: center;">
        <button class="btn btn-success btn--icon-text prestasi-action-add">
          <i class="zmdi zmdi-plus-circle"></i> Tambah Data
        </button>
        <div class="form-prestasi-item-count text-bold ml-3 hidden" style="color: var(--green);">
          Jumlah Prestasi: 0
        </div>
      </div>
    </div>
    <!-- Temporary Form -->
    <div class="form-prestasi">
      <!-- Existing -->
      <?php if (count($psb_prestasi) > 0) : ?>
        <?php foreach ($psb_prestasi as $key => $item) : ?>
          <div class="panel-bordered mt-3 form-prestasi-item form-prestasi-item-<?= $item->id ?>">
            <div class="panel-bordered-header">
              Prestasi
              <a href="javascript:;" class="pull-right prestasi-action-delete-existing" style="color: var(--red);" title="Hapus" data-prestasi-key="<?= $item->id ?>">
                <i class="zmdi zmdi-close"></i>
              </a>
            </div>
            <div class="panel-bordered-body">
              <div class="row">
                <div class="col-md-6 col-12">
                  <div class="form-group">
                    <label required>Nama Lomba</label>
                    <p class="mb-0 form-control"><?= $item->nama_lomba ?></p>
                  </div>
                </div>
                <div class="col-md-6 col-12">
                  <div class="form-group">
                    <label>Jenis Lomba</label>
                    <p class="mb-0 form-control"><?= $item->jenis_lomba ?></p>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6 col-12">
                  <div class="form-group">
                    <label>Penyelenggara</label>
                    <p class="mb-0 form-control"><?= $item->penyelenggara ?></p>
                  </div>
                </div>
                <div class="col-md-6 col-12">
                  <div class="form-group">
                    <label>Tingkat</label>
                    <p class="mb-0 form-control"><?= $item->tingkat ?></p>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6 col-12">
                  <div class="form-group">
                    <label required>Peringkat</label>
                    <p class="mb-0 form-control"><?= $item->peringkat ?></p>
                  </div>
                </div>
                <div class="col-md-6 col-12">
                  <div class="form-group">
                    <label required>Tahun</label>
                    <p class="mb-0 form-control"><?= $item->tahun ?></p>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6 col-12">
                  <div class="form-group <?= (!$app->is_mobile) ? 'mb-0' : '' ?>">
                    <label required>Foto Sertifikat / Piagam</label>
                    <p class="mb-0 form-control">
                      <a href="<?= base_url($item->file_name) ?>" data-fancybox target="_blank">
                        <?= $item->file_raw_name ?>
                      </a>
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        <?php endforeach ?>
      <?php endif ?>
      <!-- END ## Existing -->
    </div>
  </div>
</fieldset>