<!-- Data Orang Tua : Ayah -->
<div class="row">
  <div class="col-md-6 col-12">
    <div class="form-group">
      <label>Nomor Kartu Keluarga (KK)</label>
      <input type="number" name="ayah_no_kk" class="form-control mask-number psb-ayah_no_kk" maxlength="16" placeholder="-" value="<?= $psb->ayah_no_kk ?>">
      <i class="form-group__bar"></i>
    </div>
  </div>
  <div class="col-md-6 col-12">
    <div class="form-group">
      <label>Nomor Induk Keluarga (NIK)</label>
      <input type="number" name="ayah_nik" class="form-control mask-number psb-ayah_nik" maxlength="16" placeholder="-" value="<?= $psb->ayah_nik ?>">
      <i class="form-group__bar"></i>
    </div>
  </div>
</div>
<div class="form-group">
  <label required>Nama Ayah Kandung</label>
  <input type="text" name="ayah_nama_lengkap" class="form-control psb-ayah_nama_lengkap" placeholder="-" value="<?= $psb->ayah_nama_lengkap ?>" required>
  <i class="form-group__bar"></i>
</div>
<div class="row">
  <div class="col-md-8 col-12">
    <div class="form-group">
      <label>Tempat Lahir</label>
      <input type="text" name="ayah_tempat_lahir" class="form-control psb-ayah_tempat_lahir" placeholder="-" value="<?= $psb->ayah_tempat_lahir ?>">
      <i class="form-group__bar"></i>
    </div>
  </div>
  <div class="col-md-4 col-12">
    <div class="form-group">
      <label>Tanggal Lahir</label>
      <input type="text" name="ayah_tanggal_lahir" class="form-control flatpickr-date psb-ayah_tanggal_lahir" placeholder="-" value="<?= ($psb->ayah_tanggal_lahir !== '0000-00-00') ? $psb->ayah_tanggal_lahir : '' ?>">
      <i class="form-group__bar"></i>
    </div>
  </div>
</div>
<div class="form-group">
  <label required>Status</label>
  <div class="pt-1">
    <div class="form-check form-check-inline">
      <input class="form-check-input" type="radio" name="ayah_status" id="ayah_status-hidup" value="Masih Hidup" <?= ($psb->ayah_status === 'Masih Hidup') ? 'checked' : '' ?>>
      <label class="form-check-label" for="ayah_status-hidup">Masih Hidup</label>
    </div>
    <div class="form-check form-check-inline">
      <input class="form-check-input" type="radio" name="ayah_status" id="ayah_status-sudah_meninggal" value="Sudah Meninggal" <?= ($psb->ayah_status === 'Sudah Meninggal') ? 'checked' : '' ?>>
      <label class="form-check-label" for="ayah_status-sudah_meninggal">Sudah Meninggal</label>
    </div>
  </div>
</div>
<div class="form-group">
  <label>Pendidikan Terakhir</label>
  <div class="select">
    <select name="ayah_pendidikan" class="form-control psb-ayah_pendidikan" data-placeholder="Select &#8595;">
      <option disabled selected>Select &#8595;</option>
      <option value="Tidak tamat SD/MI/Sederajat" <?= ($psb->ayah_pendidikan === 'Tidak tamat SD/MI/Sederajat') ? 'selected' : '' ?>>Tidak tamat SD/MI/Sederajat</option>
      <option value="SD/MI/Sederajat" <?= ($psb->ayah_pendidikan === 'SD/MI/Sederajat') ? 'selected' : '' ?>>SD/MI/Sederajat</option>
      <option value="SMP/MTs/Sederajat" <?= ($psb->ayah_pendidikan === 'SMP/MTs/Sederajat') ? 'selected' : '' ?>>SMP/MTs/Sederajat</option>
      <option value="SMA/MA/Sederajat" <?= ($psb->ayah_pendidikan === 'SMA/MA/Sederajat') ? 'selected' : '' ?>>SMA/MA/Sederajat</option>
      <option value="Diploma" <?= ($psb->ayah_pendidikan === 'Diploma') ? 'selected' : '' ?>>Diploma</option>
      <option value="Sarjana (S1)" <?= ($psb->ayah_pendidikan === 'Sarjana (S1)') ? 'selected' : '' ?>>Sarjana (S1)</option>
      <option value="Magister (S2)" <?= ($psb->ayah_pendidikan === 'Magister (S2)') ? 'selected' : '' ?>>Magister (S2)</option>
      <option value="Doktor (S3)" <?= ($psb->ayah_pendidikan === 'Doktor (S3)') ? 'selected' : '' ?>>Doktor (S3)</option>
    </select>
    <i class="form-group__bar"></i>
  </div>
</div>
<div class="form-group">
  <label required>Pekerjaan Utama</label>
  <div class="select">
    <select name="ayah_pekerjaan" class="form-control psb-ayah_pekerjaan" data-placeholder="Select &#8595;" required>
      <option disabled selected>Select &#8595;</option>
      <option value="Tidak Bekerja" <?= ($psb->ayah_pekerjaan === 'Tidak Bekerja') ? 'selected' : '' ?>>Tidak Bekerja</option>
      <option value="Pensiunan" <?= ($psb->ayah_pekerjaan === 'Pensiunan') ? 'selected' : '' ?>>Pensiunan</option>
      <option value="Karyawan Swasta" <?= ($psb->ayah_pekerjaan === 'Karyawan Swasta') ? 'selected' : '' ?>>Karyawan Swasta</option>
      <option value="TNI/POLRI" <?= ($psb->ayah_pekerjaan === 'TNI/POLRI') ? 'selected' : '' ?>>TNI/POLRI</option>
      <option value="Pengajar Honorer" <?= ($psb->ayah_pekerjaan === 'Pengajar Honorer') ? 'selected' : '' ?>>Pengajar Honorer</option>
      <option value="Buruh Tani" <?= ($psb->ayah_pekerjaan === 'Buruh Tani') ? 'selected' : '' ?>>Buruh Tani</option>
      <option value="Pengusaha" <?= ($psb->ayah_pekerjaan === 'Pengusaha') ? 'selected' : '' ?>>Pengusaha</option>
      <option value="Pedagang Eceran" <?= ($psb->ayah_pekerjaan === 'Pedagang Eceran') ? 'selected' : '' ?>>Pedagang Eceran</option>
      <option value="Sopir/Kondektur" <?= ($psb->ayah_pekerjaan === 'Sopir/Kondektur') ? 'selected' : '' ?>>Sopir/Kondektur</option>
      <option value="Nelayan" <?= ($psb->ayah_pekerjaan === 'Nelayan') ? 'selected' : '' ?>>Nelayan</option>
      <option value="Pekerja Pabrik" <?= ($psb->ayah_pekerjaan === 'Pekerja Pabrik') ? 'selected' : '' ?>>Pekerja Pabrik</option>
      <option value="Tukang Bangunan" <?= ($psb->ayah_pekerjaan === 'Tukang Bangunan') ? 'selected' : '' ?>>Tukang Bangunan</option>
      <option value="PNS" <?= ($psb->ayah_pekerjaan === 'PNS') ? 'selected' : '' ?>>PNS</option>
      <option value="Lainnya" <?= ($psb->ayah_pekerjaan === 'Lainnya') ? 'selected' : '' ?>>Lainnya</option>
    </select>
    <i class="form-group__bar"></i>
  </div>
</div>
<div class="row control-psb-ayah_pekerjaan <?= (!in_array($psb->ayah_pekerjaan, ['TNI/POLRI', 'PNS'])) ? 'hidden' : '' ?>">
  <div class="col-md-6 col-12">
    <div class="form-group">
      <label>Pekerjaan Utama: Jenis Profesi</label>
      <div class="select">
        <select name="ayah_pekerjaan_jenis" class="form-control psb-ayah_pekerjaan_jenis" data-placeholder="Select &#8595;">
          <option disabled selected>Select &#8595;</option>
          <option value="Pegawai Kementerian" <?= ($psb->ayah_pekerjaan_jenis === 'Pegawai Kementerian') ? 'selected' : '' ?>>Pegawai Kementerian</option>
          <option value="TNI/POLRI" <?= ($psb->ayah_pekerjaan_jenis === 'TNI/POLRI') ? 'selected' : '' ?>>TNI/POLRI</option>
          <option value="Guru/Dosen" <?= ($psb->ayah_pekerjaan_jenis === 'Guru/Dosen') ? 'selected' : '' ?>>Guru/Dosen</option>
          <option value="TU.Sekolah/Madrasah" <?= ($psb->ayah_pekerjaan_jenis === 'TU.Sekolah/Madrasah') ? 'selected' : '' ?>>TU.Sekolah/Madrasah</option>
          <option value="Pegawai Pemda" <?= ($psb->ayah_pekerjaan_jenis === 'Pegawai Pemda') ? 'selected' : '' ?>>Pegawai Pemda</option>
          <option value="Dokter/Perawat" <?= ($psb->ayah_pekerjaan_jenis === 'Dokter/Perawat') ? 'selected' : '' ?>>Dokter/Perawat</option>
          <option value="Lainnya" <?= ($psb->ayah_pekerjaan_jenis === 'Lainnya') ? 'selected' : '' ?>>Lainnya</option>
        </select>
        <i class="form-group__bar"></i>
      </div>
    </div>
  </div>
  <div class="col-md-6 col-12">
    <div class="form-group">
      <label>Pekerjaan Utama: Pangkat/Golongan</label>
      <div class="select">
        <select name="ayah_pekerjaan_pangkat" class="form-control psb-ayah_pekerjaan_pangkat" data-placeholder="Select &#8595;">
          <option disabled selected>Select &#8595;</option>
          <option value="Golongan I/II" <?= ($psb->ayah_pekerjaan_pangkat === 'Golongan I/II') ? 'selected' : '' ?>>Golongan I/II</option>
          <option value="Golongan III" <?= ($psb->ayah_pekerjaan_pangkat === 'Golongan III') ? 'selected' : '' ?>>Golongan III</option>
          <option value="Golongan IV" <?= ($psb->ayah_pekerjaan_pangkat === 'Golongan IV') ? 'selected' : '' ?>>Golongan IV</option>
          <option value="Tamtama" <?= ($psb->ayah_pekerjaan_pangkat === 'Tamtama') ? 'selected' : '' ?>>Tamtama</option>
          <option value="Bintara" <?= ($psb->ayah_pekerjaan_pangkat === 'Bintara') ? 'selected' : '' ?>>Bintara</option>
          <option value="Perwira" <?= ($psb->ayah_pekerjaan_pangkat === 'Perwira') ? 'selected' : '' ?>>Perwira</option>
        </select>
        <i class="form-group__bar"></i>
      </div>
    </div>
  </div>
</div>
<div class="form-group">
  <label>Alamat Tempat Tinggal Ayah</label>
  <input type="text" name="ayah_alamat" class="form-control psb-ayah_alamat" placeholder="-" value="<?= $psb->ayah_alamat ?>">
  <i class="form-group__bar"></i>
</div>
<div class="row">
  <div class="col-md-6 col-12">
    <div class="form-group">
      <label>Kampung/Desa/Kelurahan</label>
      <input type="text" name="ayah_kelurahan" class="form-control psb-ayah_kelurahan" placeholder="-" value="<?= $psb->ayah_kelurahan ?>">
      <i class="form-group__bar"></i>
    </div>
  </div>
  <div class="col-md-6 col-12">
    <div class="form-group">
      <label>Kecamatan</label>
      <input type="text" name="ayah_kecamatan" class="form-control psb-ayah_kecamatan" placeholder="-" value="<?= $psb->ayah_kecamatan ?>">
      <i class="form-group__bar"></i>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-6 col-12">
    <div class="form-group">
      <label>Kota/Kabupaten</label>
      <input type="text" name="ayah_kota" class="form-control psb-ayah_kota" placeholder="-" value="<?= $psb->ayah_kota ?>">
      <i class="form-group__bar"></i>
    </div>
  </div>
  <div class="col-md-6 col-12">
    <div class="form-group">
      <label>Provinsi</label>
      <input type="text" name="ayah_provinsi" class="form-control psb-ayah_provinsi" placeholder="-" value="<?= $psb->ayah_provinsi ?>">
      <i class="form-group__bar"></i>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-6 col-12">
    <div class="form-group">
      <label>Telepon</label>
      <input type="number" name="ayah_telepon" class="form-control mask-number psb-ayah_telepon" maxlength="15" placeholder="-" value="<?= $psb->ayah_telepon ?>">
      <i class="form-group__bar"></i>
    </div>
  </div>
  <div class="col-md-6 col-12">
    <div class="form-group">
      <label>Handphone</label>
      <input type="number" name="ayah_handphone" class="form-control mask-number psb-ayah_handphone" maxlength="15" placeholder="-" value="<?= $psb->ayah_handphone ?>">
      <i class="form-group__bar"></i>
    </div>
  </div>
</div>
<div class="form-group">
  <label>Penghasilan Ayah per Bulan</label>
  <div class="select">
    <select name="ayah_penghasilan" class="form-control psb-ayah_penghasilan" data-placeholder="Select &#8595;">
      <option disabled selected>Select &#8595;</option>
      <option value="< Rp.500.000,-" <?= ($psb->ayah_penghasilan === '< Rp.500.000,-') ? 'selected' : '' ?>>< Rp.500.000,-</option> 
      <option value="Rp. 500.000,- s.d. Rp.1.000.000,-" <?= ($psb->ayah_penghasilan === 'Rp. 500.000,- s.d. Rp.1.000.000,-') ? 'selected' : '' ?>>Rp. 500.000,- s.d. Rp.1.000.000,-</option>
      <option value="Rp.1.000.000,- s.d. Rp.2.000.000,-" <?= ($psb->ayah_penghasilan === 'Rp.1.000.000,- s.d. Rp.2.000.000,-') ? 'selected' : '' ?>>Rp.1.000.000,- s.d. Rp.2.000.000,-</option>
      <option value="Rp.2.000.000,- s.d. Rp.3.000.000,-"> <?= ($psb->ayah_penghasilan === 'Rp.2.000.000,- s.d. Rp.3.000.000,-') ? 'selected' : '' ?>Rp.2.000.000,- s.d. Rp.3.000.000,-</option>
      <option value="Rp.3.000.000,- s.d. Rp.5.000.000,-" <?= ($psb->ayah_penghasilan === 'Rp.3.000.000,- s.d. Rp.5.000.000,-') ? 'selected' : '' ?>>Rp.3.000.000,- s.d. Rp.5.000.000,-</option>
      <option value="> Rp.5.000.000,-" <?= ($psb->ayah_penghasilan === '> Rp.5.000.000,-') ? 'selected' : '' ?>>> Rp.5.000.000,-</option>
    </select>
    <i class="form-group__bar"></i>
  </div>
</div>