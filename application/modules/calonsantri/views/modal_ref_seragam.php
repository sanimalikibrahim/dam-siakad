<!-- Modal referensi seragam -->
<div class="modal fade" id="modal-ref-seragam">
  <div class="modal-dialog modal-dialog-centered modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title pull-left">Referensi Ukuran Seragam</h5>
      </div>
      <div class="modal-body">
        <ul class="nav nav-tabs" id="seragam-tab" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" id="seragam-putra-tab" data-toggle="tab" href="#seragam-putra" role="tab" aria-controls="seragam-putra" aria-selected="true">Putra</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" id="seragam-putri-tab" data-toggle="tab" href="#seragam-putri" role="tab" aria-controls="seragam-putri" aria-selected="false">Putri</a>
          </li>
        </ul>
        <div class="tab-content" id="seragam-content">
          <div class="tab-pane fade show active" id="seragam-putra" role="tabpanel" aria-labelledby="seragam-putra-tab">
            <?= isset($seragam_putra->konten) ? htmlspecialchars_decode($seragam_putra->konten) : '-' ?>
          </div>
          <div class="tab-pane fade" id="seragam-putri" role="tabpanel" aria-labelledby="seragam-putri-tab">
            <?= isset($seragam_putra->konten) ? htmlspecialchars_decode($seragam_putri->konten) : '-' ?>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>