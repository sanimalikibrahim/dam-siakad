<!-- Kelengkapan Dokumen -->
<fieldset>
  <div class="panel-dokumen panel-active">
    <div class="row">
      <div class="col-md-4 col-12">
        <label required>Kartu NISN (Nomor Induk Siswa Nasional)</label>
      </div>
      <div class="col-md-8 col-12">
        <div class="form-group">
          <div class="upload-inline-xs">
            <div class="upload-button">
              <input type="file" name="nisn" class="upload-pure-button psb-nisn" accept="image/jpg,image/jpeg,image/png,image/gif" data-preview="nisn" />
            </div>
            <div class="upload-preview data-preview-nisn">
              <?php if (isset($psb_dokumen->nisn) && !is_null($psb_dokumen->nisn) && !empty($psb_dokumen->nisn)) : ?>
                <p class="mb-1">
                  <a href="<?= base_url($psb_dokumen->nisn) ?>" class="btn btn-light btn-sm" data-fancybox>
                    <i class="zmdi zmdi-download"></i>
                    View Current
                  </a>
                </p>
                <p class="mb-0 text-muted">
                  To change file, please click the button beside
                </p>
              <?php endif ?>
            </div>
          </div>
          <small class="form-text text-muted">
            Use file with format .JPG, .JPEG, .PNG or .GIF
          </small>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-4 col-12">
        <label required>Akte Kelahiran</label>
      </div>
      <div class="col-md-8 col-12">
        <div class="form-group">
          <div class="upload-inline-xs">
            <div class="upload-button">
              <input type="file" name="akte_kelahiran" class="upload-pure-button psb-akte_kelahiran" accept="image/jpg,image/jpeg,image/png,image/gif" data-preview="akte_kelahiran" />
            </div>
            <div class="upload-preview data-preview-akte_kelahiran">
              <?php if (isset($psb_dokumen->akte_kelahiran) && !is_null($psb_dokumen->akte_kelahiran) && !empty($psb_dokumen->akte_kelahiran)) : ?>
                <p class="mb-1">
                  <a href="<?= base_url($psb_dokumen->akte_kelahiran) ?>" class="btn btn-light btn-sm" data-fancybox>
                    <i class="zmdi zmdi-download"></i>
                    View Current
                  </a>
                </p>
                <p class="mb-0 text-muted">
                  To change file, please click the button beside
                </p>
              <?php endif ?>
            </div>
          </div>
          <small class="form-text text-muted">
            Use file with format .JPG, .JPEG, .PNG or .GIF
          </small>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-4 col-12">
        <label required>Kartu Keluarga</label>
      </div>
      <div class="col-md-8 col-12">
        <div class="form-group">
          <div class="upload-inline-xs">
            <div class="upload-button">
              <input type="file" name="kartu_keluarga" class="upload-pure-button psb-kartu_keluarga" accept="image/jpg,image/jpeg,image/png,image/gif" data-preview="kartu_keluarga" />
            </div>
            <div class="upload-preview data-preview-kartu_keluarga">
              <?php if (isset($psb_dokumen->kartu_keluarga) && !is_null($psb_dokumen->kartu_keluarga) && !empty($psb_dokumen->kartu_keluarga)) : ?>
                <p class="mb-1">
                  <a href="<?= base_url($psb_dokumen->kartu_keluarga) ?>" class="btn btn-light btn-sm" data-fancybox>
                    <i class="zmdi zmdi-download"></i>
                    View Current
                  </a>
                </p>
                <p class="mb-0 text-muted">
                  To change file, please click the button beside
                </p>
              <?php endif ?>
            </div>
          </div>
          <small class="form-text text-muted">
            Use file with format .JPG, .JPEG, .PNG or .GIF
          </small>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-4 col-12">
        <label>Ijazah SD/MI yang telah dilegalisir</label>
      </div>
      <div class="col-md-8 col-12">
        <div class="form-group">
          <div class="upload-inline-xs">
            <div class="upload-button">
              <input type="file" name="ijazah_sd" class="upload-pure-button psb-ijazah_sd" accept="image/jpg,image/jpeg,image/png,image/gif" data-preview="ijazah_sd" />
            </div>
            <div class="upload-preview data-preview-ijazah_sd">
              <?php if (isset($psb_dokumen->ijazah_sd) && !is_null($psb_dokumen->ijazah_sd) && !empty($psb_dokumen->ijazah_sd)) : ?>
                <p class="mb-1">
                  <a href="<?= base_url($psb_dokumen->ijazah_sd) ?>" class="btn btn-light btn-sm" data-fancybox>
                    <i class="zmdi zmdi-download"></i>
                    View Current
                  </a>
                </p>
                <p class="mb-0 text-muted">
                  To change file, please click the button beside
                </p>
              <?php endif ?>
            </div>
          </div>
          <small class="form-text text-muted">
            Use file with format .JPG, .JPEG, .PNG or .GIF
          </small>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-4 col-12">
        <label>SKHUN SD/MI yang telah dilegalisir</label>
      </div>
      <div class="col-md-8 col-12">
        <div class="form-group">
          <div class="upload-inline-xs">
            <div class="upload-button">
              <input type="file" name="skhun" class="upload-pure-button psb-skhun" accept="image/jpg,image/jpeg,image/png,image/gif" data-preview="skhun" />
            </div>
            <div class="upload-preview data-preview-skhun">
              <?php if (isset($psb_dokumen->skhun) && !is_null($psb_dokumen->skhun) && !empty($psb_dokumen->skhun)) : ?>
                <p class="mb-1">
                  <a href="<?= base_url($psb_dokumen->skhun) ?>" class="btn btn-light btn-sm" data-fancybox>
                    <i class="zmdi zmdi-download"></i>
                    View Current
                  </a>
                </p>
                <p class="mb-0 text-muted">
                  To change file, please click the button beside
                </p>
              <?php endif ?>
            </div>
          </div>
          <small class="form-text text-muted">
            Use file with format .JPG, .JPEG, .PNG or .GIF
          </small>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-4 col-12">
        <label>
          Buku Laporan Pendidikan SD/MI yang telah dilegalisir. Berkas yang diunggah adalah hasil scan yang sudah disatukan ke dalam file PDF atau DOCX
        </label>
      </div>
      <div class="col-md-8 col-12">
        <div class="form-group">
          <div class="upload-inline-xs">
            <div class="upload-button">
              <input type="file" name="buku_laporan" class="upload-pure-button psb-buku_laporan" accept="application/pdf,application/vnd.openxmlformats-officedocument.wordprocessingml.document" data-preview="buku_laporan" />
            </div>
            <div class="upload-preview data-preview-buku_laporan">
              <?php if (isset($psb_dokumen->buku_laporan) && !is_null($psb_dokumen->buku_laporan) && !empty($psb_dokumen->buku_laporan)) : ?>
                <p class="mb-1">
                  <a href="<?= base_url($psb_dokumen->buku_laporan) ?>" class="btn btn-light btn-sm" target="_blank">
                    <i class="zmdi zmdi-download"></i>
                    View Current
                  </a>
                </p>
                <p class="mb-0 text-muted">
                  To change file, please click the button beside
                </p>
              <?php endif ?>
            </div>
          </div>
          <small class="form-text text-muted">
            Use file with format .PDF or .DOCX
          </small>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-4 col-12">
        <label>KIP (Kartu Indonesia Pintar) asli, bagi yang memiliki</label>
      </div>
      <div class="col-md-8 col-12">
        <div class="form-group">
          <div class="upload-inline-xs">
            <div class="upload-button">
              <input type="file" name="kip" class="upload-pure-button psb-kip" accept="image/jpg,image/jpeg,image/png,image/gif" data-preview="kip" />
            </div>
            <div class="upload-preview data-preview-kip">
              <?php if (isset($psb_dokumen->kip) && !is_null($psb_dokumen->kip) && !empty($psb_dokumen->kip)) : ?>
                <p class="mb-1">
                  <a href="<?= base_url($psb_dokumen->kip) ?>" class="btn btn-light btn-sm" data-fancybox>
                    <i class="zmdi zmdi-download"></i>
                    View Current
                  </a>
                </p>
                <p class="mb-0 text-muted">
                  To change file, please click the button beside
                </p>
              <?php endif ?>
            </div>
          </div>
          <small class="form-text text-muted">
            Use file with format .JPG, .JPEG, .PNG or .GIF
          </small>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-4 col-12">
        &nbsp;
      </div>
      <div class="col-md-8 col-12">
        <div class="form-check">
          <input class="form-check-input" type="checkbox" value="1" name="accept_aturan" id="accept_aturan" <?= (isset($psb_dokumen->accept_aturan) && !is_null($psb_dokumen->accept_aturan) && !empty($psb_dokumen->accept_aturan)) ? 'checked' : '' ?>>
          <label class="form-check-label" for="accept_aturan">
            Saya bersedia untuk mentaati
            <a href="javascript:;" data-src="<?= base_url('partial/post/10') ?>" data-fancybox data-type="ajax">
              peraturan dan tata tertib pondok (klik disini)
            </a>
          </label>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-4 col-12">
        &nbsp;
      </div>
      <div class="col-md-8 col-12">
        <div class="form-check">
          <input class="form-check-input" type="checkbox" value="1" name="accept_ortu" id="accept_ortu" <?= (isset($psb_dokumen->accept_ortu) && !is_null($psb_dokumen->accept_ortu) && !empty($psb_dokumen->accept_ortu)) ? 'checked' : '' ?>>
          <label class="form-check-label" for="accept_ortu">
            Orang tua bersedia memenuhi perjanjian/ketentuan sebagai mana tertuang pada dokumen/halaman
            <a href="javascript:;" data-src="<?= base_url('partial/post/11') ?>" data-fancybox data-type="ajax">
              berikut (klik disini)
            </a>
          </label>
        </div>
      </div>
    </div>
  </div>
</fieldset>