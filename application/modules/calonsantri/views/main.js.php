<script type="text/javascript">
  $(document).ready(function() {

    var _section = "calonsantri";
    var _table = "table-calonsantri";
    var _form = "form-calonsantri";
    var _modalGenerateIdCard = "modal-generate-idcard";
    var _prestasiCount = 0;
    var _filterYear = null;
    var _filterDraw = false;

    // Inject state
    _isLockDaftarUlang_redirect = false;

    // Handle ajax stop
    $(document).ajaxStop(function() {
      $(document).find(".body-loading").fadeOut("fast", function() {
        $(this).hide();
        document.body.style.overflow = "auto";
      });
    });

    // Initialize DataTables: Index
    if ($("#" + _table)[0]) {
      var table_calonsantri = $("#" + _table).DataTable({
        processing: true,
        serverSide: true,
        ajax: {
          url: "<?php echo base_url('calonsantri/ajax_get_all/') ?>" + _filterYear,
          type: "get"
        },
        columns: [{
            data: null,
            render: function(data, type, row, meta) {
              return meta.row + meta.settings._iDisplayStart + 1;
            }
          },
          {
            data: "profile_photo",
            render: function(data, type, row, meta) {
              var output = "";
              var imageSource = (!data) ? "themes/_public/img/avatar/male-1.png" : data;
              var imageSource_thumb = imageSource;
              var imageSource_ext = (/[.]/.exec(imageSource)) ? /[^.]+$/.exec(imageSource) : undefined;

              if (imageSource_ext !== undefined) {
                var _ext = imageSource_ext[0];
                imageSource_thumb = imageSource.replace("." + _ext, "_thumb." + _ext);
              };

              output += '<a href="<?= base_url() ?>' + imageSource + '" data-fancybox>';
              output += '<img src="<?= base_url() ?>' + imageSource_thumb + '" class="img-thumbnail">';
              output += '</a>';

              return output;
            }
          },
          {
            data: "nisn"
          },
          {
            data: "nama_lengkap"
          },
          {
            data: "jenis_kelamin"
          },
          {
            data: "telepon",
            render: function(data, type, row, meta) {
              var link = "";
              var isMobile = "<?= $app->is_mobile ?>";

              if (data) {
                if (isMobile) {
                  link = '<a href="http://api.whatsapp.com/send?phone=+62' + data + '&text=Write your message here!" target="_blank">' + data + '</a>'; // mobile
                } else {
                  link = '<a href="http://web.whatsapp.com/send?phone=+62' + data + '&text=Write your message here!" target="_blank">' + data + '</a>'; // desktop
                };
              };

              return link;
            }
          },
          {
            data: "kota"
          },
          {
            data: "status",
            render: function(data, type, row, meta) {
              let label, activeClass;

              switch (parseInt(data)) {
                case 0:
                  label = "Belum Bayar, Nonaktif";
                  activeClass = "danger";
                  break;
                case 1:
                  label = "Sudah Bayar, Aktif";
                  activeClass = "success";
                  break;
                case 2:
                  label = "Tidak Lulus";
                  activeClass = "danger";
                  break;
                case 3:
                  label = "Lulus";
                  activeClass = "success";
                  break;
                case 4:
                  label = "Santri Aktif";
                  activeClass = "success";
                  break;
                default:
                  label = "Undefined";
                  activeClass = "danger";
                  break;
              };

              return '<span class="badge badge-' + activeClass + '">' + label + '</span>';
            }
          },
          {
            data: "created_at"
          },
          {
            data: null,
            className: "center",
            render: function(data, type, row, meta) {
              var output = "";
              var editUrl = "<?= base_url('calonsantri/update/') ?>" + row.id;
              var currentStatus = parseInt(row.status);
              var isActive_0 = '';
              var isActive_1 = '';
              var isActive_2 = '';
              var isActive_3 = '';
              var isActive_4 = '';
              var style_0 = '';
              var style_1 = '';
              var style_2 = '';
              var style_3 = '';
              var style_4 = '';

              if (currentStatus === 0) {
                isActive_0 = 'disabled';
                style_0 = 'style="text-decoration: line-through;"';
              } else if (currentStatus === 1) {
                isActive_1 = 'disabled';
                style_1 = 'style="text-decoration: line-through;"';
              } else if (currentStatus === 2) {
                isActive_2 = 'disabled';
                style_2 = 'style="text-decoration: line-through;"';
              } else if (currentStatus === 3) {
                isActive_3 = 'disabled';
                style_3 = 'style="text-decoration: line-through;"';
              } else if (currentStatus === 4) {
                isActive_4 = 'disabled';
                style_4 = 'style="text-decoration: line-through;"';
              };

              output += '<div class="action">';

              if (currentStatus !== 4) {
                output += '<div class="dropleft">';
                output += '<a class="btn btn-sm btn-success text-white btn-table-action" data-toggle="dropdown" style="color: #525a62;"><i class="zmdi zmdi-menu"></i> Action <div class="spinner-action"></div> </a>';
                output += '<div class="dropdown-menu dropdown-menu-right">';

                if (currentStatus === 0) {
                  output += '<a href="javascript:;" class="dropdown-item action-send-notif">Kirim Notifikasi Belum Bayar</a>';
                  output += '<div class="dropdown-divider"></div>';
                } else if (currentStatus === 1) {
                  output += '<a href="javascript:;" class="dropdown-item action-generate-idcard">Generate ID Card</a>';
                  output += '<div class="dropdown-divider"></div>';
                };

                output += '<a href="javascript:;" class="dropdown-item action-set-status" data-status="0" ' + isActive_0 + ' ' + style_0 + '>Tandai Belum Bayar & Nonaktifkan</a>';
                output += '<a href="javascript:;" class="dropdown-item action-set-status" data-status="1" ' + isActive_1 + ' ' + style_1 + '>Tandai Sudah Bayar & Aktifkan</a>';
                output += '<a href="javascript:;" class="dropdown-item action-set-status" data-status="2" ' + isActive_2 + ' ' + style_2 + '>Tandai Tidak Lulus</a>';
                output += '<a href="javascript:;" class="dropdown-item action-set-status" data-status="3" ' + isActive_3 + ' ' + style_3 + '>Tandai Lulus</a>';
                // output += '<div class="dropdown-divider"></div>';
                // output += '<a href="javascript:;" class="dropdown-item action-set-status" data-status="4" ' + isActive_4 + ' ' + style_4 + '>Jadikan Santri</a>';
                output += '</div>';
                output += '</div>';

                output += '&nbsp;<a href="' + editUrl + '" class="btn btn-sm btn-primary btn-table-action action-edit" title="Edit"><i class="zmdi zmdi-edit"></i></a>';
                output += '&nbsp;<a href="javascript:;" class="btn btn-sm btn-danger btn-table-action action-delete" title="Delete"><i class="zmdi zmdi-delete"></i></a>';

              };

              output += '&nbsp;<a href="<?= base_url('calonsantri/d-xlsx/') ?>' + row.id + '" target="_blank" class="btn btn-sm btn-secondary btn-table-action action-export" title="Export to Excel"><i class="zmdi zmdi-download"></i></a>';
              output += '</div>';

              return output;
            }
          }
        ],
        autoWidth: !1,
        responsive: {
          details: {
            renderer: function(api, rowIdx, columns) {
              var hideColumn = [];
              var data = $.map(columns, function(col, i) {
                return ($.inArray(col.columnIndex, hideColumn)) ?
                  '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                  '<td class="dt-details-td">' + col.title + ':' + '</td> ' +
                  '<td class="dt-details-td">' + col.data + '</td>' +
                  '</tr>' :
                  '';
              }).join('');

              return data ? $('<table/>').append(data) : false;
            },
            type: "inline",
            target: 'tr',
          }
        },
        columnDefs: [{
          className: 'desktop',
          targets: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
        }, {
          className: 'tablet',
          targets: [0, 1, 2, 3, 4, 5]
        }, {
          className: 'mobile',
          targets: [0, 3]
        }, {
          responsivePriority: 2,
          targets: -1
        }],
        pageLength: 15,
        language: {
          searchPlaceholder: "Search...",
          sProcessing: '<div style="text-align: center;"><div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div></div>'
        },
        sDom: '<"dataTables_ct"><"dataTables__top"fb>rt<"dataTables__bottom"ip><"clear">',
        buttons: [{
          extend: "excelHtml5",
          title: "Export Result"
        }, {
          extend: "print",
          title: "Export Result"
        }],
        initComplete: function(a, b) {
          $(this).closest(".dataTables_wrapper").find(".dataTables__top").prepend(
            '<div class="dataTables_buttons hidden-sm-down actions">' +
            '<span class="actions__item zmdi zmdi-refresh" data-table-action="reload" title="Reload" />' +
            '</div>'
          );
        },
        drawCallback: function(settings) {
          // Filter Column
          if (_filterDraw === true) {
            this.api().columns([4, 6]).every(function() {
              var column = this;
              var select = $('<select class="form-control dt-filter select2"><option value="">Filter ↓</option></select>')
                .appendTo($(column.footer()).empty())
                .on('change', function() {
                  var val = $.fn.dataTable.util.escapeRegex(
                    $(this).val()
                  );

                  column
                    .search(val ? '^' + val + '$' : '', true, false)
                    .draw();
                });

              column.data().unique().sort().each(function(d, j) {
                if (d !== "") {
                  select.append('<option value="' + d + '">' + d + '</option>')
                };
              });
            });
            _filterDraw = false;
          };
        }
      });

      $(".dataTables_filter input[type=search]").focus(function() {
        $(this).closest(".dataTables_filter").addClass("dataTables_filter--toggled")
      });

      $(".dataTables_filter input[type=search]").blur(function() {
        $(this).closest(".dataTables_filter").removeClass("dataTables_filter--toggled")
      });

      $("body").on("click", "[data-table-action]", function(a) {
        a.preventDefault();
        var b = $(this).data("table-action");
        if ("reload" === b) {
          $("#" + _table).DataTable().ajax.reload(null, false);
        };
      });
    };

    // Handle generate ID Card
    $("#" + _table).on("click", "a.action-generate-idcard", function(e) {
      e.preventDefault();
      var isDisabled = (typeof $(this).attr("disabled") !== 'undefined') ? true : false;

      if (isDisabled === false) {
        var temp = table_calonsantri.row($(this).closest('tr')).data();

        $("#" + _modalGenerateIdCard).modal("show");

        $.ajax({
          type: "get",
          url: "<?php echo base_url('calonsantri/ajax_generate_idcard/') ?>" + temp.id,
          dataType: "json",
          success: function(response) {
            if (response.status) {
              $("#" + _modalGenerateIdCard + " .idcard-wrapper").html(response.dom);
            } else {
              notify(response.data, "danger");
              $("#" + _modalGenerateIdCard).modal("hide");
            };
          }
        });
      };
    });

    // Handle set status
    $("#" + _table).on("click", "a.action-set-status", function(e) {
      e.preventDefault();
      var isDisabled = (typeof $(this).attr("disabled") !== 'undefined') ? true : false;

      if (isDisabled === false) {
        var temp = table_calonsantri.row($(this).closest('tr')).data();
        var status = parseInt($(this).attr("data-status"));

        // Show loading indicator
        $(document).find(".body-loading").fadeIn("fast", function() {
          $(this).show();
          document.body.style.overflow = "hidden";
        });

        $.ajax({
          type: "put",
          url: "<?php echo base_url('calonsantri/ajax_set_status/') ?>" + temp.id + "/" + status,
          dataType: "json",
          success: function(response) {
            if (response.status) {
              $("#" + _table).DataTable().ajax.reload(null, false);
              notify(response.data, "success");
            } else {
              notify(response.data, "danger");
            };
          }
        });
      };
    });

    // Handle send notif
    $("#" + _table).on("click", "a.action-send-notif", function(e) {
      e.preventDefault();
      var isDisabled = (typeof $(this).attr("disabled") !== 'undefined') ? true : false;

      if (isDisabled === false) {
        var temp = table_calonsantri.row($(this).closest('tr')).data();

        // Show loading indicator
        $(document).find(".body-loading").fadeIn("fast", function() {
          $(this).show();
          document.body.style.overflow = "hidden";
        });

        $.ajax({
          type: "put",
          url: "<?php echo base_url('calonsantri/ajax_send_notif/') ?>" + temp.id,
          dataType: "json",
          success: function(response) {
            if (response.status) {
              $("#" + _table).DataTable().ajax.reload(null, false);
              notify(response.data, "success");
            } else {
              notify(response.data, "danger");
            };
          }
        });
      };
    });

    // Handle data delete
    $("#" + _table).on("click", "a.action-delete", function(e) {
      e.preventDefault();
      var temp = table_calonsantri.row($(this).closest('tr')).data();

      swal({
        title: "Are you sure to delete?",
        text: "Once deleted, you will not be able to recover this data!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false
      }).then((result) => {
        if (result.value) {
          $.ajax({
            type: "delete",
            url: "<?php echo base_url('calonsantri/ajax_delete/') ?>" + temp.id,
            dataType: "json",
            success: function(response) {
              if (response.status) {
                $("#" + _table).DataTable().ajax.reload(null, false);
                notify(response.data, "success");
              } else {
                notify(response.data, "danger");
              };
            }
          });
        };
      });
    });

    // Handle Tempat Tinggal Santri
    $(".psb-tempat_tinggal").on("change", function() {
      var value = $(this).val();
      var control = $(".control-psb-tempat_tinggal_lainnya");

      if (value === "Lainnya") {
        control.show();
      } else {
        control.hide();
      };
    });

    // Handle Ayah: Pekerjaan Utama
    $(".psb-ayah_pekerjaan").on("change", function() {
      var value = $(this).val();
      var control = $(".control-psb-ayah_pekerjaan");

      if (value === "TNI/POLRI" || value === "PNS") {
        control.css("display", "flex");
      } else {
        control.hide();
      };
    });

    // Handle Ibu: Pekerjaan Utama
    $(".psb-ibu_pekerjaan").on("change", function() {
      var value = $(this).val();
      var control = $(".control-psb-ibu_pekerjaan");

      if (value === "TNI/POLRI" || value === "PNS") {
        control.css("display", "flex");
      } else {
        control.hide();
      };
    });

    // Handle Wali: Pekerjaan Utama
    $(".psb-wali_pekerjaan").on("change", function() {
      var value = $(this).val();
      var control = $(".control-psb-wali_pekerjaan");

      if (value === "TNI/POLRI" || value === "PNS") {
        control.css("display", "flex");
      } else {
        control.hide();
      };
    });

    // Handle prestasi: Add
    $(".prestasi-action-add").on("click", function(e) {
      e.preventDefault();

      // Increment form count
      _prestasiCount = parseInt(_prestasiCount) + 1;

      var wrapper = $(".form-prestasi");
      var formTemplate = '' +
        '<!-- Form Items -->' +
        '<div class="panel-bordered mt-3 form-prestasi-item form-prestasi-item-new-' + _prestasiCount + '">' +
        '<div class="panel-bordered-header">' +
        'Prestasi' +
        '<a href="javascript:;" class="pull-right prestasi-action-delete" style="color: var(--red);" title="Hapus" data-prestasi-key="new-' + _prestasiCount + '">' +
        '<i class="zmdi zmdi-close"></i>' +
        '</a>' +
        '</div>' +
        '<div class="panel-bordered-body">' +
        '<div class="row">' +
        '<div class="col-md-6 col-12">' +
        '<div class="form-group">' +
        '<label required>Nama Lomba</label>' +
        '<input type="text" name="prestasi[nama_lomba][]" class="form-control prestasi-nama_lomba" placeholder="-" required />' +
        '<i class="form-group__bar"></i>' +
        '</div>' +
        '</div>' +
        '<div class="col-md-6 col-12">' +
        '<div class="form-group">' +
        '<label>Jenis Lomba</label>' +
        '<input type="text" name="prestasi[jenis_lomba][]" class="form-control prestasi-jenis_lomba" placeholder="-" />' +
        '<i class="form-group__bar"></i>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '<div class="row">' +
        '<div class="col-md-6 col-12">' +
        '<div class="form-group">' +
        '<label>Penyelenggara</label>' +
        '<input type="text" name="prestasi[penyelenggara][]" class="form-control prestasi-penyelenggara" placeholder="-" />' +
        '<i class="form-group__bar"></i>' +
        '</div>' +
        '</div>' +
        '<div class="col-md-6 col-12">' +
        '<div class="form-group">' +
        '<label>Tingkat</label>' +
        '<input type="text" name="prestasi[tingkat][]" class="form-control prestasi-tingkat" placeholder="-" />' +
        '<i class="form-group__bar"></i>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '<div class="row">' +
        '<div class="col-md-6 col-12">' +
        '<div class="form-group">' +
        '<label required>Peringkat</label>' +
        '<input type="text" name="prestasi[peringkat][]" class="form-control prestasi-peringkat" placeholder="-" required />' +
        '<i class="form-group__bar"></i>' +
        '</div>' +
        '</div>' +
        '<div class="col-md-6 col-12">' +
        '<div class="form-group">' +
        '<label required>Tahun</label>' +
        '<input type="number" name="prestasi[tahun][]" class="form-control mask-number prestasi-tahun" maxlength="4" placeholder="-" required />' +
        '<i class="form-group__bar"></i>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '<div class="form-group mb-0">' +
        '<label required>Foto Sertifikat / Piagam</label>' +
        '<div class="position-relative">' +
        '<input type="file" name="prestasi[sertifikat][]" class="form-control prestasi-sertifikat" accept="image/jpg,image/jpeg,image/gif,image/png,application/pdf" required />' +
        '<i class="form-group__bar"></i>' +
        '</div>' +
        '<small class="form-text text-muted">' +
        'Use file with format .JPG, .JPEG, .PNG, .GIF or .PDF' +
        '</small>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '<!-- END ## Form Items -->' +
        '';

      wrapper.append(formTemplate).children(':last').hide().fadeIn("fast");
      handlePrestasiCount();
    });

    // Handle prestasi: Delete
    $(document).on("click", "a.prestasi-action-delete", function(e) {
      e.preventDefault();

      var key = $(this).attr("data-prestasi-key");
      var form = $(".form-prestasi-item-" + key);

      form.fadeOut("fast", function() {
        $(this).remove();
        handlePrestasiCount();
      });
    });

    // Handle prestasi: Delete Existing
    $(".prestasi-action-delete-existing").on("click", function(e) {
      e.preventDefault();

      var key = $(this).attr("data-prestasi-key");
      var form = $(".form-prestasi-item-" + key);

      swal({
        title: "Are you sure to delete?",
        text: "Once deleted, you will not be able to recover this data!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false
      }).then((result) => {
        if (result.value) {
          $.ajax({
            type: "delete",
            url: "<?php echo base_url('calonsantri/ajax_delete_prestasi/') ?>" + key,
            dataType: "json",
            success: function(response) {
              if (response.status) {
                form.fadeOut("fast", function() {
                  $(this).remove();
                  handlePrestasiCount();
                });
                notify(response.data, "success");
              } else {
                notify(response.data, "danger");
              };
            }
          });
        };
      });
    });

    // Handle data submit
    $("#" + _form + " .btn-submit").on("click", function(e) {
      e.preventDefault();

      swal({
        title: "Confirm",
        text: "Are you sure to save the data?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#39bbb0',
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false
      }).then((result) => {
        if (result.value) {
          var form = $("#" + _form)[0];
          var data = new FormData(form);

          $.ajax({
            type: "post",
            url: "<?php echo base_url('calonsantri/ajax_save') ?>",
            data: data,
            dataType: "json",
            enctype: "multipart/form-data",
            processData: false,
            contentType: false,
            cache: false,
            success: function(response) {
              if (response.status === true) {
                // notify(response.data, "success");
                swal({
                  title: "Success",
                  text: response.data,
                  type: "success",
                  showCancelButton: false,
                  confirmButtonColor: '#39bbb0',
                  confirmButtonText: "OK",
                  closeOnConfirm: false
                }).then((result) => {
                  window.location.href = "";
                });
              } else {
                notify(response.data, "danger");
              };
            }
          });
        };
      });
    });

    // Handle filter submit
    $("#" + _section + " .page-action-filter").on("click", function() {
      _filterDraw = true;
      _filterYear = $(".filter-year").val();
      table_calonsantri.ajax.url("<?= base_url('calonsantri/ajax_get_all/') ?>" + _filterYear).load();
    });

    // Handle prestasi: Data Count
    handlePrestasiCount = () => {
      var countItem = $(".form-prestasi-item").length;

      if (countItem > 0) {
        $(".form-prestasi-item-count").show().html("Jumlah Prestasi: " + countItem);
      } else {
        $(".form-prestasi-item-count").hide().html("Jumlah Prestasi: " + countItem);
      };
    };

    // Handle Komitmen: Bersedia
    $(".psb-komitmen").on("change", function() {
      var value = $(this).val();
      var nominal = $(".psb-komitmen_nominal");
      var control = $(".control-psb-komitmen");

      if (value === "Bersedia") {
        control.css("display", "block");
      } else {
        nominal.val("");
        control.hide();
      };
    });

    // Init prestasi count
    handlePrestasiCount();

    // Handle upload
    $(document.body).on("change", ".psb-surat_1", function() {
      readUploadMultipleDocURLXs(this);
    });
    $(document.body).on("change", ".psb-surat_2", function() {
      readUploadMultipleDocURLXs(this);
    });
    $(document.body).on("change", ".psb-ijazah_sd", function() {
      readUploadMultipleDocURLXs(this);
    });
    $(document.body).on("change", ".psb-skhun", function() {
      readUploadMultipleDocURLXs(this);
    });
    $(document.body).on("change", ".psb-buku_laporan", function() {
      readUploadMultipleDocURLXs(this);
    });
    $(document.body).on("change", ".psb-nisn", function() {
      readUploadMultipleDocURLXs(this);
    });
    $(document.body).on("change", ".psb-akte_kelahiran", function() {
      readUploadMultipleDocURLXs(this);
    });
    $(document.body).on("change", ".psb-kartu_keluarga", function() {
      readUploadMultipleDocURLXs(this);
    });
    $(document.body).on("change", ".psb-kip", function() {
      readUploadMultipleDocURLXs(this);
    });

  });
</script>