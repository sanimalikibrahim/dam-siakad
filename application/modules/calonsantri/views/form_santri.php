<!-- Data Santri -->
<fieldset>
  <div class="panel-santri panel-active">
    <div class="form-group">
      <label required>Nama Lengkap Santri</label>
      <div class="position-relative">
        <input type="text" name="nama_lengkap" class="form-control psb-nama_lengkap" placeholder="-" value="<?= $psb->nama_lengkap ?>" required>
        <i class="form-group__bar"></i>
      </div>
      <small class="form-text text-muted">
        Sesuai dengan ijazah / Akta Kelahiran
      </small>
    </div>
    <div class="row">
      <div class="col-md-8 col-12">
        <div class="form-group">
          <label required>Tempat Lahir</label>
          <input type="text" name="tempat_lahir" class="form-control psb-tempat_lahir" placeholder="-" value="<?= $psb->tempat_lahir ?>" required>
          <i class="form-group__bar"></i>
        </div>
      </div>
      <div class="col-md-4 col-12">
        <div class="form-group">
          <label required>Tanggal Lahir</label>
          <input type="text" name="tanggal_lahir" class="form-control flatpickr-date mask-date psb-tanggal_lahir" placeholder="YYYY-MM-DD" value="<?= $psb->tanggal_lahir ?>" required>
          <i class="form-group__bar"></i>
        </div>
      </div>
    </div>
    <div class="form-group">
      <label required>Jenis Kelamin</label>
      <div class="pt-1">
        <div class="form-check form-check-inline">
          <input class="form-check-input" type="radio" name="jenis_kelamin" id="jenis_kelamin-laki_laki" value="Laki-laki" <?= ($psb->jenis_kelamin === 'Laki-laki') ? 'checked' : '' ?>>
          <label class="form-check-label" for="jenis_kelamin-laki_laki">Laki-laki</label>
        </div>
        <div class="form-check form-check-inline">
          <input class="form-check-input" type="radio" name="jenis_kelamin" id="jenis_kelamin-perempuan" value="Perempuan" <?= ($psb->jenis_kelamin === 'Perempuan') ? 'checked' : '' ?>>
          <label class="form-check-label" for="jenis_kelamin-perempuan">Perempuan</label>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6 col-12">
        <div class="form-group">
          <label required>Urutan Dalam Keluarga Anak Ke</label>
          <input type="number" name="urutan_anak" class="form-control mask-number psb-urutan_anak" maxlength="2" placeholder="-" value="<?= $psb->urutan_anak ?>" required>
          <i class="form-group__bar"></i>
        </div>
      </div>
      <div class="col-md-6 col-12">
        <div class="form-group">
          <label required>Jumlah Saudara</label>
          <input type="number" name="jumlah_saudara" class="form-control mask-number psb-jumlah_saudara" maxlength="2" placeholder="-" value="<?= $psb->jumlah_saudara ?>" required>
          <i class="form-group__bar"></i>
        </div>
      </div>
    </div>
    <div class="form-group">
      <label>Golongan Darah</label>
      <div class="pt-1">
        <div class="form-check form-check-inline">
          <input class="form-check-input" type="radio" name="golongan_darah" id="golongan_darah-a" value="A" <?= ($psb->golongan_darah === 'A') ? 'checked' : '' ?>>
          <label class="form-check-label" for="golongan_darah-a">A</label>
        </div>
        <div class="form-check form-check-inline">
          <input class="form-check-input" type="radio" name="golongan_darah" id="golongan_darah-b" value="B" <?= ($psb->golongan_darah === 'B') ? 'checked' : '' ?>>
          <label class="form-check-label" for="golongan_darah-b">B</label>
        </div>
        <div class="form-check form-check-inline">
          <input class="form-check-input" type="radio" name="golongan_darah" id="golongan_darah-o" value="O" <?= ($psb->golongan_darah === 'O') ? 'checked' : '' ?>>
          <label class="form-check-label" for="golongan_darah-o">O</label>
        </div>
        <div class="form-check form-check-inline">
          <input class="form-check-input" type="radio" name="golongan_darah" id="golongan_darah-ab" value="AB" <?= ($psb->golongan_darah === 'AB') ? 'checked' : '' ?>>
          <label class="form-check-label" for="golongan_darah-ab">AB</label>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6 col-12">
        <div class="form-group">
          <label>Nomor Induk Keluarga (NIK)</label>
          <input type="number" name="nik" class="form-control mask-number psb-nik" maxlength="16" placeholder="-" value="<?= $psb->nik ?>">
          <i class="form-group__bar"></i>
        </div>
      </div>
      <div class="col-md-6 col-12">
        <div class="form-group">
          <label>Nomor Pokok Sekolah Nasional (NPSN)</label>
          <input type="number" name="npsn" class="form-control mask-number psb-npsn" maxlength="8" placeholder="-" value="<?= $psb->npsn ?>">
          <i class="form-group__bar"></i>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6 col-12">
        <div class="form-group">
          <label>Nomor Induk Madrasah (NIM)</label>
          <input type="number" name="nim" class="form-control mask-number psb-nim" maxlength="18" placeholder="-" value="<?= $psb->nim ?>">
          <i class="form-group__bar"></i>
        </div>
      </div>
      <div class="col-md-6 col-12">
        <div class="form-group">
          <label required>Nomor Induk Siswa Nasional (NISN)</label>
          <input type="number" name="nisn" class="form-control mask-number psb-nisn" maxlength="10" placeholder="-" value="<?= $psb->nisn ?>" required>
          <i class="form-group__bar"></i>
        </div>
      </div>
    </div>
    <div class="form-group">
      <label required>Alamat Tempat Tinggal Santri</label>
      <input type="text" name="alamat_santri" class="form-control psb-alamat_santri" placeholder="-" value="<?= $psb->alamat_santri ?>" required>
      <i class="form-group__bar"></i>
    </div>
    <div class="row">
      <div class="col-md-6 col-12">
        <div class="form-group">
          <label>Kampung/Desa/Kelurahan</label>
          <input type="text" name="kelurahan" class="form-control psb-kelurahan" placeholder="-" value="<?= $psb->kelurahan ?>">
          <i class="form-group__bar"></i>
        </div>
      </div>
      <div class="col-md-6 col-12">
        <div class="form-group">
          <label>Kecamatan</label>
          <input type="text" name="kecamatan" class="form-control psb-kecamatan" placeholder="-" value="<?= $psb->kecamatan ?>">
          <i class="form-group__bar"></i>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6 col-12">
        <div class="form-group">
          <label required>Kota/Kabupaten</label>
          <div class="select">
            <select name="kota" class="form-control select2 psb-kota" data-placeholder="Select &#8595;" required>
              <?= $list_regencies ?>
            </select>
            <i class="form-group__bar"></i>
          </div>
        </div>
      </div>
      <div class="col-md-6 col-12">
        <div class="form-group">
          <label required>Provinsi</label>
          <div class="select">
            <select name="provinsi" class="form-control select2 psb-provinsi" data-placeholder="Select &#8595;" required>
              <?= $list_provinces ?>
            </select>
            <i class="form-group__bar"></i>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-4 col-12">
        <div class="form-group">
          <label>Kode Pos</label>
          <input type="number" name="kode_pos" class="form-control mask-number psb-kode_pos" maxlength="5" placeholder="-" value="<?= $psb->kode_pos ?>">
          <i class="form-group__bar"></i>
        </div>
      </div>
      <div class="col-md-4 col-12">
        <div class="form-group">
          <label required>Telepon</label>
          <input type="number" name="telepon" class="form-control mask-number psb-telepon" maxlength="15" placeholder="-" value="<?= $psb->telepon ?>" required>
          <i class="form-group__bar"></i>
        </div>
      </div>
      <div class="col-md-4 col-12">
        <div class="form-group">
          <label>Handphone</label>
          <input type="number" name="handphone" class="form-control mask-number psb-handphone" maxlength="15" placeholder="-" value="<?= $psb->handphone ?>">
          <i class="form-group__bar"></i>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6 col-12">
        <div class="form-group">
          <label required>Tempat Tinggal Santri</label>
          <div class="select">
            <select name="tempat_tinggal" class="form-control psb-tempat_tinggal" data-placeholder="Select &#8595;">
              <option disabled selected>Select &#8595;</option>
              <option value="Rumah Orang Tua Kandung" <?= ($psb->tempat_tinggal === 'Rumah Orang Tua Kandung') ? 'selected' : '' ?>>Rumah Orang Tua Kandung</option>
              <option value="Rumah Kakek Nenek" <?= ($psb->tempat_tinggal === 'Rumah Kakek Nenek') ? 'selected' : '' ?>>Rumah Kakek Nenek</option>
              <option value="Rumah Kontrak" <?= ($psb->tempat_tinggal === 'Rumah Kontrak') ? 'selected' : '' ?>>Rumah Kontrak (Kontrak/Sewa)</option>
              <option value="Ikut Saudara" <?= ($psb->tempat_tinggal === 'Ikut Saudara') ? 'selected' : '' ?>>Ikut Saudara/Kerabat</option>
              <option value="Asrama" <?= ($psb->tempat_tinggal === 'Asrama') ? 'selected' : '' ?>>Asrama</option>
              <option value="Wali" <?= ($psb->tempat_tinggal === 'Wali') ? 'selected' : '' ?>>Wali (Orang Tua Asuh)</option>
              <option value="Lainnya" <?= ($psb->tempat_tinggal === 'Lainnya') ? 'selected' : '' ?>>Lainnya</option>
            </select>
            <i class="form-group__bar"></i>
          </div>
        </div>
      </div>
      <div class="col-md-6 col-12 control-psb-tempat_tinggal_lainnya <?= ($psb->tempat_tinggal !== 'Lainnya') ? 'hidden' : '' ?>">
        <div class="form-group">
          <label required>Tempat Tinggal Santri: Lainnya</label>
          <input type="text" name="tempat_tinggal_lainnya" class="form-control psb-tempat_tinggal_lainnya" placeholder="-" value="<?= $psb->tempat_tinggal_lainnya ?>" required>
          <i class="form-group__bar"></i>
        </div>
      </div>
    </div>
  </div>
  <!-- Sekolah Asal -->
  <div class="card">
    <div class="card-header border-info">
      <h5 class="card-title">Asal Sekolah</h5>
    </div>
    <div class="card-body pb-0">
      <div class="row">
        <div class="col-md-6 col-12">
          <div class="form-group">
            <label required>Nama Sekolah</label>
            <input type="text" name="sekolah_asal" class="form-control psb-sekolah_asal" placeholder="-" style="text-transform: uppercase;" value="<?= $psb->sekolah_asal ?>" required>
            <i class="form-group__bar"></i>
          </div>
        </div>
        <div class="col-md-6 col-12">
          <div class="form-group">
            <label required>Kota/Kabupaten Sekolah</label>
            <div class="select">
              <select name="sekolah_asal_alamat" class="form-control select2 psb-sekolah_asal_alamat" data-placeholder="Select &#8595;">
                <?= $list_regencies_sekolah_asal ?>
              </select>
              <i class="form-group__bar"></i>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</fieldset>