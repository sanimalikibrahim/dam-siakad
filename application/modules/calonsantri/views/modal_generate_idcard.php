<div class="modal fade" id="modal-generate-idcard" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title pull-left">Generate ID Card</h5>
      </div>
      <div class="spinner">
        <div class="lds-hourglass"></div>
      </div>
      <div class="modal-body">
        <div class="idcard-wrapper">
          <!-- Content loaded by ajax -->
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-light btn--icon-text" data-dismiss="modal">
          Close
        </button>
      </div>
    </div>
  </div>
</div>