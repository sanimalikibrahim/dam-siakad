<?php
$profile_photo_temp = $this->session->userdata('user')['profile_photo'];
$profile_photo = (!is_null($profile_photo_temp) && !empty($profile_photo_temp)) ? base_url($profile_photo_temp) : base_url('themes/_public/img/avatar/male-1.png');
?>

<style type="text/css">
  .idcard-preview {
    width: fit-content;
    width: -moz-fit-content;
    overflow: hidden;
  }

  .idcard-preview-foto {
    width: <?= $idcard->idcard_psb_foto_width ?>;
    height: <?= $idcard->idcard_psb_foto_height ?>;
    display: flex;
    justify-content: center;
    align-items: center;
    text-align: center;
    background: #fff;
    border: 1px solid #999;
    position: absolute;
    opacity: 1;
    margin-top: <?= $idcard->idcard_psb_foto_top ?>;
    margin-left: <?= $idcard->idcard_psb_foto_left ?>;
    display: <?= $idcard->idcard_psb_foto_show ?>;
  }

  .idcard-preview-foto img {
    width: 100%;
    height: 100%;
    object-fit: cover;
  }

  .idcard-preview-nomor {
    width: 150px;
    text-align: center;
    font-weight: 500;
    position: absolute;
    margin-top: <?= $idcard->idcard_psb_nomor_top ?>;
    margin-left: <?= $idcard->idcard_psb_nomor_left ?>;
    font-size: <?= $idcard->idcard_psb_nomor_font_size ?>;
    color: <?= $idcard->idcard_psb_nomor_color ?>;
  }

  .idcard-preview-nama {
    max-width: 240px;
    font-weight: 500;
    position: absolute;
    margin-top: <?= $idcard->idcard_psb_nama_top ?>;
    margin-left: <?= $idcard->idcard_psb_nama_left ?>;
    font-size: <?= $idcard->idcard_psb_nama_font_size ?>;
    color: <?= $idcard->idcard_psb_nama_color ?>;
  }

  .idcard-preview-alamat {
    max-width: 240px;
    font-weight: 500;
    position: absolute;
    margin-top: <?= $idcard->idcard_psb_alamat_top ?>;
    margin-left: <?= $idcard->idcard_psb_alamat_left ?>;
    font-size: <?= $idcard->idcard_psb_alamat_font_size ?>;
    color: <?= $idcard->idcard_psb_alamat_color ?>;
  }

  .idcard-preview-ruangan1 {
    max-width: 240px;
    font-weight: 500;
    position: absolute;
    margin-top: <?= $idcard->idcard_psb_ruangan1_top ?>;
    margin-left: <?= $idcard->idcard_psb_ruangan1_left ?>;
    font-size: <?= $idcard->idcard_psb_ruangan1_font_size ?>;
    color: <?= $idcard->idcard_psb_ruangan1_color ?>;
  }

  .idcard-preview-ruangan2 {
    max-width: 240px;
    font-weight: 500;
    position: absolute;
    margin-top: <?= $idcard->idcard_psb_ruangan2_top ?>;
    margin-left: <?= $idcard->idcard_psb_ruangan2_left ?>;
    font-size: <?= $idcard->idcard_psb_ruangan2_font_size ?>;
    color: <?= $idcard->idcard_psb_ruangan2_color ?>;
  }

  .idcard-preview-image {
    width: 400px;
    object-fit: contain;
    border: 1px solid #ccc;
  }

  .filter-info {
    text-align: center;
    padding: 10rem 0rem;
  }

  .filter-info p {
    line-height: .75rem;
    font-size: 1.075rem;
    color: #999;
    font-weight: 400;
  }

  .filter-info .zmdi {
    font-size: 3.5rem;
    margin-bottom: 1rem;
    color: #c5c5c5;
  }
</style>

<section id="psb-idcard">
  <?php if (!empty($ruangan->nomor_peserta) && $ruangan->nomor_peserta !== '-') : ?>
    <div class="row">
      <div class="col-xs-10 col-md-10">
        <h4 class="card-title"><?php echo (isset($card_title)) ? $card_title : '' ?></h4>
        <h6 class="card-subtitle"><?php echo (isset($card_subTitle)) ? $card_subTitle : '' ?></h6>
        <div class="clear-card"></div>
      </div>
    </div>
    <div class="clear-card"></div>

    <?php if (empty($psb->kota)) : ?>
      <div class="alert alert-danger">
        <i class="zmdi zmdi-info"></i>
        Untuk dapat mengunduh kartu peserta Anda harus mengisi data Kabupaten/Kota pada profil terlebih dahulu!
      </div>
    <?php endif ?>

    <div id="idcard-content" class="idcard-preview">
      <div class="idcard-preview-foto">
        <img src="<?= $profile_photo ?>" alt="FOTO">
      </div>
      <div class="idcard-preview-nomor">
        <?= (!empty($ruangan->nomor_peserta)) ? $ruangan->nomor_peserta : '-' ?>
      </div>
      <div class="idcard-preview-nama">
        <?= strtoupper($psb->nama_lengkap) ?>
      </div>
      <div class="idcard-preview-alamat">
        <?= (!empty($psb->kota)) ? strtoupper($psb->kota) : '-' ?>
      </div>
      <div class="idcard-preview-ruangan1">
        <?php
        if (isset($app->psb_test_type) && $app->psb_test_type === 'online') {
          echo (!empty($ruangan->tes_online)) ? 'R' . strtoupper($ruangan->tes_online) : '-';
        } else {
          echo (!empty($ruangan->tes_lisan)) ? strtoupper($ruangan->tes_lisan) : '-';
        };
        ?>
      </div>
      <div class="idcard-preview-ruangan2">
        <?php
        if (isset($app->psb_test_type) && $app->psb_test_type === 'online') {
          echo (!empty($ruangan->tes_online)) ? 'R' . strtoupper($ruangan->tes_online) : '-';
        } else {
          echo (!empty($ruangan->tes_tulis)) ? strtoupper($ruangan->tes_tulis) : '-';
        };
        ?>
      </div>
      <img class="idcard-preview-image" src="<?= base_url($idcard->idcard_psb_background) ?>" alt="Kartu Peserta PSB Background" />
    </div>
    <?php if (!empty($psb->kota)) : ?>
      <a id="btn-download-idcard" href="javascript:;" class="btn btn-primary mt-4 disabled">
        <i class="zmdi zmdi-download"></i>
        Unduh Kartu Peserta
      </a>
    <?php else : ?>
      <button type="button" class="btn btn-primary mt-4 disabled">
        <i class="zmdi zmdi-download"></i>
        Unduh Kartu Peserta
      </button>
    <?php endif ?>
  <?php else : ?>
    <div class="filter-info filter-no-data">
      <i class="zmdi zmdi-filter-frames"></i>
      <p>Mapping ruangan belum dibuat</p>
      <p>Silahkan tunggu atau hubungi panitia PSB</p>
    </div>
  <?php endif ?>
</section>

<script type="text/javascript">
  $(document).ready(function() {
    var getCanvas;

    // Initialize canvas
    setTimeout(function() {
      html2canvas(document.getElementById("idcard-content"), {
        onrendered: function(canvas) {
          getCanvas = canvas;
        }
      });

      // Enable download button
      $("#btn-download-idcard").removeClass("disabled");
    }, 1000);

    // Handle download idcard
    $(document).on("click", "#btn-download-idcard", function() {
      var imgageData = getCanvas.toDataURL("image/png");
      var newData = imgageData.replace(/^data:image\/png/, "data:application/octet-stream");

      $("#btn-download-idcard").attr("download", "DA_Kartu_Peserta.png").attr("href", newData);
    });
  });
</script>