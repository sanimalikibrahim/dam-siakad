<?php include_once('modal_ref_seragam.php') ?>

<!-- Seragam -->
<fieldset>
  <div class="panel-seragam panel-active">
    <a href="javascript:;" class="btn btn-success btn--icon-text mb-4" data-toggle="modal" data-target="#modal-ref-seragam">
      <i class="zmdi zmdi-eye"></i>Lihat Referensi Ukuran
    </a>
    <div class="row">
      <div class="col-xs-12 col-md-6">
        <div class="form-group">
          <label required>Ukuran Baju</label>
          <div class="select">
            <select name="ukuran_baju" class="form-control psb-ukuran_baju" data-placeholder="Select &#8595;" required>
              <option disabled selected>Select &#8595;</option>
              <option value="XS" <?= ($psb->ukuran_baju === 'XS') ? 'selected' : '' ?>>XS</option>
              <option value="S" <?= ($psb->ukuran_baju === 'S') ? 'selected' : '' ?>>S</option>
              <option value="M" <?= ($psb->ukuran_baju === 'M') ? 'selected' : '' ?>>M</option>
              <option value="L" <?= ($psb->ukuran_baju === 'L') ? 'selected' : '' ?>>L</option>
              <option value="XL" <?= ($psb->ukuran_baju === 'XL') ? 'selected' : '' ?>>XL</option>
              <option value="XXL" <?= ($psb->ukuran_baju === 'XXL') ? 'selected' : '' ?>>XXL</option>
            </select>
            <i class="form-group__bar"></i>
          </div>
        </div>
      </div>
      <div class="col-xs-12 col-md-6">
        <div class="form-group">
          <label required>Ukuran Celana / Rok</label>
          <div class="select">
            <select name="ukuran_celana" class="form-control psb-ukuran_celana" data-placeholder="Select &#8595;" required>
              <option disabled selected>Select &#8595;</option>
              <option value="XS" <?= ($psb->ukuran_celana === 'XS') ? 'selected' : '' ?>>XS</option>
              <option value="S" <?= ($psb->ukuran_celana === 'S') ? 'selected' : '' ?>>S</option>
              <option value="M" <?= ($psb->ukuran_celana === 'M') ? 'selected' : '' ?>>M</option>
              <option value="L" <?= ($psb->ukuran_celana === 'L') ? 'selected' : '' ?>>L</option>
              <option value="XL" <?= ($psb->ukuran_celana === 'XL') ? 'selected' : '' ?>>XL</option>
              <option value="XXL" <?= ($psb->ukuran_celana === 'XXL') ? 'selected' : '' ?>>XXL</option>
            </select>
            <i class="form-group__bar"></i>
          </div>
        </div>
      </div>
    </div>
  </div>
</fieldset>