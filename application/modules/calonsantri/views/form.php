<?php
$activeTab = isset($_GET['tab']) && !empty($_GET['tab']) ? $_GET['tab'] : 'personal';
$activeTab = in_array($activeTab, ['personal', 'orang_tua', 'prestasi', 'komitmen', 'surat', 'dokumen']) ? $activeTab : 'personal';
$tempPassword = date('YmdHis');
?>

<style type="text/css">
  .hidden {
    display: none;
  }

  .text-bold {
    font-weight: bold;
  }

  .opacity-0 {
    opacity: 0 !important;
  }

  .pull-right {
    float: right;
  }

  .panel-bordered {
    border: 1px solid #eee;
  }

  .panel-bordered .panel-bordered-header {
    background: #f9f9f9;
    padding: 15px;
    font-size: 12pt;
    font-weight: 500;
    border-bottom: 1px solid #eee;
  }

  .panel-bordered .panel-bordered-body {
    background: #fff;
    padding: 15px;
  }
</style>

<section id="calonsantri">
  <div class="card">
    <div class="card-body">
      <h4 class="card-title"><?php echo (isset($card_title)) ? $card_title : '' ?></h4>
      <h6 class="card-subtitle mb-0"><?php echo (isset($card_subTitle)) ? $card_subTitle : '' ?></h6>

      <div class="form-container mt-3">
        <div class="status-info mb-3">
          <?php if ($this->session->userdata('user')['role'] === 'Calon Santri' && (int) $psb->status === 0) : ?>
            <a href="<?= base_url('post/detail/3') ?>" class="btn btn-sm btn-<?= (in_array((int) $psb->status, [1, 3, 4])) ? 'success' : 'danger' ?>">
              <i class="zmdi zmdi-info"></i>
              <?= $controller->getStatus((int) $psb->status) ?>
            </a>
          <?php else : ?>
            <span class="badge badge-<?= (in_array((int) $psb->status, [1, 3, 4])) ? 'success' : 'danger' ?>">
              <?= $controller->getStatus((int) $psb->status) ?>
            </span>
          <?php endif ?>
        </div>

        <form id="form-calonsantri" autocomplete="off">
          <!-- CSRF -->
          <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />

          <!-- Temporary -->
          <input type="hidden" name="id" value="<?= $psb->id ?>" readonly />
          <input type="hidden" name="email" value="<?= $psb->email ?>" readonly />
          <input type="hidden" name="komitmen" value="<?= $psb->komitmen ?>" readonly />
          <input type="hidden" name="password" value="<?= $tempPassword ?>" readonly />
          <input type="hidden" name="password_confirm" value="<?= $tempPassword ?>" readonly />

          <div class="tab-container">
            <ul class="nav nav-tabs nav-responsive" role="tablist">
              <li class="nav-item">
                <a class="nav-link calonsantri-nav-personal <?= $activeTab === 'personal' ? 'active' : '' ?>" data-toggle="tab" href="#calonsantri-personal" role="tab">Personal</a>
              </li>
              <li class="nav-item">
                <a class="nav-link calonsantri-nav-orang_tua <?= $activeTab === 'orang_tua' ? 'active' : '' ?>" data-toggle="tab" href="#calonsantri-orang_tua" role="tab">Orang Tua</a>
              </li>
              <li class="nav-item">
                <a class="nav-link calonsantri-nav-prestasi <?= $activeTab === 'prestasi' ? 'active' : '' ?>" data-toggle="tab" href="#calonsantri-prestasi" role="tab">Prestasi</a>
              </li>
              <li class="nav-item">
                <a class="nav-link calonsantri-nav-komitmen <?= $activeTab === 'komitmen' ? 'active' : '' ?>" data-toggle="tab" href="#calonsantri-komitmen" role="tab">Komitmen</a>
              </li>
              <li class="nav-item">
                <a class="nav-link calonsantri-nav-surat <?= $activeTab === 'surat' ? 'active' : '' ?>" data-toggle="tab" href="#calonsantri-surat" role="tab">Surat Keterangan</a>
              </li>
              <?php if (in_array((int) $psb->status, array(3, 4))) : ?>
                <li class="nav-item">
                  <a class="nav-link calonsantri-nav-dokumen <?= $activeTab === 'dokumen' ? 'active' : '' ?>" data-toggle="tab" href="#calonsantri-dokumen" role="tab">Kelengkapan Dokumen</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link calonsantri-nav-seragam <?= $activeTab === 'seragam' ? 'active' : '' ?>" data-toggle="tab" href="#calonsantri-seragam" role="tab">Seragam</a>
                </li>
              <?php endif ?>
            </ul>
            <div class="tab-content clear-tab-content">
              <!-- Personal -->
              <div class="tab-pane fade show <?= $activeTab === 'personal' ? 'active' : '' ?>" id="calonsantri-personal" role="tabpanel">
                <?php include_once('form_santri.php') ?>
              </div>
              <!-- Orang Tua -->
              <div class="tab-pane fade show <?= $activeTab === 'orang_tua' ? 'active' : '' ?>" id="calonsantri-orang_tua" role="tabpanel">
                <?php include_once('form_orang_tua.php') ?>
              </div>
              <!-- Prestasi -->
              <div class="tab-pane fade show <?= $activeTab === 'prestasi' ? 'active' : '' ?>" id="calonsantri-prestasi" role="tabpanel">
                <?php include_once('form_prestasi.php') ?>
              </div>
              <!-- Komitmen -->
              <div class="tab-pane fade show <?= $activeTab === 'komitmen' ? 'active' : '' ?>" id="calonsantri-komitmen" role="tabpanel">
                <?php include_once('form_komitmen.php') ?>
              </div>
              <!-- Surat Keterangan -->
              <div class="tab-pane fade show <?= $activeTab === 'surat' ? 'active' : '' ?>" id="calonsantri-surat" role="tabpanel">
                <?php include_once('form_surat.php') ?>
              </div>
              <?php if (in_array((int) $psb->status, array(3, 4))) : ?>
                <!-- Kelengkapan Dokumen -->
                <div class="tab-pane fade show <?= $activeTab === 'dokumen' ? 'active' : '' ?>" id="calonsantri-dokumen" role="tabpanel">
                  <?php include_once('form_dokumen.php') ?>
                </div>
                <!-- Seragam -->
                <div class="tab-pane fade show <?= $activeTab === 'seragam' ? 'active' : '' ?>" id="calonsantri-seragam" role="tabpanel">
                  <?php include_once('form_seragam.php') ?>
                </div>
              <?php endif ?>
            </div>
          </div>

          <!-- Button -->
          <div class="row mt-3">
            <div class="col-md-6 col-12">
              <small class="form-text text-muted mt-3">
                Fields with red stars (<label required></label>) are required.
              </small>
            </div>
            <div class="col-md-6 col-12 text-right">
              <button type="submit" class="btn btn-primary btn-submit spinner-action-button <?= ($app->is_mobile) ? 'btn-block' : '' ?>" style="padding-left: 2.5rem; padding-right: 2.5rem;">
                <i class="zmdi zmdi-save"></i> Save
              </button>
            </div>
          </div>

        </form>
      </div>
    </div>
  </div>
</section>