<!-- Data Surat Keterangan -->
<fieldset>
  <div class="panel-surat panel-active">
    <div class="row">
      <div class="col-md-4 col-12">
        <label>
          Surat Keterangan dari SD/MI sederajat yang menyatakan bahwa Calon Santri
          masih duduk di kelas VI dan akan mengikuti Ujian Akhir atau Ijazah SD/MI maksimal 2 tahun dari tahun kelulusan
        </label>
      </div>
      <div class="col-md-8 col-12">
        <div class="form-group">
          <div class="upload-inline-xs">
            <div class="upload-button">
              <input type="file" name="surat_1" class="upload-pure-button psb-surat_1" accept="image/jpg,image/jpeg,image/png,image/gif" data-preview="surat_1" />
            </div>
            <div class="upload-preview data-preview-surat_1">
              <?php if (!is_null($psb->surat_1) && !empty($psb->surat_1)) : ?>
                <p class="mb-1">
                  <a href="<?= base_url($psb->surat_1) ?>" class="btn btn-light btn-sm" data-fancybox>
                    <i class="zmdi zmdi-download"></i>
                    View Current
                  </a>
                </p>
                <p class="mb-0 text-muted">
                  To change file, please click the button beside
                </p>
              <?php endif ?>
            </div>
          </div>
          <small class="form-text text-muted">
            Use file with format .JPG, .JPEG, .PNG or .GIF
          </small>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-4 col-12">
        <label>
          Surat Keterangan Berkelakuan Baik dari SD/MI sederajat
        </label>
      </div>
      <div class="col-md-8 col-12">
        <div class="form-group">
          <div class="upload-inline-xs">
            <div class="upload-button">
              <input type="file" name="surat_2" class="upload-pure-button psb-surat_2" accept="image/jpg,image/jpeg,image/png,image/gif" data-preview="surat_2" />
            </div>
            <div class="upload-preview data-preview-surat_2">
              <?php if (!is_null($psb->surat_2) && !empty($psb->surat_2)) : ?>
                <p class="mb-1">
                  <a href="<?= base_url($psb->surat_2) ?>" class="btn btn-light btn-sm" data-fancybox>
                    <i class="zmdi zmdi-download"></i>
                    View Current
                  </a>
                </p>
                <p class="mb-0 text-muted">
                  To change file, please click the button beside
                </p>
              <?php endif ?>
            </div>
          </div>
          <small class="form-text text-muted">
            Use file with format .JPG, .JPEG, .PNG or .GIF
          </small>
        </div>
      </div>
    </div>
  </div>
</fieldset>