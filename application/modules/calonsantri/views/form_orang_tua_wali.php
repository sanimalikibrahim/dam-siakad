<!-- Data Orang Tua : Wali -->
<div class="alert alert-light text-dark mb-4 text-center">
  <i class="zmdi zmdi-info"></i>
  Tidak perlu diisi jika memang tidak ada wali
</div>
<div class="row">
  <div class="col-md-6 col-12">
    <div class="form-group">
      <label>Nomor Kartu Keluarga (KK)</label>
      <input type="number" name="wali_no_kk" class="form-control mask-number psb-wali_no_kk" maxlength="16" placeholder="-" value="<?= $psb->wali_no_kk ?>">
      <i class="form-group__bar"></i>
    </div>
  </div>
  <div class="col-md-6 col-12">
    <div class="form-group">
      <label>Nomor Induk Keluarga (NIK)</label>
      <input type="number" name="wali_nik" class="form-control mask-number psb-wali_nik" maxlength="16" placeholder="-" value="<?= $psb->wali_nik ?>">
      <i class="form-group__bar"></i>
    </div>
  </div>
</div>
<div class="form-group">
  <label>Nama Wali</label>
  <input type="text" name="wali_nama_lengkap" class="form-control psb-wali_nama_lengkap" placeholder="-" value="<?= $psb->wali_nama_lengkap ?>">
  <i class="form-group__bar"></i>
</div>
<div class="row">
  <div class="col-md-8 col-12">
    <div class="form-group">
      <label>Tempat Lahir</label>
      <input type="text" name="wali_tempat_lahir" class="form-control psb-wali_tempat_lahir" placeholder="-" value="<?= $psb->wali_tempat_lahir ?>">
      <i class="form-group__bar"></i>
    </div>
  </div>
  <div class="col-md-4 col-12">
    <div class="form-group">
      <label>Tanggal Lahir</label>
      <input type="text" name="wali_tanggal_lahir" class="form-control flatpickr-date psb-wali_tanggal_lahir" placeholder="-" value="<?= ($psb->wali_tanggal_lahir !== '0000-00-00') ? $psb->wali_tanggal_lahir : '' ?>">
      <i class="form-group__bar"></i>
    </div>
  </div>
</div>
<div class="form-group">
  <label>Status</label>
  <div class="pt-1">
    <div class="form-check form-check-inline">
      <input class="form-check-input" type="radio" name="wali_status" id="wali_status-hidup" value="Masih Hidup" <?= ($psb->wali_status === 'Masih Hidup') ? 'checked' : '' ?>>
      <label class="form-check-label" for="wali_status-hidup">Masih Hidup</label>
    </div>
    <div class="form-check form-check-inline">
      <input class="form-check-input" type="radio" name="wali_status" id="wali_status-sudah_meninggal" value="Sudah Meninggal" <?= ($psb->wali_status === 'Sudah Meninggal') ? 'checked' : '' ?>>
      <label class="form-check-label" for="wali_status-sudah_meninggal">Sudah Meninggal</label>
    </div>
  </div>
</div>
<div class="form-group">
  <label>Pendidikan Terakhir</label>
  <div class="select">
    <select name="wali_pendidikan" class="form-control psb-wali_pendidikan" data-placeholder="Select &#8595;">
      <option disabled selected>Select &#8595;</option>
      <option value="Tidak tamat SD/MI/Sederajat" <?= ($psb->wali_pendidikan === 'Tidak tamat SD/MI/Sederajat') ? 'selected' : '' ?>>Tidak tamat SD/MI/Sederajat</option>
      <option value="SD/MI/Sederajat" <?= ($psb->wali_pendidikan === 'SD/MI/Sederajat') ? 'selected' : '' ?>>SD/MI/Sederajat</option>
      <option value="SMP/MTs/Sederajat" <?= ($psb->wali_pendidikan === 'SMP/MTs/Sederajat') ? 'selected' : '' ?>>SMP/MTs/Sederajat</option>
      <option value="SMA/MA/Sederajat" <?= ($psb->wali_pendidikan === 'SMA/MA/Sederajat') ? 'selected' : '' ?>>SMA/MA/Sederajat</option>
      <option value="Diploma" <?= ($psb->wali_pendidikan === 'Diploma') ? 'selected' : '' ?>>Diploma</option>
      <option value="Sarjana (S1)" <?= ($psb->wali_pendidikan === 'Sarjana (S1)') ? 'selected' : '' ?>>Sarjana (S1)</option>
      <option value="Magister (S2)" <?= ($psb->wali_pendidikan === 'Magister (S2)') ? 'selected' : '' ?>>Magister (S2)</option>
      <option value="Doktor (S3)" <?= ($psb->wali_pendidikan === 'Doktor (S3)') ? 'selected' : '' ?>>Doktor (S3)</option>
    </select>
    <i class="form-group__bar"></i>
  </div>
</div>
<div class="form-group">
  <label>Pekerjaan Utama</label>
  <div class="select">
    <select name="wali_pekerjaan" class="form-control psb-wali_pekerjaan" data-placeholder="Select &#8595;">
      <option disabled selected>Select &#8595;</option>
      <option value="Tidak Bekerja" <?= ($psb->wali_pekerjaan === 'Tidak Bekerja') ? 'selected' : '' ?>>Tidak Bekerja</option>
      <option value="Pensiunan" <?= ($psb->wali_pekerjaan === 'Pensiunan') ? 'selected' : '' ?>>Pensiunan</option>
      <option value="Karyawan Swasta" <?= ($psb->wali_pekerjaan === 'Karyawan Swasta') ? 'selected' : '' ?>>Karyawan Swasta</option>
      <option value="TNI/POLRI" <?= ($psb->wali_pekerjaan === 'TNI/POLRI') ? 'selected' : '' ?>>TNI/POLRI</option>
      <option value="Pengajar Honorer" <?= ($psb->wali_pekerjaan === 'Pengajar Honorer') ? 'selected' : '' ?>>Pengajar Honorer</option>
      <option value="Buruh Tani" <?= ($psb->wali_pekerjaan === 'Buruh Tani') ? 'selected' : '' ?>>Buruh Tani</option>
      <option value="Pengusaha" <?= ($psb->wali_pekerjaan === 'Pengusaha') ? 'selected' : '' ?>>Pengusaha</option>
      <option value="Pedagang Eceran" <?= ($psb->wali_pekerjaan === 'Pedagang Eceran') ? 'selected' : '' ?>>Pedagang Eceran</option>
      <option value="Sopir/Kondektur" <?= ($psb->wali_pekerjaan === 'Sopir/Kondektur') ? 'selected' : '' ?>>Sopir/Kondektur</option>
      <option value="Nelayan" <?= ($psb->wali_pekerjaan === 'Nelayan') ? 'selected' : '' ?>>Nelayan</option>
      <option value="Pekerja Pabrik" <?= ($psb->wali_pekerjaan === 'Pekerja Pabrik') ? 'selected' : '' ?>>Pekerja Pabrik</option>
      <option value="Tukang Bangunan" <?= ($psb->wali_pekerjaan === 'Tukang Bangunan') ? 'selected' : '' ?>>Tukang Bangunan</option>
      <option value="PNS" <?= ($psb->wali_pekerjaan === 'PNS') ? 'selected' : '' ?>>PNS</option>
      <option value="Lainnya" <?= ($psb->wali_pekerjaan === 'Lainnya') ? 'selected' : '' ?>>Lainnya</option>
    </select>
    <i class="form-group__bar"></i>
  </div>
</div>
<div class="row control-psb-wali_pekerjaan <?= (!in_array($psb->wali_pekerjaan, ['TNI/POLRI', 'PNS'])) ? 'hidden' : '' ?>">
  <div class="col-md-6 col-12">
    <div class="form-group">
      <label>Pekerjaan Utama: Jenis Profesi</label>
      <div class="select">
        <select name="wali_pekerjaan_jenis" class="form-control psb-wali_pekerjaan_jenis" data-placeholder="Select &#8595;">
          <option disabled selected>Select &#8595;</option>
          <option value="Pegawai Kementerian" <?= ($psb->wali_pekerjaan_jenis === 'Pegawai Kementerian') ? 'selected' : '' ?>>Pegawai Kementerian</option>
          <option value="TNI/POLRI" <?= ($psb->wali_pekerjaan_jenis === 'TNI/POLRI') ? 'selected' : '' ?>>TNI/POLRI</option>
          <option value="Guru/Dosen" <?= ($psb->wali_pekerjaan_jenis === 'Guru/Dosen') ? 'selected' : '' ?>>Guru/Dosen</option>
          <option value="TU.Sekolah/Madrasah" <?= ($psb->wali_pekerjaan_jenis === 'TU.Sekolah/Madrasah') ? 'selected' : '' ?>>TU.Sekolah/Madrasah</option>
          <option value="Pegawai Pemda" <?= ($psb->wali_pekerjaan_jenis === 'Pegawai Pemda') ? 'selected' : '' ?>>Pegawai Pemda</option>
          <option value="Dokter/Perawat" <?= ($psb->wali_pekerjaan_jenis === 'Dokter/Perawat') ? 'selected' : '' ?>>Dokter/Perawat</option>
          <option value="Lainnya" <?= ($psb->wali_pekerjaan_jenis === 'Lainnya') ? 'selected' : '' ?>>Lainnya</option>
        </select>
        <i class="form-group__bar"></i>
      </div>
    </div>
  </div>
  <div class="col-md-6 col-12">
    <div class="form-group">
      <label>Pekerjaan Utama: Pangkat/Golongan</label>
      <div class="select">
        <select name="wali_pekerjaan_pangkat" class="form-control psb-wali_pekerjaan_pangkat" data-placeholder="Select &#8595;">
          <option disabled selected>Select &#8595;</option>
          <option value="Golongan I/II" <?= ($psb->wali_pekerjaan_pangkat === 'Golongan I/II') ? 'selected' : '' ?>>Golongan I/II</option>
          <option value="Golongan III" <?= ($psb->wali_pekerjaan_pangkat === 'Golongan III') ? 'selected' : '' ?>>Golongan III</option>
          <option value="Golongan IV" <?= ($psb->wali_pekerjaan_pangkat === 'Golongan IV') ? 'selected' : '' ?>>Golongan IV</option>
          <option value="Tamtama" <?= ($psb->wali_pekerjaan_pangkat === 'Tamtama') ? 'selected' : '' ?>>Tamtama</option>
          <option value="Bintara" <?= ($psb->wali_pekerjaan_pangkat === 'Bintara') ? 'selected' : '' ?>>Bintara</option>
          <option value="Perwira" <?= ($psb->wali_pekerjaan_pangkat === 'Perwira') ? 'selected' : '' ?>>Perwira</option>
        </select>
        <i class="form-group__bar"></i>
      </div>
    </div>
  </div>
</div>
<div class="form-group">
  <label>Alamat Tempat Tinggal Wali</label>
  <input type="text" name="wali_alamat" class="form-control psb-wali_alamat" placeholder="-" value="<?= $psb->wali_alamat ?>">
  <i class="form-group__bar"></i>
</div>
<div class="row">
  <div class="col-md-6 col-12">
    <div class="form-group">
      <label>Kampung/Desa/Kelurahan</label>
      <input type="text" name="wali_kelurahan" class="form-control psb-wali_kelurahan" placeholder="-" value="<?= $psb->wali_kelurahan ?>">
      <i class="form-group__bar"></i>
    </div>
  </div>
  <div class="col-md-6 col-12">
    <div class="form-group">
      <label>Kecamatan</label>
      <input type="text" name="wali_kecamatan" class="form-control psb-wali_kecamatan" placeholder="-" value="<?= $psb->wali_kecamatan ?>">
      <i class="form-group__bar"></i>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-6 col-12">
    <div class="form-group">
      <label>Kota/Kabupaten</label>
      <input type="text" name="wali_kota" class="form-control psb-wali_kota" placeholder="-" value="<?= $psb->wali_kota ?>">
      <i class="form-group__bar"></i>
    </div>
  </div>
  <div class="col-md-6 col-12">
    <div class="form-group">
      <label>Provinsi</label>
      <input type="text" name="wali_provinsi" class="form-control psb-wali_provinsi" placeholder="-" value="<?= $psb->wali_provinsi ?>">
      <i class="form-group__bar"></i>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-6 col-12">
    <div class="form-group">
      <label>Telepon</label>
      <input type="number" name="wali_telepon" class="form-control mask-number psb-wali_telepon" maxlength="15" placeholder="-" value="<?= $psb->wali_telepon ?>">
      <i class="form-group__bar"></i>
    </div>
  </div>
  <div class="col-md-6 col-12">
    <div class="form-group">
      <label>Handphone</label>
      <input type="number" name="wali_handphone" class="form-control mask-number psb-wali_handphone" maxlength="15" placeholder="-" value="<?= $psb->wali_handphone ?>">
      <i class="form-group__bar"></i>
    </div>
  </div>
</div>
<div class="form-group">
  <label>Penghasilan Wali per Bulan</label>
  <div class="select">
    <select name="wali_penghasilan" class="form-control psb-wali_penghasilan" data-placeholder="Select &#8595;">
      <option disabled selected>Select &#8595;</option>
      <option value="< Rp.500.000,-" <?= ($psb->wali_penghasilan === '< Rp.500.000,-') ? 'selected' : '' ?>>< Rp.500.000,-</option> 
      <option value="Rp. 500.000,- s.d. Rp.1.000.000,-" <?= ($psb->wali_penghasilan === 'Rp. 500.000,- s.d. Rp.1.000.000,-') ? 'selected' : '' ?>>Rp. 500.000,- s.d. Rp.1.000.000,-</option>
      <option value="Rp.1.000.000,- s.d. Rp.2.000.000,-" <?= ($psb->wali_penghasilan === 'Rp.1.000.000,- s.d. Rp.2.000.000,-') ? 'selected' : '' ?>>Rp.1.000.000,- s.d. Rp.2.000.000,-</option>
      <option value="Rp.2.000.000,- s.d. Rp.3.000.000,-"> <?= ($psb->wali_penghasilan === 'Rp.2.000.000,- s.d. Rp.3.000.000,-') ? 'selected' : '' ?>Rp.2.000.000,- s.d. Rp.3.000.000,-</option>
      <option value="Rp.3.000.000,- s.d. Rp.5.000.000,-" <?= ($psb->wali_penghasilan === 'Rp.3.000.000,- s.d. Rp.5.000.000,-') ? 'selected' : '' ?>>Rp.3.000.000,- s.d. Rp.5.000.000,-</option>
      <option value="> Rp.5.000.000,-" <?= ($psb->wali_penghasilan === '> Rp.5.000.000,-') ? 'selected' : '' ?>>> Rp.5.000.000,-</option>
    </select>
    <i class="form-group__bar"></i>
  </div>
</div>