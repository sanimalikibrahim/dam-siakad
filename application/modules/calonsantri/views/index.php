<div class="body-loading">
    <div class="body-loading-content">
        <div class="card">
            <div class="card-body">
                <i class="zmdi zmdi-spinner zmdi-hc-spin"></i>
                Please wait...
                <div class="mb-2"></div>
                <span style="color: #9c9c9c; font-size: 1rem;">Don't close this tab activity!</span>
            </div>
        </div>
    </div>
</div>

<section id="calonsantri">
    <?php include_once('modal_generate_idcard.php') ?>

    <div class="card">
        <div class="card-body">
            <h4 class="card-title"><?php echo (isset($card_title)) ? $card_title : '' ?></h4>
            <h6 class="card-subtitle mb-0"><?php echo (isset($card_subTitle)) ? $card_subTitle : '' ?></h6>

            <div class="alert alert-light text-dark mt-4 mb-0">
                <i class="zmdi zmdi-info"></i>
                Setiap <i>action</i> yang dilakukan (tanpa konfirmasi) akan mengirim notifikasi via aplikasi dan email ke calon santri bersangkutan.
            </div>

            <div class="action-buttons border-bottom border-danger pb-2">
                <div class="table-action row">
                    <div class="buttons col">
                        <div class="input-group mb-0">
                            <div class="input-group-prepend">
                                <label class="input-group-text" style="height: 34.13px; background: #f2f2f2;">Filter</label>
                            </div>
                            <select class="custom-select filter-year" style="height: 34.13px; max-width: 150px;">
                                <?= $list_tahun ?>
                            </select>
                            <div class="input-group-apend">
                                <button class="btn btn--raised btn-primary btn--icon-text page-action-filter" style="height: 34.13px;">
                                    <i class="zmdi zmdi-filter-list"></i> Apply
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="table-responsive">
                <table id="table-calonsantri" class="table table-bordered">
                    <thead class="thead-default">
                        <tr>
                            <th width="100">No</th>
                            <th width="100">Foto</th>
                            <th>NISN</th>
                            <th>Nama Lengkap</th>
                            <th>Jenis Kelamin</th>
                            <th>Telepon</th>
                            <th>Kota</th>
                            <th>Status</th>
                            <th>Created</th>
                            <th width="240">Action</th>
                        </tr>
                    </thead>
                    <tfoot style="background: var(--gray);">
                        <tr>
                            <th colspan="4"></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th colspan="3"></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</section>