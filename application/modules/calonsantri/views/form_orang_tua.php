<!-- Data Orang Tua -->
<fieldset>
  <div class="panel-orang_tua panel-active">
    <nav>
      <ul class="nav nav-tabs nav-responsive-2" role="tablist">
        <li class="nav-item">
          <a class="nav-link active" id="nav-ayah-tab" data-toggle="tab" href="#nav-ayah" role="tab" aria-controls="nav-ayah" aria-selected="true">Ayah</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" id="nav-ibu-tab" data-toggle="tab" href="#nav-ibu" role="tab" aria-controls="nav-ibu" aria-selected="false">Ibu</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" id="nav-wali-tab" data-toggle="tab" href="#nav-wali" role="tab" aria-controls="nav-wali" aria-selected="false">Wali</a>
        </li>
      </ul>
    </nav>
    <div class="tab-content pb-0" id="nav-tabContent">
      <div class="tab-pane fade show active" id="nav-ayah" role="tabpanel" aria-labelledby="nav-ayah-tab">
        <?php include_once('form_orang_tua_ayah.php') ?>
      </div>
      <div class="tab-pane fade" id="nav-ibu" role="tabpanel" aria-labelledby="nav-ibu-tab">
        <?php include_once('form_orang_tua_ibu.php') ?>
      </div>
      <div class="tab-pane fade" id="nav-wali" role="tabpanel" aria-labelledby="nav-wali-tab">
        <?php include_once('form_orang_tua_wali.php') ?>
      </div>
    </div>
  </div>
</fieldset>