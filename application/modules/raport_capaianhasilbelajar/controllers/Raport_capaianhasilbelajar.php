<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . 'controllers/AppBackend.php');

class Raport_capaianhasilbelajar extends AppBackend
{
  function __construct()
  {
    parent::__construct();
    $this->load->model(['AppModel', 'RaportChbSikapResultModel']);
    $this->load->library('form_validation');
  }

  public function index()
  {
    $app = $this->app();
    $tahunAjar = (isset($app->raport_tahun_ajar)) ? $app->raport_tahun_ajar : '';
    $semester = (isset($app->raport_semester)) ? $app->raport_semester : '';
    $user_id = $this->session->userdata('user')['id'];
    $user_role = $this->session->userdata('user')['role'];

    if ($user_role === 'Guru') {
      $list_kelas_combo = $this->init_list_kelas_by_guru($user_id);
    } else {
      $list_kelas_combo = $this->init_list_kelas();
    };

    $pengetahuan_keterampilan = file_get_contents(FCPATH . '/directory/dummy/raport_mata_pelajaran.json');
    $pengetahuan_keterampilan = json_decode($pengetahuan_keterampilan, true);

    $data = array(
      'app' => $app,
      'main_js' => $this->load_main_js('raport_capaianhasilbelajar', false, array(
        'tahun_ajar' => $tahunAjar,
        'semester' => $semester,
      )),
      'card_title' => 'Raport › Capaian Hasil Belajar',
      'list_kelas_combo' => $list_kelas_combo,
      'pengetahuan_keterampilan' => $pengetahuan_keterampilan['data'],
      'tahun_ajar' => $tahunAjar,
      'semester' => $semester,
    );
    $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
    $this->template->load_view('index', $data, TRUE);
    $this->template->render();
  }

  public function ajax_get_all()
  {
    $this->handle_ajax_request();
    $isSearchColumn0 = $this->input->get('columns')[0]['search']['value'];
    $user_id = $this->session->userdata('user')['id'];
    $user_role = $this->session->userdata('user')['role'];
    $static_conditional_spec = array();
    $static_conditional_in_key = null;
    $static_conditional_in = array();

    // Empty default
    if (empty(trim($isSearchColumn0))) {
      $static_conditional_spec['id'] = null;
    };

    if ($user_role === 'Pembina') {
      $static_conditional_spec = array(
        'pembina' => $user_id
      );
    };

    $dtAjax_config = array(
      'table_name' => 'view_santri',
      'static_conditional_spec' => $static_conditional_spec,
      'static_conditional_in_key' => $static_conditional_in_key,
      'static_conditional_in' => $static_conditional_in,
      'order_column' => 4,
      'order_column_dir' => 'asc'
    );

    $response = $this->AppModel->getData_dtAjax($dtAjax_config);
    echo json_encode($response);
  }

  public function ajax_get_sikap_value()
  {
    $semester = $this->input->get('semester');
    $tahun_ajar = $this->input->get('tahun_ajar');
    $kelas = $this->input->get('kelas');

    $response = $this->RaportChbSikapResultModel->getAll(array(
      'semester' => $semester,
      'tahun_ajar' => $tahun_ajar,
      'kelas' => $kelas
    ));
    echo json_encode($response);
  }

  public function ajax_get_pengetahuan_keterampilan_value()
  {
    $pengetahuan_keterampilan_value = file_get_contents(FCPATH . '/directory/dummy/raport_pengetahuan_keterampilan_value.json');
    $pengetahuan_keterampilan_value = json_decode($pengetahuan_keterampilan_value, true);

    echo json_encode($pengetahuan_keterampilan_value);
  }

  public function ajax_get_pengetahuan_keterampilan_note_value()
  {
    $pengetahuan_keterampilan_value = file_get_contents(FCPATH . '/directory/dummy/raport_pengetahuan_keterampilan_note_value.json');
    $pengetahuan_keterampilan_value = json_decode($pengetahuan_keterampilan_value, true);

    echo json_encode($pengetahuan_keterampilan_value);
  }

  public function ajax_save_sikap()
  {
    $this->handle_ajax_request();

    $semester = $this->input->post('p_semester');
    $tahunAjar = $this->input->post('p_tahun_ajar');
    $kelas = $this->input->post('p_kelas');
    $data = $this->input->post('sikap');

    $response = $this->RaportChbSikapResultModel->insertBatchDuplicateUpdate($semester, $tahunAjar, $kelas, $data);
    echo json_encode($response);
  }
}
