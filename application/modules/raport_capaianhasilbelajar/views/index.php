<div class="alert alert-dark">
    <span class="badge badge-light">Tahun Ajar : <?= (!empty($tahun_ajar)) ? $tahun_ajar : '<span class="text-red">(not-set)</span>' ?></span>
    <span class="badge badge-light">Semester : <?= (!empty($semester)) ? $semester : '<span class="text-red">(not-set)</span>' ?></span>
</div>

<section id="raport_capaianhasilbelajar">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title"><?php echo (isset($card_title)) ? $card_title : '' ?></h4>
            <h6 class="card-subtitle"><?php echo (isset($card_subTitle)) ? $card_subTitle : '' ?></h6>

            <?php if (!in_array($this->session->userdata('user')['role'], ['Pembina', 'Orang Tua'])) : ?>
                <div class="table-action row">
                    <div class="buttons col">
                        <div class="input-group mb-0">
                            <div class="input-group-prepend">
                                <label class="input-group-text" style="height: 34.13px; background: #f2f2f2;">Kelas</label>
                            </div>
                            <select class="custom-select raport_capaianhasilbelajar-filter-kelas" style="height: 34.13px; max-width: 230px;">
                                <?= $list_kelas_combo ?>
                            </select>
                        </div>
                    </div>
                </div>
            <?php endif; ?>

            <div class="tab-container mt-2">
                <ul class="nav nav-tabs nav-responsive" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#nav-1" role="tab" onclick="setTabState(0)">Sikap</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#nav-2" role="tab" onclick="setTabState(1)">Pengetahuan & Keterampilan</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#nav-3" role="tab" onclick="setTabState(2)">Ekstra Kurikuler</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#nav-4" role="tab" onclick="setTabState(3)">Prestasi</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#nav-5" role="tab" onclick="setTabState(4)">Ketidakhadiran</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#nav-6" role="tab" onclick="setTabState(5)">Catatan Wali Kelas</a>
                    </li>
                </ul>
                <div class="tab-content clear-tab-content p-0">
                    <div class="tab-pane active fade show" id="nav-1" role="tabpanel">
                        <!-- Sikap -->
                        <?php include_once(APPPATH . 'modules/raport_capaianhasilbelajar/views/sikap.php') ?>
                    </div>
                    <div class="tab-pane fade show" id="nav-2" role="tabpanel">
                        <!-- Pengetahuan & Keterampilan -->
                        <?php include_once(APPPATH . 'modules/raport_capaianhasilbelajar/views/pengetahuan_keterampilan.php') ?>
                    </div>
                    <div class="tab-pane fade show" id="nav-3" role="tabpanel">
                        <!-- Ekstra Kurikuler -->
                        <?php include_once(APPPATH . 'modules/raport_capaianhasilbelajar/views/ekstra_kurikuler.php') ?>
                    </div>
                    <div class="tab-pane fade show" id="nav-4" role="tabpanel">
                        <!-- Prestasi -->
                        <?php include_once(APPPATH . 'modules/raport_capaianhasilbelajar/views/prestasi.php') ?>
                    </div>
                    <div class="tab-pane fade show" id="nav-5" role="tabpanel">
                        <!-- Ketidakhadiran -->
                        <?php include_once(APPPATH . 'modules/raport_capaianhasilbelajar/views/ketidakhadiran.php') ?>
                    </div>
                    <div class="tab-pane fade show" id="nav-6" role="tabpanel">
                        <!-- Catatan Wali Kelas -->
                        <?php include_once(APPPATH . 'modules/raport_capaianhasilbelajar/views/catatan_wali_kelas.php') ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>