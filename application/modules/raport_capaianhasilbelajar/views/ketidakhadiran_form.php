<div class="modal fade" id="modal-raport_capaianhasilbelajar-ketidakhadiran" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title pull-left">Ketidakhadiran</h5>
      </div>
      <div class="spinner">
        <div class="lds-hourglass"></div>
      </div>
      <div class="modal-body">
        <form id="form-raport_capaianhasilbelajar-ketidakhadiran" autocomplete="off">
          <!-- CSRF -->
          <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />

          <div class="row">
            <div class="col-xs-12 col-md-2">
              <b>Nomor Induk / NISN :</b>
            </div>
            <div class="col-xs-12 col-md-10">
              <span class="nisn">-</span>
            </div>
            <div class="col-xs-12 col-md-2">
              <b>Nama Peserta Didik :</b>
            </div>
            <div class="col-xs-12 col-md-10">
              <span class="nama_lengkap">-</span>
            </div>
          </div>
          <div class="border-bottom border-danger mb-3">&nbsp;</div>

          <div class="row">
            <div class="col-xs-12 col-md-2">Sakit</div>
            <div class="col-xs-12 col-md-2">
              <div class="input-group mb-2">
                <input type="number" name="ketidakhadiran[sakit]" class="form-control" min="0" style="text-align: center;" placeholder="0" />
                <div class="input-group-append">
                  <span class="input-group-text">hari</span>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-12 col-md-2">Izin</div>
            <div class="col-xs-12 col-md-2">
              <div class="input-group mb-2">
                <input type="number" name="ketidakhadiran[izin]" class="form-control" min="0" style="text-align: center;" placeholder="0" />
                <div class="input-group-append">
                  <span class="input-group-text">hari</span>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-12 col-md-2">Tanpa Keterangan</div>
            <div class="col-xs-12 col-md-2">
              <div class="input-group mb-0">
                <input type="number" name="ketidakhadiran[tanpa_keterangan]" class="form-control" min="0" style="text-align: center;" placeholder="0" />
                <div class="input-group-append">
                  <span class="input-group-text">hari</span>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success btn--icon-text action-save">
          <i class="zmdi zmdi-save"></i> Save
        </button>
        <button type="button" class="btn btn-light btn--icon-text action-cancel" data-dismiss="modal">
          Cancel
        </button>
      </div>
    </div>
  </div>
</div>