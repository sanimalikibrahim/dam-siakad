<div class="modal fade" id="modal-raport_capaianhasilbelajar-prestasi" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title pull-left">Prestasi</h5>
      </div>
      <div class="spinner">
        <div class="lds-hourglass"></div>
      </div>
      <div class="modal-body">
        <form id="form-raport_capaianhasilbelajar-prestasi" autocomplete="off">
          <!-- CSRF -->
          <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />

          <div class="row">
            <div class="col-xs-12 col-md-2">
              <b>Nomor Induk / NISN :</b>
            </div>
            <div class="col-xs-12 col-md-10">
              <span class="nisn">-</span>
            </div>
            <div class="col-xs-12 col-md-2">
              <b>Nama Peserta Didik :</b>
            </div>
            <div class="col-xs-12 col-md-10">
              <span class="nama_lengkap">-</span>
            </div>
          </div>
          <div class="border-bottom border-danger mb-3">&nbsp;</div>

          <div class="table-responsive">
            <table class="table table-bordered table-hover table-sm">
              <thead class="thead-default text-center">
                <tr>
                  <th width="50">No</th>
                  <th>Catatan Prestasi</th>
                </tr>
              </thead>
              <tbody>
                <?php for ($i = 1; $i <= 5; $i++) : ?>
                  <tr>
                    <td valign="top"><?= $i ?></td>
                    <td valign="top">
                      <textarea name="prestasi[<?= $i ?>][catatan]" class="form-control textarea-autosize bg-white" rows="1"></textarea>
                    </td>
                  </tr>
                <?php endfor ?>
              </tbody>
            </table>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success btn--icon-text action-save">
          <i class="zmdi zmdi-save"></i> Save
        </button>
        <button type="button" class="btn btn-light btn--icon-text action-cancel" data-dismiss="modal">
          Cancel
        </button>
      </div>
    </div>
  </div>
</div>