<div class="table-responsive">
    <table class="table table-bordered table-hover table-sm">
        <thead class="thead-default text-center">
            <tr>
                <th colspan="2">MATA PELAJARAN</th>
                <th width="120">KOMPETENSI</th>
                <th width="400">CATATAN</th>
            </tr>
        </thead>
        <tbody>
            <?php if (count($pengetahuan_keterampilan) > 0) : ?>
                <?php foreach ($pengetahuan_keterampilan as $index_kelompok => $kelompok) : ?>
                    <!-- Kelompok -->
                    <tr style="background: #f8f8f8;">
                        <td colspan="7"><b><?= $kelompok['kelompok_nama'] ?></b></td>
                    </tr>
                    <!-- END ## Kelompok -->
                    <?php if (count($kelompok['items']) > 0) : ?>
                        <?php $no = 1; ?>
                        <?php foreach ($kelompok['items'] as $index_mapel => $mapel) : ?>
                            <!-- Level 1 -->
                            <?php if ($mapel['is_editable'] === 1) : ?>
                                <tr>
                                    <td valign="top" rowspan="2" align="center" style="vertical-align: middle;"><?= $no++ ?></td>
                                    <td valign="middle" rowspan="2" style="vertical-align: middle;"><?= $mapel['mapel_nama'] ?></td>
                                    <td valign="middle" align="center" style="vertical-align: middle;">Pengetahuan</td>
                                    <td valign="top">
                                        <textarea name="pengetahuan_note[<?= $mapel['mapel_id'] ?>][pengetahuan_catatan]" class="form-control textarea-autosize" rows="1"></textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="middle" align="center" style="vertical-align: middle;">Keterampilan</td>
                                    <td valign="top">
                                        <textarea name="keterampilan_note[<?= $mapel['mapel_id'] ?>][keterampilan_catatan]" class="form-control textarea-autosize" rows="1"></textarea>
                                    </td>
                                </tr>
                            <?php else : ?>
                                <tr>
                                    <td valign="top" align="center"><?= $no++ ?></td>
                                    <td valign="top" colspan="3"><?= $mapel['mapel_nama'] ?></td>
                                </tr>
                            <?php endif ?>
                            <!-- END ## Level 1 -->
                            <?php if (count($mapel['items']) > 0) : ?>
                                <?php foreach ($mapel['items'] as $index_mapel2 => $mapel2) : ?>
                                    <!-- Level 2 -->
                                    <?php if ($mapel2['is_editable'] === 1) : ?>
                                        <tr>
                                            <td valign="top" align="center" rowspan="2" style="vertical-align: middle;">&nbsp;</td>
                                            <td valign="middle" rowspan="2" style="vertical-align: middle;"><?= $mapel2['mapel_nama'] ?></td>
                                            <td valign="middle" align="center" style="vertical-align: middle;">Pengetahuan</td>
                                            <td valign="top">
                                                <textarea name="pengetahuan_note[<?= $mapel2['mapel_id'] ?>][pengetahuan_catatan]" class="form-control textarea-autosize" rows="1"></textarea>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="middle" align="center" style="vertical-align: middle;">Keterampilan</td>
                                            <td valign="top">
                                                <textarea name="keterampilan_note[<?= $mapel2['mapel_id'] ?>][keterampilan_catatan]" class="form-control textarea-autosize" rows="1"></textarea>
                                            </td>
                                        </tr>
                                    <?php else : ?>
                                        <tr>
                                            <td valign="top" align="center"><?= $no++ ?></td>
                                            <td valign="top" colspan="3"><?= $mapel2['mapel_nama'] ?></td>
                                        </tr>
                                    <?php endif ?>
                                    <!-- END ## Level 2 -->
                                <?php endforeach ?>
                            <?php endif ?>
                        <?php endforeach ?>
                    <?php endif ?>
                <?php endforeach ?>
            <?php else : ?>
                <tr>
                    <td colspan="7">No data available in table</td>
                </tr>
            <?php endif ?>
        </tbody>
    </table>
</div>