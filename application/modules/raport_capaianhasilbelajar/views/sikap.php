<div class="table-responsive">
    <form id="form-raport_capaianhasilbelajar-sikap" method="post" autocomplete="off">
        <!-- CSRF -->
        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
        <input type="hidden" name="p_semester" class="p_semester" value="<?= $semester ?>" readonly />
        <input type="hidden" name="p_tahun_ajar" class="p_tahun_ajar" value="<?= $tahun_ajar ?>" readonly />
        <input type="hidden" name="p_kelas" class="p_kelas" value="" readonly />

        <table id="table-raport_capaianhasilbelajar-sikap" class="table table-bordered table-hover">
            <thead class="thead-default">
                <tr>
                    <th rowspan="2" width="170">Kelas Concated</th>
                    <th rowspan="2" width="50">No</th>
                    <th rowspan="2" width="80">NISN</th>
                    <th rowspan="2" width="80">NIL</th>
                    <th rowspan="2">Nama Lengkap</th>
                    <th colspan="2" class="text-center">Sikap Spiritual</th>
                    <th colspan="2" class="text-center">Sikap Sosial</th>
                </tr>
                <tr>
                    <!-- Sikap Spiritual -->
                    <th style="width: 150px;">Predikat</th>
                    <th style="min-width: 150px;">Deskripsi</th>
                    <!-- Sikap Sosial -->
                    <th style="width: 150px;">Predikat</th>
                    <th style="min-width: 150px;">Deskripsi</th>
                </tr>
            </thead>
        </table>
    </form>
</div>

<button class="btn btn-success btn--icon-text btn-action-float hidden action-rchb-sikap-save">
    <i class="zmdi zmdi-save"></i> Save Changes
</button>