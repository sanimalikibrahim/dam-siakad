<div class="modal fade" id="modal-raport_capaianhasilbelajar-pengetahuan_keterampilan" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title pull-left">Pengetahuan & Keterampilan</h5>
      </div>
      <div class="spinner">
        <div class="lds-hourglass"></div>
      </div>
      <div class="modal-body">
        <form id="form-raport_capaianhasilbelajar-pengetahuan_keterampilan" autocomplete="off">
          <!-- CSRF -->
          <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />

          <div class="row">
            <div class="col-xs-12 col-md-2">
              <b>Nomor Induk / NISN :</b>
            </div>
            <div class="col-xs-12 col-md-10">
              <span class="nisn">-</span>
            </div>
            <div class="col-xs-12 col-md-2">
              <b>Nama Peserta Didik :</b>
            </div>
            <div class="col-xs-12 col-md-10">
              <span class="nama_lengkap">-</span>
            </div>
          </div>
          <div class="border-bottom border-danger mb-3">&nbsp;</div>

          <div class="tab-container">
            <ul class="nav nav-tabs nav-responsive" role="tablist">
              <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#nav-rchb-1" role="tab">Nilai</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#nav-rchb-2" role="tab">Deskripsi</a>
              </li>
            </ul>
            <div class="tab-content clear-tab-content p-0">
              <div class="tab-pane active fade show" id="nav-rchb-1" role="tabpanel">
                <div class="mb-2"></div>
                <?php include_once(APPPATH . 'modules/raport_capaianhasilbelajar/views/pengetahuan_keterampilan_form_table_value.php') ?>
              </div>
              <div class="tab-pane fade show" id="nav-rchb-2" role="tabpanel">
                <div class="mb-2"></div>
                <?php include_once(APPPATH . 'modules/raport_capaianhasilbelajar/views/pengetahuan_keterampilan_form_table_deskripsi.php') ?>
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success btn--icon-text action-save">
          <i class="zmdi zmdi-save"></i> Save
        </button>
        <button type="button" class="btn btn-light btn--icon-text action-cancel" data-dismiss="modal">
          Cancel
        </button>
      </div>
    </div>
  </div>
</div>