<div class="modal fade" id="modal-raport_capaianhasilbelajar-catatan_wali_kelas" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title pull-left">Catatan Wali Kelas</h5>
      </div>
      <div class="spinner">
        <div class="lds-hourglass"></div>
      </div>
      <div class="modal-body">
        <form id="form-raport_capaianhasilbelajar-catatan_wali_kelas" autocomplete="off">
          <!-- CSRF -->
          <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />

          <div class="row">
            <div class="col-xs-12 col-md-2">
              <b>Nomor Induk / NISN :</b>
            </div>
            <div class="col-xs-12 col-md-10">
              <span class="nisn">-</span>
            </div>
            <div class="col-xs-12 col-md-2">
              <b>Nama Peserta Didik :</b>
            </div>
            <div class="col-xs-12 col-md-10">
              <span class="nama_lengkap">-</span>
            </div>
          </div>
          <div class="border-bottom border-danger mb-3">&nbsp;</div>

          <div class="form-group mb-0">
            <label class="control-label">Catatan</label>
            <textarea name="catatan" class="form-control textarea-autosize" rows="1" placeholder="Tulis disini..."></textarea>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success btn--icon-text action-save">
          <i class="zmdi zmdi-save"></i> Save
        </button>
        <button type="button" class="btn btn-light btn--icon-text action-cancel" data-dismiss="modal">
          Cancel
        </button>
      </div>
    </div>
  </div>
</div>