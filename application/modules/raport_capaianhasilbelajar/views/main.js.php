<script type="text/javascript">
  $(document).ready(function() {

    var _key = "";
    var _tabIndex = 0;
    var _tahun_ajar = "<?= (!empty($tahun_ajar)) ? $tahun_ajar : '' ?>";
    var _semester = "<?= (!empty($semester)) ? $semester : '' ?>";
    var _activeRole = "<?= $this->session->userdata('user')['role'] ?>";
    var _section = "raport_capaianhasilbelajar";
    var _table_rchb_sikap = "table-raport_capaianhasilbelajar-sikap";
    var _table_rchb_pengetahuan_keterampilan = "table-raport_capaianhasilbelajar-pengetahuan_keterampilan";
    var _table_rchb_ekstra_kurikuler = "table-raport_capaianhasilbelajar-ekstra_kurikuler";
    var _table_rchb_prestasi = "table-raport_capaianhasilbelajar-prestasi";
    var _table_rchb_ketidakhadiran = "table-raport_capaianhasilbelajar-ketidakhadiran";
    var _table_rchb_catatan_wali_kelas = "table-raport_capaianhasilbelajar-catatan_wali_kelas";
    var _modal_rchb_pengetahuan_keterampilan = "modal-raport_capaianhasilbelajar-pengetahuan_keterampilan";
    var _modal_rchb_ekstra_kurikuler = "modal-raport_capaianhasilbelajar-ekstra_kurikuler";
    var _modal_rchb_prestasi = "modal-raport_capaianhasilbelajar-prestasi";
    var _modal_rchb_ketidakhadiran = "modal-raport_capaianhasilbelajar-ketidakhadiran";
    var _modal_rchb_catatan_wali_kelas = "modal-raport_capaianhasilbelajar-catatan_wali_kelas";
    var _form_rchb_sikap = "form-raport_capaianhasilbelajar-sikap";
    var _form_rchb_pengetahuan_keterampilan = "form-raport_capaianhasilbelajar-pengetahuan_keterampilan";
    var _form_rchb_ekstra_kurikuler = "form-raport_capaianhasilbelajar-ekstra_kurikuler";
    var _form_rchb_prestasi = "form-raport_capaianhasilbelajar-prestasi";
    var _form_rchb_ketidakhadiran = "form-raport_capaianhasilbelajar-ketidakhadiran";
    var _form_rchb_catatan_wali_kelas = "form-raport_capaianhasilbelajar-catatan_wali_kelas";

    var _combo_predikat = function(name, value) {
      return `
        <select name="${name}" class="custom-select p-0 bg-white" style="text-align: center; height: 26px;">
          <option disabled selected>Select &#8595;</option>
          <option value="A" ${(value === 'A' ? 'selected' : '')}>A</option>
          <option value="B" ${(value === 'B' ? 'selected' : '')}>B</option>
          <option value="C" ${(value === 'C' ? 'selected' : '')}>C</option>
          <option value="D" ${(value === 'D' ? 'selected' : '')}>D</option>
        </select>
      `;
    };

    // Initialize DataTables: Sikap
    if ($("#" + _table_rchb_sikap)[0]) {
      var table_rchb_sikap = $("#" + _table_rchb_sikap).DataTable({
        processing: true,
        serverSide: true,
        ajax: {
          url: "<?php echo base_url('raport_capaianhasilbelajar/ajax_get_all/') ?>",
          type: "get"
        },
        columns: [{
            data: "kelas_concated"
          }, {
            data: null,
            render: function(data, type, row, meta) {
              return meta.row + meta.settings._iDisplayStart + 1;
            }
          },
          {
            data: "nisn"
          },
          {
            data: "nomor_induk_lokal"
          },
          {
            data: "nama_lengkap"
          },
          {
            data: null, // sikap_spiritual_predikat
            render: function(data, type, row, meta) {
              return _combo_predikat(`sikap[${row.id}][sikap_spiritual_predikat]`, null);
            }
          },
          {
            data: null, // sikap_spiritual_deskripsi
            render: function(data, type, row, meta) {
              return `<textarea name="sikap[${row.id}][sikap_spiritual_deskripsi]" rows="1" style="width: 100%; border: 1px solid rgb(236, 239, 241);"></textarea>`;
            }
          },
          {
            data: null, // sikap_sosial_predikat
            render: function(data, type, row, meta) {
              return _combo_predikat(`sikap[${row.id}][sikap_sosial_predikat]`, null);
            }
          },
          {
            data: null, // sikap_sosial_deskripsi
            render: function(data, type, row, meta) {
              return `<textarea name="sikap[${row.id}][sikap_sosial_deskripsi]" rows="1" style="width: 100%; border: 1px solid rgb(236, 239, 241);"></textarea>`;
            }
          }
        ],
        autoWidth: !1,
        responsive: {
          details: {
            renderer: function(api, rowIdx, columns) {
              var hideColumn = [9];
              var data = $.map(columns, function(col, i) {
                return ($.inArray(col.columnIndex, hideColumn)) ?
                  '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                  '<td class="dt-details-td">' + col.title + ':' + '</td> ' +
                  '<td class="dt-details-td">' + col.data + '</td>' +
                  '</tr>' :
                  '';
              }).join('');

              return data ? $('<table/>').append(data) : false;
            },
            type: "inline",
            target: 'tr',
          }
        },
        columnDefs: [{
          "targets": [0],
          "visible": false
        }, {
          className: 'desktop',
          targets: [0, 1, 2, 3, 4, 5, 6, 7, 8]
        }, {
          className: 'tablet',
          targets: [0, 1, 2, 3]
        }, {
          className: 'mobile',
          targets: [0, 3]
        }, {
          responsivePriority: 2,
          targets: -1
        }],
        pageLength: 100,
        language: {
          searchPlaceholder: "Search...",
          sProcessing: '<div style="text-align: center;"><div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div></div>'
        },
        sDom: '<"dataTables_ct"><"dataTables__top"fb>rt<"dataTables__bottom"ip><"clear">',
        buttons: [{
          extend: "excelHtml5",
          title: "Export Result"
        }, {
          extend: "print",
          title: "Export Result"
        }],
        initComplete: function(a, b) {
          $(this).closest(".dataTables_wrapper").find(".dataTables__top").prepend(
            '<div class="dataTables_buttons hidden-sm-down actions">' +
            '<span class="actions__item zmdi zmdi-refresh" data-table-action="reload" title="Reload" />' +
            '</div>'
          );
        },
        drawCallback: function() {
          var dataCount = $("#" + _table_rchb_sikap).DataTable().data().count();

          if (dataCount > 0) {
            $(".action-rchb-sikap-save").show();
            fetch_sikap_value();
          } else {
            $(".action-rchb-sikap-save").hide();
          };
        }
      });

      $(".dataTables_filter input[type=search]").focus(function() {
        $(this).closest(".dataTables_filter").addClass("dataTables_filter--toggled")
      });

      $(".dataTables_filter input[type=search]").blur(function() {
        $(this).closest(".dataTables_filter").removeClass("dataTables_filter--toggled")
      });

      $("body").on("click", "[data-table-action]", function(a) {
        a.preventDefault();
        var b = $(this).data("table-action");
        if ("reload" === b) {
          $("#" + _table_rchb_sikap).DataTable().ajax.reload(null, false);
        };
      });
    };

    // Initialize DataTables: Pengetahuan & Keterampilan
    if ($("#" + _table_rchb_pengetahuan_keterampilan)[0]) {
      var table_rchb_pengetahuan_keterampilan = $("#" + _table_rchb_pengetahuan_keterampilan).DataTable({
        processing: true,
        serverSide: true,
        ajax: {
          url: "<?php echo base_url('raport_capaianhasilbelajar/ajax_get_all/') ?>",
          type: "get"
        },
        columns: [{
            data: "kelas_concated"
          }, {
            data: null,
            render: function(data, type, row, meta) {
              return meta.row + meta.settings._iDisplayStart + 1;
            }
          },
          {
            data: "nisn"
          },
          {
            data: "nomor_induk_lokal"
          },
          {
            data: "nama_lengkap"
          },
          {
            data: null,
            render: function(data, type, row, meta) {
              var status = sample(['Sudah Input', 'Belum Input']);
              var statusClass = (status === 'Sudah Input') ? 'text-green' : 'text-red';
              return `<span class="${statusClass}">${status}</span>`;
            }
          },
          {
            data: null,
            className: "center",
            render: function(data, type, row, meta) {
              var buttons = '<div class="action">';
              buttons += '<a href="javascript:;" class="btn btn-sm btn-light btn-table-action action-input" data-toggle="modal" data-target="#' + _modal_rchb_pengetahuan_keterampilan + '"><i class="zmdi zmdi-edit"></i> Input</a>';
              buttons += '</div>';
              return buttons;
            },
          }
        ],
        autoWidth: !1,
        responsive: {
          details: {
            renderer: function(api, rowIdx, columns) {
              var hideColumn = [9];
              var data = $.map(columns, function(col, i) {
                return ($.inArray(col.columnIndex, hideColumn)) ?
                  '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                  '<td class="dt-details-td">' + col.title + ':' + '</td> ' +
                  '<td class="dt-details-td">' + col.data + '</td>' +
                  '</tr>' :
                  '';
              }).join('');

              return data ? $('<table/>').append(data) : false;
            },
            type: "inline",
            target: 'tr',
          }
        },
        columnDefs: [{
          "targets": [0],
          "visible": false
        }, {
          className: 'desktop',
          targets: [0, 1, 2, 3, 4, 5, 6]
        }, {
          className: 'tablet',
          targets: [0, 1, 2, 3]
        }, {
          className: 'mobile',
          targets: [0, 3]
        }, {
          responsivePriority: 2,
          targets: -1
        }],
        pageLength: 100,
        language: {
          searchPlaceholder: "Search...",
          sProcessing: '<div style="text-align: center;"><div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div></div>'
        },
        sDom: '<"dataTables_ct"><"dataTables__top"fb>rt<"dataTables__bottom"ip><"clear">',
        buttons: [{
          extend: "excelHtml5",
          title: "Export Result"
        }, {
          extend: "print",
          title: "Export Result"
        }],
        initComplete: function(a, b) {
          $(this).closest(".dataTables_wrapper").find(".dataTables__top").prepend(
            '<div class="dataTables_buttons hidden-sm-down actions">' +
            '<span class="actions__item zmdi zmdi-refresh" data-table-action="reload" title="Reload" />' +
            '</div>'
          );
        },
      });

      $(".dataTables_filter input[type=search]").focus(function() {
        $(this).closest(".dataTables_filter").addClass("dataTables_filter--toggled")
      });

      $(".dataTables_filter input[type=search]").blur(function() {
        $(this).closest(".dataTables_filter").removeClass("dataTables_filter--toggled")
      });

      $("body").on("click", "[data-table-action]", function(a) {
        a.preventDefault();
        var b = $(this).data("table-action");
        if ("reload" === b) {
          $("#" + _table_rchb_pengetahuan_keterampilan).DataTable().ajax.reload(null, false);
        };
      });
    };

    // Initialize DataTables: Ekstra Kurikuler
    if ($("#" + _table_rchb_ekstra_kurikuler)[0]) {
      var table_rchb_ekstra_kurikuler = $("#" + _table_rchb_ekstra_kurikuler).DataTable({
        processing: true,
        serverSide: true,
        ajax: {
          url: "<?php echo base_url('raport_capaianhasilbelajar/ajax_get_all/') ?>",
          type: "get"
        },
        columns: [{
            data: "kelas_concated"
          }, {
            data: null,
            render: function(data, type, row, meta) {
              return meta.row + meta.settings._iDisplayStart + 1;
            }
          },
          {
            data: "nisn"
          },
          {
            data: "nomor_induk_lokal"
          },
          {
            data: "nama_lengkap"
          },
          {
            data: null,
            render: function(data, type, row, meta) {
              var status = sample(['Sudah Input', 'Belum Input']);
              var statusClass = (status === 'Sudah Input') ? 'text-green' : 'text-red';
              return `<span class="${statusClass}">${status}</span>`;
            }
          },
          {
            data: null,
            className: "center",
            render: function(data, type, row, meta) {
              var buttons = '<div class="action">';
              buttons += '<a href="javascript:;" class="btn btn-sm btn-light btn-table-action action-input" data-toggle="modal" data-target="#' + _modal_rchb_ekstra_kurikuler + '"><i class="zmdi zmdi-edit"></i> Input</a>';
              buttons += '</div>';
              return buttons;
            },
          }
        ],
        autoWidth: !1,
        responsive: {
          details: {
            renderer: function(api, rowIdx, columns) {
              var hideColumn = [9];
              var data = $.map(columns, function(col, i) {
                return ($.inArray(col.columnIndex, hideColumn)) ?
                  '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                  '<td class="dt-details-td">' + col.title + ':' + '</td> ' +
                  '<td class="dt-details-td">' + col.data + '</td>' +
                  '</tr>' :
                  '';
              }).join('');

              return data ? $('<table/>').append(data) : false;
            },
            type: "inline",
            target: 'tr',
          }
        },
        columnDefs: [{
          "targets": [0],
          "visible": false
        }, {
          className: 'desktop',
          targets: [0, 1, 2, 3, 4, 5, 6]
        }, {
          className: 'tablet',
          targets: [0, 1, 2, 3]
        }, {
          className: 'mobile',
          targets: [0, 3]
        }, {
          responsivePriority: 2,
          targets: -1
        }],
        pageLength: 100,
        language: {
          searchPlaceholder: "Search...",
          sProcessing: '<div style="text-align: center;"><div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div></div>'
        },
        sDom: '<"dataTables_ct"><"dataTables__top"fb>rt<"dataTables__bottom"ip><"clear">',
        buttons: [{
          extend: "excelHtml5",
          title: "Export Result"
        }, {
          extend: "print",
          title: "Export Result"
        }],
        initComplete: function(a, b) {
          $(this).closest(".dataTables_wrapper").find(".dataTables__top").prepend(
            '<div class="dataTables_buttons hidden-sm-down actions">' +
            '<span class="actions__item zmdi zmdi-refresh" data-table-action="reload" title="Reload" />' +
            '</div>'
          );
        },
      });

      $(".dataTables_filter input[type=search]").focus(function() {
        $(this).closest(".dataTables_filter").addClass("dataTables_filter--toggled")
      });

      $(".dataTables_filter input[type=search]").blur(function() {
        $(this).closest(".dataTables_filter").removeClass("dataTables_filter--toggled")
      });

      $("body").on("click", "[data-table-action]", function(a) {
        a.preventDefault();
        var b = $(this).data("table-action");
        if ("reload" === b) {
          $("#" + _table_rchb_ekstra_kurikuler).DataTable().ajax.reload(null, false);
        };
      });
    };

    // Initialize DataTables: Prestasi
    if ($("#" + _table_rchb_prestasi)[0]) {
      var table_rchb_prestasi = $("#" + _table_rchb_prestasi).DataTable({
        processing: true,
        serverSide: true,
        ajax: {
          url: "<?php echo base_url('raport_capaianhasilbelajar/ajax_get_all/') ?>",
          type: "get"
        },
        columns: [{
            data: "kelas_concated"
          }, {
            data: null,
            render: function(data, type, row, meta) {
              return meta.row + meta.settings._iDisplayStart + 1;
            }
          },
          {
            data: "nisn"
          },
          {
            data: "nomor_induk_lokal"
          },
          {
            data: "nama_lengkap"
          },
          {
            data: null,
            render: function(data, type, row, meta) {
              var status = sample(['Sudah Input', 'Belum Input']);
              var statusClass = (status === 'Sudah Input') ? 'text-green' : 'text-red';
              return `<span class="${statusClass}">${status}</span>`;
            }
          },
          {
            data: null,
            className: "center",
            render: function(data, type, row, meta) {
              var buttons = '<div class="action">';
              buttons += '<a href="javascript:;" class="btn btn-sm btn-light btn-table-action action-input" data-toggle="modal" data-target="#' + _modal_rchb_prestasi + '"><i class="zmdi zmdi-edit"></i> Input</a>';
              buttons += '</div>';
              return buttons;
            },
          }
        ],
        autoWidth: !1,
        responsive: {
          details: {
            renderer: function(api, rowIdx, columns) {
              var hideColumn = [9];
              var data = $.map(columns, function(col, i) {
                return ($.inArray(col.columnIndex, hideColumn)) ?
                  '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                  '<td class="dt-details-td">' + col.title + ':' + '</td> ' +
                  '<td class="dt-details-td">' + col.data + '</td>' +
                  '</tr>' :
                  '';
              }).join('');

              return data ? $('<table/>').append(data) : false;
            },
            type: "inline",
            target: 'tr',
          }
        },
        columnDefs: [{
          "targets": [0],
          "visible": false
        }, {
          className: 'desktop',
          targets: [0, 1, 2, 3, 4, 5, 6]
        }, {
          className: 'tablet',
          targets: [0, 1, 2, 3]
        }, {
          className: 'mobile',
          targets: [0, 3]
        }, {
          responsivePriority: 2,
          targets: -1
        }],
        pageLength: 100,
        language: {
          searchPlaceholder: "Search...",
          sProcessing: '<div style="text-align: center;"><div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div></div>'
        },
        sDom: '<"dataTables_ct"><"dataTables__top"fb>rt<"dataTables__bottom"ip><"clear">',
        buttons: [{
          extend: "excelHtml5",
          title: "Export Result"
        }, {
          extend: "print",
          title: "Export Result"
        }],
        initComplete: function(a, b) {
          $(this).closest(".dataTables_wrapper").find(".dataTables__top").prepend(
            '<div class="dataTables_buttons hidden-sm-down actions">' +
            '<span class="actions__item zmdi zmdi-refresh" data-table-action="reload" title="Reload" />' +
            '</div>'
          );
        },
      });

      $(".dataTables_filter input[type=search]").focus(function() {
        $(this).closest(".dataTables_filter").addClass("dataTables_filter--toggled")
      });

      $(".dataTables_filter input[type=search]").blur(function() {
        $(this).closest(".dataTables_filter").removeClass("dataTables_filter--toggled")
      });

      $("body").on("click", "[data-table-action]", function(a) {
        a.preventDefault();
        var b = $(this).data("table-action");
        if ("reload" === b) {
          $("#" + _table_rchb_prestasi).DataTable().ajax.reload(null, false);
        };
      });
    };

    // Initialize DataTables: Ketidakhadiran
    if ($("#" + _table_rchb_ketidakhadiran)[0]) {
      var table_rchb_ketidakhadiran = $("#" + _table_rchb_ketidakhadiran).DataTable({
        processing: true,
        serverSide: true,
        ajax: {
          url: "<?php echo base_url('raport_capaianhasilbelajar/ajax_get_all/') ?>",
          type: "get"
        },
        columns: [{
            data: "kelas_concated"
          }, {
            data: null,
            render: function(data, type, row, meta) {
              return meta.row + meta.settings._iDisplayStart + 1;
            }
          },
          {
            data: "nisn"
          },
          {
            data: "nomor_induk_lokal"
          },
          {
            data: "nama_lengkap"
          },
          {
            data: null,
            render: function(data, type, row, meta) {
              var status = sample(['Sudah Input', 'Belum Input']);
              var statusClass = (status === 'Sudah Input') ? 'text-green' : 'text-red';
              return `<span class="${statusClass}">${status}</span>`;
            }
          },
          {
            data: null,
            className: "center",
            render: function(data, type, row, meta) {
              var buttons = '<div class="action">';
              buttons += '<a href="javascript:;" class="btn btn-sm btn-light btn-table-action action-input" data-toggle="modal" data-target="#' + _modal_rchb_ketidakhadiran + '"><i class="zmdi zmdi-edit"></i> Input</a>';
              buttons += '</div>';
              return buttons;
            },
          }
        ],
        autoWidth: !1,
        responsive: {
          details: {
            renderer: function(api, rowIdx, columns) {
              var hideColumn = [9];
              var data = $.map(columns, function(col, i) {
                return ($.inArray(col.columnIndex, hideColumn)) ?
                  '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                  '<td class="dt-details-td">' + col.title + ':' + '</td> ' +
                  '<td class="dt-details-td">' + col.data + '</td>' +
                  '</tr>' :
                  '';
              }).join('');

              return data ? $('<table/>').append(data) : false;
            },
            type: "inline",
            target: 'tr',
          }
        },
        columnDefs: [{
          "targets": [0],
          "visible": false
        }, {
          className: 'desktop',
          targets: [0, 1, 2, 3, 4, 5, 6]
        }, {
          className: 'tablet',
          targets: [0, 1, 2, 3]
        }, {
          className: 'mobile',
          targets: [0, 3]
        }, {
          responsivePriority: 2,
          targets: -1
        }],
        pageLength: 100,
        language: {
          searchPlaceholder: "Search...",
          sProcessing: '<div style="text-align: center;"><div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div></div>'
        },
        sDom: '<"dataTables_ct"><"dataTables__top"fb>rt<"dataTables__bottom"ip><"clear">',
        buttons: [{
          extend: "excelHtml5",
          title: "Export Result"
        }, {
          extend: "print",
          title: "Export Result"
        }],
        initComplete: function(a, b) {
          $(this).closest(".dataTables_wrapper").find(".dataTables__top").prepend(
            '<div class="dataTables_buttons hidden-sm-down actions">' +
            '<span class="actions__item zmdi zmdi-refresh" data-table-action="reload" title="Reload" />' +
            '</div>'
          );
        },
      });

      $(".dataTables_filter input[type=search]").focus(function() {
        $(this).closest(".dataTables_filter").addClass("dataTables_filter--toggled")
      });

      $(".dataTables_filter input[type=search]").blur(function() {
        $(this).closest(".dataTables_filter").removeClass("dataTables_filter--toggled")
      });

      $("body").on("click", "[data-table-action]", function(a) {
        a.preventDefault();
        var b = $(this).data("table-action");
        if ("reload" === b) {
          $("#" + _table_rchb_ketidakhadiran).DataTable().ajax.reload(null, false);
        };
      });
    };

    // Initialize DataTables: Catatan Wali Kelas
    if ($("#" + _table_rchb_catatan_wali_kelas)[0]) {
      var table_rchb_catatan_wali_kelas = $("#" + _table_rchb_catatan_wali_kelas).DataTable({
        processing: true,
        serverSide: true,
        ajax: {
          url: "<?php echo base_url('raport_capaianhasilbelajar/ajax_get_all/') ?>",
          type: "get"
        },
        columns: [{
            data: "kelas_concated"
          }, {
            data: null,
            render: function(data, type, row, meta) {
              return meta.row + meta.settings._iDisplayStart + 1;
            }
          },
          {
            data: "nisn"
          },
          {
            data: "nomor_induk_lokal"
          },
          {
            data: "nama_lengkap"
          },
          {
            data: null,
            render: function(data, type, row, meta) {
              var status = sample(['Sudah Input', 'Belum Input']);
              var statusClass = (status === 'Sudah Input') ? 'text-green' : 'text-red';
              return `<span class="${statusClass}">${status}</span>`;
            }
          },
          {
            data: null,
            className: "center",
            render: function(data, type, row, meta) {
              var buttons = '<div class="action">';
              buttons += '<a href="javascript:;" class="btn btn-sm btn-light btn-table-action action-input" data-toggle="modal" data-target="#' + _modal_rchb_catatan_wali_kelas + '"><i class="zmdi zmdi-edit"></i> Input</a>';
              buttons += '</div>';
              return buttons;
            },
          }
        ],
        autoWidth: !1,
        responsive: {
          details: {
            renderer: function(api, rowIdx, columns) {
              var hideColumn = [9];
              var data = $.map(columns, function(col, i) {
                return ($.inArray(col.columnIndex, hideColumn)) ?
                  '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                  '<td class="dt-details-td">' + col.title + ':' + '</td> ' +
                  '<td class="dt-details-td">' + col.data + '</td>' +
                  '</tr>' :
                  '';
              }).join('');

              return data ? $('<table/>').append(data) : false;
            },
            type: "inline",
            target: 'tr',
          }
        },
        columnDefs: [{
          "targets": [0],
          "visible": false
        }, {
          className: 'desktop',
          targets: [0, 1, 2, 3, 4, 5, 6]
        }, {
          className: 'tablet',
          targets: [0, 1, 2, 3]
        }, {
          className: 'mobile',
          targets: [0, 3]
        }, {
          responsivePriority: 2,
          targets: -1
        }],
        pageLength: 100,
        language: {
          searchPlaceholder: "Search...",
          sProcessing: '<div style="text-align: center;"><div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div></div>'
        },
        sDom: '<"dataTables_ct"><"dataTables__top"fb>rt<"dataTables__bottom"ip><"clear">',
        buttons: [{
          extend: "excelHtml5",
          title: "Export Result"
        }, {
          extend: "print",
          title: "Export Result"
        }],
        initComplete: function(a, b) {
          $(this).closest(".dataTables_wrapper").find(".dataTables__top").prepend(
            '<div class="dataTables_buttons hidden-sm-down actions">' +
            '<span class="actions__item zmdi zmdi-refresh" data-table-action="reload" title="Reload" />' +
            '</div>'
          );
        },
      });

      $(".dataTables_filter input[type=search]").focus(function() {
        $(this).closest(".dataTables_filter").addClass("dataTables_filter--toggled")
      });

      $(".dataTables_filter input[type=search]").blur(function() {
        $(this).closest(".dataTables_filter").removeClass("dataTables_filter--toggled")
      });

      $("body").on("click", "[data-table-action]", function(a) {
        a.preventDefault();
        var b = $(this).data("table-action");
        if ("reload" === b) {
          $("#" + _table_rchb_catatan_wali_kelas).DataTable().ajax.reload(null, false);
        };
      });
    };

    // Handle Submit Filter
    $(document).on("change", ".raport_capaianhasilbelajar-filter-kelas", function() {
      var filter_kelas = $("#" + _section + " .raport_capaianhasilbelajar-filter-kelas").val();
      filter_kelas = (filter_kelas == null || filter_kelas == "null") ? "--empty--" : filter_kelas;

      // Set kelas params for form
      $(".p_kelas").val(filter_kelas).trigger("input");

      switch (_tabIndex) {
        case 0:
          table_rchb_sikap.column(0).search(filter_kelas).draw();
          break;
        case 1:
          table_rchb_pengetahuan_keterampilan.column(0).search(filter_kelas).draw();
          break;
        case 2:
          table_rchb_ekstra_kurikuler.column(0).search(filter_kelas).draw();
          break;
        case 3:
          table_rchb_prestasi.column(0).search(filter_kelas).draw();
          break;
        case 4:
          table_rchb_ketidakhadiran.column(0).search(filter_kelas).draw();
          break;
        case 5:
          table_rchb_catatan_wali_kelas.column(0).search(filter_kelas).draw();
          break;
        default:
          break;
      };
    });

    // Handle Submit: Sikap
    $(document).on("click", ".action-rchb-sikap-save", function(e) {
      e.preventDefault();
      var button = $(".action-rchb-sikap-save");
      var buttonText = button.html();

      $.ajax({
        type: "post",
        url: "<?php echo base_url('raport_capaianhasilbelajar/ajax_save_sikap/') ?>",
        data: $("#" + _form_rchb_sikap).serialize(),
        beforeSend: function() {
          button.html("Saving data...").prop("disabled", true);
        },
        success: function(response) {
          try {
            var response = JSON.parse(response);
            if (response.status === true) {
              notify(response.data, "success");
            } else {
              notify(response.data, "danger");
            };
            button.html(buttonText).prop("disabled", false);
          } catch (error) {
            notify("Terjadi kesalahan ketika menyimpan data #1", "danger");
          };
        },
        error: function() {
          notify("Terjadi kesalahan ketika menyimpan data #2", "danger");
        },
        complete: function() {
          button.html(buttonText).prop("disabled", false);
        }
      });
    });

    // Handle Input: Pengetahuan & Keterampilan
    $("#" + _table_rchb_pengetahuan_keterampilan).on("click", "a.action-input", function(e) {
      e.preventDefault();
      reset_form_rchb_pengetahuan_keterampilan();

      try {
        var temp = table_rchb_pengetahuan_keterampilan.row($(this).closest('tr')).data();

        $(`#${_form_rchb_pengetahuan_keterampilan} .nisn`).html(temp.nisn);
        $(`#${_form_rchb_pengetahuan_keterampilan} .nama_lengkap`).html(temp.nama_lengkap);

        // Fetch value nilai
        $.ajax({
          url: "<?= base_url('raport_capaianhasilbelajar/ajax_get_pengetahuan_keterampilan_value') ?>",
          type: "get",
          data: {
            santri_id: "xxxx"
          },
          dataType: "json",
          success: function(response) {
            if (response.items.length > 0) {
              response.items.map((value, index) => {
                if ($("[name='keterampilan[" + value.mapel_id + "][angka]']").length > 0) {
                  $("[name='pengetahuan[" + value.mapel_id + "][angka]']").val(value.pengetahuan_angka);
                  $("[name='pengetahuan[" + value.mapel_id + "][predikat]']").val(value.pengetahuan_predikat);
                  $("[name='keterampilan[" + value.mapel_id + "][angka]']").val(value.keterampilan_angka);
                  $("[name='keterampilan[" + value.mapel_id + "][predikat]']").val(value.keterampilan_predikat);
                }
              });

              // Fetch value catatan
              $.ajax({
                url: "<?= base_url('raport_capaianhasilbelajar/ajax_get_pengetahuan_keterampilan_note_value') ?>",
                type: "get",
                data: {
                  santri_id: "xxxx"
                },
                dataType: "json",
                success: function(response) {
                  if (response.items.length > 0) {
                    response.items.map((value, index) => {
                      if ($("[name='pengetahuan_note[" + value.mapel_id + "][pengetahuan_catatan]']").length > 0) {
                        $("[name='pengetahuan_note[" + value.mapel_id + "][pengetahuan_catatan]']").val(value.pengetahuan_catatan);
                        $("[name='keterampilan_note[" + value.mapel_id + "][keterampilan_catatan]']").val(value.keterampilan_catatan);
                      }
                    });
                  };
                }
              });
            };
          }
        });
      } catch (error) {
        handleTransErrorMsg();
      };
    });

    // Handle Input: Ekstra Kurikuler
    $("#" + _table_rchb_ekstra_kurikuler).on("click", "a.action-input", function(e) {
      e.preventDefault();
      reset_form_rchb_ekstra_kurikuler();

      try {
        var temp = table_rchb_ekstra_kurikuler.row($(this).closest('tr')).data();

        $(`#${_form_rchb_ekstra_kurikuler} .nisn`).html(temp.nisn);
        $(`#${_form_rchb_ekstra_kurikuler} .nama_lengkap`).html(temp.nama_lengkap);

        // Fetch value
        // $.ajax({
        //   url: "<?= base_url('raport_capaianhasilbelajar/ajax_get_ekstra_kurikuler_value') ?>",
        //   type: "get",
        //   data: {
        //     santri_id: "xxxx"
        //   },
        //   dataType: "json",
        //   success: function(response) {
        //     if (response.items.length > 0) {
        //       response.items.map((value, index) => {
        //         if ($("[name='keterampilan[" + value.mapel_id + "][angka]']").length > 0) {
        //           $("[name='pengetahuan[" + value.mapel_id + "][angka]']").val(value.pengetahuan_angka);
        //         }
        //       });
        //     };
        //   }
        // });
      } catch (error) {
        handleTransErrorMsg();
      };
    });

    // Handle Input: Prestasi
    $("#" + _table_rchb_prestasi).on("click", "a.action-input", function(e) {
      e.preventDefault();
      reset_form_rchb_prestasi();

      try {
        var temp = table_rchb_prestasi.row($(this).closest('tr')).data();

        $(`#${_form_rchb_prestasi} .nisn`).html(temp.nisn);
        $(`#${_form_rchb_prestasi} .nama_lengkap`).html(temp.nama_lengkap);

        // Fetch value
        // $.ajax({
        //   url: "<?= base_url('raport_capaianhasilbelajar/ajax_get_prestasi_value') ?>",
        //   type: "get",
        //   data: {
        //     santri_id: "xxxx"
        //   },
        //   dataType: "json",
        //   success: function(response) {
        //     if (response.items.length > 0) {
        //       response.items.map((value, index) => {
        //         if ($("[name='keterampilan[" + value.mapel_id + "][angka]']").length > 0) {
        //           $("[name='pengetahuan[" + value.mapel_id + "][angka]']").val(value.pengetahuan_angka);
        //         }
        //       });
        //     };
        //   }
        // });
      } catch (error) {
        handleTransErrorMsg();
      };
    });

    // Handle Input: Ketidakhadiran
    $("#" + _table_rchb_ketidakhadiran).on("click", "a.action-input", function(e) {
      e.preventDefault();
      reset_form_rchb_ketidakhadiran();

      try {
        var temp = table_rchb_ketidakhadiran.row($(this).closest('tr')).data();

        $(`#${_form_rchb_ketidakhadiran} .nisn`).html(temp.nisn);
        $(`#${_form_rchb_ketidakhadiran} .nama_lengkap`).html(temp.nama_lengkap);

        // Fetch value
        // $.ajax({
        //   url: "<?= base_url('raport_capaianhasilbelajar/ajax_get_ketidakhadiran_value') ?>",
        //   type: "get",
        //   data: {
        //     santri_id: "xxxx"
        //   },
        //   dataType: "json",
        //   success: function(response) {
        //     if (response.items.length > 0) {
        //       response.items.map((value, index) => {
        //         if ($("[name='keterampilan[" + value.mapel_id + "][angka]']").length > 0) {
        //           $("[name='pengetahuan[" + value.mapel_id + "][angka]']").val(value.pengetahuan_angka);
        //         }
        //       });
        //     };
        //   }
        // });
      } catch (error) {
        handleTransErrorMsg();
      };
    });

    // Handle Input: Catatan Wali Kelas
    $("#" + _table_rchb_catatan_wali_kelas).on("click", "a.action-input", function(e) {
      e.preventDefault();
      reset_form_rchb_catatan_wali_kelas();

      try {
        var temp = table_rchb_catatan_wali_kelas.row($(this).closest('tr')).data();

        $(`#${_form_rchb_catatan_wali_kelas} .nisn`).html(temp.nisn);
        $(`#${_form_rchb_catatan_wali_kelas} .nama_lengkap`).html(temp.nama_lengkap);

        // Fetch value
        // $.ajax({
        //   url: "<?= base_url('raport_capaianhasilbelajar/ajax_get_catatan_wali_kelas_value') ?>",
        //   type: "get",
        //   data: {
        //     santri_id: "xxxx"
        //   },
        //   dataType: "json",
        //   success: function(response) {
        //     if (response.items.length > 0) {
        //       response.items.map((value, index) => {
        //         if ($("[name='keterampilan[" + value.mapel_id + "][angka]']").length > 0) {
        //           $("[name='pengetahuan[" + value.mapel_id + "][angka]']").val(value.pengetahuan_angka);
        //         }
        //       });
        //     };
        //   }
        // });
      } catch (error) {
        handleTransErrorMsg();
      };
    });

    // Handle form reset: Pengetahuan & Keterampilan
    reset_form_rchb_pengetahuan_keterampilan = () => {
      $(`#${_form_rchb_pengetahuan_keterampilan}`).trigger("reset");
      $(`#${_form_rchb_pengetahuan_keterampilan} .nisn`).html("-");
      $(`#${_form_rchb_pengetahuan_keterampilan} .nama_lengkap`).html("-");
    };

    // Handle form reset: Ekstra Kurikuler
    reset_form_rchb_ekstra_kurikuler = () => {
      $(`#${_form_rchb_ekstra_kurikuler}`).trigger("reset");
      $(`#${_form_rchb_ekstra_kurikuler} .nisn`).html("-");
      $(`#${_form_rchb_ekstra_kurikuler} .nama_lengkap`).html("-");
    };

    // Handle form reset: Prestasi
    reset_form_rchb_prestasi = () => {
      $(`#${_form_rchb_prestasi}`).trigger("reset");
      $(`#${_form_rchb_prestasi} .nisn`).html("-");
      $(`#${_form_rchb_prestasi} .nama_lengkap`).html("-");
    };

    // Handle form reset: Ketidakhadiran
    reset_form_rchb_ketidakhadiran = () => {
      $(`#${_form_rchb_ketidakhadiran}`).trigger("reset");
      $(`#${_form_rchb_ketidakhadiran} .nisn`).html("-");
      $(`#${_form_rchb_ketidakhadiran} .nama_lengkap`).html("-");
    };

    // Handle form reset: Catatan Wali Kelas
    reset_form_rchb_catatan_wali_kelas = () => {
      $(`#${_form_rchb_catatan_wali_kelas}`).trigger("reset");
      $(`#${_form_rchb_catatan_wali_kelas} .nisn`).html("-");
      $(`#${_form_rchb_catatan_wali_kelas} .nama_lengkap`).html("-");
    };

    setTabState = (tabIndex) => {
      var _tabIndexPrev = _tabIndex;
      _tabIndex = tabIndex;

      if (tabIndex != _tabIndexPrev) {
        $(".raport_capaianhasilbelajar-filter-kelas").trigger("change");
      };
    };

    // Fetch sikap value
    fetch_sikap_value = () => {
      setTimeout(function() {
        var filter_kelas = $("#" + _section + " .raport_capaianhasilbelajar-filter-kelas").val();
        filter_kelas = (filter_kelas == null || filter_kelas == "null") ? "--empty--" : filter_kelas;

        $.ajax({
          url: "<?= base_url('raport_capaianhasilbelajar/ajax_get_sikap_value') ?>",
          type: "get",
          data: {
            semester: _semester,
            tahun_ajar: _tahun_ajar,
            kelas: filter_kelas
          },
          dataType: "json",
          beforeSend: function() {
            $("#" + _table_rchb_sikap + "_processing").show();
          },
          success: function(response) {
            if (response.length > 0) {
              $.each(response, function(key, item) {
                $.each(item, function(col, val) {
                  $(`[name='sikap[${item.santri_id}][${col}]']`).val(val).trigger("input").trigger("change");
                });
              });
            };
          },
          complete: function() {
            $("#" + _table_rchb_sikap + "_processing").hide();
          },
        });
      }, 500);
    };

  });

  function sample(array) {
    return array[Math.floor(Math.random() * array.length)];
  };
</script>