<div class="table-responsive">
    <table class="table table-bordered table-hover table-sm">
        <thead class="thead-default text-center">
            <tr>
                <th rowspan="4" colspan="2">MATA PELAJARAN</th>
                <th rowspan="4" width="80">KKM</th>
                <th colspan="2">Pengetahuan</th>
                <th colspan="2">Keterampilan</th>
            </tr>
            <tr>
                <th colspan="2">( KI-3 ) </th>
                <th colspan="2">( KI-4 ) </th>
            </tr>
            <tr>
                <!-- Pengetahuan -->
                <th>Angka</th>
                <th>Predikat</th>
                <!-- Keterampilan -->
                <th>Angka</th>
                <th>Predikat</th>
            </tr>
            <tr>
                <!-- Pengetahuan -->
                <th>0 - 100</th>
                <th>D / C / B / A</th>
                <!-- Keterampilan -->
                <th>0 - 100</th>
                <th>D / C / B / A</th>
            </tr>
        </thead>
        <tbody>
            <?php if (count($pengetahuan_keterampilan) > 0) : ?>
                <?php foreach ($pengetahuan_keterampilan as $index_kelompok => $kelompok) : ?>
                    <!-- Kelompok -->
                    <tr style="background: #f8f8f8;">
                        <td colspan="7"><b><?= $kelompok['kelompok_nama'] ?></b></td>
                    </tr>
                    <!-- END ## Kelompok -->
                    <?php if (count($kelompok['items']) > 0) : ?>
                        <?php $no = 1; ?>
                        <?php foreach ($kelompok['items'] as $index_mapel => $mapel) : ?>
                            <!-- Level 1 -->
                            <tr>
                                <td valign="top" align="center"><?= $no++ ?></td>
                                <?php if ($mapel['is_editable'] === 1) : ?>
                                    <td valign="top"><?= $mapel['mapel_nama'] ?></td>
                                    <td valign="top" align="center"><?= $mapel['kkm'] ?></td>
                                    <td valign="top">
                                        <input type="number" name="pengetahuan[<?= $mapel['mapel_id'] ?>][angka]" min="0" max="100" class="form-control bg-white" style="text-align: center; height: 26px;" placeholder="0" />
                                    </td>
                                    <td valign="top">
                                        <select name="pengetahuan[<?= $mapel['mapel_id'] ?>][predikat]" class="custom-select p-0 bg-white" style="text-align: center; height: 26px;">
                                            <option disabled selected>Select &#8595;</option>
                                            <option value="A">A</option>
                                            <option value="B">B</option>
                                            <option value="C">C</option>
                                            <option value="D">D</option>
                                        </select>
                                    </td>
                                    <td valign="top">
                                        <input type="number" name="keterampilan[<?= $mapel['mapel_id'] ?>][angka]" min="0" max="100" class="form-control bg-white" style="text-align: center; height: 26px;" placeholder="0" />
                                    </td>
                                    <td valign="top">
                                        <select name="keterampilan[<?= $mapel['mapel_id'] ?>][predikat]" class="custom-select p-0 bg-white" style="text-align: center; height: 26px;">
                                            <option disabled selected>Select &#8595;</option>
                                            <option value="A">A</option>
                                            <option value="B">B</option>
                                            <option value="C">C</option>
                                            <option value="D">D</option>
                                        </select>
                                    </td>
                                <?php else : ?>
                                    <td valign="top" colspan="6"><?= $mapel['mapel_nama'] ?></td>
                                <?php endif ?>
                            </tr>
                            <!-- END ## Level 1 -->
                            <?php if (count($mapel['items']) > 0) : ?>
                                <?php foreach ($mapel['items'] as $index_mapel2 => $mapel2) : ?>
                                    <!-- Level 2 -->
                                    <tr>
                                        <td>&nbsp;</td>
                                        <?php if ($mapel2['is_editable'] === 1) : ?>
                                            <td valign="top"><?= $mapel2['mapel_nama'] ?></td>
                                            <td valign="top" align="center"><?= $mapel2['kkm'] ?></td>
                                            <td valign="top">
                                                <input type="number" name="pengetahuan[<?= $mapel2['mapel_id'] ?>][angka]" min="0" max="100" class="form-control bg-white" style="text-align: center; height: 26px;" placeholder="0" />
                                            </td>
                                            <td valign="top">
                                                <select name="pengetahuan[<?= $mapel2['mapel_id'] ?>][predikat]" class="custom-select p-0 bg-white" style="text-align: center; height: 26px;">
                                                    <option disabled selected>Select &#8595;</option>
                                                    <option value="A">A</option>
                                                    <option value="B">B</option>
                                                    <option value="C">C</option>
                                                    <option value="D">D</option>
                                                </select>
                                            </td>
                                            <td valign="top">
                                                <input type="number" name="keterampilan[<?= $mapel2['mapel_id'] ?>][angka]" min="0" max="100" class="form-control bg-white" style="text-align: center; height: 26px;" placeholder="0" />
                                            </td>
                                            <td valign="top">
                                                <select name="keterampilan[<?= $mapel2['mapel_id'] ?>][predikat]" class="custom-select p-0 bg-white" style="text-align: center; height: 26px;">
                                                    <option disabled selected>Select &#8595;</option>
                                                    <option value="A">A</option>
                                                    <option value="B">B</option>
                                                    <option value="C">C</option>
                                                    <option value="D">D</option>
                                                </select>
                                            </td>
                                        <?php else : ?>
                                            <td valign="top" colspan="6"><?= $mapel2['mapel_nama'] ?></td>
                                        <?php endif ?>
                                    </tr>
                                    <!-- END ## Level 2 -->
                                <?php endforeach ?>
                            <?php endif ?>
                        <?php endforeach ?>
                    <?php endif ?>
                <?php endforeach ?>
            <?php else : ?>
                <tr>
                    <td colspan="7">No data available in table</td>
                </tr>
            <?php endif ?>
        </tbody>
    </table>
</div>