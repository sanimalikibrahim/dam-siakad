<style type="text/css">
  .buku-grid {
    border: 1px solid #ddd;
    padding: 15px;
    margin-bottom: 2rem;
  }

  .buku-grid .dataTables_wrapper {
    margin: 0;
  }

  .buku-grid .dataTables_wrapper .table {
    margin: 0;
  }

  .buku-grid .table-responsive {
    border: 1px solid #ddd;
    padding: 8px;
  }

  .buku-grid .dataTables_paginate {
    margin-top: 8px;
  }

  .buku-grid-table {
    margin-top: 1rem;
  }
</style>

<div class="modal fade" id="modal-form-transaksibuku" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-dialog-centered modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title pull-left">Peminjaman Buku</h5>
      </div>
      <div class="spinner">
        <div class="lds-hourglass"></div>
      </div>
      <div class="modal-body">
        <form id="form-transaksibuku" autocomplete="off">
          <!-- CSRF -->
          <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />

          <div class="form-group">
            <label required>Peminjam</label>
            <div class="select">
              <select name="user_id" class="form-control select2 transaksibuku-user_id" data-placeholder="Select &#8595;" required>
                <?= $list_user ?>
              </select>
              <i class="form-group__bar"></i>
            </div>
          </div>

          <div class="row">
            <div class="col">
              <div class="form-group">
                <label required>Tanggal Pinjam</label>
                <input type="text" name="tanggal_pinjam" class="form-control transaksibuku-tanggal_pinjam" placeholder="Tanggal Pinjam" value="<?= date('Y-m-d') ?>" required readonly />
                <i class="form-group__bar"></i>
              </div>
            </div>
            <div class="col">
              <div class="form-group">
                <label required>Tanggal Kembali</label>
                <input type="text" name="tanggal_kembali" class="form-control flatpickr-date transaksibuku-tanggal_kembali" placeholder="Tanggal Kembali" value="<?= date('Y-m-d', strtotime('+7 day', strtotime(date('Y-m-d')))) ?>" required />
                <i class="form-group__bar"></i>
              </div>
            </div>
          </div>

          <div class="form-group">
            <label>Keterangan</label>
            <textarea name="keterangan_pinjam" class="form-control transaksibuku-keterangan_pinjam" placeholder="Keterangan"></textarea>
            <i class="form-group__bar"></i>
          </div>

          <label required>Buku</label>
          <div class="buku-grid">
            <?php if (!$app->is_mobile) : ?>
              <!-- Desktop -->
              <div class="input-group mb-0">
                <div class="input-group-prepend">
                  <label class="input-group-text" style="height: 34.13px; background: #f2f2f2;">Cari Buku</label>
                </div>
                <input type="text" name="search_keyword" class="form-control transaksibuku-search_keyword" placeholder="Masukan informasi..." />
                <i class="form-group__bar"></i>
                <div class="input-group-apend">
                  <a href="javascript:;" class="btn btn-secondary transaksibuku-search_submit" style="height: 34.13px;">
                    <i class="zmdi zmdi-search"></i>
                  </a>
                  <a href="javascript:;" class="btn btn-primary transaksibuku-scan_barcode" data-toggle="modal" data-target="#modal-scanbarcode" style="height: 34.13px;">
                    <i class="zmdi zmdi-center-focus-strong"></i> Scan Barcode
                  </a>
                </div>
              </div>
            <?php else : ?>
              <!-- Mobile -->
              <div class="input-group mb-2">
                <input type="text" name="search_keyword" class="form-control no-padding-l transaksibuku-search_keyword" placeholder="Masukan informasi..." />
                <i class="form-group__bar"></i>
                <div class="input-group-apend">
                  <a href="javascript:;" class="btn btn-secondary transaksibuku-search_submit" style="height: 34.13px;">
                    <i class="zmdi zmdi-search"></i>
                  </a>
                </div>
              </div>
              <div class="form-group mb-0">
                <a href="javascript:;" class="btn btn-primary btn-block transaksibuku-scan_barcode" data-toggle="modal" data-target="#modal-scanbarcode" style="height: 34.13px;">
                  <i class="zmdi zmdi-center-focus-strong"></i> Scan Barcode
                </a>
              </div>
            <?php endif; ?>
            <div class="buku-grid-table table-responsive">
              <table id="table-buku" class="table table-bordered" style="margin-bottom: 0; width: 100%;">
                <thead class="thead-default">
                  <tr>
                    <th width="100">No</th>
                    <th>Kategori</th>
                    <th>Kode</th>
                    <th>Rak</th>
                    <th>Judul</th>
                    <th>Tahun</th>
                    <th width="100" style="text-align: center;">Action</th>
                  </tr>
                </thead>
              </table>
            </div>
            <div class="buku-grid-table text-center">
              <i class="zmdi zmdi-long-arrow-down"></i>
            </div>
            <div class="buku-grid-table table-responsive">
              <table id="table-buku-assigned" class="table table-bordered" style="margin-bottom: 0; width: 100%;">
                <thead class="thead-default">
                  <tr>
                    <th width="100">No</th>
                    <th>Kategori</th>
                    <th>Kode</th>
                    <th>Rak</th>
                    <th>Judul</th>
                    <th>Tahun</th>
                    <th width="100" style="text-align: center;">Action</th>
                  </tr>
                </thead>
              </table>
            </div>
            <div class="hidden-payload"></div>
          </div>

          <small class="form-text text-muted">
            Fields with red stars (<label required></label>) are required.
          </small>

        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success btn--icon-text transaksibuku-action-save">
          <i class="zmdi zmdi-save"></i> Save
        </button>
        <button type="button" class="btn btn-light btn--icon-text transaksibuku-action-cancel" data-dismiss="modal">
          Cancel
        </button>
      </div>
    </div>
  </div>
</div>