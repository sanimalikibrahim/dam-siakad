<div class="modal fade" id="modal-scanbarcode" data-backdrop="static" data-keyboard="false" style="z-index: 99999 !important;">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title pull-left">Scan Barcode</h5>
      </div>
      <div class="spinner">
        <div class="lds-hourglass"></div>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <label>Source</label>
          <div class="select">
            <select id="cam-source" class="form-control"></select>
            <i class="form-group__bar"></i>
          </div>
        </div>
        <div id="cam-error" style="color: #ff6b68; text-align: center;"></div>
        <canvas id="cam-canvas" style="width: 100%;"></canvas>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-light btn--icon-text scanbarcode-action-close" data-dismiss="modal">
          Close
        </button>
      </div>
    </div>
  </div>
</div>