<style type="text/css">
  .table-detail {
    width: 100%;
  }

  .table-detail tr th,
  td {
    border-bottom: 1px solid #ddd;
    padding: 4px 0px;
  }

  .buku-grid-picked .dataTables_paginate {
    margin-top: 8px;
  }
</style>

<div class="modal fade" id="modal-detail-transaksibuku" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-dialog-centered modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title pull-left">Peminjaman Buku</h5>
      </div>
      <div class="spinner">
        <div class="lds-hourglass"></div>
      </div>
      <div class="modal-body">
        <table class="table-detail">
          <tr>
            <th valign="top" width="130">Kode :</th>
            <td valign="top"><span class="detail-item-kode">-</span></td>
          </tr>
          <tr>
            <th valign="top" width="130">Peminjam :</th>
            <td valign="top"><span class="detail-item-peminjam">-</span></td>
          </tr>
          <tr>
            <th valign="top" width="130">Tanggal Pinjam :</th>
            <td valign="top"><span class="detail-item-tanggal_pinjam">-</span></td>
          </tr>
          <tr>
            <th valign="top" width="130">Tanggal Kembali :</th>
            <td valign="top"><span class="detail-item-tanggal_kembali">-</span></td>
          </tr>
          <tr>
            <th valign="top" width="130">Jumlah Buku :</th>
            <td valign="top">
              Dipinjam : <span class="detail-item-jumlah_dipinjam">-</span> <br />
              Belum Dikembalikan : <span class="detail-item-jumlah_belum_dikembalikan">-</span> <br />
              Dikembalikan : <span class="detail-item-jumlah_dikembalikan">-</span>
            </td>
          </tr>
          <tr>
            <th valign="top" width="130">Keterangan :</th>
            <td valign="top">
              <div class="detail-item-keterangan">-</div>
            </td>
          </tr>
        </table>

        <div class="buku-grid-picked table-responsive">
          <table id="table-buku-picked" class="table table-bordered" style="margin-bottom: 0; width: 100%;">
            <thead class="thead-default">
              <tr>
                <th width="100">No</th>
                <th>Kode</th>
                <th>Judul</th>
                <th>Dikembalikan</th>
                <th>Status</th>
                <th width="100" style="text-align: center;">Action</th>
              </tr>
            </thead>
          </table>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-light btn--icon-text transaksibuku-action-cancel" data-dismiss="modal">
          Close
        </button>
      </div>
    </div>
  </div>
</div>