<style type="text/css">
    .modal {
        overflow-y: auto;
    }
</style>

<section id="transaksibuku">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title"><?php echo (isset($card_title)) ? $card_title : '' ?></h4>
            <h6 class="card-subtitle"><?php echo (isset($card_subTitle)) ? $card_subTitle : '' ?></h6>

            <div class="table-action row">
                <div class="buttons <?= (!$app->is_mobile) ? 'col-auto' : 'col-12' ?> no-padding-r buttons-action-add">
                    <button class="btn btn--raised btn-primary btn--icon-text transaksibuku-action-add" data-toggle="modal" data-target="#modal-form-transaksibuku">
                        <i class="zmdi zmdi-plus-circle"></i> Add New
                    </button>
                </div>
                <!-- Filter on desktop -->
                <div class="buttons col buttons-action-filter">
                    <div class="input-group mb-0">
                        <?php if (!$app->is_mobile) : ?>
                            <div class="input-group-prepend">
                                <label class="input-group-text" style="height: 34.13px; background: #f2f2f2;">Tanggal Pinjam</label>
                            </div>
                        <?php endif; ?>
                        <input type="text" class="custom-select filter-tanggal_pinjam flatpickr-datemax-today" placeholder="Select" style="height: 34.13px; max-width: 150px;" value="<?= date('Y-m-d') ?>" />
                        <div class="input-group-apend">
                            <button class="btn btn--raised btn-success btn--icon-text page-action-filter" style="height: 34.13px;">
                                <i class="zmdi zmdi-filter-list"></i> Apply
                            </button>
                        </div>
                    </div>
                </div>
                <!-- END ## Filter on desktop -->
            </div>

            <?php include_once('form.php') ?>
            <?php include_once('detail.php') ?>
            <?php include_once('scan_barcode.php') ?>

            <div class="table-responsive">
                <table id="table-transaksibuku" class="table table-bordered">
                    <thead class="thead-default">
                        <tr>
                            <th width="100" rowspan="2">No</th>
                            <th rowspan="2">Kode</th>
                            <th rowspan="2">Peminjam</th>
                            <th rowspan="2">Tanggal Pinjam</th>
                            <th rowspan="2">Tanggal Kembali</th>
                            <th colspan="3" class="text-center">Jumlah Buku</th>
                            <th rowspan="2">Status</th>
                            <th rowspan="2">Created</th>
                            <th width="170" rowspan="2">Action</th>
                        </tr>
                        <tr>
                            <th>Dipinjam</th>
                            <th>Belum Dikembalikan</th>
                            <th>Dikembalikan</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</section>