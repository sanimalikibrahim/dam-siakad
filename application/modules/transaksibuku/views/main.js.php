<script type="text/javascript">
  $(document).ready(function() {

    var _key = "";
    var _filterDate = "<?= date('Y-m-d') ?>";
    var _section = "transaksibuku";
    var _table = "table-transaksibuku";
    var _table_buku = "table-buku";
    var _table_buku_assigned = "table-buku-assigned";
    var _table_buku_picked = "table-buku-picked";
    var _modal = "modal-form-transaksibuku";
    var _modal_detail = "modal-detail-transaksibuku";
    var _modal_scan_barcode = "modal-scanbarcode";
    var _form = "form-transaksibuku";

    // Initialize WebCodeCamJS
    var WebCodeCamJS_Args = {
      brightness: 15,
      resultFunction: function(result) {
        $("#" + _form + " ." + _section + "-search_keyword").val(result.code);
        $("#" + _modal_scan_barcode + " button.scanbarcode-action-close").click();
        searchBuku();
      },
      cameraSuccess: function(stream) {
        $("#" + _modal_scan_barcode + " #cam-error").html("");
      },
      cameraError: function(error) {
        $("#" + _modal_scan_barcode + " #cam-error").html("Failed to open camera.");
      },
      getDevicesError: function(error) {
        $("#" + _modal_scan_barcode + " #cam-error").html("Failed to open camera.");
      },
      getUserMediaError: function(error) {
        $("#" + _modal_scan_barcode + " #cam-error").html("Failed to open camera.");
      },
      canPlayFunction: function() {
        $("#" + _modal_scan_barcode + " #cam-error").html("Can't execute play function.");
      },
    };
    var WebCodeCamJS_Decoder = $("#cam-canvas").WebCodeCamJQuery(WebCodeCamJS_Args).data().plugin_WebCodeCamJQuery;
    WebCodeCamJS_Decoder.buildSelectMenu("#cam-source");

    // Initialize DataTables: Index
    if ($("#" + _table)[0]) {
      var table_transaksibuku = $("#" + _table).DataTable({
        processing: true,
        serverSide: true,
        ajax: {
          url: "<?php echo base_url('transaksibuku/ajax_get_all/?date=') ?>" + _filterDate,
          type: "get"
        },
        columns: [{
            data: null,
            render: function(data, type, row, meta) {
              return meta.row + meta.settings._iDisplayStart + 1;
            }
          },
          {
            data: "kode"
          },
          {
            data: "nama_lengkap"
          },
          {
            data: "tanggal_pinjam"
          },
          {
            data: "tanggal_kembali"
          },
          {
            data: "jumlah_buku"
          },
          {
            data: "jumlah_buku_belum_dikembalikan"
          },
          {
            data: "jumlah_buku_dikembalikan"
          },
          {
            data: "status",
            render: function(data, type, row, meta) {
              let output = '<span class="badge badge-warning">' + data + '</span>';

              if (data === 'Selesai') {
                output = '<span class="badge badge-success">' + data + '</span>';
              };

              return output;
            }
          },
          {
            data: "created_at"
          },
          {
            data: null,
            className: "center",
            defaultContent: '<div class="action">' +
              '<a href="javascript:;" class="btn btn-sm btn-light btn-table-action action-detail" data-toggle="modal" data-target="#' + _modal_detail + '"><i class="zmdi zmdi-eye"></i> Detail</a>&nbsp;' +
              '<a href="javascript:;" class="btn btn-sm btn-light btn-table-action action-delete"><i class="zmdi zmdi-delete"></i> Delete</a>' +
              '</div>'
          }
        ],
        autoWidth: !1,
        sorting: [
          [0]
        ],
        responsive: {
          details: {
            renderer: function(api, rowIdx, columns) {
              var hideColumn = [];
              var data = $.map(columns, function(col, i) {
                return ($.inArray(col.columnIndex, hideColumn)) ?
                  '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                  '<td class="dt-details-td">' + col.title + ':' + '</td> ' +
                  '<td class="dt-details-td">' + col.data + '</td>' +
                  '</tr>' :
                  '';
              }).join('');

              return data ? $('<table/>').append(data) : false;
            },
            type: "inline",
            target: 'tr',
          }
        },
        columnDefs: [{
          targets: [9],
          visible: false
        }, {
          targets: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
          orderable: false
        }, {
          className: 'desktop',
          targets: [0, 1, 2, 3, 4, 5, 6, 7, 8, 10]
        }, {
          className: 'tablet',
          targets: [0, 2, 5, 6, 7, 8]
        }, {
          className: 'mobile',
          targets: [0, 1, 5]
        }, {
          responsivePriority: 2,
          targets: -1
        }],
        pageLength: 15,
        language: {
          searchPlaceholder: "Search...",
          sProcessing: '<div style="text-align: center;"><div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div></div>'
        },
        sDom: '<"dataTables_ct"><"dataTables__top"fb>rt<"dataTables__bottom"ip><"clear">',
        buttons: [{
          extend: "excelHtml5",
          title: "Export Result"
        }, {
          extend: "print",
          title: "Export Result"
        }],
        initComplete: function(a, b) {
          $(this).closest(".dataTables_wrapper").find(".dataTables__top").prepend(
            '<div class="dataTables_buttons hidden-sm-down actions">' +
            '<span class="actions__item zmdi zmdi-refresh" data-table-action="reload" title="Reload" />' +
            '</div>'
          );
        },
      });

      $(".dataTables_filter input[type=search]").focus(function() {
        $(this).closest(".dataTables_filter").addClass("dataTables_filter--toggled")
      });

      $(".dataTables_filter input[type=search]").blur(function() {
        $(this).closest(".dataTables_filter").removeClass("dataTables_filter--toggled")
      });

      $("body").on("click", "[data-table-action]", function(a) {
        a.preventDefault();
        var b = $(this).data("table-action");
        if ("reload" === b) {
          $("#" + _table).DataTable().ajax.reload(null, false);
        };
      });
    };

    // Intialize DataTables: Buku
    var table_buku = $("#" + _table_buku).DataTable({
      destroy: true,
      info: false,
      searching: false,
      lengthChange: false,
      data: [],
      pageLength: 10,
      columns: [{
          data: null,
          render: function(data, type, row, meta) {
            return meta.row + meta.settings._iDisplayStart + 1;
          }
        },
        {
          data: 'kategori_nama'
        },
        {
          data: 'kode'
        },
        {
          data: 'kode_rak'
        },
        {
          data: 'judul'
        },
        {
          data: 'tahun_terbit'
        },
        {
          data: null,
          className: "center",
          defaultContent: '<div class="action">' +
            '<a href="javascript:;" class="btn btn-sm btn-primary btn-table-action action-buku-add"><i class="zmdi zmdi-plus-circle"></i></a>' +
            '</div>'
        }
      ],
      responsive: {
        details: {
          renderer: function(api, rowIdx, columns) {
            var hideColumn = [];
            var data = $.map(columns, function(col, i) {
              return ($.inArray(col.columnIndex, hideColumn)) ?
                '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                '<td class="dt-details-td">' + col.title + ':' + '</td> ' +
                '<td class="dt-details-td">' + col.data + '</td>' +
                '</tr>' :
                '';
            }).join('');

            return data ? $('<table/>').append(data) : false;
          },
          type: "inline",
          target: 'tr',
        }
      },
      columnDefs: [{
        className: 'desktop',
        targets: [0, 1, 2, 3, 4, 5, 6]
      }, {
        className: 'tablet',
        targets: [0, 2, 4]
      }, {
        className: 'mobile',
        targets: [0, 4]
      }, {
        responsivePriority: 2,
        targets: -1
      }],
    });

    // Intialize DataTables: Buku Assigned
    var table_buku_assigned = $("#" + _table_buku_assigned).DataTable({
      destroy: true,
      info: false,
      searching: false,
      lengthChange: false,
      data: [],
      pageLength: 10,
      columns: [{
          data: null,
        },
        {
          data: 'kategori_nama'
        },
        {
          data: 'kode'
        },
        {
          data: 'kode_rak'
        },
        {
          data: 'judul'
        },
        {
          data: 'tahun_terbit'
        },
        {
          data: null,
          className: "center",
          defaultContent: '<div class="action">' +
            '<a href="javascript:;" class="btn btn-sm btn-danger btn-table-action action-buku-delete"><i class="zmdi zmdi-delete"></i></a>' +
            '</div>'
        }
      ],
      responsive: {
        details: {
          renderer: function(api, rowIdx, columns) {
            var hideColumn = [];
            var data = $.map(columns, function(col, i) {
              return ($.inArray(col.columnIndex, hideColumn)) ?
                '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                '<td class="dt-details-td">' + col.title + ':' + '</td> ' +
                '<td class="dt-details-td">' + col.data + '</td>' +
                '</tr>' :
                '';
            }).join('');

            return data ? $('<table/>').append(data) : false;
          },
          type: "inline",
          target: 'tr',
        }
      },
      columnDefs: [{
        className: 'desktop',
        targets: [0, 1, 2, 3, 4, 5, 6]
      }, {
        className: 'tablet',
        targets: [0, 2, 4]
      }, {
        className: 'mobile',
        targets: [0, 4]
      }, {
        responsivePriority: 2,
        targets: -1
      }],
    });

    // Initalize column number: Buku Assigned
    table_buku_assigned.on('order.dt search.dt', function() {
      table_buku_assigned.column(0, {
        search: 'applied',
        order: 'applied'
      }).nodes().each(function(cell, i) {
        cell.innerHTML = i + 1;
      });
    }).draw();

    // Intialize DataTables: Buku Picked
    var table_buku_picked = $("#" + _table_buku_picked).DataTable({
      destroy: true,
      info: false,
      lengthChange: false,
      sorting: [],
      data: [],
      pageLength: 10,
      columns: [{
          data: null,
        },
        {
          data: 'buku_kode'
        },
        {
          data: 'buku_judul'
        },
        {
          data: 'tanggal_pengembalian'
        },
        {
          data: 'status'
        },
        {
          data: null,
          className: "center",
          render: function(data, type, row, meta) {
            let output = '<div class="action">';
            if (row.status === 'Menunggu Pengembalian') {
              output += '<a href="javascript:;" class="btn btn-sm btn-success btn-table-action action-buku-setstatus" status="Selesai" data-toggle="tooltip" data-original-title="Set as Selesai"><i class="zmdi zmdi-check-circle"></i></a>';
            } else {
              output += '<a href="javascript:;" class="btn btn-sm btn-danger btn-table-action action-buku-setstatus" status="Menunggu Pengembalian" data-toggle="tooltip" data-original-title="Set as Menunggu Pengembalian"><i class="zmdi zmdi-close-circle"></i></a>';
            };
            output += '</div>';

            return output;
          }
        }
      ],
      language: {
        searchPlaceholder: "Search..."
      },
      responsive: {
        details: {
          renderer: function(api, rowIdx, columns) {
            var hideColumn = [];
            var data = $.map(columns, function(col, i) {
              return ($.inArray(col.columnIndex, hideColumn)) ?
                '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                '<td class="dt-details-td">' + col.title + ':' + '</td> ' +
                '<td class="dt-details-td">' + col.data + '</td>' +
                '</tr>' :
                '';
            }).join('');

            return data ? $('<table/>').append(data) : false;
          },
          type: "inline",
          target: 'tr',
        }
      },
      columnDefs: [{
        targets: 0,
        orderable: false
      }, {
        className: 'desktop',
        targets: [0, 1, 2, 3, 4, 5]
      }, {
        className: 'tablet',
        targets: [0, 2, 4]
      }, {
        className: 'mobile',
        targets: [0, 2]
      }, {
        responsivePriority: 2,
        targets: -1
      }],
    });

    // Initalize column number: Buku Picked
    table_buku_picked.on('order.dt search.dt', function() {
      table_buku_picked.column(0, {
        search: 'applied',
        order: 'applied'
      }).nodes().each(function(cell, i) {
        cell.innerHTML = i + 1;
      });
    }).draw();

    // Handle data add
    $("#" + _section).on("click", "button." + _section + "-action-add", function(e) {
      e.preventDefault();
      resetForm();
    });

    // Handle data detail
    $("#" + _table).on("click", "a.action-detail", function(e) {
      e.preventDefault();
      var temp = table_transaksibuku.row($(this).closest('tr')).data();

      getTransaksiDetail(temp.kode);
    });

    // Handle data submit
    $("#" + _modal + " ." + _section + "-action-save").on("click", function(e) {
      e.preventDefault();
      $.ajax({
        type: "post",
        url: "<?php echo base_url('transaksibuku/ajax_save/') ?>" + _key,
        data: $("#" + _form).serialize(),
        success: function(response) {
          var response = JSON.parse(response);
          if (response.status === true) {
            resetForm();
            $("#" + _modal).modal("hide");
            $("#" + _table).DataTable().ajax.reload(null, false);
            notify(response.data, "success");
          } else {
            notify(response.data, "danger");
          };
        }
      });
    });

    // Handle data delete
    $("#" + _table).on("click", "a.action-delete", function(e) {
      e.preventDefault();
      var temp = table_transaksibuku.row($(this).closest('tr')).data();

      swal({
        title: "Are you sure to delete?",
        text: "Once deleted, you will not be able to recover this data!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false
      }).then((result) => {
        if (result.value) {
          $.ajax({
            type: "delete",
            url: "<?php echo base_url('transaksibuku/ajax_delete/') ?>" + temp.kode,
            dataType: "json",
            success: function(response) {
              if (response.status) {
                $("#" + _table).DataTable().ajax.reload(null, false);
                notify(response.data, "success");
              } else {
                notify(response.data, "danger");
              };
            }
          });
        };
      });
    });

    // Handle search buku
    $("#" + _form + " ." + _section + "-search_keyword").on("keyup", function(e) {
      e.preventDefault();
      if (e.which === 13) {
        searchBuku();
      };
    });
    $("#" + _form + " ." + _section + "-search_submit").on("click", function(e) {
      e.preventDefault();
      searchBuku();
    });

    // Handle scan barcode
    $("#" + _form + " ." + _section + "-scan_barcode").on("click", function(e) {
      e.preventDefault();
      $("#" + _modal).modal("hide");
      WebCodeCamJS_Decoder.play();
    });

    // handle scan barcode: Stop
    $("#" + _modal_scan_barcode).on("click", "button.scanbarcode-action-close", function() {
      $("#" + _modal).modal("show");
      WebCodeCamJS_Decoder.stop();
    });

    // Handle scan barcode: Change source
    $("#" + _modal_scan_barcode + " #cam-source").on("change", function() {
      WebCodeCamJS_Decoder.stop().play();
    });

    // Handle assign buku
    $("#" + _table_buku).on("click", "a.action-buku-add", function(e) {
      e.preventDefault();
      var temp = table_buku.row($(this).closest('tr')).data();
      var isExist = false;
      var hiddenPayload = '';

      table_buku_assigned.rows(function(idx, data, node) {
        if (data.id === temp.id) {
          isExist = true;
        };
      });

      if (isExist === false) {
        hiddenPayload = '<input type="hidden" name="buku_id[]" class="buku-assigned-' + temp.id + '" value="' + temp.id + '" readonly />';
        table_buku_assigned.row.add(temp).draw();
        $(".hidden-payload").append(hiddenPayload);
      } else {
        notify("Already exist cannot be added.", "warning");
      };
    });

    // Handle delete buku assigned
    $("#" + _table_buku_assigned).on("click", "a.action-buku-delete", function(e) {
      e.preventDefault();
      var temp = table_buku_assigned.row($(this).closest('tr')).data();

      table_buku_assigned.row($(this).parents('tr')).remove().draw();
      $(".buku-assigned-" + temp.id).remove();
    });

    // Handle data set status
    $("#" + _table_buku_picked).on("click", "a.action-buku-setstatus", function(e) {
      e.preventDefault();
      var temp = table_buku_picked.row($(this).closest('tr')).data();
      var status = $(this).attr("status");

      $.ajax({
        type: "post",
        url: "<?php echo base_url('transaksibuku/ajax_set_status/') ?>",
        data: {
          id: temp.id,
          status: status
        },
        dataType: "json",
        success: function(response) {
          if (response.status === true) {
            getTransaksiDetail(temp.kode);
            table_transaksibuku.ajax.reload();
          } else {
            notify(response.data, "danger");
          };
        }
      });
    });

    // Handle filter
    $("#" + _section).on("click", "button.page-action-filter", function(e) {
      e.preventDefault();
      var currentDate = getCurrentDate();
      var date = $("#" + _section + " .filter-tanggal_pinjam").val();
      _filterDate = date;

      // Fetch new data
      if (_filterDate !== "") {
        table_transaksibuku.ajax.url("<?php echo base_url('transaksibuku/ajax_get_all/?date=') ?>" + _filterDate).load();
      };

      // Handle add button
      if (date === currentDate) {
        $("#" + _section + " .buttons-action-add").show();
      } else {
        $("#" + _section + " .buttons-action-add").hide();
      };
    });

    // Handle form reset
    resetForm = () => {
      _key = "";
      $("#" + _form).trigger("reset");
      $("#" + _form + " ." + _section + "-user_id").val("").trigger('change');
      $("#" + _table_buku).dataTable().fnClearTable();
      $("#" + _table_buku_assigned).dataTable().fnClearTable();
      $(".hidden-payload").html("");
    };

    // Handle get search buku
    function searchBuku() {
      const keyword = $("#" + _form + " ." + _section + "-search_keyword").val().trim();

      if (keyword !== "") {
        $.ajax({
          type: "post",
          url: "<?php echo base_url('transaksibuku/ajax_search_buku/') ?>" + keyword,
          data: {
            keyword: keyword
          },
          dataType: "json",
          success: function(response) {
            $("#" + _table_buku).dataTable().fnClearTable();

            if (response.length > 0) {
              $("#" + _table_buku).dataTable().fnAddData(response);
            }
          }
        });
      };
    };

    // Handle get detail
    function getTransaksiDetail(kode) {
      $("#" + _modal_detail + " .table-detail .detail-item-kode").html("-");
      $("#" + _modal_detail + " .table-detail .detail-item-peminjam").html("-");
      $("#" + _modal_detail + " .table-detail .detail-item-tanggal_pinjam").html("-");
      $("#" + _modal_detail + " .table-detail .detail-item-tanggal_kembali").html("-");
      $("#" + _modal_detail + " .table-detail .detail-item-jumlah_dipinjam").html("-");
      $("#" + _modal_detail + " .table-detail .detail-item-jumlah_belum_dikembalikan").html("-");
      $("#" + _modal_detail + " .table-detail .detail-item-jumlah_dikembalikan").html("-");
      $("#" + _modal_detail + " .table-detail .detail-item-keterangan").html("-");
      $("#" + _table_buku_picked).dataTable().fnClearTable();

      // Fetch data
      $.ajax({
        url: "<?php echo base_url('transaksibuku/ajax_get_detail/') ?>" + kode,
        type: "get",
        success: function(response) {
          var response = JSON.parse(response);

          if (response.status === true) {
            $("#" + _modal_detail + " .table-detail .detail-item-kode").html(response.data.header.kode);
            $("#" + _modal_detail + " .table-detail .detail-item-peminjam").html(response.data.header.nama_lengkap);
            $("#" + _modal_detail + " .table-detail .detail-item-tanggal_pinjam").html(response.data.header.tanggal_pinjam);
            $("#" + _modal_detail + " .table-detail .detail-item-tanggal_kembali").html(response.data.header.tanggal_kembali);
            $("#" + _modal_detail + " .table-detail .detail-item-jumlah_dipinjam").html(response.data.header.jumlah_buku);
            $("#" + _modal_detail + " .table-detail .detail-item-jumlah_belum_dikembalikan").html(response.data.header.jumlah_buku_belum_dikembalikan);
            $("#" + _modal_detail + " .table-detail .detail-item-jumlah_dikembalikan").html(response.data.header.jumlah_buku_dikembalikan);
            $("#" + _modal_detail + " .table-detail .detail-item-keterangan").html(response.data.body[0].keterangan_pinjam);
            $("#" + _table_buku_picked).dataTable().fnAddData(response.data.body);

            // Initialize tooltip
            $('[data-toggle="tooltip"]').tooltip();
          } else {
            notify(response.data, "danger");
          };
        }
      });
    };

  });
</script>