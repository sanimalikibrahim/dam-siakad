<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . 'controllers/AppBackend.php');

class Transaksibuku extends AppBackend
{
  function __construct()
  {
    parent::__construct();
    $this->load->model([
      'AppModel',
      'UserModel',
      'BukuModel',
      'TransaksibukuModel'
    ]);
    $this->load->library('form_validation');
  }

  public function index()
  {
    $data = array(
      'app' => $this->app(),
      'main_js' => $this->load_main_js('transaksibuku'),
      'card_title' => 'Peminjaman Buku',
      'list_user' => $this->init_list($this->UserModel->getAllCombo(), 'id', 'label')
    );
    $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
    $this->template->load_view('index', $data, TRUE);
    $this->template->render();
  }

  public function ajax_get_all()
  {
    $this->handle_ajax_request();
    $date = $this->input->get('date');
    $dtAjax_config = array(
      'table_name' => 'view_transaksi_buku_list',
      'order_column' => 9,
      'order_column_dir' => 'desc',
      'static_conditional' => [
        'tanggal_pinjam' => (is_null($date) || empty($date)) ? date('Y-m-d') : $date
      ]
    );
    $response = $this->AppModel->getData_dtAjax($dtAjax_config);
    echo json_encode($response);
  }

  public function ajax_save($id = null)
  {
    $this->handle_ajax_request();
    $this->form_validation->set_rules($this->TransaksibukuModel->rules_pinjam());

    if ($this->form_validation->run() === true) {
      if (is_null($id)) {
        echo json_encode($this->TransaksibukuModel->insertPinjam());
      } else {
        echo json_encode($this->TransaksibukuModel->updatePengembalian($id));
      };
    } else {
      $errors = validation_errors('<div>- ', '</div>');
      echo json_encode(array('status' => false, 'data' => $errors));
    };
  }

  public function ajax_delete($id)
  {
    $this->handle_ajax_request();
    echo json_encode($this->TransaksibukuModel->delete($id));
  }

  public function ajax_search_buku()
  {
    $this->handle_ajax_request();
    echo json_encode($this->BukuModel->getSearch($this->input->post('keyword')));
  }

  public function ajax_get_detail($kode = null)
  {
    $this->handle_ajax_request();
    echo json_encode($this->TransaksibukuModel->getDetailFull($kode));
  }

  public function ajax_set_status()
  {
    $this->handle_ajax_request();
    echo json_encode($this->TransaksibukuModel->setStatus());
  }
}
