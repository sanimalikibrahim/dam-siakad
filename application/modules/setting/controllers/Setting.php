<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . 'controllers/AppBackend.php');
require_once(APPPATH . 'controllers/ApiClient.php');

class Setting extends AppBackend
{
  function __construct()
  {
    parent::__construct();
    $this->load->model([
      'UserModel',
      'SettingAppModel',
      'SettingSmtpModel',
      'SettingAccountModel',
      'SettingDashboardModel',
      'SettingMoreModel'
    ]);
  }

  private function _updateUserPasswordLearndash($id, $password)
  {
    $apiClient = new ApiClient();
    $response = $apiClient->updateUserPassword($id, $password);

    return $response;
  }

  public function index()
  {
    redirect(base_url('setting/account'));
  }

  public function application()
  {
    $data = array(
      'app' => $this->app(),
      'main_js' => $this->load_main_js('setting'),
      'card_title' => 'Pengaturan › Aplikasi'
    );
    $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
    $this->template->load_view('application', $data, TRUE);
    $this->template->render();
  }

  public function smtp()
  {
    $data = array(
      'app' => $this->app(),
      'main_js' => $this->load_main_js('setting'),
      'card_title' => 'Pengaturan › SMTP'
    );
    $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
    $this->template->load_view('smtp', $data, TRUE);
    $this->template->render();
  }

  public function account()
  {
    $data = array(
      'app' => $this->app(),
      'main_js' => $this->load_main_js('setting'),
      'card_title' => 'Pengaturan › Account',
      'role' => $this->session->userdata('user')['role'],
      'data' => $this->SettingAccountModel->getDetail(['id' => $this->session->userdata('user')['id']]),
    );
    $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
    $this->template->load_view('account', $data, TRUE);
    $this->template->render();
  }

  public function dashboard()
  {
    $data = array(
      'app' => $this->app(),
      'main_js' => $this->load_main_js('setting'),
      'card_title' => 'Pengaturan › Dashboard Image'
    );
    $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
    $this->template->load_view('dashboard_image', $data, TRUE);
    $this->template->render();
  }

  public function more()
  {
    $data = array(
      'app' => $this->app(),
      'main_js' => $this->load_main_js('setting'),
      'card_title' => 'Pengaturan › Lain-lain'
    );
    $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
    $this->template->load_view('more', $data, TRUE);
    $this->template->render();
  }

  public function ajax_save_application()
  {
    $this->handle_ajax_request();
    $this->form_validation->set_rules($this->SettingAppModel->rules());

    if ($this->form_validation->run() === true) {
      echo json_encode($this->SettingAppModel->update());
    } else {
      $errors = validation_errors('<div>- ', '</div>');
      echo json_encode(array('status' => false, 'data' => $errors));
    };
  }

  public function ajax_test_smtp()
  {
    $this->handle_ajax_request();

    $app = $this->app();
    $mailParams = array(
      'receiver' => $app->smtp_user,
      'subject' => 'Test (' . md5(date('Y-m-d H:i:s')) . ')',
      'message' => '<h2>Test</h2><p>Congratulations, email has been successfully sent from ' . $app->app_name . '.</p>'
    );

    echo json_encode($this->sendMail($mailParams));
  }

  public function ajax_save_smtp()
  {
    $this->handle_ajax_request();
    $this->form_validation->set_rules($this->SettingSmtpModel->rules());

    if ($this->form_validation->run() === true) {
      echo json_encode($this->SettingSmtpModel->update());
    } else {
      $errors = validation_errors('<div>- ', '</div>');
      echo json_encode(array('status' => false, 'data' => $errors));
    };
  }

  public function ajax_save_account()
  {
    $this->handle_ajax_request();
    $this->form_validation->set_rules($this->SettingAccountModel->rules());

    $role = $this->session->userdata('user')['role'];
    $userId = $this->session->userdata('user')['id'];
    $password = $this->input->post('password');

    if ($this->form_validation->run() === true) {
      $isErrorLearndash = false;

      // Upload foto prifile
      if (!empty($_FILES['profile_photo']['name'])) {
        $cpUpload = new CpUpload();
        $upload = $cpUpload->run('profile_photo', 'profile', true, true, 'jpg|jpeg|png|gif');

        $_POST['profile_photo'] = '';

        if ($upload->status === true) {
          $_POST['profile_photo'] = $upload->data->base_path;
        };
      };

      // Update user in learndash-wp
      if (in_array($role, ['Santri', 'Calon Santri']) && (!is_null($password) && !empty($password))) {
        $user = $this->UserModel->getDetail(['id' => $userId]);
        $learndashUserId = $user->learndash_user_id;

        if (!is_null($learndashUserId)) {
          $updateLearndash = $this->_updateUserPasswordLearndash($learndashUserId, $password);

          if ($updateLearndash['status'] !== true) {
            $isErrorLearndash = $updateLearndash;
          };
        };
      };

      if ($isErrorLearndash === false) {
        echo json_encode($this->SettingAccountModel->update());
      } else {
        echo json_encode($isErrorLearndash);
      };
    } else {
      $errors = validation_errors('<div>- ', '</div>');
      echo json_encode(array('status' => false, 'data' => $errors));
    };
  }

  public function ajax_save_dashboard()
  {
    $this->handle_ajax_request();
    $this->form_validation->set_rules($this->SettingDashboardModel->rules());

    if ($this->form_validation->run() === true) {
      if (!empty($_FILES['dashboard_image_source']['name'])) {
        $cpUpload = new CpUpload();
        $upload = $cpUpload->run('dashboard_image_source', 'dashboard', true, true, 'jpg|jpeg|png|gif');

        $_POST['dashboard_image_source'] = '';

        if ($upload->status === true) {
          $_POST['dashboard_image_source'] = $upload->data->base_path;
        };
      };

      echo json_encode($this->SettingDashboardModel->update());
    } else {
      $errors = validation_errors('<div>- ', '</div>');
      echo json_encode(array('status' => false, 'data' => $errors));
    };
  }

  public function ajax_save_more()
  {
    $this->handle_ajax_request();
    $this->form_validation->set_rules($this->SettingMoreModel->rules());

    if ($this->form_validation->run() === true) {
      // Upload Kartu PSB
      if (!empty($_FILES['idcard_psb_background']['name'])) {
        $cpUpload = new CpUpload();
        $upload = $cpUpload->run('idcard_psb_background', 'idcard', true, true, 'jpg|jpeg|png|gif');

        $_POST['idcard_psb_background'] = '';

        if ($upload->status === true) {
          $_POST['idcard_psb_background'] = $upload->data->base_path;
        };
      };

      // Upload Kartu Pelajar
      if (!empty($_FILES['idcard_pelajar_background']['name'])) {
        $cpUpload = new CpUpload();
        $upload = $cpUpload->run('idcard_pelajar_background', 'idcard', true, true, 'jpg|jpeg|png|gif');

        $_POST['idcard_pelajar_background'] = '';

        if ($upload->status === true) {
          $_POST['idcard_pelajar_background'] = $upload->data->base_path;
        };
      };

      echo json_encode($this->SettingMoreModel->update());
    } else {
      $errors = validation_errors('<div>- ', '</div>');
      echo json_encode(array('status' => false, 'data' => $errors));
    };
  }
}
