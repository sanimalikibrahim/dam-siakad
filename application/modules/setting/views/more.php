<?php
$profile_photo_temp = $this->session->userdata('user')['profile_photo'];
$profile_photo = (!is_null($profile_photo_temp) && !empty($profile_photo_temp)) ? base_url($profile_photo_temp) : base_url('themes/_public/img/avatar/male-1.png');
$qrcode = base_url('themes/_public/img/qrcode-sample.jpg');
?>

<style type="text/css">
    #setting .card-title {
        color: #2196F3;
        font-weight: 500;
    }
</style>

<section id="setting">
    <div class="card">
        <div class="card-body">

            <?php if (in_array($this->session->userdata('user')['role'], ['Administrator', 'Staff Perpustakaan', 'Panitia PSB'])) : ?>
                <form id="form-setting-more" enctype="multipart/form-data" autocomplete="off">
                    <!-- CSRF -->
                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />

                    <div class="row">
                        <div class="col-xs-10 col-md-6">
                            <?php if (in_array($this->session->userdata('user')['role'], ['Administrator', 'Staff Perpustakaan'])) : ?>
                                <!-- Pengajuan Buku -->
                                <div class="pengajuan_buku mb-5">
                                    <h4 class="card-title mb-0">Pengajuan Buku</h4>
                                    <hr />
                                    <div class="form-group mb-2">
                                        <label required>Format Nomor</label>
                                        <input type="text" name="pengajuan_nomor" class="form-control setting-pengajuan_nomor" placeholder="Format Nomor" value="<?php echo (isset($app->pengajuan_nomor)) ? $app->pengajuan_nomor : '' ?>" />
                                        <i class="form-group__bar"></i>
                                    </div>
                                    <div style="border: 1px solid #eceff1; padding: 8px 10px;">
                                        Parameter :
                                        <table class="table table-sm table-bordered mt-3 mb-0">
                                            <tr>
                                                <td>
                                                    <b class="text-primary">{INC}</b>
                                                    <p class="m-0">
                                                        Auto increment, nomor akan ditambah 1 mengikuti format yang sama.<br />
                                                        <small class="text-muted">Contoh: 0001, 0002, etc...</small>
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <b class="text-primary">{MONTH}</b>
                                                    <p class="m-0">
                                                        Menampilkan urutan bulan dalam format romawi. <br />
                                                        <small class="text-muted">Contoh: I, II, III, etc...</small>
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <b class="text-primary">{MONTH_NUM}</b>
                                                    <p class="m-0">
                                                        Menampilkan urutan bulan dalam format nomor. <br />
                                                        <small class="text-muted">Contoh: 1, 2, 3, etc...</small>
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <b class="text-primary">{YEAR}</b>
                                                    <p class="m-0">
                                                        Menampilkan tahun dalam 4 digit. <br />
                                                        <small class="text-muted">Contoh: <?= date('Y') ?></small>
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <b class="text-primary">{YEAR_2}</b>
                                                    <p class="m-0">
                                                        Menampilkan tahun dalam 2 digit. <br />
                                                        <small class="text-muted">Contoh: <?= date('y') ?></small>
                                                    </p>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            <?php endif ?>
                            <?php if (in_array($this->session->userdata('user')['role'], ['Administrator', 'Panitia PSB'])) : ?>
                                <!-- Sinkronisasi LMS -->
                                <div class="sinkronisasi_lms mb-5">
                                    <h4 class="card-title mb-0">Sinkronisasi LMS</h4>
                                    <hr />
                                    <div class="form-group">
                                        <label required>API URL</label>
                                        <input type="text" name="api_url" class="form-control setting-api_url" placeholder="URL" value="<?php echo (isset($app->api_url)) ? $app->api_url : '' ?>" />
                                        <i class="form-group__bar"></i>
                                    </div>

                                    <div class="form-group">
                                        <label required>Authenticate</label>
                                        <div class="row">
                                            <div class="col-xs-10 col-md-6">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" style="width: 90px;">Username</span>
                                                    </div>
                                                    <input type="text" name="api_user" class="form-control setting-api_user" placeholder="Username" value="<?php echo (isset($app->api_user)) ? $app->api_user : '' ?>" />
                                                    <i class="form-group__bar"></i>
                                                </div>
                                            </div>
                                            <div class="col-xs-10 col-md-6">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" style="width: 90px;">Password</span>
                                                    </div>
                                                    <input type="password" name="api_password" class="form-control setting-api_password" placeholder="Password" value="<?php echo (isset($app->api_password)) ? $app->api_password : '' ?>" />
                                                    <i class="form-group__bar"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-10 col-md-6">
                                            <div class="form-group mb-2">
                                                <label required>Course ID</label>
                                                <input type="number" name="sinkronisasi_course_id" class="form-control mask-number setting-sinkronisasi_course_id" maxlength="10" placeholder="Course ID" value="<?php echo (isset($app->sinkronisasi_course_id)) ? $app->sinkronisasi_course_id : '' ?>" />
                                                <i class="form-group__bar"></i>
                                            </div>
                                        </div>
                                        <div class="col-xs-10 col-md-6">
                                            <div class="form-group mb-2">
                                                <label required>Group ID</label>
                                                <input type="number" name="sinkronisasi_group_id" class="form-control mask-number setting-sinkronisasi_group_id" maxlength="10" placeholder="Group ID" value="<?php echo (isset($app->sinkronisasi_group_id)) ? $app->sinkronisasi_group_id : '' ?>" />
                                                <i class="form-group__bar"></i>
                                            </div>
                                        </div>
                                        <div class="col-xs-10 col-md-12">
                                            <div style="border: 1px solid #eceff1; padding: 8px 10px;">
                                                Course ID & Group ID didapat dari aplikasi LMS, ini akan digunakan untuk sinkronisasi data kriteria penilaian.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endif ?>
                            <?php if (in_array($this->session->userdata('user')['role'], ['Administrator', 'Panitia PSB'])) : ?>
                                <!-- Pendaftaran Santri Baru -->
                                <div class="psb mb-5">
                                    <h4 class="card-title mb-0">Pendaftaran Santri Baru</h4>
                                    <hr />
                                    <div class="form-group">
                                        <label required>Status</label>
                                        <div class="pt-1">
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="psb_status" id="psb_status-1" value="1" <?= ((int) $app->psb_status === 1) ? 'checked' : '' ?>>
                                                <label class="form-check-label" for="psb_status-1">Aktif</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="psb_status" id="psb_status-0" value="0" <?= ((int) $app->psb_status === 0) ? 'checked' : '' ?>>
                                                <label class="form-check-label" for="psb_status-0">Nonaktif</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label required>Tanggal Selesai</label>
                                        <input type="text" name="psb_selesai" class="form-control flatpickr-datetime-24 setting-psb_selesai" placeholder="Tanggal Selesai" value="<?php echo (isset($app->psb_selesai)) ? $app->psb_selesai : '' ?>" />
                                        <i class="form-group__bar"></i>
                                    </div>
                                    <div class="form-group">
                                        <label required>Tahun Pelajaran</label>
                                        <div class="row">
                                            <div class="col-xs-10 col-md-6">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" style="width: 75px;">Mulai</span>
                                                    </div>
                                                    <input type="number" name="psb_tahun_mulai" class="form-control mask-number setting-psb_tahun_mulai idcard_psb-control" maxlength="4" placeholder="Mulai" value="<?= (isset($app->psb_tahun_mulai)) ? $app->psb_tahun_mulai : '' ?>" />
                                                    <i class="form-group__bar"></i>
                                                </div>
                                            </div>
                                            <div class="col-xs-10 col-md-6">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" style="width: 75px;">Selesai</span>
                                                    </div>
                                                    <input type="number" name="psb_tahun_selesai" class="form-control mask-number setting-psb_tahun_selesai idcard_psb-control" maxlength="4" placeholder="Left" value="<?= (isset($app->psb_tahun_selesai)) ? $app->psb_tahun_selesai : '' ?>" />
                                                    <i class="form-group__bar"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label required>Tipe Test</label>
                                        <div class="pt-1">
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="psb_test_type" id="psb_test_type-1" value="online" <?= (strtolower($app->psb_test_type) === 'online') ? 'checked' : '' ?>>
                                                <label class="form-check-label" for="psb_test_type-1">Online</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="psb_test_type" id="psb_test_type-0" value="offline" <?= (strtolower($app->psb_test_type) === 'offline') ? 'checked' : '' ?>>
                                                <label class="form-check-label" for="psb_test_type-0">Offline</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endif ?>
                        </div>
                        <div class="col-xs-10 col-md-12">
                            <?php if (!$app->is_mobile) : ?>
                                <?php if (in_array($this->session->userdata('user')['role'], ['Administrator', 'Panitia PSB'])) : ?>
                                    <!-- Kartu Peserta PSB -->
                                    <div class="idcard_psb">
                                        <h4 class="card-title mb-0">Kartu Peserta PSB</h4>
                                        <hr />
                                        <div class="row">
                                            <div class="col-xs-12 col">
                                                <div class="form-group">
                                                    <label>Background</label>
                                                    <div class="position-relative">
                                                        <input type="file" name="idcard_psb_background" class="form-control setting-idcard_psb_background" />
                                                        <i class="form-group__bar"></i>
                                                    </div>
                                                    <small class="form-text text-muted">
                                                        Recommended image size: 279.68 x 396.85 (px)
                                                    </small>
                                                </div>
                                                <div class="form-group">
                                                    <label required>Foto</label>
                                                    <div class="row">
                                                        <div class="col-xs-10 col-md-6">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text" style="width: 60px;">Top</span>
                                                                </div>
                                                                <input type="number" name="idcard_psb_foto_top" class="form-control mask-number setting-idcard_psb_foto_top idcard_psb-control" maxlength="5" placeholder="Top" value="<?= (isset($app->idcard_psb_foto_top)) ? $app->idcard_psb_foto_top : '' ?>" />
                                                                <i class="form-group__bar"></i>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-10 col-md-6">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text" style="width: 60px;">Left</span>
                                                                </div>
                                                                <input type="number" name="idcard_psb_foto_left" class="form-control mask-number setting-idcard_psb_foto_left idcard_psb-control" maxlength="5" placeholder="Left" value="<?= (isset($app->idcard_psb_foto_left)) ? $app->idcard_psb_foto_left : '' ?>" />
                                                                <i class="form-group__bar"></i>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-10 col-md-6">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text" style="width: 60px;">Width</span>
                                                                </div>
                                                                <input type="number" name="idcard_psb_foto_width" class="form-control mask-number setting-idcard_psb_foto_width idcard_psb-control" maxlength="5" placeholder="Width" value="<?= (isset($app->idcard_psb_foto_width)) ? $app->idcard_psb_foto_width : '' ?>" />
                                                                <i class="form-group__bar"></i>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-10 col-md-6">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text" style="width: 60px;">Height</span>
                                                                </div>
                                                                <input type="number" name="idcard_psb_foto_height" class="form-control mask-number setting-idcard_psb_foto_height idcard_psb-control" maxlength="5" placeholder="Height" value="<?= (isset($app->idcard_psb_foto_height)) ? $app->idcard_psb_foto_height : '' ?>" />
                                                                <i class="form-group__bar"></i>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-10 col-md-6">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text" style="width: 60px;">Show</span>
                                                                </div>
                                                                <select name="idcard_psb_foto_show" class="form-control setting-idcard_psb_foto_show idcard_psb-control">
                                                                    <option value="flex" <?= ($app->idcard_psb_foto_show === 'flex') ? 'selected' : '' ?>>Yes</option>
                                                                    <option value="none" <?= ($app->idcard_psb_foto_show === 'none') ? 'selected' : '' ?>>No</option>
                                                                </select>
                                                                <i class="form-group__bar"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label required>No. Peserta</label>
                                                    <div class="row">
                                                        <div class="col-xs-10 col-md-6">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text" style="width: 60px;">Top</span>
                                                                </div>
                                                                <input type="number" name="idcard_psb_nomor_top" class="form-control mask-number setting-idcard_psb_nomor_top idcard_psb-control" maxlength="5" placeholder="Top" value="<?= (isset($app->idcard_psb_nomor_top)) ? $app->idcard_psb_nomor_top : '' ?>" />
                                                                <i class="form-group__bar"></i>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-10 col-md-6">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text" style="width: 60px;">Left</span>
                                                                </div>
                                                                <input type="number" name="idcard_psb_nomor_left" class="form-control mask-number setting-idcard_psb_nomor_left idcard_psb-control" maxlength="5" placeholder="Left" value="<?= (isset($app->idcard_psb_nomor_left)) ? $app->idcard_psb_nomor_left : '' ?>" />
                                                                <i class="form-group__bar"></i>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-10 col-md-6">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text" style="width: 60px;">Size</span>
                                                                </div>
                                                                <input type="number" name="idcard_psb_nomor_font_size" class="form-control mask-number setting-idcard_psb_nomor_font_size idcard_psb-control" maxlength="5" placeholder="Size" value="<?= (isset($app->idcard_psb_nomor_font_size)) ? $app->idcard_psb_nomor_font_size : '' ?>" />
                                                                <i class="form-group__bar"></i>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-10 col-md-6">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text" style="width: 60px;">Color</span>
                                                                </div>
                                                                <input type="text" name="idcard_psb_nomor_color" class="form-control setting-idcard_psb_nomor_color idcard_psb-control" placeholder="Color" value="<?= (isset($app->idcard_psb_nomor_color)) ? $app->idcard_psb_nomor_color : '' ?>" />
                                                                <i class="form-group__bar"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label required>Nama</label>
                                                    <div class="row">
                                                        <div class="col-xs-10 col-md-6">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text" style="width: 60px;">Top</span>
                                                                </div>
                                                                <input type="number" name="idcard_psb_nama_top" class="form-control mask-number setting-idcard_psb_nama_top idcard_psb-control" maxlength="5" placeholder="Top" value="<?= (isset($app->idcard_psb_nama_top)) ? $app->idcard_psb_nama_top : '' ?>" />
                                                                <i class="form-group__bar"></i>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-10 col-md-6">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text" style="width: 60px;">Left</span>
                                                                </div>
                                                                <input type="number" name="idcard_psb_nama_left" class="form-control mask-number setting-idcard_psb_nama_left idcard_psb-control" maxlength="5" placeholder="Left" value="<?= (isset($app->idcard_psb_nama_left)) ? $app->idcard_psb_nama_left : '' ?>" />
                                                                <i class="form-group__bar"></i>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-10 col-md-6">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text" style="width: 60px;">Size</span>
                                                                </div>
                                                                <input type="number" name="idcard_psb_nama_font_size" class="form-control mask-number setting-idcard_psb_nama_font_size idcard_psb-control" maxlength="5" placeholder="Size" value="<?= (isset($app->idcard_psb_nama_font_size)) ? $app->idcard_psb_nama_font_size : '' ?>" />
                                                                <i class="form-group__bar"></i>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-10 col-md-6">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text" style="width: 60px;">Color</span>
                                                                </div>
                                                                <input type="text" name="idcard_psb_nama_color" class="form-control setting-idcard_psb_nama_color idcard_psb-control" placeholder="Color" value="<?= (isset($app->idcard_psb_nama_color)) ? $app->idcard_psb_nama_color : '' ?>" />
                                                                <i class="form-group__bar"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label required>Alamat</label>
                                                    <div class="row">
                                                        <div class="col-xs-10 col-md-6">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text" style="width: 60px;">Top</span>
                                                                </div>
                                                                <input type="number" name="idcard_psb_alamat_top" class="form-control mask-number setting-idcard_psb_alamat_top idcard_psb-control" maxlength="5" placeholder="Top" value="<?= (isset($app->idcard_psb_alamat_top)) ? $app->idcard_psb_alamat_top : '' ?>" />
                                                                <i class="form-group__bar"></i>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-10 col-md-6">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text" style="width: 60px;">Left</span>
                                                                </div>
                                                                <input type="number" name="idcard_psb_alamat_left" class="form-control mask-number setting-idcard_psb_alamat_left idcard_psb-control" maxlength="5" placeholder="Left" value="<?= (isset($app->idcard_psb_alamat_left)) ? $app->idcard_psb_alamat_left : '' ?>" />
                                                                <i class="form-group__bar"></i>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-10 col-md-6">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text" style="width: 60px;">Size</span>
                                                                </div>
                                                                <input type="number" name="idcard_psb_alamat_font_size" class="form-control mask-number setting-idcard_psb_alamat_font_size idcard_psb-control" maxlength="5" placeholder="Size" value="<?= (isset($app->idcard_psb_alamat_font_size)) ? $app->idcard_psb_alamat_font_size : '' ?>" />
                                                                <i class="form-group__bar"></i>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-10 col-md-6">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text" style="width: 60px;">Color</span>
                                                                </div>
                                                                <input type="text" name="idcard_psb_alamat_color" class="form-control setting-idcard_psb_alamat_color idcard_psb-control" placeholder="Color" value="<?= (isset($app->idcard_psb_alamat_color)) ? $app->idcard_psb_alamat_color : '' ?>" />
                                                                <i class="form-group__bar"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label required>Ruangan 1</label>
                                                    <div class="row">
                                                        <div class="col-xs-10 col-md-6">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text" style="width: 60px;">Top</span>
                                                                </div>
                                                                <input type="number" name="idcard_psb_ruangan1_top" class="form-control mask-number setting-idcard_psb_ruangan1_top idcard_psb-control" maxlength="5" placeholder="Top" value="<?= (isset($app->idcard_psb_ruangan1_top)) ? $app->idcard_psb_ruangan1_top : '' ?>" />
                                                                <i class="form-group__bar"></i>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-10 col-md-6">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text" style="width: 60px;">Left</span>
                                                                </div>
                                                                <input type="number" name="idcard_psb_ruangan1_left" class="form-control mask-number setting-idcard_psb_ruangan1_left idcard_psb-control" maxlength="5" placeholder="Left" value="<?= (isset($app->idcard_psb_ruangan1_left)) ? $app->idcard_psb_ruangan1_left : '' ?>" />
                                                                <i class="form-group__bar"></i>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-10 col-md-6">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text" style="width: 60px;">Size</span>
                                                                </div>
                                                                <input type="number" name="idcard_psb_ruangan1_font_size" class="form-control mask-number setting-idcard_psb_ruangan1_font_size idcard_psb-control" maxlength="5" placeholder="Size" value="<?= (isset($app->idcard_psb_ruangan1_font_size)) ? $app->idcard_psb_ruangan1_font_size : '' ?>" />
                                                                <i class="form-group__bar"></i>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-10 col-md-6">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text" style="width: 60px;">Color</span>
                                                                </div>
                                                                <input type="text" name="idcard_psb_ruangan1_color" class="form-control setting-idcard_psb_ruangan1_color idcard_psb-control" placeholder="Color" value="<?= (isset($app->idcard_psb_ruangan1_color)) ? $app->idcard_psb_ruangan1_color : '' ?>" />
                                                                <i class="form-group__bar"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label required>Ruangan 2</label>
                                                    <div class="row">
                                                        <div class="col-xs-10 col-md-6">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text" style="width: 60px;">Top</span>
                                                                </div>
                                                                <input type="number" name="idcard_psb_ruangan2_top" class="form-control mask-number setting-idcard_psb_ruangan2_top idcard_psb-control" maxlength="5" placeholder="Top" value="<?= (isset($app->idcard_psb_ruangan2_top)) ? $app->idcard_psb_ruangan2_top : '' ?>" />
                                                                <i class="form-group__bar"></i>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-10 col-md-6">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text" style="width: 60px;">Left</span>
                                                                </div>
                                                                <input type="number" name="idcard_psb_ruangan2_left" class="form-control mask-number setting-idcard_psb_ruangan2_left idcard_psb-control" maxlength="5" placeholder="Left" value="<?= (isset($app->idcard_psb_ruangan2_left)) ? $app->idcard_psb_ruangan2_left : '' ?>" />
                                                                <i class="form-group__bar"></i>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-10 col-md-6">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text" style="width: 60px;">Size</span>
                                                                </div>
                                                                <input type="number" name="idcard_psb_ruangan2_font_size" class="form-control mask-number setting-idcard_psb_ruangan2_font_size idcard_psb-control" maxlength="5" placeholder="Size" value="<?= (isset($app->idcard_psb_ruangan2_font_size)) ? $app->idcard_psb_ruangan2_font_size : '' ?>" />
                                                                <i class="form-group__bar"></i>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-10 col-md-6">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text" style="width: 60px;">Color</span>
                                                                </div>
                                                                <input type="text" name="idcard_psb_ruangan2_color" class="form-control setting-idcard_psb_ruangan2_color idcard_psb-control" placeholder="Color" value="<?= (isset($app->idcard_psb_ruangan2_color)) ? $app->idcard_psb_ruangan2_color : '' ?>" />
                                                                <i class="form-group__bar"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-auto">
                                                <div style="background: #252525; padding: 10px; display: flex; justify-content: center; align-items: center;">
                                                    <div id="idcard-content" class="idcard-preview" style="width: fit-content; width: -moz-fit-content; overflow: hidden;">
                                                        <div class="idcard-preview-foto" style="width: 105px; height: 150px; display: flex; justify-content: center; align-items: center; text-align: center; background: #fff; border: 1px solid #999; position: absolute; opacity: 1;">
                                                            <img class="idcard-preview-foto-thumb" src="<?= $profile_photo ?>" alt="FOTO" style="width: 100%; height: 100%; object-fit: cover;">
                                                        </div>
                                                        <div class="idcard-preview-nomor" style="width: 150px; text-align: center; font-weight: 500; position: absolute;">
                                                            <span class="idcard-test-label-nomor">0001</span>
                                                        </div>
                                                        <div class="idcard-preview-nama" style="max-width: 240px; font-weight: 500; position: absolute;">
                                                            <span class="idcard-test-label-nama">Nama Lengkap</span>
                                                        </div>
                                                        <div class="idcard-preview-alamat" style="max-width: 240px; font-weight: 500; position: absolute;">
                                                            <span class="idcard-test-label-alamat">Alamat</span>
                                                        </div>
                                                        <div class="idcard-preview-ruangan1" style="max-width: 240px; font-weight: 500; position: absolute;">
                                                            <span class="idcard-test-label-ruangan1">Ruangan 1</span>
                                                        </div>
                                                        <div class="idcard-preview-ruangan2" style="max-width: 240px; font-weight: 500; position: absolute;">
                                                            <span class="idcard-test-label-ruangan2">Ruangan 2</span>
                                                        </div>
                                                        <img class="idcard-preview-image" src="<?= base_url((isset($app->idcard_psb_background)) ? $app->idcard_psb_background : '') ?>" alt="Kartu Peserta PSB Background" style="width: 400px; object-fit: contain;" />
                                                    </div>
                                                </div>
                                                <div class="mt-4 mb-4">
                                                    <button class="btn btn-light" type="button" data-toggle="collapse" data-target="#idcard-generate" aria-expanded="false" aria-controls="idcard-generate">
                                                        <i class="zmdi zmdi-layers"></i>
                                                        Simulation
                                                    </button>
                                                    <div class="collapse" id="idcard-generate">
                                                        <div class="card card-body m-0 mt-2">
                                                            <div class="form-group">
                                                                <label>No. Peserta</label>
                                                                <input type="text" name="idcard-test-nomor" class="form-control idcard-test-nomor idcard_psb-control" placeholder="No. Peserta" value="0001" />
                                                                <i class="form-group__bar"></i>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Nama Lengkap</label>
                                                                <input type="text" name="idcard-test-nama" class="form-control idcard-test-nama idcard_psb-control" placeholder="Nama Lengkap" value="NAMA LENGKAP" />
                                                                <i class="form-group__bar"></i>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Alamat</label>
                                                                <input type="text" name="idcard-test-alamat" class="form-control idcard-test-alamat idcard_psb-control" placeholder="Alamat" value="ALAMAT" />
                                                                <i class="form-group__bar"></i>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Ruangan 1</label>
                                                                <input type="text" name="idcard-test-ruangan1" class="form-control idcard-test-ruangan1 idcard_psb-control" placeholder="Ruangan 1" value="I" />
                                                                <i class="form-group__bar"></i>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Ruangan 2</label>
                                                                <input type="text" name="idcard-test-ruangan2" class="form-control idcard-test-ruangan2 idcard_psb-control" placeholder="Ruangan 2" value="II" />
                                                                <i class="form-group__bar"></i>
                                                            </div>
                                                            <a id="btn-download-idcard" href="javascript:;" class="btn btn-success">
                                                                <i class="zmdi zmdi-download"></i>
                                                                Download
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endif ?>
                            <?php else : ?>
                                <div class="alert alert-warning">
                                    Kartu Peserta PSB only available in desktop mode.
                                </div>
                            <?php endif ?>
                        </div>
                        <div class="col-xs-10 col-md-12">
                            <?php if (!$app->is_mobile) : ?>
                                <?php if (in_array($this->session->userdata('user')['role'], ['Administrator', 'Staff TU'])) : ?>
                                    <!-- Kartu Pelajar -->
                                    <div class="idcard_pelajar">
                                        <h4 class="card-title mb-0">Kartu Pelajar</h4>
                                        <hr />
                                        <div class="row">
                                            <div class="col-xs-12 col">
                                                <div class="form-group">
                                                    <label>Background</label>
                                                    <div class="position-relative">
                                                        <input type="file" name="idcard_pelajar_background" class="form-control setting-idcard_pelajar_background" />
                                                        <i class="form-group__bar"></i>
                                                    </div>
                                                    <small class="form-text text-muted">
                                                        Recommended image size: 279.68 x 396.85 (px)
                                                    </small>
                                                </div>
                                                <div class="form-group">
                                                    <label required>Foto</label>
                                                    <div class="row">
                                                        <div class="col-xs-10 col-md-6">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text" style="width: 60px;">Top</span>
                                                                </div>
                                                                <input type="number" name="idcard_pelajar_foto_top" class="form-control mask-number setting-idcard_pelajar_foto_top idcard_pelajar-control" maxlength="5" placeholder="Top" value="<?= (isset($app->idcard_pelajar_foto_top)) ? $app->idcard_pelajar_foto_top : '' ?>" />
                                                                <i class="form-group__bar"></i>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-10 col-md-6">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text" style="width: 60px;">Left</span>
                                                                </div>
                                                                <input type="number" name="idcard_pelajar_foto_left" class="form-control mask-number setting-idcard_pelajar_foto_left idcard_pelajar-control" maxlength="5" placeholder="Left" value="<?= (isset($app->idcard_pelajar_foto_left)) ? $app->idcard_pelajar_foto_left : '' ?>" />
                                                                <i class="form-group__bar"></i>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-10 col-md-6">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text" style="width: 60px;">Width</span>
                                                                </div>
                                                                <input type="number" name="idcard_pelajar_foto_width" class="form-control mask-number setting-idcard_pelajar_foto_width idcard_pelajar-control" maxlength="5" placeholder="Width" value="<?= (isset($app->idcard_pelajar_foto_width)) ? $app->idcard_pelajar_foto_width : '' ?>" />
                                                                <i class="form-group__bar"></i>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-10 col-md-6">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text" style="width: 60px;">Height</span>
                                                                </div>
                                                                <input type="number" name="idcard_pelajar_foto_height" class="form-control mask-number setting-idcard_pelajar_foto_height idcard_pelajar-control" maxlength="5" placeholder="Height" value="<?= (isset($app->idcard_pelajar_foto_height)) ? $app->idcard_pelajar_foto_height : '' ?>" />
                                                                <i class="form-group__bar"></i>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-10 col-md-6">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text" style="width: 60px;">Show</span>
                                                                </div>
                                                                <select name="idcard_pelajar_foto_show" class="form-control setting-idcard_pelajar_foto_show idcard_pelajar-control">
                                                                    <option value="flex" <?= ($app->idcard_pelajar_foto_show === 'flex') ? 'selected' : '' ?>>Yes</option>
                                                                    <option value="none" <?= ($app->idcard_pelajar_foto_show === 'none') ? 'selected' : '' ?>>No</option>
                                                                </select>
                                                                <i class="form-group__bar"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label required>Nomor Induk Lokal</label>
                                                    <div class="row">
                                                        <div class="col-xs-10 col-md-6">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text" style="width: 60px;">Top</span>
                                                                </div>
                                                                <input type="number" name="idcard_pelajar_nomor_top" class="form-control mask-number setting-idcard_pelajar_nomor_top idcard_pelajar-control" maxlength="5" placeholder="Top" value="<?= (isset($app->idcard_pelajar_nomor_top)) ? $app->idcard_pelajar_nomor_top : '' ?>" />
                                                                <i class="form-group__bar"></i>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-10 col-md-6">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text" style="width: 60px;">Left</span>
                                                                </div>
                                                                <input type="number" name="idcard_pelajar_nomor_left" class="form-control mask-number setting-idcard_pelajar_nomor_left idcard_pelajar-control" maxlength="5" placeholder="Left" value="<?= (isset($app->idcard_pelajar_nomor_left)) ? $app->idcard_pelajar_nomor_left : '' ?>" />
                                                                <i class="form-group__bar"></i>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-10 col-md-6">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text" style="width: 60px;">Size</span>
                                                                </div>
                                                                <input type="number" name="idcard_pelajar_nomor_font_size" class="form-control mask-number setting-idcard_pelajar_nomor_font_size idcard_pelajar-control" maxlength="5" placeholder="Size" value="<?= (isset($app->idcard_pelajar_nomor_font_size)) ? $app->idcard_pelajar_nomor_font_size : '' ?>" />
                                                                <i class="form-group__bar"></i>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-10 col-md-6">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text" style="width: 60px;">Color</span>
                                                                </div>
                                                                <input type="text" name="idcard_pelajar_nomor_color" class="form-control setting-idcard_pelajar_nomor_color idcard_pelajar-control" placeholder="Color" value="<?= (isset($app->idcard_pelajar_nomor_color)) ? $app->idcard_pelajar_nomor_color : '' ?>" />
                                                                <i class="form-group__bar"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label required>Nama</label>
                                                    <div class="row">
                                                        <div class="col-xs-10 col-md-6">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text" style="width: 60px;">Top</span>
                                                                </div>
                                                                <input type="number" name="idcard_pelajar_nama_top" class="form-control mask-number setting-idcard_pelajar_nama_top idcard_pelajar-control" maxlength="5" placeholder="Top" value="<?= (isset($app->idcard_pelajar_nama_top)) ? $app->idcard_pelajar_nama_top : '' ?>" />
                                                                <i class="form-group__bar"></i>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-10 col-md-6">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text" style="width: 60px;">Left</span>
                                                                </div>
                                                                <input type="number" name="idcard_pelajar_nama_left" class="form-control mask-number setting-idcard_pelajar_nama_left idcard_pelajar-control" maxlength="5" placeholder="Left" value="<?= (isset($app->idcard_pelajar_nama_left)) ? $app->idcard_pelajar_nama_left : '' ?>" />
                                                                <i class="form-group__bar"></i>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-10 col-md-6">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text" style="width: 60px;">Size</span>
                                                                </div>
                                                                <input type="number" name="idcard_pelajar_nama_font_size" class="form-control mask-number setting-idcard_pelajar_nama_font_size idcard_pelajar-control" maxlength="5" placeholder="Size" value="<?= (isset($app->idcard_pelajar_nama_font_size)) ? $app->idcard_pelajar_nama_font_size : '' ?>" />
                                                                <i class="form-group__bar"></i>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-10 col-md-6">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text" style="width: 60px;">Color</span>
                                                                </div>
                                                                <input type="text" name="idcard_pelajar_nama_color" class="form-control setting-idcard_pelajar_nama_color idcard_pelajar-control" placeholder="Color" value="<?= (isset($app->idcard_pelajar_nama_color)) ? $app->idcard_pelajar_nama_color : '' ?>" />
                                                                <i class="form-group__bar"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label required>Alamat</label>
                                                    <div class="row">
                                                        <div class="col-xs-10 col-md-6">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text" style="width: 60px;">Top</span>
                                                                </div>
                                                                <input type="number" name="idcard_pelajar_alamat_top" class="form-control mask-number setting-idcard_pelajar_alamat_top idcard_pelajar-control" maxlength="5" placeholder="Top" value="<?= (isset($app->idcard_pelajar_alamat_top)) ? $app->idcard_pelajar_alamat_top : '' ?>" />
                                                                <i class="form-group__bar"></i>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-10 col-md-6">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text" style="width: 60px;">Left</span>
                                                                </div>
                                                                <input type="number" name="idcard_pelajar_alamat_left" class="form-control mask-number setting-idcard_pelajar_alamat_left idcard_pelajar-control" maxlength="5" placeholder="Left" value="<?= (isset($app->idcard_pelajar_alamat_left)) ? $app->idcard_pelajar_alamat_left : '' ?>" />
                                                                <i class="form-group__bar"></i>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-10 col-md-6">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text" style="width: 60px;">Size</span>
                                                                </div>
                                                                <input type="number" name="idcard_pelajar_alamat_font_size" class="form-control mask-number setting-idcard_pelajar_alamat_font_size idcard_pelajar-control" maxlength="5" placeholder="Size" value="<?= (isset($app->idcard_pelajar_alamat_font_size)) ? $app->idcard_pelajar_alamat_font_size : '' ?>" />
                                                                <i class="form-group__bar"></i>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-10 col-md-6">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text" style="width: 60px;">Color</span>
                                                                </div>
                                                                <input type="text" name="idcard_pelajar_alamat_color" class="form-control setting-idcard_pelajar_alamat_color idcard_pelajar-control" placeholder="Color" value="<?= (isset($app->idcard_pelajar_alamat_color)) ? $app->idcard_pelajar_alamat_color : '' ?>" />
                                                                <i class="form-group__bar"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label required>QR-Code</label>
                                                    <div class="row">
                                                        <div class="col-xs-10 col-md-6">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text" style="width: 60px;">Top</span>
                                                                </div>
                                                                <input type="number" name="idcard_pelajar_qrcode_top" class="form-control mask-number setting-idcard_pelajar_qrcode_top idcard_pelajar-control" maxlength="5" placeholder="Top" value="<?= (isset($app->idcard_pelajar_qrcode_top)) ? $app->idcard_pelajar_qrcode_top : '' ?>" />
                                                                <i class="form-group__bar"></i>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-10 col-md-6">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text" style="width: 60px;">Left</span>
                                                                </div>
                                                                <input type="number" name="idcard_pelajar_qrcode_left" class="form-control mask-number setting-idcard_pelajar_qrcode_left idcard_pelajar-control" maxlength="5" placeholder="Left" value="<?= (isset($app->idcard_pelajar_qrcode_left)) ? $app->idcard_pelajar_qrcode_left : '' ?>" />
                                                                <i class="form-group__bar"></i>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-10 col-md-6">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text" style="width: 60px;">Width</span>
                                                                </div>
                                                                <input type="number" name="idcard_pelajar_qrcode_width" class="form-control mask-number setting-idcard_pelajar_qrcode_width idcard_pelajar-control" maxlength="5" placeholder="Width" value="<?= (isset($app->idcard_pelajar_qrcode_width)) ? $app->idcard_pelajar_qrcode_width : '' ?>" />
                                                                <i class="form-group__bar"></i>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-10 col-md-6">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text" style="width: 60px;">Height</span>
                                                                </div>
                                                                <input type="number" name="idcard_pelajar_qrcode_height" class="form-control mask-number setting-idcard_pelajar_qrcode_height idcard_pelajar-control" maxlength="5" placeholder="Height" value="<?= (isset($app->idcard_pelajar_qrcode_height)) ? $app->idcard_pelajar_qrcode_height : '' ?>" />
                                                                <i class="form-group__bar"></i>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-10 col-md-6">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text" style="width: 60px;">Show</span>
                                                                </div>
                                                                <select name="idcard_pelajar_qrcode_show" class="form-control setting-idcard_pelajar_qrcode_show idcard_pelajar-control">
                                                                    <option value="flex" <?= ($app->idcard_pelajar_qrcode_show === 'flex') ? 'selected' : '' ?>>Yes</option>
                                                                    <option value="none" <?= ($app->idcard_pelajar_qrcode_show === 'none') ? 'selected' : '' ?>>No</option>
                                                                </select>
                                                                <i class="form-group__bar"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-auto">
                                                <div style="background: #252525; padding: 10px; display: flex; justify-content: center; align-items: center;">
                                                    <div id="idcard-pelajar-content" class="idcard-pelajar-preview" style="width: fit-content; width: -moz-fit-content; overflow: hidden;">
                                                        <div class="idcard-pelajar-preview-qrcode" style="width: 120px; height: 120px; display: flex; justify-content: center; align-items: center; text-align: center; background: #fff; border: 1px solid #999; position: absolute; opacity: 1;">
                                                            <img class="idcard-pelajar-preview-qrcode-thumb" src="<?= $qrcode ?>" alt="QR-Code" style="width: 100%; height: 100%; object-fit: cover;">
                                                        </div>
                                                        <div class="idcard-pelajar-preview-foto" style="width: 105px; height: 150px; display: flex; justify-content: center; align-items: center; text-align: center; background: #fff; border: 1px solid #999; position: absolute; opacity: 1;">
                                                            <img class="idcard-pelajar-preview-foto-thumb" src="<?= $profile_photo ?>" alt="FOTO" style="width: 100%; height: 100%; object-fit: cover;">
                                                        </div>
                                                        <div class="idcard-pelajar-preview-nomor" style="width: 150px; font-weight: 500; position: absolute;">
                                                            <span class="idcard-pelajar-test-label-nomor">0001</span>
                                                        </div>
                                                        <div class="idcard-pelajar-preview-nama" style="max-width: 240px; font-weight: 500; position: absolute;">
                                                            <span class="idcard-pelajar-test-label-nama">Nama Lengkap</span>
                                                        </div>
                                                        <div class="idcard-pelajar-preview-alamat" style="max-width: 240px; font-weight: 500; position: absolute;">
                                                            <span class="idcard-pelajar-test-label-alamat">Alamat</span>
                                                        </div>
                                                        <img class="idcard-pelajar-preview-image" src="<?= base_url((isset($app->idcard_pelajar_background)) ? $app->idcard_pelajar_background : '') ?>" alt="Kartu Peserta PSB Background" style="width: 500px; object-fit: contain;" />
                                                    </div>
                                                </div>
                                                <div class="mt-4 mb-4">
                                                    <button class="btn btn-light" type="button" data-toggle="collapse" data-target="#idcard-pelajar-generate" aria-expanded="false" aria-controls="idcard-pelajar-generate">
                                                        <i class="zmdi zmdi-layers"></i>
                                                        Simulation
                                                    </button>
                                                    <div class="collapse" id="idcard-pelajar-generate">
                                                        <div class="card card-body m-0 mt-2">
                                                            <div class="form-group">
                                                                <label>Nomor Induk Lokal</label>
                                                                <input type="text" name="idcard-pelajar-test-nomor" class="form-control idcard-pelajar-test-nomor idcard_pelajar-control" placeholder="No. Peserta" value="0001" />
                                                                <i class="form-group__bar"></i>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Nama Lengkap</label>
                                                                <input type="text" name="idcard-pelajar-test-nama" class="form-control idcard-pelajar-test-nama idcard_pelajar-control" placeholder="Nama Lengkap" value="NAMA LENGKAP" />
                                                                <i class="form-group__bar"></i>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Alamat</label>
                                                                <input type="text" name="idcard-pelajar-test-alamat" class="form-control idcard-pelajar-test-alamat idcard_pelajar-control" placeholder="Alamat" value="ALAMAT" />
                                                                <i class="form-group__bar"></i>
                                                            </div>
                                                            <a id="btn-download-idcard-pelajar" href="javascript:;" class="btn btn-success">
                                                                <i class="zmdi zmdi-download"></i>
                                                                Download
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endif ?>
                            <?php else : ?>
                                <div class="alert alert-warning">
                                    Kartu Pelajar only available in desktop mode.
                                </div>
                            <?php endif ?>
                        </div>
                    </div>

                    <small class="form-text text-muted">
                        Fields with red stars (<label required></label>) are required.
                    </small>

                    <div class="row" style="margin-top: 2rem;">
                        <div class="col col-md-3 col-lg-2">
                            <button class="btn btn--raised btn-primary btn--icon-text btn-block page-action-save-more spinner-action-button">
                                Save Changes
                                <div class="spinner-action"></div>
                            </button>
                        </div>
                    </div>
                </form>
            <?php else : ?>
                Sorry, no content available for your role (<?= $this->session->userdata('user')['role'] ?>).
            <?php endif ?>

        </div>
    </div>
</section>