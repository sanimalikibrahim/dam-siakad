<script type="text/javascript">
  $(document).ready(function() {

    var getCanvas;
    var idcard = $("#idcard-content");
    var idcard_pelajar = $("#idcard-pelajar-content");
    var _form_application = "form-setting-application";
    var _form_smtp = "form-setting-smtp";
    var _form_account = "form-setting-account";
    var _form_dashboard = "form-setting-dashboard_image";
    var _form_more = "form-setting-more";

    // Inject state
    _isLockDaftarUlang = false;

    // Handle ajax stop
    $(document).ajaxStop(function() {
      $(document).find(".body-loading").fadeOut("fast", function() {
        $(this).hide();
        document.body.style.overflow = "auto";
      });
    });

    // Call dashboard_preview
    dashboard_preview();

    // Call idcard_psb_preview
    idcard_psb_preview();

    // Call idcard_pelajar_preview
    idcard_pelajar_preview();

    // Handle data submit Application
    $("#" + _form_application + " .page-action-save-application").on("click", function(e) {
      e.preventDefault();
      tinyMCE.triggerSave();

      var form = $("#" + _form_application)[0];
      var data = new FormData(form);

      $.ajax({
        type: "post",
        url: "<?php echo base_url('setting/ajax_save_application/') ?>",
        data: data,
        dataType: "json",
        enctype: "multipart/form-data",
        processData: false,
        contentType: false,
        cache: false,
        success: function(response) {
          if (response.status === true) {
            notify(response.data, "success");
            window.location.href = "<?php echo base_url('setting/application') ?>";
          } else {
            notify(response.data, "danger");
          };
        }
      });
      return false;
    });

    // Handle test SMTP
    $("#" + _form_smtp + " .page-action-test-smtp").on("click", function(e) {
      e.preventDefault();

      $.ajax({
        type: "get",
        url: "<?php echo base_url('setting/ajax_test_smtp/') ?>",
        dataType: "json",
        success: function(response) {
          if (response.status === true) {
            notify(response.data, "success");
          } else {
            notify(response.data, "danger");
          };
        }
      });
      return false;
    });

    // Handle data submit SMTP
    $("#" + _form_smtp + " .page-action-save-smtp").on("click", function(e) {
      e.preventDefault();
      tinyMCE.triggerSave();

      var form = $("#" + _form_smtp)[0];
      var data = new FormData(form);

      $.ajax({
        type: "post",
        url: "<?php echo base_url('setting/ajax_save_smtp/') ?>",
        data: data,
        dataType: "json",
        enctype: "multipart/form-data",
        processData: false,
        contentType: false,
        cache: false,
        success: function(response) {
          if (response.status === true) {
            notify(response.data, "success");
          } else {
            notify(response.data, "danger");
          };
        }
      });
      return false;
    });

    // Handle data submit Account
    $("#" + _form_account + " .page-action-save-account").on("click", function(e) {
      e.preventDefault();
      tinyMCE.triggerSave();

      var form = $("#" + _form_account)[0];
      var data = new FormData(form);

      // Show loading indicator
      $(document).find(".body-loading").fadeIn("fast", function() {
        $(this).show();
        document.body.style.overflow = "hidden";
      });

      $.ajax({
        type: "post",
        url: "<?php echo base_url('setting/ajax_save_account/') ?>",
        data: data,
        dataType: "json",
        enctype: "multipart/form-data",
        processData: false,
        contentType: false,
        cache: false,
        success: function(response) {
          if (response.status === true) {
            swal({
              title: "Success",
              text: response.data,
              type: "success",
              showCancelButton: false,
              confirmButtonColor: '#39bbb0',
              confirmButtonText: "OK",
              closeOnConfirm: false
            }).then((result) => {
              window.location.href = "";
            });
          } else {
            notify(response.data, "danger");
          };
        }
      });
      return false;
    });

    // Handle data submit Dashboard Image
    $("#" + _form_dashboard + " .page-action-save-dashboard_image").on("click", function(e) {
      e.preventDefault();
      var form = $("#" + _form_dashboard)[0];
      var data = new FormData(form);

      $.ajax({
        type: "post",
        url: "<?php echo base_url('setting/ajax_save_dashboard/') ?>",
        data: data,
        dataType: "json",
        enctype: "multipart/form-data",
        processData: false,
        contentType: false,
        cache: false,
        success: function(response) {
          if (response.status === true) {
            notify(response.data, "success");
          } else {
            notify(response.data, "danger");
          };
        }
      });
      return false;
    });

    // Handle data submit More
    $("#" + _form_more + " .page-action-save-more").on("click", function(e) {
      e.preventDefault();
      tinyMCE.triggerSave();

      var form = $("#" + _form_more)[0];
      var data = new FormData(form);

      $.ajax({
        type: "post",
        url: "<?php echo base_url('setting/ajax_save_more/') ?>",
        data: data,
        dataType: "json",
        enctype: "multipart/form-data",
        processData: false,
        contentType: false,
        cache: false,
        success: function(response) {
          if (response.status === true) {
            notify(response.data, "success");
          } else {
            notify(response.data, "danger");
          };
        }
      });
      return false;
    });

    // Handle Dashboard Image changed
    $("#" + _form_dashboard + " .setting-dashboard_image").on("keyup", function() {
      dashboard_preview();
    });
    $("#" + _form_dashboard + " .setting-dashboard_image").on("change", function() {
      dashboard_preview();
    });

    // Handle Dashboard Image upload
    $(".setting-dashboard_image_source").change(function() {
      dashboard_get_preview(this);
    });

    // Handle Dashboard Image preview
    function dashboard_preview() {
      var _dashboard_image_width = $(".setting-dashboard_image_width").val();
      var _dashboard_image_max_height = $(".setting-dashboard_image_max_height").val();
      var _dashboard_image_object_fit = $(".setting-dashboard_image_object_fit").val();
      var _dashboard_image_object_position = $(".setting-dashboard_image_object_position").val();
      var _dashboard_image_box_shadow = $(".setting-dashboard_image_box_shadow").val();
      _dashboard_image_box_shadow = (_dashboard_image_box_shadow == "1") ? "0 1px 2px rgba(0, 0, 0, 0.1)" : "none";

      $(".setting-preview-dashboard_image").css("width", _dashboard_image_width + "%");
      $(".setting-preview-dashboard_image").css("max-height", _dashboard_image_max_height + "px");
      $(".setting-preview-dashboard_image").css("object-fit", _dashboard_image_object_fit);
      $(".setting-preview-dashboard_image").css("object-position", _dashboard_image_object_position);
      $(".setting-preview-dashboard_image").css("box-shadow", _dashboard_image_box_shadow);
    };

    // Handle Dashboard Image choose file preview
    function dashboard_get_preview(input) {
      $('.setting-preview-dashboard_image').html("");

      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
          if (e.target.result != "") {
            $('.setting-preview-dashboard_image').attr("src", e.target.result);
          };
        };
        reader.readAsDataURL(input.files[0]);
      };
    };

    // Handle Kartu Peserta PSB changed
    $("#" + _form_more + " .idcard_psb-control").on("keyup", function() {
      idcard_psb_preview();
    });
    $("#" + _form_more + " .idcard_psb-control").on("change", function() {
      idcard_psb_preview();
    });

    // Handle Kartu Peserta PSB upload
    $(".setting-idcard_psb_background").change(function() {
      idcard_psb_get_preview(this);
    });

    // Handle download idcard
    $("#btn-download-idcard").on("click", function(e) {
      e.preventDefault();

      html2canvas(idcard, {
        onrendered: function(canvas) {
          const imgageData = canvas.toDataURL("image/png");
          const newData = imgageData.replace(/^data:image\/png/, "data:application/octet-stream");
          const idcard_test_nomor = $(".idcard-test-nomor").val();

          var link = document.createElement("a");
          link.setAttribute("href", newData);
          link.setAttribute("download", "DA_Kartu_Peserta_" + idcard_test_nomor + ".png");
          link.click();
        }
      });
    });

    // Handle Kartu Peserta PSB choose file preview
    function idcard_psb_get_preview(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
          if (e.target.result != "") {
            $('.idcard-preview-image').attr("src", e.target.result);
          };
        };
        reader.readAsDataURL(input.files[0]);
      };
    };

    // Handle Kartu Peserta PSB preview
    function idcard_psb_preview() {
      const idcard_psb_foto_top = $(".setting-idcard_psb_foto_top").val();
      const idcard_psb_foto_left = $(".setting-idcard_psb_foto_left").val();
      const idcard_psb_foto_show = $(".setting-idcard_psb_foto_show").val();
      const idcard_psb_foto_width = $(".setting-idcard_psb_foto_width").val();
      const idcard_psb_foto_height = $(".setting-idcard_psb_foto_height").val();
      const idcard_psb_nomor_top = $(".setting-idcard_psb_nomor_top").val();
      const idcard_psb_nomor_left = $(".setting-idcard_psb_nomor_left").val();
      const idcard_psb_nomor_font_size = $(".setting-idcard_psb_nomor_font_size").val();
      const idcard_psb_nomor_color = $(".setting-idcard_psb_nomor_color").val();
      const idcard_psb_nama_top = $(".setting-idcard_psb_nama_top").val();
      const idcard_psb_nama_left = $(".setting-idcard_psb_nama_left").val();
      const idcard_psb_nama_font_size = $(".setting-idcard_psb_nama_font_size").val();
      const idcard_psb_nama_color = $(".setting-idcard_psb_nama_color").val();
      const idcard_psb_alamat_top = $(".setting-idcard_psb_alamat_top").val();
      const idcard_psb_alamat_left = $(".setting-idcard_psb_alamat_left").val();
      const idcard_psb_alamat_font_size = $(".setting-idcard_psb_alamat_font_size").val();
      const idcard_psb_alamat_color = $(".setting-idcard_psb_alamat_color").val();
      const idcard_psb_ruangan1_top = $(".setting-idcard_psb_ruangan1_top").val();
      const idcard_psb_ruangan1_left = $(".setting-idcard_psb_ruangan1_left").val();
      const idcard_psb_ruangan1_font_size = $(".setting-idcard_psb_ruangan1_font_size").val();
      const idcard_psb_ruangan1_color = $(".setting-idcard_psb_ruangan1_color").val();
      const idcard_psb_ruangan2_top = $(".setting-idcard_psb_ruangan2_top").val();
      const idcard_psb_ruangan2_left = $(".setting-idcard_psb_ruangan2_left").val();
      const idcard_psb_ruangan2_font_size = $(".setting-idcard_psb_ruangan2_font_size").val();
      const idcard_psb_ruangan2_color = $(".setting-idcard_psb_ruangan2_color").val();
      const idcard_test_nomor = $(".idcard-test-nomor").val();
      const idcard_test_nama = $(".idcard-test-nama").val();
      const idcard_test_alamat = $(".idcard-test-alamat").val();
      const idcard_test_ruangan1 = $(".idcard-test-ruangan1").val();
      const idcard_test_ruangan2 = $(".idcard-test-ruangan2").val();
      const idcard_preview_foto = $(".idcard-preview-foto");
      const idcard_preview_foto_thumb = $(".idcard-preview-foto-thumb");
      const idcard_preview_nomor = $(".idcard-preview-nomor");
      const idcard_preview_nama = $(".idcard-preview-nama");
      const idcard_preview_alamat = $(".idcard-preview-alamat");
      const idcard_preview_ruangan1 = $(".idcard-preview-ruangan1");
      const idcard_preview_ruangan2 = $(".idcard-preview-ruangan2");
      const idcard_test_label_nomor = $(".idcard-test-label-nomor");
      const idcard_test_label_nama = $(".idcard-test-label-nama");
      const idcard_test_label_alamat = $(".idcard-test-label-alamat");
      const idcard_test_label_ruangan1 = $(".idcard-test-label-ruangan1");
      const idcard_test_label_ruangan2 = $(".idcard-test-label-ruangan2");

      idcard_preview_foto.css("margin-top", idcard_psb_foto_top + "px");
      idcard_preview_foto.css("margin-left", idcard_psb_foto_left + "px");
      idcard_preview_foto.css("display", idcard_psb_foto_show);
      idcard_preview_foto.css("width", idcard_psb_foto_width + "px");
      idcard_preview_foto.css("height", idcard_psb_foto_height + "px");
      idcard_preview_nomor.css("margin-top", idcard_psb_nomor_top + "px");
      idcard_preview_nomor.css("margin-left", idcard_psb_nomor_left + "px");
      idcard_preview_nomor.css("font-size", idcard_psb_nomor_font_size + "px");
      idcard_preview_nomor.css("color", idcard_psb_nomor_color);
      idcard_preview_nama.css("margin-top", idcard_psb_nama_top + "px");
      idcard_preview_nama.css("margin-left", idcard_psb_nama_left + "px");
      idcard_preview_nama.css("font-size", idcard_psb_nama_font_size + "px");
      idcard_preview_nama.css("color", idcard_psb_nama_color);
      idcard_preview_alamat.css("margin-top", idcard_psb_alamat_top + "px");
      idcard_preview_alamat.css("margin-left", idcard_psb_alamat_left + "px");
      idcard_preview_alamat.css("font-size", idcard_psb_alamat_font_size + "px");
      idcard_preview_alamat.css("color", idcard_psb_alamat_color);
      idcard_preview_ruangan1.css("margin-top", idcard_psb_ruangan1_top + "px");
      idcard_preview_ruangan1.css("margin-left", idcard_psb_ruangan1_left + "px");
      idcard_preview_ruangan1.css("font-size", idcard_psb_ruangan1_font_size + "px");
      idcard_preview_ruangan1.css("color", idcard_psb_ruangan1_color);
      idcard_preview_ruangan2.css("margin-top", idcard_psb_ruangan2_top + "px");
      idcard_preview_ruangan2.css("margin-left", idcard_psb_ruangan2_left + "px");
      idcard_preview_ruangan2.css("font-size", idcard_psb_ruangan2_font_size + "px");
      idcard_preview_ruangan2.css("color", idcard_psb_ruangan2_color);

      idcard_test_label_nomor.html(idcard_test_nomor);
      idcard_test_label_nama.html(idcard_test_nama);
      idcard_test_label_alamat.html(idcard_test_alamat);
      idcard_test_label_ruangan1.html(idcard_test_ruangan1);
      idcard_test_label_ruangan2.html(idcard_test_ruangan2);
    };

    // Handle upload
    $(document.body).on("change", ".user-profile_photo", function() {
      readUploadInlineURL(this);
    });

    // KARTU PELAJAR
    // Handle Kartu Peserta PSB changed
    $("#" + _form_more + " .idcard_pelajar-control").on("keyup", function() {
      idcard_pelajar_preview();
    });
    $("#" + _form_more + " .idcard_pelajar-control").on("change", function() {
      idcard_pelajar_preview();
    });

    // Handle Kartu Peserta PSB upload
    $(".setting-idcard_pelajar_background").change(function() {
      idcard_pelajar_get_preview(this);
    });

    // Handle download idcard
    $("#btn-download-idcard-pelajar").on("click", function(e) {
      e.preventDefault();

      html2canvas(idcard_pelajar, {
        onrendered: function(canvas) {
          const imgageData = canvas.toDataURL("image/png");
          const newData = imgageData.replace(/^data:image\/png/, "data:application/octet-stream");
          const idcard_test_nomor = $(".idcard-pelajar-test-nomor").val();

          var link = document.createElement("a");
          link.setAttribute("href", newData);
          link.setAttribute("download", "DA_Kartu_Pelajar_" + idcard_test_nomor + ".png");
          link.click();
        }
      });
    });

    // Handle Kartu Peserta PSB choose file preview
    function idcard_pelajar_get_preview(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
          if (e.target.result != "") {
            $('.idcard-pelajar-preview-image').attr("src", e.target.result);
          };
        };
        reader.readAsDataURL(input.files[0]);
      };
    };

    // Handle Kartu Peserta PSB preview
    function idcard_pelajar_preview() {
      const idcard_pelajar_qrcode_top = $(".setting-idcard_pelajar_qrcode_top").val();
      const idcard_pelajar_qrcode_left = $(".setting-idcard_pelajar_qrcode_left").val();
      const idcard_pelajar_qrcode_show = $(".setting-idcard_pelajar_qrcode_show").val();
      const idcard_pelajar_qrcode_width = $(".setting-idcard_pelajar_qrcode_width").val();
      const idcard_pelajar_qrcode_height = $(".setting-idcard_pelajar_qrcode_height").val();
      const idcard_pelajar_foto_top = $(".setting-idcard_pelajar_foto_top").val();
      const idcard_pelajar_foto_left = $(".setting-idcard_pelajar_foto_left").val();
      const idcard_pelajar_foto_show = $(".setting-idcard_pelajar_foto_show").val();
      const idcard_pelajar_foto_width = $(".setting-idcard_pelajar_foto_width").val();
      const idcard_pelajar_foto_height = $(".setting-idcard_pelajar_foto_height").val();
      const idcard_pelajar_nomor_top = $(".setting-idcard_pelajar_nomor_top").val();
      const idcard_pelajar_nomor_left = $(".setting-idcard_pelajar_nomor_left").val();
      const idcard_pelajar_nomor_font_size = $(".setting-idcard_pelajar_nomor_font_size").val();
      const idcard_pelajar_nomor_color = $(".setting-idcard_pelajar_nomor_color").val();
      const idcard_pelajar_nama_top = $(".setting-idcard_pelajar_nama_top").val();
      const idcard_pelajar_nama_left = $(".setting-idcard_pelajar_nama_left").val();
      const idcard_pelajar_nama_font_size = $(".setting-idcard_pelajar_nama_font_size").val();
      const idcard_pelajar_nama_color = $(".setting-idcard_pelajar_nama_color").val();
      const idcard_pelajar_alamat_top = $(".setting-idcard_pelajar_alamat_top").val();
      const idcard_pelajar_alamat_left = $(".setting-idcard_pelajar_alamat_left").val();
      const idcard_pelajar_alamat_font_size = $(".setting-idcard_pelajar_alamat_font_size").val();
      const idcard_pelajar_alamat_color = $(".setting-idcard_pelajar_alamat_color").val();
      const idcard_test_nomor = $(".idcard-pelajar-test-nomor").val();
      const idcard_test_nama = $(".idcard-pelajar-test-nama").val();
      const idcard_test_alamat = $(".idcard-pelajar-test-alamat").val();
      const idcard_preview_qrcode = $(".idcard-pelajar-preview-qrcode");
      const idcard_preview_qrcode_thumb = $(".idcard-pelajar-preview-qrcode-thumb");
      const idcard_preview_foto = $(".idcard-pelajar-preview-foto");
      const idcard_preview_foto_thumb = $(".idcard-pelajar-preview-foto-thumb");
      const idcard_preview_nomor = $(".idcard-pelajar-preview-nomor");
      const idcard_preview_nama = $(".idcard-pelajar-preview-nama");
      const idcard_preview_alamat = $(".idcard-pelajar-preview-alamat");
      const idcard_test_label_nomor = $(".idcard-pelajar-test-label-nomor");
      const idcard_test_label_nama = $(".idcard-pelajar-test-label-nama");
      const idcard_test_label_alamat = $(".idcard-pelajar-test-label-alamat");

      idcard_preview_qrcode.css("margin-top", idcard_pelajar_qrcode_top + "px");
      idcard_preview_qrcode.css("margin-left", idcard_pelajar_qrcode_left + "px");
      idcard_preview_qrcode.css("display", idcard_pelajar_qrcode_show);
      idcard_preview_qrcode.css("width", idcard_pelajar_qrcode_width + "px");
      idcard_preview_qrcode.css("height", idcard_pelajar_qrcode_height + "px");
      idcard_preview_foto.css("margin-top", idcard_pelajar_foto_top + "px");
      idcard_preview_foto.css("margin-left", idcard_pelajar_foto_left + "px");
      idcard_preview_foto.css("display", idcard_pelajar_foto_show);
      idcard_preview_foto.css("width", idcard_pelajar_foto_width + "px");
      idcard_preview_foto.css("height", idcard_pelajar_foto_height + "px");
      idcard_preview_nomor.css("margin-top", idcard_pelajar_nomor_top + "px");
      idcard_preview_nomor.css("margin-left", idcard_pelajar_nomor_left + "px");
      idcard_preview_nomor.css("font-size", idcard_pelajar_nomor_font_size + "px");
      idcard_preview_nomor.css("color", idcard_pelajar_nomor_color);
      idcard_preview_nama.css("margin-top", idcard_pelajar_nama_top + "px");
      idcard_preview_nama.css("margin-left", idcard_pelajar_nama_left + "px");
      idcard_preview_nama.css("font-size", idcard_pelajar_nama_font_size + "px");
      idcard_preview_nama.css("color", idcard_pelajar_nama_color);
      idcard_preview_alamat.css("margin-top", idcard_pelajar_alamat_top + "px");
      idcard_preview_alamat.css("margin-left", idcard_pelajar_alamat_left + "px");
      idcard_preview_alamat.css("font-size", idcard_pelajar_alamat_font_size + "px");
      idcard_preview_alamat.css("color", idcard_pelajar_alamat_color);

      idcard_test_label_nomor.html(idcard_test_nomor);
      idcard_test_label_nama.html(idcard_test_nama);
      idcard_test_label_alamat.html(idcard_test_alamat);
    };
    // END ## KARTU PELAJAR

  });
</script>