<script type="text/javascript">
  $(document).ready(function() {

    var _key = "";
    var _section = "matapelajaran";
    var _table = "table-matapelajaran";
    var _modal = "modal-form-matapelajaran";
    var _form = "form-matapelajaran";
    var _isFetchLesson = false;

    // Initialize DataTables: Index
    if ($("#" + _table)[0]) {
      var table_matapelajaran = $("#" + _table).DataTable({
        processing: true,
        serverSide: true,
        ajax: {
          url: "<?php echo base_url('matapelajaran/ajax_get_all/') ?>",
          type: "get"
        },
        columns: [{
            data: null,
            render: function(data, type, row, meta) {
              return meta.row + meta.settings._iDisplayStart + 1;
            }
          },
          {
            data: "jenis"
          },
          {
            data: "kode"
          },
          {
            data: "nama"
          },
          {
            data: "learndash_lesson_name"
          },
          {
            data: "created_at"
          },
          {
            data: "updated_at"
          },
          {
            data: null,
            className: "center",
            defaultContent: '<div class="action">' +
              '<a href="javascript:;" class="btn btn-sm btn-light btn-table-action action-edit" data-toggle="modal" data-target="#' + _modal + '"><i class="zmdi zmdi-edit"></i> Edit</a>&nbsp;' +
              '<a href="javascript:;" class="btn btn-sm btn-light btn-table-action action-delete"><i class="zmdi zmdi-delete"></i> Delete</a>' +
              '</div>'
          }
        ],
        autoWidth: !1,
        responsive: {
          details: {
            renderer: function(api, rowIdx, columns) {
              var hideColumn = [];
              var data = $.map(columns, function(col, i) {
                return ($.inArray(col.columnIndex, hideColumn)) ?
                  '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                  '<td class="dt-details-td">' + col.title + ':' + '</td> ' +
                  '<td class="dt-details-td">' + col.data + '</td>' +
                  '</tr>' :
                  '';
              }).join('');

              return data ? $('<table/>').append(data) : false;
            },
            type: "inline",
            target: 'tr',
          }
        },
        columnDefs: [{
          className: 'desktop',
          targets: [0, 1, 2, 3, 4, 5, 6, 7]
        }, {
          className: 'tablet',
          targets: [0, 1, 2, 3]
        }, {
          className: 'mobile',
          targets: [0, 3]
        }, {
          responsivePriority: 2,
          targets: -1
        }],
        pageLength: 15,
        language: {
          searchPlaceholder: "Search...",
          sProcessing: '<div style="text-align: center;"><div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div></div>'
        },
        sDom: '<"dataTables_ct"><"dataTables__top"fb>rt<"dataTables__bottom"ip><"clear">',
        buttons: [{
          extend: "excelHtml5",
          title: "Export Result"
        }, {
          extend: "print",
          title: "Export Result"
        }],
        initComplete: function(a, b) {
          $(this).closest(".dataTables_wrapper").find(".dataTables__top").prepend(
            '<div class="dataTables_buttons hidden-sm-down actions">' +
            '<span class="actions__item zmdi zmdi-refresh" data-table-action="reload" title="Reload" />' +
            '</div>'
          );
        },
      });

      $(".dataTables_filter input[type=search]").focus(function() {
        $(this).closest(".dataTables_filter").addClass("dataTables_filter--toggled")
      });

      $(".dataTables_filter input[type=search]").blur(function() {
        $(this).closest(".dataTables_filter").removeClass("dataTables_filter--toggled")
      });

      $("body").on("click", "[data-table-action]", function(a) {
        a.preventDefault();
        var b = $(this).data("table-action");
        if ("reload" === b) {
          $("#" + _table).DataTable().ajax.reload(null, false);
        };
      });
    };

    // Handle data add
    $("#" + _section).on("click", "button." + _section + "-action-add", function(e) {
      e.preventDefault();
      resetForm();

      if (_isFetchLesson === false) {
        fetchLessonList();
      };
    });

    // Handle data edit
    $("#" + _table).on("click", "a.action-edit", function(e) {
      e.preventDefault();
      resetForm();
      var temp = table_matapelajaran.row($(this).closest('tr')).data();

      // Set key for update params, important!
      _key = temp.id;

      $("#" + _form + " ." + _section + "-jenis").val(temp.jenis).trigger('input');
      $("#" + _form + " ." + _section + "-kode").val(temp.kode).trigger('input');
      $("#" + _form + " ." + _section + "-nama").val(temp.nama).trigger('input');
      $("#" + _form + " ." + _section + "-learndash_lesson_name").val(temp.learndash_lesson_name).trigger('input');

      if (_isFetchLesson === true) {
        $("#" + _form + " ." + _section + "-learndash_lesson_id").val(temp.learndash_lesson_id).trigger('change');
      } else {
        fetchLessonList().then(() => {
          $("#" + _form + " ." + _section + "-learndash_lesson_id").val(temp.learndash_lesson_id).trigger('change');
        });
      };
    });

    // Handle data submit
    $("#" + _modal + " ." + _section + "-action-save").on("click", function(e) {
      e.preventDefault();
      $.ajax({
        type: "post",
        url: "<?php echo base_url('matapelajaran/ajax_save/') ?>" + _key,
        data: $("#" + _form).serialize(),
        success: function(response) {
          var response = JSON.parse(response);
          if (response.status === true) {
            resetForm();
            $("#" + _modal).modal("hide");
            $("#" + _table).DataTable().ajax.reload(null, false);
            notify(response.data, "success");
          } else {
            notify(response.data, "danger");
          };
        }
      });
    });

    // Handle data delete
    $("#" + _table).on("click", "a.action-delete", function(e) {
      e.preventDefault();
      var temp = table_matapelajaran.row($(this).closest('tr')).data();

      swal({
        title: "Are you sure to delete?",
        text: "Once deleted, you will not be able to recover this data!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false
      }).then((result) => {
        if (result.value) {
          $.ajax({
            type: "delete",
            url: "<?php echo base_url('matapelajaran/ajax_delete/') ?>" + temp.id,
            dataType: "json",
            success: function(response) {
              if (response.status) {
                $("#" + _table).DataTable().ajax.reload(null, false);
                notify(response.data, "success");
              } else {
                notify(response.data, "danger");
              };
            }
          });
        };
      });
    });

    // Handle learndash ID select
    $("." + _section + "-learndash_lesson_id").on("select2:select", function(e) {
      var data = $("." + _section + "-learndash_lesson_id").select2('data');
      $("." + _section + "-learndash_lesson_name").val(data[0].text);
    });

    // Handle form reset
    resetForm = () => {
      _key = "";
      $("#" + _form).trigger("reset");
      $("." + _section + "-learndash_lesson_id").val(null).trigger("change");
    };

    // Handle fetch lesson
    async function fetchLessonList() {
      await $.ajax({
        type: "get",
        url: "<?php echo base_url('matapelajaran/ajax_get_lesson/') ?>",
        success: function(response) {
          var response = JSON.parse(response);
          if (response.status === true) {
            var data = response.data;

            if (data.length > 0) {
              data.map((value, index) => {
                var newOption = new Option(value.id_nama, value.id, false, false);
                $("." + _section + "-learndash_lesson_id").append(newOption).trigger("change");
              });

              $("." + _section + "-learndash_lesson_id").val(null).trigger("change");
              _isFetchLesson = true;
            };
          } else {
            notify(response.data, "danger");
          };
        }
      });
    };

    // Handle is json object
    function isJson(object) {
      try {
        return $.parseJSON(object);
      } catch (err) {
        return false;
      };
    };

  });
</script>