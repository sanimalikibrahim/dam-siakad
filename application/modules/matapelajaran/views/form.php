<div class="modal fade" id="modal-form-matapelajaran" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title pull-left">Mata Pelajaran</h5>
      </div>
      <div class="spinner">
        <div class="lds-hourglass"></div>
      </div>
      <div class="modal-body">
        <form id="form-matapelajaran" autocomplete="off">
          <!-- CSRF -->
          <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />

          <div class="form-group">
            <label required>Jenis</label>
            <div class="select">
              <select name="jenis" class="form-control matapelajaran-jenis" data-placeholder="Select &#8595;" required>
                <option disabled selected>Select &#8595;</option>
                <option value="Umum">Umum</option>
                <option value="Pesantren">Pesantren</option>
              </select>
              <i class="form-group__bar"></i>
            </div>
          </div>
          <div class="form-group">
            <label required>Kode</label>
            <input type="text" name="kode" class="form-control matapelajaran-kode" maxlength="50" placeholder="Kode" required />
            <i class="form-group__bar"></i>
          </div>
          <div class="form-group">
            <label required>Nama</label>
            <input type="text" name="nama" class="form-control matapelajaran-nama" maxlength="150" placeholder="Nama" required />
            <i class="form-group__bar"></i>
          </div>
          <div class="form-group">
            <label>Learndash ID</label>
            <div class="select">
              <select name="learndash_lesson_id" class="form-control select2 matapelajaran-learndash_lesson_id" data-placeholder="Select &#8595;" required>
                <!-- ajax load -->
              </select>
              <input type="hidden" name="learndash_lesson_name" class="matapelajaran-learndash_lesson_name" readonly />
              <i class="form-group__bar"></i>
            </div>
          </div>

          <small class=" form-text text-muted">
            Fields with red stars (<label required></label>) are required.
          </small>

        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success btn--icon-text matapelajaran-action-save">
          <i class="zmdi zmdi-save"></i> Save
        </button>
        <button type="button" class="btn btn-light btn--icon-text matapelajaran-action-cancel" data-dismiss="modal">
          Cancel
        </button>
      </div>
    </div>
  </div>
</div>