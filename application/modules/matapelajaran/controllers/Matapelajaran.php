<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . 'controllers/AppBackend.php');
require_once(APPPATH . 'controllers/ApiClient.php');

class Matapelajaran extends AppBackend
{
  function __construct()
  {
    parent::__construct();
    $this->load->model([
      'AppModel',
      'MataPelajaranModel'
    ]);
    $this->load->library('form_validation');
  }

  public function index()
  {
    $data = array(
      'app' => $this->app(),
      'main_js' => $this->load_main_js('matapelajaran'),
      'card_title' => 'Mata Pelajaran'
    );
    $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
    $this->template->load_view('index', $data, TRUE);
    $this->template->render();
  }

  public function ajax_get_all()
  {
    $this->handle_ajax_request();
    $dtAjax_config = array(
      'table_name' => 'mata_pelajaran',
      'order_column' => 5,
      'order_column_dir' => 'desc'
    );
    $response = $this->AppModel->getData_dtAjax($dtAjax_config);
    echo json_encode($response);
  }

  public function ajax_save($id = null)
  {
    $this->handle_ajax_request();
    $this->form_validation->set_rules($this->MataPelajaranModel->rules());

    if ($this->form_validation->run() === true) {
      if (is_null($id)) {
        echo json_encode($this->MataPelajaranModel->insert());
      } else {
        echo json_encode($this->MataPelajaranModel->update($id));
      };
    } else {
      $errors = validation_errors('<div>- ', '</div>');
      echo json_encode(array('status' => false, 'data' => $errors));
    };
  }

  public function ajax_delete($id)
  {
    $this->handle_ajax_request();
    echo json_encode($this->MataPelajaranModel->delete($id));
  }

  public function ajax_get_lesson()
  {
    $this->handle_ajax_request();

    $apiClient = new ApiClient();
    $response = $apiClient->getListLesson(100);

    if ($response['status'] === true) {
      $temp = $response['data'];
      $data = [];

      if (count($temp) > 0) {
        foreach ($temp as $index => $item) {
          $data[] = (object) [
            'id' => $item->id,
            'nama' => $item->title->rendered,
            'id_nama' => $item->id . ' | ' . $item->title->rendered
          ];
        };
      };

      $response = array('status' => true, 'data' => $data);
    } else {
      $response = array('status' => false, 'data' => 'Terjadi kesalahan ketika mengambil data pelajaran ke LMS.');
    };

    echo json_encode($response);
  }
}
