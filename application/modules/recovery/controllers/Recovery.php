<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . 'controllers/AppBackend.php');
require_once(APPPATH . 'controllers/ApiClient.php');

class Recovery extends AppBackend
{
  function __construct()
  {
    parent::__construct();
    $this->load->model([
      'RecoveryModel',
      'UserModel'
    ]);
    $this->load->library('form_validation');
  }

  private function _updateUserPasswordLearndash($id, $password)
  {
    $apiClient = new ApiClient();
    $response = $apiClient->updateUserPassword($id, $password);

    return $response;
  }

  private function _sendMail($email = null, $token = null)
  {
    $app = $this->app();
    $template = $this->load->view('mail_template', array(
      'link' => base_url('recovery?token=' . $token)
    ), true);

    $mailParams = array(
      'receiver' => $email,
      'subject' => 'Peringatan Keamanan | ' . $app->app_name,
      'message' => $template
    );

    return $this->sendMail($mailParams);
  }

  public function index()
  {
    $token = $this->input->get('token');
    $isValidToken = false;
    $viewToLoad = 'index';
    $user = null;

    if (!empty($token) && !is_null($token)) {
      $viewToLoad = 'reset_form';
      $user = $this->RecoveryModel->checkToken($token);

      if ($user !== false) {
        $isValidToken = true;
      };
    };

    $data = array(
      'app' => $this->app(),
      'main_js' => $this->load_main_js('recovery'),
      'page_title' => 'Recovery | ' . $this->app()->app_name,
      'token' => $token,
      'isValidToken' => $isValidToken,
      'user' => $user
    );
    $this->load->view($viewToLoad, $data);
  }

  public function ajax_submit()
  {
    $this->handle_ajax_request();
    $this->form_validation->set_rules($this->RecoveryModel->rules());

    if ($this->form_validation->run() === true) {
      $username = $this->input->post('username');
      $temp = $this->RecoveryModel->getDetail($username);

      if (!is_null($temp)) {
        $token = sha1($this->generateRandom() . date('YmdHis'));
        $setToken = $this->RecoveryModel->setToken($temp->id, $token);

        if ($setToken['status'] === true) {
          $sendMail = $this->_sendMail($temp->email, $token);

          if ($sendMail['status'] === true) {
            echo json_encode(array('status' => true, 'data' => 'Successfully send your email, please check inbox/spam folder.'));
          } else {
            echo json_encode($sendMail);
          };
        } else {
          echo json_encode($setToken);
        };
      } else {
        echo json_encode(array('status' => false, 'data' => 'Sorry, the NISN / Email you entered was not found.'));
      };
    } else {
      $errors = validation_errors('<div>- ', '</div>');
      echo json_encode(array('status' => false, 'data' => $errors));
    };
  }

  public function ajax_submit_reset()
  {
    $this->handle_ajax_request();
    $this->form_validation->set_rules($this->RecoveryModel->rulesReset());

    if ($this->form_validation->run() === true) {
      $token = $this->input->post('token');
      $password = $this->input->post('password');

      if (!empty($token) && !is_null($token)) {
        $user = $this->UserModel->getDetail(['token' => $token]);
        $setPassword = $this->RecoveryModel->setPassword($token, $password);

        if ($setPassword['status'] === true) {
          // Update user in learndash-wp
          if (in_array($user->role, ['Santri', 'Calon Santri'])) {
            $learndashUserId = $user->learndash_user_id;
            $this->_updateUserPasswordLearndash($learndashUserId, $password);
          };

          echo json_encode($setPassword);
        } else {
          echo json_encode($setPassword);
        };
      } else {
        echo json_encode(array('status' => false, 'data' => 'Missing token parameter, try again later.'));
      };
    } else {
      $errors = validation_errors('<div>- ', '</div>');
      echo json_encode(array('status' => false, 'data' => $errors));
    };
  }
}
