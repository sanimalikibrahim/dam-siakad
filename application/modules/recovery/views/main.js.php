<script type="text/javascript">
  $(document).ready(function() {

    var _form = "form-recovery";
    var _form_reset = "form-reset";

    // Handle ajax start
    $(document).ajaxStart(function() {
      $(".spinner").css("display", "flex");
    });

    // Handle ajax stop
    $(document).ajaxStop(function() {
      $(".spinner").hide();
    });

    // Handle data submit
    $("#" + _form + " .page-action-recovery").on("click", function(e) {
      e.preventDefault();

      var form = $("#" + _form)[0];
      var data = new FormData(form);

      // Hide alert
      $(document).find(".recovery-alert").hide();

      $.ajax({
        type: "post",
        url: "<?php echo base_url('recovery/ajax_submit/') ?>",
        data: data,
        dataType: "json",
        enctype: "multipart/form-data",
        processData: false,
        contentType: false,
        cache: false,
        success: function(response) {
          if (response.status === true) {
            $(document).find(".recovery-alert").show();
          } else {
            notify(response.data, "danger");
            $("#" + _form + " .recovery-username").focus();
          };
        }
      });
      return false;
    });

    // Handle data submit: reset
    $("#" + _form_reset + " .page-action-reset").on("click", function(e) {
      e.preventDefault();

      var form = $("#" + _form_reset)[0];
      var data = new FormData(form);

      // Hide alert
      $(document).find(".reset-alert").hide();

      $.ajax({
        type: "post",
        url: "<?php echo base_url('recovery/ajax_submit_reset/') ?>",
        data: data,
        dataType: "json",
        enctype: "multipart/form-data",
        processData: false,
        contentType: false,
        cache: false,
        success: function(response) {
          if (response.status === true) {
            $(document).find(".reset-alert").show();
            $(document).find(".reset-input-wrapper").hide();
          } else {
            notify(response.data, "danger");
            $("#" + _form_reset + " .reset-password").focus();
          };
        }
      });
      return false;
    });

  });
</script>