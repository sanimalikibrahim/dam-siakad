<html>

<head>
  <style type="text/css">
    .mail-container {
      background-color: #fff;
      text-align: center;
      padding: 1.5rem .5rem;
      font-size: 17px;
      font-weight: 400;
      font-family: Helvetica, Arial, sans-serif, 'Open Sans';
    }

    .mail-body {
      padding: 15px;
      width: 100%;
      max-width: 415px;
      background-color: #fff;
      margin: 0 auto;
      color: #707c9b;
    }

    .mail-body {
      line-height: 23px;
    }

    .mail-body .img-logo {
      width: 68px;
    }

    .mail-body .title {
      font-size: 20px;
      line-height: 36px;
      font-weight: bold;
      margin: 0;
    }

    .border-bottom {
      width: 50px;
      border-bottom: 1px solid rgb(71, 120, 251);
      margin-top: 1rem;
    }

    .border-bottom2 {
      width: 150px;
      border-bottom: 1px solid rgb(71, 120, 251);
      margin: 0 auto;
      margin-top: 2rem;
    }

    a.btn-reset {
      padding: 12px 20px;
      border-radius: 4px;
      font-weight: 600;
      font-family: Helvetica, Arial, sans-serif, 'Open Sans';
      color: #ffffff;
      background: #f7941c;
      font-size: 15px;
      text-decoration: none;
    }

    hr {
      border: 1px solid rgb(248, 248, 248);
      margin-top: 1.3rem;
      margin-bottom: 1.3rem;
    }

    .text-center {
      text-align: center;
    }

    .text-left {
      text-align: left;
    }

    em {
      font-size: 14px;
      font-weight: 400;
      text-decoration: none;
      line-height: 16px;
    }

    a,
    small {
      font-size: 11px;
      font-weight: 400;
      text-decoration: none;
      line-height: 16px;
      color: #707c9b;
    }
  </style>
</head>

<body>
  <div class="mail-container">
    <div class="mail-body">
      <img src="https://siakad.darularqamgarut.sch.id/themes/_public/img/logo/logo.png" class="img-logo" alt="Logo DA Garut">
    </div>
    <hr />
    <div class="mail-body text-left">
      <p class="title">Peringatan Keamanan</p>
      <div class="border-bottom"></div>
    </div>
    <div class="mail-body text-left">
      <p>Seseorang telah meminta perubahan kata sandi untuk akun Anda.</p>
      <p>Silahkan klik tombol dibawah untuk merubah kata sandi. Atau, <b>abaikan</b> pesan ini jika itu bukan Anda.</p>

      <div class="text-center" style="margin-top: 2.5rem;">
        <a href="<?= $link ?>" class="btn-reset" target="_blank">
          Ubah Kata Sandi
        </a>
        <div class="border-bottom2"></div>
      </div>

      <div class="text-center" style="margin-top: 2.5rem;">
        <em>
          Tanggal permintaan <?= date('Y-m-d H:i:s') ?> WIB.
        </em>
      </div>
    </div>
    <hr />
    <div class="mail-body">
      <small>
        <a href="https://darularqamgarut.sch.id/" target="_blank">
          <b><?= date('Y') ?></b> &copy; Darul Arqam Muhammadiyah Garut
        </a>
        <p>Jl. Raya Garut - Tasikmalaya No. 36 Kp. Sawahlega RT/RW 01/02 Desa Ngamplangsari Kecamatan Cilawu, Garut 44181</p>
      </small>
    </div>
  </div>
</body>

</html>