<style type="text/css">
  .report-wrapper {
    margin: 0 auto;
  }

  .report-wrapper .report-logo {
    width: 70px;
    position: absolute;
    left: 3.5rem;
    top: 1.7rem;
    border-left: 10px solid white;
    border-right: 10px solid white;
  }

  .report-wrapper .report-form-name {
    border: 1px solid #999;
    padding: 8px;
    position: absolute;
    top: 4.5rem;
    right: 4rem;
  }

  .report-wrapper .report {
    border: 1px solid #333;
    padding: 2rem;
    padding-top: 1rem;
    margin-top: 15px;
  }

  .table-border {
    border-collapse: collapse;
    font-size: 1rem;
  }

  .table-border th {
    border: 1px solid #999;
    padding: 8px 8px;
  }

  .table-border td {
    border: 1px solid #999;
    padding: 4px 8px;
  }

  .ttd-input {
    width: 100%;
    height: 70px;
  }

  .ttd-hr {
    border-style: dotted;
    border-color: #ccc;
    width: 200px;
    margin-top: 2px;
    margin-bottom: 2px;
  }

  .text-center {
    text-align: center;
  }

  .text-right {
    text-align: right;
  }

  .pull-left {
    float: left;
    text-align: left;
  }

  .pull-right {
    float: right;
    text-align: right
  }
</style>

<div class="report-wrapper">
  <img src="<?= base_url('themes/_public/img/logo/logo-white.png') ?>" class="report-logo" />
  <div class="report-form-name">Form B.6</div>

  <div class="report">
    <table style="width: 100%;">
      <tr>
        <th colspan="3" style="padding-bottom: 2rem; font-size: 14pt; font-weight: 500; text-align: center;">
          LAPORAN KEUANGAN PERPUSTAKAAN
        </th>
      </tr>
      <tr>
        <td style="width: 100px;">NOMOR</td>
        <td style="width: 10px;">:</td>
        <td><?= $pengajuan->nomor ?></td>
      </tr>
      <tr>
        <td style="width: 100px;">TANGGAL</td>
        <td style="width: 10px;">:</td>
        <td>
          <?php
          $date = $pengajuan->tanggal;
          $day = date('d', strtotime($date));
          $month = $controller->get_month(date('m', strtotime($date)));
          $year = date('Y', strtotime($date));

          echo $day . ' ' . $month . ' ' . $year;
          ?>
        </td>
      </tr>
      <tr>
        <td style="width: 100px;">NAMA BIDANG</td>
        <td style="width: 10px;">:</td>
        <td>Perpustakaan</td>
      </tr>
      <tr>
        <td colspan="3">
          <table class="table-border" style="width: 100%; margin-top: 2rem;">
            <thead>
              <tr>
                <th class="text-center" style="width: 50px;">No</th>
                <th class="text-center">Uraian Jenis Kebutuhan</th>
                <th class="text-center" style="width: 160px;">Jumlah Kebutuhan</th>
                <th class="text-center" style="width: 120px;">Kode Satuan</th>
                <th class="text-center" style="width: 160px;">Harga Satuan</th>
                <th class="text-center" style="width: 160px;">Total Harga</th>
              </tr>
            </thead>
            <tbody>
              <?php if (count($pengajuan_item) > 0) : ?>
                <?php $no = 1; ?>
                <?php $jumlah = 0; ?>
                <?php foreach ($pengajuan_item as $key => $item) : ?>
                  <?php $jumlah = (float) $jumlah + (!is_null($item->amount) && !empty($item->amount) ? (float) $item->amount : 0) ?>
                  <tr>
                    <td class="text-center"><?= $no++ ?></td>
                    <td><?= $item->item ?></td>
                    <td class="text-center"><?= $item->quantity ?></td>
                    <td class="text-center"><?= $item->unit ?></td>
                    <td>
                      <div class="pull-left">Rp.</div>
                      <div class="pull-right"><?= number_format($item->unit_price) ?></div>
                    </td>
                    <td>
                      <div class="pull-left">Rp.</div>
                      <div class="pull-right"><?= number_format($item->amount) ?></div>
                    </td>
                  </tr>
                <?php endforeach; ?>
              <?php endif; ?>
            </tbody>
            <tfoot>
              <tr>
                <th colspan="5" class="text-right">Jumlah</th>
                <th>
                  <div class="pull-left">Rp.</div>
                  <div class="pull-right"><?= number_format($jumlah) ?></div>
                </th>
              </tr>
            </tfoot>
          </table>
        </td>
      </tr>
      <tr>
        <td colspan="3">
          <table class="table-border" style="width: 100%; margin-top: 2rem; text-align: center;">
            <!-- <td class="text-center">
              <p>&nbsp;</p>
              Disetujui,
              <div class="ttd-input"></div>
              <?= $pengajuan_ttd->staff_bendahara ?>
              <hr class="ttd-hr" />
              NKTAM : -
            </td>
            <td class="text-center">
              <p>Garut, <?= date('d') . ' ' . $controller->get_month(date('m')) . ' ' . date('Y') ?></p>
              Pemohon,
              <div class="ttd-input"></div>
              <?= $pengajuan_ttd->staff_perpustakaan ?>
              <hr class="ttd-hr" />
              NKTAM : -
            </td> -->
            <tr>
              <td colspan="2" class="text-right" style="padding: 0; padding-bottom: 1rem; border: none;">
                Garut, <?= date('d') . ' ' . $controller->get_month(date('m')) . ' ' . date('Y') ?>
              </td>
            </tr>
            <tr>
              <td><b>Pemohon</b></td>
              <td><b>Disetujui</b></td>
            </tr>
            <tr>
              <td style="height: 90px;">
                <?= $pengajuan_ttd->staff_perpustakaan ?>
                <hr class="ttd-hr" />
                NKTAM : -
              </td>
              <td style="height: 90px;">
                <?= $pengajuan_ttd->staff_bendahara ?>
                <hr class="ttd-hr" />
                NKTAM : -
              </td>
            </tr>
            <tr>
              <td><b>Staff Perpustakaan</b></td>
              <td><b>Staff Bendahara</b></td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </div>
</div>