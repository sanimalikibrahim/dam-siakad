<div class="modal fade" id="modal-detail-pengajuanbuku">
  <div class="modal-dialog modal-dialog-centered modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title pull-left">Pengajuan Buku: Detail</h5>
      </div>
      <div class="spinner">
        <div class="lds-hourglass"></div>
      </div>
      <div class="modal-body">

        <div class="tab-container">
          <ul class="nav nav-tabs <?= ($app->is_mobile) ? 'nav-fill' : '' ?>" role="tablist">
            <li class="nav-item">
              <a class="nav-link active nav-link-active" data-toggle="tab" href="#tab-pengajuanbuku-data" role="tab">Data</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" data-toggle="tab" href="#tab-pengajuanbuku-history" role="tab">History</a>
            </li>
          </ul>
          <div class="tab-content clear-tab-content">
            <!-- Data -->
            <div class="tab-pane active fade show" id="tab-pengajuanbuku-data" role="tabpanel">
              <div class="form-group mb-3">
                <label><b>Nomor Pengajuan</b></label>
                <div class="form-control-label pengajuanbuku-nomor"></div>
              </div>

              <div class="form-group mb-3">
                <label><b>Tanggal Pengajuan</b></label>
                <div class="form-control-label pengajuanbuku-tanggal"></div>
              </div>

              <div class="form-group mb-3">
                <label><b>Status</b></label>
                <div class="form-control-label pengajuanbuku-status"></div>
              </div>

              <!-- Grid -->
              <div class="table-responsive">
                <table id="table-pengajuanbuku-detail-item" class="table table-bordered table-sm" style="width: 100%;">
                  <thead class="thead-default">
                    <tr>
                      <th width="100">No</th>
                      <th>Item</th>
                      <th>Quantity</th>
                      <th>Unit</th>
                      <th>Unit Price</th>
                      <th>Amount</th>
                    </tr>
                  </thead>
                </table>
              </div>
            </div>
            <!-- History -->
            <div class="tab-pane fade show" id="tab-pengajuanbuku-history" role="tabpanel">
              <div class="pengajuanbuku-hitory"></div>
            </div>
          </div>
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-light" data-dismiss="modal">
          Close
        </button>
      </div>
    </div>
  </div>
</div>