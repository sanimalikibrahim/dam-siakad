<div class="modal fade" id="modal-form-pengajuanbuku-item" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-dialog-centered modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title pull-left">Pengajuan Buku: Item</h5>
      </div>
      <div class="spinner">
        <div class="lds-hourglass"></div>
      </div>
      <div class="modal-body">
        <div class="form-group mb-3">
          <label><b>Nomor Pengajuan</b></label>
          <div class="form-control-label pengajuanbuku-item-nomor"></div>
        </div>

        <!-- Form input -->
        <div class="table-action">
          <div class="buttons">
            <button class="btn btn--raised btn-success btn--icon-text pengajuanbuku-item-action-add">
              <i class="zmdi zmdi-plus-circle"></i> Add New
            </button>
          </div>
        </div>

        <div class="border border-bottom-0 form-pengajuanbuku-item-input bg-light" style="padding: 10px;">
          <span class="form-pengajuanbuku-item-input-title" style="font-weight: bold;">-</span>
        </div>
        <div class="border form-pengajuanbuku-item-input mb-2" style="padding: 10px;">
          <form id="form-pengajuanbuku-item" autocomplete="off">
            <!-- CSRF -->
            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
            <input type="hidden" name="pengajuan_id" class="pengajuanbuku-item-pengajuan_id" readonly />

            <div class="form-group" style="margin-bottom: 3rem;">
              <label required>Item</label>
              <textarea name="item" class="form-control textarea-autosize text-counter pengajuanbuku-item-item" rows="1" data-max-length="200" style="overflow: hidden; overflow-wrap: break-word; height: 31px;" placeholder="Item" required></textarea>
              <i class="form-group__bar"></i>
            </div>

            <div class="row">
              <div class="col">
                <div class="form-group">
                  <label required>Quantity</label>
                  <input type="number" name="quantity" class="form-control mask-number pengajuanbuku-item-quantity" maxlength="10" placeholder="Quantity" required />
                  <i class="form-group__bar"></i>
                </div>
              </div>
              <div class="col">
                <div class="form-group">
                  <label required>Unit</label>
                  <input type="text" name="unit" class="form-control pengajuanbuku-item-unit" maxlength="30" placeholder="Unit" value="Pcs" required />
                  <i class="form-group__bar"></i>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-xs-12 col-md-6">
                <div class="form-group">
                  <label required>Unit Price</label>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text" id="basic-addon1">Rp</span>
                    </div>
                    <input type="text" name="unit_price" class="form-control mask-money pengajuanbuku-item-unit_price" maxlength="20" placeholder="Unit Price" required />
                    <i class="form-group__bar"></i>
                  </div>
                </div>
              </div>
              <div class="col-xs-12 col-md-6">
                <div class="form-group">
                  <label required>Amount</label>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text" id="basic-addon1">Rp</span>
                    </div>
                    <input type="text" name="amount" class="form-control mask-money pengajuanbuku-item-amount" maxlength="20" placeholder="Amount" value="0" required readonly />
                    <i class="form-group__bar"></i>
                  </div>
                </div>
              </div>
            </div>
          </form>

          <small class="form-text text-muted">
            Fields with red stars (<label required></label>) are required.
          </small>

          <div class="border-top mt-2 text-right" style="padding-top: 10px;">
            <button type="button" class="btn btn-success btn--icon-text pengajuanbuku-item-action-save">
              <i class="zmdi zmdi-save"></i> Save
            </button>
            <button type="button" class="btn btn-light btn--icon-text pengajuanbuku-item-action-cancel">
              Cancel
            </button>
          </div>
        </div>

        <!-- Grid -->
        <div class="table-responsive">
          <table id="table-pengajuanbuku-item" class="table table-bordered table-sm" style="width: 100%;">
            <thead class="thead-default">
              <tr>
                <th width="100">No</th>
                <th>Item</th>
                <th>Quantity</th>
                <th>Unit</th>
                <th>Unit Price</th>
                <th>Amount</th>
                <th width="150">Action</th>
              </tr>
            </thead>
          </table>
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-light" data-dismiss="modal">
          Close
        </button>
      </div>
    </div>
  </div>
</div>