<div class="modal fade" id="modal-form-pengajuanbuku" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title pull-left">Pengajuan Buku</h5>
      </div>
      <div class="spinner">
        <div class="lds-hourglass"></div>
      </div>
      <div class="modal-body">
        <form id="form-pengajuanbuku" autocomplete="off">
          <!-- CSRF -->
          <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />

          <div class="form-group">
            <label required>Nomor</label>
            <input type="text" name="nomor" class="form-control pengajuanbuku-nomor" maxlength="100" placeholder="Nomor" required />
            <i class="form-group__bar"></i>
          </div>

          <div class="form-group">
            <label required>Tanggal</label>
            <input type="text" name="tanggal" class="form-control flatpickr-date pengajuanbuku-tanggal" placeholder="Tanggal" required />
            <i class="form-group__bar"></i>
          </div>

          <small class="form-text text-muted">
            Fields with red stars (<label required></label>) are required.
          </small>

        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success btn--icon-text pengajuanbuku-action-save">
          <i class="zmdi zmdi-save"></i> Save
        </button>
        <button type="button" class="btn btn-light btn--icon-text pengajuanbuku-action-cancel" data-dismiss="modal">
          Cancel
        </button>
      </div>
    </div>
  </div>
</div>