<style type="text/css">
    .form-control[readonly] {
        opacity: 1;
    }

    .form-control-label {
        width: 100%;
        padding: .375rem 0;
        border-bottom: 1px solid #eceff1;
    }

    .pengajuanbuku-hitory .alert {
        padding: 10px;
        font-size: 9.5pt;
    }

    #table-pengajuanbuku-item_wrapper {
        margin-top: 0;
    }

    #table-pengajuanbuku-detail-item_wrapper {
        margin-top: 0;
    }

    .dropdown-item {
        padding: .5rem 1rem !important;
        font-size: 9.5pt !important;
    }
</style>

<section id="pengajuanbuku">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title"><?php echo (isset($card_title)) ? $card_title : '' ?></h4>
            <h6 class="card-subtitle"><?php echo (isset($card_subTitle)) ? $card_subTitle : '' ?></h6>

            <div class="table-action">
                <div class="buttons">
                    <button class="btn btn--raised btn-primary btn--icon-text pengajuanbuku-action-add" data-toggle="modal" data-target="#modal-form-pengajuanbuku">
                        <i class="zmdi zmdi-plus-circle"></i> Add New
                    </button>
                </div>
            </div>

            <?php include_once('form.php') ?>
            <?php include_once('form_item.php') ?>
            <?php include_once('detail.php') ?>
            <?php include_once('partial.php') ?>

            <div class="table-responsive">
                <table id="table-pengajuanbuku" class="table table-bordered">
                    <thead class="thead-default">
                        <tr>
                            <th width="100">No</th>
                            <th>Nomor</th>
                            <th>Tanggal</th>
                            <th>Status</th>
                            <th>Created</th>
                            <th width="240">Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</section>