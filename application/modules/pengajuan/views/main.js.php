<script type="text/javascript">
  $(document).ready(function() {

    var _key = "";
    var _key_item = "";
    var _section = "pengajuanbuku";
    var _table = "table-pengajuanbuku";
    var _table_item = "table-pengajuanbuku-item";
    var _table_detail_item = "table-pengajuanbuku-detail-item";
    var _modal = "modal-form-pengajuanbuku";
    var _modal_item = "modal-form-pengajuanbuku-item";
    var _modal_detail = "modal-detail-pengajuanbuku";
    var _form = "form-pengajuanbuku";
    var _form_item = "form-pengajuanbuku-item";
    var _modal_partial = "modal-partial";
    var _toggleItemInput = false;

    // Initialize DataTables: Index
    if ($("#" + _table)[0]) {
      var table_pengajuanbuku = $("#" + _table).DataTable({
        processing: true,
        serverSide: true,
        ajax: {
          url: "<?php echo base_url('pengajuan/ajax_get_all/') ?>",
          type: "get"
        },
        columns: [{
            data: null,
            render: function(data, type, row, meta) {
              var nomor = meta.row + meta.settings._iDisplayStart + 1;

              if ($.inArray(row.status, ["2", "4"]) !== -1) {
                return '<span style="color: #ff6b68;">' + nomor + '</span>';
              } else if ($.inArray(row.status, ["5"]) !== -1) {
                return '<span style="color: #FF9800;">' + nomor + '</span>';
              } else {
                return '<span>' + nomor + '</span>';
              };
            }
          },
          {
            data: "nomor",
            render: function(data, type, row, meta) {
              if ($.inArray(row.status, ["2", "4"]) !== -1) {
                return '<span style="color: #ff6b68;">' + data + '</span>';
              } else if ($.inArray(row.status, ["5"]) !== -1) {
                return '<span style="color: #FF9800;">' + data + '</span>';
              } else {
                return '<span>' + data + '</span>';
              };
            }
          },
          {
            data: "tanggal",
            render: function(data, type, row, meta) {
              if ($.inArray(row.status, ["2", "4"]) !== -1) {
                return '<span style="color: #ff6b68;">' + data + '</span>';
              } else if ($.inArray(row.status, ["5"]) !== -1) {
                return '<span style="color: #FF9800;">' + data + '</span>';
              } else {
                return '<span>' + data + '</span>';
              };
            }
          },
          {
            data: "status",
            render: function(data, type, row, meta) {
              var status = getStatus(data);

              if ($.inArray(data, ["2", "4"]) !== -1) {
                return '<span style="color: #ff6b68;">' + status + '</span>';
              } else if ($.inArray(row.status, ["5"]) !== -1) {
                return '<span style="color: #FF9800;">' + status + '</span>';
              } else {
                return '<span>' + status + '</span>';
              };
            }
          },
          {
            data: "created_at",
            render: function(data, type, row, meta) {
              if ($.inArray(row.status, ["2", "4"]) !== -1) {
                return '<span style="color: #ff6b68;">' + data + '</span>';
              } else if ($.inArray(row.status, ["5"]) !== -1) {
                return '<span style="color: #FF9800;">' + data + '</span>';
              } else {
                return '<span>' + data + '</span>';
              };
            }
          },
          {
            data: null,
            className: "center",
            render: function(data, type, row, meta) {
              var output = '';

              output += '<div class="action">';
              if ($.inArray(row.status, ["0", "2", "4"]) !== -1) {
                output += '<div class="dropleft">';
                output += '<a class="btn btn-sm btn-light btn-table-action" data-toggle="dropdown" style="color: #525a62;"><i class="zmdi zmdi-menu"></i> Action</a>';
                output += '<div class="dropdown-menu dropdown-menu-right">';
                output += '<a href="javascript:;" class="dropdown-item action-edit" data-toggle="modal" data-target="#' + _modal + '">Edit</a>';
                output += '<a href="javascript:;" class="dropdown-item action-delete">Delete</a>';
                output += '<div class="dropdown-divider"></div>';
                output += '<a href="javascript:;" class="dropdown-item action-item" data-toggle="modal" data-target="#' + _modal_item + '">Item</a>';
                output += '</div>';
                output += '</div>';
                output += '&nbsp;<a href="javascript:;" class="btn btn-sm btn-light btn-table-action action-detail" data-toggle="modal" data-target="#' + _modal_detail + '"><i class="zmdi zmdi-eye"></i> Detail</a>';
                output += '&nbsp;<a href="javascript:;" class="btn btn-sm btn-info btn-table-action action-send" data-status="1" title="Send to Kabid"><i class="zmdi zmdi-sign-in"></i> Send</a>';
              } else if ($.inArray(row.status, ["5"]) !== -1) {
                output += '&nbsp;<a href="javascript:;" class="btn btn-sm btn-light btn-table-action action-detail" data-toggle="modal" data-target="#' + _modal_detail + '"><i class="zmdi zmdi-eye"></i> Detail</a>';
                // output += '&nbsp;<a href="javascript:;" class="btn btn-sm btn-success btn-table-action action-send-report" data-status="6" title="Send Report"><i class="zmdi zmdi-check-circle"></i> Send Report</a>';
                output += '&nbsp;<a href="javascript:;" class="btn btn-sm btn-success btn-table-action action-detail-report" title="Send Report" data-toggle="modal" data-target="#' + _modal_partial + '"><i class="zmdi zmdi-check-circle"></i> Send Report</a>';
              } else {
                output += '<a href="javascript:;" class="btn btn-sm btn-light btn-table-action action-detail" data-toggle="modal" data-target="#' + _modal_detail + '"><i class="zmdi zmdi-eye"></i> Detail</a>';
              };
              output += '</div>';

              return output;
            }
          }
        ],
        autoWidth: !1,
        responsive: {
          details: {
            renderer: function(api, rowIdx, columns) {
              var hideColumn = [];
              var data = $.map(columns, function(col, i) {
                return ($.inArray(col.columnIndex, hideColumn)) ?
                  '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                  '<td class="dt-details-td">' + col.title + ':' + '</td> ' +
                  '<td class="dt-details-td">' + col.data + '</td>' +
                  '</tr>' :
                  '';
              }).join('');

              return data ? $('<table/>').append(data) : false;
            },
            type: "inline",
            target: 'tr',
          }
        },
        columnDefs: [{
          className: 'desktop',
          targets: [0, 1, 2, 3, 4, 5]
        }, {
          className: 'tablet',
          targets: [0, 1, 2, 3]
        }, {
          className: 'mobile',
          targets: [0, 1]
        }, {
          responsivePriority: 2,
          targets: -1
        }],
        pageLength: 15,
        language: {
          searchPlaceholder: "Search...",
          sProcessing: '<div style="text-align: center;"><div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div></div>'
        },
        sDom: '<"dataTables_ct"><"dataTables__top"fb>rt<"dataTables__bottom"ip><"clear">',
        buttons: [{
          extend: "excelHtml5",
          title: "Export Result"
        }, {
          extend: "print",
          title: "Export Result"
        }],
        initComplete: function(a, b) {
          $(this).closest(".dataTables_wrapper").find(".dataTables__top").prepend(
            '<div class="dataTables_buttons hidden-sm-down actions">' +
            '<span class="actions__item zmdi zmdi-refresh" data-table-action="reload" title="Reload" />' +
            '</div>'
          );
        },
      });

      $(".dataTables_filter input[type=search]").focus(function() {
        $(this).closest(".dataTables_filter").addClass("dataTables_filter--toggled")
      });

      $(".dataTables_filter input[type=search]").blur(function() {
        $(this).closest(".dataTables_filter").removeClass("dataTables_filter--toggled")
      });

      $("body").on("click", "[data-table-action]", function(a) {
        a.preventDefault();
        var b = $(this).data("table-action");
        if ("reload" === b) {
          $("#" + _table).DataTable().ajax.reload(null, false);
        };
      });
    };

    // Intialize DataTables: Item
    var table_pengajuanbuku_item = $("#" + _table_item).DataTable({
      destroy: true,
      info: false,
      searching: false,
      lengthChange: false,
      data: [],
      pageLength: 10,
      columns: [{
          data: null,
        },
        {
          data: 'item'
        },
        {
          data: 'quantity',
          render: function(data, type, row, meta) {
            return meta.settings.fnFormatNumber(data);
          }
        },
        {
          data: 'unit'
        },
        {
          data: 'unit_price',
          render: function(data, type, row, meta) {
            return "Rp " + meta.settings.fnFormatNumber(data);
          }
        },
        {
          data: 'amount',
          render: function(data, type, row, meta) {
            return "Rp " + meta.settings.fnFormatNumber(data);
          }
        },
        {
          data: null,
          className: "center",
          render: function(data, type, row, meta) {
            var output = '';

            output += '<div class="action">';
            output += '<a href="javascript:;" class="btn btn-sm btn-light btn-table-action action-edit" title="Edit"><i class="zmdi zmdi-edit"></i></a>';
            output += '&nbsp;<a href="javascript:;" class="btn btn-sm btn-light btn-table-action action-delete" title="Delete"><i class="zmdi zmdi-delete"></i></a>';
            output += '</div>';

            return output;
          }
        }
      ],
      responsive: {
        details: {
          renderer: function(api, rowIdx, columns) {
            var hideColumn = [];
            var data = $.map(columns, function(col, i) {
              return ($.inArray(col.columnIndex, hideColumn)) ?
                '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                '<td class="dt-details-td">' + col.title + ':' + '</td> ' +
                '<td class="dt-details-td">' + col.data + '</td>' +
                '</tr>' :
                '';
            }).join('');

            return data ? $('<table/>').append(data) : false;
          },
          type: "inline",
          target: 'tr',
        }
      },
      columnDefs: [{
        className: 'desktop',
        targets: [0, 1, 2, 3, 4, 5, 6]
      }, {
        className: 'tablet',
        targets: [0, 1, 2, 4]
      }, {
        className: 'mobile',
        targets: [0, 1]
      }, {
        responsivePriority: 2,
        targets: -1
      }],
    });

    // Initalize column number: Item
    table_pengajuanbuku_item.on('order.dt search.dt', function() {
      table_pengajuanbuku_item.column(0, {
        search: 'applied',
        order: 'applied'
      }).nodes().each(function(cell, i) {
        cell.innerHTML = i + 1;
      });
    }).draw();

    // Intialize DataTables: Item Detail
    var table_pengajuanbuku_detail_item = $("#" + _table_detail_item).DataTable({
      destroy: true,
      info: false,
      searching: false,
      lengthChange: false,
      data: [],
      pageLength: 10,
      columns: [{
          data: null,
        },
        {
          data: 'item'
        },
        {
          data: 'quantity',
          render: function(data, type, row, meta) {
            return meta.settings.fnFormatNumber(data);
          }
        },
        {
          data: 'unit'
        },
        {
          data: 'unit_price',
          render: function(data, type, row, meta) {
            return "Rp " + meta.settings.fnFormatNumber(data);
          }
        },
        {
          data: 'amount',
          render: function(data, type, row, meta) {
            return "Rp " + meta.settings.fnFormatNumber(data);
          }
        }
      ],
      responsive: {
        details: {
          renderer: function(api, rowIdx, columns) {
            var hideColumn = [];
            var data = $.map(columns, function(col, i) {
              return ($.inArray(col.columnIndex, hideColumn)) ?
                '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                '<td class="dt-details-td">' + col.title + ':' + '</td> ' +
                '<td class="dt-details-td">' + col.data + '</td>' +
                '</tr>' :
                '';
            }).join('');

            return data ? $('<table/>').append(data) : false;
          },
          type: "inline",
          target: 'tr',
        }
      },
      columnDefs: [{
        className: 'desktop',
        targets: [0, 1, 2, 3, 4, 5]
      }, {
        className: 'tablet',
        targets: [0, 1, 2, 4]
      }, {
        className: 'mobile',
        targets: [0, 1]
      }, {
        responsivePriority: 2,
        targets: -1
      }],
    });

    // Initalize column number: Item Detail
    table_pengajuanbuku_detail_item.on('order.dt search.dt', function() {
      table_pengajuanbuku_detail_item.column(0, {
        search: 'applied',
        order: 'applied'
      }).nodes().each(function(cell, i) {
        cell.innerHTML = i + 1;
      });
    }).draw();

    // Handle data add
    $("#" + _section).on("click", "button." + _section + "-action-add", function(e) {
      e.preventDefault();
      resetForm();

      // Fetch nomor
      $.ajax({
        url: "<?= base_url('pengajuan/ajax_get_nomor') ?>",
        type: "get",
        dataType: "json",
        success: function(response) {
          $("#" + _form + " ." + _section + "-nomor").val(response).trigger('input');
        }
      });
    });

    // Handle data add: Item
    $("#" + _section).on("click", "button." + _section + "-item-action-add", function(e) {
      e.preventDefault();
      resetFormItem();
    });

    // Handle data edit
    $("#" + _table).on("click", "a.action-edit", function(e) {
      e.preventDefault();
      resetForm();
      var temp = table_pengajuanbuku.row($(this).closest('tr')).data();

      // Set key for update params, important!
      _key = temp.id;

      $("#" + _form + " ." + _section + "-nomor").val(temp.nomor).trigger('input');
      $("#" + _form + " ." + _section + "-tanggal").val(temp.tanggal).trigger('input');
    });

    // Handle data edit: Item
    $("#" + _table_item).on("click", "a.action-edit", function(e) {
      e.preventDefault();
      var temp = table_pengajuanbuku_item.row($(this).closest('tr')).data();

      // Set key for update params, important!
      _key_item = temp.id;
      _toggleItemInput = false;

      resetFormItem(false);
      $("#" + _form_item + " ." + _section + "-item-item").val(temp.item.replace(/<br\s*[\/]?>/gi, "\r\n")).trigger("input");
      $("#" + _form_item + " ." + _section + "-item-quantity").val(temp.quantity).trigger('input');
      $("#" + _form_item + " ." + _section + "-item-unit").val(temp.unit).trigger('input');
      $("#" + _form_item + " ." + _section + "-item-unit_price").val(temp.unit_price).trigger('input');
      $("#" + _form_item + " ." + _section + "-item-amount").val(temp.amount).trigger('input');

      // Handle textarea height
      setTimeout(function() {
        setTextareHeight();
      }, 500);
    });

    // Handle data open item
    $("#" + _table).on("click", "a.action-item", function(e) {
      e.preventDefault();
      _toggleItemInput = true;

      resetFormItem();
      var temp = table_pengajuanbuku.row($(this).closest('tr')).data();

      // Set key for update params, important!
      _key = temp.id;

      $("#" + _modal_item + " ." + _section + "-item-nomor").html(temp.nomor);

      fetchItem(temp.id);
    });

    // Handle data open detail
    $("#" + _table).on("click", "a.action-detail", function(e) {
      e.preventDefault();
      resetDetail();
      var temp = table_pengajuanbuku.row($(this).closest('tr')).data();

      // Set key for update params, important!
      _key = temp.id;

      $("#" + _modal_detail + " .nav-link-active").click();
      $("#" + _modal_detail + " ." + _section + "-nomor").html(temp.nomor);
      $("#" + _modal_detail + " ." + _section + "-tanggal").html(temp.tanggal);
      $("#" + _modal_detail + " ." + _section + "-status").html(getStatus(temp.status));
      $("#" + _modal_detail + " ." + _section + "-hitory").html("<i>No data found.</i>");

      fetchItem(_key, _table_detail_item);

      // Fetch history
      $.ajax({
        url: "<?= base_url('pengajuan/ajax_get_history/') ?>" + _key,
        type: "get",
        dataType: "json",
        success: function(response) {
          if (response.length > 0) {
            var historyItem = "";

            response.map((item) => {
              var alertType = (item.action === "Sent" || item.action === "Approve") ? "bg-light" : "bg-red";
              var alertTextColor = (item.action === "Sent" || item.action === "Approve") ? "#747a80" : "#f9f9f9";
              historyItem += '<div class="alert alert-info ' + alertType + ' mb-1" style="color: ' + alertTextColor + ';">';
              historyItem += '<b>' + item.nama_lengkap + '</b> ' + item.description;
              historyItem += '<p class="mb-0"><small>' + item.created_at + '</small></p> ';
              historyItem += '</div>';
            });

            $("#" + _modal_detail + " ." + _section + "-hitory").html(historyItem);
          };
        }
      });
    });

    // Handle open report
    $("#" + _table).on("click", "a.action-detail-report", function(e) {
      e.preventDefault();
      var temp = table_pengajuanbuku.row($(this).closest('tr')).data();

      // Set key for update params, important!
      _key = temp.id;

      $("#" + _modal_partial + " .partial-title").html("Laporan");
      $("#" + _modal_partial + " .partial-content").html("");
      $("#" + _modal_partial + " .partial-footer").html("");

      // Fetch history
      $.ajax({
        url: "<?= base_url('pengajuan/ajax_get_report/') ?>" + _key,
        type: "get",
        success: function(response) {
          var button = '<a href="javascript:;" class="btn btn-success action-send-report" data-id="' + temp.id + '" data-nomor="' + temp.nomor + '" data-status="6" title="Send Report"><i class="zmdi zmdi-check-circle"></i> Send Report</a>';

          $("#" + _modal_partial + " .partial-content").html(response);
          $("#" + _modal_partial + " .partial-footer").html(button);
        }
      });
    });

    // Handle data submit
    $("#" + _modal + " ." + _section + "-action-save").on("click", function(e) {
      e.preventDefault();
      $.ajax({
        type: "post",
        url: "<?php echo base_url('pengajuan/ajax_save/') ?>" + _key,
        data: $("#" + _form).serialize(),
        success: function(response) {
          var response = JSON.parse(response);
          if (response.status === true) {
            resetForm();
            $("#" + _modal).modal("hide");
            $("#" + _table).DataTable().ajax.reload(null, false);
            notify(response.data, "success");
          } else {
            notify(response.data, "danger");
          };
        }
      });
    });

    // Handle data submit: Item
    $("#" + _modal_item + " ." + _section + "-item-action-save").on("click", function(e) {
      e.preventDefault();
      $.ajax({
        type: "post",
        url: "<?php echo base_url('pengajuan/ajax_save_item/') ?>" + _key_item,
        data: $("#" + _form_item).serialize(),
        dataType: "json",
        success: function(response) {
          if (response.status === true) {
            resetFormItem();
            fetchItem(_key);
            notify(response.data, "success");
          } else {
            notify(response.data, "danger");
          };
        }
      });
    });

    // Handle data cancel: Item
    $("#" + _modal_item + " ." + _section + "-item-action-cancel").on("click", function(e) {
      e.preventDefault();
      resetFormItem();
    });

    // Handle data delete
    $("#" + _table).on("click", "a.action-delete", function(e) {
      e.preventDefault();
      var temp = table_pengajuanbuku.row($(this).closest('tr')).data();

      swal({
        title: "Are you sure to delete?",
        text: "Once deleted, you will not be able to recover this data!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false
      }).then((result) => {
        if (result.value) {
          $.ajax({
            type: "delete",
            url: "<?php echo base_url('pengajuan/ajax_delete/') ?>" + temp.id,
            dataType: "json",
            success: function(response) {
              if (response.status) {
                $("#" + _table).DataTable().ajax.reload(null, false);
                notify(response.data, "success");
              } else {
                notify(response.data, "danger");
              };
            }
          });
        };
      });
    });

    // Handle data delete: Item
    $("#" + _table_item).on("click", "a.action-delete", function(e) {
      e.preventDefault();
      var temp = table_pengajuanbuku_item.row($(this).closest('tr')).data();

      // Togggle form input item
      _toggleItemInput = true;
      $("#" + _modal_item + " ." + _section + "-item-action-cancel").click();

      swal({
        title: "Are you sure to delete?",
        text: "Once deleted, you will not be able to recover this data!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false
      }).then((result) => {
        if (result.value) {
          $.ajax({
            type: "delete",
            url: "<?php echo base_url('pengajuan/ajax_delete_item/') ?>" + temp.id,
            dataType: "json",
            success: function(response) {
              if (response.status) {
                fetchItem(_key);
                notify(response.data, "success");
              } else {
                notify(response.data, "danger");
              };
            }
          });
        };
      });
    });

    // Handle data send
    $("#" + _table).on("click", "a.action-send", function(e) {
      e.preventDefault();
      var temp = table_pengajuanbuku.row($(this).closest('tr')).data();
      var status = $(this).attr("data-status");

      swal({
        title: "Are you sure to send?",
        html: temp.nomor,
        type: "question",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false
      }).then((result) => {
        if (result.value) {
          $.ajax({
            type: "delete",
            url: "<?php echo base_url('pengajuan/ajax_set_status/') ?>" + temp.id + "/" + status,
            dataType: "json",
            success: function(response) {
              if (response.status) {
                $("#" + _table).DataTable().ajax.reload(null, false);
                swal('Success!', 'Your data has been sent successfully.', 'success');
              } else {
                notify(response.data, "danger");
              };
            }
          });
        };
      });
    });

    // Handle data send report
    $("#" + _modal_partial).on("click", "a.action-send-report", function(e) {
      e.preventDefault();
      var id = $(this).attr("data-id");
      var nomor = $(this).attr("data-nomor");
      var status = $(this).attr("data-status");

      swal({
        title: "Are you sure to send report?",
        html: nomor,
        type: "question",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false
      }).then((result) => {
        if (result.value) {
          $("#" + _modal_partial).modal("hide");

          $.ajax({
            type: "delete",
            url: "<?php echo base_url('pengajuan/ajax_set_status/') ?>" + id + "/" + status,
            dataType: "json",
            success: function(response) {
              if (response.status) {
                $("#" + _table).DataTable().ajax.reload(null, false);
                swal('Success!', 'Your data has been sent successfully.', 'success');
              } else {
                notify(response.data, "danger");
              };
            }
          });
        };
      });
    });

    // Perform calculating
    $("#" + _form_item + " ." + _section + "-item-quantity").on("keyup mouseup", function() {
      calculateTotal();
    });
    $("#" + _form_item + " ." + _section + "-item-unit_price").on("keyup", function() {
      calculateTotal();
    });

    // Handle form reset
    resetForm = () => {
      _key = "";
      $("#" + _form).trigger("reset");
    };

    // Handle form reset item
    resetFormItem = (resetKey = true) => {
      if (resetKey === true) {
        _key_item = "";
      };

      $("#" + _form_item).trigger("reset");
      $("#" + _modal_item + " ." + _section + "-item-pengajuan_id").val(_key).trigger("input");
      setTextareHeight(true);
      toggleItemInput();
    };

    // Handle form input item
    toggleItemInput = () => {
      var input = $("#" + _modal_item + " .form-pengajuanbuku-item-input");
      var inputTitle = $("#" + _modal_item + " .form-pengajuanbuku-item-input-title");
      var addButton = $("#" + _modal_item + " button." + _section + "-item-action-add");

      if (_toggleItemInput === true) {
        _toggleItemInput = false;
        input.hide();
        addButton.fadeIn("fast");
      } else {
        _toggleItemInput = true;
        input.fadeIn("fast");
        addButton.hide();
      };

      if (_key_item === "") {
        inputTitle.html("Add New");
      } else {
        inputTitle.html("Edit");
      };
    };

    // Handle detail reset
    resetDetail = () => {
      $("#" + _modal_detail + " ." + _section + "-kode").html("-");
      $("#" + _modal_detail + " ." + _section + "-tanggal").html("-");
      $("#" + _modal_detail + " ." + _section + "-item").html("-");
      $("#" + _modal_detail + " ." + _section + "-quantity").html("-");
      $("#" + _modal_detail + " ." + _section + "-unit_price").html("-");
      $("#" + _modal_detail + " ." + _section + "-total").html("-");
      $("#" + _modal_detail + " ." + _section + "-status").html("-");
      $("#" + _modal_detail + " ." + _section + "-hitory").html("-");
    };

    // Handle textarea height
    setTextareHeight = (isReset = false) => {
      if (isReset === true) {
        $("#" + _form_item + " ." + _section + "-item-item").css("height", "31px").keyup();
      } else {
        $("#" + _form_item + " ." + _section + "-item-item").height($("#" + _form_item + " ." + _section + "-item-item")[0].scrollHeight).keyup();
      };
    };

    // Handle calculating
    calculateTotal = () => {
      var quantity = $("#" + _form_item + " ." + _section + "-item-quantity").val().replace(/[^\d]/g, "");
      quantity = (quantity != "") ? quantity : 0;

      var unit_price = $("#" + _form_item + " ." + _section + "-item-unit_price").val().replace(/[^\d]/g, "");
      unit_price = (unit_price != "") ? unit_price : 0;

      var amount = $("#" + _form_item + " ." + _section + "-item-amount");
      var result = unit_price * quantity;

      amount.val(result).trigger("input");
    };

    // Handle get status
    getStatus = (status) => {
      var output = 'Undefined';

      switch (status) {
        case "0":
          output = "Draft";
          break;
        case "1":
          output = "Menunggu Persetujuan Kabid";
          break;
        case "2":
          output = "Ditolak Kabid";
          break;
        case "3":
          output = "Menunggu Persetujuan Bendahara";
          break;
        case "4":
          output = "Ditolak Bendahara";
          break;
        case "5":
          output = "Proses Pembelian Buku";
          break;
        case "6":
          output = "Selesai";
          break;
        default:
          break;
      };

      return output;
    };

    // Handle number format
    formatNumber = (num) => {
      return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
    };

    // Handle fetch item
    fetchItem = (id, tableLayout = _table_item) => {
      $.ajax({
        url: "<?php echo base_url('pengajuan/ajax_get_item/') ?>" + id,
        type: "get",
        dataType: "json",
        success: function(response) {
          $("#" + tableLayout).dataTable().fnClearTable();

          if (response.length > 0) {
            $("#" + tableLayout).dataTable().fnAddData(response);
          };
        }
      });
    };

  });
</script>