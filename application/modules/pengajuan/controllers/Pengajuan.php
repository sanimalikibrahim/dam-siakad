<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . 'controllers/AppBackend.php');

class Pengajuan extends AppBackend
{
  function __construct()
  {
    parent::__construct();
    $this->load->model([
      'AppModel',
      'PengajuanModel',
      'PengajuanitemModel',
      'PengajuanhistoryModel'
    ]);
    $this->load->library('form_validation');
  }

  public function index()
  {
    show_404();
  }

  public function buku()
  {
    $data = array(
      'app' => $this->app(),
      'main_js' => $this->load_main_js('pengajuan'),
      'card_title' => 'Pengajuan › Buku Perpustakaan'
    );
    $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
    $this->template->load_view('index', $data, TRUE);
    $this->template->render();
  }

  public function ajax_get_all()
  {
    $this->handle_ajax_request();
    $dtAjax_config = array(
      'table_name' => 'pengajuan',
      'order_column' => 4,
      'order_column_dir' => 'desc',
      'static_conditional' => array(
        'jenis' => 'Buku Perpustakaan',
        'created_by' => $this->session->userdata('user')['id']
      )
    );
    $response = $this->AppModel->getData_dtAjax($dtAjax_config);
    echo json_encode($response);
  }

  public function ajax_save($id = null)
  {
    $this->handle_ajax_request();
    $this->form_validation->set_rules($this->PengajuanModel->rules($id));

    if ($this->form_validation->run() === true) {
      if (is_null($id)) {
        echo json_encode($this->PengajuanModel->insert());
      } else {
        echo json_encode($this->PengajuanModel->update($id));
      };
    } else {
      $errors = validation_errors('<div>- ', '</div>');
      echo json_encode(array('status' => false, 'data' => $errors));
    };
  }

  public function ajax_set_status($id = null, $status = null)
  {
    $this->handle_ajax_request();

    if (!is_null($id) && !is_null($status)) {
      $pengajuanItem = $this->PengajuanitemModel->getAll(['pengajuan_id' => $id]);

      if (count($pengajuanItem) > 0) {
        $setStatus = $this->PengajuanModel->setStatus($id, $status);
        $pengajuan = $this->PengajuanModel->getDetail(['id' => $id]);
        $userFullName = $this->session->userdata('user')['nama_lengkap'];

        if ($setStatus['status'] === true) {
          $action = 'undefined';
          $description = 'undefined';
          $notification_next_to = [];
          $notification_status_next = null;
          $notification_link_next = null;

          switch ($status) {
            case '1':
              $action = 'Sent';
              $description = 'melakukan pengajuan.';
              $notification_next_to = ['Kabid'];
              $notification_status_next = '<span style="color: #2196F3;">' . $userFullName . '</span> mengajukan permohonan ' . $pengajuan->nomor . '.';
              $notification_link_next = 'persetujuan/buku/detail/' . $id;
              break;
            case '6':
              $action = 'Sent';
              $description = 'mengirim laporan pembelian.';
              $notification_next_to = ['Kabid', 'Staff Bendahara', 'Staff Keuangan'];
              $notification_status_next = '<span style="color: #2196F3;">' . $userFullName . '</span> membuat laporan pembelian untuk permohonan ' . $pengajuan->nomor . '.';
              $notification_link_next = 'laporan/pembelian-buku/?nomor=' . $pengajuan->nomor;
              break;
            default:
              break;
          };

          // Log
          $log_data = array(
            'pengajuan_id' => $id,
            'user_pic' => $this->session->userdata('user')['id'],
            'action' => $action,
            'description' => $description,
            'revisi' => $pengajuan->revisi
          );
          $log = $this->PengajuanhistoryModel->insert($log_data);
          // END ## Log

          if ($log['status'] === true) {
            // // Notification
            // To Next Role Receiver
            if (count($notification_next_to) > 0) {
              foreach ($notification_next_to as $key => $item) {
                $notification_data = array(
                  'ref' => 'pengajuanbuku',
                  'ref_id' => $id,
                  'description' => $notification_status_next,
                  'link' => $notification_link_next
                );
                $this->set_notification($notification_data, $item);
              };
            };
            // END ## Notification

            $response = $setStatus;
          } else {
            $response = $log;
          };
        } else {
          $response = $setStatus;
        };
      } else {
        $response = array('status' => false, 'data' => 'Please add items first.');
      };
    } else {
      $response = array('status' => false, 'data' => '(System) Parameters is bad.');
    };

    echo json_encode($response);
  }

  public function ajax_get_history($id)
  {
    $this->handle_ajax_request();
    echo json_encode($this->PengajuanhistoryModel->getAll(['pengajuan_id' => $id], 'id', 'desc'));
  }

  public function ajax_delete($id)
  {
    $this->handle_ajax_request();
    echo json_encode($this->PengajuanModel->delete($id));
  }

  public function ajax_get_nomor()
  {
    $this->handle_ajax_request();
    echo json_encode($this->generatePengajuanNomor());
  }

  public function ajax_get_item($id)
  {
    $this->handle_ajax_request();
    echo json_encode($this->PengajuanitemModel->getAll(['pengajuan_id' => $id]));
  }

  public function ajax_save_item($id = null)
  {
    $this->handle_ajax_request();
    $this->form_validation->set_rules($this->PengajuanitemModel->rules());

    if ($this->form_validation->run() === true) {
      if (is_null($id)) {
        echo json_encode($this->PengajuanitemModel->insert());
      } else {
        echo json_encode($this->PengajuanitemModel->update($id));
      };
    } else {
      $errors = validation_errors('<div>- ', '</div>');
      echo json_encode(array('status' => false, 'data' => $errors));
    };
  }

  public function ajax_delete_item($id)
  {
    $this->handle_ajax_request();
    echo json_encode($this->PengajuanitemModel->delete($id));
  }

  public function ajax_get_report($id)
  {
    $this->handle_ajax_request();

    if (!is_null($id)) {
      $pengajuan = $this->PengajuanModel->getDetail(['id' => $id]);
      $pengajuanItem = $this->PengajuanitemModel->getAll(['pengajuan_id' => $id]);
      $pengajuan_ttd = $this->_getTtd($id);

      $this->load->view('report_pembelian', array(
        'controller' => $this,
        'pengajuan' => $pengajuan,
        'pengajuan_item' => $pengajuanItem,
        'pengajuan_ttd' => $pengajuan_ttd
      ));
    } else {
      show_404();
    };
  }

  private function _getTtd($id)
  {
    $staff_perpustakaan =  $this->PengajuanModel->checkTtd($id, 'Sent', 'Staff Perpustakaan');
    $kabid =  $this->PengajuanModel->checkTtd($id, 'Approve', 'Kabid');
    $staff_bendahara =  $this->PengajuanModel->checkTtd($id, 'Approve', 'Staff Bendahara');

    $result = array(
      'staff_perpustakaan' => $staff_perpustakaan,
      'kabid' => $kabid,
      'staff_bendahara' => $staff_bendahara,
    );

    return (object) $result;
  }
}
