<div class="modal fade" id="modal-form-mappingortu" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-dialog-centered modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title pull-left">Mapping Orang Tua</h5>
      </div>
      <div class="spinner">
        <div class="lds-hourglass"></div>
      </div>
      <div class="modal-body">
        <form id="form-mappingortu" autocomplete="off">
          <!-- CSRF -->
          <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />

          <div class="row">
            <div class="col-sm-5">
              <div class="form-group">
                <label required>Orang Tua</label>
                <div class="select">
                  <select name="ortu" class="form-control select2 mappingortu-ortu" style="width: 100%;" required>
                    <?= $list_ortu ?>
                  </select>
                  <i class="form-group__bar"></i>
                </div>
              </div>
            </div>
            <div class="col-sm-5 col-md-4 col-lg-5">
              <div class="form-group">
                <label required>Santri</label>
                <div class="select">
                  <select name="santri" class="form-control select2 mappingortu-santri" search-placeholder="Search by NISN or Nama Lengkap" required>
                    <?= $list_santri ?>
                  </select>
                  <i class="form-group__bar"></i>
                </div>
              </div>
            </div>
            <div class="col-sm-2 col-md-3 col-lg-2">
              <div class="form-group">
                <label class="d-none d-sm-block">&nbsp;</label>
                <a href="javascript:;" class="btn btn-secondary btn-sm btn-block mappingortu-add-item">Add to List</a>
              </div>
            </div>
          </div>

          <div class="table-responsive">
            <table id="table-mappingortu-item" class="table table-bordered table-sm">
              <thead>
                <tr>
                  <th>Nama Lengkap</th>
                  <th>NISN</th>
                  <th>Nomor Induk Lokal</th>
                  <th>Asrama</th>
                  <th>Kelas</th>
                  <th width="80">Action</th>
                </tr>
              </thead>
              <tbody class="table-mappingortu-item-lists"></tbody>
            </table>
          </div>

        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success btn--icon-text mappingortu-action-save">
          <i class="zmdi zmdi-save"></i> Save
        </button>
        <button type="button" class="btn btn-light btn--icon-text mappingortu-action-cancel" data-dismiss="modal">
          Cancel
        </button>
      </div>
    </div>
  </div>
</div>

<style type="text/css">
  #table-mappingortu-item_wrapper {
    margin: 0 !important;
  }
</style>