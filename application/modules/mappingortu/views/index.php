<section id="mappingortu">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title"><?php echo (isset($card_title)) ? $card_title : '' ?></h4>
            <h6 class="card-subtitle"><?php echo (isset($card_subTitle)) ? $card_subTitle : '' ?></h6>

            <div class="table-action">
                <div class="buttons">
                    <button class="btn btn--raised btn-primary btn--icon-text mappingortu-action-add" data-toggle="modal" data-target="#modal-form-mappingortu">
                        <i class="zmdi zmdi-plus-circle"></i> Add New
                    </button>
                </div>
            </div>

            <?php include_once('form.php') ?>

            <div class="table-responsive">
                <table id="table-mappingortu" class="table table-bordered">
                    <thead class="thead-default">
                        <tr>
                            <th width="100">No</th>
                            <th>Orang Tua</th>
                            <th width="250">Username</th>
                            <th width="150">Jumlah Santri</th>
                            <th width="150">Status</th>
                            <th width="180">Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</section>