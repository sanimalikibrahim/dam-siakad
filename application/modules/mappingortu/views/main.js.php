<script type="text/javascript">
  $(document).ready(function() {

    var _key = "";
    var _section = "mappingortu";
    var _table = "table-mappingortu";
    var _table_item = "table-mappingortu-item";
    var _modal = "modal-form-mappingortu";
    var _form = "form-mappingortu";
    var _santriSelected = [];
    var _isForceSubmit = false;

    // Initialize DataTables: Index
    if ($("#" + _table)[0]) {
      var table_mappingortu = $("#" + _table).DataTable({
        processing: true,
        serverSide: true,
        ajax: {
          url: "<?php echo base_url('mappingortu/ajax_get_all/') ?>",
          type: "get"
        },
        columns: [{
            data: null,
            render: function(data, type, row, meta) {
              return meta.row + meta.settings._iDisplayStart + 1;
            }
          },
          {
            data: "nama_lengkap",
          },
          {
            data: "username",
          },
          {
            data: "jumlah_santri",
            render: function(data) {
              return data + " Orang";
            }
          },
          {
            data: "status",
            render: function(data) {
              if (data === "Draft") {
                return `<span class="text-red">${data}</span>`;
              } else {
                return data;
              };
            }
          },
          {
            data: null,
            className: "center",
            render: function(data, type, row, meta) {
              var buttons = `
                <div class="action">
                  <a href="javascript:;" class="btn btn-sm btn-light btn-table-action action-edit" data-toggle="modal" data-target="#${_modal}"><i class="zmdi zmdi-edit"></i> Edit</a>&nbsp;
                  <a href="javascript:;" class="btn btn-sm btn-light btn-table-action action-delete"><i class="zmdi zmdi-delete"></i> Delete</a>
                `;

              // if (row.status === "Draft") {
              //   buttons += `&nbsp;<a href="javascript:;" class="btn btn-sm btn-success btn-table-action action-publish"><i class="zmdi zmdi-play"></i> Publish</a>`;
              // };

              buttons += `</div>`;
              return buttons;
            },
          }
        ],
        autoWidth: !1,
        responsive: {
          details: {
            renderer: function(api, rowIdx, columns) {
              var hideColumn = [];
              var data = $.map(columns, function(col, i) {
                return ($.inArray(col.columnIndex, hideColumn)) ?
                  '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                  '<td class="dt-details-td">' + col.title + ':' + '</td> ' +
                  '<td class="dt-details-td">' + col.data + '</td>' +
                  '</tr>' :
                  '';
              }).join('');

              return data ? $('<table/>').append(data) : false;
            },
            type: "inline",
            target: 'tr',
          }
        },
        columnDefs: [{
          className: 'desktop',
          targets: [0, 1, 2, 3, 4, 5]
        }, {
          className: 'tablet',
          targets: [0, 1, 2, 3]
        }, {
          className: 'mobile',
          targets: [0, 1]
        }, {
          responsivePriority: 1,
          targets: -1
        }],
        pageLength: 15,
        language: {
          searchPlaceholder: "Search...",
          sProcessing: '<div style="text-align: center;"><div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div></div>'
        },
        sDom: '<"dataTables_ct"><"dataTables__top"fb>rt<"dataTables__bottom"ip><"clear">',
        buttons: [{
          extend: "excelHtml5",
          title: "Export Result"
        }, {
          extend: "print",
          title: "Export Result"
        }],
        initComplete: function(a, b) {
          $(this).closest(".dataTables_wrapper").find(".dataTables__top").prepend(
            '<div class="dataTables_buttons hidden-sm-down actions">' +
            '<span class="actions__item zmdi zmdi-refresh" data-table-action="reload" title="Reload" />' +
            '</div>'
          );
        },
      });

      $(".dataTables_filter input[type=search]").focus(function() {
        $(this).closest(".dataTables_filter").addClass("dataTables_filter--toggled")
      });

      $(".dataTables_filter input[type=search]").blur(function() {
        $(this).closest(".dataTables_filter").removeClass("dataTables_filter--toggled")
      });

      $("body").on("click", "[data-table-action]", function(a) {
        a.preventDefault();
        var b = $(this).data("table-action");
        if ("reload" === b) {
          $("#" + _table).DataTable().ajax.reload(null, false);
        };
      });
    };

    // Initialize DataTables: Item
    if ($("#" + _table_item)[0]) {
      var table_mappingortu_item = $("#" + _table_item).DataTable();
    };

    // Handle data add
    $("#" + _section).on("click", "button." + _section + "-action-add", function(e) {
      e.preventDefault();
      resetForm();

      // Reinitialize select2 ortu
      $("#" + _form + " ." + _section + "-ortu option:not(:first)").prop("disabled", false);
      $("#" + _form + " ." + _section + "-ortu").select2();
    });

    // Handle data edit
    $("#" + _table).on("click", "a.action-edit", function(e) {
      e.preventDefault();
      resetForm();

      var temp = table_mappingortu.row($(this).closest('tr')).data();
      var ortuSelected = temp.ortu_id;
      var tempData = [];

      // Set key for update params, important!
      _key = temp.ortu_id;

      // Get existing item
      $.ajax({
        type: "get",
        url: "<?php echo base_url('mappingortu/ajax_get_item/') ?>" + temp.ortu_id,
        dataType: "json",
        success: function(response) {
          if (response.length > 0) {
            response.map(function(item) {
              addRowToList(item.santri_id, item.nama_lengkap_santri, item.nisn, item.nomor_induk_lokal, item.asrama, `${item.kelas} ${item.sub_kelas}`);
            });
          };
        }
      });


      $("#" + _form + " ." + _section + "-ortu").val(ortuSelected).trigger("change");

      // Reinitialize select2 ortu
      $("#" + _form + " ." + _section + "-ortu option[value='" + ortuSelected + "']").prop("disabled", false);
      $("#" + _form + " ." + _section + "-ortu option:not(:selected)").prop("disabled", true);
      $("#" + _form + " ." + _section + "-ortu").select2();
    });

    // Handle data submit
    $("#" + _modal + " ." + _section + "-action-save").on("click", function(e) {
      e.preventDefault();
      $.ajax({
        type: "post",
        url: "<?php echo base_url('mappingortu/ajax_save/') ?>" + _isForceSubmit,
        data: $("#" + _form).serialize(),
        success: function(response) {
          var response = JSON.parse(response);
          if (response.status === true) {
            resetForm();
            $("#" + _modal).modal("hide");
            $("#" + _table).DataTable().ajax.reload(null, false);
            notify(response.data, "success");
          } else {
            if (response.isExists === true) {
              var ortuSelected = $("#" + _form + " ." + _section + "-ortu option:selected").text();

              swal({
                title: `Orang Tua "${ortuSelected}" already has data, update it?`,
                text: "Once updated, you will not be able to recover this data!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: "Yes",
                cancelButtonText: "No",
                closeOnConfirm: false
              }).then((result) => {
                if (result.value) {
                  _isForceSubmit = true;
                  $("#" + _modal + " ." + _section + "-action-save").click();
                };
              });
            } else {
              notify(response.data, "danger");
            };
          };
        }
      });
    });

    // Handle data delete
    $("#" + _table).on("click", "a.action-delete", function(e) {
      e.preventDefault();
      var temp = table_mappingortu.row($(this).closest('tr')).data();

      swal({
        title: "Are you sure to delete?",
        text: "Once deleted, you will not be able to recover this data!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false
      }).then((result) => {
        if (result.value) {
          $.ajax({
            type: "delete",
            url: "<?php echo base_url('mappingortu/ajax_delete/') ?>" + temp.ortu_id,
            dataType: "json",
            success: function(response) {
              if (response.status) {
                $("#" + _table).DataTable().ajax.reload(null, false);
                notify(response.data, "success");
              } else {
                notify(response.data, "danger");
              };
            }
          });
        };
      });
    });

    // Handle data publish
    $("#" + _table).on("click", "a.action-publish", function(e) {
      e.preventDefault();
      var temp = table_mappingortu.row($(this).closest('tr')).data();

      swal({
        title: "Are you sure to publish?",
        text: "Once published, you will not be able to recover this data!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false
      }).then((result) => {
        if (result.value) {
          $.ajax({
            type: "get",
            url: "<?php echo base_url('mappingortu/ajax_publish/') ?>" + temp.ortu_id,
            dataType: "json",
            success: function(response) {
              if (response.status) {
                $("#" + _table).DataTable().ajax.reload(null, false);
                notify(response.data, "success");
              } else {
                notify(response.data, "danger");
              };
            }
          });
        };
      });
    });

    // Handle form reset
    resetForm = () => {
      _key = "";
      table_mappingortu_item.clear().draw();
      _santriSelected = [];
      _isForceSubmit = false;

      $("#" + _form).trigger("reset");
      $("#" + _form + " ." + _section + "-ortu").val("-1").trigger("change");
      $("#" + _form + " ." + _section + "-santri").val("-1").trigger("change");
      $("#" + _form + " ." + _section + "-ortu").select2();
      $("#" + _form + " ." + _section + "-ortu").select2("destroy");
    };

    // Handle add item
    $("#" + _modal + " ." + _section + "-add-item").on("click", function() {
      var santri = $("#" + _modal + " ." + _section + "-santri");
      var santriData = santri.select2().find(":selected").data();
      var santriId = santriData.data.id;
      var santriName = santriData.fullname;
      var santriNisn = santriData.nisn;
      var santriNil = santriData.nil;
      var santriAsrama = santriData.asrama;
      var santriKelas = `${santriData.kelas} ${santriData.subkelas}`;

      addRowToList(santriId, santriName, santriNisn, santriNil, santriAsrama, santriKelas);
    });

    // Handle remove item
    $("#" + _table_item + " tbody").on("click", "a.maapingkelas-remove-item", function(e) {
      e.preventDefault();
      var temp = table_mappingortu_item.row($(this).closest('tr')).data();

      table_mappingortu_item.row($(this).parents('tr')).remove().draw();
      _santriSelected = $.grep(_santriSelected, function(value) {
        return value != temp[1].toString();
      });
    });

    // Handle add item to grid
    function addRowToList(santriId, santriName, santriNisn, santriNil, santriAsrama, santriKelas) {
      if (santriId !== undefined && santriId !== null && santriId !== "-1") {
        if ($.inArray(santriNisn.toString(), _santriSelected) === -1) {
          var newRecord = `
          <tr>
            <td>
              ${santriName}
              <input type="hidden" name="santri_id[]" class="mappingortu-santri_id" value="${santriId}" readonly />
            </td>
            <td>${santriNisn}</td>
            <td>${santriNil}</td>
            <td>${santriAsrama}</td>
            <td>${santriKelas}</td>
            <td class="text-center" style="width: 80px;">
              <a href="javascript:;" class="btn btn-secondary btn-sm btn-warning maapingkelas-remove-item">
                <i class="zmdi zmdi-delete"></i>
              </a>
            </td>
          </tr>
        `;

          table_mappingortu_item.row.add($(newRecord)).draw();
          _santriSelected.push(santriNisn.toString());
          $("#" + _form + " ." + _section + "-santri").val("-1").trigger("change");
        } else {
          notify(`Santri "${santriNisn} | ${santriName}" already exist!`, "warning");
        };
      } else {
        notify("Please select the santri first!", "warning");
      };
    };

  });
</script>