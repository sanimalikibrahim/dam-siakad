<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . 'controllers/AppBackend.php');

class Mappingortu extends AppBackend
{
  function __construct()
  {
    parent::__construct();
    $this->load->model([
      'AppModel',
      'MappingOrtuModel',
    ]);
    $this->load->library('form_validation');
  }

  public function index()
  {
    $data = array(
      'app' => $this->app(),
      'main_js' => $this->load_main_js('mappingortu'),
      'card_title' => 'Mapping Orang Tua',
      'list_ortu' => $this->init_list_ortu(),
      'list_santri' => $this->init_list_santri(),
    );
    $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
    $this->template->load_view('index', $data, TRUE);
    $this->template->render();
  }

  public function ajax_get_all()
  {
    $this->handle_ajax_request();

    $dtAjax_config = array(
      'table_name' => 'view_mapping_ortu_head',
      'order_column' => 1,
      'order_column_dir' => 'asc'
    );
    $response = $this->AppModel->getData_dtAjax($dtAjax_config);
    echo json_encode($response);
  }

  public function ajax_get_item($ortuId = null)
  {
    $this->handle_ajax_request();
    echo json_encode($this->MappingOrtuModel->getAll(array('ortu_id' => $ortuId)));
  }

  public function ajax_save($isForce = false)
  {
    $this->handle_ajax_request();

    $isForce = filter_var($isForce, FILTER_VALIDATE_BOOLEAN);
    $ortu = $this->input->post('ortu');
    $listSantri = $this->input->post('santri_id');

    if (!is_null($ortu) && !is_null($ortu)) {
      if (!is_null($listSantri) && count($listSantri) > 0) {
        $isExists = $this->MappingOrtuModel->getAll(array('ortu_id' => $ortu));

        if (count($isExists) > 0 && $isForce === false) {
          echo json_encode(array('status' => false, 'data' => 'Orang tua already exist!', 'isExists' => true));
        } else {
          $tempData = array();

          foreach ($listSantri as $key => $santriId) {
            $tempData[] = array(
              'ortu_id' => $ortu,
              'santri_id' => $santriId,
              'status' => 'Publish',
              'created_by' => $this->session->userdata('user')['id']
            );
          };

          if ($isForce === true) {
            $this->MappingOrtuModel->deleteByOrtu($ortu);
          };

          echo json_encode($this->MappingOrtuModel->insertBatch($tempData));
        };
      } else {
        echo json_encode(array('status' => false, 'data' => 'Please add Santri first!'));
      };
    } else {
      echo json_encode(array('status' => false, 'data' => 'The Orang Tua field is required.'));
    };
  }

  public function ajax_delete($ortuId = null)
  {
    $this->handle_ajax_request();
    echo json_encode($this->MappingOrtuModel->deleteByOrtu($ortuId));
  }

  public function ajax_publish($ortuId = null)
  {
    $this->handle_ajax_request();
    echo json_encode($this->MappingOrtuModel->publishByOrtu($ortuId));
  }
}
