<script type="text/javascript">
  $(document).ready(function() {

    var _key = "";
    var _section = "persetujuanbuku";
    var _table = "table-persetujuanbuku";
    var _table_detail_item = "table-persetujuanbuku-detail-item";
    var _persetujuanRefId = $("#" + _section + " .persetujuan-pengajuan_id").val();
    var _modalApproval = "modal-form-persetujuanbuku";
    var _formApproval = "form-persetujuanbuku";

    // Initialize DataTables: Index
    if ($("#" + _table)[0]) {
      var table_persetujuanbuku = $("#" + _table).DataTable({
        processing: true,
        serverSide: true,
        ajax: {
          url: "<?php echo base_url('persetujuan/ajax_get_all/') ?>",
          type: "get"
        },
        columns: [{
            data: null,
            render: function(data, type, row, meta) {
              var nomor = meta.row + meta.settings._iDisplayStart + 1;

              if ($.inArray(row.status, ["2", "4"]) !== -1) {
                return '<span style="color: #ff6b68;">' + nomor + '</span>';
              } else if ($.inArray(row.status, ["5"]) !== -1) {
                return '<span style="color: #FF9800;">' + nomor + '</span>';
              } else {
                return '<span>' + nomor + '</span>';
              };
            }
          },
          {
            data: "nomor",
            render: function(data, type, row, meta) {
              if ($.inArray(row.status, ["2", "4"]) !== -1) {
                return '<span style="color: #ff6b68;">' + data + '</span>';
              } else if ($.inArray(row.status, ["5"]) !== -1) {
                return '<span style="color: #FF9800;">' + data + '</span>';
              } else {
                return '<span>' + data + '</span>';
              };
            }
          },
          {
            data: "tanggal",
            render: function(data, type, row, meta) {
              if ($.inArray(row.status, ["2", "4"]) !== -1) {
                return '<span style="color: #ff6b68;">' + data + '</span>';
              } else if ($.inArray(row.status, ["5"]) !== -1) {
                return '<span style="color: #FF9800;">' + data + '</span>';
              } else {
                return '<span>' + data + '</span>';
              };
            }
          },
          {
            data: "status",
            render: function(data, type, row, meta) {
              var status = getStatus(data);

              if ($.inArray(data, ["2", "4"]) !== -1) {
                return '<span style="color: #ff6b68;">' + status + '</span>';
              } else if ($.inArray(row.status, ["5"]) !== -1) {
                return '<span style="color: #FF9800;">' + status + '</span>';
              } else {
                return '<span>' + status + '</span>';
              };
            }
          },
          {
            data: "created_at",
            render: function(data, type, row, meta) {
              if ($.inArray(row.status, ["2", "4"]) !== -1) {
                return '<span style="color: #ff6b68;">' + data + '</span>';
              } else if ($.inArray(row.status, ["5"]) !== -1) {
                return '<span style="color: #FF9800;">' + data + '</span>';
              } else {
                return '<span>' + data + '</span>';
              };
            }
          },
          {
            data: null,
            className: "center",
            render: function(data, type, row, meta) {
              var output = '';

              output += '<div class="action">';
              output += '<a href="<?= base_url('persetujuan/buku/detail/') ?>' + row.id + '" class="btn btn-sm btn-light btn-table-action"><i class="zmdi zmdi-eye"></i> Detail</a>';
              output += '</div>';

              return output;
            }
          }
        ],
        autoWidth: !1,
        responsive: {
          details: {
            renderer: function(api, rowIdx, columns) {
              var hideColumn = [];
              var data = $.map(columns, function(col, i) {
                return ($.inArray(col.columnIndex, hideColumn)) ?
                  '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                  '<td class="dt-details-td">' + col.title + ':' + '</td> ' +
                  '<td class="dt-details-td">' + col.data + '</td>' +
                  '</tr>' :
                  '';
              }).join('');

              return data ? $('<table/>').append(data) : false;
            },
            type: "inline",
            target: 'tr',
          }
        },
        columnDefs: [{
          className: 'desktop',
          targets: [0, 1, 2, 3, 4, 5]
        }, {
          className: 'tablet',
          targets: [0, 1, 2, 3]
        }, {
          className: 'mobile',
          targets: [0, 1]
        }, {
          responsivePriority: 2,
          targets: -1
        }],
        pageLength: 15,
        language: {
          searchPlaceholder: "Search...",
          sProcessing: '<div style="text-align: center;"><div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div></div>'
        },
        sDom: '<"dataTables_ct"><"dataTables__top"fb>rt<"dataTables__bottom"ip><"clear">',
        buttons: [{
          extend: "excelHtml5",
          title: "Export Result"
        }, {
          extend: "print",
          title: "Export Result"
        }],
        initComplete: function(a, b) {
          $(this).closest(".dataTables_wrapper").find(".dataTables__top").prepend(
            '<div class="dataTables_buttons hidden-sm-down actions">' +
            '<span class="actions__item zmdi zmdi-refresh" data-table-action="reload" title="Reload" />' +
            '</div>'
          );
        },
      });

      $(".dataTables_filter input[type=search]").focus(function() {
        $(this).closest(".dataTables_filter").addClass("dataTables_filter--toggled")
      });

      $(".dataTables_filter input[type=search]").blur(function() {
        $(this).closest(".dataTables_filter").removeClass("dataTables_filter--toggled")
      });

      $("body").on("click", "[data-table-action]", function(a) {
        a.preventDefault();
        var b = $(this).data("table-action");
        if ("reload" === b) {
          $("#" + _table).DataTable().ajax.reload(null, false);
        };
      });
    };

    // Intialize DataTables: Item
    if ($("#" + _table_detail_item)[0]) {
      var table_persetujuanbuku = $("#" + _table_detail_item).DataTable({
        processing: true,
        serverSide: true,
        ajax: {
          url: "<?php echo base_url('persetujuan/ajax_get_item/') ?>" + _persetujuanRefId,
          type: "get"
        },
        columns: [{
            data: null,
            render: function(data, type, row, meta) {
              return meta.row + meta.settings._iDisplayStart + 1;
            }
          },
          {
            data: 'item'
          },
          {
            data: 'quantity',
            render: function(data, type, row, meta) {
              return meta.settings.fnFormatNumber(data);
            }
          },
          {
            data: 'unit'
          },
          {
            data: 'unit_price',
            render: function(data, type, row, meta) {
              return "Rp " + meta.settings.fnFormatNumber(data);
            }
          },
          {
            data: 'amount',
            render: function(data, type, row, meta) {
              return "Rp " + meta.settings.fnFormatNumber(data);
            }
          }
        ],
        responsive: {
          details: {
            renderer: function(api, rowIdx, columns) {
              var hideColumn = [];
              var data = $.map(columns, function(col, i) {
                return ($.inArray(col.columnIndex, hideColumn)) ?
                  '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                  '<td class="dt-details-td">' + col.title + ':' + '</td> ' +
                  '<td class="dt-details-td">' + col.data + '</td>' +
                  '</tr>' :
                  '';
              }).join('');

              return data ? $('<table/>').append(data) : false;
            },
            type: "inline",
            target: 'tr',
          }
        },
        columnDefs: [{
          className: 'desktop',
          targets: [0, 1, 2, 3, 4, 5]
        }, {
          className: 'tablet',
          targets: [0, 1, 2, 4]
        }, {
          className: 'mobile',
          targets: [0, 1]
        }, {
          responsivePriority: 2,
          targets: -1
        }],
        autoWidth: !1,
        pageLength: 15,
        language: {
          searchPlaceholder: "Search...",
          sProcessing: '<div style="text-align: center;"><div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div></div>'
        },
        sDom: '<"dataTables_ct"><"dataTables__top"fb>rt<"dataTables__bottom"ip><"clear">',
        buttons: [{
          extend: "excelHtml5",
          title: "Export Result"
        }, {
          extend: "print",
          title: "Export Result"
        }],
        initComplete: function(a, b) {
          $(this).closest(".dataTables_wrapper").find(".dataTables__top").prepend(
            '<div class="dataTables_buttons hidden-sm-down actions">' +
            '<span class="actions__item zmdi zmdi-refresh" data-table-action="reload" title="Reload" />' +
            '</div>'
          );
        },
      });

      $(".dataTables_filter input[type=search]").focus(function() {
        $(this).closest(".dataTables_filter").addClass("dataTables_filter--toggled")
      });

      $(".dataTables_filter input[type=search]").blur(function() {
        $(this).closest(".dataTables_filter").removeClass("dataTables_filter--toggled")
      });

      $("body").on("click", "[data-table-action]", function(a) {
        a.preventDefault();
        var b = $(this).data("table-action");
        if ("reload" === b) {
          $("#" + _table_detail_item).DataTable().ajax.reload(null, false);
        };
      });
    };

    // Set modal approval
    $("#" + _section).on("click", "button." + _section + "-page-action", function(e) {
      e.preventDefault();
      var dataStatus = $(this).attr("data-status");
      var statusName = $(this).attr("data-status-name");

      setModalApproval(dataStatus, statusName);
    });

    // Handle data send
    $("#" + _modalApproval).on("click", "button." + _section + "-action-button", function(e) {
      e.preventDefault();
      $.ajax({
        url: "<?php echo base_url('persetujuan/ajax_set_status/') ?>",
        type: "post",
        data: $("#" + _formApproval).serialize(),
        dataType: "json",
        success: function(response) {
          if (response.status === true) {
            $("#" + _modalApproval).modal("hide");
            swal({
              title: "Success",
              text: "Your data has been sent successfully.",
              type: "success",
            }).then((okay) => {
              if (okay) {
                window.location.href = "<?= base_url('persetujuan/buku') ?>";
              };
            });
          } else {
            notify(response.data, "danger");
          };
        }
      });
    });

    // Handle modal approval
    setModalApproval = (dataStatus, statusName) => {
      var status = $("#" + _formApproval + " ." + _section + "-status");
      var note = $("#" + _formApproval + " ." + _section + "-note");
      var button = $("#" + _modalApproval + " ." + _section + "-action-button");
      var buttonIcon = $("#" + _modalApproval + " ." + _section + "-action-button-icon");
      var buttonLabel = $("#" + _modalApproval + " ." + _section + "-action-button-label");

      note.val("").trigger("input");
      note.css("height", "31px").keyup();
      status.val(dataStatus);

      if (statusName === "approve") {
        button.addClass("btn-primary").removeClass("btn-danger");
        buttonIcon.addClass("zmdi-thumb-up").removeClass("zmdi-close-circle");
        buttonLabel.html("Approve");
      } else {
        button.addClass("btn-danger").removeClass("btn-primary");
        buttonIcon.addClass("zmdi-close-circle").removeClass("zmdi-thumb-up");
        buttonLabel.html("Reject");
      };
    };

    // Handle get status
    getStatus = (status) => {
      var output = 'Undefined';

      switch (status) {
        case "0":
          output = "Draft";
          break;
        case "1":
          output = "Menunggu Persetujuan Kabid";
          break;
        case "2":
          output = "Ditolak Kabid";
          break;
        case "3":
          output = "Menunggu Persetujuan Bendahara";
          break;
        case "4":
          output = "Ditolak Bendahara";
          break;
        case "5":
          output = "Proses Pembelian Buku";
          break;
        case "6":
          output = "Selesai";
          break;
        default:
          break;
      };

      return output;
    };

  });
</script>