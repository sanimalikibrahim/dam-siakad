<div class="modal fade" id="modal-form-persetujuanbuku" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title pull-left">Konfirmasi</h5>
      </div>
      <div class="spinner">
        <div class="lds-hourglass"></div>
      </div>
      <div class="modal-body">
        <form id="form-persetujuanbuku" autocomplete="off">
          <!-- CSRF -->
          <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />

          <input type="hidden" name="pengajuan_id" class="persetujuan-pengajuan_id" value="<?= $data_pengajuan->id ?>" readonly />
          <input type="hidden" name="status" class="persetujuanbuku-status" readonly />

          <div class="form-group">
            <label>Note</label>
            <textarea name="note" class="form-control textarea-autosize text-counter persetujuanbuku-note" rows="1" data-max-length="255" style="overflow: hidden; overflow-wrap: break-word; height: 31px;" placeholder="Optional, but required for reject..."></textarea>
            <i class="form-group__bar"></i>
          </div>

        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn--icon-text persetujuanbuku-action-button">
          <i class="zmdi persetujuanbuku-action-button-icon"></i>
          <span class="persetujuanbuku-action-button-label">Action</span>
        </button>
        <button type="button" class="btn btn-light btn--icon-text persetujuanbuku-action-button-cancel" data-dismiss="modal">
          Cancel
        </button>
      </div>
    </div>
  </div>
</div>