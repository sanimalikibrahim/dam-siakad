<style type="text/css">
  .form-control[readonly] {
    opacity: 1;
  }

  .form-control-label {
    width: 100%;
    padding: .375rem 0;
    border-bottom: 1px solid #eceff1;
  }

  .persetujuanbuku-hitory .alert {
    padding: 10px;
  }

  #table-persetujuanbuku-item_wrapper {
    margin-top: 0;
  }

  #table-persetujuanbuku-detail-item_wrapper {
    margin-top: 0;
  }

  .item-info {
    text-align: center;
    padding: 10rem 0rem;
  }

  .item-info p {
    line-height: .75rem;
    font-size: 1.075rem;
    color: #999;
    font-weight: 400;
  }

  .item-info .zmdi {
    font-size: 3.5rem;
    margin-bottom: 1rem;
    color: #c5c5c5;
  }
</style>

<section id="persetujuanbuku">
  <div class="card">
    <div class="card-body">
      <h4 class="card-title"><?php echo (isset($card_title)) ? $card_title : '' ?></h4>
      <h6 class="card-subtitle"><?php echo (isset($card_subTitle)) ? $card_subTitle : '' ?></h6>

      <?php include_once('form_approval.php') ?>

      <input type="hidden" class="persetujuan-pengajuan_id" value="<?= $data_pengajuan->id ?>" readonly />

      <div class="tab-container">
        <ul class="nav nav-tabs <?= ($app->is_mobile) ? 'nav-fill' : '' ?>" role="tablist">
          <li class="nav-item">
            <a class="nav-link active nav-link-active" data-toggle="tab" href="#tab-persetujuanbuku-data" role="tab">Data</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#tab-persetujuanbuku-history" role="tab">History</a>
          </li>
        </ul>
        <div class="tab-content clear-tab-content">
          <!-- Data -->
          <div class="tab-pane active fade show" id="tab-persetujuanbuku-data" role="tabpanel">
            <div class="form-group mb-3">
              <label><b>Nomor Pengajuan</b></label>
              <div class="form-control-label persetujuanbuku-nomor">
                <?= $data_pengajuan->nomor ?>
              </div>
            </div>

            <div class="form-group mb-3">
              <label><b>Tanggal Pengajuan</b></label>
              <div class="form-control-label persetujuanbuku-tanggal">
                <?= $data_pengajuan->tanggal ?>
              </div>
            </div>

            <div class="form-group mb-3">
              <label><b>Status</b></label>
              <div class="form-control-label persetujuanbuku-status">
                <?= $controller->getPengajuanStatus($data_pengajuan->status) ?>
              </div>
            </div>

            <!-- Grid -->
            <div class="table-responsive">
              <table id="table-persetujuanbuku-detail-item" class="table table-bordered table-sm" style="width: 100%;">
                <thead class="thead-default">
                  <tr>
                    <th width="100">No</th>
                    <th>Item</th>
                    <th>Quantity</th>
                    <th>Unit</th>
                    <th>Unit Price</th>
                    <th>Amount</th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
          <!-- History -->
          <div class="tab-pane fade show" id="tab-persetujuanbuku-history" role="tabpanel">
            <div class="persetujuanbuku-hitory">
              <?php if (count($data_pengajuan_history) > 0) : ?>
                <?php foreach ($data_pengajuan_history as $index => $item) : ?>
                  <?php $alertType = ($item->action === 'Sent' || $item->action === 'Approve') ? 'bg-light' : 'bg-red' ?>
                  <?php $alertTextColor = ($item->action === 'Sent' || $item->action === 'Approve') ? '#747a80' : '#f9f9f9' ?>
                  <div class="alert alert-info <?= $alertType ?> mb-1" style="color: <?= $alertTextColor ?>">
                    <b><?= $item->nama_lengkap ?></b> <?= $item->description ?>
                    <p class="mb-0"><small><?= $item->created_at ?></small></p>
                  </div>
                <?php endforeach; ?>
              <?php else : ?>
                <div class="item-info item-no-data">
                  <i class="zmdi zmdi-folder-outline"></i>
                  <p>Tidak ditemukan data</p>
                </div>
              <?php endif;  ?>
            </div>
          </div>
        </div>
      </div>

      <?php if ($is_button_active) : ?>
        <div class="border-top border-primary mt-4 text-right" style="padding-top: 1.5rem;">
          <button type="button" class="btn btn-primary btn--icon-text persetujuanbuku-page-action" data-toggle="modal" data-target="#modal-form-persetujuanbuku" data-status="<?= $data_status_approve ?>" data-status-name="approve">
            <i class="zmdi zmdi-thumb-up"></i> Approve
          </button>
          <button type="button" class="btn btn-danger btn--icon-text persetujuanbuku-page-action" data-toggle="modal" data-target="#modal-form-persetujuanbuku" data-status="<?= $data_status_reject ?>" data-status-name="reject">
            <i class="zmdi zmdi-close-circle"></i> Reject
          </button>
        </div>
      <?php endif; ?>
    </div>
  </div>
</section>