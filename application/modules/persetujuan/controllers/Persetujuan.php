<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . 'controllers/AppBackend.php');

class Persetujuan extends AppBackend
{
  function __construct()
  {
    parent::__construct();
    $this->load->model([
      'AppModel',
      'PengajuanModel',
      'PengajuanitemModel',
      'PengajuanhistoryModel'
    ]);
    $this->load->library('form_validation');
  }

  public function index()
  {
    show_404();
  }

  public function buku()
  {
    $data = array(
      'app' => $this->app(),
      'main_js' => $this->load_main_js('persetujuan'),
      'card_title' => 'Persetujuan › Buku Perpustakaan'
    );
    $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
    $this->template->load_view('index', $data, TRUE);
    $this->template->render();
  }

  public function buku_detail($id = null)
  {
    if (!is_null($id)) {
      $role = $this->session->userdata('user')['role'];
      $status = [1, 2, 3, 4, 5, 6];
      $buttonActive = false;
      $statusApprove = null;
      $statusReject = null;

      switch ($role) {
        case 'Kabid':
          $status = [1, 2, 3, 4, 5, 6];
          break;
        case 'Staff Bendahara':
          $status = [3, 4, 5, 6];
          break;
        default:
          break;
      };

      $pengajuan = $this->PengajuanModel->getDetail([], ['id' => $id, 'jenis' => 'Buku Perpustakaan', 'status' => $status]);
      $pengajuan_history = $this->PengajuanhistoryModel->getAll(['pengajuan_id' => $id], 'id', 'desc');

      if (!is_null($pengajuan)) {
        // Set button active
        if ($role === 'Kabid' && $pengajuan->status === '1') {
          $buttonActive = true;
        } else if ($role === 'Staff Bendahara' && $pengajuan->status === '3') {
          $buttonActive = true;
        };

        // Set data status
        if ($role === 'Kabid') {
          $statusApprove = 3;
          $statusReject = 2;
        } else if ($role === 'Staff Bendahara') {
          $statusApprove = 5;
          $statusReject = 4;
        };

        $data = array(
          'controller' => $this,
          'app' => $this->app(),
          'main_js' => $this->load_main_js('persetujuan'),
          'card_title' => 'Persetujuan › Buku Perpustakaan',
          'is_button_active' => $buttonActive,
          'data_pengajuan' => $pengajuan,
          'data_pengajuan_history' => $pengajuan_history,
          'data_ttd' => $this->_getTtd($id),
          'data_status_approve' => $statusApprove,
          'data_status_reject' => $statusReject
        );
        $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
        $this->template->load_view('detail', $data, TRUE);
        $this->template->render();
      } else {
        show_404();
      };
    } else {
      show_404();
    };
  }

  public function ajax_get_all()
  {
    $this->handle_ajax_request();

    $role = $this->session->userdata('user')['role'];
    $status = [1, 2, 3, 4, 5, 6];

    switch ($role) {
      case 'Kabid':
        $status = [1, 2, 3, 4, 5, 6];
        break;
      case 'Staff Bendahara':
        $status = [3, 4, 5, 6];
        break;
      default:
        break;
    };

    $dtAjax_config = array(
      'table_name' => 'pengajuan',
      'order_column' => 4,
      'order_column_dir' => 'desc',
      'static_conditional' => array(
        'jenis' => 'Buku Perpustakaan'
      ),
      'static_conditional_in_key' => 'status',
      'static_conditional_in' => $status
    );
    $response = $this->AppModel->getData_dtAjax($dtAjax_config);
    echo json_encode($response);
  }

  public function ajax_set_status($id = null, $status = null)
  {
    $this->handle_ajax_request();

    $id = $this->input->post('pengajuan_id');
    $status = $this->input->post('status');
    $note = $this->input->post('note');

    if (!is_null($id) && !is_null($status)) {
      if (in_array($status, [2, 4]) && empty($note)) {
        $response = array('status' => false, 'data' => 'Note field is required for reject.');
      } else {
        $setStatus = $this->PengajuanModel->setStatus($id, $status);
        $pengajuan = $this->PengajuanModel->getDetail(['id' => $id]);
        $userFullName = $this->session->userdata('user')['nama_lengkap'];

        if ($setStatus['status'] === true) {
          $action = 'undefined';
          $description = 'undefined';
          $notification_back_to = [];
          $notification_next_to = [];
          $notification_status = null;
          $notification_status_back = null;
          $notification_status_next = null;

          switch ($status) {
            case '2':
              $action = 'Reject';
              $description = 'menolak';
              $notification_back_to = [];
              $notification_next_to = [];
              $notification_status = '<span style="color: #2196F3;">' . $userFullName . '</span> menolak permohonan ' . $pengajuan->nomor . '.';
              $notification_status_back = null;
              $notification_status_next = null;
              break;
            case '3':
              $action = 'Approve';
              $description = 'menyetujui';
              $notification_back_to = [];
              $notification_next_to = ['Staff Bendahara'];
              $notification_status = '<span style="color: #2196F3;">' . $userFullName . '</span> menyetujui permohonan ' . $pengajuan->nomor . '.';
              $notification_status_back = null;
              $notification_status_next = '<span style="color: #2196F3;">' . $userFullName . '</span> mengajukan permohonan ' . $pengajuan->nomor . '.';
              break;
            case '4':
              $action = 'Reject';
              $description = 'menolak';
              $notification_back_to = ['Kabid'];
              $notification_next_to = [];
              $notification_status = '<span style="color: #2196F3;">' . $userFullName . '</span> menolak permohonan ' . $pengajuan->nomor . '.';
              $notification_status_back = '<span style="color: #2196F3;">' . $userFullName . '</span> menolak permohonan ' . $pengajuan->nomor . '.';
              $notification_status_next = null;
              break;
            case '5':
              $action = 'Approve';
              $description = 'menyetujui';
              $notification_back_to = ['Kabid'];
              $notification_next_to = [];
              $notification_status = '<span style="color: #2196F3;">' . $userFullName . '</span> menyetujui permohonan ' . $pengajuan->nomor . '.';
              $notification_status_back = '<span style="color: #2196F3;">' . $userFullName . '</span> menyetujui permohonan ' . $pengajuan->nomor . '.';
              $notification_status_next = null;
              break;
            case '6':
              $action = 'Sent';
              $description = 'mengirim laporan pembelian';
              $notification_back_to = [];
              $notification_next_to = ['Kabid', 'Staff Bendahara', 'Staff Keuangan'];
              $notification_status = null;
              $notification_status_back = null;
              $notification_status_next = '<span style="color: #2196F3;">' . $userFullName . '</span> membuat laporan pembelian untuk permohonan ' . $pengajuan->nomor . '.';
              break;
            default:
              break;
          };

          $description .= (!empty($note)) ? ', note: <br />' . $note : '.';

          // Log
          $log_data = array(
            'pengajuan_id' => $id,
            'user_pic' => $this->session->userdata('user')['id'],
            'action' => $action,
            'description' => $description,
            'revisi' => $pengajuan->revisi
          );
          $log = $this->PengajuanhistoryModel->insert($log_data);
          // END ## Log

          if ($log['status'] === true) {
            // // Notification
            // To stakeholder-1
            if (!is_null($notification_status)) {
              $notification_data = array(
                'user_to' => $pengajuan->created_by,
                'ref' => 'pengajuanbuku',
                'ref_id' => $id,
                'description' => $notification_status,
                'link' => 'pengajuan/buku/detail/' . $id
              );
              $this->set_notification($notification_data);
            };

            // To Back Role Receiver
            if (count($notification_back_to) > 0) {
              foreach ($notification_back_to as $key => $item) {
                $notification_data = array(
                  'ref' => 'pengajuanbuku',
                  'ref_id' => $id,
                  'description' => $notification_status_back,
                  'link' => 'persetujuan/buku/detail/' . $id
                );
                $this->set_notification($notification_data, $item);
              };
            };

            // To Next Role Receiver
            if (count($notification_next_to) > 0) {
              foreach ($notification_next_to as $key => $item) {
                $notification_data = array(
                  'ref' => 'pengajuanbuku',
                  'ref_id' => $id,
                  'description' => $notification_status_next,
                  'link' => 'persetujuan/buku/detail/' . $id
                );
                $this->set_notification($notification_data, $item);
              };
            };
            // END ## Notification

            $response = $setStatus;
          } else {
            $response = $log;
          };
        } else {
          $response = $setStatus;
        };
      };
    } else {
      $response = array('status' => false, 'data' => '(System) Parameters is bad.');
    };

    echo json_encode($response);
  }

  public function ajax_get_item($id)
  {
    $this->handle_ajax_request();
    $dtAjax_config = array(
      'table_name' => 'pengajuan_item',
      'static_conditional' => array(
        'pengajuan_id' => $id
      ),
    );
    $response = $this->AppModel->getData_dtAjax($dtAjax_config);
    echo json_encode($response);
  }

  private function _getTtd($id)
  {
    $administrator =  $this->PengajuanModel->checkTtd($id, 'Sent', 'Administrator');
    $kabid =  $this->PengajuanModel->checkTtd($id, 'Approve', 'Kabid');
    $staff_bendahara =  $this->PengajuanModel->checkTtd($id, 'Approve', 'Staff Bendahara');

    $result = array(
      'administrator' => $administrator,
      'kabid' => $kabid,
      'staff_bendahara' => $staff_bendahara,
    );

    return (object) $result;
  }
}
