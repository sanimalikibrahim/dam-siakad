<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . 'controllers/AppBackend.php');

class Penilaianmapel extends AppBackend
{
  function __construct()
  {
    parent::__construct();
    $this->load->model([
      'AppModel',
      'PenilaianmapelModel',
    ]);
    $this->load->library('form_validation');
  }

  public function index()
  {
    $user_id = $this->session->userdata('user')['id'];
    $user_role = $this->session->userdata('user')['role'];

    if ($user_role === 'Guru') {
      $list_kelas = $this->init_list_kelas_by_guru($user_id);
      $list_mapel = $this->init_list_mapel_by_guru($user_id);
    } else {
      $list_kelas = $this->init_list_kelas();
      $list_mapel = $this->init_list_mapel_by_guru(-1);
    };

    $data = array(
      'app' => $this->app(),
      'main_js' => $this->load_main_js('penilaianmapel', false),
      'card_title' => 'Penilaian › Mata Pelajaran',
      'list_kelas' => $list_kelas,
      'list_mapel' => $list_mapel,
    );
    $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
    $this->template->load_view('index', $data, TRUE);
    $this->template->render();
  }

  public function ajax_get_activity()
  {
    $this->handle_ajax_request();

    $filter_date = $this->input->post('filter_date');
    $filter_kelas = $this->input->post('filter_kelas');
    $filter_mapel = $this->input->post('filter_mapel');
    $kelas = null;
    $sub_kelas = null;

    if (!empty($filter_date) && !empty($filter_kelas) && !empty($filter_mapel)) {
      $filter_kelas_ex = explode('#', $filter_kelas);
      $kelas = (isset($filter_kelas_ex[0])) ? $filter_kelas_ex[0] : null;
      $sub_kelas = (isset($filter_kelas_ex[1])) ? $filter_kelas_ex[1] : null;

      $model = $this->PenilaianmapelModel->getAll_withSantri($kelas, $sub_kelas, $filter_date, $filter_mapel);
      $jenisPenilaian = $this->getJenisPenilaian($model);

      if (count($model) > 0) {
        $export_params = '?ref=cxsmi&date=' . $filter_date . '&kelas=' . $filter_kelas . '&mapel=' . $filter_mapel;
        $export_params = urlencode($export_params);

        $data = array(
          'app' => $this,
          'is_today' => true,
          'data_santri' => $model,
          'jenis_penilaian' => $jenisPenilaian,
          'filter_date' => $filter_date,
          'filter_kelas' => $filter_kelas,
          'filter_mapel' => $filter_mapel,
          'export_params' => $export_params
        );
        $this->load->view('grid', $data);
      } else {
        echo json_encode(['status' => false, 'data' => 'Data santri tidak ditemukan.']);
      };
    } else {
      echo json_encode(['status' => false, 'data' => 'Pastikan Tanggal, Kelas & Mata Pelajaran sudah dipilih.']);
    };
  }

  public function ajax_save_activity()
  {
    $this->handle_ajax_request();

    $filter_date = $this->input->post('filter_date');
    $filter_kelas = $this->input->post('filter_kelas');
    $filter_mapel = $this->input->post('filter_mapel');
    $penilaianMapelItem = $this->input->post('penilaianmapel');
    $data = array();

    if (!empty($filter_date) && !empty($filter_kelas) && !empty($filter_mapel)) {
      $date = date('Y-m-d', strtotime($filter_date));

      if (!is_null($penilaianMapelItem) && count($penilaianMapelItem) > 0) {
        // Delete existing
        $this->PenilaianmapelModel->delete([
          'tanggal' => $date,
          'kelas' => $filter_kelas,
          'mata_pelajaran_id' => $filter_mapel
        ]);

        foreach ($penilaianMapelItem as $santri_id => $item) {
          // Store data POST
          if (isset($item['nilai'])) {
            $nilai = (!empty(trim($item['nilai']))) ? trim($item['nilai']) : null;
            $data[] = array(
              'kelas' => $filter_kelas,
              'santri_id' => $santri_id,
              'mata_pelajaran_id' => $filter_mapel,
              'pengajar_id' => $this->session->userdata('user')['id'],
              'nilai' => $nilai,
              'catatan' => $item['catatan'],
              'tanggal' => $date,
              'jenis' => $penilaianMapelItem['jenis'],
              'created_by' => $this->session->userdata('user')['id']
            );
          };
        };

        if (count($data) > 0) {
          // Insert new data
          echo json_encode($this->PenilaianmapelModel->insertBatch($data));
        } else {
          echo json_encode(array('status' => false, 'data' => 'No data to submit.'));
        };
      } else {
        echo json_encode(array('status' => false, 'data' => 'No data to submit.'));
      };
    } else {
      echo json_encode(array('status' => false, 'data' => 'Pastikan Tanggal, Kelas & Mata Pelajaran sudah dipilih.'));
    };
  }

  public function export()
  {
    $param = $this->input->get('param');
    $filterDate = null;
    $filterKelas = null;
    $filterMapel = null;

    // Extract params
    foreach (explode('&', $param) as $chunk) {
      $param = explode('=', $chunk);

      if ($param) {
        $filterDate = (urldecode($param[0]) === 'date') ? urldecode($param[1]) : $filterDate;
        $filterKelas = (urldecode($param[0]) === 'kelas') ? urldecode($param[1]) : $filterKelas;
        $filterMapel = (urldecode($param[0]) === 'mapel') ? urldecode($param[1]) : $filterMapel;
      };
    };
    // END ## Extract params

    if (!is_null($filterDate) && !is_null($filterKelas) && !is_null($filterMapel)) {
      // Extract kelas
      $filterKelasEx = explode('#', $filterKelas);
      $exKelas = (isset($filterKelasEx[0])) ? $filterKelasEx[0] : null;
      $exSubKelas = (isset($filterKelasEx[1])) ? $filterKelasEx[1] : null;
      // END ## Extract kelas

      $tanggalRgx = date('Ymd', strtotime($filterDate));
      $tanggalHariName = $this->get_day(date('D', strtotime($filterDate)));
      $tanggalHari = date('d', strtotime($filterDate));
      $tanggalBulanName = $this->get_month(date('m'), strtotime($filterDate));
      $tanggalTahun = date('Y', strtotime($filterDate));
      $tanggalFormated = $tanggalHariName . ', ' . $tanggalHari . ' ' . $tanggalBulanName . ' ' . $tanggalTahun;
      $kelasClean = str_replace('#', ' ', $filterKelas);
      $startAttributeRow = 6;
      $startDataRow = 7;
      $fileName = 'penilaian_mapel-' . $tanggalRgx . '.xlsx';
      $payload = array();

      $fileTemplate = FCPATH . 'directory/templates/template-penilaian_mapel.xlsx';
      // $model = $this->AbsenHarianModel->getAll(['tanggal' => $filterDate, 'kelas' => $filterKelas, 'mata_pelajaran_id' => $filterMapel]);
      $model = $this->PenilaianmapelModel->getAll_withSantri($exKelas, $exSubKelas, $filterDate, $filterMapel);
      $mapelData = $this->MataPelajaranModel->getDetail(['id' => $filterMapel]);
      $jenisPenilaian = $this->getJenisPenilaian($model, true);

      if (count($model) > 0) {
        // Modified payload
        foreach ($model as $index => $item) {
          $payload[] = (object) array(
            'id' => $item->id,
            'nomor_induk_lokal' => $item->nomor_induk_lokal,
            'nisn' => $item->nisn,
            'nama_lengkap' => $item->nama_lengkap,
            'nilai' => $item->nilai,
            'catatan' => $item->catatan
          );
        };

        $inject  = '$a3 = $sheet->getCell("A3")->getFormattedValue();';
        $inject .= '$a4 = $sheet->getCell("A4")->getFormattedValue();';

        $inject .= '$a3 = str_replace(\'${kelas}\', "' . $kelasClean . '", $a3);'; // Kelas
        $inject .= '$a3 = str_replace(\'${mapel}\', "' . $mapelData->nama . '", $a3);'; // Mapel
        $inject .= '$a3 = str_replace(\'${jenis}\', "' . $jenisPenilaian . '", $a3);'; // Tanggal
        $inject .= '$a3 = str_replace(\'${tanggal}\', "' . $tanggalFormated . '", $a3);'; // Jenis
        $inject .= '$a4 = str_replace(\'${kelas}\', "' . $kelasClean . '", $a4);'; // Kelas
        $inject .= '$a4 = str_replace(\'${mapel}\', "' . $mapelData->nama . '", $a4);'; // Mapel
        $inject .= '$a4 = str_replace(\'${tanggal}\', "' . $tanggalFormated . '", $a4);'; // Tanggal
        $inject .= '$a4 = str_replace(\'${jenis}\', "' . $jenisPenilaian . '", $a4);'; // Jenis
        $inject .= '$sheet->setCellValue("A3", strtoupper($a3));';
        $inject .= '$sheet->setCellValue("A4", strtoupper($a4));';

        $this->generateExcelByTemplate($fileTemplate, $startAttributeRow, $startDataRow, $payload, $fileName, $inject);
      } else {
        echo 'Tidak ditemukan data.';
      };
    } else {
      echo 'Terjadi kesalahan ketika membuat file.';
    };
  }

  private function getJenisPenilaian($model, $returnAsName = false)
  {
    /*
      1 = Harian
      2 = UTS
      3 = UAS
    */

    $jenis = 1; // Default

    // Check jenis penilaian from array
    $isJenis1 = (array_search('1', array_column($model, 'jenis')) !== false) ? true : false;
    $isJenis2 = (array_search('2', array_column($model, 'jenis')) !== false) ? true : false;
    $isJenis3 = (array_search('3', array_column($model, 'jenis')) !== false) ? true : false;

    if ($isJenis1 === true) {
      $jenis = 1;
      $jenisName = 'Harian';
    } else if ($isJenis2 === true) {
      $jenis = 2;
      $jenisName = 'UTS';
    } else if ($isJenis3 === true) {
      $jenis = 3;
      $jenisName = 'UAS';
    };

    if ($returnAsName === true) {
      return $jenisName;
    } else {
      return $jenis;
    };
  }
}
