<style type="text/css">
  .table-scroll {
    position: relative;
    overflow: hidden;
    border: 1px solid #ececec;
    border-radius: 2px;
    z-index: 0;
  }

  .table-wrap {
    width: 100%;
    overflow: auto;
    padding: .5rem;
  }

  .table-scroll table {
    width: 100%;
    margin: auto;
    border-collapse: collapse;
  }

  .table-scroll tr th {
    background-color: #fafafa;
  }

  .table-scroll th,
  .table-scroll td {
    padding: 4px 8px;
    border: 1px solid #ececec;
    white-space: nowrap;
    vertical-align: top;
    text-align: center;
  }

  .table-scroll tbody tr:hover {
    background: #f2f2f2;
  }

  .table-scroll thead,
  .table-scroll tfoot {
    background: #f9f9f9;
  }

  /* Input style */
  input[type=radio] {
    -moz-appearance: none;
    -webkit-appearance: none;
    -o-appearance: none;
    outline: none;
    content: none;
  }

  input[type=radio]:before {
    content: "     ";
    white-space: pre;
    font-size: 11px;
    color: transparent !important;
    background: #fff;
    width: 25px;
    height: 25px;
    border: 1px dotted #ccc;
    border-radius: 4px;
  }

  input[type=radio]:checked:before {
    color: #2dad76 !important;
    content: " ✓ ";
    width: 25px;
    height: 25px;
  }

  .input-text {
    border: 1px dotted #ccc;
    border-radius: 4px;
    font-size: 11px;
    color: #333;
  }

  .dataTables_wrapper,
  .dataTables_wrapper .table {
    margin: 0;
  }

  .page-action {
    background-color: #fafafa;
    padding: 15px;
    margin-top: 1rem;
    box-shadow: 0 1px 2px rgba(0, 0, 0, .075);
  }
</style>

<?php if (count($data_santri) > 0) : ?>
  <form id="form-penilaianmapel" method="post" enctype="multipart/form-data" autocomplete="off">
    <!-- Hidden temp -->
    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />

    <div id="table-scroll" class="table-scroll">
      <div class="action-buttons p-2 border-bottom border-danger">
        <div class="pb-2 mb-2 border-bottom border-light">
          <a href="<?= base_url('penilaianmapel/export/?param=' . $export_params) ?>" class="btn btn-secondary btn--icon-text absensi-export-excel">
            <i class="zmdi zmdi-download"></i> Export to Excel
          </a>
        </div>
        <div class="row">
          <label class="col-xs-12 col-md-2" style="max-width: 130px;"><b>Jenis Penilaian</b></label>
          <div class="col-xs-12 col-md-10">
            <div class="form-check form-check-inline">
              <input class="form-check-input" type="radio" name="penilaianmapel[jenis]" id="jenis_1" value="1" <?= ($jenis_penilaian === 1) ? 'checked' : '' ?> />
              <label class="form-check-label" for="jenis_1">Harian</label>
            </div>
            <div class="form-check form-check-inline">
              <input class="form-check-input" type="radio" name="penilaianmapel[jenis]" id="jenis_2" value="2" <?= ($jenis_penilaian === 2) ? 'checked' : '' ?> />
              <label class="form-check-label" for="jenis_2">UTS</label>
            </div>
            <div class="form-check form-check-inline">
              <input class="form-check-input" type="radio" name="penilaianmapel[jenis]" id="jenis_3" value="3" <?= ($jenis_penilaian === 3) ? 'checked' : '' ?> />
              <label class="form-check-label" for="jenis_3">UAS</label>
            </div>
          </div>
        </div>
      </div>
      <div class="table-wrap">
        <table id="table-penilaianmapel" class="table table-hover hover">
          <thead>
            <tr>
              <th style="width: 70px;">No</th>
              <th style="width: 120px">NIL</th>
              <th>Nama</th>
              <th style="width: 120px;">Nilai</th>
              <th>Catatan</th>
            </tr>
          </thead>
          <tbody>
            <?php $no = 1 ?>
            <?php foreach ($data_santri as $index => $item) : ?>
              <tr>
                <td><?= $no++ ?></td>
                <td><?= $item->nomor_induk_lokal ?></td>
                <td style="text-align: left;"><?= $item->nama_lengkap ?></td>
                <td>
                  <input type="number" name="penilaianmapel[<?= $item->id ?>][nilai]" value="<?= isset($item->nilai) ? $item->nilai : null ?>" min="0" max="100" class="mask-number" style="width: 120px;" />
                </td>
                <td>
                  <input type="text" name="penilaianmapel[<?= $item->id ?>][catatan]" value="<?= isset($item->catatan) ? $item->catatan : null ?>" style="width: 100%; min-width: 120px;" />
                </td>
              </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
      </div>
    </div>

    <div class="page-action">
      <div class="row">
        <div class="col text-right">
          <button type="submit" class="btn btn--raised btn-success btn--icon-text page-action-save spinner-action-button">
            <i class="zmdi zmdi-save"></i>
            Save Changes
          </button>
        </div>
      </div>
    </div>
  </form>
<?php else : ?>
  <div class="filter-info filter-no-data">
    <i class="zmdi zmdi-filter-frames"></i>
    <p>Tidak ditemukan data</p>
    <p>Silahkan lakukan filter terlebih dahulu</p>
  </div>
<?php endif; ?>