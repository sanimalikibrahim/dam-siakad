<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . 'controllers/AppBackend.php');
require_once(APPPATH . 'controllers/ApiClient.php');

class User extends AppBackend
{
  function __construct()
  {
    parent::__construct();
    $this->load->model([
      'AppModel',
      'UserModel',
      'RoleModel'
    ]);
    $this->load->library('form_validation');
  }

  private function _updateUserPasswordLearndash($id, $password)
  {
    $apiClient = new ApiClient();
    $response = $apiClient->updateUserPassword($id, $password);

    return $response;
  }

  public function index()
  {
    $data = array(
      'app' => $this->app(),
      'main_js' => $this->load_main_js('user'),
      'card_title' => 'Pengguna',
      'list_role' => $this->init_list($this->RoleModel->getAll([], 'name', 'asc'), 'name', 'name'),
    );
    $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
    $this->template->load_view('index', $data, TRUE);
    $this->template->render();
  }

  public function ajax_getAll()
  {
    $this->handle_ajax_request();
    $dtAjax_config = array(
      'table_name' => 'user',
      'order_column' => 5,
      'order_column_dir' => 'desc'
    );
    $response = $this->AppModel->getData_dtAjax($dtAjax_config);
    echo json_encode($response);
  }

  public function ajax_save($id = null)
  {
    $this->handle_ajax_request();
    $this->form_validation->set_rules($this->UserModel->rules($id));

    $role = $this->input->post('role');
    $password = $this->input->post('password');

    if ($this->form_validation->run() === true) {
      if (is_null($id)) {
        echo json_encode($this->UserModel->insert());
      } else {
        $isErrorLearndash = false;

        // Update user in learndash-wp
        if (in_array($role, ['Santri', 'Calon Santri']) && (!is_null($password) && !empty($password))) {
          $user = $this->UserModel->getDetail(['id' => $id]);
          $learndashUserId = $user->learndash_user_id;

          if (!is_null($learndashUserId)) {
            $updateLearndash = $this->_updateUserPasswordLearndash($learndashUserId, $password);

            if ($updateLearndash['status'] !== true) {
              $isErrorLearndash = $updateLearndash;
            };
          };
        };

        if ($isErrorLearndash === false) {
          echo json_encode($this->UserModel->update($id));
        } else {
          echo json_encode($isErrorLearndash);
        };
      };
    } else {
      $errors = validation_errors('<div>- ', '</div>');
      echo json_encode(array('status' => false, 'data' => $errors));
    };
  }

  public function ajax_delete($id)
  {
    $this->handle_ajax_request();
    echo json_encode($this->UserModel->delete($id));
  }
}
