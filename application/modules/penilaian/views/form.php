<div class="modal fade" id="modal-form-penilaian" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title pull-left">Penilaian</h5>
      </div>
      <div class="spinner">
        <div class="lds-hourglass"></div>
      </div>
      <div class="modal-body">
        <form id="form-penilaian" autocomplete="off">
          <!-- CSRF -->
          <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />

          <div class="form-group">
            <label required>NISN</label>
            <input type="text" name="nisn" class="form-control mask-number penilaian-nisn" maxlength="30" placeholder="NISN" required readonly />
            <i class="form-group__bar"></i>
          </div>

          <div class="form-group">
            <label required>Nama Lengkap</label>
            <input type="text" name="nama_lengkap" class="form-control penilaian-nama_lengkap" maxlength="200" placeholder="Nama Lengkap" required readonly />
            <i class="form-group__bar"></i>
          </div>

          <?php if (count($pivot_data) > 0) : ?>
            <?php foreach ($pivot_data['column']->label as $index => $item) : ?>
              <?php if (!in_array($item, ['NISN', 'Nama Lengkap', 'Jenis Kelamin'])) : ?>
                <div class="form-group">
                  <label><?= $item ?></label>
                  <div class="input-group">
                    <input type="number" name="nilai[<?= $pivot_data['column']->field[$index] ?>]" class="form-control pl-0 penilaian-<?= $pivot_data['column']->field[$index] ?>" maxlength="6" min="0" max="100" placeholder="0" />
                    <div class="input-group-append">
                      <span class="input-group-text">Max: 100</span>
                    </div>
                    <i class="form-group__bar"></i>
                  </div>
                </div>
              <?php endif; ?>
            <?php endforeach; ?>
          <?php endif; ?>

          <small class="form-text text-muted">
            Fields with red stars (<label required></label>) are required.
          </small>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success btn--icon-text penilaian-action-save">
          <i class="zmdi zmdi-save"></i> Save
        </button>
        <button type="button" class="btn btn-light btn--icon-text penilaian-action-cancel" data-dismiss="modal">
          Cancel
        </button>
      </div>
    </div>
  </div>
</div>