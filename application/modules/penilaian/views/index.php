<style type="text/css">
    .table-action .buttons {
        padding-top: 10px;
        display: flex;
        align-items: center;
    }

    .vertical-line {
        width: 1px;
        height: 30px;
        background: var(--cd-color-2);
    }

    .hidden {
        display: none;
    }

    input[readonly] {
        opacity: 1 !important;
        border-bottom: 1px dashed #eceff1 !important;
    }
</style>

<section id="penilaian">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">
                <?php echo (isset($card_title)) ? $card_title : '' ?>
                <span class="data-loader hidden">
                    <i class="zmdi zmdi-settings zmdi-hc-spin"></i>
                </span>
            </h4>
            <h6 class="card-subtitle mb-0"><?php echo (isset($card_subTitle)) ? $card_subTitle : '' ?></h6>

            <?php include_once('form.php') ?>

            <div class="table-responsive">
                <table id="table-penilaian" class="table table-bordered">
                    <thead class="thead-default">
                        <tr>
                            <th width="100">No</th>
                            <?php if (count($pivot_data['column']) > 0) : ?>
                                <?php foreach ($pivot_data['column']->label as $index => $item) : ?>
                                    <th><?= $item ?></th>
                                <?php endforeach; ?>
                            <?php endif; ?>
                            <th width="100">Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</section>