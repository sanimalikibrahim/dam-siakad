<script type="text/javascript">
  $(document).ready(function() {

    var _key = "";
    var _section = "penilaian";
    var _table = "table-penilaian";
    var _modal = "modal-form-penilaian";
    var _form = "form-penilaian";
    var _columns = [];
    var _datas = <?= json_encode($pivot_data['data']) ?>;

    // Collect column
    _columns.push({
      data: null,
      render: function(data, type, row, meta) {
        return meta.row + meta.settings._iDisplayStart + 1;
      }
    });

    <?php if (count($pivot_data) > 0) : ?>
      <?php foreach ($pivot_data['column']->field as $index => $item) : ?>
        _columns.push({
          data: "<?= $item ?>"
        });
      <?php endforeach; ?>
    <?php endif; ?>

    _columns.push({
      data: null,
      className: "center",
      defaultContent: '<div class="action">' +
        '<a href="javascript:;" class="btn btn-sm btn-light btn-table-action action-edit" data-toggle="modal" data-target="#' + _modal + '"><i class="zmdi zmdi-edit"></i> Edit</a>' +
        '</div>'
    });
    // END ##Collect column

    // Initialize DataTables: Index
    if ($("#" + _table)[0]) {
      var table_penilaian = $("#" + _table).DataTable({
        data: _datas,
        columns: _columns,
        autoWidth: !1,
        responsive: {
          details: {
            renderer: function(api, rowIdx, columns) {
              var hideColumn = [];
              var data = $.map(columns, function(col, i) {
                return ($.inArray(col.columnIndex, hideColumn)) ?
                  '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                  '<td class="dt-details-td">' + col.title + ':' + '</td> ' +
                  '<td class="dt-details-td">' + col.data + '</td>' +
                  '</tr>' :
                  '';
              }).join('');

              return data ? $('<table/>').append(data) : false;
            },
            type: "inline",
            target: 'tr',
          }
        },
        pageLength: 15,
        language: {
          searchPlaceholder: "Search...",
          sProcessing: '<div style="text-align: center;"><div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div></div>'
        },
        sDom: '<"dataTables_ct"><"dataTables__top"fb>rt<"dataTables__bottom"ip><"clear">',
        buttons: [{
          extend: "excelHtml5",
          title: "Export Result"
        }, {
          extend: "print",
          title: "Export Result"
        }]
      });

      $(".dataTables_filter input[type=search]").focus(function() {
        $(this).closest(".dataTables_filter").addClass("dataTables_filter--toggled")
      });

      $(".dataTables_filter input[type=search]").blur(function() {
        $(this).closest(".dataTables_filter").removeClass("dataTables_filter--toggled")
      });
    };

    // Handle data add
    $("#" + _section).on("click", "button." + _section + "-action-add", function(e) {
      e.preventDefault();
      resetForm();
    });

    // Handle data edit
    $("#" + _table).on("click", "a.action-edit", function(e) {
      e.preventDefault();
      resetForm();
      var temp = table_penilaian.row($(this).closest('tr')).data();

      // Set key for update params, important!
      _key = temp.id;

      $.map(temp, function(value, index) {
        $("#" + _form + " ." + _section + "-" + index).val(value).trigger("change").trigger("input");
      });
    });

    // Handle data submit
    $("#" + _modal + " ." + _section + "-action-save").on("click", function(e) {
      e.preventDefault();
      $.ajax({
        type: "post",
        url: "<?php echo base_url('penilaian/ajax_save/') ?>" + _key,
        data: $("#" + _form).serialize(),
        dataType: "json",
        success: function(response) {
          if (response.status === true) {
            resetForm();
            $("#" + _modal).modal("hide");
            initializeDatas();
            notify(response.data, "success");
          } else {
            notify(response.data, "danger");
          };
        }
      });
    });

    // Handle initialize datas
    initializeDatas = () => {
      $("#" + _section + " .data-loader").show();

      $.ajax({
        type: "get",
        url: "<?= base_url('penilaian/ajax_get_all') ?>",
        dataType: "json",
        success: function(response) {
          table_penilaian.clear();
          table_penilaian.rows.add(response);
          table_penilaian.draw();
        }
      }).done(function() {
        $("#" + _section + " .data-loader").hide();
      }).fail(function() {
        $("#" + _section + " .data-loader").hide();
      });
    };

    // Handle form reset
    resetForm = () => {
      _key = "";
      $("#" + _form).trigger("reset");
    };

  });
</script>