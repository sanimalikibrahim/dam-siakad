<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . 'controllers/AppBackend.php');

class Penilaian extends AppBackend
{
  function __construct()
  {
    parent::__construct();
    $this->load->model([
      'AppModel',
      'PenilaianModel',
      'KriteriaModel'
    ]);
    $this->load->library('form_validation');
  }

  public function index()
  {
    $pivotData = $this->PenilaianModel->getPivot();
    $data = array(
      'app' => $this->app(),
      'main_js' => $this->load_main_js('penilaian', false, [
        'pivot_data' => $pivotData
      ]),
      'card_title' => 'Penilaian',
      'pivot_data' => $pivotData
    );
    $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
    $this->template->load_view('index', $data, TRUE);
    $this->template->render();
  }

  public function ajax_get_all()
  {
    $this->handle_ajax_request();
    $data = $this->PenilaianModel->getPivot();

    echo json_encode($data['data']);
  }

  public function ajax_save($user_id = null)
  {
    $this->handle_ajax_request();
    $this->form_validation->set_rules($this->PenilaianModel->rules());

    $nilai = $this->input->post('nilai');
    $hasErrors = [];
    $transaction = ['status' => false, 'data' => 'Cannot initialize the data.'];

    if (is_array($nilai)) {
      foreach ($nilai as $index => $value) {
        $kriteria = $this->KriteriaModel->getDetail([
          'nama' => str_replace('_', ' ', $index)
        ]);

        if (!is_null($kriteria)) {
          $kriteria_id = $kriteria->id;

          $_POST['user_id'] = $user_id;
          $_POST['kriteria_id'] = $kriteria_id;
          $_POST['nilai'] = $value;
        };

        $isExist = $this->PenilaianModel->getDetail([
          'user_id' => $user_id,
          'kriteria_id' => $kriteria_id
        ]);

        if (is_null($isExist)) {
          $transaction = $this->PenilaianModel->insert();
        } else {
          $transaction = $this->PenilaianModel->update($isExist->id);
        };

        if ($transaction['status'] === false) {
          $hasErrors[] = $kriteria_id;
        };
      };
    };

    if (count($hasErrors) === 0) {
      $response = array('status' => true, 'data' => 'Data has been saved.');
    } else {
      $response = array('status' => false, 'data' => 'Some data failed to save, try again later.');
    };

    echo json_encode($response);
  }
}
