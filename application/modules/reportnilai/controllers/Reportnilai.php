<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . 'controllers/AppBackend.php');

class Reportnilai extends AppBackend
{
  function __construct()
  {
    parent::__construct();
    $this->load->model([
      'AppModel',
      'PenilaianModel',
      'KriteriaModel'
    ]);
    $this->load->library('form_validation');
  }

  public function test()
  {
    $data = $this->KriteriaModel->getBobot();

    echo '<pre>';
    print_r($data);
    die;
  }

  public function index()
  {
    $pivotData = $this->PenilaianModel->getPivot();
    $maxNilai = $this->PenilaianModel->getMaxNilai();
    $bobotData = $this->KriteriaModel->getBobot();
    $data = array(
      'app' => $this->app(),
      'main_js' => $this->load_main_js('reportnilai', false, [
        'pivot_data' => $pivotData,
        'max_nilai' => $maxNilai,
        'bobot_data' => $bobotData
      ]),
      'card_title' => 'Laporan',
      'pivot_data' => $pivotData
    );
    $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
    $this->template->load_view('index', $data, TRUE);
    $this->template->render();
  }

  public function ajax_get_all()
  {
    $this->handle_ajax_request();
    $data = $this->PenilaianModel->getPivot();

    echo json_encode($data['data']);
  }
}
