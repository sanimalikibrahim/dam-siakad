<div class="table-responsive">
  <table id="table-ranking" class="table table-bordered">
    <thead class="thead-default">
      <tr>
        <th width="100">No</th>
        <?php if (count($pivot_data) > 0) : ?>
          <?php foreach ($pivot_data['column']->label as $index => $item) : ?>
            <?php if (in_array($pivot_data['column']->field[$index], ['nisn', 'nama_lengkap', 'jenis_kelamin', 'id', 'email', 'kota', 'telepon', 'tahun', 'status'])) : ?>
              <th><?= $item ?></th>
            <?php endif; ?>
          <?php endforeach; ?>
        <?php endif; ?>
        <th width="150">Nilai</th>
        <th width="150">Ranking</th>
      </tr>
    </thead>
    <tfoot style="background: var(--gray);">
      <tr>
        <th colspan="3"></th>
        <th></th>
        <th colspan="2"></th>
      </tr>
    </tfoot>
  </table>
</div>