<script type="text/javascript">
  $(document).ready(function() {

    var _section = "penilaian";
    var _table = "table-penilaian";
    var _table_normalisasi = "table-normalisasi";
    var _table_ranking = "table-ranking";
    var _columns = [];
    var _columns_normalisasi = [];
    var _columns_ranking = [];
    var _removeKeys = ['id', 'nisn', 'email', 'nama_lengkap', 'jenis_kelamin', 'kota', 'telepon', 'tahun', 'status'];
    var _fields = <?= json_encode($pivot_data['column']->field) ?>;
    var _datas = <?= json_encode($pivot_data['data']) ?>;
    var _maxNilai = <?= json_encode($max_nilai) ?>;
    var _bobots = <?= json_encode($bobot_data) ?>;
    var _nilai = [];

    // Initialize ranking
    getRangkingNilai = (id) => {
      var tempData = _datas;
      var tempDataFiltered = tempData.filter(item => item.id == id);
      var tempDataFilteredClean = [];
      var tempNilai = [];
      var total = 0;

      if (tempDataFiltered !== null && tempDataFiltered.length > 0) {
        tempDataFiltered = tempDataFiltered[0];

        $.each(tempDataFiltered, function(key, item) {
          if ($.inArray(key, _removeKeys) != -1) {
            // console.log("isExist", key);
          } else {
            var maxNilai = _maxNilai[key];
            var bobot = parseFloat(_bobots[key]).toFixed(2);
            var normalisasi = roundNumber(item) / roundNumber(maxNilai);
            normalisasi = roundNumber(normalisasi);
            const nilai = roundNumber(normalisasi) * roundNumber(bobot);

            tempDataFilteredClean.push(nilai.toFixed(3));
          };
        });

        total = tempDataFilteredClean.reduce((pv, cv) => {
          return roundNumber(pv) + (roundNumber(cv) || 0);
        }, 0);
      };

      return roundNumber(total);
    };

    // Initialize column: Penilaian
    initializeColumn_penilaian = () => {
      _columns.push({
        data: null,
        render: function(data, type, row, meta) {
          return meta.row + meta.settings._iDisplayStart + 1;
        }
      });

      if (_fields !== null && _fields.length > 0) {
        _fields.map(function(item, index) {
          _columns.push({
            data: item
          });
        });
      };
    };
    initializeColumn_penilaian();

    // Initialize column: Normalisasi
    initializeColumn_normalisasi = () => {
      _columns_normalisasi.push({
        data: null,
        render: function(data, type, row, meta) {
          return meta.row + meta.settings._iDisplayStart + 1;
        }
      });

      if (_fields !== null && _fields.length > 0) {
        _fields.map(function(item, index) {
          if ($.inArray(item, _removeKeys) != -1) {
            _columns_normalisasi.push({
              data: item
            });
          } else {
            _columns_normalisasi.push({
              data: item,
              render: function(data, type, row, meta) {
                var maxNilai = _maxNilai[item];
                var nilai = Math.round((parseFloat(data) + Number.EPSILON) * 100) / 100;
                nilai = nilai / parseFloat(maxNilai);
                nilai = parseFloat(nilai).toFixed(3);

                return !isNaN(nilai) ? nilai : "";
              }
            });
          };
        });
      };
    };
    initializeColumn_normalisasi();

    // Initialize column: Ranking
    initializeColumn_ranking = () => {
      _columns_ranking.push({
        data: null,
        render: function(data, type, row, meta) {
          return meta.row + meta.settings._iDisplayStart + 1;
        }
      });

      if (_fields !== null && _fields.length > 0) {
        _fields.map(function(item, index) {
          if ($.inArray(item, _removeKeys) != -1) {
            _columns_ranking.push({
              data: item
            });
          };
        });
      };

      _columns_ranking.push({
        data: null,
        render: function(data, type, row, meta) {
          return getRangkingNilai(row.id);
        }
      }, {
        data: null,
        render: function(data, type, row, meta) {
          return meta.row + meta.settings._iDisplayStart + 1;
        }
      });
    };
    initializeColumn_ranking();

    // Initialize DataTables: Penilaian
    if ($("#" + _table)[0]) {
      var table_penilaian = $("#" + _table).DataTable({
        data: _datas,
        columns: _columns,
        autoWidth: !1,
        responsive: {
          details: {
            renderer: function(api, rowIdx, columns) {
              var hideColumn = [];
              var data = $.map(columns, function(col, i) {
                return ($.inArray(col.columnIndex, hideColumn)) ?
                  '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                  '<td class="dt-details-td">' + col.title + ':' + '</td> ' +
                  '<td class="dt-details-td">' + col.data + '</td>' +
                  '</tr>' :
                  '';
              }).join('');

              return data ? $('<table/>').append(data) : false;
            },
            type: "inline",
            target: 'tr',
          }
        },
        pageLength: 15,
        language: {
          searchPlaceholder: "Search...",
          sProcessing: '<div style="text-align: center;"><div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div></div>'
        },
        sDom: '<"dataTables_ct"><"dataTables__top"fb>rt<"dataTables__bottom"ip><"clear">',
        buttons: [{
          extend: "excelHtml5",
          title: "Export Result"
        }, {
          extend: "print",
          title: "Export Result"
        }]
      });

      $(".dataTables_filter input[type=search]").focus(function() {
        $(this).closest(".dataTables_filter").addClass("dataTables_filter--toggled")
      });

      $(".dataTables_filter input[type=search]").blur(function() {
        $(this).closest(".dataTables_filter").removeClass("dataTables_filter--toggled")
      });
    };

    // Initialize DataTables: Normalisasi
    if ($("#" + _table_normalisasi)[0]) {
      var table_normalisasi = $("#" + _table_normalisasi).DataTable({
        data: _datas,
        columns: _columns_normalisasi,
        autoWidth: !1,
        responsive: {
          details: {
            renderer: function(api, rowIdx, columns) {
              var hideColumn = [];
              var data = $.map(columns, function(col, i) {
                return ($.inArray(col.columnIndex, hideColumn)) ?
                  '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                  '<td class="dt-details-td">' + col.title + ':' + '</td> ' +
                  '<td class="dt-details-td">' + col.data + '</td>' +
                  '</tr>' :
                  '';
              }).join('');

              return data ? $('<table/>').append(data) : false;
            },
            type: "inline",
            target: 'tr',
          }
        },
        pageLength: 15,
        language: {
          searchPlaceholder: "Search...",
          sProcessing: '<div style="text-align: center;"><div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div></div>'
        },
        sDom: '<"dataTables_ct"><"dataTables__top"fb>rt<"dataTables__bottom"ip><"clear">',
        buttons: [{
          extend: "excelHtml5",
          title: "Export Result"
        }, {
          extend: "print",
          title: "Export Result"
        }]
      });

      $(".dataTables_filter input[type=search]").focus(function() {
        $(this).closest(".dataTables_filter").addClass("dataTables_filter--toggled")
      });

      $(".dataTables_filter input[type=search]").blur(function() {
        $(this).closest(".dataTables_filter").removeClass("dataTables_filter--toggled")
      });
    };

    // Initialize DataTables: Ranking
    if ($("#" + _table_ranking)[0]) {
      var table_ranking = $("#" + _table_ranking).DataTable({
        data: _datas,
        columns: _columns_ranking,
        autoWidth: !1,
        responsive: {
          details: {
            renderer: function(api, rowIdx, columns) {
              var hideColumn = [];
              var data = $.map(columns, function(col, i) {
                return ($.inArray(col.columnIndex, hideColumn)) ?
                  '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                  '<td class="dt-details-td">' + col.title + ':' + '</td> ' +
                  '<td class="dt-details-td">' + col.data + '</td>' +
                  '</tr>' :
                  '';
              }).join('');

              return data ? $('<table/>').append(data) : false;
            },
            type: "inline",
            target: 'tr',
          }
        },
        columnDefs: [{
          targets: [0, 1, 2, 3, 4, 5],
          orderable: false
        }],
        order: [
          [4, "desc"]
        ],
        pageLength: 15,
        language: {
          searchPlaceholder: "Search...",
          sProcessing: '<div style="text-align: center;"><div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div></div>'
        },
        sDom: '<"dataTables_ct"><"dataTables__top">rt<"dataTables__bottom"ip><"clear">',
        buttons: [{
          extend: "excelHtml5",
          title: "Export Result"
        }, {
          extend: "print",
          title: "Export Result"
        }],
        initComplete: function(a, b) {
          // Filter Jenis Kelamin
          this.api().columns([3]).every(function() {
            var column = this;
            var select = $('<select class="form-control dt-filter select2"><option value="">Filter ↓</option></select>')
              .appendTo($(column.footer()).empty())
              .on('change', function() {
                var val = $.fn.dataTable.util.escapeRegex(
                  $(this).val()
                );

                column
                  .search(val ? '^' + val + '$' : '', true, false)
                  .draw();
              });

            column.data().unique().sort().each(function(d, j) {
              if (d !== "") {
                select.append('<option value="' + d + '">' + d + '</option>')
              };
            });
          });
        },
      });

      $(".dataTables_filter input[type=search]").focus(function() {
        $(this).closest(".dataTables_filter").addClass("dataTables_filter--toggled")
      });

      $(".dataTables_filter input[type=search]").blur(function() {
        $(this).closest(".dataTables_filter").removeClass("dataTables_filter--toggled")
      });

      // Write ranking value
      table_ranking.on('order.dt search.dt', function() {
        table_ranking.column(0, {
          search: 'applied',
          order: 'applied'
        }).nodes().each(function(cell, i) {
          cell.innerHTML = i + 1;
        });
        table_ranking.column(5, {
          search: 'applied',
          order: 'applied'
        }).nodes().each(function(cell, i) {
          cell.innerHTML = i + 1;
        });
      }).draw();
    };

    // Handle round number
    function roundNumber(num, dec = 3) {
      var result = Math.round(num * Math.pow(10, dec)) / Math.pow(10, dec);
      return result;
    };

  });
</script>