<style type="text/css">
    .hidden {
        display: none;
    }

    .dataTables_wrapper {
        margin-top: 0;
    }
</style>

<section id="penilaian">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">
                <?php echo (isset($card_title)) ? $card_title : '' ?>
                <span class="data-loader hidden">
                    <i class="zmdi zmdi-settings zmdi-hc-spin"></i>
                </span>
            </h4>
            <h6 class="card-subtitle"><?php echo (isset($card_subTitle)) ? $card_subTitle : '' ?></h6>

            <ul class="nav nav-tabs nav-responsive" role="tablist">
                <li class="nav-item">
                    <a class="nav-link nav-penilaian active" data-toggle="tab" href="#nav-penilaian" role="tab">Penilaian</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link nav-normalisasi" data-toggle="tab" href="#nav-normalisasi" role="tab">Normalisasi</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link nav-ranking" data-toggle="tab" href="#nav-ranking" role="tab">Ranking</a>
                </li>
            </ul>
            </nav>
            <div class="tab-content" id="report-content">
                <div class="tab-pane fade show active" id="nav-penilaian" role="tabpanel" aria-labelledby="penilaian-tab">
                    <?php include_once('penilaian.php') ?>
                </div>
                <div class="tab-pane fade" id="nav-normalisasi" role="tabpanel" aria-labelledby="normalisasi-tab">
                    <?php include_once('normalisasi.php') ?>
                </div>
                <div class="tab-pane fade" id="nav-ranking" role="tabpanel" aria-labelledby="ranking-tab">
                    <?php include_once('ranking.php') ?>
                </div>
            </div>
        </div>
    </div>
</section>