<div class="table-responsive">
  <table id="table-normalisasi" class="table table-bordered">
    <thead class="thead-default">
      <tr>
        <th width="100">No</th>
        <?php if (count($pivot_data) > 0) : ?>
          <?php foreach ($pivot_data['column']->label as $index => $item) : ?>
            <th><?= $item ?></th>
          <?php endforeach; ?>
        <?php endif; ?>
      </tr>
    </thead>
  </table>
</div>