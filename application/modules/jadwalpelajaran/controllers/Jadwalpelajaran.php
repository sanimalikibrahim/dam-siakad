<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . 'controllers/AppBackend.php');

class Jadwalpelajaran extends AppBackend
{
  function __construct()
  {
    parent::__construct();
    $this->load->model([
      'AppModel',
      'MataPelajaranModel',
      'JadwalPelajaranModel',
      'JadwalPelajaranItemModel',
      'UserModel'
    ]);
    $this->load->library('form_validation');
  }

  public function index()
  {
    $data = array(
      'app' => $this->app(),
      'main_js' => $this->load_main_js('jadwalpelajaran'),
      'card_title' => 'Jadwal Pelajaran',
      'list_kelas' => $this->init_list_kelas()
    );
    $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
    $this->template->load_view('index', $data, TRUE);
    $this->template->render();
  }

  public function item($id = null)
  {
    $jadwalPelajaran = $this->JadwalPelajaranModel->getDetail(['id' => $id]);

    if (!is_null($jadwalPelajaran)) {
      $data = array(
        'app' => $this->app(),
        'main_js' => $this->load_main_js('jadwalpelajaran', false, array(
          'data_id' => $id,
        )),
        'card_title' => 'Jadwal Pelajaran › Semester ' . $jadwalPelajaran->semester,
        'card_subTitle' => $this->jsonToString($jadwalPelajaran->kelas, ', ', '#', ' '),
        'data_id' => $id,
        'list_mata_pelajaran' => $this->init_list_mapel(),
        'list_pengajar' => $this->init_list($this->UserModel->getAllByRole(['Guru', 'Ustadz'], 'nama_lengkap'), 'id', 'nama_lengkap')
      );
      $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
      $this->template->load_view('index_item', $data, TRUE);
      $this->template->render();
    } else {
      show_404();
    };
  }

  public function detail($id = null)
  {
    $jadwalPelajaran = $this->JadwalPelajaranModel->getDetail(['id' => $id]);

    if (!is_null($jadwalPelajaran)) {
      $data = array(
        'controller' => $this,
        'app' => $this->app(),
        'main_js' => $this->load_main_js('jadwalpelajaran'),
        'card_title' => 'Jadwal Pelajaran › Detail',
        'data_id' => $id,
        'jadwal_pelajaran' => $jadwalPelajaran,
        'jadwal_pelajaran_item' => $this->JadwalPelajaranItemModel->getAll(['jadwal_pelajaran_id' => $id])
      );
      $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
      $this->template->load_view('detail', $data, TRUE);
      $this->template->render();
    } else {
      show_404();
    };
  }

  public function download_pdf($id = null)
  {
    libxml_use_internal_errors(true);

    try {
      $jadwalPelajaran = $this->JadwalPelajaranModel->getDetail(['id' => $id]);
      $content =  $this->_getPartialDetail($id, true);
      $file_name = 'Jadwal_Pelajaran_Semester-' . $jadwalPelajaran->semester . '.pdf';

      $mpdf = new \Mpdf\Mpdf(['orientation' => 'L']);

      header("Content-type:application/pdf");
      header("Content-Disposition:attachment;filename=$file_name");

      $mpdf->WriteHTML($content);
      $mpdf->Output($file_name, 'D');
    } catch (\Throwable $th) {
      echo 'An error occurred while creating the file.';
    };
  }

  private function _getPartialDetail($id = null, $renderAsData = false)
  {
    $data = array(
      'controller' => $this,
      'data_id' => $id,
      'jadwal_pelajaran' => $this->JadwalPelajaranModel->getDetail(['id' => $id]),
      'jadwal_pelajaran_item' => $this->JadwalPelajaranItemModel->getAll(['jadwal_pelajaran_id' => $id])
    );
    return $this->load->view('jadwal_pelajaran_item', $data, $renderAsData);
  }

  public function ajax_get_all()
  {
    $this->handle_ajax_request();
    $dtAjax_config = array(
      'table_name' => 'jadwal_pelajaran',
      'order_column' => 3,
      'order_column_dir' => 'desc'
    );
    $response = $this->AppModel->getData_dtAjax($dtAjax_config);
    echo json_encode($response);
  }

  public function ajax_get_all_item($id = null)
  {
    $this->handle_ajax_request();
    $dtAjax_config = array(
      'table_name' => 'view_jadwal_pelajaran_item',
      'static_conditional' => array(
        'jadwal_pelajaran_id' => $id
      ),
      'order_column' => 0,
      'order_column_dir' => 'desc'
    );
    $response = $this->AppModel->getData_dtAjax($dtAjax_config);
    echo json_encode($response);
  }

  public function ajax_save($id = null)
  {
    $this->handle_ajax_request();
    $this->form_validation->set_rules($this->JadwalPelajaranModel->rules());

    if ($this->form_validation->run() === true) {
      if (is_null($id)) {
        echo json_encode($this->JadwalPelajaranModel->insert());
      } else {
        echo json_encode($this->JadwalPelajaranModel->update($id));
      };
    } else {
      $errors = validation_errors('<div>- ', '</div>');
      echo json_encode(array('status' => false, 'data' => $errors));
    };
  }

  public function ajax_save_item($id = null)
  {
    $this->handle_ajax_request();
    $this->form_validation->set_rules($this->JadwalPelajaranItemModel->rules($id));

    if ($this->form_validation->run() === true) {
      $_POST['hari'] = $this->get_day_by_num($this->input->post('hari_nomor'));

      if (is_null($id)) {
        echo json_encode($this->JadwalPelajaranItemModel->insert());
      } else {
        echo json_encode($this->JadwalPelajaranItemModel->update($id));
      };
    } else {
      $errors = validation_errors('<div>- ', '</div>');
      echo json_encode(array('status' => false, 'data' => $errors));
    };
  }

  public function ajax_delete($id)
  {
    $this->handle_ajax_request();
    echo json_encode($this->JadwalPelajaranModel->delete($id));
  }

  public function ajax_delete_item($id)
  {
    $this->handle_ajax_request();
    echo json_encode($this->JadwalPelajaranItemModel->delete($id));
  }

  public function get_day_count($id = null, $hari_nomor = null)
  {
    $data = $this->JadwalPelajaranItemModel->getAll(['jadwal_pelajaran_id' => $id, 'hari_nomor' => $hari_nomor]);
    return count($data);
  }
}
