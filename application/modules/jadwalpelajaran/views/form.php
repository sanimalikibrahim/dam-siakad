<div class="modal fade" id="modal-form-jadwalpelajaran" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title pull-left">Jadwal Pelajaran</h5>
      </div>
      <div class="spinner">
        <div class="lds-hourglass"></div>
      </div>
      <div class="modal-body">
        <form id="form-jadwalpelajaran" autocomplete="off">
          <!-- CSRF -->
          <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />

          <div class="form-group">
            <label required>Semester</label>
            <input type="text" name="semester" class="form-control jadwalpelajaran-semester" maxlength="5" placeholder="Semester" required />
            <i class="form-group__bar"></i>
          </div>

          <div class="form-group">
            <label required>Kelas</label>
            <div class="select">
              <select name="kelas[]" class="form-control select2 jadwalpelajaran-kelas" multiple="multiple" required>
                <?= $list_kelas ?>
              </select>
              <i class="form-group__bar"></i>
            </div>
          </div>

          <small class="form-text text-muted">
            Fields with red stars (<label required></label>) are required.
          </small>

        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success btn--icon-text jadwalpelajaran-action-save">
          <i class="zmdi zmdi-save"></i> Save
        </button>
        <button type="button" class="btn btn-light btn--icon-text jadwalpelajaran-action-cancel" data-dismiss="modal">
          Cancel
        </button>
      </div>
    </div>
  </div>
</div>