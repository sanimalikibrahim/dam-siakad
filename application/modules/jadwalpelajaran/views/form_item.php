<div class="modal fade" id="modal-form-jadwalpelajaran-item" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title pull-left">Jadwal Pelajaran</h5>
      </div>
      <div class="spinner">
        <div class="lds-hourglass"></div>
      </div>
      <div class="modal-body">
        <form id="form-jadwalpelajaran-item" autocomplete="off">
          <!-- CSRF -->
          <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />

          <input type="hidden" name="jadwal_pelajaran_id" value="<?= $data_id ?>" readonly />

          <div class="row">
            <div class="col-xs-12 col-md-6">
              <div class="form-group">
                <label required>Hari</label>
                <div class="select">
                  <select name="hari_nomor" class="form-control jadwalpelajaran-item-hari_nomor" data-placeholder="Select &#8595;" required>
                    <option disabled selected>Select &#8595;</option>
                    <option value="1">Senin</option>
                    <option value="2">Selasa</option>
                    <option value="3">Rabu</option>
                    <option value="4">Kamis</option>
                    <option value="5">Jumat</option>
                    <option value="6">Sabtu</option>
                    <option value="7">Minggu</option>
                  </select>
                  <i class="form-group__bar"></i>
                </div>
              </div>
            </div>
            <div class="col-xs-12 col-md-6">
              <div class="form-group">
                <label required>Sesi</label>
                <input type="number" name="sesi" class="form-control jadwalpelajaran-item-sesi" min="1" placeholder="Sesi" />
                <i class="form-group__bar"></i>
              </div>
            </div>
          </div>

          <div class="form-group">
            <label required>Jam</label>
            <div class="buttons">
              <div class="input-group mb-0">
                <input type="text" name="jam_mulai" class="form-control no-padding-l flatpickr-timeonly jadwalpelajaran-item-jam_mulai" placeholder="Mulai" required />
                <i class="form-group__bar"></i>
                <input type="text" name="jam_selesai" class="form-control no-padding-l flatpickr-timeonly jadwalpelajaran-item-jam_selesai" placeholder="Selesai" required />
                <i class="form-group__bar"></i>
              </div>
            </div>
          </div>

          <div class="form-group">
            <label required>Mata Pelajaran</label>
            <div class="select">
              <select name="mata_pelajaran_id" class="form-control select2 jadwalpelajaran-item-mata_pelajaran_id" required>
                <?= $list_mata_pelajaran ?>
              </select>
              <i class="form-group__bar"></i>
            </div>
          </div>

          <div class="form-group">
            <label>Pengajar</label>
            <div class="select">
              <select name="pengajar_id" class="form-control select2 jadwalpelajaran-item-pengajar_id">
                <?= $list_pengajar ?>
              </select>
              <i class="form-group__bar"></i>
            </div>
          </div>

          <div class="form-group">
            <label>Ruangan</label>
            <input type="text" name="ruangan" class="form-control jadwalpelajaran-item-ruangan" maxlength="50" placeholder="Ruangan" />
            <i class="form-group__bar"></i>
          </div>

          <small class="form-text text-muted">
            Fields with red stars (<label required></label>) are required.
          </small>

        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success btn--icon-text jadwalpelajaran-item-action-save">
          <i class="zmdi zmdi-save"></i> Save
        </button>
        <button type="button" class="btn btn-light btn--icon-text jadwalpelajaran-item-action-cancel" data-dismiss="modal">
          Cancel
        </button>
      </div>
    </div>
  </div>
</div>