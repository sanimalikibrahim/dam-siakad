<section id="jadwalpelajaran">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title"><?php echo (isset($card_title)) ? $card_title : '' ?></h4>
            <h6 class="card-subtitle"><?php echo (isset($card_subTitle)) ? $card_subTitle : '' ?></h6>

            <div class="table-action" style="margin-bottom: 1.5rem;">
                <div class="buttons">
                    <a href="<?php echo base_url('jadwalpelajaran') ?>" class="btn btn-secondary">
                        <i class="zmdi zmdi-arrow-left"></i>
                    </a>
                    <a href="<?php echo base_url('jadwalpelajaran/d-pdf/' . $data_id) ?>" class="btn btn-success">
                        <i class="zmdi zmdi-download"></i> Export to PDF
                    </a>
                </div>
            </div>

            <?php include_once('jadwal_pelajaran_item.php') ?>
        </div>
    </div>
</section>