<style type="text/css">
  @page {
    margin: 20px;
  }

  .report-wrapper {
    margin: 0 auto;
  }

  .report-logo {
    width: 45px;
  }

  .report-form-name {
    border: 1px solid #999;
    padding: 8px;
  }

  .report-wrapper .report {
    border: 1px solid #333;
    padding: 2rem;
  }

  .table-border {
    border-collapse: collapse;
    font-size: 1rem;
  }

  .table-border th {
    border: 1px solid #999;
    padding: 8px 8px;
  }

  .table-border td {
    border: 1px solid #999;
    padding: 4px 8px;
  }

  .ttd-input {
    width: 100%;
    height: 70px;
  }

  .item-value {
    border: none;
    padding: 0;
    margin: 0;
    width: 100%;
  }

  .item-value td {
    border: none;
    padding: 0;
    margin: 0;
  }

  .text-center {
    text-align: center;
  }

  .text-right {
    text-align: right;
  }

  .pull-left {
    float: left;
    text-align: left;
  }

  .pull-right {
    float: right;
    text-align: right
  }
</style>

<div class="report-wrapper">
  <div class="report">
    <table style="width: 100%;">
      <tr>
        <th style="width: 15%; text-align: left;">
          <img src="<?= base_url('themes/_public/img/logo/logo-white.png') ?>" class="report-logo" />
        </th>
        <th style="font-size: 13pt; font-weight: bold; text-align: center;">
          JADWAL PELAJARAN SEMESTER <?= $jadwal_pelajaran->semester ?>
          <p style="font-size: 11pt; font-weight: 500;">KELAS <?= $controller->jsonToString($jadwal_pelajaran->kelas, ', ', '#', ' ') ?></p>
        </th>
        <th style="width: 15%; text-align: right;">
          <!-- Empty col -->
          &nbsp;
        </th>
      </tr>
      <tr>
        <td colspan="3">
          <table class="table-border" style="width: 100%; margin-top: 2rem;">
            <thead>
              <tr>
                <th class="text-center" style="width: 50px;">No</th>
                <th class="text-center" style="width: 150px;">Hari</th>
                <th class="text-center" style="width: 150px;">Jam</th>
                <th class="text-center" style="width: auto;">Mata Pelajaran</th>
                <th class="text-center" style="width: auto;">Pengajar</th>
                <th class="text-center" style="width: auto;">Ruangan</th>
              </tr>
            </thead>
            <tbody>
              <?php if (count($jadwal_pelajaran_item) > 0) : ?>
                <?php $no = 1 ?>
                <?php $tempHari = '' ?>

                <?php foreach ($jadwal_pelajaran_item as $key => $item) : ?>
                  <?php $jam_mulai = ($item->jam_mulai) ? substr($item->jam_mulai, 0, 5) : '00:00' ?>
                  <?php $jam_selesai = ($item->jam_selesai) ? substr($item->jam_selesai, 0, 5) : '00:00' ?>
                  <?php $same_day_count = $controller->get_day_count($item->jadwal_pelajaran_id, $item->hari_nomor) ?>

                  <tr>
                    <td class="text-center"><?= $no++ ?></td>

                    <?php if ($item->hari !== $tempHari) : ?>
                      <td <?= ($same_day_count > 1) ? 'rowspan="' . $same_day_count . '"' : '' ?> class="text-center" valign="middle">
                        <?= $item->hari ?>
                      </td>
                    <?php endif; ?>

                    <td class="text-center"><?= $jam_mulai . ' - ' . $jam_selesai ?></td>
                    <td><?= $item->mata_pelajaran_nama ?></td>
                    <td><?= $item->pengajar_nama_lengkap ?></td>
                    <td><?= $item->ruangan ?></td>
                  </tr>

                  <?php $tempHari = $item->hari ?>
                <?php endforeach; ?>
              <?php endif; ?>
            </tbody>
          </table>
        </td>
      </tr>
    </table>
  </div>
</div>