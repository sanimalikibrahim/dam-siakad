<script type="text/javascript">
  $(document).ready(function() {

    var _key = "";
    var _section = "jadwalpelajaran";
    var _table = "table-jadwalpelajaran";
    var _table_item = "table-jadwalpelajaran-item";
    var _modal = "modal-form-jadwalpelajaran";
    var _modal_item = "modal-form-jadwalpelajaran-item";
    var _form = "form-jadwalpelajaran";
    var _form_item = "form-jadwalpelajaran-item";

    // Initialize DataTables: Index
    if ($("#" + _table)[0]) {
      var table_jadwalpelajaran = $("#" + _table).DataTable({
        processing: true,
        serverSide: true,
        ajax: {
          url: "<?php echo base_url('jadwalpelajaran/ajax_get_all/') ?>",
          type: "get"
        },
        columns: [{
            data: null,
            render: function(data, type, row, meta) {
              return meta.row + meta.settings._iDisplayStart + 1;
            }
          },
          {
            data: "semester"
          },
          {
            data: "kelas",
            render: function(data) {
              const result = isJson(data);
              var output = '';

              if (result !== false) {
                result.map((item) => {
                  output += item.replace(/\#/g, " ") + ", ";
                });
              } else {
                output = data;
              };

              return output.replace(/,\s*$/, "");
            }
          },
          {
            data: "created_at"
          },
          {
            data: "updated_at"
          },
          {
            data: null,
            className: "center",
            render: function(data, type, row, meta) {
              var output = '';

              output += '<div class="action">';
              output += '<div class="dropleft">';
              output += '<a class="btn btn-sm btn-light btn-table-action" data-toggle="dropdown" style="color: #525a62;"><i class="zmdi zmdi-menu"></i> Action</a>';
              output += '<div class="dropdown-menu dropdown-menu-right">';
              output += '<a href="javascript:;" class="dropdown-item action-edit" data-toggle="modal" data-target="#' + _modal + '">Edit</a>';
              output += '<a href="javascript:;" class="dropdown-item action-delete">Delete</a>';
              output += '<div class="dropdown-divider"></div>';
              output += '<a href="<?= base_url('jadwalpelajaran/detail/') ?>' + row.id + '" class="dropdown-item">Detail</a>';
              output += '<a href="<?= base_url('jadwalpelajaran/d-pdf/') ?>' + row.id + '" class="dropdown-item">Export to PDF</a>';
              output += '</div>';
              output + = '&nbsp;<a href="<?= base_url('jadwalpelajaran/item/') ?>' + row.id + '" class="btn btn-sm btn-success btn-table-action action-set-jadwal"><i class="zmdi zmdi-time"></i> Set Jadwal</a>&nbsp;';
              output += '</div>';

              return output;
            }
          }
        ],
        autoWidth: !1,
        responsive: {
          details: {
            renderer: function(api, rowIdx, columns) {
              var hideColumn = [];
              var data = $.map(columns, function(col, i) {
                return ($.inArray(col.columnIndex, hideColumn)) ?
                  '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                  '<td class="dt-details-td">' + col.title + ':' + '</td> ' +
                  '<td class="dt-details-td">' + col.data + '</td>' +
                  '</tr>' :
                  '';
              }).join('');

              return data ? $('<table/>').append(data) : false;
            },
            type: "inline",
            target: 'tr',
          }
        },
        columnDefs: [{
          className: 'desktop',
          targets: [0, 1, 2, 3, 4, 5]
        }, {
          className: 'tablet',
          targets: [0, 1, 2, 3]
        }, {
          className: 'mobile',
          targets: [0, 2]
        }, {
          responsivePriority: 2,
          targets: -1
        }],
        pageLength: 15,
        language: {
          searchPlaceholder: "Search...",
          sProcessing: '<div style="text-align: center;"><div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div></div>'
        },
        sDom: '<"dataTables_ct"><"dataTables__top"fb>rt<"dataTables__bottom"ip><"clear">',
        buttons: [{
          extend: "excelHtml5",
          title: "Export Result"
        }, {
          extend: "print",
          title: "Export Result"
        }],
        initComplete: function(a, b) {
          $(this).closest(".dataTables_wrapper").find(".dataTables__top").prepend(
            '<div class="dataTables_buttons hidden-sm-down actions">' +
            '<span class="actions__item zmdi zmdi-refresh" data-table-action="reload" title="Reload" />' +
            '</div>'
          );
        },
      });

      $(".dataTables_filter input[type=search]").focus(function() {
        $(this).closest(".dataTables_filter").addClass("dataTables_filter--toggled")
      });

      $(".dataTables_filter input[type=search]").blur(function() {
        $(this).closest(".dataTables_filter").removeClass("dataTables_filter--toggled")
      });

      $("body").on("click", "[data-table-action]", function(a) {
        a.preventDefault();
        var b = $(this).data("table-action");
        if ("reload" === b) {
          $("#" + _table).DataTable().ajax.reload(null, false);
        };
      });
    };

    // Initialize DataTables: Item
    if ($("#" + _table_item)[0]) {
      var table_jadwalpelajaran_item = $("#" + _table_item).DataTable({
        processing: true,
        serverSide: true,
        ajax: {
          url: "<?php echo base_url('jadwalpelajaran/ajax_get_all_item/' . $data_id) ?>",
          type: "get"
        },
        columns: [{
            data: null,
            render: function(data, type, row, meta) {
              return meta.row + meta.settings._iDisplayStart + 1;
            }
          },
          {
            data: "hari"
          },
          {
            data: "sesi"
          },
          {
            data: null,
            render: function(data, type, row, meta) {
              var jam_mulai = (row.jam_mulai !== null) ? row.jam_mulai.substr(0, 5) : '00:00';
              var jam_selesai = (row.jam_selesai !== null) ? row.jam_selesai.substr(0, 5) : '00:00';

              return jam_mulai + " - " + jam_selesai;
            }
          },
          {
            data: "mata_pelajaran_nama"
          },
          {
            data: "pengajar_nama_lengkap"
          },
          {
            data: "ruangan"
          },
          {
            data: "created_at"
          },
          {
            data: "updated_at"
          },
          {
            data: null,
            className: "center",
            render: function(data, type, row, meta) {
              const output = '<div class="action">' +
                '<a href="javascript:;" class="btn btn-sm btn-light btn-table-action action-edit" data-toggle="modal" data-target="#' + _modal_item + '"><i class="zmdi zmdi-edit"></i> Edit</a>&nbsp;' +
                '<a href="javascript:;" class="btn btn-sm btn-light btn-table-action action-delete"><i class="zmdi zmdi-delete"></i> Delete</a>' +
                '</div>';

              return output;
            }
          }
        ],
        autoWidth: !1,
        responsive: {
          details: {
            renderer: function(api, rowIdx, columns) {
              var hideColumn = [];
              var data = $.map(columns, function(col, i) {
                return ($.inArray(col.columnIndex, hideColumn)) ?
                  '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                  '<td class="dt-details-td">' + col.title + ':' + '</td> ' +
                  '<td class="dt-details-td">' + col.data + '</td>' +
                  '</tr>' :
                  '';
              }).join('');

              return data ? $('<table/>').append(data) : false;
            },
            type: "inline",
            target: 'tr',
          }
        },
        columnDefs: [{
          targets: [1, 7],
          visible: false
        }, {
          className: 'desktop',
          targets: [0, 2, 3, 4, 5, 6, 7, 8, 9]
        }, {
          className: 'tablet',
          targets: [0, 2, 3, 4]
        }, {
          className: 'mobile',
          targets: [0, 4]
        }, {
          responsivePriority: 2,
          targets: -1
        }],
        pageLength: 15,
        language: {
          searchPlaceholder: "Search...",
          sProcessing: '<div style="text-align: center;"><div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div></div>'
        },
        sDom: '<"dataTables_ct"><"dataTables__top"fb>rt<"dataTables__bottom"ip><"clear">',
        buttons: [{
          extend: "excelHtml5",
          title: "Export Result"
        }, {
          extend: "print",
          title: "Export Result"
        }],
        initComplete: function(a, b) {
          $(this).closest(".dataTables_wrapper").find(".dataTables__top").prepend(
            '<div class="dataTables_buttons hidden-sm-down actions">' +
            '<span class="actions__item zmdi zmdi-refresh" data-table-action="reload" title="Reload" />' +
            '</div>'
          );
        },
        drawCallback: function(settings) {
          var api = this.api();
          var rows = api.rows({
            page: 'current'
          }).nodes();
          var last = null;

          api.column(1, {
            page: 'current'
          }).data().each(function(group, i) {
            group = (group != null) ? group : "(Has no parent)";
            if (last !== group) {
              $(rows).eq(i).before(
                '<tr class="group"><td colspan="10">' + group + '</td></tr>'
              );

              last = group;
            }
          });
        },
      });

      $(".dataTables_filter input[type=search]").focus(function() {
        $(this).closest(".dataTables_filter").addClass("dataTables_filter--toggled")
      });

      $(".dataTables_filter input[type=search]").blur(function() {
        $(this).closest(".dataTables_filter").removeClass("dataTables_filter--toggled")
      });

      $("body").on("click", "[data-table-action]", function(a) {
        a.preventDefault();
        var b = $(this).data("table-action");
        if ("reload" === b) {
          $("#" + _table_item).DataTable().ajax.reload(null, false);
        };
      });
    };

    // Handle data add
    $("#" + _section).on("click", "button." + _section + "-action-add", function(e) {
      e.preventDefault();
      resetForm();
    });

    // Handle data add: Item
    $("#" + _section).on("click", "button." + _section + "-item-action-add", function(e) {
      e.preventDefault();
      resetFormItem();
    });

    // Handle data edit
    $("#" + _table).on("click", "a.action-edit", function(e) {
      e.preventDefault();
      resetForm();
      var temp = table_jadwalpelajaran.row($(this).closest('tr')).data();

      // Set key for update params, important!
      _key = temp.id;

      $("#" + _form + " ." + _section + "-semester").val(temp.semester).trigger('input');
      $("#" + _form + " ." + _section + "-kelas").val(JSON.parse(temp.kelas)).trigger('change');
    });

    // Handle data edit: Item
    $("#" + _table_item).on("click", "a.action-edit", function(e) {
      e.preventDefault();
      resetFormItem();
      var temp = table_jadwalpelajaran_item.row($(this).closest('tr')).data();

      // Set key for update params, important!
      _key = temp.id;

      $("#" + _form_item + " ." + _section + "-item-hari_nomor").val(temp.hari_nomor).trigger('change');
      $("#" + _form_item + " ." + _section + "-item-sesi").val(temp.sesi).trigger('input');
      $("#" + _form_item + " ." + _section + "-item-jam_mulai").val(temp.jam_mulai).trigger('input');
      $("#" + _form_item + " ." + _section + "-item-jam_selesai").val(temp.jam_selesai).trigger('input');
      $("#" + _form_item + " ." + _section + "-item-mata_pelajaran_id").val(temp.mata_pelajaran_id).trigger('change');
      $("#" + _form_item + " ." + _section + "-item-pengajar_id").val(temp.pengajar_id).trigger('change');
      $("#" + _form_item + " ." + _section + "-item-ruangan").val(temp.ruangan).trigger('input');
    });

    // Handle data submit
    $("#" + _modal + " ." + _section + "-action-save").on("click", function(e) {
      e.preventDefault();
      $.ajax({
        type: "post",
        url: "<?php echo base_url('jadwalpelajaran/ajax_save/') ?>" + _key,
        data: $("#" + _form).serialize(),
        success: function(response) {
          var response = JSON.parse(response);
          if (response.status === true) {
            resetForm();
            $("#" + _modal).modal("hide");
            $("#" + _table).DataTable().ajax.reload(null, false);
            notify(response.data, "success");
          } else {
            notify(response.data, "danger");
          };
        }
      });
    });

    // Handle data submit: Item
    $("#" + _modal_item + " ." + _section + "-item-action-save").on("click", function(e) {
      e.preventDefault();
      $.ajax({
        type: "post",
        url: "<?php echo base_url('jadwalpelajaran/ajax_save_item/') ?>" + _key,
        data: $("#" + _form_item).serialize(),
        success: function(response) {
          var response = JSON.parse(response);
          if (response.status === true) {
            resetFormItem();
            $("#" + _modal_item).modal("hide");
            $("#" + _table_item).DataTable().ajax.reload(null, false);
            notify(response.data, "success");
          } else {
            notify(response.data, "danger");
          };
        }
      });
    });

    // Handle data delete
    $("#" + _table).on("click", "a.action-delete", function(e) {
      e.preventDefault();
      var temp = table_jadwalpelajaran.row($(this).closest('tr')).data();

      swal({
        title: "Are you sure to delete?",
        text: "Once deleted, you will not be able to recover this data!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false
      }).then((result) => {
        if (result.value) {
          $.ajax({
            type: "delete",
            url: "<?php echo base_url('jadwalpelajaran/ajax_delete/') ?>" + temp.id,
            dataType: "json",
            success: function(response) {
              if (response.status) {
                $("#" + _table).DataTable().ajax.reload(null, false);
                notify(response.data, "success");
              } else {
                notify(response.data, "danger");
              };
            }
          });
        };
      });
    });

    // Handle data delete: Item
    $("#" + _table_item).on("click", "a.action-delete", function(e) {
      e.preventDefault();
      var temp = table_jadwalpelajaran_item.row($(this).closest('tr')).data();

      swal({
        title: "Are you sure to delete?",
        text: "Once deleted, you will not be able to recover this data!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false
      }).then((result) => {
        if (result.value) {
          $.ajax({
            type: "delete",
            url: "<?php echo base_url('jadwalpelajaran/ajax_delete_item/') ?>" + temp.id,
            dataType: "json",
            success: function(response) {
              if (response.status) {
                $("#" + _table_item).DataTable().ajax.reload(null, false);
                notify(response.data, "success");
              } else {
                notify(response.data, "danger");
              };
            }
          });
        };
      });
    });

    // Handle form reset
    resetForm = () => {
      _key = "";
      $("#" + _form).trigger("reset");
      $("#" + _form + " ." + _section + "-kelas").val("").trigger('change');
    };

    // Handle form reset: Item
    resetFormItem = () => {
      _key = "";
      $("#" + _form_item).trigger("reset");
      $("#" + _form_item + " ." + _section + "-item-mata_pelajaran_id").val("").trigger('change');
      $("#" + _form_item + " ." + _section + "-item-pengajar_id").val("").trigger('change');
    };

    // Handle is json object
    function isJson(object) {
      try {
        return $.parseJSON(object);
      } catch (err) {
        return false;
      };
    };

  });
</script>