<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . 'controllers/AppBackend.php');
require_once(APPPATH . 'controllers/ApiClient.php');

class Synckriteria extends AppBackend
{
  function __construct()
  {
    parent::__construct();
    $this->load->model([
      'KriteriaModel'
    ]);
    $this->load->library('form_validation');
  }

  public function index()
  {
    $user_id = $this->session->userdata('user')['id'];
    $data = array(
      'app' => $this->app(),
      'main_js' => $this->load_main_js('sync/views/kriteria/main.js.php', true),
      'card_title' => 'Sinkronisasi › Kriteria',
    );
    $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
    $this->template->load_view('kriteria/index', $data, TRUE);
    $this->template->render();
  }

  public function ajax_sync()
  {
    $this->handle_ajax_request();

    $apiClient = new ApiClient();
    $response = $apiClient->getListQuiz();

    if ($response['status'] === true) {
      $temp = $response['data'];
      $data = [];

      if (count($temp) > 0) {
        foreach ($temp as $index => $item) {
          $data[] = [
            'id' => $item->id,
            'nama' => $item->title->rendered
          ];
        };
      };

      if (count($data) > 0) {
        // Store to db
        $this->KriteriaModel->truncate();
        $transaction = $this->KriteriaModel->insertBatch($data);

        $result = $transaction;
      } else {
        $result = ['status' => true, 'data' => 'No data found.'];
      };
    } else {
      $result = ['status' => false, 'data' => 'Failed to get the data.'];
    };

    echo json_encode($result);
  }
}
