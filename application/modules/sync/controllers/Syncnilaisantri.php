<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . 'controllers/AppBackend.php');
require_once(APPPATH . 'controllers/ApiClient.php');

class Syncnilaisantri extends AppBackend
{
  function __construct()
  {
    parent::__construct();
    $this->load->model([
      'PenilaianModel',
      'KriteriaModel',
      'MappingKelasLmsModel',
      'PenilaianmapelModel',
      'UserModel',
    ]);
    $this->load->library('form_validation');
  }

  public function index()
  {
    $listKelas = $this->MappingKelasLmsModel->getAllList();
    $data = array(
      'app' => $this->app(),
      'main_js' => $this->load_main_js('sync/views/nilaisantri/main.js.php', true),
      'card_title' => 'Sinkronisasi › Nilai Santri',
      'list_kelas' => $this->init_list($listKelas, 'learndash_group_id', 'kelas_concated', null, null, 'Kelas')
    );
    $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
    $this->template->load_view('nilaisantri/index', $data, TRUE);
    $this->template->render();
  }

  public function ajax_sync()
  {
    $this->handle_ajax_request();
    $this->form_validation->set_rules($this->PenilaianmapelModel->rules_sync());

    $groupId = $this->input->post('learndash_group_id');
    $courseId = $this->input->post('learndash_course_id');
    $lessonId = $this->input->post('learndash_lesson_id');
    $topicId = $this->input->post('learndash_topic_id');
    $quizId = $this->input->post('learndash_quiz_id');
    $tanggal = $this->input->post('tanggal');
    $jenis = $this->input->post('jenis');
    $catatan = $this->input->post('catatan');
    $catatan = (!is_null($catatan) && !empty($catatan)) ? $catatan : 'Sync LMS at ' . date('Y-m-d H:i:s');

    if ($this->form_validation->run() === true) {
      $userList = $this->MappingKelasLmsModel->getAllSantriKelas($groupId);
      $currentPage = 1;
      $maxPage = 50;
      $perPage = 50;
      $payload = [];
      $payloadMerge = [];

      for ($page = $currentPage; $page < $maxPage; $page++) {
        $apiClient = new ApiClient();
        $response = $apiClient->getDetailQuiz($quizId, $page, $perPage);

        if ($response['status'] === true) {
          $temp = $response['data'];

          if (count($temp) > 0) {
            foreach ($temp as $index => $item) {
              // Set temporary data
              $payload[] = [
                'user_id' => $item->user,
                'nilai' => $item->answers_correct,
                'date' => $item->date,
              ];
            };
          };
        } else {
          $page = $maxPage;
        };
      };

      // Merge user local with lms
      if (count($payload) > 0) {
        foreach ($userList as $index => $item) {
          $checkNilai = $this->searchInArray($payload, 'user_id', $item->learndash_user_id);
          $tempNilai = null;
          $tempDate = null;

          if (!is_null($checkNilai)) {
            $tempNilai = $checkNilai['nilai'];
            $tempDate = date('Y-m-d', strtotime($checkNilai['date']));
          };

          // Set final data
          $payloadMerge[] = array(
            'kelas' => $item->kelas . '#' . $item->sub_kelas,
            'santri_id' => $item->id,
            'mata_pelajaran_id' => 3, // can aya
            'pengajar_id' => $this->session->userdata('user')['id'],
            'nilai' => $tempNilai,
            'catatan' => $catatan,
            'tanggal' => $tanggal,
            'jenis' => $jenis,
            'learndash_group_id' => $groupId,
            'learndash_course_id' => $courseId,
            'learndash_lesson_id' => $lessonId,
            'learndash_topic_id' => $topicId,
            'learndash_quiz_id' => $quizId,
            'learndash_user_id' => $item->learndash_user_id,
            'learndash_date' => $tempDate,
            'created_by' => $this->session->userdata('user')['id'],
          );
        };
      };

      if (count($payloadMerge) > 0) {
        // Store to db
        $this->PenilaianmapelModel->deleteByKelasDate($tanggal, $groupId, $courseId, $lessonId, $topicId, $quizId, $jenis);
        $transaction = $this->PenilaianmapelModel->insertBatch($payloadMerge);

        $result = $transaction;
      } else {
        $result = ['status' => true, 'data' => 'No data found.', 'validation' => true];
      };

      echo json_encode($result);
    } else {
      $errors = validation_errors('<div>- ', '</div>');
      echo json_encode(array('status' => false, 'data' => $errors, 'validation' => false));
    };
  }

  public function ajax_get_lms()
  {
    $this->handle_ajax_request();

    $type = $this->input->get('type');
    $ref = $this->input->get('ref');
    $ref2 = $this->input->get('ref2');
    $ref3 = $this->input->get('ref3');

    $apiClient = new ApiClient();

    switch ($type) {
      case 'course':
        $response = $apiClient->getListCourse(100, $ref);
        break;
      case 'lesson':
        $response = $apiClient->getListLesson(100, $ref);
        break;
      case 'topic':
        $response = $apiClient->getListTopic(100, $ref, $ref2);
        break;
      case 'quiz':
        $response = $apiClient->getListQuiz2(100, $ref, $ref2, $ref3);
        break;
    };

    if ($response['status'] === true) {
      $temp = $response['data'];
      $data = [];

      if (count($temp) > 0) {
        foreach ($temp as $index => $item) {
          $data[] = (object) [
            'id' => $item->id,
            'nama' => $item->title->rendered,
            'id_nama' => $item->id . ' | ' . $item->title->rendered
          ];
        };
      };

      $response = array('status' => true, 'data' => $data);
    } else {
      $response = array('status' => false, 'data' => 'Terjadi kesalahan ketika mengambil data ke LMS.');
    };

    echo json_encode($response);
  }
}
