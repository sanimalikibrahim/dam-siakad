<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . 'controllers/AppBackend.php');
require_once(APPPATH . 'controllers/ApiClient.php');

class Syncpenilaian extends AppBackend
{
  function __construct()
  {
    parent::__construct();
    $this->load->model([
      'PenilaianModel',
      'KriteriaModel'
    ]);
    $this->load->library('form_validation');
  }

  public function index()
  {
    $data = array(
      'app' => $this->app(),
      'main_js' => $this->load_main_js('sync/views/penilaian/main.js.php', true),
      'card_title' => 'Sinkronisasi › Penilaian',
    );
    $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
    $this->template->load_view('penilaian/index', $data, TRUE);
    $this->template->render();
  }

  public function ajax_sync()
  {
    $this->handle_ajax_request();

    $kriteria = $this->KriteriaModel->getAll();
    $currentPage = 1;
    $maxPage = 50;
    $perPage = 50;
    $data = [];

    if (count($kriteria) > 0) {
      foreach ($kriteria as $key => $quiz) {
        $quizId = $quiz->id;

        for ($page = $currentPage; $page < $maxPage; $page++) {
          $apiClient = new ApiClient();
          $response = $apiClient->getDetailQuiz($quizId, $page, $perPage);

          if ($response['status'] === true) {
            $temp = $response['data'];

            if (count($temp) > 0) {
              foreach ($temp as $index => $item) {
                $data[] = [
                  'kriteria_id' => $quizId,
                  'user_id' => $item->user,
                  'nilai' => $item->answers_correct
                ];
              };
            };
          } else {
            $page = $maxPage;
          };
        };
      };

      if (count($data) > 0) {
        // Store to db
        $this->PenilaianModel->truncate();
        $transaction = $this->PenilaianModel->insertBatch($data);

        $result = $transaction;
      } else {
        $result = ['status' => true, 'data' => 'No data found.'];
      };
    } else {
      $result = ['status' => true, 'data' => 'Data kriteria has no rows.'];
    };

    echo json_encode($result);
  }
}
