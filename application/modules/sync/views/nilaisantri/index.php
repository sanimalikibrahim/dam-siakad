<style type="text/css">
    .filter-main {
        padding-top: 4rem;
    }

    .filter-info {
        text-align: center;
        padding: 5rem 0rem;
    }

    .filter-info p {
        line-height: .75rem;
        font-size: 1.075rem;
        color: #999;
        font-weight: 400;
    }

    .filter-info .zmdi {
        font-size: 3.5rem;
        margin-bottom: 1rem;
        color: #c5c5c5;
    }

    .page-action-filter {
        padding: 15px 40px;
        font-size: 13px;
        font-weight: 800;
    }

    .select2 {
        text-align: left !important;
        border: 1px solid #eceff1 !important;
        border-radius: 2px;
        padding-left: 8px;
        padding-right: 8px;
    }

    .flatpickr-calendar.open {
        z-index: 999999;
    }

    span.select2-container {
        z-index: 1;
    }

    .custom-input {
        border: 1px solid #eceff1 !important;
        height: 34.13px;
        padding-left: 8px;
        padding-right: 8px;
    }
</style>

<section id="syncnilaisantri">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title"><?php echo (isset($card_title)) ? $card_title : '' ?></h4>
            <h6 class="card-subtitle"><?php echo (isset($card_subTitle)) ? $card_subTitle : '' ?></h6>

            <div class="text-center filter-main">
                <?php include_once(FCPATH . 'application/modules/sync/views/info1.php') ?>

                <form id="form-sync-nilaisantri" autocomplete="off">
                    <!-- CSRF -->
                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />

                    <div class="row justify-content-md-center">
                        <div class="col-md-4 col-xs-12">
                            <div class="row">
                                <div class="col-md-6 col-xs-12">
                                    <div class="form-group mb-3 text-left">
                                        <label required>Tanggal</label>
                                        <input type="text" name="tanggal" id="filter-tanggal" class="custom-select flatpickr-datemax-today" placeholder="Tanggal" style="height: 34.13px;" value="<?= date('Y-m-d') ?>" />
                                        <i class="form-group__bar"></i>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-12">
                                    <div class="form-group mb-3 text-left">
                                        <label required>Jenis</label>
                                        <select name="jenis" id="filter-jenis" class="form-control select2" data-placeholder="Jenis &#8595;" required>
                                            <option value="1">Harian</option>
                                            <option value="2">UTS</option>
                                            <option value="3">UAS</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group mb-3 text-left">
                                <label required>Kelas</label>
                                <select name="learndash_group_id" id="filter-learndash_group_id" class="form-control select2" data-placeholder="Kelas &#8595;" required>
                                    <?= $list_kelas ?>
                                </select>
                            </div>
                            <div class="form-group mb-3 text-left">
                                <label required>Program</label>
                                <select name="learndash_course_id" id="filter-learndash_course_id" class="form-control select2" data-placeholder="Program &#8595;" required>
                                </select>
                            </div>
                            <div class="form-group mb-3 text-left">
                                <label required>Pelajaran</label>
                                <select name="learndash_lesson_id" id="filter-learndash_lesson_id" class="form-control select2" data-placeholder="Pelajaran &#8595;" required>
                                </select>
                            </div>
                            <div class="form-group mb-3 text-left">
                                <label required>Topik</label>
                                <select name="learndash_topic_id" id="filter-learndash_topic_id" class="form-control select2" data-placeholder="Topik &#8595;" required>
                                </select>
                            </div>
                            <div class="form-group mb-3 text-left">
                                <label required>Ulangan/Ujian</label>
                                <select name="learndash_quiz_id" id="filter-learndash_quiz_id" class="form-control select2" data-placeholder="Ulangan/Ujian &#8595;" required>
                                </select>
                            </div>
                            <div class="form-group text-left">
                                <label>Catatan</label>
                                <input type="text" name="catatan" class="form-control custom-input" placeholder="Catatan" />
                                <i class="form-group__bar"></i>
                            </div>
                        </div>
                    </div>
                </form>

                <button class="btn btn--raised btn-primary btn--icon-text page-action-filter">
                    <i class="zmdi zmdi-play"></i> START
                </button>
            </div>

            <?php include_once(FCPATH . 'application/modules/sync/views/info2.php') ?>
        </div>
    </div>
</section>