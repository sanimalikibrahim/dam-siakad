<script type="text/javascript">
  $(document).ready(function() {

    var _section = "syncnilaisantri";

    // Handle ajax loader
    $(document).ajaxStart(function() {
      $(".body-spinner").css("display", "flex");
    });
    $(document).ajaxStop(function() {
      $(".body-spinner").hide();
    });

    // Handle filter submit
    $("#" + _section + " .page-action-filter").on("click", function(e) {
      e.preventDefault();

      var spinnerContent = '<h5 class="m-0" style="color: var(--white);">Sending request...</h5>';
      spinnerContent += '<span style="color: var(--white);">You cannot cancel this process, dont close this tab activity!</span>';

      swal({
        title: "Are you sure to synchronization?",
        text: "Once synchronized, you will not be able to recover this data!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false
      }).then((result) => {
        if (result.value) {
          $(".body-spinner .body-spinner-content").html(spinnerContent);

          $.ajax({
            type: "post",
            url: "<?php echo base_url('sync/syncnilaisantri/ajax_sync/') ?>",
            data: $("#form-sync-nilaisantri").serialize(),
            success: function(result) {
              var response = JSON.parse(result);

              var swallTitle = "Success";
              var swallType = "success";

              if (response.status) {
                swallTitle = "Success";
                swallType = "success";
              } else {
                swallTitle = "Failed";
                swallType = "error";
              };

              if (response.validation === false) {
                notify(response.data, "danger");
              } else {
                swal({
                  title: swallTitle,
                  text: response.data,
                  type: swallType,
                  showCancelButton: false,
                  confirmButtonColor: '#39bbb0',
                  confirmButtonText: "OK",
                  closeOnConfirm: false
                }).then((result) => {
                  // When OK todo here...
                });
              };
            }
          });
        };
      });
    });

    // Handle get program
    $(document).on("change", "#filter-learndash_group_id", function(e) {
      e.preventDefault();
      var value = $(this).val();

      $.ajax({
        type: "get",
        url: "<?php echo base_url('sync/syncnilaisantri/ajax_get_lms/') ?>",
        data: {
          type: "course",
          ref: value
        },
        success: function(response) {
          var response = JSON.parse(response);

          // Clear current options
          $("#filter-learndash_course_id").empty();
          $("#filter-learndash_lesson_id").empty();
          $("#filter-learndash_topic_id").empty();
          $("#filter-learndash_quiz_id").empty();

          if (response.status === true) {
            var data = response.data;

            if (data.length > 0) {
              data.map((value, index) => {
                var newOption = new Option(value.id_nama, value.id, false, false);
                $("#filter-learndash_course_id").append(newOption);
              });

              $("#filter-learndash_course_id").val(null).trigger("change");
              _isFetchGroup = true;
            };
          } else {
            notify(response.data, "danger");
          };
        }
      });
    });

    // Handle get lesson
    $(document).on("change", "#filter-learndash_course_id", function(e) {
      e.preventDefault();
      var value = $(this).val();
      var parent = $("#filter-learndash_course_id").val();

      if (parent != null && parent != "") {
        $.ajax({
          type: "get",
          url: "<?php echo base_url('sync/syncnilaisantri/ajax_get_lms/') ?>",
          data: {
            type: "lesson",
            ref: value
          },
          success: function(response) {
            var response = JSON.parse(response);

            // Clear current options
            $("#filter-learndash_lesson_id").empty();
            $("#filter-learndash_topic_id").empty();
            $("#filter-learndash_quiz_id").empty();

            if (response.status === true) {
              var data = response.data;

              if (data.length > 0) {
                data.map((value, index) => {
                  var newOption = new Option(value.id_nama, value.id, false, false);
                  $("#filter-learndash_lesson_id").append(newOption);
                });

                $("#filter-learndash_lesson_id").val(null).trigger("change");
                _isFetchGroup = true;
              };
            } else {
              notify(response.data, "danger");
            };
          }
        });
      };
    });

    // Handle get topic
    $(document).on("change", "#filter-learndash_lesson_id", function(e) {
      e.preventDefault();
      var value = $(this).val();
      var parent = $("#filter-learndash_lesson_id").val();
      var program = $("#filter-learndash_course_id").val();

      if (parent != null && parent != "") {
        $.ajax({
          type: "get",
          url: "<?php echo base_url('sync/syncnilaisantri/ajax_get_lms/') ?>",
          data: {
            type: "topic",
            ref: value,
            ref2: program
          },
          success: function(response) {
            var response = JSON.parse(response);

            // Clear current options
            $("#filter-learndash_topic_id").empty();
            $("#filter-learndash_quiz_id").empty();

            if (response.status === true) {
              var data = response.data;

              if (data.length > 0) {
                data.map((value, index) => {
                  var newOption = new Option(value.id_nama, value.id, false, false);
                  $("#filter-learndash_topic_id").append(newOption);
                });

                $("#filter-learndash_topic_id").val(null).trigger("change");
                _isFetchGroup = true;
              };
            } else {
              notify(response.data, "danger");
            };
          }
        });
      };
    });

    // Handle get quiz
    $(document).on("change", "#filter-learndash_topic_id", function(e) {
      e.preventDefault();
      var value = $(this).val();
      var parent = $("#filter-learndash_topic_id").val();
      var program = $("#filter-learndash_course_id").val();
      var lesson = $("#filter-learndash_lesson_id").val();

      if (parent != null && parent != "") {
        $.ajax({
          type: "get",
          url: "<?php echo base_url('sync/syncnilaisantri/ajax_get_lms/') ?>",
          data: {
            type: "quiz",
            ref: value,
            ref2: program,
            ref3: lesson
          },
          success: function(response) {
            var response = JSON.parse(response);

            // Clear current options
            $("#filter-learndash_quiz_id").empty();

            if (response.status === true) {
              var data = response.data;

              if (data.length > 0) {
                data.map((value, index) => {
                  var newOption = new Option(value.id_nama, value.id, false, false);
                  $("#filter-learndash_quiz_id").append(newOption);
                });

                $("#filter-learndash_quiz_id").val(null).trigger("change");
                _isFetchGroup = true;
              };
            } else {
              notify(response.data, "danger");
            };
          }
        });
      };
    });

  });
</script>