<style type="text/css">
    .filter-main {
        padding-top: 4rem;
    }

    .filter-info {
        text-align: center;
        padding: 5rem 0rem;
    }

    .filter-info p {
        line-height: .75rem;
        font-size: 1.075rem;
        color: #999;
        font-weight: 400;
    }

    .filter-info .zmdi {
        font-size: 3.5rem;
        margin-bottom: 1rem;
        color: #c5c5c5;
    }

    .page-action-filter {
        padding: 15px 40px;
        font-size: 13px;
        font-weight: 800;
    }
</style>

<section id="synckriteria">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title"><?php echo (isset($card_title)) ? $card_title : '' ?></h4>
            <h6 class="card-subtitle"><?php echo (isset($card_subTitle)) ? $card_subTitle : '' ?></h6>

            <div class="text-center filter-main">
                <?php include_once(FCPATH . 'application/modules/sync/views/info1.php') ?>

                <button class="btn btn--raised btn-primary btn--icon-text page-action-filter">
                    <i class="zmdi zmdi-play"></i> START
                </button>
            </div>

            <?php include_once(FCPATH . 'application/modules/sync/views/info2.php') ?>
        </div>
    </div>
</section>