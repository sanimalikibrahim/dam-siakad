<script type="text/javascript">
  $(document).ready(function() {

    var _section = "synckriteria";

    // Handle ajax loader
    $(document).ajaxStart(function() {
      $(".body-spinner").css("display", "flex");
    });
    $(document).ajaxStop(function() {
      $(".body-spinner").hide();
    });

    // Handle filter submit
    $("#" + _section + " .page-action-filter").on("click", function() {
      var spinnerContent = '<h5 class="m-0" style="color: var(--white);">Sending request...</h5>';
      spinnerContent += '<span style="color: var(--white);">You cannot cancel this process, dont close this tab activity!</span>';

      swal({
        title: "Are you sure to synchronization?",
        text: "Once synchronized, you will not be able to recover this data!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false
      }).then((result) => {
        if (result.value) {
          $(".body-spinner .body-spinner-content").html(spinnerContent);

          $.ajax({
            type: "get",
            url: "<?php echo base_url('sync/synckriteria/ajax_sync/') ?>",
            dataType: "json",
            success: function(response) {
              var swallTitle = "Success";
              var swallType = "success";

              if (response.status) {
                swallTitle = "Success";
                swallType = "success";
              } else {
                swallTitle = "Failed";
                swallType = "error";
              };

              swal({
                title: swallTitle,
                text: response.data,
                type: swallType,
                showCancelButton: false,
                confirmButtonColor: '#39bbb0',
                confirmButtonText: "OK",
                closeOnConfirm: false
              }).then((result) => {
                window.location.href = "";
              });
            }
          });
        };
      });
    });

  });
</script>