<div class="mb-5">
  <strong>PENTING!</strong>
  <li>Proses sinkronisasi tidak dapat dibatalkan</li>
  <li>Semua data akan dihapus terlebih dahulu, dan diisi dengan data baru</li>
  <li>Pastikan koneksi internet stabil</li>
</div>