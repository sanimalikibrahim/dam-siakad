<script type="text/javascript">
  $(document).ready(function() {

    var _key = "";
    var _activeRole = "<?= $this->session->userdata('user')['role'] ?>";
    var _section = "raport_pengembangandiri";
    var _table_rchb_kompetensi_dasar = "table-raport_pengembangandiri-kompetensi_dasar";
    var _table_rchb_ibadah_santri = "table-raport_pengembangandiri-ibadah_santri";
    var _modal_rchb_kompetensi_dasar = "modal-raport_pengembangandiri-kompetensi_dasar";
    var _modal_rchb_ibadah_santri = "modal-raport_pengembangandiri-ibadah_santri";
    var _form_rchb_kompetensi_dasar = "form-raport_pengembangandiri-kompetensi_dasar";
    var _form_rchb_ibadah_santri = "form-raport_pengembangandiri-ibadah_santri";

    var _combo_predikat = function(name, value) {
      return `
        <select name="${name}" class="custom-select p-0 bg-white" style="text-align: center; height: 26px;">
          <option disabled selected>Select &#8595;</option>
          <option value="A" ${(value === 'A' ? 'selected' : '')}>A</option>
          <option value="B" ${(value === 'B' ? 'selected' : '')}>B</option>
          <option value="C" ${(value === 'C' ? 'selected' : '')}>C</option>
          <option value="D" ${(value === 'D' ? 'selected' : '')}>D</option>
        </select>
      `;
    };

    // Initialize DataTables: Kompetensi Dasar
    if ($("#" + _table_rchb_kompetensi_dasar)[0]) {
      var table_rchb_kompetensi_dasar = $("#" + _table_rchb_kompetensi_dasar).DataTable({
        processing: true,
        serverSide: true,
        ajax: {
          url: "<?php echo base_url('raport_pengembangandiri/ajax_get_all/') ?>",
          type: "get"
        },
        columns: [{
            data: "kelas_concated"
          }, {
            data: null,
            render: function(data, type, row, meta) {
              return meta.row + meta.settings._iDisplayStart + 1;
            }
          },
          {
            data: "nisn"
          },
          {
            data: "nomor_induk_lokal"
          },
          {
            data: "nama_lengkap"
          },
          {
            data: null,
            render: function(data, type, row, meta) {
              var status = sample(['Sudah Input', 'Belum Input']);
              var statusClass = (status === 'Sudah Input') ? 'text-green' : 'text-red';
              return `<span class="${statusClass}">${status}</span>`;
            }
          },
          {
            data: null,
            className: "center",
            render: function(data, type, row, meta) {
              var buttons = '<div class="action">';
              buttons += '<a href="javascript:;" class="btn btn-sm btn-light btn-table-action action-input" data-toggle="modal" data-target="#' + _modal_rchb_kompetensi_dasar + '"><i class="zmdi zmdi-edit"></i> Input</a>';
              buttons += '</div>';
              return buttons;
            },
          }
        ],
        autoWidth: !1,
        responsive: {
          details: {
            renderer: function(api, rowIdx, columns) {
              var hideColumn = [9];
              var data = $.map(columns, function(col, i) {
                return ($.inArray(col.columnIndex, hideColumn)) ?
                  '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                  '<td class="dt-details-td">' + col.title + ':' + '</td> ' +
                  '<td class="dt-details-td">' + col.data + '</td>' +
                  '</tr>' :
                  '';
              }).join('');

              return data ? $('<table/>').append(data) : false;
            },
            type: "inline",
            target: 'tr',
          }
        },
        columnDefs: [{
          "targets": [0],
          "visible": false
        }, {
          className: 'desktop',
          targets: [0, 1, 2, 3, 4, 5, 6]
        }, {
          className: 'tablet',
          targets: [0, 1, 2, 3]
        }, {
          className: 'mobile',
          targets: [0, 3]
        }, {
          responsivePriority: 2,
          targets: -1
        }],
        pageLength: 100,
        language: {
          searchPlaceholder: "Search...",
          sProcessing: '<div style="text-align: center;"><div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div></div>'
        },
        sDom: '<"dataTables_ct"><"dataTables__top"fb>rt<"dataTables__bottom"ip><"clear">',
        buttons: [{
          extend: "excelHtml5",
          title: "Export Result"
        }, {
          extend: "print",
          title: "Export Result"
        }],
        initComplete: function(a, b) {
          $(this).closest(".dataTables_wrapper").find(".dataTables__top").prepend(
            '<div class="dataTables_buttons hidden-sm-down actions">' +
            '<span class="actions__item zmdi zmdi-refresh" data-table-action="reload" title="Reload" />' +
            '</div>'
          );
        },
      });

      $(".dataTables_filter input[type=search]").focus(function() {
        $(this).closest(".dataTables_filter").addClass("dataTables_filter--toggled")
      });

      $(".dataTables_filter input[type=search]").blur(function() {
        $(this).closest(".dataTables_filter").removeClass("dataTables_filter--toggled")
      });

      $("body").on("click", "[data-table-action]", function(a) {
        a.preventDefault();
        var b = $(this).data("table-action");
        if ("reload" === b) {
          $("#" + _table_rchb_kompetensi_dasar).DataTable().ajax.reload(null, false);
        };
      });
    };

    // Initialize DataTables: Ibadah Santri
    if ($("#" + _table_rchb_ibadah_santri)[0]) {
      var table_rchb_ibadah_santri = $("#" + _table_rchb_ibadah_santri).DataTable({
        processing: true,
        serverSide: true,
        ajax: {
          url: "<?php echo base_url('raport_pengembangandiri/ajax_get_all/') ?>",
          type: "get"
        },
        columns: [{
            data: "kelas_concated"
          }, {
            data: null,
            render: function(data, type, row, meta) {
              return meta.row + meta.settings._iDisplayStart + 1;
            }
          },
          {
            data: "nisn"
          },
          {
            data: "nomor_induk_lokal"
          },
          {
            data: "nama_lengkap"
          },
          {
            data: null,
            render: function(data, type, row, meta) {
              var status = sample(['Sudah Input', 'Belum Input']);
              var statusClass = (status === 'Sudah Input') ? 'text-green' : 'text-red';
              return `<span class="${statusClass}">${status}</span>`;
            }
          },
          {
            data: null,
            className: "center",
            render: function(data, type, row, meta) {
              var buttons = '<div class="action">';
              buttons += '<a href="javascript:;" class="btn btn-sm btn-light btn-table-action action-input" data-toggle="modal" data-target="#' + _modal_rchb_ibadah_santri + '"><i class="zmdi zmdi-edit"></i> Input</a>';
              buttons += '</div>';
              return buttons;
            },
          }
        ],
        autoWidth: !1,
        responsive: {
          details: {
            renderer: function(api, rowIdx, columns) {
              var hideColumn = [9];
              var data = $.map(columns, function(col, i) {
                return ($.inArray(col.columnIndex, hideColumn)) ?
                  '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                  '<td class="dt-details-td">' + col.title + ':' + '</td> ' +
                  '<td class="dt-details-td">' + col.data + '</td>' +
                  '</tr>' :
                  '';
              }).join('');

              return data ? $('<table/>').append(data) : false;
            },
            type: "inline",
            target: 'tr',
          }
        },
        columnDefs: [{
          "targets": [0],
          "visible": false
        }, {
          className: 'desktop',
          targets: [0, 1, 2, 3, 4, 5, 6]
        }, {
          className: 'tablet',
          targets: [0, 1, 2, 3]
        }, {
          className: 'mobile',
          targets: [0, 3]
        }, {
          responsivePriority: 2,
          targets: -1
        }],
        pageLength: 100,
        language: {
          searchPlaceholder: "Search...",
          sProcessing: '<div style="text-align: center;"><div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div></div>'
        },
        sDom: '<"dataTables_ct"><"dataTables__top"fb>rt<"dataTables__bottom"ip><"clear">',
        buttons: [{
          extend: "excelHtml5",
          title: "Export Result"
        }, {
          extend: "print",
          title: "Export Result"
        }],
        initComplete: function(a, b) {
          $(this).closest(".dataTables_wrapper").find(".dataTables__top").prepend(
            '<div class="dataTables_buttons hidden-sm-down actions">' +
            '<span class="actions__item zmdi zmdi-refresh" data-table-action="reload" title="Reload" />' +
            '</div>'
          );
        },
      });

      $(".dataTables_filter input[type=search]").focus(function() {
        $(this).closest(".dataTables_filter").addClass("dataTables_filter--toggled")
      });

      $(".dataTables_filter input[type=search]").blur(function() {
        $(this).closest(".dataTables_filter").removeClass("dataTables_filter--toggled")
      });

      $("body").on("click", "[data-table-action]", function(a) {
        a.preventDefault();
        var b = $(this).data("table-action");
        if ("reload" === b) {
          $("#" + _table_rchb_ibadah_santri).DataTable().ajax.reload(null, false);
        };
      });
    };

    // Handle Submit Filter
    $(document).on("change", ".raport_pengembangandiri-filter-kelas", function() {
      var filter_kelas = $("#" + _section + " .raport_pengembangandiri-filter-kelas").val();
      filter_kelas = (filter_kelas == null || filter_kelas == "null") ? "--empty--" : filter_kelas;
      table_rchb_kompetensi_dasar.column(0).search(filter_kelas).draw();
      table_rchb_ibadah_santri.column(0).search(filter_kelas).draw();
    });

    // Handle Input: Kompetensi Dasar
    $("#" + _table_rchb_kompetensi_dasar).on("click", "a.action-input", function(e) {
      e.preventDefault();
      reset_form_rchb_kompetensi_dasar();

      try {
        var temp = table_rchb_kompetensi_dasar.row($(this).closest('tr')).data();

        $(`#${_form_rchb_kompetensi_dasar} .nisn`).html(temp.nisn);
        $(`#${_form_rchb_kompetensi_dasar} .nama_lengkap`).html(temp.nama_lengkap);

        // Fetch value
        // $.ajax({
        //   url: "<?= base_url('raport_pengembangandiri/ajax_get_kompetensi_dasar_value') ?>",
        //   type: "get",
        //   data: {
        //     santri_id: "xxxx"
        //   },
        //   dataType: "json",
        //   success: function(response) {
        //     if (response.items.length > 0) {
        //       response.items.map((value, index) => {
        //         if ($("[name='keterampilan[" + value.mapel_id + "][angka]']").length > 0) {
        //           $("[name='pengetahuan[" + value.mapel_id + "][angka]']").val(value.pengetahuan_angka);
        //         }
        //       });
        //     };
        //   }
        // });
      } catch (error) {
        handleTransErrorMsg();
      };
    });

    // Handle Input: Ibadah Santri
    $("#" + _table_rchb_ibadah_santri).on("click", "a.action-input", function(e) {
      e.preventDefault();
      reset_form_rchb_ibadah_santri();

      try {
        var temp = table_rchb_ibadah_santri.row($(this).closest('tr')).data();

        $(`#${_form_rchb_ibadah_santri} .nisn`).html(temp.nisn);
        $(`#${_form_rchb_ibadah_santri} .nama_lengkap`).html(temp.nama_lengkap);

        // Fetch value
        // $.ajax({
        //   url: "<?= base_url('raport_pengembangandiri/ajax_get_ibadah_santri_value') ?>",
        //   type: "get",
        //   data: {
        //     santri_id: "xxxx"
        //   },
        //   dataType: "json",
        //   success: function(response) {
        //     if (response.items.length > 0) {
        //       response.items.map((value, index) => {
        //         if ($("[name='keterampilan[" + value.mapel_id + "][angka]']").length > 0) {
        //           $("[name='pengetahuan[" + value.mapel_id + "][angka]']").val(value.pengetahuan_angka);
        //         }
        //       });
        //     };
        //   }
        // });
      } catch (error) {
        handleTransErrorMsg();
      };
    });

    // Handle form reset: Kompetensi Dasar
    reset_form_rchb_kompetensi_dasar = () => {
      $(`#${_form_rchb_kompetensi_dasar}`).trigger("reset");
      $(`#${_form_rchb_kompetensi_dasar} .nisn`).html("-");
      $(`#${_form_rchb_kompetensi_dasar} .nama_lengkap`).html("-");
    };

    // Handle form reset: Ibadah Santri
    reset_form_rchb_ibadah_santri = () => {
      $(`#${_form_rchb_ibadah_santri}`).trigger("reset");
      $(`#${_form_rchb_ibadah_santri} .nisn`).html("-");
      $(`#${_form_rchb_ibadah_santri} .nama_lengkap`).html("-");
    };

  });

  function sample(array) {
    return array[Math.floor(Math.random() * array.length)];
  };
</script>