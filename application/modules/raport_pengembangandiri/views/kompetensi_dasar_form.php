<div class="modal fade" id="modal-raport_pengembangandiri-kompetensi_dasar" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title pull-left">Kompetensi Dasar</h5>
      </div>
      <div class="spinner">
        <div class="lds-hourglass"></div>
      </div>
      <div class="modal-body">
        <form id="form-raport_pengembangandiri-kompetensi_dasar" autocomplete="off">
          <!-- CSRF -->
          <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />

          <div class="row">
            <div class="col-xs-12 col-md-2">
              <b>Nomor Induk / NISN :</b>
            </div>
            <div class="col-xs-12 col-md-10">
              <span class="nisn">-</span>
            </div>
            <div class="col-xs-12 col-md-2">
              <b>Nama Peserta Didik :</b>
            </div>
            <div class="col-xs-12 col-md-10">
              <span class="nama_lengkap">-</span>
            </div>
          </div>
          <div class="border-bottom border-danger mb-3">&nbsp;</div>

          <div class="table-responsive">
            <table class="table table-bordered table-hover table-sm">
              <thead class="thead-default text-center">
                <tr>
                  <th rowspan="2" width="50">NO</th>
                  <th rowspan="2" width="250">KOMPETENSI DASAR</th>
                  <th colspan="3">CAPAIAN</th>
                  <th rowspan="2">DESKRIPSI</th>
                </tr>
                <tr>
                  <th width="80">BASELINE</th>
                  <th width="80">TARGET</th>
                  <th width="80">HASIL</th>
                </tr>
              </thead>
              <tbody>
                <?php if (count($kompetensi_dasar) > 0) : ?>
                  <?php $no = 1; ?>
                  <?php foreach ($kompetensi_dasar as $index => $item) : ?>
                    <tr>
                      <td valign="top" align="center"><?= $no++ ?></td>
                      <td valign="top"><?= $item['nama'] ?></td>
                      <td valign="top" align="center"><?= $item['baseline'] ?></td>
                      <td valign="top" align="center"><?= $item['target'] ?></td>
                      <td valign="top">
                        <input type="number" name="kompetensi_dasar[<?= $item['id'] ?>][hasil]" min="0" max="100" class="form-control" style="text-align: center; height: 26px;" placeholder="0" />
                      </td>
                      <td valign="top">
                        <textarea name="kompetensi_dasar[<?= $item['id'] ?>][deskripsi]" class="form-control textarea-autosize" rows="1"></textarea>
                      </td>
                    </tr>
                  <?php endforeach ?>
                <?php else : ?>
                  <tr>
                    <td colspan="6">No data available in table</td>
                  </tr>
                <?php endif ?>
              </tbody>
            </table>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success btn--icon-text action-save">
          <i class="zmdi zmdi-save"></i> Save
        </button>
        <button type="button" class="btn btn-light btn--icon-text action-cancel" data-dismiss="modal">
          Cancel
        </button>
      </div>
    </div>
  </div>
</div>