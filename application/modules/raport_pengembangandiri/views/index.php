<section id="raport_pengembangandiri">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title"><?php echo (isset($card_title)) ? $card_title : '' ?></h4>
            <h6 class="card-subtitle"><?php echo (isset($card_subTitle)) ? $card_subTitle : '' ?></h6>

            <?php if (!in_array($this->session->userdata('user')['role'], ['Pembina', 'Orang Tua'])) : ?>
                <div class="table-action row">
                    <div class="buttons col">
                        <div class="input-group mb-0">
                            <div class="input-group-prepend">
                                <label class="input-group-text" style="height: 34.13px; background: #f2f2f2;">Kelas</label>
                            </div>
                            <select class="custom-select raport_pengembangandiri-filter-kelas" style="height: 34.13px; max-width: 230px;">
                                <?= $list_kelas_combo ?>
                            </select>
                        </div>
                    </div>
                </div>
            <?php endif; ?>

            <div class="tab-container mt-2">
                <ul class="nav nav-tabs nav-responsive" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#nav-1" role="tab">Kompetensi Dasar</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#nav-2" role="tab">Ibadah Santri</a>
                    </li>
                </ul>
                <div class="tab-content clear-tab-content p-0">
                    <div class="tab-pane active fade show" id="nav-1" role="tabpanel">
                        <!-- Kompetensi Dasar -->
                        <?php include_once(APPPATH . 'modules/raport_pengembangandiri/views/kompetensi_dasar.php') ?>
                    </div>
                    <div class="tab-pane fade show" id="nav-2" role="tabpanel">
                        <!-- Ibadah Santri -->
                        <?php include_once(APPPATH . 'modules/raport_pengembangandiri/views/ibadah_santri.php') ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>