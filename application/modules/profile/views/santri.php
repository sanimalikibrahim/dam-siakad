<style type="text/css">
    .footer-wrap {
        background-color: #f2f2f2;
        padding: 15px;
        box-shadow: 0 1px 2px rgba(0, 0, 0, .075);
        margin-top: 2rem;
    }

    .footer-wrap .btn-submit {
        padding-left: 30px;
        padding-right: 30px;
    }

    input[type="text"]:disabled {
        background-color: #fff;
    }
</style>

<div class="card">
    <div class="card-body">
        <form id="form-santri" autocomplete="off">
            <!-- CSRF -->
            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />

            <!-- Temporary -->
            <input type="hidden" name="id" value="<?= $data_santri->id ?>" readonly />

            <div class="tab-container">
                <ul class="nav nav-tabs nav-responsive" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link santri-nav-general active" data-toggle="tab" href="#santri-general" role="tab">General</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link santri-nav-ayah" data-toggle="tab" href="#santri-ayah" role="tab">Ayah</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link santri-nav-ibu" data-toggle="tab" href="#santri-ibu" role="tab">Ibu</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link santri-nav-wali" data-toggle="tab" href="#santri-wali" role="tab">Wali</a>
                    </li>
                </ul>
                <div class="tab-content clear-tab-content">
                    <!-- General -->
                    <div class="tab-pane active fade show" id="santri-general" role="tabpanel">
                        <div class="row">
                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label required>NISN</label>
                                    <input type="number" name="nisn" class="form-control mask-number santri-nisn" placeholder="Nomor Induk Siswa Nasional" value="<?= $data_santri->nisn ?>" required readonly />
                                    <i class="form-group__bar"></i>
                                </div>
                            </div>
                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label>Nomor Induk Lokal</label>
                                    <input type="number" name="nomor_induk_lokal" class="form-control mask-number santri-nomor_induk_lokal" placeholder="Nomor Induk Lokal" value="<?= $data_santri->nomor_induk_lokal ?>" />
                                    <i class="form-group__bar"></i>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label>Nomor Absen</label>
                                    <input type="number" name="nomor_absen" class="form-control mask-number santri-nomor_absen" placeholder="Nomor Absen" value="<?= $data_santri->nomor_absen ?>" />
                                    <i class="form-group__bar"></i>
                                </div>
                            </div>
                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label required>Nama Lengkap</label>
                                    <input type="text" name="nama_lengkap" class="form-control santri-nama_lengkap" placeholder="Nama Lengkap" required value="<?= $data_santri->nama_lengkap ?>" />
                                    <i class="form-group__bar"></i>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label required>Jenis Kelamin</label>
                                    <div class="select">
                                        <select name="jenis_kelamin" class="form-control santri-jenis_kelamin" data-placeholder="Select &#8595;">
                                            <option disabled <?= (empty($data_santri->jenis_kelamin) || is_null($data_santri->jenis_kelamin)) ? 'selected' : '' ?>>Select &#8595;</option>
                                            <option value="Laki-laki" <?= ($data_santri->jenis_kelamin === 'Laki-laki') ? 'selected' : '' ?>>Laki-laki</option>
                                            <option value="Perempuan" <?= ($data_santri->jenis_kelamin === 'Perempuan') ? 'selected' : '' ?>>Perempuan</option>
                                        </select>
                                        <i class="form-group__bar"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label>Kelas</label>
                                    <div class="buttons">
                                        <div class="input-group mb-0">
                                            <div class="select">
                                                <select name="kelas" class="form-control no-padding-l santri-kelas" data-placeholder="Select &#8595;">
                                                    <?= $list_kelas ?>
                                                </select>
                                                <i class="form-group__bar"></i>
                                            </div>
                                            <div class="select" style="flex: 1;">
                                                <select name="sub_kelas" class="form-control no-padding-l santri-sub_kelas" data-placeholder="Select &#8595;">
                                                    <?= $list_sub_kelas ?>
                                                </select>
                                                <i class="form-group__bar"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label>Asrama</label>
                                    <input type="text" name="asrama" class="form-control santri-asrama" placeholder="Nomor Asrama" value="<?= $data_santri->asrama ?>" />
                                    <i class="form-group__bar"></i>
                                </div>
                            </div>
                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label required>Tempat, Tanggal Lahir</label>
                                    <div class="buttons">
                                        <div class="input-group mb-0">
                                            <input type="text" name="tempat_lahir" class="form-control no-padding-l santri-tempat_lahir" placeholder="Tempat Lahir" required value="<?= $data_santri->tempat_lahir ?>" />
                                            <i class="form-group__bar"></i>
                                            <input type="text" name="tanggal_lahir" class="form-control no-padding-l flatpickr-date mask-date santri-tanggal_lahir" placeholder="YYYY-MM-DD" required value="<?= $data_santri->tanggal_lahir ?>" />
                                            <i class="form-group__bar"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label>Wali</label>
                                    <input type="text" name="wali" class="form-control santri-wali" placeholder="Wali" value="<?= $data_santri->wali ?>" />
                                    <i class="form-group__bar"></i>
                                </div>
                            </div>
                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label>Pembina</label>
                                    <div class="select">
                                        <select name="pembina" class="form-control santri-pembina" data-placeholder="Select &#8595;">
                                            <?= $list_pembina ?>
                                        </select>
                                        <i class="form-group__bar"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label>Tanggal Masuk</label>
                                    <input type="text" name="tanggal_masuk" class="form-control flatpickr-date santri-tanggal_masuk" placeholder="Tanggal Masuk" value="<?= $data_santri->tanggal_masuk ?>" />
                                    <i class="form-group__bar"></i>
                                </div>
                            </div>
                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label>Golongan Darah</label>
                                    <div class="select">
                                        <select name="golongan_darah" class="form-control santri-golongan_darah" data-placeholder="Select &#8595;">
                                            <option disabled>Select &#8595;</option>
                                            <option value="A" <?= ($data_santri->golongan_darah === 'A') ? 'selected' : '' ?>>A</option>
                                            <option value="B" <?= ($data_santri->golongan_darah === 'B') ? 'selected' : '' ?>>B</option>
                                            <option value="AB" <?= ($data_santri->golongan_darah === 'AB') ? 'selected' : '' ?>>AB</option>
                                            <option value="O" <?= ($data_santri->golongan_darah === 'O') ? 'selected' : '' ?>>O</option>
                                        </select>
                                        <i class="form-group__bar"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label>Berat Badan</label>
                                    <input type="number" name="berat_badan" class="form-control mask-number santri-berat_badan" placeholder="Berat Badan" value="<?= $data_santri->berat_badan ?>" />
                                    <i class="form-group__bar"></i>
                                </div>
                            </div>
                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label>Tinggi Badan</label>
                                    <input type="number" name="tinggi_badan" class="form-control mask-number santri-tinggi_badan" placeholder="Tinggi Badan" value="<?= $data_santri->tinggi_badan ?>" />
                                    <i class="form-group__bar"></i>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-12">
                                <div class="form-group" style="margin-bottom: 3rem;">
                                    <label>Alamat</label>
                                    <textarea name="alamat" class="form-control textarea-autosize text-counter santri-alamat" rows="1" data-max-length="250" placeholder="Alamat Lengkap" style="overflow: hidden; overflow-wrap: break-word; height: 31px;"><?= $data_santri->alamat ?></textarea>
                                    <i class="form-group__bar"></i>
                                </div>
                            </div>
                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label>Provinsi, Kabupaten / Kota</label>
                                    <div class="buttons">
                                        <div class="input-group mb-0">
                                            <div class="select" style="flex: 1;">
                                                <select name="provinsi" class="form-control select2 santri-provinsi" data-placeholder="Select &#8595;">
                                                    <?= $list_provinces ?>
                                                </select>
                                                <i class="form-group__bar"></i>
                                            </div>
                                            &nbsp;&nbsp;
                                            <div class="select" style="flex: 1;">
                                                <select name="kabupaten_kota" class="form-control select2 santri-kabupaten_kota" data-placeholder="Select &#8595;">
                                                    <?= $list_regencies ?>
                                                </select>
                                                <i class="form-group__bar"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Ayah -->
                    <div class="tab-pane fade show" id="santri-ayah" role="tabpanel">
                        <div class="row">
                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label>Nama Lengkap</label>
                                    <input type="text" name="ayah_nama" class="form-control santri-ayah_nama" placeholder="Nama Lengkap" value="<?= $data_santri->ayah_nama ?>" />
                                    <i class="form-group__bar"></i>
                                </div>
                            </div>
                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label>Nomor HP</label>
                                    <input type="number" name="ayah_hp" class="form-control mask-number santri-ayah_hp" placeholder="Nomor Handphone" value="<?= $data_santri->ayah_hp ?>" />
                                    <i class="form-group__bar"></i>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-12">
                                <div class="form-group" style="margin-bottom: 3rem;">
                                    <label>Alamat</label>
                                    <textarea name="ayah_alamat" class="form-control textarea-autosize text-counter santri-ayah_alamat" rows="1" data-max-length="250" placeholder="Alamat Lengkap" style="overflow: hidden; overflow-wrap: break-word; height: 31px;"><?= $data_santri->ayah_alamat ?></textarea>
                                    <i class="form-group__bar"></i>
                                </div>
                            </div>
                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label>Provinsi, Kabupaten / Kota</label>
                                    <div class="buttons">
                                        <div class="input-group mb-0">
                                            <div class="select" style="flex: 1;">
                                                <select name="ayah_provinsi" class="form-control select2 santri-ayah_provinsi" data-placeholder="Select &#8595;">
                                                    <?= $list_provinces_ayah ?>
                                                </select>
                                                <i class="form-group__bar"></i>
                                            </div>
                                            &nbsp;&nbsp;
                                            <div class="select" style="flex: 1;">
                                                <select name="ayah_kabupaten_kota" class="form-control select2 santri-ayah_kabupaten_kota" data-placeholder="Select &#8595;">
                                                    <?= $list_regencies_ayah ?>
                                                </select>
                                                <i class="form-group__bar"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Ibu -->
                    <div class="tab-pane fade show" id="santri-ibu" role="tabpanel">
                        <div class="row">
                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label>Nama Lengkap</label>
                                    <input type="text" name="ibu_nama" class="form-control santri-ibu_nama" placeholder="Nama Lengkap" value="<?= $data_santri->ibu_nama ?>" />
                                    <i class="form-group__bar"></i>
                                </div>
                            </div>
                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label>Nomor HP</label>
                                    <input type="number" name="ibu_hp" class="form-control mask-number santri-ibu_hp" placeholder="Nomor Handphone" value="<?= $data_santri->ibu_hp ?>" />
                                    <i class="form-group__bar"></i>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-12">
                                <div class="form-group" style="margin-bottom: 3rem;">
                                    <label>Alamat</label>
                                    <textarea name="ibu_alamat" class="form-control textarea-autosize text-counter santri-ibu_alamat" rows="1" data-max-length="250" placeholder="Alamat Lengkap" style="overflow: hidden; overflow-wrap: break-word; height: 31px;"><?= $data_santri->ibu_alamat ?></textarea>
                                    <i class="form-group__bar"></i>
                                </div>
                            </div>
                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label>Provinsi, Kabupaten / Kota</label>
                                    <div class="buttons">
                                        <div class="input-group mb-0">
                                            <div class="select" style="flex: 1;">
                                                <select name="ibu_provinsi" class="form-control select2 santri-ibu_provinsi" data-placeholder="Select &#8595;">
                                                    <?= $list_provinces_ibu ?>
                                                </select>
                                                <i class="form-group__bar"></i>
                                            </div>
                                            &nbsp;&nbsp;
                                            <div class="select" style="flex: 1;">
                                                <select name="ibu_kabupaten_kota" class="form-control select2 santri-ibu_kabupaten_kota" data-placeholder="Select &#8595;">
                                                    <?= $list_regencies_ibu ?>
                                                </select>
                                                <i class="form-group__bar"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Wali -->
                    <div class="tab-pane fade show" id="santri-wali" role="tabpanel">
                        <div class="row">
                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label>Nama Lengkap</label>
                                    <input type="text" name="wali_nama" class="form-control santri-wali_nama" placeholder="Nama Lengkap" value="<?= $data_santri->wali_nama ?>" />
                                    <i class="form-group__bar"></i>
                                </div>
                            </div>
                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label>Nomor HP</label>
                                    <input type="number" name="wali_hp" class="form-control mask-number santri-wali_hp" placeholder="Nomor Handphone" value="<?= $data_santri->wali_hp ?>" />
                                    <i class="form-group__bar"></i>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label>Hubungan</label>
                                    <input type="text" name="wali_hubungan" class="form-control santri-wali_hubungan" placeholder="Hubungan" value="<?= $data_santri->wali_hubungan ?>" />
                                    <i class="form-group__bar"></i>
                                </div>
                            </div>
                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label>Provinsi, Kabupaten / Kota</label>
                                    <div class="buttons">
                                        <div class="input-group mb-0">
                                            <div class="select" style="flex: 1;">
                                                <select name="wali_provinsi" class="form-control select2 santri-wali_provinsi" data-placeholder="Select &#8595;">
                                                    <?= $list_provinces_wali ?>
                                                </select>
                                                <i class="form-group__bar"></i>
                                            </div>
                                            &nbsp;&nbsp;
                                            <div class="select" style="flex: 1;">
                                                <select name="wali_kabupaten_kota" class="form-control select2 santri-wali_kabupaten_kota" data-placeholder="Select &#8595;">
                                                    <?= $list_regencies_wali ?>
                                                </select>
                                                <i class="form-group__bar"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <small class="form-text text-muted">
                Fields with red stars (<label required></label>) are required.
            </small>

            <div class="footer-wrap">
                <button type="submit" class="btn btn-primary btn-submit spinner-action-button <?= ($app->is_mobile) ? 'btn-block' : '' ?>"><i class="zmdi zmdi-save"></i> Save</button>
            </div>

        </form>
    </div>
</div>