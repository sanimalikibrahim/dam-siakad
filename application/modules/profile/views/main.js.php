<script type="text/javascript">
  $(document).ready(function() {

    var _form = "form-santri";

    // Handle data submit
    $("#" + _form + " .btn-submit").on("click", function(e) {
      e.preventDefault();

      swal({
        title: "Konfirmasi",
        text: "Ananda yakin semua data yang akan disimpan sudah benar?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#39bbb0',
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false
      }).then((result) => {
        if (result.value) {
          var form = $("#" + _form)[0];
          var data = new FormData(form);

          $.ajax({
            type: "post",
            url: "<?php echo base_url('profile/ajax_save/') ?>",
            data: data,
            dataType: "json",
            enctype: "multipart/form-data",
            processData: false,
            contentType: false,
            cache: false,
            success: function(response) {
              if (response.status === true) {
                // notify(response.data, "success");
                swal({
                  title: "Success",
                  text: response.data,
                  type: "success",
                  showCancelButton: false,
                  confirmButtonColor: '#39bbb0',
                  confirmButtonText: "OK",
                  closeOnConfirm: false
                }).then((result) => {
                  window.location.href = "";
                });
              } else {
                notify(response.data, "danger");
              };
            }
          });
        };
      });
    });

  });
</script>