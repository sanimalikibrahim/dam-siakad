<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . 'controllers/AppBackend.php');

class Profile extends AppBackend
{
  function __construct()
  {
    parent::__construct();
    $this->load->model([
      'UserModel',
      'KelasModel',
      'SubkelasModel',
      'ProvincesModel',
      'RegenciesModel',
      'SantriModel',
      'PsbModel'
    ]);
  }

  public function index()
  {
    $role = $this->session->userdata('user')['role'];

    if (!is_null($role)) {
      switch ($role) {
        case 'Santri':
          $this->santri();
          break;
        case 'Calon Santri':
          $this->calon_santri();
          break;
        default:
          show_404();
          break;
      };
    } else {
      show_404();
    };
  }

  public function calon_santri()
  {
    $userId = $this->session->userdata('user')['id'];
    $role = $this->session->userdata('user')['role'];
    $santri = $this->PsbModel->getDetail(['id' => $userId]);

    if (!is_null($santri) && $role === 'Calon Santri') {
      redirect(base_url('profile/calon-santri'));
    } else {
      show_404();
    };
  }

  public function santri()
  {
    $userId = $this->session->userdata('user')['id'];
    $role = $this->session->userdata('user')['role'];
    $santri = $this->SantriModel->getDetail(['id' => $userId]);

    if (!is_null($santri) && $role === 'Santri') {
      $data = array(
        'app' => $this->app(),
        'main_js' => $this->load_main_js('profile'),
        'page_title' => 'Profile',
        'list_kelas' => $this->init_list($this->KelasModel->getAll(), 'nama', 'nama', $santri->kelas),
        'list_sub_kelas' => $this->init_list($this->SubkelasModel->getAll(), 'nama', 'nama', $santri->sub_kelas),
        'list_provinces' => $this->init_list($this->ProvincesModel->getAll(), 'id', 'name', $santri->provinsi),
        'list_regencies' => $this->init_list($this->RegenciesModel->getAll(), 'id', 'name', $santri->kabupaten_kota),
        'list_provinces_ayah' => $this->init_list($this->ProvincesModel->getAll(), 'id', 'name', $santri->ayah_provinsi),
        'list_regencies_ayah' => $this->init_list($this->RegenciesModel->getAll(), 'id', 'name', $santri->ayah_kabupaten_kota),
        'list_provinces_ibu' => $this->init_list($this->ProvincesModel->getAll(), 'id', 'name', $santri->ibu_provinsi),
        'list_regencies_ibu' => $this->init_list($this->RegenciesModel->getAll(), 'id', 'name', $santri->ibu_kabupaten_kota),
        'list_provinces_wali' => $this->init_list($this->ProvincesModel->getAll(), 'id', 'name', $santri->wali_provinsi),
        'list_regencies_wali' => $this->init_list($this->RegenciesModel->getAll(), 'id', 'name', $santri->wali_kabupaten_kota),
        'list_pembina' => $this->init_list($this->UserModel->getAll(['role' => 'Pembina']), 'id', 'nama_lengkap', $santri->pembina),
        'data_santri' => $santri
      );
      $this->template->set('title', $data['page_title'] . ' | ' . $data['app']->app_name, TRUE);
      $this->template->load_view('santri', $data, TRUE);
      $this->template->render();
    } else {
      show_404();
    };
  }

  public function ajax_save()
  {
    $this->handle_ajax_request();
    $this->id = $this->input->post('id');

    if (!empty($this->id) && !is_null($this->id)) {
      $this->form_validation->set_rules($this->SantriModel->rules($this->id));

      if ($this->form_validation->run() === true) {
        if (is_null($this->id)) {
          echo json_encode($this->SantriModel->insert());
        } else {
          echo json_encode($this->SantriModel->update($this->id));
        };
      } else {
        $errors = validation_errors('<div>- ', '</div>');
        echo json_encode(array('status' => false, 'data' => $errors));
      };
    } else {
      echo json_encode(array('status' => false, 'data' => 'Terjadi kesalahan ketika menyimpan data, coba lagi nanti.'));
    };
  }
}
