<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . 'controllers/AppBackend.php');

class Ruangan extends AppBackend
{
  function __construct()
  {
    parent::__construct();
    $this->load->model([
      'AppModel',
      'RuanganModel',
      'RuanganTipeModel'
    ]);
    $this->load->library('form_validation');
  }

  public function index()
  {
    $data = array(
      'app' => $this->app(),
      'main_js' => $this->load_main_js('ruangan'),
      'ruangan_tipe_list' => $this->init_list($this->RuanganTipeModel->getAll(), 'nama', 'nama'),
      'card_title' => 'Ruangan'
    );
    $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
    $this->template->load_view('index', $data, TRUE);
    $this->template->render();
  }

  public function ajax_get_all()
  {
    $this->handle_ajax_request();
    $dtAjax_config = array(
      'table_name' => 'ruangan',
      'order_column' => 4,
      'order_column_dir' => 'asc'
    );
    $response = $this->AppModel->getData_dtAjax($dtAjax_config);
    echo json_encode($response);
  }

  public function ajax_save($id = null)
  {
    $this->handle_ajax_request();
    $this->form_validation->set_rules($this->RuanganModel->rules());

    if ($this->form_validation->run() === true) {
      if (is_null($id)) {
        echo json_encode($this->RuanganModel->insert());
      } else {
        echo json_encode($this->RuanganModel->update($id));
      };
    } else {
      $errors = validation_errors('<div>- ', '</div>');
      echo json_encode(array('status' => false, 'data' => $errors));
    };
  }

  public function ajax_delete($id)
  {
    $this->handle_ajax_request();
    echo json_encode($this->RuanganModel->delete($id));
  }

  // Type
  public function type()
  {
    $data = array(
      'app' => $this->app(),
      'main_js' => $this->load_main_js('ruangan/views/type/main.js.php', true),
      'ruangan_tipe_list' => $this->init_list($this->RuanganTipeModel->getAll(), 'nama', 'nama'),
      'card_title' => 'Ruangan › Tipe'
    );
    $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
    $this->template->load_view('type/index', $data, TRUE);
    $this->template->render();
  }

  public function ajax_get_all_type()
  {
    $this->handle_ajax_request();
    $dtAjax_config = array(
      'table_name' => 'ruangan_tipe',
      'order_column' => 4,
      'order_column_dir' => 'desc'
    );
    $response = $this->AppModel->getData_dtAjax($dtAjax_config);
    echo json_encode($response);
  }

  public function ajax_save_type($id = null)
  {
    $this->handle_ajax_request();
    $this->form_validation->set_rules($this->RuanganTipeModel->rules());

    if ($this->form_validation->run() === true) {
      if (is_null($id)) {
        echo json_encode($this->RuanganTipeModel->insert());
      } else {
        echo json_encode($this->RuanganTipeModel->update($id));
      };
    } else {
      $errors = validation_errors('<div>- ', '</div>');
      echo json_encode(array('status' => false, 'data' => $errors));
    };
  }

  public function ajax_delete_type($id)
  {
    $this->handle_ajax_request();
    echo json_encode($this->RuanganTipeModel->delete($id));
  }
  // END ## Type
}
