<section id="ruangan">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title"><?php echo (isset($card_title)) ? $card_title : '' ?></h4>
            <h6 class="card-subtitle"><?php echo (isset($card_subTitle)) ? $card_subTitle : '' ?></h6>

            <div class="table-action">
                <div class="buttons">
                    <a href="<?= base_url('ruangan/type') ?>" class="btn btn--raised btn-secondary btn--icon-text">
                        <i class="zmdi zmdi-flip"></i> Tipe Ruangan
                    </a>
                    <button class="btn btn--raised btn-primary btn--icon-text ruangan-action-add" data-toggle="modal" data-target="#modal-form-ruangan">
                        <i class="zmdi zmdi-plus-circle"></i> Add New
                    </button>
                </div>
            </div>

            <?php include_once('form.php') ?>

            <div class="table-responsive">
                <table id="table-ruangan" class="table table-bordered">
                    <thead class="thead-default">
                        <tr>
                            <th width="100">No</th>
                            <th>Nama Ruangan</th>
                            <th>Kapasitas</th>
                            <th>Jenis</th>
                            <th>Tipe</th>
                            <th>Created</th>
                            <th width="170">Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</section>