<div class="modal fade" id="modal-form-ruangan" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title pull-left">Tipe Ruangan</h5>
      </div>
      <div class="spinner">
        <div class="lds-hourglass"></div>
      </div>
      <div class="modal-body">
        <form id="form-ruangan" autocomplete="off">
          <!-- CSRF -->
          <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />

          <div class="form-group">
            <label required>Nama</label>
            <input type="text" name="nama" class="form-control ruangan-nama" placeholder="Nama" required />
            <i class="form-group__bar"></i>
          </div>
          <div class="form-group">
            <label required>Use for Test?</label>
            <div class="select">
              <select name="use_for_test" class="form-control ruangan-use_for_test" data-placeholder="Select &#8595;" required>
                <option disabled selected>Select &#8595;</option>
                <option value="1">Yes</option>
                <option value="0">No</option>
              </select>
              <i class="form-group__bar"></i>
            </div>
          </div>
          <div class="form-group">
            <label required>Is Active?</label>
            <div class="select">
              <select name="is_active" class="form-control ruangan-is_active" data-placeholder="Select &#8595;" required>
                <option disabled selected>Select &#8595;</option>
                <option value="1">Yes</option>
                <option value="0">No</option>
              </select>
              <i class="form-group__bar"></i>
            </div>
          </div>

          <small class="form-text text-muted">
            Fields with red stars (<label required></label>) are required.
          </small>

        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success btn--icon-text ruangan-action-save">
          <i class="zmdi zmdi-save"></i> Save
        </button>
        <button type="button" class="btn btn-light btn--icon-text ruangan-action-cancel" data-dismiss="modal">
          Cancel
        </button>
      </div>
    </div>
  </div>
</div>