<div class="modal fade" id="modal-form-ruangan" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title pull-left">Ruangan</h5>
      </div>
      <div class="spinner">
        <div class="lds-hourglass"></div>
      </div>
      <div class="modal-body">
        <form id="form-ruangan" autocomplete="off">
          <!-- CSRF -->
          <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />

          <div class="form-group">
            <label required>Nama Ruangan</label>
            <input type="text" name="nama_ruangan" class="form-control ruangan-nama_ruangan" placeholder="Nama Ruangan" required />
            <i class="form-group__bar"></i>
          </div>
          <div class="form-group">
            <label required>Kapasitas</label>
            <input type="number" name="kapasitas" class="form-control mask-number ruangan-kapasitas" placeholder="Kapasitas" required />
            <i class="form-group__bar"></i>
          </div>
          <div class="form-group">
            <label required>Jenis</label>
            <div class="select">
              <select name="jenis" class="form-control ruangan-jenis" data-placeholder="Select &#8595;" required>
                <option disabled selected>Select &#8595;</option>
                <option value="Putra">Putra</option>
                <option value="Putri">Putri</option>
              </select>
              <i class="form-group__bar"></i>
            </div>
          </div>
          <div class="form-group">
            <label>Tipe</label>
            <div class="select">
              <select name="tipe" class="form-control ruangan-tipe" data-placeholder="Select &#8595;">
                <?= $ruangan_tipe_list ?>
              </select>
              <i class="form-group__bar"></i>
            </div>
          </div>

          <small class="form-text text-muted">
            Fields with red stars (<label required></label>) are required.
          </small>

        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success btn--icon-text ruangan-action-save">
          <i class="zmdi zmdi-save"></i> Save
        </button>
        <button type="button" class="btn btn-light btn--icon-text ruangan-action-cancel" data-dismiss="modal">
          Cancel
        </button>
      </div>
    </div>
  </div>
</div>