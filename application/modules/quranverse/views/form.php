<div class="modal fade" id="modal-form-quranverse" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title pull-left">Quran Surah</h5>
      </div>
      <div class="spinner">
        <div class="lds-hourglass"></div>
      </div>
      <div class="modal-body">
        <form id="form-quranverse" autocomplete="off">
          <!-- CSRF -->
          <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />

          <div class="form-group">
            <label required>Surah</label>
            <div class="select">
              <select name="surah_id" class="form-control select2 quranverse-surah_id" data-placeholder="Select &#8595;" required>
                <?= $list_quran_surah ?>
              </select>
              <i class="form-group__bar"></i>
            </div>
          </div>

          <div class="form-group">
            <label required>Verse No</label>
            <input type="number" name="verse_id" class="form-control mask-number quranverse-verse_id" maxlength="30" placeholder="Verse No" required />
            <i class="form-group__bar"></i>
          </div>

          <div class="form-group" style="margin-bottom: 3rem;">
            <label required>Verse</label>
            <textarea name="verse_text" class="form-control textarea-autosize text-counter quranverse-verse_text" rows="1" data-max-length="2000" placeholder="Verse" style="overflow: hidden; overflow-wrap: break-word; height: 31px;"></textarea>
            <i class="form-group__bar"></i>
          </div>

          <small class="form-text text-muted">
            Fields with red stars (<label required></label>) are required.
          </small>

        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success btn--icon-text quranverse-action-save">
          <i class="zmdi zmdi-save"></i> Save
        </button>
        <button type="button" class="btn btn-light btn--icon-text quranverse-action-cancel" data-dismiss="modal">
          Cancel
        </button>
      </div>
    </div>
  </div>
</div>