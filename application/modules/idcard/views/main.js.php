<script type="text/javascript">
  $(document).ready(function() {

    var idcard = $("#idcard-content");
    var getCanvas;

    initialize_idcard();

    // Handle convert idcard
    function initialize_idcard() {
      html2canvas(idcard, {
        onrendered: function(canvas) {
          getCanvas = canvas;
        }
      });
    };

    // Handle download idcard
    $("#btn-download-idcard").on("click", function() {
      var imgageData = getCanvas.toDataURL("image/png");
      var newData = imgageData.replace(/^data:image\/png/, "data:application/octet-stream");

      $("#btn-download-idcard").attr("download", "DA_Kartu_Peserta.png").attr("href", newData);
    });

  });
</script>