<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . 'controllers/AppBackend.php');

class Idcard extends AppBackend
{
  function __construct()
  {
    parent::__construct();
    $this->load->model([
      'PsbModel',
      'MappingRuanganModel'
    ]);
  }

  private function _intializeStyle()
  {
    $app = $this->app();
    $idcard = array(
      'idcard_psb_background' => (isset($app->idcard_psb_background)) ? $app->idcard_psb_background : '',
      'idcard_psb_nomor_top' => (isset($app->idcard_psb_nomor_top)) ? $app->idcard_psb_nomor_top . 'px' : '',
      'idcard_psb_nomor_left' => (isset($app->idcard_psb_nomor_left)) ? $app->idcard_psb_nomor_left . 'px' : '',
      'idcard_psb_nomor_font_size' => (isset($app->idcard_psb_nomor_font_size)) ? $app->idcard_psb_nomor_font_size . 'px' : '',
      'idcard_psb_nomor_color' => (isset($app->idcard_psb_nomor_color)) ? $app->idcard_psb_nomor_color : '',
      'idcard_psb_nama_top' => (isset($app->idcard_psb_nama_top)) ? $app->idcard_psb_nama_top . 'px' : '',
      'idcard_psb_nama_left' => (isset($app->idcard_psb_nama_left)) ? $app->idcard_psb_nama_left . 'px' : '',
      'idcard_psb_nama_font_size' => (isset($app->idcard_psb_nama_font_size)) ? $app->idcard_psb_nama_font_size . 'px' : '',
      'idcard_psb_nama_color' => (isset($app->idcard_psb_nama_color)) ? $app->idcard_psb_nama_color : '',
      'idcard_psb_alamat_top' => (isset($app->idcard_psb_alamat_top)) ? $app->idcard_psb_alamat_top . 'px' : '',
      'idcard_psb_alamat_left' => (isset($app->idcard_psb_alamat_left)) ? $app->idcard_psb_alamat_left . 'px' : '',
      'idcard_psb_alamat_font_size' => (isset($app->idcard_psb_alamat_font_size)) ? $app->idcard_psb_alamat_font_size . 'px' : '',
      'idcard_psb_alamat_color' => (isset($app->idcard_psb_alamat_color)) ? $app->idcard_psb_alamat_color : '',
      'idcard_psb_ruangan1_top' => (isset($app->idcard_psb_ruangan1_top)) ? $app->idcard_psb_ruangan1_top . 'px' : '',
      'idcard_psb_ruangan1_left' => (isset($app->idcard_psb_ruangan1_left)) ? $app->idcard_psb_ruangan1_left . 'px' : '',
      'idcard_psb_ruangan1_font_size' => (isset($app->idcard_psb_ruangan1_font_size)) ? $app->idcard_psb_ruangan1_font_size . 'px' : '',
      'idcard_psb_ruangan1_color' => (isset($app->idcard_psb_ruangan1_color)) ? $app->idcard_psb_ruangan1_color : '',
      'idcard_psb_ruangan2_top' => (isset($app->idcard_psb_ruangan2_top)) ? $app->idcard_psb_ruangan2_top . 'px' : '',
      'idcard_psb_ruangan2_left' => (isset($app->idcard_psb_ruangan2_left)) ? $app->idcard_psb_ruangan2_left . 'px' : '',
      'idcard_psb_ruangan2_font_size' => (isset($app->idcard_psb_ruangan2_font_size)) ? $app->idcard_psb_ruangan2_font_size . 'px' : '',
      'idcard_psb_ruangan2_color' => (isset($app->idcard_psb_ruangan2_color)) ? $app->idcard_psb_ruangan2_color : '',
      'idcard_psb_foto_top' => (isset($app->idcard_psb_foto_top)) ? $app->idcard_psb_foto_top . 'px' : '',
      'idcard_psb_foto_left' => (isset($app->idcard_psb_foto_left)) ? $app->idcard_psb_foto_left . 'px' : '',
      'idcard_psb_foto_show' => (isset($app->idcard_psb_foto_show)) ? $app->idcard_psb_foto_show : 'none',
      'idcard_psb_foto_width' => (isset($app->idcard_psb_foto_width)) ? $app->idcard_psb_foto_width . 'px' : '',
      'idcard_psb_foto_height' => (isset($app->idcard_psb_foto_height)) ? $app->idcard_psb_foto_height . 'px' : '',
    );

    return (object) $idcard;
  }

  public function index()
  {
    show_404();
  }

  public function generate()
  {
    $role = $this->session->userdata('user')['role'];
    $userId = $this->session->userdata('user')['id'];
    $psbStatus = null;
    $deniedDom = null;

    $psb = $this->PsbModel->getDetail(['id' => $userId]);
    $mappingRuangan = $this->MappingRuanganModel->getRuanganByPsb($userId);

    if (!is_null($psb)) {
      if ($role === 'Calon Santri') {
        $psbStatus = $this->PsbModel->getStatus((int) $psb->status);

        if ((int) $psb->status === 0) {
          $deniedDom = '
            <span style="font-size: 12pt; font-weight: 500;">Maaf!</span>
            <p class="mt-1 mb-0">
              Anda hanya dapat melihat informasi ini setelah / sudah melakukan <strong>Pembayaran Administrasi Pendaftaran</strong>,
              status Anda saat ini
              <strong>' . $psbStatus . '</strong>.
            </p>
            <p class="mt-1">
              <a href="' . base_url('post/detail/3') . '">Klik disini</a> untuk melihat petunjuk pembayaran.
            </p>
            <hr />
            <i>Terima kasih</i>
          ';
        };
      };

      $data = array(
        'app' => $this->app(),
        'main_js' => $this->load_main_js('idcard'),
        'card_title' => 'Kartu Peserta',
        'idcard' => $this->_intializeStyle(),
        'psb' => $psb,
        'psb_status' => $psbStatus,
        'ruangan' => $mappingRuangan,
        'denied_dom' => $deniedDom
      );
      $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
      $this->template->load_view('generate', $data, TRUE);
      $this->template->render();
    } else {
      show_404();
    };
  }
}
