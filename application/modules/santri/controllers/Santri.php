<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . 'controllers/AppBackend.php');

class Santri extends AppBackend
{
  function __construct()
  {
    parent::__construct();
    $this->load->model([
      'AppModel',
      'UserModel',
      'SantriModel',
      'KelasModel',
      'SubkelasModel',
      'ProvincesModel',
      'RegenciesModel',
      'MappingOrtuModel',
    ]);
    $this->load->library('form_validation');
  }

  public function index()
  {
    $user_id = $this->session->userdata('user')['id'];
    $user_role = $this->session->userdata('user')['role'];

    if ($user_role === 'Guru') {
      $list_kelas_combo = $this->init_list_kelas_by_guru($user_id);
    } else {
      $list_kelas_combo = $this->init_list_kelas();
    };

    $data = array(
      'app' => $this->app(),
      'main_js' => $this->load_main_js('santri'),
      'card_title' => 'Santri',
      'list_kelas' => $this->init_list($this->KelasModel->getAll(), 'nama', 'nama'),
      'list_sub_kelas' => $this->init_list($this->SubkelasModel->getAll(), 'nama', 'nama'),
      'list_provinces' => $this->init_list($this->ProvincesModel->getAll(), 'id', 'name'),
      'list_regencies' => $this->init_list($this->RegenciesModel->getAll(), 'id', 'name'),
      'list_pembina' => $this->init_list($this->UserModel->getAll(['role' => 'Pembina']), 'id', 'nama_lengkap'),
      'list_kelas_combo' => $list_kelas_combo,
    );
    $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
    $this->template->load_view('index', $data, TRUE);
    $this->template->render();
  }

  public function ajax_get_all()
  {
    $this->handle_ajax_request();
    $user_id = $this->session->userdata('user')['id'];
    $user_role = $this->session->userdata('user')['role'];
    $static_conditional_spec = array();
    $static_conditional_in_key = null;
    $static_conditional_in = array();

    if ($user_role === 'Pembina') {
      $static_conditional_spec = array(
        'pembina' => $user_id
      );
    } else if ($user_role === 'Orang Tua') {
      $santriList = $this->MappingOrtuModel->getSantriIdFromOrtu($user_id);
      $static_conditional_in_key = 'id';
      $static_conditional_in = $santriList;
    };

    $dtAjax_config = array(
      'table_name' => 'view_santri',
      'static_conditional_spec' => $static_conditional_spec,
      'static_conditional_in_key' => $static_conditional_in_key,
      'static_conditional_in' => $static_conditional_in,
      'order_column' => 6,
      'order_column_dir' => 'desc'
    );

    $response = $this->AppModel->getData_dtAjax($dtAjax_config);
    echo json_encode($response);
  }

  public function ajax_save($id = null)
  {
    $this->handle_ajax_request();
    $this->form_validation->set_rules($this->SantriModel->rules($id));

    if ($this->form_validation->run() === true) {
      if (is_null($id)) {
        echo json_encode($this->SantriModel->insert());
      } else {
        echo json_encode($this->SantriModel->update($id));
      };
    } else {
      $errors = validation_errors('<div>- ', '</div>');
      echo json_encode(array('status' => false, 'data' => $errors));
    };
  }

  public function ajax_delete($id)
  {
    $this->handle_ajax_request();
    echo json_encode($this->SantriModel->delete($id));
  }

  public function ajax_generate_qrcode_by_kelas()
  {
    // $this->handle_ajax_request();
    $filter_kelas = $this->input->post('filter_kelas');

    if (!empty($filter_kelas)) {
      $filter_kelas_ex = explode('#', $filter_kelas);
      $kelas = (isset($filter_kelas_ex[0])) ? $filter_kelas_ex[0] : null;
      $sub_kelas = (isset($filter_kelas_ex[1])) ? $filter_kelas_ex[1] : null;

      $santri = $this->SantriModel->getAll(['kelas' => $kelas, 'sub_kelas' => $sub_kelas]);

      if (count($santri) > 0) {
        foreach ($santri as $index => $item) {
          $santriId = $item->id;
          $santriNisn = $item->nisn;
          $santriNama = $item->nama_lengkap;
          $santriQrCode = $item->qrcode;
          $code = $santriId . '##' . $santriNisn . '##' . $santriNama;

          if (is_null($santriQrCode) && empty($santriQrCode)) {
            // $qrToDelete = FCPATH . 'directory/santri/qrcode/' . $item->qrcode;
            // @unlink($qrToDelete);

            $generateQr = $this->generateQrCode($code, false, true);

            if ($generateQr->status === true) {
              $this->SantriModel->setQrCode($santriId, $generateQr->file_path);
            };
          };
        };

        return $this->load->view('qrcode_preview', array(
          'controller' => $this,
          'santri' => $this->SantriModel->getAll(['kelas' => $kelas, 'sub_kelas' => $sub_kelas]),
          'idcard' => $this->_intializeKartuPelajarStyle(),
        ));
      } else {
        echo '<center><i>Santri is not available, cannot generate qrcode.</i></center>';
      };
    } else {
      echo json_encode(['status' => false, 'data' => 'Pastikan Kelas sudah dipilih.']);
    };
  }

  private function _intializeKartuPelajarStyle()
  {
    $app = $this->app();
    $idcard = array(
      'idcard_pelajar_background' => (isset($app->idcard_pelajar_background)) ? $app->idcard_pelajar_background : '',
      'idcard_pelajar_nomor_top' => (isset($app->idcard_pelajar_nomor_top)) ? $app->idcard_pelajar_nomor_top . 'px' : '',
      'idcard_pelajar_nomor_left' => (isset($app->idcard_pelajar_nomor_left)) ? $app->idcard_pelajar_nomor_left . 'px' : '',
      'idcard_pelajar_nomor_font_size' => (isset($app->idcard_pelajar_nomor_font_size)) ? $app->idcard_pelajar_nomor_font_size . 'px' : '',
      'idcard_pelajar_nomor_color' => (isset($app->idcard_pelajar_nomor_color)) ? $app->idcard_pelajar_nomor_color : '',
      'idcard_pelajar_nama_top' => (isset($app->idcard_pelajar_nama_top)) ? $app->idcard_pelajar_nama_top . 'px' : '',
      'idcard_pelajar_nama_left' => (isset($app->idcard_pelajar_nama_left)) ? $app->idcard_pelajar_nama_left . 'px' : '',
      'idcard_pelajar_nama_font_size' => (isset($app->idcard_pelajar_nama_font_size)) ? $app->idcard_pelajar_nama_font_size . 'px' : '',
      'idcard_pelajar_nama_color' => (isset($app->idcard_pelajar_nama_color)) ? $app->idcard_pelajar_nama_color : '',
      'idcard_pelajar_alamat_top' => (isset($app->idcard_pelajar_alamat_top)) ? $app->idcard_pelajar_alamat_top . 'px' : '',
      'idcard_pelajar_alamat_left' => (isset($app->idcard_pelajar_alamat_left)) ? $app->idcard_pelajar_alamat_left . 'px' : '',
      'idcard_pelajar_alamat_font_size' => (isset($app->idcard_pelajar_alamat_font_size)) ? $app->idcard_pelajar_alamat_font_size . 'px' : '',
      'idcard_pelajar_alamat_color' => (isset($app->idcard_pelajar_alamat_color)) ? $app->idcard_pelajar_alamat_color : '',
      'idcard_pelajar_foto_top' => (isset($app->idcard_pelajar_foto_top)) ? $app->idcard_pelajar_foto_top . 'px' : '',
      'idcard_pelajar_foto_left' => (isset($app->idcard_pelajar_foto_left)) ? $app->idcard_pelajar_foto_left . 'px' : '',
      'idcard_pelajar_foto_show' => (isset($app->idcard_pelajar_foto_show)) ? $app->idcard_pelajar_foto_show : 'none',
      'idcard_pelajar_foto_width' => (isset($app->idcard_pelajar_foto_width)) ? $app->idcard_pelajar_foto_width . 'px' : '',
      'idcard_pelajar_foto_height' => (isset($app->idcard_pelajar_foto_height)) ? $app->idcard_pelajar_foto_height . 'px' : '',
      'idcard_pelajar_qrcode_top' => (isset($app->idcard_pelajar_qrcode_top)) ? $app->idcard_pelajar_qrcode_top . 'px' : '',
      'idcard_pelajar_qrcode_left' => (isset($app->idcard_pelajar_qrcode_left)) ? $app->idcard_pelajar_qrcode_left . 'px' : '',
      'idcard_pelajar_qrcode_show' => (isset($app->idcard_pelajar_qrcode_show)) ? $app->idcard_pelajar_qrcode_show : 'none',
      'idcard_pelajar_qrcode_width' => (isset($app->idcard_pelajar_qrcode_width)) ? $app->idcard_pelajar_qrcode_width . 'px' : '',
      'idcard_pelajar_qrcode_height' => (isset($app->idcard_pelajar_qrcode_height)) ? $app->idcard_pelajar_qrcode_height . 'px' : '',
    );

    return (object) $idcard;
  }
}
