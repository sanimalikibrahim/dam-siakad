<?php
$default_profile_photo = base_url('themes/_public/img/avatar/male-1.png');
?>

<style type="text/css">
  .qrcode-wrapper {
    margin: 0;
    padding: 0;
    /* max-height: 424px; */
    white-space: normal;
    overflow: auto;
  }

  /* ID-Card Preview */
  .idcard-preview {
    float: left;
    width: fit-content;
    width: -moz-fit-content;
    margin: 0px 0px 10px 10px;
    overflow: hidden;
  }

  .idcard-preview-qrcode {
    width: <?= $idcard->idcard_pelajar_qrcode_width ?>;
    height: <?= $idcard->idcard_pelajar_qrcode_height ?>;
    justify-content: center;
    align-items: center;
    text-align: center;
    background: #fff;
    border: 1px solid #999;
    position: absolute;
    opacity: 1;
    margin-top: <?= $idcard->idcard_pelajar_qrcode_top ?>;
    margin-left: <?= $idcard->idcard_pelajar_qrcode_left ?>;
    display: <?= $idcard->idcard_pelajar_qrcode_show ?>;
  }

  .idcard-preview-qrcode img {
    width: 100%;
    height: 100%;
    object-fit: cover;
  }

  .idcard-preview-foto {
    width: <?= $idcard->idcard_pelajar_foto_width ?>;
    height: <?= $idcard->idcard_pelajar_foto_height ?>;
    display: flex;
    justify-content: center;
    align-items: center;
    text-align: center;
    background: #fff;
    border: 1px solid #999;
    position: absolute;
    opacity: 1;
    margin-top: <?= $idcard->idcard_pelajar_foto_top ?>;
    margin-left: <?= $idcard->idcard_pelajar_foto_left ?>;
    display: <?= $idcard->idcard_pelajar_foto_show ?>;
  }

  .idcard-preview-foto img {
    width: 100%;
    height: 100%;
    object-fit: cover;
  }

  .idcard-preview-nomor {
    width: 150px;
    font-weight: 500;
    position: absolute;
    margin-top: <?= $idcard->idcard_pelajar_nomor_top ?>;
    margin-left: <?= $idcard->idcard_pelajar_nomor_left ?>;
    font-size: <?= $idcard->idcard_pelajar_nomor_font_size ?>;
    color: <?= $idcard->idcard_pelajar_nomor_color ?>;
  }

  .idcard-preview-nama {
    max-width: 240px;
    font-weight: 500;
    position: absolute;
    margin-top: <?= $idcard->idcard_pelajar_nama_top ?>;
    margin-left: <?= $idcard->idcard_pelajar_nama_left ?>;
    font-size: <?= $idcard->idcard_pelajar_nama_font_size ?>;
    color: <?= $idcard->idcard_pelajar_nama_color ?>;
  }

  .idcard-preview-alamat {
    max-width: 240px;
    font-weight: 500;
    position: absolute;
    margin-top: <?= $idcard->idcard_pelajar_alamat_top ?>;
    margin-left: <?= $idcard->idcard_pelajar_alamat_left ?>;
    font-size: <?= $idcard->idcard_pelajar_alamat_font_size ?>;
    color: <?= $idcard->idcard_pelajar_alamat_color ?>;
  }

  .idcard-preview-image {
    width: 400px;
    object-fit: contain;
    border: 1px solid #ccc;
  }

  /* END ## ID-Card Preview */
</style>

<?php if (count($santri) > 0) : ?>
  <div class="qrcode-wrapper">
    <?php $no = 1; ?>
    <?php foreach ($santri as $key => $item) : ?>
      <?php if (!empty($item->nomor_induk_lokal) && !is_null($item->nomor_induk_lokal)) : ?>
        <!-- Preview item -->
        <div class="idcard-preview">
          <div class="idcard-preview-qrcode">
            <img src="<?= base_url($item->qrcode) ?>" alt="QR-Code">
          </div>
          <div class="idcard-preview-foto">
            <img src="<?= (!is_null($item->profile_photo) && !empty($item->profile_photo)) ? base_url($item->profile_photo) : $default_profile_photo ?>" alt="FOTO">
          </div>
          <div class="idcard-preview-nomor">
            <?= $item->nomor_induk_lokal ?>
          </div>
          <div class="idcard-preview-nama">
            <?= strtoupper($item->nama_lengkap) ?>
          </div>
          <div class="idcard-preview-alamat">
            <?= (!empty($item->alamat) && !is_null($item->alamat)) ? strtoupper($item->alamat) : '-' ?>
          </div>
          <img class="idcard-preview-image" src="<?= base_url($idcard->idcard_pelajar_background) ?>" alt="Kartu Pelajar Background" style="width: 500px; object-fit: contain;" />
        </div>
        <!-- END ## Preview item -->
        <?php if ($no === 27) : ?>
          <?php $no = 1 ?>
          <div style="page-break-after: always;"></div>
        <?php else : ?>
          <?php $no++ ?>
        <?php endif; ?>
      <?php endif; ?>
    <?php endforeach ?>
  </div>
<?php endif ?>