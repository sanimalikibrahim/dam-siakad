<div class="modal fade" id="modal-partial">
  <div class="modal-dialog modal-dialog-centered modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title pull-left partial-title">-</h5>
      </div>
      <div class="spinner">
        <div class="lds-hourglass"></div>
      </div>
      <div class="modal-body">
        <div class="partial-content"></div>
      </div>
      <div class="modal-footer">
        <div class="partial-footer"></div>
        <button type="button" class="btn btn-light" data-dismiss="modal">
          Close
        </button>
      </div>
    </div>
  </div>
</div>