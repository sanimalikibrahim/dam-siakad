<script type="text/javascript">
  $(document).ready(function() {

    var _key = "";
    var _section = "santri";
    var _table = "table-santri";
    var _modal = "modal-form-santri";
    var _modal_partial = "modal-partial";
    var _form = "form-santri";
    var _activeRole = "<?= $this->session->userdata('user')['role'] ?>";

    // Initialize DataTables: Index
    if ($("#" + _table)[0]) {
      var table_santri = $("#" + _table).DataTable({
        processing: true,
        serverSide: true,
        ajax: {
          url: "<?php echo base_url('santri/ajax_get_all/') ?>",
          type: "get"
        },
        columns: [{
            data: null,
            render: function(data, type, row, meta) {
              return meta.row + meta.settings._iDisplayStart + 1;
            }
          },
          {
            data: "nisn"
          },
          {
            data: "nomor_induk_lokal"
          },
          {
            data: "nama_lengkap"
          },
          {
            data: "kelas",
            render: function(data, type, row, meta) {
              if (data !== null) {
                return data + " " + row["sub_kelas"];
              } else {
                return null;
              };
            }
          },
          {
            data: "asrama"
          },
          {
            data: "created_at"
          },
          {
            data: "updated_at"
          },
          {
            data: null,
            className: "center",
            render: function(data, type, row, meta) {
              var buttons = '<div class="action">';
              buttons += '<a href="javascript:;" class="btn btn-sm btn-light btn-table-action action-edit" data-toggle="modal" data-target="#' + _modal + '"><i class="zmdi zmdi-edit"></i> Edit</a>&nbsp;';

              if ($.inArray(_activeRole, ['Pembina', 'Orang Tua']) === -1) {
                buttons += '<a href="javascript:;" class="btn btn-sm btn-light btn-table-action action-delete"><i class="zmdi zmdi-delete"></i> Delete</a>';
              };

              buttons += '</div>';
              return buttons;
            },
          },
          {
            data: "kelas_concated"
          },
        ],
        autoWidth: !1,
        responsive: {
          details: {
            renderer: function(api, rowIdx, columns) {
              var hideColumn = [9];
              var data = $.map(columns, function(col, i) {
                return ($.inArray(col.columnIndex, hideColumn)) ?
                  '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                  '<td class="dt-details-td">' + col.title + ':' + '</td> ' +
                  '<td class="dt-details-td">' + col.data + '</td>' +
                  '</tr>' :
                  '';
              }).join('');

              return data ? $('<table/>').append(data) : false;
            },
            type: "inline",
            target: 'tr',
          }
        },
        columnDefs: [{
          "targets": [9],
          "visible": false
        }, {
          className: 'desktop',
          targets: [0, 1, 2, 3, 4, 5, 6, 7, 8]
        }, {
          className: 'tablet',
          targets: [0, 1, 2, 3]
        }, {
          className: 'mobile',
          targets: [0, 3]
        }, {
          responsivePriority: 2,
          targets: -1
        }],
        pageLength: 15,
        language: {
          searchPlaceholder: "Search...",
          sProcessing: '<div style="text-align: center;"><div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div></div>'
        },
        sDom: '<"dataTables_ct"><"dataTables__top"fb>rt<"dataTables__bottom"ip><"clear">',
        buttons: [{
          extend: "excelHtml5",
          title: "Export Result"
        }, {
          extend: "print",
          title: "Export Result"
        }],
        initComplete: function(a, b) {
          $(this).closest(".dataTables_wrapper").find(".dataTables__top").prepend(
            '<div class="dataTables_buttons hidden-sm-down actions">' +
            '<span class="actions__item zmdi zmdi-refresh" data-table-action="reload" title="Reload" />' +
            '</div>'
          );
        },
      });

      $(".dataTables_filter input[type=search]").focus(function() {
        $(this).closest(".dataTables_filter").addClass("dataTables_filter--toggled")
      });

      $(".dataTables_filter input[type=search]").blur(function() {
        $(this).closest(".dataTables_filter").removeClass("dataTables_filter--toggled")
      });

      $("body").on("click", "[data-table-action]", function(a) {
        a.preventDefault();
        var b = $(this).data("table-action");
        if ("reload" === b) {
          $("#" + _table).DataTable().ajax.reload(null, false);
        };
      });

      $(document).on("change", ".santri-filter-kelas", function() {
        var filter_kelas = $("#" + _section + " .santri-filter-kelas").val();
        table_santri.column(9).search(filter_kelas).draw();
      });
    };

    // Handle data add
    $("#" + _section).on("click", "button." + _section + "-action-add", function(e) {
      e.preventDefault();
      resetForm();
      $("#" + _form + " ." + _section + "-nav-general").click();
    });

    // Handle data edit
    $("#" + _table).on("click", "a.action-edit", function(e) {
      e.preventDefault();
      resetForm();
      var temp = table_santri.row($(this).closest('tr')).data();

      // Set key for update params, important!
      _key = temp.id;

      // Set tab active
      $("#" + _form + " ." + _section + "-nav-general").click();

      // General
      $("#" + _form + " ." + _section + "-nisn").val(temp.nisn).trigger('input');
      $("#" + _form + " ." + _section + "-nomor_induk_lokal").val(temp.nomor_induk_lokal).trigger("input");
      $("#" + _form + " ." + _section + "-nomor_absen").val(temp.nomor_absen).trigger("input");
      $("#" + _form + " ." + _section + "-nama_lengkap").val(temp.nama_lengkap).trigger("input");
      $("#" + _form + " ." + _section + "-jenis_kelamin").val(temp.jenis_kelamin).trigger("change");
      $("#" + _form + " ." + _section + "-kelas").val(temp.kelas).trigger("change");
      $("#" + _form + " ." + _section + "-sub_kelas").val(temp.sub_kelas).trigger("change");
      $("#" + _form + " ." + _section + "-asrama").val(temp.asrama).trigger("input");
      $("#" + _form + " ." + _section + "-tempat_lahir").val(temp.tempat_lahir).trigger("input");
      $("#" + _form + " ." + _section + "-tanggal_lahir").val(temp.tanggal_lahir).trigger("input");
      $("#" + _form + " ." + _section + "-wali").val(temp.wali).trigger("input");
      $("#" + _form + " ." + _section + "-pembina").val(temp.pembina).trigger("change");
      $("#" + _form + " ." + _section + "-tanggal_masuk").val(temp.tanggal_masuk).trigger("input");
      $("#" + _form + " ." + _section + "-golongan_darah").val(temp.golongan_darah).trigger("change");
      $("#" + _form + " ." + _section + "-berat_badan").val(temp.berat_badan).trigger("input");
      $("#" + _form + " ." + _section + "-tinggi_badan").val(temp.tinggi_badan).trigger("input");
      $("#" + _form + " ." + _section + "-alamat").val(temp.alamat.replace(/<br\s*[\/]?>/gi, "\r\n")).trigger("input");
      $("#" + _form + " ." + _section + "-provinsi").val(temp.provinsi).trigger("change");
      $("#" + _form + " ." + _section + "-kabupaten_kota").val(temp.kabupaten_kota).trigger("change");
      // Ayah
      $("#" + _form + " ." + _section + "-ayah_nama").val(temp.ayah_nama).trigger("input");
      $("#" + _form + " ." + _section + "-ayah_hp").val(temp.ayah_hp).trigger("input");
      $("#" + _form + " ." + _section + "-ayah_alamat").val(temp.ayah_alamat.replace(/<br\s*[\/]?>/gi, "\r\n")).trigger("input");
      $("#" + _form + " ." + _section + "-ayah_provinsi").val(temp.ayah_provinsi).trigger("change");
      $("#" + _form + " ." + _section + "-ayah_kabupaten_kota").val(temp.ayah_kabupaten_kota).trigger("change");
      // Ibu
      $("#" + _form + " ." + _section + "-ibu_nama").val(temp.ibu_nama).trigger("input");
      $("#" + _form + " ." + _section + "-ibu_hp").val(temp.ibu_hp).trigger("input");
      $("#" + _form + " ." + _section + "-ibu_alamat").val(temp.ibu_alamat.replace(/<br\s*[\/]?>/gi, "\r\n")).trigger("input");
      $("#" + _form + " ." + _section + "-ibu_provinsi").val(temp.ibu_provinsi).trigger("change");
      $("#" + _form + " ." + _section + "-ibu_kabupaten_kota").val(temp.ibu_kabupaten_kota).trigger("change");
      // Wali
      $("#" + _form + " ." + _section + "-wali_nama").val(temp.wali_nama).trigger("input");
      $("#" + _form + " ." + _section + "-wali_hp").val(temp.wali_hp).trigger("input");
      $("#" + _form + " ." + _section + "-wali_hubungan").val(temp.wali_hubungan).trigger("input");
      $("#" + _form + " ." + _section + "-wali_provinsi").val(temp.wali_provinsi).trigger("change");
      $("#" + _form + " ." + _section + "-wali_kabupaten_kota").val(temp.wali_kabupaten_kota).trigger("change");

      // Handle textarea height
      setTimeout(function() {
        setTextareHeight(true);
      }, 500);
    });

    // Handle data submit
    $("#" + _modal + " ." + _section + "-action-save").on("click", function(e) {
      e.preventDefault();
      $.ajax({
        type: "post",
        url: "<?php echo base_url('santri/ajax_save/') ?>" + _key,
        data: $("#" + _form).serialize(),
        success: function(response) {
          var response = JSON.parse(response);
          if (response.status === true) {
            resetForm();
            $("#" + _modal).modal("hide");
            $("#" + _table).DataTable().ajax.reload(null, false);
            notify(response.data, "success");
          } else {
            notify(response.data, "danger");
          };
        }
      });
    });

    // Handle data delete
    $("#" + _table).on("click", "a.action-delete", function(e) {
      e.preventDefault();
      var temp = table_santri.row($(this).closest('tr')).data();

      swal({
        title: "Are you sure to delete?",
        text: "Once deleted, you will not be able to recover this data!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false
      }).then((result) => {
        if (result.value) {
          $.ajax({
            type: "delete",
            url: "<?php echo base_url('santri/ajax_delete/') ?>" + temp.id,
            dataType: "json",
            success: function(response) {
              if (response.status) {
                $("#" + _table).DataTable().ajax.reload(null, false);
                notify(response.data, "success");
              } else {
                notify(response.data, "danger");
              };
            }
          });
        };
      });
    });

    // Handle generate qr-code
    $("#" + _section).on("click", "button." + _section + "-action-print-qrcode", function(e) {
      e.preventDefault();
      var filter_kelas = $("#" + _section + " .santri-filter-kelas").val();

      if (filter_kelas != null && filter_kelas != "-1") {
        $("#" + _modal_partial).modal("show");
        $("#" + _modal_partial + " .partial-title").html("Generate ID-Card");
        $("#" + _modal_partial + " .partial-content").html("");
        $("#" + _modal_partial + " .partial-footer").html("");

        // Fetch all barcode
        $.ajax({
          type: "post",
          url: "<?= base_url('santri/ajax_generate_qrcode_by_kelas/') ?>",
          data: {
            filter_kelas
          },
          success: function(response) {
            var button = '';
            button += '<a href="javascript:;" class="btn btn-success btn-print-qrcode-modal"><i class="zmdi zmdi-print"></i> Print</a>&nbsp;';

            $("#" + _modal_partial + " .partial-content").html(response);
            $("#" + _modal_partial + " .partial-footer").html(button);
          }
        });
      } else {
        notify("Please select Kelas before!", "warning");
      };
    });

    // Handle print qr-code
    $("#" + _modal_partial).on("click", "a.btn-print-qrcode-modal", function() {
      const canvas = $("#" + _modal_partial + " .partial-content");

      // Print by selector element
      $(canvas).printThis();
    });

    // Handle form reset
    resetForm = () => {
      _key = "";
      $("#" + _form).trigger("reset");
      setTextareHeight(true);
      $("#" + _form + " ." + _section + "-provinsi").val("").trigger('change');
      $("#" + _form + " ." + _section + "-kabupaten_kota").val("").trigger('change');
      $("#" + _form + " ." + _section + "-ayah_provinsi").val("").trigger('change');
      $("#" + _form + " ." + _section + "-ayah_kabupaten_kota").val("").trigger('change');
      $("#" + _form + " ." + _section + "-ibu_provinsi").val("").trigger('change');
      $("#" + _form + " ." + _section + "-ibu_kabupaten_kota").val("").trigger('change');
      $("#" + _form + " ." + _section + "-wali_provinsi").val("").trigger('change');
      $("#" + _form + " ." + _section + "-wali_kabupaten_kota").val("").trigger('change');
    };

    // Handle textarea height
    setTextareHeight = (isReset = false) => {
      if (isReset === true) {
        $("#" + _form + " ." + _section + "-alamat").css("height", "31px").keyup();
        $("#" + _form + " ." + _section + "-ayah_alamat").css("height", "31px").keyup();
        $("#" + _form + " ." + _section + "-ibu_alamat").css("height", "31px").keyup();
      } else {
        $("#" + _form + " ." + _section + "-alamat").height($("#" + _form + " ." + _section + "-alamat")[0].scrollHeight).keyup();
        $("#" + _form + " ." + _section + "-ayah_alamat").height($("#" + _form + " ." + _section + "-ayah_alamat")[0].scrollHeight).keyup();
        $("#" + _form + " ." + _section + "-ibu_alamat").height($("#" + _form + " ." + _section + "-ibu_alamat")[0].scrollHeight).keyup();
      };
    };

  });
</script>