<section id="santri">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title"><?php echo (isset($card_title)) ? $card_title : '' ?></h4>
            <h6 class="card-subtitle"><?php echo (isset($card_subTitle)) ? $card_subTitle : '' ?></h6>

            <?php if (!in_array($this->session->userdata('user')['role'], ['Pembina', 'Orang Tua'])) : ?>
                <div class="table-action row">
                    <div class="buttons col-auto">
                        <button class="btn btn--raised btn-primary btn--icon-text santri-action-add" data-toggle="modal" data-target="#modal-form-santri">
                            <i class="zmdi zmdi-plus-circle"></i> Add New
                        </button>
                    </div>
                    <div class="buttons col">
                        <div class="input-group mb-0">
                            <div class="input-group-prepend">
                                <label class="input-group-text" style="height: 34.13px; background: #f2f2f2;">Kelas</label>
                            </div>
                            <select class="custom-select santri-filter-kelas" style="height: 34.13px; max-width: 230px;">
                                <?= $list_kelas_combo ?>
                            </select>
                            <div class="input-group-apend">
                                <button class="btn btn--raised btn-success btn--icon-text santri-action-print-qrcode">
                                    <i class="zmdi zmdi-card"></i> Generate ID-Card
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>

            <?php include_once('form.php') ?>
            <?php include_once('partial.php') ?>

            <div class="table-responsive">
                <table id="table-santri" class="table table-bordered">
                    <thead class="thead-default">
                        <tr>
                            <th width="100">No</th>
                            <th>NISN</th>
                            <th>NIL</th>
                            <th>Nama Lengkap</th>
                            <th>Kelas</th>
                            <th>Asrama</th>
                            <th>Created</th>
                            <th>Updated</th>
                            <th width="170">Action</th>
                            <th width="170">Kelas Concated</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</section>