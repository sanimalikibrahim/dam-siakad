<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . 'controllers/AppBackend.php');

class Raport_cetak extends AppBackend
{
  function __construct()
  {
    parent::__construct();
    $this->load->model([
      'AppModel',
      'UserModel',
      'SantriModel',
      'KelasModel',
      'SubkelasModel',
    ]);
    $this->load->library('form_validation');
  }

  public function index()
  {
    $user_id = $this->session->userdata('user')['id'];
    $user_role = $this->session->userdata('user')['role'];

    if ($user_role === 'Guru') {
      $list_kelas_combo = $this->init_list_kelas_by_guru($user_id);
    } else {
      $list_kelas_combo = $this->init_list_kelas();
    };

    $data = array(
      'app' => $this->app(),
      'main_js' => $this->load_main_js('raport_cetak'),
      'card_title' => 'Raport › Cetak',
      'list_kelas' => $this->init_list($this->KelasModel->getAll(), 'nama', 'nama'),
      'list_sub_kelas' => $this->init_list($this->SubkelasModel->getAll(), 'nama', 'nama'),
      'list_kelas_combo' => $list_kelas_combo
    );
    $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
    $this->template->load_view('index', $data, TRUE);
    $this->template->render();
  }

  public function ajax_get_all()
  {
    $this->handle_ajax_request();
    $isSearchColumn0 = $this->input->get('columns')[0]['search']['value'];
    $user_id = $this->session->userdata('user')['id'];
    $user_role = $this->session->userdata('user')['role'];
    $static_conditional_spec = array();
    $static_conditional_in_key = null;
    $static_conditional_in = array();

    // Empty default
    if (empty(trim($isSearchColumn0))) {
      $static_conditional_spec['id'] = null;
    };

    if ($user_role === 'Pembina') {
      $static_conditional_spec = array(
        'pembina' => $user_id
      );
    };

    $dtAjax_config = array(
      'table_name' => 'view_santri',
      'static_conditional_spec' => $static_conditional_spec,
      'static_conditional_in_key' => $static_conditional_in_key,
      'static_conditional_in' => $static_conditional_in,
      'order_column' => 4,
      'order_column_dir' => 'asc'
    );

    $response = $this->AppModel->getData_dtAjax($dtAjax_config);
    echo json_encode($response);
  }

  public function ajax_save($id = null)
  {
    $this->handle_ajax_request();
    $this->form_validation->set_rules($this->SantriModel->rules($id));

    if ($this->form_validation->run() === true) {
      if (is_null($id)) {
        echo json_encode($this->SantriModel->insert());
      } else {
        echo json_encode($this->SantriModel->update($id));
      };
    } else {
      $errors = validation_errors('<div>- ', '</div>');
      echo json_encode(array('status' => false, 'data' => $errors));
    };
  }
}
