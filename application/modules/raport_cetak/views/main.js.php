<script type="text/javascript">
  $(document).ready(function() {

    var _key = "";
    var _activeRole = "<?= $this->session->userdata('user')['role'] ?>";
    var _section = "raport_cetak";
    var _table = "table-raport_cetak";

    // Initialize DataTables: Kompetensi Dasar
    if ($("#" + _table)[0]) {
      var table = $("#" + _table).DataTable({
        processing: true,
        serverSide: true,
        ajax: {
          url: "<?php echo base_url('raport_cetak/ajax_get_all/') ?>",
          type: "get"
        },
        columns: [{
            data: "kelas_concated"
          }, {
            data: null,
            render: function(data, type, row, meta) {
              return meta.row + meta.settings._iDisplayStart + 1;
            }
          },
          {
            data: "nisn"
          },
          {
            data: "nomor_induk_lokal"
          },
          {
            data: "nama_lengkap"
          },
          {
            data: null,
            render: function(data, type, row, meta) {
              var status = sample(['Sudah Input', 'Belum Input']);
              var statusClass = (status === 'Sudah Input') ? 'text-green' : 'text-red';
              return `<span class="${statusClass}">${status}</span>`;
            }
          },
          {
            data: null,
            className: "center",
            render: function(data, type, row, meta) {
              var buttons = '<div class="action">';
              buttons += '<a href="javascript:;" class="btn btn-sm btn-light btn-table-action action-export"><i class="zmdi zmdi-download"></i> Export</a>';
              buttons += '</div>';
              return buttons;
            },
          }
        ],
        autoWidth: !1,
        responsive: {
          details: {
            renderer: function(api, rowIdx, columns) {
              var hideColumn = [9];
              var data = $.map(columns, function(col, i) {
                return ($.inArray(col.columnIndex, hideColumn)) ?
                  '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                  '<td class="dt-details-td">' + col.title + ':' + '</td> ' +
                  '<td class="dt-details-td">' + col.data + '</td>' +
                  '</tr>' :
                  '';
              }).join('');

              return data ? $('<table/>').append(data) : false;
            },
            type: "inline",
            target: 'tr',
          }
        },
        columnDefs: [{
          "targets": [0],
          "visible": false
        }, {
          className: 'desktop',
          targets: [0, 1, 2, 3, 4, 5, 6]
        }, {
          className: 'tablet',
          targets: [0, 1, 2, 3]
        }, {
          className: 'mobile',
          targets: [0, 3]
        }, {
          responsivePriority: 2,
          targets: -1
        }],
        pageLength: 100,
        language: {
          searchPlaceholder: "Search...",
          sProcessing: '<div style="text-align: center;"><div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div></div>'
        },
        sDom: '<"dataTables_ct"><"dataTables__top"fb>rt<"dataTables__bottom"ip><"clear">',
        buttons: [{
          extend: "excelHtml5",
          title: "Export Result"
        }, {
          extend: "print",
          title: "Export Result"
        }],
        initComplete: function(a, b) {
          $(this).closest(".dataTables_wrapper").find(".dataTables__top").prepend(
            '<div class="dataTables_buttons hidden-sm-down actions">' +
            '<span class="actions__item zmdi zmdi-refresh" data-table-action="reload" title="Reload" />' +
            '</div>'
          );
        },
      });

      $(".dataTables_filter input[type=search]").focus(function() {
        $(this).closest(".dataTables_filter").addClass("dataTables_filter--toggled")
      });

      $(".dataTables_filter input[type=search]").blur(function() {
        $(this).closest(".dataTables_filter").removeClass("dataTables_filter--toggled")
      });

      $("body").on("click", "[data-table-action]", function(a) {
        a.preventDefault();
        var b = $(this).data("table-action");
        if ("reload" === b) {
          $("#" + _table).DataTable().ajax.reload(null, false);
        };
      });
    };

    // Handle Submit Filter
    $(document).on("change", ".raport_cetak-filter-kelas", function() {
      var filter_kelas = $("#" + _section + " .raport_cetak-filter-kelas").val();
      filter_kelas = (filter_kelas == null || filter_kelas == "null") ? "--empty--" : filter_kelas;
      table.column(0).search(filter_kelas).draw();
    });

    // Handle Export
    $("#" + _table).on("click", "a.action-export", function(e) {
      e.preventDefault();

      try {
        var temp = table.row($(this).closest('tr')).data();

        notify("On going...", "danger");
      } catch (error) {
        handleTransErrorMsg();
      };
    });

  });

  function sample(array) {
    return array[Math.floor(Math.random() * array.length)];
  };
</script>