<section id="raport_cetak">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title"><?php echo (isset($card_title)) ? $card_title : '' ?></h4>
            <h6 class="card-subtitle"><?php echo (isset($card_subTitle)) ? $card_subTitle : '' ?></h6>

            <?php if (!in_array($this->session->userdata('user')['role'], ['Pembina', 'Orang Tua'])) : ?>
                <div class="table-action row">
                    <div class="buttons col">
                        <div class="input-group mb-0">
                            <div class="input-group-prepend">
                                <label class="input-group-text" style="height: 34.13px; background: #f2f2f2;">Kelas</label>
                            </div>
                            <select class="custom-select raport_cetak-filter-kelas" style="height: 34.13px; max-width: 230px;">
                                <?= $list_kelas_combo ?>
                            </select>
                        </div>
                    </div>
                </div>
            <?php endif; ?>

            <div class="table-responsive">
                <table id="table-raport_cetak" class="table table-bordered table-hover">
                    <thead class="thead-default">
                        <tr>
                            <th width="170">Kelas Concated</th>
                            <th width="50">No</th>
                            <th width="80">NISN</th>
                            <th width="80">NIL</th>
                            <th>Nama Lengkap</th>
                            <th width="170">Status</th>
                            <th width="170">Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</section>