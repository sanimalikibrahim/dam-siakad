<script type="text/javascript">
  $(document).ready(function() {

    var _p_nisn = $(".p-nisn").val();
    var _section = "pembayaran";
    var _table = "table-pembayaran";
    var _table_daftar_ulang = "table-pembayaran-daftar_ulang";
    var _form_upload = "form-upload";

    // Handle ajax stop
    $(document).ajaxStop(function() {
      $(document).find(".body-loading").fadeOut("fast", function() {
        $(this).hide();
        document.body.style.overflow = "auto";
      });
    });

    // START ## Pendaftaran

    // Initialize DataTables: Index
    if ($("#" + _table)[0]) {
      var table_pembayaran = $("#" + _table).DataTable({
        processing: true,
        serverSide: true,
        ajax: {
          url: "<?php echo base_url('pembayaran/ajax_get_all/') ?>",
          type: "get"
        },
        columns: [{
            data: null,
            render: function(data, type, row, meta) {
              return meta.row + meta.settings._iDisplayStart + 1;
            }
          },
          {
            data: "psb_nisn"
          },
          {
            data: "psb_nama_lengkap"
          },
          {
            data: "description"
          },
          {
            data: "file_name_thumb",
            render: function(data, type, row, meta) {
              let output = '';

              output += '<a href="<?= base_url() ?>' + row.file_name + '" data-fancybox>';
              output += '<img src="<?= base_url() ?>' + data + '" class="img img-responsive img-thumbnail" style="width: 45px; border: 1px solid #999;" />';
              output += '</a>';

              return output;
            }
          },
          {
            data: "telepon",
            render: function(data, type, row, meta) {
              var link = "";
              var isMobile = "<?= $app->is_mobile ?>";

              if (data) {
                if (isMobile) {
                  link = '<a href="http://api.whatsapp.com/send?phone=+62' + data + '&text=Write your message here!" target="_blank">' + data + '</a>'; // mobile
                } else {
                  link = '<a href="http://web.whatsapp.com/send?phone=+62' + data + '&text=Write your message here!" target="_blank">' + data + '</a>'; // desktop
                };
              };

              return link;
            }
          },
          {
            data: "created_at"
          },
          {
            data: null,
            className: "center",
            render: function(data, type, row, meta) {
              let output = '';

              output += '<div class="action">';

              if (parseInt(row.is_verified) === 1) {
                output = '<i class="zmdi zmdi-check-circle"></i> Diterima';
              } else {
                if (parseInt(row.psb_status) === 0) {
                  output += '<a href="javascript:;" class="btn btn-sm btn-success btn-table-action action-set-status spinner-action-button" data-status="1">Konfirmasi dan Aktifkan <div class="spinner-action"></div></a>';
                  output += '&nbsp;';
                  output += '<a href="javascript:;" class="btn btn-sm btn-light btn-table-action action-delete"><i class="zmdi zmdi-delete"></i> Delete</a>';
                } else if (parseInt(row.psb_status) === 3) {
                  output += '<a href="javascript:;" class="btn btn-sm btn-success btn-table-action action-set-status spinner-action-button" data-status="4">Konfirmasi dan Jadikan Santri <div class="spinner-action"></div></a>';
                  output += '&nbsp;';
                  output += '<a href="javascript:;" class="btn btn-sm btn-light btn-table-action action-delete"><i class="zmdi zmdi-delete"></i> Delete</a>';
                } else {
                  output = '<i class="zmdi zmdi-check-circle"></i> Diterima';
                };
              };

              output += '</div>';

              return output;
            }
          }
        ],
        autoWidth: !1,
        responsive: {
          details: {
            renderer: function(api, rowIdx, columns) {
              var hideColumn = [];
              var data = $.map(columns, function(col, i) {
                return ($.inArray(col.columnIndex, hideColumn)) ?
                  '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                  '<td class="dt-details-td">' + col.title + ':' + '</td> ' +
                  '<td class="dt-details-td">' + col.data + '</td>' +
                  '</tr>' :
                  '';
              }).join('');

              return data ? $('<table/>').append(data) : false;
            },
            type: "inline",
            target: 'tr',
          }
        },
        columnDefs: [{
          className: 'desktop',
          targets: [0, 1, 2, 3, 4, 5, 6]
        }, {
          className: 'tablet',
          targets: [0, 1, 2, 5]
        }, {
          className: 'mobile',
          targets: [0, 2]
        }, {
          responsivePriority: 2,
          targets: -1
        }],
        pageLength: 15,
        language: {
          searchPlaceholder: "Search...",
          sProcessing: '<div style="text-align: center;"><div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div></div>'
        },
        sDom: '<"dataTables_ct"><"dataTables__top"fb>rt<"dataTables__bottom"ip><"clear">',
        buttons: [{
          extend: "excelHtml5",
          title: "Export Result"
        }, {
          extend: "print",
          title: "Export Result"
        }],
        initComplete: function(a, b) {
          $(this).closest(".dataTables_wrapper").find(".dataTables__top").prepend(
            '<div class="dataTables_buttons hidden-sm-down actions">' +
            '<span class="actions__item zmdi zmdi-refresh" data-table-action="reload" title="Reload" />' +
            '</div>'
          );
        },
        oSearch: {
          sSearch: _p_nisn
        }
      });

      $(".dataTables_filter input[type=search]").focus(function() {
        $(this).closest(".dataTables_filter").addClass("dataTables_filter--toggled")
      });

      $(".dataTables_filter input[type=search]").blur(function() {
        $(this).closest(".dataTables_filter").removeClass("dataTables_filter--toggled")
      });

      $("body").on("click", "[data-table-action]", function(a) {
        a.preventDefault();
        var b = $(this).data("table-action");
        if ("reload" === b) {
          $("#" + _table).DataTable().ajax.reload(null, false);
        };
      });
    };

    // Handle set status
    $("#" + _table).on("click", "a.action-set-status", function(e) {
      e.preventDefault();
      var temp = table_pembayaran.row($(this).closest('tr')).data();
      var status = parseInt($(this).attr("data-status"));
      var confirmText = '';

      if (status === 1) {
        confirmText = 'Terima pembayaran dan ubah status calon santri menjadi "Sudah Bayar, Aktif".';
      } else if (status === 4) {
        confirmText = 'Terima pembayaran dan ubah status calon santri menjadi "Santri".';
      };

      swal({
        title: "Konfirmasi?",
        text: confirmText,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false
      }).then((result) => {
        if (result.value) {
          // Show loading indicator
          $(document).find(".body-loading").fadeIn("fast", function() {
            $(this).show();
            document.body.style.overflow = "hidden";
          });

          $.ajax({
            type: "delete",
            url: "<?php echo base_url('pembayaran/ajax_set_status/') ?>" + temp.ref_id + "/" + status,
            dataType: "json",
            success: function(response) {
              if (response.status) {
                $("#" + _table).DataTable().ajax.reload(null, false);
                notify(response.data, "success");
              } else {
                notify(response.data, "danger");
              };
            }
          });
        };
      });
    });

    // Handle data delete
    $("#" + _table).on("click", "a.action-delete", function(e) {
      e.preventDefault();
      var temp = table_pembayaran.row($(this).closest('tr')).data();

      swal({
        title: "Are you sure to delete?",
        text: "Once deleted, you will not be able to recover this data!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false
      }).then((result) => {
        if (result.value) {
          $.ajax({
            type: "delete",
            url: "<?php echo base_url('pembayaran/ajax_delete/') ?>" + temp.id,
            dataType: "json",
            success: function(response) {
              if (response.status) {
                $("#" + _table).DataTable().ajax.reload(null, false);
                notify(response.data, "success");
              } else {
                notify(response.data, "danger");
              };
            }
          });
        };
      });
    });

    // END ## Pendaftaran

    // START ## Daftar Ulang

    // Initialize DataTables: Index
    if ($("#" + _table_daftar_ulang)[0]) {
      var table_pembayaran_daftar_ulang = $("#" + _table_daftar_ulang).DataTable({
        processing: true,
        serverSide: true,
        ajax: {
          url: "<?php echo base_url('pembayaran/ajax_get_all_daftar_ulang/') ?>",
          type: "get"
        },
        columns: [{
            data: null,
            render: function(data, type, row, meta) {
              return meta.row + meta.settings._iDisplayStart + 1;
            }
          },
          {
            data: "psb_nisn"
          },
          {
            data: "psb_nama_lengkap"
          },
          {
            data: "description"
          },
          {
            data: "file_name_thumb",
            render: function(data, type, row, meta) {
              let output = '';

              output += '<a href="<?= base_url() ?>' + row.file_name + '" data-fancybox>';
              output += '<img src="<?= base_url() ?>' + data + '" class="img img-responsive img-thumbnail" style="width: 45px; border: 1px solid #999;" />';
              output += '</a>';

              return output;
            }
          },
          {
            data: "telepon",
            render: function(data, type, row, meta) {
              var link = "";
              var isMobile = "<?= $app->is_mobile ?>";

              if (data) {
                if (isMobile) {
                  link = '<a href="http://api.whatsapp.com/send?phone=+62' + data + '&text=Write your message here!" target="_blank">' + data + '</a>'; // mobile
                } else {
                  link = '<a href="http://web.whatsapp.com/send?phone=+62' + data + '&text=Write your message here!" target="_blank">' + data + '</a>'; // desktop
                };
              };

              return link;
            }
          },
          {
            data: "created_at"
          },
          {
            data: null,
            className: "center",
            render: function(data, type, row, meta) {
              let output = '';

              output += '<div class="action">';

              if (parseInt(row.is_verified) === 1) {
                output = '<i class="zmdi zmdi-check-circle"></i> Diterima';
              } else {
                if (parseInt(row.psb_status) === 0) {
                  output += '<a href="javascript:;" class="btn btn-sm btn-success btn-table-action action-set-status spinner-action-button" data-status="1">Konfirmasi dan Aktifkan <div class="spinner-action"></div></a>';
                  output += '&nbsp;';
                  output += '<a href="javascript:;" class="btn btn-sm btn-light btn-table-action action-delete"><i class="zmdi zmdi-delete"></i> Delete</a>';
                } else if (parseInt(row.psb_status) === 3) {
                  output += '<a href="javascript:;" class="btn btn-sm btn-success btn-table-action action-set-status spinner-action-button" data-status="4">Konfirmasi dan Jadikan Santri <div class="spinner-action"></div></a>';
                  output += '&nbsp;';
                  output += '<a href="javascript:;" class="btn btn-sm btn-light btn-table-action action-delete"><i class="zmdi zmdi-delete"></i> Delete</a>';
                } else {
                  output = '<i class="zmdi zmdi-check-circle"></i> Diterima';
                };
              };

              output += '</div>';

              return output;
            }
          }
        ],
        autoWidth: !1,
        responsive: {
          details: {
            renderer: function(api, rowIdx, columns) {
              var hideColumn = [];
              var data = $.map(columns, function(col, i) {
                return ($.inArray(col.columnIndex, hideColumn)) ?
                  '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                  '<td class="dt-details-td">' + col.title + ':' + '</td> ' +
                  '<td class="dt-details-td">' + col.data + '</td>' +
                  '</tr>' :
                  '';
              }).join('');

              return data ? $('<table/>').append(data) : false;
            },
            type: "inline",
            target: 'tr',
          }
        },
        columnDefs: [{
          className: 'desktop',
          targets: [0, 1, 2, 3, 4, 5, 6]
        }, {
          className: 'tablet',
          targets: [0, 1, 2, 5]
        }, {
          className: 'mobile',
          targets: [0, 2]
        }, {
          responsivePriority: 2,
          targets: -1
        }],
        pageLength: 15,
        language: {
          searchPlaceholder: "Search...",
          sProcessing: '<div style="text-align: center;"><div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div></div>'
        },
        sDom: '<"dataTables_ct"><"dataTables__top"fb>rt<"dataTables__bottom"ip><"clear">',
        buttons: [{
          extend: "excelHtml5",
          title: "Export Result"
        }, {
          extend: "print",
          title: "Export Result"
        }],
        initComplete: function(a, b) {
          $(this).closest(".dataTables_wrapper").find(".dataTables__top").prepend(
            '<div class="dataTables_buttons hidden-sm-down actions">' +
            '<span class="actions__item zmdi zmdi-refresh" data-table-action="reload" title="Reload" />' +
            '</div>'
          );
        },
        oSearch: {
          sSearch: _p_nisn
        }
      });

      $(".dataTables_filter input[type=search]").focus(function() {
        $(this).closest(".dataTables_filter").addClass("dataTables_filter--toggled")
      });

      $(".dataTables_filter input[type=search]").blur(function() {
        $(this).closest(".dataTables_filter").removeClass("dataTables_filter--toggled")
      });

      $("body").on("click", "[data-table-action]", function(a) {
        a.preventDefault();
        var b = $(this).data("table-action");
        if ("reload" === b) {
          $("#" + _table_daftar_ulang).DataTable().ajax.reload(null, false);
        };
      });
    };

    // Handle set status
    $("#" + _table_daftar_ulang).on("click", "a.action-set-status", function(e) {
      e.preventDefault();
      var temp = table_pembayaran_daftar_ulang.row($(this).closest('tr')).data();
      var status = parseInt($(this).attr("data-status"));
      var confirmText = '';

      if (status === 1) {
        confirmText = 'Terima pembayaran dan ubah status calon santri menjadi "Sudah Bayar, Aktif".';
      } else if (status === 4) {
        confirmText = 'Terima pembayaran dan ubah status calon santri menjadi "Santri".';
      };

      swal({
        title: "Konfirmasi?",
        text: confirmText,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false
      }).then((result) => {
        if (result.value) {
          // Show loading indicator
          $(document).find(".body-loading").fadeIn("fast", function() {
            $(this).show();
            document.body.style.overflow = "hidden";
          });

          $.ajax({
            type: "delete",
            url: "<?php echo base_url('pembayaran/ajax_set_status/') ?>" + temp.ref_id + "/" + status,
            dataType: "json",
            success: function(response) {
              if (response.status) {
                $("#" + _table_daftar_ulang).DataTable().ajax.reload(null, false);
                notify(response.data, "success");
              } else {
                notify(response.data, "danger");
              };
            }
          });
        };
      });
    });

    // Handle data delete
    $("#" + _table_daftar_ulang).on("click", "a.action-delete", function(e) {
      e.preventDefault();
      var temp = table_pembayaran_daftar_ulang.row($(this).closest('tr')).data();

      swal({
        title: "Are you sure to delete?",
        text: "Once deleted, you will not be able to recover this data!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false
      }).then((result) => {
        if (result.value) {
          $.ajax({
            type: "delete",
            url: "<?php echo base_url('pembayaran/ajax_delete/') ?>" + temp.id,
            dataType: "json",
            success: function(response) {
              if (response.status) {
                $("#" + _table_daftar_ulang).DataTable().ajax.reload(null, false);
                notify(response.data, "success");
              } else {
                notify(response.data, "danger");
              };
            }
          });
        };
      });
    });

    // END ## Daftar Ulang

    // Handle submit upload
    $("#" + _form_upload + " .page-action-save").on("click", function(e) {
      e.preventDefault();

      swal({
        title: "Confirm",
        text: "Are you sure to submit the data?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#39bbb0',
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false
      }).then((result) => {
        if (result.value) {
          var form = $("#" + _form_upload)[0];
          var data = new FormData(form);

          $.ajax({
            type: "post",
            url: "<?php echo base_url('pembayaran/ajax_save_upload') ?>",
            data: data,
            dataType: "json",
            enctype: "multipart/form-data",
            processData: false,
            contentType: false,
            cache: false,
            success: function(response) {
              if (response.status === true) {
                swal({
                  title: "Success",
                  text: response.data,
                  type: "success",
                  showCancelButton: false,
                  confirmButtonColor: '#39bbb0',
                  confirmButtonText: "OK",
                  closeOnConfirm: false
                }).then((result) => {
                  window.location.href = "";
                });
              } else {
                notify(response.data, "danger");
              };
            }
          });
        };
      });
    });

    // Handle filter submit
    $("#" + _section + " .page-action-filter").on("click", function() {
      _filterDraw = true;
      _filterYear = $(".filter-year").val();
      table_pembayaran.ajax.url("<?= base_url('pembayaran/ajax_get_all/') ?>" + _filterYear).load();
      table_pembayaran_daftar_ulang.ajax.url("<?= base_url('pembayaran/ajax_get_all_daftar_ulang/') ?>" + _filterYear).load();
    });

    // Handle upload
    $(document.body).on("change", ".pembayaran-file_name", function() {
      readUploadInlineURL(this);
    });

  });
</script>