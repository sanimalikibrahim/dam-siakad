<?php
$activeTab = isset($_GET['tab']) && !empty($_GET['tab']) ? $_GET['tab'] : 'pendaftaran';
$activeTab = in_array($activeTab, ['pendaftaran', 'daftar_ulang']) ? $activeTab : 'pendaftaran';
?>

<style type="text/css">
    .dataTables_wrapper {
        margin-top: 0;
    }
</style>

<div class="body-loading">
    <div class="body-loading-content">
        <div class="card">
            <div class="card-body">
                <i class="zmdi zmdi-spinner zmdi-hc-spin"></i>
                Please wait...
                <div class="mb-2"></div>
                <span style="color: #9c9c9c; font-size: 1rem;">Don't close this tab activity!</span>
            </div>
        </div>
    </div>
</div>

<section id="pembayaran">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">
                <?php echo (isset($card_title)) ? $card_title : '' ?>
            </h4>
            <h6 class="card-subtitle"><?php echo (isset($card_subTitle)) ? $card_subTitle : '' ?></h6>

            <div class="action-buttons border-bottom border-danger pb-2 mb-2">
                <div class="table-action row">
                    <div class="buttons col">
                        <div class="input-group mb-0">
                            <div class="input-group-prepend">
                                <label class="input-group-text" style="height: 34.13px; background: #f2f2f2;">Filter</label>
                            </div>
                            <select class="custom-select filter-year" style="height: 34.13px; max-width: 150px;">
                                <?= $list_tahun ?>
                            </select>
                            <div class="input-group-apend">
                                <button class="btn btn--raised btn-primary btn--icon-text page-action-filter" style="height: 34.13px;">
                                    <i class="zmdi zmdi-filter-list"></i> Apply
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Temporary -->
            <input type="hidden" name="p-nisn" class="p-nisn" value="<?= $p_nisn ?>" readonly />

            <div class="tab-container">
                <ul class="nav nav-tabs nav-responsive" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link pembayaran-nav-pendaftaran <?= $activeTab === 'pendaftaran' ? 'active' : '' ?>" data-toggle="tab" href="#pembayaran-pendaftaran" role="tab">Pendaftaran</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link pembayaran-nav-daftar_ulang <?= $activeTab === 'daftar_ulang' ? 'active' : '' ?>" data-toggle="tab" href="#pembayaran-daftar_ulang" role="tab">Daftar Ulang</a>
                    </li>
                </ul>
                <div class="tab-content clear-tab-content">
                    <!-- Pendaftaran -->
                    <div class="tab-pane fade show <?= $activeTab === 'pendaftaran' ? 'active' : '' ?>" id="pembayaran-pendaftaran" role="tabpanel">
                        <div class="table-responsive">
                            <table id="table-pembayaran" class="table table-bordered">
                                <thead class="thead-default">
                                    <tr>
                                        <th width="100">No</th>
                                        <th>NISN</th>
                                        <th>Nama Lengkap</th>
                                        <th>Deskripsi</th>
                                        <th>File</th>
                                        <th>Telepon</th>
                                        <th>Created</th>
                                        <th width="250">Action</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <!-- Daftar Ulang -->
                    <div class="tab-pane fade show <?= $activeTab === 'daftar_ulang' ? 'active' : '' ?>" id="pembayaran-daftar_ulang" role="tabpanel">
                        <div class="table-responsive">
                            <table id="table-pembayaran-daftar_ulang" class="table table-bordered">
                                <thead class="thead-default">
                                    <tr>
                                        <th width="100">No</th>
                                        <th>NISN</th>
                                        <th>Nama Lengkap</th>
                                        <th>Deskripsi</th>
                                        <th>File</th>
                                        <th>Telepon</th>
                                        <th>Created</th>
                                        <th width="250">Action</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>