<style type="text/css">
    .img-home {
        margin-top: 1rem;
    }

    @media only screen and (max-width: 768px) {
        .img-home {
            margin-bottom: 2rem;
            width: 100%;
        }
    }
</style>

<section id="pembayaran">
    <div class="card">
        <div class="card-body">

            <?php if ((in_array($psb_status, [0]) && count($current_doc) === 0) || (in_array($psb_status, [3]) && count($current_doc2) === 0)) : ?>
                <form id="form-upload" enctype="multipart/form-data" autocomplete="off">
                    <!-- CSRF -->
                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />

                    <div class="row">
                        <div class="col-xs-10 col-md-10">
                            <h4 class="card-title"><?php echo (isset($card_title)) ? $card_title : '' ?></h4>
                            <h6 class="card-subtitle"><?php echo (isset($card_subTitle)) ? $card_subTitle : '' ?></h6>
                            <div class="clear-card"></div>
                        </div>
                    </div>
                    <div class="clear-card"></div>

                    <div class="row">
                        <div class="col-md-5 col-12">
                            <div class="alert alert-warning mb-4">
                                <h4 class="alert-heading">Penting!</h4>
                                Anda tidak dapat mengubah data yang sudah dikirim, jadi pastikan semua data sudah benar sebelum melakukan submit.
                            </div>

                            <input type="hidden" name="ref" value="<?= $doc_ref ?>" readonly required />
                            <input type="hidden" name="description" class="form-control" value="<?= $doc_description ?>" readonly required />

                            <div class="form-group">
                                <label required><?= $doc_description ?></label>
                                <div class="upload-inline">
                                    <div class="upload-button">
                                        <input type="file" name="file_name" class="upload-pure-button pembayaran-file_name" accept="imaage/jpg,image/jpeg,image/png,image/gif" />
                                    </div>
                                    <div class="upload-preview"></div>
                                </div>
                                <small class="form-text text-muted">
                                    Use file with format .JPG, .JPEG, .PNG or .GIF
                                </small>
                            </div>
                        </div>
                    </div>

                    <small class="form-text text-muted">
                        Fields with red stars (<label required></label>) are required.
                    </small>

                    <div class="row" style="margin-top: 2rem;">
                        <div class="col col-md-3 col-lg-2">
                            <button class="btn btn--raised btn-primary btn--icon-text btn-block page-action-save spinner-action-button">
                                Submit
                                <div class="spinner-action"></div>
                            </button>
                        </div>
                    </div>
                </form>
                <?php if (in_array($psb_status, [3])) : ?>
                    <div class="mb-5"></div>
                <?php endif; ?>
            <?php endif ?>

            <?php if (count($current_doc) > 0) : ?>
                <?php if (!is_null($informasi_bukti_pembayaran)) : ?>
                    <div class="alert alert-light text-dark">
                        <?= $informasi_bukti_pembayaran->konten ?>
                    </div>
                <?php endif ?>
                <div class="table-responsive">
                    <?php $nomor = 1; ?>
                    <?php foreach ($current_doc as $index => $item) : ?>
                        <table class="table table-sm">
                            <thead>
                                <tr>
                                    <th scope="col" colspan="3">
                                        <?php
                                        $isVerified = $item->is_verified;
                                        $badgeClass = (int) $isVerified === 1 ? 'success' : 'warning';
                                        $badgeLabel = (int) $isVerified === 1 ? 'Sudah Verifikasi' : 'Menunggu Verifikasi';

                                        echo '<span class="badge badge-dark">' . $nomor++ . '</span> ';

                                        if (!is_null($isVerified)) {
                                            echo '<span class="badge badge-' . $badgeClass . '">' . $badgeLabel . '</span>';
                                        };
                                        ?>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td width="110">Description</td>
                                    <td width="10">:</td>
                                    <td><?= $item->description ?></td>
                                </tr>
                                <tr>
                                    <td width="110">Upload at</td>
                                    <td width="10">:</td>
                                    <td><?= $item->created_at ?></td>
                                </tr>
                                <tr>
                                    <td width="110">File</td>
                                    <td width="10">:</td>
                                    <td>
                                        <a href="<?= base_url($item->file_name) ?>" data-fancybox>
                                            <img src="<?= base_url($item->file_name_thumb) ?>" class="img img-responsive img-thumbnail" style="width: 150px;" />
                                        </a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    <?php endforeach ?>
                </div>
            <?php endif ?>

        </div>
    </div>
</section>