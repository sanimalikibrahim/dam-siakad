<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . 'controllers/AppBackend.php');
require_once(APPPATH . 'controllers/ApiClient.php');

class Pembayaran extends AppBackend
{
  function __construct()
  {
    parent::__construct();
    $this->load->model([
      'AppModel',
      'PsbModel',
      'DocumentModel',
      'PostModel'
    ]);
  }

  private function _sendMail($psb, $notifDesc, $notifSubject)
  {
    $app = $this->app();

    $template = '
      <div style="background: #fff; padding: 1.5rem; border: 4px solid #f2f2f2;">
        <h4>Assalamualaikum, ' . $psb->nama_lengkap . '</h4>
        <p>' . $notifDesc . '</p>
        <hr />
        <i>Panitia PSB</i> <br/>
        <i>Pondok Pesantren Darul Arqam Muhammadiyah Garut</i>
      </div>
    ';

    $mailParams = array(
      'receiver' => $psb->email,
      'subject' => $notifSubject . ' | ' . $app->app_name,
      'message' => $template
    );

    return $this->sendMail($mailParams);
  }

  private function _sendNotifMail($id, $status, $psb)
  {
    // Ucapan Selamat Untuk Santri Baru
    $info_ucapanSelamat = $this->PostModel->getDetail(['id' => 9]);
    $info_ucapanSelamat = !is_null($info_ucapanSelamat) ? $info_ucapanSelamat->konten : '';
    $info_ucapanSelamat = str_replace('{base_url}', base_url(), $info_ucapanSelamat);

    switch ((int) $status) {
      case 1:
        $notifSubject = 'Pembayaran Pendaftaran Diterima';
        $notifRef = 'psb_buktibayar_pendaftaran';
        $notifDesc = 'Terima kasih, pembayaran pendaftaran ananda telah kami terima.';
        $notifDescMail = $notifDesc;
        $notifLink = 'profile/calon-santri';
        break;
      case 4:
        $notifSubject = 'Pembayaran Daftar Ulang Diterima';
        $notifRef = 'psb_buktibayar_daftarulang';
        $notifDesc = 'Selamat! Ananda saat ini sudah menjadi Santri.';
        $notifDescMail = $info_ucapanSelamat;
        $notifLink = 'profile/calon-santri';
        break;
      default:
        $notifSubject = 'Panitia PSB';
        $notifRef = 'psb';
        $notifDesc = 'Undefined';
        $notifDescMail = $notifDesc;
        $notifLink = 'profile/calon-santri';
        break;
    };

    // Send mail
    $this->_sendMail($psb, $notifDescMail, $notifSubject);

    // Notification
    $notifParams = array(
      'user_from' => $this->session->userdata('user')['id'],
      'user_to' => $id,
      'ref' => $notifRef,
      'ref_id' => $id,
      'description' => $notifDesc,
      'link' => $notifLink
    );
    $this->set_notification($notifParams);
    // END ## Notification
  }

  private function _storeToLearndash($user = null)
  {
    $apiClient = new ApiClient();
    $response = $apiClient->storeUserToLearndash($user->learndash_user_id, [
      'username' => $user->username,
      'password' => (!empty($user->learndash_user_ref) && !is_null($user->learndash_user_ref)) ? $user->learndash_user_ref : $user->username,
      'email' => $user->email,
      'name' => $user->nama_lengkap,
      'first_name' => $user->nama_lengkap,
      'last_name' => ''
    ]);

    return $response;
  }

  private function _searchUserFromLearndash($email = null)
  {
    $apiClient = new ApiClient();
    $response = $apiClient->getUserSearch($email);

    return $response;
  }

  private function _deleteFromLearndash($wpUserId = null)
  {
    $apiClient = new ApiClient();
    $response = $apiClient->deleteUser($wpUserId);

    return $response;
  }

  public function index()
  {
    $data = array(
      'app' => $this->app(),
      'main_js' => $this->load_main_js('pembayaran', false, [
        'app' => $this->app()
      ]),
      'card_title' => 'Pembayaran',
      'p_nisn' => $this->input->get('nisn'),
      'list_tahun' => $this->init_list($this->PsbModel->getTahunList(), 'tahun', 'tahun'),
    );
    $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
    $this->template->load_view('index', $data, TRUE);
    $this->template->render();
  }

  public function ajax_get_all($year = null)
  {
    $this->handle_ajax_request();
    $dtAjax_config = array(
      'table_name' => 'view_psb_bukti_bayar',
      'static_conditional' => array(
        'LOWER(description)' => strtolower('Bukti Bayar Pendaftaran'),
        'tahun' => $year
      ),
      'order_column' => 0,
      'order_column_dir' => 'asc'
    );
    $response = $this->AppModel->getData_dtAjax($dtAjax_config);
    echo json_encode($response);
  }

  public function ajax_get_all_daftar_ulang($year = null)
  {
    $this->handle_ajax_request();
    $dtAjax_config = array(
      'table_name' => 'view_psb_bukti_bayar',
      'static_conditional' => array(
        'LOWER(description)' => strtolower('Bukti Bayar Daftar Ulang'),
        'tahun' => $year
      ),
      'order_column' => 0,
      'order_column_dir' => 'asc'
    );
    $response = $this->AppModel->getData_dtAjax($dtAjax_config);
    echo json_encode($response);
  }

  public function ajax_set_status($id, $status)
  {
    $this->handle_ajax_request();
    $psb = $this->PsbModel->getDetail(['id' => $id]);
    $user = $this->UserModel->getDetail(['id' => $id]);

    if (!is_null($psb)) {
      $syncStatus = true;

      // Delete user from learndash-wp if exist
      if ((int) $status === 0) {
        $wpUserIdLokal = $user->learndash_user_id;

        if (!is_null($wpUserIdLokal) && !empty($wpUserIdLokal)) {
          $this->_deleteFromLearndash($wpUserIdLokal);
        } else {
          $wpUserTemp = $this->_searchUserFromLearndash($user->email);
          $wpUserId = (isset($wpUserTemp['data']) && isset($wpUserTemp['data'][0])) ? $wpUserTemp['data'][0]->id : null;

          if (!is_null($wpUserId)) {
            $this->_deleteFromLearndash($wpUserId);
          };
        };
      };
      // END ## Delete user from learndash-wp if exist

      // Store to learndash-wp if payment has been received: Bukti Bayar Pendaftaran
      if ((int) $status === 1) {
        // Delete user when exists
        $wpUserTemp = $this->_searchUserFromLearndash($user->email);
        $wpUserId = (isset($wpUserTemp['data']) && isset($wpUserTemp['data'][0])) ? $wpUserTemp['data'][0]->id : null;

        if (!is_null($wpUserId)) {
          $this->_deleteFromLearndash($wpUserId);
        };
        // END ## Delete user when exists

        $storeToLearndash = $this->_storeToLearndash($user);

        if ($storeToLearndash['status'] === true) {
          $syncStatus = true;

          // Update meta in user
          $learndashUserId = (isset($storeToLearndash['data']) && isset($storeToLearndash['data']->id)) ? $storeToLearndash['data']->id : null;
          $this->UserModel->setLearndashId($id, $learndashUserId);
        } else {
          $syncStatus = false;
          $response = $storeToLearndash;
        };
      };
      // END ## Store to learndash-wp if payment has been received: Bukti Bayar Pendaftaran

      if ($syncStatus === true) {
        $setStatus = $this->PsbModel->setStatus($id, $status);
        $this->DocumentModel->setIsVerifiedByRefId($id, 1);

        if ($setStatus['status'] === true) {
          $this->_sendNotifMail($id, $status, $psb);
          $response = $setStatus;
        } else {
          $response = $setStatus;
        };
      };
    } else {
      $response = ['status' => false, 'data' => 'Calon santri dengan ID: ' . $id . ' tidak ditemukan.'];
    };

    echo json_encode($response);
  }

  public function upload()
  {
    $role = $this->session->userdata('user')['role'];
    $userId = $this->session->userdata('user')['id'];

    if (in_array($role, ['Calon Santri'])) {
      $psb = $this->PsbModel->getDetail(['id' => $userId]);
      $docRef = '';
      $docDescription = '';

      if (!is_null($psb)) {
        $psbStatus = (int) $psb->status;
        $currentBuktiBayar = $this->DocumentModel->getAll(['created_by' => $userId], 'ref', ['psb_buktibayar_pendaftaran', 'psb_buktibayar_daftarulang']);
        $currentBuktiBayar2 = $this->DocumentModel->getAll(['created_by' => $userId], 'ref', ['psb_buktibayar_daftarulang']);
        $informasiBuktiPembayaran = $this->PostModel->getDetail(['id' => 5]);

        if (in_array($psbStatus, [0, 3]) || count($currentBuktiBayar) > 0) {
          switch ($psbStatus) {
            case 0:
              $docRef = 'psb_buktibayar_pendaftaran';
              $docDescription = 'Bukti Bayar Pendaftaran';
              break;
            case 3:
              $docRef = 'psb_buktibayar_daftarulang';
              $docDescription = 'Bukti Bayar Daftar Ulang';
              break;
            default:
              break;
          };

          $data = array(
            'app' => $this->app(),
            'main_js' => $this->load_main_js('pembayaran'),
            'card_title' => 'Pembayaran › Upload',
            'psb_status' => $psbStatus,
            'doc_ref' => $docRef,
            'doc_description' => $docDescription,
            'current_doc' => $currentBuktiBayar,
            'current_doc2' => $currentBuktiBayar2,
            'informasi_bukti_pembayaran' => $informasiBuktiPembayaran
          );
          $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
          $this->template->load_view('upload', $data, TRUE);
          $this->template->render();
        } else {
          show_error('Anda hanya dapat menggunakan fitur ini jika berstatus "<b>Belum Bayar, Nonaktif</b>" atau "<b>Lulus</b>".');
        };
      } else {
        show_404();
      };
    } else {
      show_error('Hanya calon santri yang dapat menggunakan fitur ini.');
    };
  }

  public function ajax_save_upload()
  {
    $this->handle_ajax_request();

    if (!empty($_FILES['file_name']['name'])) {
      $cpUpload = new CpUpload();

      $directory = 'pembayaran';
      $upload = $cpUpload->run('file_name', $directory, true, true);

      if ($upload->status === true) {
        $post = array(
          'ref' => $this->input->post('ref'),
          'ref_id' => $this->session->userdata('user')['id'],
          'description' => $this->input->post('description'),
          'file_raw_name' => $upload->data->raw_name . $upload->data->file_ext,
          'file_raw_name_thumb' => ($upload->data->is_image) ? $upload->data->raw_name . '_thumb' . $upload->data->file_ext : null,
          'file_name' => $upload->data->base_path,
          'file_name_thumb' => ($upload->data->is_image) ? 'directory/' . $directory . '/' . $upload->data->raw_name . '_thumb' . $upload->data->file_ext : null,
          'file_size' => $upload->data->file_size,
          'file_type' => $upload->data->file_type,
          'file_ext' => $upload->data->file_ext
        );
        $transaction = $this->DocumentModel->insert($post);

        if ($transaction['status'] === true) {
          // Notification
          $userId = $this->session->userdata('user')['id'];
          $psb = $this->PsbModel->getDetail(['id' => $userId]);

          $notifParams = array(
            'user_from' => $userId,
            'ref' => $this->input->post('ref'),
            'ref_id' => $userId,
            'description' => '<span style="color: #2196F3;">' . $psb->nama_lengkap . '</span> mengirim bukti pembayaran.',
            'link' => 'pembayaran/?nisn=' . $psb->nisn . '&source=notification'
          );
          $this->set_notification($notifParams, 'Panitia PSB');
          // END ## Notification
        };

        echo json_encode($transaction);
      } else {
        echo json_encode(array('status' => false, 'data' => $upload->data));
      };
    } else {
      echo json_encode(array('status' => false, 'data' => 'No file to upload.'));
    };
  }

  public function ajax_delete($id)
  {
    $this->handle_ajax_request();
    echo json_encode($this->DocumentModel->delete($id));
  }
}
