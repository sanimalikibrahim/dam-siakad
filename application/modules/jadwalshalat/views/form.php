<div class="modal fade" id="modal-form-jadwalshalat" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title pull-left">Jadwal Shalat</h5>
      </div>
      <div class="spinner">
        <div class="lds-hourglass"></div>
      </div>
      <div class="modal-body">
        <form id="form-jadwalshalat" autocomplete="off">
          <!-- CSRF -->
          <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />

          <div class="form-group">
            <label required>Jenis</label>
            <div class="select">
              <select name="jenis" class="form-control jadwalshalat-jenis" data-placeholder="Select &#8595;">
                <option disabled selected>Select &#8595;</option>
                <option value="Wajib">Wajib</option>
                <option value="Sunat">Sunat</option>
              </select>
              <i class="form-group__bar"></i>
            </div>
          </div>

          <div class="form-group">
            <label required>Nama</label>
            <input type="text" name="nama" class="form-control jadwalshalat-nama" maxlength="50" placeholder="Nama" required />
            <i class="form-group__bar"></i>
          </div>

          <div class="form-group">
            <label required>Mulai / Selesai</label>
            <div class="buttons">
              <div class="input-group mb-0">
                <input type="text" name="mulai" class="form-control no-padding-l flatpickr-timeonly jadwalshalat-mulai" placeholder="Mulai" required />
                <i class="form-group__bar"></i>
                <input type="text" name="selesai" class="form-control no-padding-l flatpickr-timeonly jadwalshalat-selesai" placeholder="Selesai" required />
                <i class="form-group__bar"></i>
              </div>
            </div>
          </div>

          <small class="form-text text-muted">
            Fields with red stars (<label required></label>) are required.
          </small>

        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success btn--icon-text jadwalshalat-action-save">
          <i class="zmdi zmdi-save"></i> Save
        </button>
        <button type="button" class="btn btn-light btn--icon-text jadwalshalat-action-cancel" data-dismiss="modal">
          Cancel
        </button>
      </div>
    </div>
  </div>
</div>