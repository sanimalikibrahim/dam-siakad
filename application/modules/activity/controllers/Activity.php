<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . 'controllers/AppBackend.php');

class Activity extends AppBackend
{
  private $_list_per_page = 16;

  function __construct()
  {
    parent::__construct();
    $this->load->model([
      'AppModel',
      'KegiatanModel',
      'KegiatanresultModel',
      'JadwalshalatModel'
    ]);
    $this->load->library('form_validation');
  }

  public function index()
  {
    $santri_id = $this->session->userdata('user')['id'];
    $kegiatan_result_today = $this->KegiatanresultModel->getDetail_list(['created_by' => $santri_id, 'DATE(created_at)' => date('Y-m-d')]);
    $kegiatan_result_list = $this->KegiatanresultModel->getAll_list(['created_by' => $santri_id], $this->_list_per_page, 0);
    $kegiatan_result_list_count = $this->KegiatanresultModel->getAll_list_count(['created_by' => $santri_id]);

    if (is_null($kegiatan_result_today)) {
      // Dummy for today
      $kegiatan_result_today = (object) [
        'created_at' => date('Y-m-d H:i:s'),
        'tahun' => date('Y'),
        'bulan' => date('m'),
        'hari' => date('d'),
        'nama_hari' => date('D'),
        'persentase' => 0
      ];
    };

    $data = array(
      'app' => $this->app(),
      'main_js' => $this->load_main_js('activity'),
      'page_title' => 'Catatan Kegiatan',
      'controller' => $this,
      'data_kegiatan_result_today' => $kegiatan_result_today,
      'data_kegiatan_result_list' => $kegiatan_result_list,
      'data_kegiatan_result_list_count' => $kegiatan_result_list_count
    );
    $this->template->set('title', $data['page_title'] . ' | ' . $data['app']->app_name, TRUE);
    $this->template->load_view('index', $data, TRUE);
    $this->template->render();
  }

  public function detail()
  {
    $date = $this->input->get('date');
    $date_result = (!empty($date) && is_numeric($date)) ? date('Y-m-d', $date) : null;
    $santri_id = $this->session->userdata('user')['id'];

    if ($this->validate_date($date_result)) {
      $kegiatan_result_list = $this->KegiatanresultModel->getDetail_list(['created_by' => $santri_id, 'DATE(created_at)' => $date_result]);
      $jadwal_shalat = $this->JadwalshalatModel->getAll_list();

      if ($date_result === date('Y-m-d') && is_null($kegiatan_result_list)) {
        // Dummy for today
        $kegiatan_result_list = (object) [
          'created_at' => date('Y-m-d H:i:s'),
          'tahun' => date('Y'),
          'bulan' => date('m'),
          'hari' => date('d'),
          'nama_hari' => date('D'),
          'persentase' => 0
        ];
      };

      if (!is_null($kegiatan_result_list)) {
        $data = array(
          'app' => $this->app(),
          'main_js' => $this->load_main_js('activity'),
          'page_title' => 'Rincian Catatan Kegiatan',
          'controller' => $this,
          'date_result' => $date_result,
          'data_kegiatan' => $this->KegiatanModel->getAll_fullItem($date_result, $santri_id),
          'data_result_list' => $kegiatan_result_list,
          'data_jadwal_shalat' => $jadwal_shalat
        );
        $this->template->set('title', $data['page_title'] . ' | ' . $data['app']->app_name, TRUE);
        $this->template->load_view('detail', $data, TRUE);
        $this->template->render();
      } else {
        redirect(base_url('activity'));
      };
    } else {
      redirect(base_url('activity'));
    };
  }

  public function ajax_load_more($offset = null)
  {
    $this->handle_ajax_request();

    if (!is_null($offset) && !empty($offset)) {
      $santri_id = $this->session->userdata('user')['id'];
      $kegiatan_result_list = $this->KegiatanresultModel->getAll_list(['created_by' => $santri_id], $this->_list_per_page, $offset);

      // Response DOM
      $dom = '';
      if (count($kegiatan_result_list) > 0) {
        foreach ($kegiatan_result_list as $key => $item) {
          $dom .= '
            <div class="col-md-3">
              <a href="' . base_url('activity/detail/?date=' . strtotime(date('Y-m-d', strtotime($item->created_at)))) . '">
                <div class="card activity-link">
                  <div class="card-body item-body">
                    <div class="activity">
                      <div class="left">
                        <div class="day">
                          ' . $this->get_day($item->nama_hari) . '
                        </div>
                        <div class="date">
                          ' . $item->hari . ' ' . $this->get_month($item->bulan) . ' ' . $item->tahun . '
                        </div>
                      </div>
                      <div class="right">
                        <div class="chart">
                            <div class="easy-pie-chart" data-percent="' . (int) $item->persentase . '" data-size="60" data-track-color="#eee" data-bar-color="#607D8B">
                              <span class="easy-pie-chart__value">' . (int) $item->persentase . '</span>
                            </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </a>
            </div>
          ';
        };
      };

      echo json_encode(['status' => true, 'data' => $dom, 'count' => count($kegiatan_result_list)]);
    } else {
      echo json_encode(['status' => false, 'data' => 'Failed to fetch more data.', 'count' => 0]);
    };
  }

  public function ajax_save()
  {
    $this->handle_ajax_request();

    $today = date('Y-m-d');
    $santri_id = $this->session->userdata('user')['id'];
    $kegiatan_item = $this->input->post('kegiatan_item');
    $data = [];

    if (!is_null($kegiatan_item) && count($kegiatan_item) > 0) {
      // Delete existing
      $this->KegiatanresultModel->delete(['santri_id' => $santri_id, 'DATE(created_at)' => $today]);

      // Store data POST
      if (!is_null($kegiatan_item) && count($kegiatan_item) > 0) {
        foreach ($kegiatan_item as $key => $item) {
          if (!empty($item)) {
            $data[] = array(
              'kegiatan_item_id' => $key,
              'santri_id' => $santri_id,
              'jawaban' => $item,
              'created_by' => $santri_id
            );
          };
        };
      };

      // Store data FILE
      $file = $this->_save_attachment();
      $file_data = ($file['status']) ? $file['data'] : [];

      // Merge data POST and FILE
      $data = array_merge($data, $file_data);

      // Insert new data
      echo json_encode($this->KegiatanresultModel->insertBatch($data));
    } else {
      echo json_encode(array('status' => false, 'data' => 'No data to submit.'));
    };
  }

  private function _save_attachment()
  {
    $this->handle_ajax_request();

    if (!empty($_FILES['kegiatan_item']['name'])) {
      $cpUpload = new CpUpload();
      $files = $cpUpload->re_arrange($_FILES['kegiatan_item']);
      $post = array();
      $error = '';
      $directory = 'activity';
      $santri_id = $this->session->userdata('user')['id'];

      foreach ($files as $key => $item) {
        if (!empty($item['name'])) {
          $upload = $cpUpload->run($item, $directory, true, true, 'jpg|jpeg|png|gif|3gp|mp4', true);

          if ($upload->status === true) {
            $post[] = array(
              'kegiatan_item_id' => $key,
              'santri_id' => $santri_id,
              'jawaban' => $upload->data->base_path,
              'created_by' => $santri_id
            );
          } else {
            $error .= $upload->data;
          };
        };
      };

      if (empty($error) && count($post) > 0) {
        return array('status' => true, 'data' => $post);
      } else {
        return array('status' => false, 'data' => []);
      };
    } else {
      return array('status' => true, 'data' => []);
    };
  }
}
