<script type="text/javascript">
  $(document).ready(function() {

    var _form = "form-activity";
    var _offset = 0;
    var _per_page = 16;

    // Handle data submit
    $("#" + _form + " .btn-submit").on("click", function(e) {
      e.preventDefault();

      swal({
        title: "Konfirmasi",
        text: "Ananda yakin semua data yang akan disubmit sudah benar dan dapat di pertanggung jawabkan?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#39bbb0',
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false
      }).then((result) => {
        if (result.value) {
          var form = $("#" + _form)[0];
          var data = new FormData(form);

          $.ajax({
            type: "post",
            url: "<?php echo base_url('activity/ajax_save/') ?>",
            data: data,
            dataType: "json",
            enctype: "multipart/form-data",
            processData: false,
            contentType: false,
            cache: false,
            success: function(response) {
              if (response.status === true) {
                // notify(response.data, "success");
                swal({
                  title: "Sukses",
                  text: "Muat ulang halaman untuk menyegarkan halaman?",
                  type: "success",
                  showCancelButton: true,
                  confirmButtonColor: '#39bbb0',
                  confirmButtonText: "Yes",
                  cancelButtonText: "No",
                  closeOnConfirm: false
                }).then((result) => {
                  if (result.value) {
                    window.location.href = "";
                  };
                });
              } else {
                notify(response.data, "danger");
              };
            }
          });
        };
      });
    });

    // Handle tab change
    $(".activity-tab-control").on("change", function() {
      var tab_class = $(this).val();
      $("." + tab_class).click();
    });

    // Handle load more
    $("#loadmore-activity").on("click", function() {
      _offset = _offset + _per_page;

      $.ajax({
        type: "get",
        url: "<?php echo base_url('activity/ajax_load_more/') ?>" + _offset,
        dataType: "json",
        success: function(response) {
          if (response.status === true) {
            if (response.count > 0) {
              $("#activity-lists").append(response.data);

              // Init easy-pie-chart
              $('.easy-pie-chart').each(function() {
                var value = $(this).data('value');
                var size = $(this).data('size');
                var trackColor = $(this).data('track-color');
                var barColor = $(this).data('bar-color');

                $(this).find('.easy-pie-chart__value').css({
                  lineHeight: (size) + 'px',
                  fontSize: (size / 4) + 'px',
                  color: barColor
                });

                $(this).easyPieChart({
                  easing: 'easeOutBounce',
                  barColor: barColor,
                  trackColor: trackColor,
                  scaleColor: 'rgba(0,0,0,0)',
                  lineCap: 'round',
                  lineWidth: 2,
                  size: size,
                  animate: 3000,
                  onStep: function(from, to, percent) {
                    $(this.el).find('.percent').text(Math.round(percent));
                  }
                })
              });
            } else {
              $(".loadmore-activity-wrap").hide();
            };
          } else {
            notify(response.data, "danger");
          };
        }
      });
    });

  });
</script>