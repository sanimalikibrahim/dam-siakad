<style type="text/css">
    .activity {
        display: flex;
        color: #747a80;
    }

    .activity .left {
        display: flex;
        flex: 1;
        flex-direction: column;
        justify-content: center;
    }

    .activity .right {
        display: flex;
        flex: 1;
        justify-content: flex-end;
    }

    .activity .day {
        font-size: 11pt;
        font-weight: 500;
        margin-bottom: 4px;
    }

    .activity .date {
        font-size: 10pt;
        font-weight: 300;
    }

    .activity .pie-chart {
        width: 60px;
        height: 60px;
    }

    .activity-link {
        transition: 0.2s;
        margin-bottom: <?= ($app->is_mobile) ? '1rem' : '2.3rem' ?>;
    }

    .activity-link:hover {
        box-shadow: 0 1px 15px rgba(0, 0, 0, .075);
        transition: 0.2s;
    }

    .item-body {
        padding: <?= $app->is_mobile ? '1.2rem 2.2rem' : '2.2rem' ?>;
    }
</style>

<div id="activity-lists" class="row">
    <!-- Today -->
    <div class="col-md-3">
        <a href="<?= base_url('activity/detail/?date=' . strtotime(date('Y-m-d'))) ?>">
            <div class="card activity-link">
                <div class="card-body item-body">
                    <div class="activity">
                        <div class="left">
                            <div class="day" style="color: #32c787;">
                                Hari Ini, <?= $controller->get_day($data_kegiatan_result_today->nama_hari) ?>
                            </div>
                            <div class="date" style="color: #32c787;">
                                <?= $data_kegiatan_result_today->hari . ' ' . $controller->get_month($data_kegiatan_result_today->bulan) . ' ' . $data_kegiatan_result_today->tahun ?>
                            </div>
                        </div>
                        <div class="right">
                            <div class="chart">
                                <div class="easy-pie-chart" data-percent="<?= (int) $data_kegiatan_result_today->persentase ?>" data-size="60" data-track-color="#eee" data-bar-color="#32c787">
                                    <span class="easy-pie-chart__value"><?= (int) $data_kegiatan_result_today->persentase ?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
    <!-- Other Data -->
    <?php if (count($data_kegiatan_result_list) > 0) : ?>
        <?php foreach ($data_kegiatan_result_list as $key => $item) : ?>
            <?php if (strtotime(date('Y-m-d')) !== strtotime(date('Y-m-d', strtotime($item->created_at)))) : ?>
                <!-- Other day -->
                <div class="col-md-3">
                    <a href="<?= base_url('activity/detail/?date=' . strtotime(date('Y-m-d', strtotime($item->created_at)))) ?>">
                        <div class="card activity-link">
                            <div class="card-body item-body">
                                <div class="activity">
                                    <div class="left">
                                        <div class="day">
                                            <?= $controller->get_day($item->nama_hari) ?>
                                        </div>
                                        <div class="date">
                                            <?= $item->hari . ' ' . $controller->get_month($item->bulan) . ' ' . $item->tahun ?>
                                        </div>
                                    </div>
                                    <div class="right">
                                        <div class="chart">
                                            <div class="easy-pie-chart" data-percent="<?= (int) $item->persentase ?>" data-size="60" data-track-color="#eee" data-bar-color="#607D8B">
                                                <span class="easy-pie-chart__value"><?= (int) $item->persentase ?></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            <?php endif; ?>
        <?php endforeach; ?>
    <?php endif; ?>
    <!-- END ## Other Data -->
</div>

<?php if ($data_kegiatan_result_list_count > 16) : ?>
    <div class="row loadmore-activity-wrap">
        <div class="col text-center">
            <a href="javascript:;" id="loadmore-activity" class="btn btn-success spinner-action-button" style="padding-left: 30px; padding-right: 30px; margin-bottom: 2rem;">Load More</a>
        </div>
    </div>
<?php endif; ?>