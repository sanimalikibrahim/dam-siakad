<style type="text/css">
    .activity {
        display: flex;
        color: #747a80;
    }

    .activity .left {
        display: flex;
        flex: 1;
        flex-direction: column;
        justify-content: center;
    }

    .activity .right {
        display: flex;
        flex: 1;
        justify-content: flex-end;
    }

    .activity .day {
        font-size: 12pt;
        font-weight: 400;
        margin-bottom: 4px;
    }

    .activity .date {
        font-size: 10pt;
        font-weight: 300;
    }

    .activity .pie-chart {
        width: 60px;
        height: 60px;
    }

    .margin-responsive {
        margin-bottom: <?= ($app->is_mobile) ? '1rem' : '1.5rem' ?>;
    }

    .petunjuk {
        color: #f2f2f2;
    }

    .petunjuk-title {
        font-size: 12pt;
        font-weight: 300;
        margin-bottom: 4px;
    }

    .nav-tabs .nav-item.show .nav-link,
    .nav-tabs .nav-link {
        border: 1px solid transparent;
    }

    .nav-tabs .nav-item.show .nav-link,
    .nav-tabs .nav-link.active {
        border: 1px solid #f6f6f6;
    }

    .card-body__title {
        display: flex;
        align-items: center;
        margin-bottom: <?= ($app->is_mobile) ? '.50rem' : '.20rem' ?>;
    }

    .kegiatan_item-judul {
        margin-left: 5px;
    }

    .kegiatan_item-badge {
        align-self: flex-start;
        font-weight: 300;
    }

    .answer-wrap {
        padding-left: 3.1rem;
        margin-bottom: .75rem;
        padding-bottom: .75rem;
        border-bottom: 1px solid #f6f6f6;
    }

    .footer-wrap {
        background-color: #f2f2f2;
        padding: 15px;
        box-shadow: 0 1px 2px rgba(0, 0, 0, .075);
        margin-top: 2rem;
    }

    .footer-wrap .btn-submit {
        padding-left: 30px;
        padding-right: 30px;
    }

    input[type="text"]:disabled {
        background-color: #fff;
    }

    .file-preview {
        margin-top: .75rem;
        margin-left: 3px;
        object-fit: cover;
        width: 120px;
        height: auto;
        border: 1px solid #f9f9f9;
        border-radius: 4px;
    }

    .lightbox>a:before {
        content: none;
    }

    .lightbox>a:after {
        content: none;
    }

    .item-body {
        padding: <?= $app->is_mobile ? '1.2rem 2.2rem' : '2.2rem' ?>;
    }

    .lg-backdrop {
        background-color: rgba(0, 0, 0, 0.8);
    }
</style>

<?php $isToday = strtotime(date('Y-m-d')) === strtotime(date('Y-m-d', strtotime($data_result_list->created_at))) ?>
<?php $timeNow = time() ?>

<div class="table-action margin-responsive">
    <div class="buttons" style="padding-top: 0;">
        <a href="<?php echo base_url('activity') ?>" class="btn btn-secondary">
            <i class="zmdi zmdi-arrow-left"></i> Back to list
        </a>
    </div>
</div>

<div class="row">
    <div class="col">
        <div class="card margin-responsive">
            <div class="card-body item-body">
                <div class="activity">
                    <div class="left">
                        <div class="day" style="<?= ($isToday) ? 'color: #32c787;' : '' ?>">
                            <?= ($isToday) ? 'Hari Ini,' : '' ?>
                            <?= $controller->get_day($data_result_list->nama_hari) ?>
                        </div>
                        <div class="date" style="<?= ($isToday) ? 'color: #32c787;' : '' ?>">
                            <?= $data_result_list->hari . ' ' . $controller->get_month($data_result_list->bulan) . ' ' . $data_result_list->tahun ?>
                        </div>
                    </div>
                    <div class="right">
                        <div class="chart">
                            <div class="easy-pie-chart" data-percent="<?= (int) $data_result_list->persentase ?>" data-size="60" data-track-color="#eee" data-bar-color="<?= ($isToday) ? '#32c787' : '#607D8B' ?>">
                                <span class="easy-pie-chart__value"><?= (int) $data_result_list->persentase ?></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col">
        <div class="card margin-responsive">
            <div class="card-body bg-blue-grey item-body">
                <div class="petunjuk">
                    <div class="petunjuk-title">
                        Petunjuk Pengisian
                    </div>
                    <div class="petunjuk-body">
                        <ol style="padding-left: 15px;">
                            <li>Isi kegiatan anada hanya untuk kegiatan yang dilakukan hari ini saja.</li>
                            <li>Pengisian form shalat wajib hanya dapat dilakukan pada jadwal shalat tersebut.</li>
                            <li>Isi kegiatan harian ananda dengan niat ikhlas secara jujur dan bertanggung jawab.</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="card margin-responsive">
    <div class="card-body">
        <form method="post" id="form-activity" enctype="multipart/form-data" autocomplete="off">
            <!-- CSRF -->
            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
            <input type="hidden" name="date_result" value="<?= $date_result ?>" />

            <?php if (count($data_kegiatan) > 0) : ?>
                <div class="tab-container">
                    <ul class="nav nav-tabs nav-fill" role="tablist" style="<?= ($app->is_mobile) ? 'display: none' : '' ?>">
                        <?php $no = 1; ?>
                        <?php foreach ($data_kegiatan as $key => $item) : ?>
                            <li class="nav-item">
                                <a class="nav-link <?= ($no === 1) ? 'active' : '' ?> activity-tab-<?= $item->slug ?>" data-toggle="tab" href="#activity-<?= $item->slug ?>" role="tab"><?= $item->judul ?></a>
                            </li>
                            <?php $no++; ?>
                        <?php endforeach; ?>
                    </ul>
                    <?php if ($app->is_mobile) : ?>
                        <div class="form-group" style="margin-bottom: 0;">
                            <div class="select">
                                <select class="activity-tab-control form-control">
                                    <?php foreach ($data_kegiatan as $key => $item) : ?>
                                        <option value="activity-tab-<?= $item->slug ?>"><?= $item->judul ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    <?php endif; ?>
                    <div class="tab-content clear-tab-content">
                        <?php $no_content = 1; ?>
                        <?php foreach ($data_kegiatan as $key => $parent) : ?>
                            <div class="tab-pane fade show <?= ($no_content === 1) ? 'active' : '' ?>" id="activity-<?= $parent->slug ?>" role="tabpanel">
                                <?php if (!empty($parent->keterangan) && !is_null($parent->keterangan)) : ?>
                                    <div class="alert alert-secondary" role="alert" style="margin-bottom: 2rem;">
                                        <?= $parent->keterangan ?>
                                    </div>
                                <?php endif; ?>
                                <?php if (count($parent->kegiatan_item) > 0) : ?>
                                    <?php $no_item = 1; ?>
                                    <?php foreach ($parent->kegiatan_item as $index => $item) :  ?>
                                        <?php $uniqueId = 'kegiatan_item[' . $item->id . ']' ?>
                                        <?php $currentAnswer = $item->jawaban ?>
                                        <?php $aktif = $item->aktif ?>
                                        <?php
                                        $isActive = true;
                                        $waktuMulai = null;
                                        $waktuSelesai = null;

                                        if ($aktif !== 'Kapanpun') {
                                            $waktuMulai = (isset($data_jadwal_shalat->{$aktif})) ? $data_jadwal_shalat->{$aktif}->mulai : null;
                                            $waktuSelesai = (isset($data_jadwal_shalat->{$aktif})) ? $data_jadwal_shalat->{$aktif}->selesai : null;

                                            if (!is_null($waktuMulai) && !is_null($waktuSelesai)) {
                                                $isActive = (($timeNow >= strtotime($waktuMulai)) && ($timeNow <= strtotime($waktuSelesai))) ? true : false;
                                            };
                                        };
                                        ?>

                                        <!-- Judul Pertanyaan -->
                                        <h3 class="card-body__title">
                                            <span class="badge badge-light badge-pill kegiatan_item-badge"><?= $no_item ?></span>&nbsp;
                                            <span class="kegiatan_item-judul">
                                                <?= $item->nama ?>
                                                <?php if ($aktif !== 'Kapanpun' && !is_null($waktuMulai) && !is_null($waktuSelesai) && $isToday) : ?>
                                                    <a href="javascript:;" data-toggle="popover" data-trigger="focus" data-content="Pengisian hanya dapat dilakukan jam <?= $waktuMulai ?> - <?= $waktuSelesai ?>">
                                                        <span class="zmdi zmdi-<?= (!$isActive) ? 'lock-outline' : 'lock-open' ?>" style="color: <?= (!$isActive) ? '#ff6b68' : '#39bbb0' ?>; font-size: 10pt;"></span>
                                                    </a>
                                                <?php endif; ?>
                                            </span>
                                        </h3>

                                        <?php if ($item->tipe_jawaban === 'Pilihan Ganda Single') : ?>
                                            <!-- Pilihan Ganda Single -->
                                            <div class="answer-wrap">
                                                <?php if (!empty($item->option1) && !is_null($item->option1)) : ?>
                                                    <div class="radio <?= (!$app->is_mobile) ? 'radio--inline' : '' ?>">
                                                        <input type="radio" name="<?= $uniqueId ?>" id="<?= $uniqueId ?>-option1" value="<?= $item->option1 ?>" <?= ($currentAnswer == $item->option1) ? 'checked' : '' ?> <?= ($isToday && $isActive) ? '' : 'readonly onclick="javascript: return false;"' ?>>
                                                        <label class="radio__label" for="<?= $uniqueId ?>-option1"><?= $item->option1 ?></label>
                                                    </div>
                                                <?php endif; ?>
                                                <?php if (!empty($item->option2) && !is_null($item->option2)) : ?>
                                                    <div class="radio <?= (!$app->is_mobile) ? 'radio--inline' : '' ?>">
                                                        <input type="radio" name="<?= $uniqueId ?>" id="<?= $uniqueId ?>-option2" value="<?= $item->option2 ?>" <?= ($currentAnswer == $item->option2) ? 'checked' : '' ?> <?= ($isToday && $isActive) ? '' : 'readonly onclick="javascript: return false;"' ?>>
                                                        <label class="radio__label" for="<?= $uniqueId ?>-option2"><?= $item->option2 ?></label>
                                                    </div>
                                                <?php endif; ?>
                                                <?php if (!empty($item->option3) && !is_null($item->option3)) : ?>
                                                    <div class="radio <?= (!$app->is_mobile) ? 'radio--inline' : '' ?>">
                                                        <input type="radio" name="<?= $uniqueId ?>" id="<?= $uniqueId ?>-option3" value="<?= $item->option3 ?>" <?= ($currentAnswer == $item->option3) ? 'checked' : '' ?> <?= ($isToday && $isActive) ? '' : 'readonly onclick="javascript: return false;"' ?>>
                                                        <label class="radio__label" for="<?= $uniqueId ?>-option3"><?= $item->option3 ?></label>
                                                    </div>
                                                <?php endif; ?>
                                                <?php if (!empty($item->option4) && !is_null($item->option4)) : ?>
                                                    <div class="radio <?= (!$app->is_mobile) ? 'radio--inline' : '' ?>">
                                                        <input type="radio" name="<?= $uniqueId ?>" id="<?= $uniqueId ?>-option4" value="<?= $item->option4 ?>" <?= ($currentAnswer == $item->option4) ? 'checked' : '' ?> <?= ($isToday && $isActive) ? '' : 'readonly onclick="javascript: return false;"' ?>>
                                                        <label class="radio__label" for="<?= $uniqueId ?>-option4"><?= $item->option4 ?></label>
                                                    </div>
                                                <?php endif; ?>
                                                <?php if (!empty($item->option5) && !is_null($item->option5)) : ?>
                                                    <div class="radio <?= (!$app->is_mobile) ? 'radio--inline' : '' ?>">
                                                        <input type="radio" name="<?= $uniqueId ?>" id="<?= $uniqueId ?>-option5" value="<?= $item->option5 ?>" <?= ($currentAnswer == $item->option5) ? 'checked' : '' ?> <?= ($isToday && $isActive) ? '' : 'readonly onclick="javascript: return false;"' ?>>
                                                        <label class="radio__label" for="<?= $uniqueId ?>-option5"><?= $item->option5 ?></label>
                                                    </div>
                                                <?php endif; ?>
                                            </div>
                                            <!-- END ## Pilihan Ganda Single -->
                                        <?php elseif ($item->tipe_jawaban === 'Isian Bebas') : ?>
                                            <!-- Isian Bebas -->
                                            <div class="answer-wrap">
                                                <div class="form-group" style="margin-bottom: 0; margin-left: 3px;">
                                                    <input type="text" name="<?= $uniqueId ?>" class="form-control" placeholder="<?= $item->nama ?>" value="<?= $currentAnswer ?>" <?= ($isToday && $isActive) ? '' : 'readonly' ?> />
                                                    <i class="form-group__bar"></i>
                                                </div>
                                            </div>
                                            <!-- END ## Isian Bebas -->
                                        <?php elseif ($item->tipe_jawaban === 'File') : ?>
                                            <!-- File -->
                                            <div class="answer-wrap">
                                                <div class="form-group" style="margin-bottom: 0; margin-left: 3px;">
                                                    <input type="file" name="<?= $uniqueId ?>" class="form-control" <?= ($isToday && $isActive) ? '' : 'disabled' ?> />
                                                    <i class="form-group__bar"></i>
                                                </div>
                                                <?php if (!empty($currentAnswer)) : ?>
                                                    <div class="lightbox">
                                                        <a href="<?= base_url($currentAnswer) ?>">
                                                            <img src="<?= base_url($currentAnswer) ?>" class="file-preview" />
                                                        </a>
                                                    </div>
                                                <?php endif; ?>
                                            </div>
                                            <!-- END ## File -->
                                        <?php endif; ?>

                                        <?php $no_item++ ?>
                                    <?php endforeach; ?>
                                <?php else : ?>
                                    No data available
                                <?php endif; ?>
                            </div>
                            <?php $no_content++; ?>
                        <?php endforeach; ?>
                    </div>
                </div>
                <?php if ($isToday) : ?>
                    <div class="footer-wrap">
                        <button type="submit" class="btn btn-primary btn-submit spinner-action-button <?= ($app->is_mobile) ? 'btn-block' : '' ?>"><i class="zmdi zmdi-save"></i> Submit</button>
                    </div>
                <?php endif; ?>
            <?php else : ?>
                No data available
            <?php endif; ?>
        </form>
    </div>
</div>