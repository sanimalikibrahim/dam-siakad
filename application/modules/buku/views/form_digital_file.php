<style type="text/css">
    .attachment {
        margin-top: 1rem;
        padding: 15px;
        background-color: #f9f9f9;
        border-radius: 2px;
    }

    .attachment-item {
        margin-bottom: 1rem;
    }

    .attachment-preview {
        width: 48px;
        height: 48px;
        border-radius: 2px;
    }

    .attachment-preview-img {
        object-fit: cover;
    }

    .attachment-preview-file {
        background-color: #607D8B;
        color: #f9f9f9;
        font-size: 15pt;
        display: flex;
        justify-content: center;
        align-items: center;
    }

    .attachment-size {
        color: #888;
        font-size: 10px;
    }
</style>

<section id="buku">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title"><?php echo (isset($card_title)) ? $card_title : '' ?></h4>
            <h6 class="card-subtitle"><?php echo (isset($card_subTitle)) ? $card_subTitle : '' ?></h6>

            <form id="form-buku-digital-file" enctype="multipart/form-data" autocomplete="off">
                <!-- CSRF -->
                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />

                <!-- Temporary -->
                <input type="hidden" class="app-is_mobile" value="<?= (isset($app)) ? (int) $app->is_mobile : 0 ?>" readonly />
                <input type="hidden" name="buku-id" class="buku-digital-id" value="<?= (isset($data_buku)) ? $data_buku->id : null ?>" readonly />

                <div class="table-action">
                    <div class="buttons">
                        <a href="<?php echo base_url('buku/digital') ?>" class="btn btn-secondary">
                            <i class="zmdi zmdi-arrow-left"></i>
                        </a>
                        <a href="javascript:;" class="btn btn-primary btn--raised attachment-add">
                            <i class="zmdi zmdi-file-plus"></i> Add File
                        </a>
                        <div class="attachment-input-wrapper"></div>
                    </div>
                </div>

                <div class="form-group mt-3">
                    <?php if (count($data_attachment) > 0) : ?>
                        <?php foreach ($data_attachment as $key => $item) : ?>
                            <div class="attachment mb-2 attachment-data-item-<?php echo $item->id ?>" style="margin-top: 0;">
                                <?php $isImage = (strpos($item->file_type, 'image') !== false) ? true : false ?>
                                <div class="attachment-data-item-<?php echo $item->id ?>">
                                    <div class="row">
                                        <?php if (!$app->is_mobile) : ?>
                                            <div class="col-auto no-padding-r">
                                                <?php if ($isImage) : ?>
                                                    <img src="<?php echo base_url($item->file_name_thumb) ?>" class="attachment-preview attachment-preview-img" />
                                                <?php else : ?>
                                                    <div class="attachment-preview attachment-preview-file">
                                                        <i class="zmdi zmdi-file-text"></i>
                                                    </div>
                                                <?php endif; ?>
                                            </div>
                                        <?php endif; ?>
                                        <div class="col">
                                            <a href="<?php echo base_url($item->file_name) ?>" target="_blank">
                                                <span>
                                                    <?= (!empty($item->description) && !is_null($item->description)) ? $item->description : $item->file_raw_name ?>
                                                </span>
                                            </a>
                                            <div class="attachment-size">
                                                <i class="zmdi zmdi-time"></i>
                                                <?php echo $item->created_at ?>
                                                <i class="zmdi zmdi-minus"></i>
                                                <?php echo $item->file_size ?> KB
                                            </div>
                                        </div>
                                        <div class="<?= ($app->is_mobile) ? 'col-12' : 'col-auto' ?>" style="display: flex; align-items: center;">
                                            <a href="javascript:;" class="btn btn-danger link-black attachment-delete-existing" data-id="<?php echo $item->id ?>" style="padding: 0 6px; margin-top: 4px;">
                                                <small><i class="zmdi zmdi-close-circle"></i> Delete</small>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    <?php else : ?>
                        <div class="attachment">
                            <label>No data available</label>
                        </div>
                    <?php endif; ?>
                </div>
            </form>

            <div class="row attachment-submit-wrapper" style="display: none;">
                <div class="col text-right">
                    <button type="submit" class="btn btn--raised btn-success btn--icon-text spinner-action-button attachment-submit">
                        <i class="zmdi zmdi-save"></i>
                        Save Changes
                    </button>
                </div>
            </div>
        </div>
    </div>
</section>