<section id="buku">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title"><?php echo (isset($card_title)) ? $card_title : '' ?></h4>
            <h6 class="card-subtitle"><?php echo (isset($card_subTitle)) ? $card_subTitle : '' ?></h6>

            <div class="table-action">
                <div class="buttons">
                    <a href="<?= base_url('buku') ?>" class="btn btn--raised btn-secondary">
                        <i class="zmdi zmdi-flip"></i> Fisik
                    </a>
                    <button class="btn btn--raised btn-primary btn--icon-text buku-digital-action-add" data-toggle="modal" data-target="#modal-form-buku-digital">
                        <i class="zmdi zmdi-plus-circle"></i> Add New
                    </button>
                </div>
            </div>

            <?php include_once('form_digital.php') ?>

            <div class="table-responsive">
                <table id="table-buku-digital" class="table table-bordered">
                    <thead class="thead-default">
                        <tr>
                            <th width="100">No</th>
                            <th>Kode</th>
                            <th>Judul</th>
                            <th>Tahun Terbit</th>
                            <th>Status</th>
                            <th>Created</th>
                            <th>Updated</th>
                            <th width="200">Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</section>