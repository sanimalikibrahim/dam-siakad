<div class="modal fade" id="modal-form-buku" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-dialog-centered modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title pull-left">Katalog Buku</h5>
      </div>
      <div class="spinner">
        <div class="lds-hourglass"></div>
      </div>
      <div class="modal-body">
        <form id="form-buku" autocomplete="off">
          <!-- CSRF -->
          <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />

          <div class="row">
            <div class="col-xs-12 col-lg-6">
              <div class="form-group">
                <label required>Kategori</label>
                <div class="select">
                  <select name="buku_kategori_id" class="form-control select2 buku-buku_kategori_id" data-placeholder="Select &#8595;" required>
                    <?= $list_buku_kategori ?>
                  </select>
                  <i class="form-group__bar"></i>
                </div>
              </div>
            </div>
            <div class="col-xs-12 col-lg-6">
              <div class="form-group">
                <label required>Kode</label>
                <input type="text" name="kode" class="form-control buku-kode" maxlength="100" placeholder="Kode" required />
                <i class="form-group__bar"></i>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-xs-12 col-lg-6">
              <div class="form-group">
                <label>Kode Rak</label>
                <input type="text" name="kode_rak" class="form-control buku-kode_rak" maxlength="100" placeholder="Kode Rak" />
                <i class="form-group__bar"></i>
              </div>
            </div>
            <div class="col-xs-12 col-lg-6">
              <div class="form-group">
                <label required>Judul</label>
                <input type="text" name="judul" class="form-control buku-judul" maxlength="255" placeholder="Judul" required />
                <i class="form-group__bar"></i>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-xs-12 col-lg-6">
              <div class="form-group">
                <label>Pengarang / Penulis</label>
                <input type="text" name="pengarang" class="form-control buku-pengarang" maxlength="200" placeholder="Pengarang / Penulis" />
                <i class="form-group__bar"></i>
              </div>
            </div>
            <div class="col-xs-12 col-lg-6">
              <div class="form-group">
                <label>Penerbit</label>
                <input type="text" name="penerbit" class="form-control buku-penerbit" maxlength="200" placeholder="Penerbit" />
                <i class="form-group__bar"></i>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-xs-12 col-lg-6">
              <div class="form-group">
                <label>Tahun Terbit</label>
                <input type="number" name="tahun_terbit" class="form-control mask-number buku-tahun_terbit" maxlength="4" placeholder="Tahun Terbit" />
                <i class="form-group__bar"></i>
              </div>
            </div>
            <div class="col-xs-12 col-lg-6">
              <div class="form-group">
                <label>Barcode</label>
                <div class="buku-barcode"></div>
              </div>
            </div>
          </div>

          <small class="form-text text-muted">
            Fields with red stars (<label required></label>) are required.
          </small>

        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success btn--icon-text buku-action-save">
          <i class="zmdi zmdi-save"></i> Save
        </button>
        <button type="button" class="btn btn-light btn--icon-text buku-action-cancel" data-dismiss="modal">
          Cancel
        </button>
      </div>
    </div>
  </div>
</div>