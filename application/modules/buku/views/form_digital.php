<div class="modal fade" id="modal-form-buku-digital" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-dialog-centered modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title pull-left">Katalog Buku</h5>
      </div>
      <div class="spinner">
        <div class="lds-hourglass"></div>
      </div>
      <div class="modal-body">
        <form id="form-buku-digital" autocomplete="off">
          <!-- CSRF -->
          <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />

          <div class="row">
            <div class="col-xs-12 col-lg-6">
              <div class="form-group">
                <label required>Kategori</label>
                <div class="select">
                  <select name="buku_kategori_id" class="form-control select2 buku-digital-buku_kategori_id" data-placeholder="Select &#8595;" required>
                    <?= $list_buku_kategori ?>
                  </select>
                  <i class="form-group__bar"></i>
                </div>
              </div>
            </div>
            <div class="col-xs-12 col-lg-6">
              <div class="form-group">
                <label required>Kode</label>
                <input type="text" name="kode" class="form-control buku-digital-kode" maxlength="100" placeholder="Kode" required />
                <i class="form-group__bar"></i>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-xs-12 col-lg-6">
              <div class="form-group">
                <label required>Judul</label>
                <input type="text" name="judul" class="form-control buku-digital-judul" maxlength="255" placeholder="Judul" required />
                <i class="form-group__bar"></i>
              </div>
            </div>
            <div class="col-xs-12 col-lg-6">
              <div class="form-group">
                <label>Pengarang / Penulis</label>
                <input type="text" name="pengarang" class="form-control buku-digital-pengarang" maxlength="200" placeholder="Pengarang / Penulis" />
                <i class="form-group__bar"></i>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-xs-12 col-lg-6">
              <div class="form-group">
                <label>Penerbit</label>
                <input type="text" name="penerbit" class="form-control buku-digital-penerbit" maxlength="200" placeholder="Penerbit" />
                <i class="form-group__bar"></i>
              </div>
            </div>
            <div class="col-xs-12 col-lg-6">
              <div class="form-group">
                <label>Tahun Terbit</label>
                <input type="number" name="tahun_terbit" class="form-control mask-number buku-digital-tahun_terbit" maxlength="4" placeholder="Tahun Terbit" />
                <i class="form-group__bar"></i>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-xs-12 col-lg-6">
              <div class="form-group">
                <label>Status</label>
                <div class="select">
                  <select name="status" class="form-control buku-digital-status" data-placeholder="Select &#8595;">
                    <option value="Draft" selected>Draft</option>
                    <option value="Publish">Publish</option>
                  </select>
                  <i class="form-group__bar"></i>
                </div>
              </div>
            </div>
            <div class="col-xs-12 col-lg-6">
              <div class="form-group">
                <label required>Cover</label>
                <div class="upload-inline">
                  <div class="upload-button">
                    <input type="file" name="cover_img" class="upload-pure-button buku-digital-cover_img" accept="imaage/jpg,image/jpeg,image/png,image/gif" />
                  </div>
                  <div class="upload-preview"></div>
                </div>
              </div>
            </div>
          </div>

          <small class="form-text text-muted">
            Fields with red stars (<label required></label>) are required.
          </small>

        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success btn--icon-text buku-digital-action-save">
          <i class="zmdi zmdi-save"></i> Save
        </button>
        <button type="button" class="btn btn-light btn--icon-text buku-digital-action-cancel" data-dismiss="modal">
          Cancel
        </button>
      </div>
    </div>
  </div>
</div>