<script type="text/javascript">
  $(document).ready(function() {

    var _key = "";
    var _section = "buku";
    var _table = "table-buku";
    var _modal = "modal-form-buku";
    var _form = "form-buku";
    var _table_digital = "table-buku-digital";
    var _modal_digital = "modal-form-buku-digital";
    var _form_digital = "form-buku-digital";
    var _form_digital_file = "form-buku-digital-file";
    var _modal_partial = "modal-partial";

    // Fisik
    // Initialize DataTables: Index
    if ($("#" + _table)[0]) {
      var table_buku = $("#" + _table).DataTable({
        processing: true,
        serverSide: true,
        ajax: {
          url: "<?php echo base_url('buku/ajax_get_all/') ?>",
          type: "get"
        },
        columns: [{
            data: null,
            render: function(data, type, row, meta) {
              return meta.row + meta.settings._iDisplayStart + 1;
            }
          },
          {
            data: "kode"
          },
          {
            data: "kode_rak"
          },
          {
            data: "judul"
          },
          {
            data: "tahun_terbit"
          },
          {
            data: "created_at"
          },
          {
            data: "updated_at"
          },
          {
            data: null,
            className: "center",
            defaultContent: '<div class="action">' +
              '<a href="javascript:;" class="btn btn-sm btn-light btn-table-action action-edit" data-toggle="modal" data-target="#' + _modal + '"><i class="zmdi zmdi-edit"></i> Edit</a>&nbsp;' +
              '<a href="javascript:;" class="btn btn-sm btn-light btn-table-action action-delete"><i class="zmdi zmdi-delete"></i> Delete</a>' +
              '</div>'
          }
        ],
        autoWidth: !1,
        responsive: {
          details: {
            renderer: function(api, rowIdx, columns) {
              var hideColumn = [];
              var data = $.map(columns, function(col, i) {
                return ($.inArray(col.columnIndex, hideColumn)) ?
                  '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                  '<td class="dt-details-td">' + col.title + ':' + '</td> ' +
                  '<td class="dt-details-td">' + col.data + '</td>' +
                  '</tr>' :
                  '';
              }).join('');

              return data ? $('<table/>').append(data) : false;
            },
            type: "inline",
            target: 'tr',
          }
        },
        columnDefs: [{
          className: 'desktop',
          targets: [0, 1, 2, 3, 4, 5, 6, 7]
        }, {
          className: 'tablet',
          targets: [0, 1, 2, 3, 4]
        }, {
          className: 'mobile',
          targets: [0, 3]
        }, {
          responsivePriority: 2,
          targets: -1
        }],
        pageLength: 15,
        language: {
          searchPlaceholder: "Search...",
          sProcessing: '<div style="text-align: center;"><div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div></div>'
        },
        sDom: '<"dataTables_ct"><"dataTables__top"fb>rt<"dataTables__bottom"ip><"clear">',
        buttons: [{
          extend: "excelHtml5",
          title: "Export Result"
        }, {
          extend: "print",
          title: "Export Result"
        }],
        initComplete: function(a, b) {
          $(this).closest(".dataTables_wrapper").find(".dataTables__top").prepend(
            '<div class="dataTables_buttons hidden-sm-down actions">' +
            '<span class="actions__item zmdi zmdi-refresh" data-table-action="reload" title="Reload" />' +
            '</div>'
          );
        },
      });

      $(".dataTables_filter input[type=search]").focus(function() {
        $(this).closest(".dataTables_filter").addClass("dataTables_filter--toggled")
      });

      $(".dataTables_filter input[type=search]").blur(function() {
        $(this).closest(".dataTables_filter").removeClass("dataTables_filter--toggled")
      });

      $("body").on("click", "[data-table-action]", function(a) {
        a.preventDefault();
        var b = $(this).data("table-action");
        if ("reload" === b) {
          $("#" + _table).DataTable().ajax.reload(null, false);
        };
      });
    };

    // Handle data add
    $("#" + _section).on("click", "button." + _section + "-action-add", function(e) {
      e.preventDefault();
      resetForm();
      $("#" + _form + " ." + _section + "-barcode").html("<i>Will be generated automatically.</i>");
    });

    // Handle generate all barcode
    $("#" + _section).on("click", "button." + _section + "-action-print-barcode", function(e) {
      e.preventDefault();

      $("#" + _modal_partial + " .partial-title").html("Print Barcode");
      $("#" + _modal_partial + " .partial-content").html("");
      $("#" + _modal_partial + " .partial-footer").html("");

      // Fetch all barcode
      $.ajax({
        url: "<?= base_url('buku/ajax_get_all_barocde/') ?>",
        type: "get",
        success: function(response) {
          var button = '';
          button += '<a href="javascript:;" class="btn btn-success btn-print-barcode-modal"><i class="zmdi zmdi-print"></i> Print</a>&nbsp;';
          button += '<a href="<?= base_url('buku/d-pdf/all-barcode/') ?>" class="btn btn-success"><i class="zmdi zmdi-download"></i> Export to PDF</a>';

          $("#" + _modal_partial + " .partial-content").html(response);
          $("#" + _modal_partial + " .partial-footer").html(button);
        }
      });
    });

    // Handle print all barcode
    $("#" + _modal_partial).on("click", "a.btn-print-barcode-modal", function() {
      const canvas = $("#" + _modal_partial + " .partial-content");

      // Print by selector element
      $(canvas).printThis();
    });

    // Handle data edit
    $("#" + _table).on("click", "a.action-edit", function(e) {
      e.preventDefault();
      resetForm();
      var temp = table_buku.row($(this).closest('tr')).data();
      var barcode = '<i>No file found.</i>';

      // Set key for update params, important!
      _key = temp.id;

      // Set barcode
      if (temp.barcode !== null) {
        barcode = '<img src="<?= base_url() ?>' + temp.barcode + '" class="img-thumbnail" style="margin-top: 1rem;" />';
      } else {
        barcode = '<a href="javascript:;" class="btn btn-secondary btn-sm action-generate-barcode"><i class="zmdi zmdi-blur"></i> Generate</a>';
      };

      $("#" + _form + " ." + _section + "-buku_kategori_id").val(temp.buku_kategori_id).trigger('change');
      $("#" + _form + " ." + _section + "-kode").val(temp.kode).trigger('input');
      $("#" + _form + " ." + _section + "-kode_rak").val(temp.kode_rak).trigger('input');
      $("#" + _form + " ." + _section + "-judul").val(temp.judul).trigger('input');
      $("#" + _form + " ." + _section + "-pengarang").val(temp.pengarang).trigger('input');
      $("#" + _form + " ." + _section + "-penerbit").val(temp.penerbit).trigger('input');
      $("#" + _form + " ." + _section + "-tahun_terbit").val(temp.tahun_terbit).trigger('input');
      $("#" + _form + " ." + _section + "-barcode").html(barcode);
    });

    // Handle generate barcode
    $("#" + _form).on("click", "a.action-generate-barcode", function(e) {
      e.preventDefault();
      $.ajax({
        type: "get",
        url: "<?php echo base_url('buku/ajax_generate_barcode/') ?>" + _key,
        success: function(response) {
          var response = JSON.parse(response);
          if (response.status === true) {
            barcode = '<img src="<?= base_url() ?>' + response.file_path + '" class="img-thumbnail" style="margin-top: 1rem;" />';
            $("#" + _form + " ." + _section + "-barcode").html(barcode);
            $("#" + _table).DataTable().ajax.reload(null, false);
            notify(response.data, "success");
          } else {
            notify(response.data, "danger");
          };
        }
      });
    });

    // Handle data submit
    $("#" + _modal + " ." + _section + "-action-save").on("click", function(e) {
      e.preventDefault();
      $.ajax({
        type: "post",
        url: "<?php echo base_url('buku/ajax_save/') ?>" + _key,
        data: $("#" + _form).serialize(),
        success: function(response) {
          var response = JSON.parse(response);
          if (response.status === true) {
            resetForm();
            $("#" + _modal).modal("hide");
            $("#" + _table).DataTable().ajax.reload(null, false);
            notify(response.data, "success");
          } else {
            notify(response.data, "danger");
          };
        }
      });
    });

    // Handle data delete
    $("#" + _table).on("click", "a.action-delete", function(e) {
      e.preventDefault();
      var temp = table_buku.row($(this).closest('tr')).data();

      swal({
        title: "Are you sure to delete?",
        text: "Once deleted, you will not be able to recover this data!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false
      }).then((result) => {
        if (result.value) {
          $.ajax({
            type: "delete",
            url: "<?php echo base_url('buku/ajax_delete/') ?>" + temp.id,
            dataType: "json",
            success: function(response) {
              if (response.status) {
                $("#" + _table).DataTable().ajax.reload(null, false);
                notify(response.data, "success");
              } else {
                notify(response.data, "danger");
              };
            }
          });
        };
      });
    });

    // Handle form reset
    resetForm = () => {
      _key = "";
      $("#" + _form).trigger("reset");
      $("#" + _form + " ." + _section + "-buku_kategori_id").val("").trigger('change');
      $("#" + _form + " ." + _section + "-barcode").html("<i>No file found.</i>");
    };
    // END ## Fisik

    // Digital
    // Initialize DataTables: Index
    if ($("#" + _table_digital)[0]) {
      var table_buku_digital = $("#" + _table_digital).DataTable({
        processing: true,
        serverSide: true,
        ajax: {
          url: "<?php echo base_url('buku/ajax_get_all_digital/') ?>",
          type: "get"
        },
        columns: [{
            data: null,
            render: function(data, type, row, meta) {
              return meta.row + meta.settings._iDisplayStart + 1;
            }
          },
          {
            data: "kode"
          },
          {
            data: "judul"
          },
          {
            data: "tahun_terbit"
          },
          {
            data: "status"
          },
          {
            data: "created_at"
          },
          {
            data: "updated_at"
          },
          {
            data: null,
            className: "center",
            render: function(data, type, row, meta) {
              return '<div class="action">' +
                '<a href="<?= base_url('buku/digital/file/') ?>' + row.id + '" class="btn btn-sm btn-success btn-table-action action-file-digital">File</a>&nbsp;' +
                '<a href="javascript:;" class="btn btn-sm btn-light btn-table-action action-edit-digital" data-toggle="modal" data-target="#' + _modal_digital + '"><i class="zmdi zmdi-edit"></i> Edit</a>&nbsp;' +
                '<a href="javascript:;" class="btn btn-sm btn-light btn-table-action action-delete-digital"><i class="zmdi zmdi-delete"></i> Delete</a>' +
                '</div>';
            }
          }
        ],
        autoWidth: !1,
        responsive: {
          details: {
            renderer: function(api, rowIdx, columns) {
              var hideColumn = [];
              var data = $.map(columns, function(col, i) {
                return ($.inArray(col.columnIndex, hideColumn)) ?
                  '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                  '<td class="dt-details-td">' + col.title + ':' + '</td> ' +
                  '<td class="dt-details-td">' + col.data + '</td>' +
                  '</tr>' :
                  '';
              }).join('');

              return data ? $('<table/>').append(data) : false;
            },
            type: "inline",
            target: 'tr',
          }
        },
        columnDefs: [{
          className: 'desktop',
          targets: [0, 1, 2, 3, 4, 5, 6, 7]
        }, {
          className: 'tablet',
          targets: [0, 1, 2, 4]
        }, {
          className: 'mobile',
          targets: [0, 2]
        }, {
          responsivePriority: 2,
          targets: -1
        }],
        pageLength: 15,
        language: {
          searchPlaceholder: "Search...",
          sProcessing: '<div style="text-align: center;"><div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div></div>'
        },
        sDom: '<"dataTables_ct"><"dataTables__top"fb>rt<"dataTables__bottom"ip><"clear">',
        buttons: [{
          extend: "excelHtml5",
          title: "Export Result"
        }, {
          extend: "print",
          title: "Export Result"
        }],
        initComplete: function(a, b) {
          $(this).closest(".dataTables_wrapper").find(".dataTables__top").prepend(
            '<div class="dataTables_buttons hidden-sm-down actions">' +
            '<span class="actions__item zmdi zmdi-refresh" data-table-action="reload" title="Reload" />' +
            '</div>'
          );
        },
      });

      $(".dataTables_filter input[type=search]").focus(function() {
        $(this).closest(".dataTables_filter").addClass("dataTables_filter--toggled")
      });

      $(".dataTables_filter input[type=search]").blur(function() {
        $(this).closest(".dataTables_filter").removeClass("dataTables_filter--toggled")
      });

      $("body").on("click", "[data-table-action]", function(a) {
        a.preventDefault();
        var b = $(this).data("table-action");
        if ("reload" === b) {
          $("#" + _table_digital).DataTable().ajax.reload(null, false);
        };
      });
    };

    // Handle data add
    $("#" + _section).on("click", "button." + _section + "-digital-action-add", function(e) {
      e.preventDefault();
      resetFormDigital();
    });

    // Handle data edit
    $("#" + _table_digital).on("click", "a.action-edit-digital", function(e) {
      e.preventDefault();
      resetFormDigital();
      var temp = table_buku_digital.row($(this).closest('tr')).data();

      // Set key for update params, important!
      _key = temp.id;

      $("#" + _form_digital + " ." + _section + "-digital-buku_kategori_id").val(temp.buku_kategori_id).trigger('change');
      $("#" + _form_digital + " ." + _section + "-digital-kode").val(temp.kode).trigger('input');
      $("#" + _form_digital + " ." + _section + "-digital-judul").val(temp.judul).trigger('input');
      $("#" + _form_digital + " ." + _section + "-digital-pengarang").val(temp.pengarang).trigger('input');
      $("#" + _form_digital + " ." + _section + "-digital-penerbit").val(temp.penerbit).trigger('input');
      $("#" + _form_digital + " ." + _section + "-digital-tahun_terbit").val(temp.tahun_terbit).trigger('input');
      $("#" + _form_digital + " ." + _section + "-digital-status").val(temp.status).trigger('change');

      // Handle cover
      if (temp.cover_img !== null && temp.cover_img !== "") {
        var cover = "<?php echo base_url() ?>" + temp.cover_img;
        $("#" + _form_digital + " .upload-preview").html('<img src="' + cover + '"/>');
      };
    });

    // Handle data submit
    $("#" + _modal_digital + " ." + _section + "-digital-action-save").on("click", function(e) {
      e.preventDefault();
      var form = $("#" + _form_digital)[0];
      var data = new FormData(form);

      $.ajax({
        type: "post",
        url: "<?php echo base_url('buku/ajax_save_digital/') ?>" + _key,
        data: data,
        dataType: "json",
        enctype: "multipart/form-data",
        processData: false,
        contentType: false,
        cache: false,
        success: function(response) {
          if (response.status === true) {
            resetFormDigital();
            $("#" + _modal_digital).modal("hide");
            $("#" + _table_digital).DataTable().ajax.reload(null, false);
            notify(response.data, "success");
          } else {
            notify(response.data, "danger");
          };
        }
      });
    });

    // Handle data delete
    $("#" + _table_digital).on("click", "a.action-delete-digital", function(e) {
      e.preventDefault();
      var temp = table_buku_digital.row($(this).closest('tr')).data();

      swal({
        title: "Are you sure to delete?",
        text: "Once deleted, you will not be able to recover this data!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false
      }).then((result) => {
        if (result.value) {
          $.ajax({
            type: "delete",
            url: "<?php echo base_url('buku/ajax_delete_digital/') ?>" + temp.id,
            dataType: "json",
            success: function(response) {
              if (response.status) {
                $("#" + _table_digital).DataTable().ajax.reload(null, false);
                notify(response.data, "success");
              } else {
                notify(response.data, "danger");
              };
            }
          });
        };
      });
    });

    // Handle form reset
    resetFormDigital = () => {
      _key = "";
      $("#" + _form_digital).trigger("reset");
      $("#" + _form_digital + " ." + _section + "-digital-buku_kategori_id").val("").trigger('change');
      $("#" + _form_digital + " .upload-preview").html("");
    };

    // Handle upload
    $(document.body).on("change", ".buku-digital-cover_img", function() {
      readUploadInlineURL(this);
    });
    // END ## Digital

    // Digital File
    // Handle add file
    $("#" + _section + " .attachment-add").on("click", function() {
      var isMobile = $("#" + _form_digital_file + " .app-is_mobile").val();
      var key = (randomString(10) + Math.floor(Date.now() / 1000)).toString();
      var inputFileDesc = '' +
        '<div class="input-group">' +
        '<div class="input-group-prepend">' +
        '<span class="input-group-text">Description</span>' +
        '</div>' +
        '<input type="text" name="description[]" class="form-control attachment-description attachment-description-' + key + '" maxlength="255" placeholder="Description..." />' +
        '</div>' +
        '';
      var actionButton = '';
      var actionButtonDesktop = '' +
        '<div class="upload-action data-action-' + key + '">' +
        '<a href="javascript:;" class="btn btn-sm btn-danger attachment-delete" data-action="' + key + '" style="padding: 0 6px;">' +
        '<small><i class="zmdi zmdi-close-circle"></i> Delete</small>' +
        '</a>' +
        '</div>' +
        '';

      if (isMobile === "1") {
        inputFileDesc = '' +
          '<input type="text" name="description[]" class="form-control attachment-description attachment-description-' + key + '" maxlength="255" placeholder="Description..." />' +
          '';
        actionButton = '' +
          '<div class="upload-footer-action attachment-input-' + key + ' text-right">' +
          '<div class="upload-action data-action-' + key + '">' +
          '<a href="javascript:;" class="btn btn-sm btn-danger attachment-delete" data-action="' + key + '" style="padding: 0 6px;">' +
          '<small><i class="zmdi zmdi-close-circle"></i> Delete</small>' +
          '</a>' +
          '</div>' +
          '</div>' +
          '';
        actionButtonDesktop = '';
      };

      var inputFile = '' +
        '<div class="upload-input-description attachment-input-' + key + '" style="margin-top: 10px;">' +
        inputFileDesc +
        '</div>' +
        '<div class="upload-inline-xs attachment-input-' + key + '">' +
        '<div class="upload-button"><input type="file" name="file_name[]" class="upload-pure-button attachment-file" data-preview="' + key + '" accept="application/pdf" /></div>' +
        '<div class="upload-preview data-preview-' + key + '">No file chosen</div>' +
        actionButtonDesktop +
        '</div>' +
        actionButton +
        '';
      $("#" + _section + " .attachment-input-wrapper").append(inputFile).children(':last').hide().fadeIn("fast");

      // Handle submit visibility
      attachmentSubmitVisibile();
    });

    // Handle delete input
    $(document.body).on("click", ".attachment-delete", function() {
      var key = $(this).attr("data-action");
      $(".attachment-input-" + key).fadeOut("fast", function() {
        $(this).remove();

        // Handle submit visibility
        attachmentSubmitVisibile();
      });
    });

    // Handle delete data
    $(document.body).on("click", "a.attachment-delete-existing", function(e) {
      e.preventDefault();
      var id = $(this).attr("data-id");

      swal({
        title: "Are you sure to delete?",
        text: "Once deleted, you will not be able to recover this data!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false
      }).then((result) => {
        if (result.value) {
          $.ajax({
            type: "delete",
            url: "<?php echo base_url('buku/ajax_delete_attachment/') ?>" + id,
            dataType: "json",
            success: function(response) {
              if (response.status) {
                $(".attachment-data-item-" + id).fadeOut("fast", function() {
                  $(this).remove();

                  // Handle submit visibility
                  attachmentSubmitVisibile();
                });
                notify(response.data, "success");
              } else {
                notify(response.data, "danger");
              };
            }
          });
        };
      });
    });

    // Handle data submit
    $("#" + _section + " #" + _form_digital_file).on("submit", function(e) {
      e.preventDefault();
    });
    $("#" + _section + " .attachment-submit").on("click", function(e) {
      e.preventDefault();
      var id = $("#" + _form_digital_file + " .buku-digital-id").val();
      var form = $("#" + _form_digital_file)[0];
      var data = new FormData(form);

      $.ajax({
        type: "post",
        url: "<?php echo base_url('buku/ajax_save_digital_file/') ?>",
        data: data,
        dataType: "json",
        enctype: "multipart/form-data",
        processData: false,
        contentType: false,
        cache: false,
        success: function(response) {
          if (response.status === true) {
            notify(response.data, "success");
            window.location.href = "<?php echo base_url('buku/digital/file/') ?>" + id;
          } else {
            notify(response.data, "danger");
          };
        }
      });

      return false;
    });

    // Handle submit visibility
    function attachmentSubmitVisibile() {
      var input = $("#" + _section + " .attachment-input-wrapper").html();
      var submit = $("#" + _section + " .attachment-submit-wrapper");

      if (input === "") {
        submit.hide();
      } else {
        submit.show();
      };
    };

    // Handle upload
    $(document.body).on("change", ".attachment-file", function() {
      readUploadMultipleDocURLXs(this);
    });
    // END ## Digital File

    // Generate random string
    function randomString(length) {
      var result = '';
      var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
      var charactersLength = characters.length;

      for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
      };

      return result;
    };

  });
</script>