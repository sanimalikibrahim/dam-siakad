<section id="buku">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title"><?php echo (isset($card_title)) ? $card_title : '' ?></h4>
            <h6 class="card-subtitle"><?php echo (isset($card_subTitle)) ? $card_subTitle : '' ?></h6>

            <div class="table-action">
                <div class="buttons">
                    <a href="<?= base_url('buku/digital') ?>" class="btn btn--raised btn-secondary">
                        <i class="zmdi zmdi-flip"></i> Digital
                    </a>
                    <button class="btn btn--raised btn-primary btn--icon-text buku-action-add" data-toggle="modal" data-target="#modal-form-buku">
                        <i class="zmdi zmdi-plus-circle"></i> Add New
                    </button>
                    <div style="display: <?= ($app->is_mobile) ? 'block' : 'none' ?>;" class="mb-1"></div>
                    <button class="btn btn--raised btn-success btn--icon-text buku-action-print-barcode" data-toggle="modal" data-target="#modal-partial">
                        <i class="zmdi zmdi-print"></i> Print Barcode
                    </button>
                </div>
            </div>

            <?php include_once('form.php') ?>
            <?php include_once('partial.php') ?>

            <div class="table-responsive">
                <table id="table-buku" class="table table-bordered">
                    <thead class="thead-default">
                        <tr>
                            <th width="100">No</th>
                            <th>Kode</th>
                            <th>Kode Rak</th>
                            <th>Judul</th>
                            <th>Tahun Terbit</th>
                            <th>Created</th>
                            <th>Updated</th>
                            <th width="170">Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</section>