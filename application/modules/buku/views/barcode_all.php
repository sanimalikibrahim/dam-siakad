<style type="text/css">
  .barcode-wrapper {
    margin: 0;
    padding: 0;
    max-height: 424px;
    white-space: normal;
    overflow: auto;
  }

  .barcode-item {
    float: left;
    background: #fff;
    border: 1px solid #333;
    padding: 5px;
    width: 180px;
    text-align: center;
    margin: 0px 0px 2px 2px;
    word-break: break-all;
  }

  .barcode-item p {
    margin-top: 0;
    margin-bottom: 10px;
  }

  .barcode-item .barcode-image {
    width: 165px;
  }
</style>

<?php if (count($buku) > 0) : ?>
  <div class="barcode-wrapper">
    <?php $no = 1; ?>
    <?php foreach ($buku as $key => $item) : ?>
      <div class="barcode-item">
        <p><b><?= $item->kode ?></b></p>
        <img src="<?= base_url($item->barcode) ?>" class="barcode-image" />
      </div>
      <?php if ($no === 27) : ?>
        <?php $no = 1 ?>
        <div style="page-break-after: always;"></div>
      <?php else : ?>
        <?php $no++ ?>
      <?php endif; ?>
    <?php endforeach ?>
  </div>
<?php endif ?>