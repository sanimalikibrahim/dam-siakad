<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . 'controllers/AppBackend.php');

class Buku extends AppBackend
{
  function __construct()
  {
    parent::__construct();
    $this->load->model([
      'AppModel',
      'BukukategoriModel',
      'BukuModel',
      'DocumentModel'
    ]);
    $this->load->library('form_validation');
  }

  // Fisik
  public function index()
  {
    $data = array(
      'app' => $this->app(),
      'main_js' => $this->load_main_js('buku'),
      'card_title' => 'Katalog Buku Fisik',
      'list_buku_kategori' => $this->init_list($this->BukukategoriModel->getAll([], 'nama', 'asc'), 'id', 'nama')
    );
    $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
    $this->template->load_view('index', $data, TRUE);
    $this->template->render();
  }

  public function ajax_get_all()
  {
    $this->handle_ajax_request();
    $dtAjax_config = array(
      'table_name' => 'view_buku',
      'order_column' => 5,
      'order_column_dir' => 'desc',
      'static_conditional' => [
        'jenis' => 'Fisik'
      ],
    );
    $response = $this->AppModel->getData_dtAjax($dtAjax_config);
    echo json_encode($response);
  }

  public function ajax_save($id = null)
  {
    $this->handle_ajax_request();
    $this->form_validation->set_rules($this->BukuModel->rules($id));

    if ($this->form_validation->run() === true) {
      if (is_null($id)) {
        $generateBarcode = $this->generateBarcode();

        if ($generateBarcode->status === true) {
          $_POST['barcode'] = $generateBarcode->file_path;
          echo json_encode($this->BukuModel->insert());
        } else {
          echo json_encode($generateBarcode);
        };
      } else {
        echo json_encode($this->BukuModel->update($id));
      };
    } else {
      $errors = validation_errors('<div>- ', '</div>');
      echo json_encode(array('status' => false, 'data' => $errors));
    };
  }

  public function ajax_delete($id)
  {
    $this->handle_ajax_request();
    echo json_encode($this->BukuModel->delete($id));
  }

  public function ajax_generate_barcode($id = null)
  {
    $this->handle_ajax_request();

    if (!is_null($id)) {
      $generateBarcode = $this->generateBarcode();

      if ($generateBarcode->status === true) {
        $this->BukuModel->setBarcode($id, $generateBarcode->file_path);
      };

      echo json_encode($generateBarcode);
    } else {
      echo json_encode(array('status' => false, 'data' => 'Parameter is not valid.'));
    };
  }

  public function ajax_get_all_barocde()
  {
    $this->handle_ajax_request();

    if (!$this->app()->is_mobile) {
      $this->_getAllBarcode();
    } else {
      echo '<center><i>The preview is not available for mobile, but you can download it as a PDF to see this.</i></center>';
    };
  }

  public function download_pdf_all_barcode()
  {
    libxml_use_internal_errors(true);

    try {
      $content =  $this->_getAllBarcode(true);
      $file_name = 'Buku_Fisik-Barcode-' . date('YmdHis') . '.pdf';

      $mpdf = new \Mpdf\Mpdf();

      header("Content-type:application/pdf");
      header("Content-Disposition:attachment;filename=$file_name");

      $mpdf->WriteHTML($content);
      $mpdf->Output($file_name, 'D');
    } catch (\Throwable $th) {
      echo 'An error occurred while creating the file.';
    };
  }

  private function _getAllBarcode($renderAsData = false)
  {
    $buku = $this->BukuModel->getAll(['jenis' => 'Fisik'], 'id', 'desc');

    if (count($buku) > 0) {
      return $this->load->view('barcode_all', array(
        'controller' => $this,
        'buku' => $buku
      ), $renderAsData);
    } else {
      echo '<center><i>Buku is not available, cannot generate barcode.</i></center>';
    };
  }
  // END ## Fisik

  // Digital
  public function digital()
  {
    $data = array(
      'app' => $this->app(),
      'main_js' => $this->load_main_js('buku'),
      'card_title' => 'Katalog Buku Digital',
      'list_buku_kategori' => $this->init_list($this->BukukategoriModel->getAll([], 'nama', 'asc'), 'id', 'nama')
    );
    $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
    $this->template->load_view('index_digital', $data, TRUE);
    $this->template->render();
  }

  public function digital_file($id = null)
  {
    if (!is_null($id)) {
      $detail = $this->BukuModel->getDetail(['id' => $id, 'jenis' => 'Digital']);

      if (!is_null($detail) && !empty($detail)) {
        $data = array(
          'app' => $this->app(),
          'main_js' => $this->load_main_js('buku'),
          'card_title' => 'Katalog Buku Digital › File',
          'card_subTitle' => $detail->judul,
          'list_buku_kategori' => $this->init_list($this->BukukategoriModel->getAll([], 'nama', 'asc'), 'id', 'nama'),
          'data_buku' => $detail,
          'data_attachment' => $this->DocumentModel->getAll(['ref' => 'bukudigital', 'ref_id' => $id])
        );
        $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
        $this->template->load_view('form_digital_file', $data, TRUE);
        $this->template->render();
      } else {
        redirect(base_url('buku/digital'));
      };
    } else {
      redirect(base_url('buku/digital'));
    };
  }

  public function ajax_get_all_digital()
  {
    $this->handle_ajax_request();
    $dtAjax_config = array(
      'table_name' => 'view_buku',
      'order_column' => 5,
      'order_column_dir' => 'desc',
      'static_conditional' => [
        'jenis' => 'Digital'
      ],
    );
    $response = $this->AppModel->getData_dtAjax($dtAjax_config);
    echo json_encode($response);
  }

  public function ajax_save_digital($id = null)
  {
    $this->handle_ajax_request();
    $this->form_validation->set_rules($this->BukuModel->rules($id));

    if ($this->form_validation->run() === true) {
      $cpUpload = new CpUpload();
      $upload = $cpUpload->run('cover_img', 'buku', true, true, 'jpg|png|gif');

      if (is_null($id)) {
        if ($upload->status === true) {
          unlink($upload->data->full_path); // Delete file ori
          $_POST['cover_img'] = $upload->data->thumb_path;
          echo json_encode($this->BukuModel->insertDigital());
        } else {
          echo json_encode(array('status' => false, 'data' => $upload->data));
        };
      } else {
        $_POST['cover_img'] = '';
        if ($upload->status === true) {
          unlink($upload->data->full_path); // Delete file ori
          $_POST['cover_img'] = $upload->data->thumb_path;
        };
        echo json_encode($this->BukuModel->updateDigital($id));
      };
    } else {
      $errors = validation_errors('<div>- ', '</div>');
      echo json_encode(array('status' => false, 'data' => $errors));
    };
  }

  public function ajax_save_digital_file()
  {
    $this->handle_ajax_request();
    $id = $this->input->post('buku-id');

    if (!empty($id) && !empty($_FILES['file_name'])) {
      $cpUpload = new CpUpload();
      $files = $cpUpload->re_arrange($_FILES['file_name']);
      $description = $this->input->post('description');
      $post = array();
      $error = '';
      $directory = 'bukudigital';

      foreach ($files as $index => $item) {
        if (empty($description[$index])) {
          $error .= '<div>- The Description field is required.</div>';
        };
      };

      if (empty($error)) {
        foreach ($files as $index => $item) {
          if (!empty($item['name'])) {
            $upload = $cpUpload->run($item, $directory, true, true, 'pdf', true);

            if ($upload->status === true) {
              $post[] = array(
                'ref' => $directory,
                'ref_id' => $id,
                'description' => $description[$index],
                'file_raw_name' => $upload->data->raw_name . $upload->data->file_ext,
                'file_raw_name_thumb' => ($upload->data->is_image) ? $upload->data->raw_name . '_thumb' . $upload->data->file_ext : null,
                'file_name' => $upload->data->base_path,
                'file_name_thumb' => ($upload->data->is_image) ? 'directory/' . $directory . '/' . $upload->data->raw_name . '_thumb' . $upload->data->file_ext : null,
                'file_size' => $upload->data->file_size,
                'file_type' => $upload->data->file_type,
                'file_ext' => $upload->data->file_ext,
                'created_by' =>  $this->session->userdata('user')['id']
              );
            } else {
              $error .= $upload->data;
            };
          };
        };
      };

      if (empty($error) && count($post) > 0) {
        echo json_encode($this->DocumentModel->insertBatch($post));
      } else {
        echo json_encode(array('status' => false, 'data' => $error));
      };
    } else {
      echo json_encode(array('status' => true, 'data' => 'No file to upload.'));
    };
  }

  public function ajax_delete_digital($id)
  {
    $this->handle_ajax_request();
    echo json_encode($this->BukuModel->deleteDigital($id));
  }

  public function ajax_delete_attachment($id)
  {
    $this->handle_ajax_request();
    echo json_encode($this->DocumentModel->delete($id));
  }
  // END ## Digital
}
