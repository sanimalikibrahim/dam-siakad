<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . 'controllers/AppBackend.php');
require_once(FCPATH . 'vendor/autoload.php');

use GuzzleHttp\Client;

class ApiClient extends AppBackend
{
  private $apiUrl;
  private $apiUser;
  private $apiPassword;
  private $learndashGroupId;
  private $courseId;

  function __construct()
  {
    parent::__construct();

    $app = $this->app(); // Get config item from table setting

    $this->apiUrl = (isset($app->api_url)) ? $app->api_url : null;
    $this->apiUser = (isset($app->api_user)) ? $app->api_user : null;
    $this->apiPassword = (isset($app->api_password)) ? $app->api_password : null;
    $this->courseId = (isset($app->sinkronisasi_course_id)) ? $app->sinkronisasi_course_id : null;
    $this->learndashGroupId = (isset($app->sinkronisasi_group_id)) ? $app->sinkronisasi_group_id : null;
  }

  private function _response($request)
  {
    $responseCode = $request->getStatusCode();
    $responseBody = $request->getBody()->getContents();

    if (in_array((int) $responseCode, [200, 201])) {
      $responseBodyDecode = json_decode($responseBody);
      $responseBody = (JSON_ERROR_NONE !== json_last_error()) ? $responseBody : $responseBodyDecode;

      $response = array(
        'status' => true,
        'data' => $responseBody
      );
    } else {
      $response = $this->_responseError($responseBody);
    };

    return $response;
  }

  private function _responseError($throw = null)
  {
    $response = array(
      'status' => false,
      'data' => 'Failed while requesting the data.',
      'error' => $throw // Uncomment this for development mode
    );

    return $response;
  }

  public function testAuth()
  {
    $endPoint = 'wp/v2/users/me';

    try {
      $client = new \GuzzleHttp\Client(['verify' => false]);
      $request = $client->request('GET', $this->apiUrl . $endPoint, [
        'auth' => [$this->apiUser, $this->apiPassword]
      ]);

      $responseCode = $request->getStatusCode();

      if (in_array((int) $responseCode, [200, 201])) {
        $response = 'OK, user is active.';
      };
    } catch (\Throwable $th) {
      $response = 'Failed to get user credential.';
    };

    return $response;
  }

  public function deleteUser($id)
  {
    $endPoint = 'wp/v2/users/' . $id . '/?reassign=false&force=true';

    try {
      $client = new \GuzzleHttp\Client(['verify' => false]);
      $client->request('DELETE', $this->apiUrl . $endPoint, [
        'auth' => [$this->apiUser, $this->apiPassword]
      ]);

      $response = true;
    } catch (\Throwable $th) {
      $response = true;
    };

    return $response; // Just true when success or failed, we don't care about that
  }

  public function updateUserPassword($id, $password)
  {
    $endPoint = 'wp/v2/users/' . $id;

    try {
      // Update user
      $client = new \GuzzleHttp\Client(['verify' => false]);
      $request = $client->request('POST', $this->apiUrl . $endPoint, [
        'auth' => [$this->apiUser, $this->apiPassword],
        'form_params' => [
          'password' => $password,
        ]
      ]);

      $response = $this->_response($request);
    } catch (\Throwable $th) {
      $response = $this->_responseError($th);
    };

    return $response;
  }

  public function storeUserToLearndash($learndashUserId, $data)
  {
    $endPoint = 'wp/v2/users';

    // Delete user, just skip if not exist (it's safe)
    $this->deleteUser($learndashUserId);

    try {
      $data = (object) $data;

      // Create new user
      $client = new \GuzzleHttp\Client(['verify' => false]);
      $request = $client->request('POST', $this->apiUrl . $endPoint, [
        'auth' => [$this->apiUser, $this->apiPassword],
        'form_params' => [
          'username' => $data->username,
          'password' => $data->password,
          'email' => $data->email,
          'name' => $data->name,
          'first_name' => $data->first_name,
          'last_name' => $data->last_name
        ]
      ]);

      $user = $this->_response($request);

      if ($user['status'] === true) {
        // Insert user into learndash group
        $learndash = $this->storeUserToLearndashGroup($user);

        if ($learndash['status'] === true) {
          $response = $user;
        } else {
          $user = $user['data'];
          $userId = (isset($user->id)) ? $user->id : null;

          // Rollback user, just skip if not exist (it's safe)
          $this->deleteUser($userId);

          $response = $learndash;
        }
      } else {
        $response = $user;
      };
    } catch (\Throwable $th) {
      $response = $this->_responseError($th);
    };

    return $response;
  }

  public function storeUserToLearndashGroup($user)
  {
    $endPoint = 'ldlms/v1/groups/' . $this->learndashGroupId . '/users';

    try {
      $user = $user['data'];
      $userId = $user->id;

      $client = new \GuzzleHttp\Client(['verify' => false]);
      $request = $client->request('POST', $this->apiUrl . $endPoint, [
        'auth' => [$this->apiUser, $this->apiPassword],
        'json' => [
          'user_ids' => [$userId]
        ]
      ]);

      $response = $this->_response($request);
    } catch (\Throwable $th) {
      $response = $this->_responseError($th);
    };

    return $response;
  }

  public function getListQuiz()
  {
    $endPoint = 'ldlms/v2/sfwd-quiz?course=' . $this->courseId;

    try {
      $client = new \GuzzleHttp\Client(['verify' => false]);
      $request = $client->request('GET', $this->apiUrl . $endPoint, [
        'auth' => [$this->apiUser, $this->apiPassword]
      ]);

      $responseCode = $request->getStatusCode();

      if (in_array((int) $responseCode, [200, 201])) {
        $response = $this->_response($request);
      };
    } catch (\Throwable $th) {
      $response = $this->_responseError($th);
    };

    return $response;
  }

  public function getDetailQuiz($id, $page = 1, $perPage = 50)
  {
    $endPoint = 'ldlms/v2/sfwd-quiz/' . $id . '/statistics?page=' . $page . '&per_page=' . $perPage;

    try {
      $client = new \GuzzleHttp\Client(['verify' => false]);
      $request = $client->request('GET', $this->apiUrl . $endPoint, [
        'auth' => [$this->apiUser, $this->apiPassword]
      ]);

      $responseCode = $request->getStatusCode();

      if (in_array((int) $responseCode, [200, 201])) {
        $response = $this->_response($request);
      };
    } catch (\Throwable $th) {
      $response = $this->_responseError($th);
    };

    return $response;
  }

  public function getUserSearch($keyword)
  {
    $endPoint = 'wp/v2/users/?search=' . trim(strtolower($keyword));

    try {
      $client = new \GuzzleHttp\Client(['verify' => false]);
      $request = $client->request('GET', $this->apiUrl . $endPoint, [
        'auth' => [$this->apiUser, $this->apiPassword]
      ]);

      $responseCode = $request->getStatusCode();

      if (in_array((int) $responseCode, [200, 201])) {
        $response = $this->_response($request);
      };
    } catch (\Throwable $th) {
      $response = $this->_responseError($th);
    };

    return $response;
  }

  public function getListLesson($perPage = 50, $courseId = null)
  {
    $courseId = (is_null($courseId)) ? '' : '&course=' . $courseId;
    $endPoint = 'ldlms/v2/sfwd-lessons?per_page=' . $perPage . $courseId;

    try {
      $client = new \GuzzleHttp\Client(['verify' => false]);
      $request = $client->request('GET', $this->apiUrl . $endPoint, [
        'auth' => [$this->apiUser, $this->apiPassword]
      ]);

      $responseCode = $request->getStatusCode();

      if (in_array((int) $responseCode, [200, 201])) {
        $response = $this->_response($request);
      };
    } catch (\Throwable $th) {
      $response = $this->_responseError($th);
    };

    return $response;
  }

  public function getListGroup($perPage = 50)
  {
    $endPoint = 'ldlms/v2/groups?per_page=' . $perPage;

    try {
      $client = new \GuzzleHttp\Client(['verify' => false]);
      $request = $client->request('GET', $this->apiUrl . $endPoint, [
        'auth' => [$this->apiUser, $this->apiPassword]
      ]);

      $responseCode = $request->getStatusCode();

      if (in_array((int) $responseCode, [200, 201])) {
        $response = $this->_response($request);
      };
    } catch (\Throwable $th) {
      $response = $this->_responseError($th);
    };

    return $response;
  }

  public function getListCourse($perPage = 50, $groupId = null)
  {
    $endPoint = 'ldlms/v2/groups/' . $groupId . '/courses?per_page=' . $perPage;

    try {
      $client = new \GuzzleHttp\Client(['verify' => false]);
      $request = $client->request('GET', $this->apiUrl . $endPoint, [
        'auth' => [$this->apiUser, $this->apiPassword]
      ]);

      $responseCode = $request->getStatusCode();

      if (in_array((int) $responseCode, [200, 201])) {
        $response = $this->_response($request);
      };
    } catch (\Throwable $th) {
      $response = $this->_responseError($th);
    };

    return $response;
  }

  public function getListTopic($perPage = 50, $lessonId = null, $courseId = null)
  {
    $endPoint = 'ldlms/v2/sfwd-topic?per_page=' . $perPage . '&course=' . $courseId . '&lesson=' . $lessonId;

    try {
      $client = new \GuzzleHttp\Client(['verify' => false]);
      $request = $client->request('GET', $this->apiUrl . $endPoint, [
        'auth' => [$this->apiUser, $this->apiPassword]
      ]);

      $responseCode = $request->getStatusCode();

      if (in_array((int) $responseCode, [200, 201])) {
        $response = $this->_response($request);
      };
    } catch (\Throwable $th) {
      $response = $this->_responseError($th);
    };

    return $response;
  }

  public function getListQuiz2($perPage = 50, $topicId = null, $courseId = null, $lessonId = null)
  {
    $endPoint = 'ldlms/v2/sfwd-quiz?per_page=' . $perPage . '&course=' . $courseId . '&lesson=' . $lessonId . '&topic=' . $topicId;

    try {
      $client = new \GuzzleHttp\Client(['verify' => false]);
      $request = $client->request('GET', $this->apiUrl . $endPoint, [
        'auth' => [$this->apiUser, $this->apiPassword]
      ]);

      $responseCode = $request->getStatusCode();

      if (in_array((int) $responseCode, [200, 201])) {
        $response = $this->_response($request);
      };
    } catch (\Throwable $th) {
      $response = $this->_responseError($th);
    };

    return $response;
  }
}
