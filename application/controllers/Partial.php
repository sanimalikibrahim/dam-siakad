<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . 'controllers/AppBackend.php');

class Partial extends AppBackend
{
  function __construct()
  {
    parent::__construct();
    $this->load->model(['PostModel']);
  }

  public function post($id = null)
  {
    $post = $this->PostModel->getDetail(['id' => $id]);

    if (!is_null($post)) {
      $this->load->view('partial_post', ['data' => $post]);
    } else {
      show_404();
    };
  }
}
