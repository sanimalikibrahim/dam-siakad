<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$route['default_controller'] = 'dashboard';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['notification/(:num)'] = 'notification';
$route['search/(:any)'] = 'search';
$route['buku/digital/file/(:num)'] = 'buku/digital_file/$1';
$route['bukudigital/(:num)'] = 'bukudigital/book/$1';
$route['bukudigital/view/(:num)'] = 'bukudigital/view/$1';
$route['persetujuan/buku/detail/(:num)'] = 'persetujuan/buku_detail/$1';
$route['jadwalpelajaran/d-pdf/(:num)'] = 'jadwalpelajaran/download_pdf/$1';
$route['buku/d-pdf/all-barcode'] = 'buku/download_pdf_all_barcode';
$route['profile/calon-santri'] = 'calonsantri/update';
$route['notice/persyaratan-daftar-ulang'] = 'notice/persyaratan_psb';
$route['notice/timeline'] = 'notice/timeline';
$route['post/timeline/item/(:num)'] = 'post/timeline_item/$1';
$route['post/s/read/(:any)'] = 'post/static_read/$1';
$route['post/t/read/(:any)'] = 'post/timeline_read/$1';
$route['upload-pembayaran'] = 'pembayaran/upload';
$route['generate-idcard'] = 'idcard/generate';

$route['laporan/pembelian-buku'] = 'laporan/buku';
$route['laporan/d-pdf/pembelian-buku/(:num)'] = 'laporan/download_pdf_report_buku/$1';
$route['laporan/santri-baru'] = 'laporan/santri_baru';
$route['laporan/calon-santri'] = 'laporan/calon_santri';
$route['laporan/d-xlsx/santri-baru'] = 'laporan/download_excel_santri_baru';
$route['laporan/d-xlsx/calon-santri'] = 'laporan/download_excel_calon_santri';

$route['rekap/santri-baru'] = 'rekap/santri_baru';
$route['rekap/calon-santri'] = 'rekap/calon_santri';
$route['rekap/kelasharian'] = 'rekap/rekapkelasharian/index';
$route['rekap/absenpengajar'] = 'rekap/rekapabsenpengajar/index';
$route['rekap/seragam'] = 'rekap/rekapseragam/index';
$route['rekap/d-pdf/berjamaah'] = 'rekap/download_pdf_berjamaah';
$route['rekap/d-xlsx/santri-baru'] = 'rekap/download_excel_santri_baru';
$route['rekap/d-xlsx/calon-santri'] = 'rekap/download_excel_calon_santri';
$route['rekap/d-xlsx/absen-kelas-compact'] = 'rekap/rekapkelasharian/export_excel_compact';
$route['rekap/d-xlsx/absen-kelas-month'] = 'rekap/rekapkelasharian/export_excel_month';
$route['rekap/d-xlsx/absen-pengajar-compact'] = 'rekap/rekapabsenpengajar/export_excel_compact';
$route['rekap/d-xlsx/absen-pengajar-month'] = 'rekap/rekapabsenpengajar/export_excel_month';
$route['rekap/d-xlsx/seragam'] = 'rekap/rekapseragam/export_excel';

$route['calonsantri/d-xlsx/(:num)'] = 'calonsantri/download_excel/$1';

$route['raport/capaianhasilbelajar'] = 'raport_capaianhasilbelajar/index';
$route['raport/pengembangandiri'] = 'raport_pengembangandiri/index';
$route['raport/print'] = 'raport_cetak/index';
$route['raport/setting'] = 'raport_setting/index';
