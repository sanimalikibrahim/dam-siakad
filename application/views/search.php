<style type="text/css">
    .list-link {
        color: #777;
    }

    .list-content {
        margin-bottom: 15px;
    }

    .list-content p {
        margin-bottom: 0;
    }

    .list-content small {
        color: #888;
    }
</style>

<section id="user">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title"><?php echo (isset($card_title)) ? $card_title : '' ?></h4>
            <h6 class="card-subtitle"><?php echo (isset($card_subTitle)) ? $card_subTitle : '' ?></h6>

            <?php if (count($data) > 0) : ?>
                <?php foreach ($data as $key => $item) : ?>
                    <a href="#" class="list-link">
                        <div class="row list-content">
                            <div class="col">
                                <p><?php echo $item->nama_lengkap ?></p>
                                <small>
                                    NISN: <?= $item->nisn ?>
                                    <i class="zmdi zmdi-minus"></i>
                                    NIL: <?= $item->nomor_induk_lokal ?>
                                    <i class="zmdi zmdi-minus"></i>
                                    <?= $item->kelas . ' ' . $item->sub_kelas ?>
                                    <i class="zmdi zmdi-minus"></i>
                                    <?= $item->jenis_kelamin ?>
                                    <?= (!empty($item->alamat) && !is_null($item->alamat)) ? '<i class="zmdi zmdi-minus"></i> ' . $item->alamat : '' ?>
                                </small>
                            </div>
                        </div>
                    </a>
                <?php endforeach; ?>
            <?php else : ?>
                <p>No data found</p>
            <?php endif; ?>
        </div>
    </div>
</section>