<?php
defined('BASEPATH') or exit('No direct script access allowed');

class RuanganModel extends CI_Model
{
  private $_table = 'ruangan';
  private $_tableView = 'view_ruangan';

  public function rules()
  {
    return array(
      [
        'field' => 'nama_ruangan',
        'label' => 'Nama Ruangan',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'kapasitas',
        'label' => 'Kapasitas',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'jenis',
        'label' => 'Jenis',
        'rules' => 'required|trim'
      ],
    );
  }

  public function getAll($params = [], $limitStart = null, $limitOffset = null, $orderName = true)
  {
    $this->db->where($params);

    if ($orderName) {
      $this->db->order_by('nama_ruangan', 'asc');
    };

    if (!is_null($limitStart) && !is_null($limitOffset)) {
      $this->db->limit($limitOffset, $limitStart);
    };

    return $this->db->get($this->_tableView)->result();
  }

  public function getDetail($params = [])
  {
    return $this->db->where($params)->get($this->_tableView)->row();
  }

  public function insert()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->nama_ruangan = $this->input->post('nama_ruangan');
      $this->kapasitas = $this->input->post('kapasitas');
      $this->jenis = $this->input->post('jenis');
      $this->tipe = $this->input->post('tipe');
      $this->created_by = $this->session->userdata('user')['id'];
      $this->db->insert($this->_table, $this);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function insertBatch($data)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->insert_batch($this->_table, $data);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function update($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->nama_ruangan = $this->input->post('nama_ruangan');
      $this->kapasitas = $this->input->post('kapasitas');
      $this->jenis = $this->input->post('jenis');
      $this->tipe = $this->input->post('tipe');
      $this->updated_at = date('Y-m-d H:i:s');
      $this->updated_by = $this->session->userdata('user')['id'];
      $this->db->update($this->_table, $this, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function delete($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->delete($this->_table, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  public function truncate()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->truncate($this->_table);

      $response = array('status' => true, 'data' => 'Data has been truncated.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to truncate your data.');
    };

    return $response;
  }

  function br2nl($text)
  {
    return str_replace("\r\n", '<br/>', htmlspecialchars_decode($text));
  }

  function clean_number($number)
  {
    return preg_replace('/[^0-9]/', '', $number);
  }
}
