<?php
defined('BASEPATH') or exit('No direct script access allowed');

class TimelineModel extends CI_Model
{
  private $_table = 'timeline';
  private $_tableView = '';

  public function rules()
  {
    return array(
      [
        'field' => 'judul',
        'label' => 'Judul',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'url',
        'label' => 'URL',
        'rules' => 'required|trim|regex_match[/^[a-z0-9-]+$/]'
      ],
      [
        'field' => 'is_active',
        'label' => 'Status',
        'rules' => 'required|trim'
      ]
    );
  }

  public function getAll($params = [])
  {
    return $this->db->where($params)->get($this->_table)->result();
  }

  public function getDetail($params = [])
  {
    return $this->db->where($params)->get($this->_table)->row();
  }

  public function insert()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->judul = $this->input->post('judul');
      $this->url = $this->input->post('url');
      $this->is_active = $this->input->post('is_active');
      $this->created_by = $this->session->userdata('user')['id'];
      $this->db->insert($this->_table, $this);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function insertBatch($data)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->insert_batch($this->_table, $data);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function update($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->judul = $this->input->post('judul');
      $this->url = $this->input->post('url');
      $this->is_active = $this->input->post('is_active');
      $this->updated_at = date('Y-m-d H:i:s');
      $this->updated_by = $this->session->userdata('user')['id'];
      $this->db->update($this->_table, $this, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function delete($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      if ((int) $id === 1) {
        $response = array('status' => false, 'data' => 'This data is related to other modules, you are not allowed to delete it.');
      } else {
        $this->db->delete($this->_table, ['id' => $id]);
        $this->db->delete('timeline_item', ['timeline_id' => $id]);

        $response = array('status' => true, 'data' => 'Data has been deleted.');
      };
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  public function truncate()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->truncate($this->_table);

      $response = array('status' => true, 'data' => 'Data has been truncated.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to truncate your data.');
    };

    return $response;
  }

  function br2nl($text)
  {
    return str_replace("\r\n", '<br/>', htmlspecialchars_decode($text));
  }

  function clean_number($number)
  {
    return preg_replace('/[^0-9]/', '', $number);
  }
}
