<?php
defined('BASEPATH') or exit('No direct script access allowed');

class SantriModel extends CI_Model
{
  private $_table = 'santri';
  private $_tableView = 'view_santri';

  public function rules($id = null)
  {
    return array(
      [
        'field' => 'nisn',
        'label' => 'NISN',
        'rules' => [
          'required',
          'trim',
          [
            'nisn_exist',
            function ($nisn) use ($id) {
              return $this->_nisn_exist($nisn, $id);
            }
          ]
        ]
      ],
      [
        'field' => 'nama_lengkap',
        'label' => 'Nama Lengkap',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'jenis_kelamin',
        'label' => 'Jenis Kelamin',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'tempat_lahir',
        'label' => 'Tempat Lahir',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'tanggal_lahir',
        'label' => 'Tanggal Lahir',
        'rules' => 'required|trim'
      ]
    );
  }

  private function _nisn_exist($nisn, $id)
  {
    $id = (!IS_NULL($id)) ? $id : 0;
    $temp = $this->db->where(['id !=' => $id, 'nisn' => $nisn])->get($this->_table);

    if ($temp->num_rows() > 0) {
      $this->form_validation->set_message('nisn_exist', 'NISN "' . $nisn . '" has been used by other user.');
      return false;
    } else {
      return true;
    };
  }

  public function getAll($params = [], $santriList = [])
  {
    if (count($santriList) > 0) {
      $this->db->where_in('id', $santriList);
    };

    $this->db->where($params);
    $this->db->order_by('nama_lengkap', 'asc');
    return $this->db->get($this->_tableView)->result();
  }

  public function getDetail($params = [])
  {
    return $this->db->where($params)->get($this->_tableView)->row();
  }

  public function getKelasConcatedByPembina($pembina = null)
  {
    return $this->db->select("kelas, sub_kelas, concat(t.kelas, ' ', t.sub_kelas) AS concated, concat(t.kelas, '#', t.sub_kelas) AS concated_key")
      ->where(['pembina' => $pembina])
      ->group_by(['kelas', 'sub_kelas'])
      ->order_by('kelas', 'asc')
      ->order_by('sub_kelas', 'asc')
      ->get($this->_table)
      ->result();
  }

  public function getKelasByPembina($pembina = null)
  {
    return $this->db->select('kelas AS nama')
      ->where(['pembina' => $pembina])
      ->group_by(['kelas'])
      ->order_by('kelas', 'asc')
      ->get($this->_table)
      ->result();
  }

  public function getSubKelasByPembina($pembina = null)
  {
    return $this->db->select('sub_kelas AS nama')
      ->where(['pembina' => $pembina])
      ->group_by(['sub_kelas'])
      ->order_by('sub_kelas', 'asc')
      ->get($this->_table)
      ->result();
  }

  public function insert()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $user = [
        'username' => $this->input->post('nisn'),
        'nama_lengkap' => $this->input->post('nama_lengkap'),
        'role' => 'Santri'
      ];

      if ($this->db->insert('user', $user)) {
        $this->id = $this->db->insert_id();
        $this->nisn = $this->input->post('nisn');
        $this->nomor_induk_lokal = $this->input->post('nomor_induk_lokal');
        $this->nomor_absen = $this->input->post('nomor_absen');
        $this->nama_lengkap = $this->input->post('nama_lengkap');
        $this->jenis_kelamin = $this->input->post('jenis_kelamin');
        $this->kelas = $this->input->post('kelas');
        $this->sub_kelas = $this->input->post('sub_kelas');
        $this->asrama = $this->input->post('asrama');
        $this->wali = $this->input->post('wali');
        $this->pembina = $this->input->post('pembina');
        $this->tempat_lahir = $this->input->post('tempat_lahir');
        $this->tanggal_lahir = $this->input->post('tanggal_lahir');
        $this->alamat = $this->br2nl($this->input->post('alamat'));
        $this->provinsi = $this->input->post('provinsi');
        $this->kabupaten_kota = $this->input->post('kabupaten_kota');
        $this->tanggal_masuk = $this->input->post('tanggal_masuk');
        $this->golongan_darah = $this->input->post('golongan_darah');
        $this->berat_badan = $this->clean_number($this->input->post('berat_badan'));
        $this->tinggi_badan = $this->input->post('tinggi_badan');
        $this->ayah_nama = $this->input->post('ayah_nama');
        $this->ayah_alamat = $this->br2nl($this->input->post('ayah_alamat'));
        $this->ayah_provinsi = $this->input->post('ayah_provinsi');
        $this->ayah_kabupaten_kota = $this->input->post('ayah_kabupaten_kota');
        $this->ayah_hp = $this->input->post('ayah_hp');
        $this->ibu_nama = $this->input->post('ibu_nama');
        $this->ibu_alamat = $this->br2nl($this->input->post('ibu_alamat'));
        $this->ibu_provinsi = $this->input->post('ibu_provinsi');
        $this->ibu_kabupaten_kota = $this->input->post('ibu_kabupaten_kota');
        $this->ibu_hp = $this->input->post('ibu_hp');
        $this->wali_nama = $this->input->post('wali_nama');
        $this->wali_hubungan = $this->input->post('wali_hubungan');
        $this->wali_provinsi = $this->input->post('wali_provinsi');
        $this->wali_kabupaten_kota = $this->input->post('wali_kabupaten_kota');
        $this->wali_hp = $this->input->post('wali_hp');
        $this->created_by = $this->session->userdata('user')['id'];
        $this->db->insert($this->_table, $this);

        $response = array('status' => true, 'data' => 'Data has been saved.');
      } else {
        $response = array('status' => false, 'data' => 'Failed to create user data.');
      };
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function insertBatch($data)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->insert_batch($this->_table, $data);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function update($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $temp = $this->getDetail(['id' => $id]);

      $this->nisn = $this->input->post('nisn');
      $this->nomor_induk_lokal = $this->input->post('nomor_induk_lokal');
      $this->nomor_absen = $this->input->post('nomor_absen');
      $this->nama_lengkap = $this->input->post('nama_lengkap');
      $this->jenis_kelamin = $this->input->post('jenis_kelamin');
      $this->kelas = $this->input->post('kelas');
      $this->sub_kelas = $this->input->post('sub_kelas');
      $this->asrama = $this->input->post('asrama');
      $this->wali = $this->input->post('wali');
      $this->pembina = $this->input->post('pembina');
      $this->tempat_lahir = $this->input->post('tempat_lahir');
      $this->tanggal_lahir = $this->input->post('tanggal_lahir');
      $this->alamat = $this->br2nl($this->input->post('alamat'));
      $this->provinsi = $this->input->post('provinsi');
      $this->kabupaten_kota = $this->input->post('kabupaten_kota');
      $this->tanggal_masuk = $this->input->post('tanggal_masuk');
      $this->golongan_darah = $this->input->post('golongan_darah');
      $this->berat_badan = $this->clean_number($this->input->post('berat_badan'));
      $this->tinggi_badan = $this->input->post('tinggi_badan');
      $this->ayah_nama = $this->input->post('ayah_nama');
      $this->ayah_alamat = $this->br2nl($this->input->post('ayah_alamat'));
      $this->ayah_provinsi = $this->input->post('ayah_provinsi');
      $this->ayah_kabupaten_kota = $this->input->post('ayah_kabupaten_kota');
      $this->ayah_hp = $this->input->post('ayah_hp');
      $this->ibu_nama = $this->input->post('ibu_nama');
      $this->ibu_alamat = $this->br2nl($this->input->post('ibu_alamat'));
      $this->ibu_provinsi = $this->input->post('ibu_provinsi');
      $this->ibu_kabupaten_kota = $this->input->post('ibu_kabupaten_kota');
      $this->ibu_hp = $this->input->post('ibu_hp');
      $this->wali_nama = $this->input->post('wali_nama');
      $this->wali_hubungan = $this->input->post('wali_hubungan');
      $this->wali_provinsi = $this->input->post('wali_provinsi');
      $this->wali_kabupaten_kota = $this->input->post('wali_kabupaten_kota');
      $this->wali_hp = $this->input->post('wali_hp');
      $this->updated_at = date('Y-m-d H:i:s');
      $this->updated_by = $this->session->userdata('user')['id'];
      $this->db->update($this->_table, $this, ['id' => $id]);

      // Update in user
      $user = [
        'username' => $this->nisn,
        'nama_lengkap' => $this->nama_lengkap
      ];
      $this->db->where(['username' => $temp->nisn])->update('user', $user);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function delete($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->delete($this->_table, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  public function truncate()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->truncate($this->_table);

      $response = array('status' => true, 'data' => 'Data has been truncated.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to truncate your data.');
    };

    return $response;
  }

  public function checkData()
  {
    $response = array('status' => false, 'data' => 'Fungsi ini hanya tersedia untuk Santri.');
    $userId = $this->session->userdata('user')['id'];
    $role = $this->session->userdata('user')['role'];

    if ($role === 'Santri') {
      try {
        $data = $this->getDetail(['id' => $userId]);

        if ($data) {
          $fieldRequired = array(
            'nisn' => 'NISN',
            'nama_lengkap' => 'Nama Lengkap',
            'jenis_kelamin' => 'Jenis Kelamin',
            'kelas' => 'Kelas',
            'sub_kelas' => 'Sub Kelas',
            'tempat_lahir' => 'Tempat Lahir',
            'tanggal_lahir' => 'Tanggal Lahir',
            'pembina' => 'Pembina',
            'alamat' => 'Alamat',
            'provinsi' => 'Provinsi',
            'kabupaten_kota' => 'Kabupaten / Kota',
            'ayah_nama' => 'Nama Ayah',
            'ayah_alamat' => 'Alamat Ayah',
            'ibu_nama' => 'Nama Ibu',
            'ibu_alamat' => 'Alamat Ibu'
          );
          $fieldIsEmpty = array();

          foreach ($fieldRequired as $key => $label) {
            $value = htmlspecialchars(trim($data->{$key}));

            if (empty($value) || is_null($value)) {
              $fieldIsEmpty[] = $label;
            };
          };

          if (count($fieldIsEmpty) > 0) {
            // HTML response
            $listRequired = '<ol class="mt-2">';
            foreach ($fieldIsEmpty as $key => $item) {
              $listRequired .= '<li>' . $item . '</li>';
            };
            $listRequired .= '</ol>';

            $html = '
              <div class="alert alert-danger" style="margin-bottom: 2rem;">
                <h4 class="alert-heading">Peringatan!</h4>
                <p>Data diri ananda belum lengkap, <a href="' . base_url('profile') . '" class="alert-link">klik disini</a> untuk melengkapinya.</p>
                <hr/>
                <div class="mb-0">
                  <a href="' . base_url('profile') . '" class="btn btn-light btn-sm"><i class="zmdi zmdi-check-circle"></i> Lengkapi Sekarang</a>
                  <a class="btn btn-light btn-sm" data-toggle="collapse" href="#collapseCheckSantriData" role="button" aria-expanded="false" aria-controls="collapseCheckSantriData">Rincian</a>
                  <div class="collapse mt-3" id="collapseCheckSantriData">
                    <div class="card card-body m-0 p-3" style="color: #333;">
                      Data yang harus ananda lengkapi di antaranya :
                      ' . $listRequired . '
                    </div>
                  </div>
                </div>
              </div>
            ';
            // END ## HTML response

            $response = array('status' => true, 'data' => $html);
          } else {
            $response = array('status' => false, 'data' => 'Terima kasih, ananda sudah melengkapi data diri.');
          };
        } else {
          $response = array('status' => false, 'data' => 'Maaf, data mengenai ananda tidak tersedia.');
        };
      } catch (\Throwable $th) {
        $response = array('status' => false, 'data' => 'Terjadi kesalahan ketika mengambil data.');
      };
    };

    return (object) $response;
  }

  public function setQrCode($id, $file_path)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->qrcode = $file_path;
      $this->updated_at = date('Y-m-d H:i:s');
      $this->updated_by = $this->session->userdata('user')['id'];
      $this->db->update($this->_table, $this, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  function br2nl($text)
  {
    return str_replace("\r\n", '<br/>', htmlspecialchars_decode($text));
  }

  function clean_number($number)
  {
    return preg_replace('/[^0-9]/', '', $number);
  }
}
