<?php
defined('BASEPATH') or exit('No direct script access allowed');

class MappingKelasModel extends CI_Model
{
  private $_table = 'mapping_kelas';
  private $_tableView = 'view_mapping_kelas';

  public function rules()
  {
    return array(
      [
        'field' => 'kelas_id',
        'label' => 'Kelas',
        'rules' => 'required|trim|integer'
      ],
      [
        'field' => 'sub_kelas_id',
        'label' => 'Sub Kelas',
        'rules' => 'required|trim|integer'
      ],
      [
        'field' => 'santri_id',
        'label' => 'Santri',
        'rules' => 'required|trim|integer'
      ],
      [
        'field' => 'nil',
        'label' => 'Nomor Induk Lokal',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'asrama',
        'label' => 'Asrama',
        'rules' => 'required|trim'
      ],
    );
  }

  public function getAll($params = [], $order = [])
  {
    if (count($order) === 2) {
      $this->db->order_by($order[0], $order[1]);
    };
    $this->db->where($params);
    return $this->db->get($this->_tableView)->result();
  }

  public function getDetail($params = [])
  {
    return $this->db->where($params)->get($this->_tableView)->row();
  }

  public function insert()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->semester = $this->input->post('semester');
      $this->kelas = json_encode($this->input->post('kelas'));
      $this->created_by = $this->session->userdata('user')['id'];
      $this->db->insert($this->_table, $this);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function insertBatch($data)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->insert_batch($this->_table, $data);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function update($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->semester = $this->input->post('semester');
      $this->kelas = json_encode($this->input->post('kelas'));
      $this->updated_at = date('Y-m-d H:i:s');
      $this->updated_by = $this->session->userdata('user')['id'];
      $this->db->update($this->_table, $this, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function publishByKelas($kelas = null, $subKelas = null)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      // Set status
      $this->status = 'Publish';
      $this->updated_at = date('Y-m-d H:i:s');
      $this->updated_by = $this->session->userdata('user')['id'];
      $this->db->update($this->_table, $this, ['kelas_id' => $kelas, 'sub_kelas_id' => $subKelas]);

      // Sync with santri
      $syncSantriQuery = "
        UPDATE santri
        INNER JOIN mapping_kelas m ON m.santri_id = santri.id
        INNER JOIN kelas k ON k.id = m.kelas_id
        INNER JOIN sub_kelas sk ON sk.id = m.sub_kelas_id
        SET santri.kelas = k.nama, santri.sub_kelas = sk.nama, santri.nomor_induk_lokal = m.nil, santri.asrama = m.asrama
        WHERE m.kelas_id = '$kelas' AND m.sub_kelas_id = '$subKelas'
      ";
      $this->db->query($syncSantriQuery);

      $response = array('status' => true, 'data' => 'Data has been published.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to publish your data.');
    };

    return $response;
  }

  public function delete($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->delete($this->_table, ['id' => $id]);
      $this->db->delete('jadwal_pelajaran_item', ['jadwal_pelajaran_id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  public function deleteByKelas($kelas = null, $subKelas = null)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->delete($this->_table, ['kelas_id' => $kelas, 'sub_kelas_id' => $subKelas]);

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  public function truncate()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->truncate($this->_table);

      $response = array('status' => true, 'data' => 'Data has been truncated.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to truncate your data.');
    };

    return $response;
  }

  function br2nl($text)
  {
    return str_replace("\r\n", '<br/>', htmlspecialchars_decode($text));
  }

  function clean_number($number)
  {
    return preg_replace('/[^0-9]/', '', $number);
  }
}
