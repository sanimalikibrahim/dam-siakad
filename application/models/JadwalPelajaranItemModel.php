<?php
defined('BASEPATH') or exit('No direct script access allowed');

class JadwalPelajaranItemModel extends CI_Model
{
  private $_table = 'jadwal_pelajaran_item';
  private $_tableView = 'view_jadwal_pelajaran_item';

  public function rules($id)
  {
    return array(
      [
        'field' => 'jadwal_pelajaran_id',
        'label' => 'Jadwal Pelajaran ID',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'hari_nomor',
        'label' => 'Hari',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'sesi',
        'label' => 'Sesi',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'jam_mulai',
        'label' => 'Jam Mulai',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'jam_selesai',
        'label' => 'Jam Selesai',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'mata_pelajaran_id',
        'label' => 'Mata Pelajaran',
        'rules' => [
          'required',
          [
            'mapel_validate',
            function () use ($id) {
              return $this->_mapel_validate($id);
            }
          ]
        ]
      ],
    );
  }

  private function _mapel_validate($id)
  {
    $jadwal_pelajaran_id = $this->input->post('jadwal_pelajaran_id');
    $mata_pelajaran_id = $this->input->post('mata_pelajaran_id');
    $hari_nomor = $this->input->post('hari_nomor');
    $mapel = $this->db->where([
      'id !=' => $id,
      'jadwal_pelajaran_id' => $jadwal_pelajaran_id,
      'hari_nomor' => $hari_nomor,
      'mata_pelajaran_id' => $mata_pelajaran_id
    ])->get($this->_tableView)->result();

    if (count($mapel) > 0) {
      $this->form_validation->set_message('mapel_validate', 'Mata Pelajaran already used in this day.');
      return false;
    } else {
      return true;
    };
  }

  public function getAll($params = [], $orderField = null, $orderBy = 'asc')
  {
    $this->db->where($params);

    if (!is_null($orderField)) {
      $this->db->order_by($orderField, $orderBy);
    };

    return $this->db->get($this->_tableView)->result();
  }

  public function getAll_sesi($kelas = null)
  {
    $query = "
      SELECT DISTINCT sesi
      FROM $this->_tableView
      WHERE jadwal_pelajaran_kelas LIKE '%$kelas%'
    ";
    return $this->db->query($query)->result();
  }

  public function getDetail($params = [])
  {
    return $this->db->where($params)->get($this->_tableView)->row();
  }

  public function insert()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->jadwal_pelajaran_id = $this->input->post('jadwal_pelajaran_id');
      $this->mata_pelajaran_id = $this->input->post('mata_pelajaran_id');
      $this->sesi = $this->input->post('sesi');
      $this->hari_nomor = $this->input->post('hari_nomor');
      $this->hari = $this->input->post('hari');
      $this->jam_mulai = $this->input->post('jam_mulai');
      $this->jam_selesai = $this->input->post('jam_selesai');
      $this->pengajar_id = $this->input->post('pengajar_id');
      $this->ruangan = $this->input->post('ruangan');
      $this->created_by = $this->session->userdata('user')['id'];
      $this->db->insert($this->_table, $this);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function insertBatch($data)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->insert_batch($this->_table, $data);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function update($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->jadwal_pelajaran_id = $this->input->post('jadwal_pelajaran_id');
      $this->mata_pelajaran_id = $this->input->post('mata_pelajaran_id');
      $this->sesi = $this->input->post('sesi');
      $this->hari_nomor = $this->input->post('hari_nomor');
      $this->hari = $this->input->post('hari');
      $this->jam_mulai = $this->input->post('jam_mulai');
      $this->jam_selesai = $this->input->post('jam_selesai');
      $this->pengajar_id = $this->input->post('pengajar_id');
      $this->ruangan = $this->input->post('ruangan');
      $this->updated_at = date('Y-m-d H:i:s');
      $this->updated_by = $this->session->userdata('user')['id'];
      $this->db->update($this->_table, $this, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function delete($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->delete($this->_table, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  public function truncate()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->truncate($this->_table);

      $response = array('status' => true, 'data' => 'Data has been truncated.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to truncate your data.');
    };

    return $response;
  }

  function br2nl($text)
  {
    return str_replace("\r\n", '<br/>', htmlspecialchars_decode($text));
  }

  function clean_number($number)
  {
    return preg_replace('/[^0-9]/', '', $number);
  }
}
