<?php
defined('BASEPATH') or exit('No direct script access allowed');

class TimelineitemModel extends CI_Model
{
  private $_table = 'timeline_item';
  private $_tableView = '';

  public function rules()
  {
    return array(
      [
        'field' => 'timeline_id',
        'label' => 'Timeline ID',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'tanggal_mulai',
        'label' => 'Tanggal Mulai',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'tanggal_selesai',
        'label' => 'Tanggal Selesai',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'judul',
        'label' => 'Judul',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'deskripsi',
        'label' => 'Deskripsi',
        'rules' => 'required|trim'
      ]
    );
  }

  public function getAll($params = [])
  {
    return $this->db->where($params)->get($this->_table)->result();
  }

  public function getDetail($params = [])
  {
    return $this->db->where($params)->get($this->_table)->row();
  }

  public function insert()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $_iconPost = $this->input->post('icon');
      $_iconBgColorPost = $this->input->post('icon_bg_color');
      $_icon = (!empty($_iconPost) && !is_null($_iconPost)) ? $_iconPost : 'zmdi zmdi-calendar';
      $_iconBgColor = (!empty($_iconBgColorPost) && !is_null($_iconBgColorPost)) ? $_iconBgColorPost : '#75cd65';

      $this->timeline_id = $this->input->post('timeline_id');
      $this->judul = $this->input->post('judul');
      $this->deskripsi = $this->input->post('deskripsi');
      $this->tanggal_mulai = $this->input->post('tanggal_mulai');
      $this->tanggal_selesai = $this->input->post('tanggal_selesai');
      $this->icon = $_icon;
      $this->icon_bg_color = $_iconBgColor;
      $this->created_by = $this->session->userdata('user')['id'];
      $this->db->insert($this->_table, $this);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function insertBatch($data)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->insert_batch($this->_table, $data);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function update($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $_iconPost = $this->input->post('icon');
      $_iconBgColorPost = $this->input->post('icon_bg_color');
      $_icon = (!empty($_iconPost) && !is_null($_iconPost)) ? $_iconPost : 'zmdi zmdi-calendar';
      $_iconBgColor = (!empty($_iconBgColorPost) && !is_null($_iconBgColorPost)) ? $_iconBgColorPost : '#75cd65';

      $this->timeline_id = $this->input->post('timeline_id');
      $this->judul = $this->input->post('judul');
      $this->deskripsi = $this->input->post('deskripsi');
      $this->tanggal_mulai = $this->input->post('tanggal_mulai');
      $this->tanggal_selesai = $this->input->post('tanggal_selesai');
      $this->icon = $_icon;
      $this->icon_bg_color = $_iconBgColor;
      $this->updated_at = date('Y-m-d H:i:s');
      $this->updated_by = $this->session->userdata('user')['id'];
      $this->db->update($this->_table, $this, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function delete($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->delete($this->_table, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  public function truncate()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->truncate($this->_table);

      $response = array('status' => true, 'data' => 'Data has been truncated.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to truncate your data.');
    };

    return $response;
  }

  function br2nl($text)
  {
    return str_replace("\r\n", '<br/>', htmlspecialchars_decode($text));
  }

  function clean_number($number)
  {
    return preg_replace('/[^0-9]/', '', $number);
  }
}
