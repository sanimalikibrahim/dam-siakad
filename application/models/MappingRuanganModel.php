<?php
defined('BASEPATH') or exit('No direct script access allowed');

class MappingRuanganModel extends CI_Model
{
  private $_table = 'mapping_ruangan';
  private $_tableView = '';

  public function rules()
  {
    return array(
      [
        'field' => 'ruangan_id',
        'label' => 'Ruangan ID',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'psb_id',
        'label' => 'PSB ID',
        'rules' => 'required|trim'
      ],
    );
  }

  public function getAllHead($tipe = null, $year = null)
  {
    $query = "
      SELECT
        t.id,
        t.nama_ruangan,
        t.kapasitas,
        t.jenis,
        t.tipe,
        (
          SELECT COUNT(id)
          FROM mapping_ruangan WHERE ruangan_id = t.id AND tahun = '$year'
        ) AS user_assigned
      FROM ruangan t
      WHERE LOWER(t.tipe) = LOWER('$tipe')
      GROUP BY t.id, t.nama_ruangan, t.kapasitas, t.jenis, t.tipe
    ";
    return $this->db->query($query)->result();
  }

  public function getLastUpdated($year = null)
  {
    $data = $this->db->where(array('tahun' => $year))->get($this->_table)->row();
    $lastUpdated = '-';

    if (!is_null($data)) {
      $lastUpdated = $data->created_at;
    };

    return $lastUpdated;
  }

  public function getAll($params = [])
  {
    return $this->db->where($params)->get($this->_table)->result();
  }

  public function getDetail($params = [])
  {
    return $this->db->where($params)->get($this->_table)->row();
  }

  public function getDetailByRuangan($ruangan_id = null, $year = null)
  {
    $query = "
      SELECT
        t.id,
        t.ruangan_id,
        r.nama_ruangan,
        r.jenis,
        r.tipe,
        t.psb_id,
        p.nisn,
        p.nama_lengkap,
        p.jenis_kelamin,
        t.nomor_peserta,
        t.tahun,
        t.created_at,
        t.created_by
      FROM mapping_ruangan t
      LEFT JOIN ruangan r ON r.id = t.ruangan_id
      LEFT JOIN psb p ON p.id = t.psb_id
      WHERE t.ruangan_id = '$ruangan_id' AND t.tahun = '$year'
      ORDER BY nomor_peserta ASC
    ";
    return $this->db->query($query)->result();
  }

  public function getDetailByRuangan_toUpper($ruangan_id = null, $year = null)
  {
    $query = "
      SELECT
        t.id,
        t.ruangan_id,
        UPPER(r.nama_ruangan) AS nama_ruangan,
        UPPER(r.jenis) AS jenis,
        UPPER(r.tipe) AS tipe,
        t.psb_id,
        p.nisn,
        UPPER(p.nama_lengkap) AS nama_lengkap,
        UPPER(p.jenis_kelamin) AS jenis_kelamin,
        t.nomor_peserta,
        t.tahun,
        t.created_at,
        t.created_by
      FROM mapping_ruangan t
      LEFT JOIN ruangan r ON r.id = t.ruangan_id
      LEFT JOIN psb p ON p.id = t.psb_id
      WHERE t.ruangan_id = '$ruangan_id' AND t.tahun = '$year'
      ORDER BY nomor_peserta ASC
    ";
    return $this->db->query($query)->result();
  }

  public function getRuanganByPsb($psb_id = null)
  {
    $nomorPeserta = '-';
    $ruanganTesLisan = '-';
    $ruanganTesTulis = '-';
    $ruanganTesOnline = '-';

    $query = "
      SELECT t.nomor_peserta, r.tipe, r.nama_ruangan
      FROM mapping_ruangan t
      LEFT JOIN ruangan r ON r.id = t.ruangan_id
      WHERE t.psb_id = '$psb_id'
    ";
    $data = $this->db->query($query)->result();

    if (count($data) > 0) {
      foreach ($data as $index => $item) {
        $nomorPeserta = $item->nomor_peserta;

        if (strtolower($item->tipe) == strtolower('tes lisan')) {
          $ruanganTesLisan = $item->nama_ruangan;
        } else if (strtolower($item->tipe) == strtolower('tes tulis')) {
          $ruanganTesTulis = $item->nama_ruangan;
        } else if (strtolower($item->tipe) == strtolower('tes online')) {
          $ruanganTesOnline = $item->nama_ruangan;
        };
      };
    };

    return (object) array(
      'nomor_peserta' => $nomorPeserta,
      'tes_lisan' => $ruanganTesLisan,
      'tes_tulis' => $ruanganTesTulis,
      'tes_online' => $ruanganTesOnline,
    );
  }

  public function insert()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->ruangan_id = $this->input->post('ruangan_id');
      $this->psb_id = $this->input->post('psb_id');
      $this->nomor_peserta = $this->input->post('nomor_peserta');
      $this->tahun = $this->input->post('tahun');
      $this->created_by = $this->session->userdata('user')['id'];
      $this->db->insert($this->_table, $this);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function insertBatch($data)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->insert_batch($this->_table, $data);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function update($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->ruangan_id = $this->input->post('ruangan_id');
      $this->psb_id = $this->input->post('psb_id');
      $this->nomor_peserta = $this->input->post('nomor_peserta');
      $this->tahun = $this->input->post('tahun');
      $this->db->update($this->_table, $this, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function updateNomorPeserta_online($year = null)
  {
    $query = "
      UPDATE mapping_ruangan t
      JOIN ruangan r ON r.id = t.ruangan_id
      SET nomor_peserta = CONCAT('R', r.nama_ruangan, '-', t.nomor_peserta)
      WHERE t.tahun = '$year'
    ";
    return $this->db->query($query);
  }

  public function delete($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->delete($this->_table, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  public function deleteByTahun($tahun = null)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->delete($this->_table, ['tahun' => $tahun]);

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  public function truncate()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->truncate($this->_table);

      $response = array('status' => true, 'data' => 'Data has been truncated.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to truncate your data.');
    };

    return $response;
  }

  function br2nl($text)
  {
    return str_replace("\r\n", '<br/>', htmlspecialchars_decode($text));
  }

  function clean_number($number)
  {
    return preg_replace('/[^0-9]/', '', $number);
  }
}
