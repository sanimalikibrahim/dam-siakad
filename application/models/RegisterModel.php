<?php
defined('BASEPATH') or exit('No direct script access allowed');

class RegisterModel extends CI_Model
{
  private $_table = 'user';

  public function rules_santri()
  {
    return array(
      [
        'field' => 'nisn',
        'label' => 'NISN',
        'rules' => [
          'required',
          'trim',
          'numeric',
          [
            'username_exist',
            function ($username) {
              return $this->_username_exist($username);
            }
          ]
        ]
      ],
      [
        'field' => 'nama_lengkap',
        'label' => 'Nama Lengkap',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'kelas',
        'label' => 'Kelas',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'sub_kelas',
        'label' => 'Sub Kelas',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'pembina',
        'label' => 'Pembina',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'email',
        'label' => 'Email',
        'rules' => [
          'required',
          'trim',
          'valid_email',
          [
            'email_exist',
            function ($email) {
              return $this->_email_exist($email);
            }
          ]
        ]
      ],
      [
        'field' => 'password',
        'label' => 'Password',
        'rules' => 'required'
      ],
      [
        'field' => 'password_confirm',
        'label' => 'Confirm Password',
        'rules' => [
          'required',
          [
            'password_match',
            function ($password_confirm) {
              return $this->_password_match($password_confirm);
            }
          ]
        ]
      ],
    );
  }

  private function _email_exist($email)
  {
    $temp = $this->db->where(['email' => $email])->get($this->_table);

    if ($temp->num_rows() > 0) {
      $this->form_validation->set_message('email_exist', 'Email "' . $email . '" has been used by other user.');
      return false;
    } else {
      return true;
    };
  }

  private function _username_exist($username)
  {
    $temp = $this->db->where(['username' => $username])->get($this->_table);

    if ($temp->num_rows() > 0) {
      $this->form_validation->set_message('username_exist', 'NISN "' . $username . '" has been used by other user.');
      return false;
    } else {
      return true;
    };
  }

  private function _password_match($password_confirm)
  {
    $password = $this->input->post('password');

    if ($password_confirm !== $password) {
      $this->form_validation->set_message('password_match', 'Confirm Password is not match.');
      return false;
    } else {
      return true;
    };
  }

  public function insert_santri()
  {
    $response = array('status' => false, 'data' => 'No operation.', 'email' => null);

    try {
      $this->username = $this->input->post('nisn');
      $this->nama_lengkap = $this->input->post('nama_lengkap');
      $this->email = $this->input->post('email');
      $this->password = md5($this->input->post('password'));
      $this->role = 'Santri';

      if ($this->db->insert($this->_table, $this)) {
        // Insert to santri
        $santri = [
          'id' => $this->db->insert_id(),
          'nisn' => $this->input->post('nisn'),
          'nama_lengkap' => $this->input->post('nama_lengkap'),
          'kelas' => $this->input->post('kelas'),
          'sub_kelas' => $this->input->post('sub_kelas'),
          'asrama' => $this->input->post('asrama'),
          'pembina' => $this->input->post('pembina')
        ];
        $this->db->insert('santri', $santri);

        $response = array('status' => true, 'data' => 'Successfully create account.', 'email' => $this->input->post('email'));
      } else {
        $response = array('status' => false, 'data' => 'Failed to create your account, try again later.', 'email' => $this->input->post('email'));
      };
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to create your account, try again later.', 'email' => $this->input->post('email'));
    };

    return $response;
  }
}
