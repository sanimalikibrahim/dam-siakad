<?php
defined('BASEPATH') or exit('No direct script access allowed');

class JadwalshalatModel extends CI_Model
{
  private $_table = 'jadwal_shalat';
  private $_tableView = '';

  public function rules($id = null)
  {
    return array(
      [
        'field' => 'jenis',
        'label' => 'Jenis',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'nama',
        'label' => 'Nama',
        'rules' => [
          'required',
          'trim',
          [
            'nama_exist',
            function ($nama) use ($id) {
              return $this->_nama_exist($nama, $id);
            }
          ]
        ]
      ],
      [
        'field' => 'mulai',
        'label' => 'Mulai',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'selesai',
        'label' => 'Selesai',
        'rules' => 'required|trim'
      ]
    );
  }

  private function _nama_exist($nama, $id)
  {
    $jenis = $this->input->post('jenis');
    $temp = $this->db->where(['id !=' => $id, 'nama' => $nama, 'jenis' => $jenis])->get($this->_table);

    if ($temp->num_rows() > 0) {
      $this->form_validation->set_message('nama_exist', 'Nama "' . $nama . '" already in use.');
      return false;
    } else {
      return true;
    };
  }

  public function getAll_list($params = [])
  {
    $data = $this->db->where($params)->get($this->_table)->result();
    $response = [];

    if (count($data) > 0) {
      foreach ($data as $key => $item) {
        $response[$item->nama] = (object) [
          'mulai' => $item->mulai,
          'selesai' => $item->selesai
        ];
      };
    };

    return (object) $response;
  }

  public function getAll($params = [])
  {
    return $this->db->where($params)->get($this->_table)->result();
  }

  public function getDetail($params = [])
  {
    return $this->db->where($params)->get($this->_table)->row();
  }

  public function insert()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->jenis = $this->input->post('jenis');
      $this->nama = $this->input->post('nama');
      $this->mulai = $this->input->post('mulai');
      $this->selesai = $this->input->post('selesai');
      $this->created_by = $this->session->userdata('user')['id'];
      $this->db->insert($this->_table, $this);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function insertBatch($data)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->insert_batch($this->_table, $data);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function update($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->jenis = $this->input->post('jenis');
      $this->nama = $this->input->post('nama');
      $this->mulai = $this->input->post('mulai');
      $this->selesai = $this->input->post('selesai');
      $this->updated_at = date('Y-m-d H:i:s');
      $this->updated_by = $this->session->userdata('user')['id'];
      $this->db->update($this->_table, $this, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function delete($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->delete($this->_table, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  public function truncate()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->truncate($this->_table);

      $response = array('status' => true, 'data' => 'Data has been truncated.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to truncate your data.');
    };

    return $response;
  }

  function br2nl($text)
  {
    return str_replace("\r\n", '<br/>', htmlspecialchars_decode($text));
  }

  function clean_number($number)
  {
    return preg_replace('/[^0-9]/', '', $number);
  }
}
