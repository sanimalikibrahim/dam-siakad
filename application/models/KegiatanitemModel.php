<?php
defined('BASEPATH') or exit('No direct script access allowed');

class KegiatanitemModel extends CI_Model
{
  private $_table = 'kegiatan_item';
  private $_tableView = 'view_kegiatan_item';

  public function rules()
  {
    return array(
      [
        'field' => 'kegiatan_id',
        'label' => 'Kegiatan',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'slug',
        'label' => 'Slug',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'nama',
        'label' => 'Nama',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'tipe_jawaban',
        'label' => 'Tipe Jawaban',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'aktif',
        'label' => 'Aktif',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'option1',
        'label' => 'Option 1',
        'rules' => [
          'trim',
          [
            'option1_required',
            function ($option1) {
              return $this->_option1_required($option1);
            }
          ]
        ]
      ],
      [
        'field' => 'option2',
        'label' => 'Option 2',
        'rules' => [
          'trim',
          [
            'option2_required',
            function ($option2) {
              return $this->_option2_required($option2);
            }
          ]
        ]
      ]
    );
  }

  private function _option1_required($option1)
  {
    $tipe_jawaban = $this->input->post('tipe_jawaban');

    if (in_array($tipe_jawaban, ['Pilihan Ganda Single']) && empty($option1)) {
      $this->form_validation->set_message('option1_required', 'Then Option 1 field is required.');
      return false;
    } else {
      return true;
    };
  }

  private function _option2_required($option2)
  {
    $tipe_jawaban = $this->input->post('tipe_jawaban');

    if (in_array($tipe_jawaban, ['Pilihan Ganda Single']) && empty($option2)) {
      $this->form_validation->set_message('option2_required', 'Then Option 2 field is required.');
      return false;
    } else {
      return true;
    };
  }

  public function getAll($params = [])
  {
    return $this->db->where($params)->get($this->_tableView)->result();
  }

  public function getDetail($params = [])
  {
    return $this->db->where($params)->get($this->_tableView)->row();
  }

  public function insert()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->kegiatan_id = $this->input->post('kegiatan_id');
      $this->nama = $this->input->post('nama');
      $this->tipe_jawaban = $this->input->post('tipe_jawaban');
      $this->option1 = $this->input->post('option1');
      $this->option2 = $this->input->post('option2');
      $this->option3 = $this->input->post('option3');
      $this->option4 = $this->input->post('option4');
      $this->option5 = $this->input->post('option5');
      $this->aktif = $this->input->post('aktif');
      $this->slug = $this->input->post('slug');
      $this->created_by = $this->session->userdata('user')['id'];
      $this->db->insert($this->_table, $this);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function insertBatch($data)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->insert_batch($this->_table, $data);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function update($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->kegiatan_id = $this->input->post('kegiatan_id');
      $this->nama = $this->input->post('nama');
      $this->tipe_jawaban = $this->input->post('tipe_jawaban');
      $this->option1 = $this->input->post('option1');
      $this->option2 = $this->input->post('option2');
      $this->option3 = $this->input->post('option3');
      $this->option4 = $this->input->post('option4');
      $this->option5 = $this->input->post('option5');
      $this->aktif = $this->input->post('aktif');
      $this->slug = $this->input->post('slug');
      $this->updated_at = date('Y-m-d H:i:s');
      $this->updated_by = $this->session->userdata('user')['id'];
      $this->db->update($this->_table, $this, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function delete($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->delete($this->_table, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  public function truncate()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->truncate($this->_table);

      $response = array('status' => true, 'data' => 'Data has been truncated.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to truncate your data.');
    };

    return $response;
  }

  function br2nl($text)
  {
    return str_replace("\r\n", '<br/>', htmlspecialchars_decode($text));
  }

  function clean_number($number)
  {
    return preg_replace('/[^0-9]/', '', $number);
  }
}
