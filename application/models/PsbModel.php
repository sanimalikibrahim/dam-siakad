<?php
defined('BASEPATH') or exit('No direct script access allowed');

class PsbModel extends CI_Model
{
  private $_table = 'psb';
  private $_tableView = '';

  public function rules($id = null)
  {
    return array(
      [
        'field' => 'email',
        'label' => 'Email',
        'rules' => [
          'required', 'trim', 'valid_email',
          [
            'email_exist',
            function ($email) use ($id) {
              return $this->_email_exist($email, $id);
            }
          ]
        ]
      ],
      [
        'field' => 'password',
        'label' => 'Password',
        'rules' => 'required'
      ],
      [
        'field' => 'password_confirm',
        'label' => 'Confirm Password',
        'rules' => [
          'required',
          [
            'password_match',
            function ($password_confirm) {
              return $this->_password_match($password_confirm);
            }
          ]
        ]
      ],
      [
        'field' => 'nama_lengkap',
        'label' => 'Nama Lengkap',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'nisn',
        'label' => 'NISN',
        'rules' => [
          'required', 'trim',
          [
            'nisn_exist',
            function ($nisn) use ($id) {
              return $this->_nisn_exist($nisn, $id);
            }
          ]
        ]
      ],
      [
        'field' => 'tempat_lahir',
        'label' => 'Tempat Lahir',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'tanggal_lahir',
        'label' => 'Tanggal Lahir',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'jenis_kelamin',
        'label' => 'Jenis Kelamin',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'telepon',
        'label' => 'Telepon',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'urutan_anak',
        'label' => 'Urutan Anak',
        'rules' => 'required|trim|integer'
      ],
      [
        'field' => 'jumlah_saudara',
        'label' => 'Jumlah Saudara',
        'rules' => 'required|trim|integer'
      ],
      [
        'field' => 'alamat_santri',
        'label' => 'Alamat Tempat Tinggal Santri',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'kota',
        'label' => 'Kota/Kabupaten',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'provinsi',
        'label' => 'Provinsi',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'tempat_tinggal',
        'label' => 'Tempat Tinggal',
        'rules' => [
          'required', 'trim',
          [
            'tempat_tinggal_check',
            function ($tempat_tinggal) {
              return $this->_tempat_tinggal_check($tempat_tinggal);
            }
          ]
        ]
      ],
      [
        'field' => 'ayah_nama_lengkap',
        'label' => 'Ayah: Nama Lengkap',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'ayah_status',
        'label' => 'Ayah: Status',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'ayah_pekerjaan',
        'label' => 'Ayah: Pekerjaan',
        'rules' => [
          'required', 'trim',
          [
            'ayah_pekerjaan_check',
            function ($ayah_pekerjaan) {
              return $this->_ayah_pekerjaan_check($ayah_pekerjaan);
            }
          ]
        ]
      ],
      [
        'field' => 'ibu_nama_lengkap',
        'label' => 'Ibu: Nama Lengkap',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'ibu_status',
        'label' => 'Ibu: Status',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'ibu_pekerjaan',
        'label' => 'Ibu: Pekerjaan',
        'rules' => [
          'required', 'trim',
          [
            'ibu_pekerjaan_check',
            function ($ibu_pekerjaan) {
              return $this->_ibu_pekerjaan_check($ibu_pekerjaan);
            }
          ]
        ]
      ],
      [
        'field' => 'wali_pekerjaan',
        'label' => 'Wali: Pekerjaan',
        'rules' => [
          'trim',
          [
            'wali_pekerjaan_check',
            function ($wali_pekerjaan) {
              return $this->_wali_pekerjaan_check($wali_pekerjaan);
            }
          ]
        ]
      ],
      [
        'field' => 'ukuran_baju',
        'label' => 'Ukuran Baju',
        'rules' => [
          'trim',
          [
            'ukuran_baju_check',
            function ($ukuran_baju) use ($id) {
              return $this->_ukuran_baju_check($ukuran_baju, $id);
            }
          ]
        ]
      ],
      [
        'field' => 'ukuran_celana',
        'label' => 'Celana / Rok',
        'rules' => [
          'trim',
          [
            'ukuran_celana_check',
            function ($ukuran_celana) use ($id) {
              return $this->_ukuran_celana_check($ukuran_celana, $id);
            }
          ]
        ]
      ],
      [
        'field' => 'prestasi',
        'label' => 'Prestasi',
        'rules' => [
          [
            'prestasi_check',
            function () {
              return $this->_prestasi_check();
            }
          ]
        ]
      ],
      [
        'field' => 'komitmen',
        'label' => 'Komitmen',
        'rules' => [
          'required', 'trim',
          [
            'komitmen_check',
            function ($komitmen) {
              return $this->_komitmen_check($komitmen);
            }
          ]
        ]
      ],
      [
        'field' => 'sekolah_asal',
        'label' => 'Nama Sekolah Asal',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'sekolah_asal_alamat',
        'label' => 'Kota/Kabupaten Sekolah Asal',
        'rules' => 'required|trim'
      ],
    );
  }

  public function rulesAkun($id = null)
  {
    return array(
      [
        'field' => 'nisn',
        'label' => 'NISN',
        'rules' => [
          'required', 'trim', 'min_length[8]',
          [
            'nisn_exist',
            function ($nisn) use ($id) {
              return $this->_nisn_exist($nisn, $id);
            }
          ]
        ]
      ],
      [
        'field' => 'email',
        'label' => 'Email',
        'rules' => [
          'required', 'trim', 'valid_email',
          [
            'email_exist',
            function ($email) use ($id) {
              return $this->_email_exist($email, $id);
            }
          ]
        ]
      ],
      [
        'field' => 'password',
        'label' => 'Password',
        'rules' => 'required|min_length[8]'
      ],
      [
        'field' => 'password_confirm',
        'label' => 'Confirm Password',
        'rules' => [
          'required',
          'min_length[8]',
          [
            'password_match',
            function ($password_confirm) {
              return $this->_password_match($password_confirm);
            }
          ]
        ]
      ],
    );
  }

  public function rulesSantri($id = null)
  {
    return array(
      [
        'field' => 'nama_lengkap',
        'label' => 'Nama Lengkap',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'tempat_lahir',
        'label' => 'Tempat Lahir',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'tanggal_lahir',
        'label' => 'Tanggal Lahir',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'jenis_kelamin',
        'label' => 'Jenis Kelamin',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'telepon',
        'label' => 'Telepon',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'urutan_anak',
        'label' => 'Urutan Anak',
        'rules' => 'required|trim|integer'
      ],
      [
        'field' => 'jumlah_saudara',
        'label' => 'Jumlah Saudara',
        'rules' => 'required|trim|integer'
      ],
      [
        'field' => 'alamat_santri',
        'label' => 'Alamat Tempat Tinggal Santri',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'kota',
        'label' => 'Kota/Kabupaten',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'provinsi',
        'label' => 'Provinsi',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'tempat_tinggal',
        'label' => 'Tempat Tinggal',
        'rules' => [
          'required', 'trim',
          [
            'tempat_tinggal_check',
            function ($tempat_tinggal) {
              return $this->_tempat_tinggal_check($tempat_tinggal);
            }
          ]
        ]
      ],
      [
        'field' => 'sekolah_asal',
        'label' => 'Nama Sekolah Asal',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'sekolah_asal_alamat',
        'label' => 'Kota/Kabupaten Sekolah Asal',
        'rules' => 'required|trim'
      ],
    );
  }

  public function rulesOrangTua($id = null)
  {
    return array(
      [
        'field' => 'ayah_nama_lengkap',
        'label' => 'Ayah: Nama Lengkap',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'ayah_status',
        'label' => 'Ayah: Status',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'ayah_pekerjaan',
        'label' => 'Ayah: Pekerjaan',
        'rules' => [
          'required', 'trim',
          [
            'ayah_pekerjaan_check',
            function ($ayah_pekerjaan) {
              return $this->_ayah_pekerjaan_check($ayah_pekerjaan);
            }
          ]
        ]
      ],
      [
        'field' => 'ibu_nama_lengkap',
        'label' => 'Ibu: Nama Lengkap',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'ibu_status',
        'label' => 'Ibu: Status',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'ibu_pekerjaan',
        'label' => 'Ibu: Pekerjaan',
        'rules' => [
          'required', 'trim',
          [
            'ibu_pekerjaan_check',
            function ($ibu_pekerjaan) {
              return $this->_ibu_pekerjaan_check($ibu_pekerjaan);
            }
          ]
        ]
      ],
      [
        'field' => 'wali_pekerjaan',
        'label' => 'Wali: Pekerjaan',
        'rules' => [
          'trim',
          [
            'wali_pekerjaan_check',
            function ($wali_pekerjaan) {
              return $this->_wali_pekerjaan_check($wali_pekerjaan);
            }
          ]
        ]
      ],
    );
  }

  public function rulesPestasi($id = null)
  {
    return array(
      [
        'field' => 'prestasi',
        'label' => 'Prestasi',
        'rules' => [
          [
            'prestasi_check',
            function () {
              return $this->_prestasi_check();
            }
          ]
        ]
      ],
    );
  }

  private function _password_match($password_confirm)
  {
    $password = $this->input->post('password');

    if ($password_confirm !== $password) {
      $this->form_validation->set_message('password_match', 'Confirm Password tidak sama.');
      return false;
    } else {
      return true;
    };
  }

  private function _email_exist($email, $id)
  {
    $id = (!IS_NULL($id)) ? $id : 0;
    $temp = $this->db->where(['id !=' => $id, 'email' => $email])->get('user');

    if ($temp->num_rows() > 0) {
      $this->form_validation->set_message('email_exist', 'Email "' . $email . '" sudah pernah digunakan.');
      return false;
    } else {
      return true;
    };
  }

  private function _nisn_exist($nisn, $id)
  {
    $id = (!IS_NULL($id)) ? $id : 0;
    $temp = $this->db->where(['id !=' => $id, 'nisn' => $nisn])->get($this->_table);

    if ($temp->num_rows() > 0) {
      $this->form_validation->set_message('nisn_exist', 'NISN "' . $nisn . '" sudah pernah digunakan.');
      return false;
    } else {
      return true;
    };
  }

  private function _tempat_tinggal_check($tempat_tinggal)
  {
    $lainnya = $this->input->post('tempat_tinggal_lainnya');

    if ($tempat_tinggal === 'Lainnya') {
      if (empty($lainnya) || is_null($lainnya)) {
        $this->form_validation->set_message('tempat_tinggal_check', 'The Tempat Tinggal: Lainnya field is required.');
        return false;
      } else {
        return true;
      };
    } else {
      $_POST['tempat_tinggal_lainnya'] = '';
      return true;
    };
  }

  private function _ayah_pekerjaan_check($ayah_pekerjaan)
  {
    if (!in_array($ayah_pekerjaan, ['PNS', 'TNI/POLRI'])) {
      $_POST['ayah_pekerjaan_jenis'] = '';
      $_POST['ayah_pekerjaan_pangkat'] = '';
    };
    return true;
  }

  private function _ibu_pekerjaan_check($ibu_pekerjaan)
  {
    if (!in_array($ibu_pekerjaan, ['PNS', 'TNI/POLRI'])) {
      $_POST['ibu_pekerjaan_jenis'] = '';
      $_POST['ibu_pekerjaan_pangkat'] = '';
    };
    return true;
  }

  private function _wali_pekerjaan_check($wali_pekerjaan)
  {
    if (!in_array($wali_pekerjaan, ['PNS', 'TNI/POLRI'])) {
      $_POST['wali_pekerjaan_jenis'] = '';
      $_POST['wali_pekerjaan_pangkat'] = '';
    };
    return true;
  }

  private function _ukuran_baju_check($ukuran_baju, $id)
  {
    $temp = $this->db->where(['id' => $id])->get('psb')->row();

    if (!is_null($id) && !is_null($temp)) {
      $currentStatus = $temp->status;
    } else {
      $currentStatus = null;
    };

    if (!is_null($currentStatus)) {
      if ($currentStatus == 3) {
        if (is_null($ukuran_baju) || empty($ukuran_baju)) {
          $this->form_validation->set_message('ukuran_baju_check', 'The Ukuran Baju is required.');
          return false;
        };
      };
      return true;
    } else {
      return true;
    };
  }

  private function _ukuran_celana_check($ukuran_celana, $id)
  {
    $temp = $this->db->where(['id' => $id])->get('psb')->row();

    if (!is_null($id) && !is_null($temp)) {
      $currentStatus = $temp->status;
    } else {
      $currentStatus = null;
    };

    if (!is_null($currentStatus)) {
      if ($currentStatus == 3) {
        if (is_null($ukuran_celana) || empty($ukuran_celana)) {
          $this->form_validation->set_message('ukuran_celana_check', 'The Ukuran Celana is required.');
          return false;
        };
      };
      return true;
    } else {
      return true;
    };
  }

  private function _prestasi_check()
  {
    $prestasi = $this->input->post('prestasi');

    if (!is_null($prestasi)) {
      $prestasiCount = (is_array($prestasi)) ? count($prestasi) : 0;

      if ($prestasiCount > 0) {
        $prestasi = $this->re_arrange($prestasi);
        $prestasiFile = $_FILES['prestasi'];
        $prestasiFile = $this->re_arrange($prestasiFile);
        $prestasiFile = $prestasiFile['sertifikat'];
        $errorMessage = '';
        $nomor = 1;

        foreach ($prestasi as $key => $item) {
          if (empty($item['nama_lomba'])) {
            $errorMessage .= '<li>Item(' . $nomor . '): The Nama Lomba field is required.</li>';
          };

          if (empty($item['peringkat'])) {
            $errorMessage .= '<li>Item(' . $nomor . '): The Peringkat field is required.</li>';
          };

          if (empty($item['tahun'])) {
            $errorMessage .= '<li>Item(' . $nomor . '): The Tahun field is required.</li>';
          };

          if (empty($prestasiFile['name'][$key])) {
            $errorMessage .= '<li>Item(' . $nomor . '): The Foto Sertifikat / Piagam field is required.</li>';
          };

          $nomor++;
        };

        if (!empty($errorMessage)) {
          $errorResponse = '
            Prestasi, please correct the following errors:
            <ul>
              ' . $errorMessage . '
            </ul>
          ';

          $this->form_validation->set_message('prestasi_check', $errorResponse);
          return false;
        };
      };
    };

    return true;
  }

  private function _komitmen_check($komitmen)
  {
    $nominal = $this->input->post('komitmen_nominal');

    if ($komitmen === 'Bersedia') {
      if (empty($nominal) || is_null($nominal)) {
        $this->form_validation->set_message('komitmen_check', 'The Komitmen: Nominal field is required.');
        return false;
      } else {
        return true;
      };
    } else {
      $_POST['komitmen_nominal'] = '';
      return true;
    };
  }

  public function getTahunList()
  {
    $this->db->distinct();
    $this->db->select('tahun');
    $this->db->order_by('tahun', 'desc');
    return $this->db->get($this->_table)->result();
  }

  public function getCount($params = [])
  {
    $this->db->where($params);
    $this->db->from($this->_table);
    return $this->db->count_all_results();
  }

  public function getCountByYear($startDate = null, $endDate = null, $status = null)
  {
    $filterStatus = (!is_null($status) && $status != '-1') ? " AND status = '$status'" : "";
    $query = "
      SELECT DATE(created_at) AS tanggal, COUNT(t.id) AS jumlah
      FROM psb t
      WHERE DATE(created_at) BETWEEN '$startDate' AND '$endDate' $filterStatus
      GROUP BY DATE(created_at)
      ORDER BY DATE(created_at) ASC
    ";
    return $this->db->query($query)->result();
  }

  public function getCountByKota($startDate = null, $endDate = null, $status = null)
  {
    $filterStatus = (!is_null($status) && $status != '-1') ? " AND status = '$status'" : "";
    $query = "
        SELECT UPPER(kota) AS kota,
        (
          SELECT COUNT(id)
          FROM psb
          WHERE DATE(created_at) BETWEEN '$startDate' AND '$endDate' $filterStatus
            AND LOWER(kota) = LOWER(t.kota)
            AND LOWER(jenis_kelamin) = LOWER('Laki-laki')
        ) AS laki_laki,
        (
          SELECT COUNT(id)
          FROM psb
          WHERE DATE(created_at) BETWEEN '$startDate' AND '$endDate' $filterStatus
            AND LOWER(kota) = LOWER(t.kota)
            AND LOWER(jenis_kelamin) = LOWER('Perempuan')
        ) AS perempuan
      FROM psb t
      WHERE DATE(created_at) BETWEEN '$startDate' AND '$endDate' $filterStatus
      GROUP BY kota
      ORDER BY kota ASC
    ";
    return $this->db->query($query)->result();
  }

  public function getCountByGender($startDate = null, $endDate = null)
  {
    $query = "
      SELECT
        (
          SELECT COUNT(id)
          FROM psb
          WHERE DATE(created_at) BETWEEN '$startDate' AND '$endDate' AND status = 0
            AND LOWER(jenis_kelamin) = LOWER('Laki-laki')
        ) AS putra_belum_terverifikasi,
        (
          SELECT COUNT(id)
          FROM psb
          WHERE DATE(created_at) BETWEEN '$startDate' AND '$endDate' AND status > 0
            AND LOWER(jenis_kelamin) = LOWER('Laki-laki')
        ) AS putra_terverifikasi,
        (
          SELECT COUNT(id)
          FROM psb
          WHERE DATE(created_at) BETWEEN '$startDate' AND '$endDate' AND status = 0
            AND LOWER(jenis_kelamin) = LOWER('Perempuan')
        ) AS putri_belum_terverifikasi,
        (
          SELECT COUNT(id)
          FROM psb
          WHERE DATE(created_at) BETWEEN '$startDate' AND '$endDate' AND status > 0
            AND LOWER(jenis_kelamin) = LOWER('Perempuan')
        ) AS putri_terverifikasi
    ";
    return $this->db->query($query)->row();
  }

  public function getAll($params = [], $limitStart = null, $limitOffset = null)
  {
    $this->db->where($params);
    $this->db->order_by('nama_lengkap', 'asc');

    if (!is_null($limitStart) && !is_null($limitOffset)) {
      $this->db->limit($limitOffset, $limitStart);
    };

    return $this->db->get($this->_table)->result();
  }

  public function getAllRandom($year = null, $gender = null)
  {
    $query = "
      SELECT *
      FROM " . $this->_table . " AS t1
      WHERE t1.tahun = '$year' AND LOWER(jenis_kelamin) = LOWER('$gender') AND t1.status = '1'
      ORDER BY RAND()
    ";
    return $this->db->query($query)->result();
  }

  public function getDetail($params = [])
  {
    return $this->db->where($params)->get($this->_table)->row();
  }

  public function insert()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $user = [
        'email' => $this->input->post('email'),
        'username' => $this->input->post('nisn'),
        'password' => md5($this->input->post('password')),
        'nama_lengkap' => $this->input->post('nama_lengkap'),
        'profile_photo' => $this->input->post('profile_photo'),
        'learndash_user_ref' => $this->input->post('password'),
        'role' => 'Calon Santri'
      ];

      if ($this->db->insert('user', $user)) {
        $this->id = $this->db->insert_id();
        $this->email = $this->input->post('email');
        $this->nama_lengkap = $this->input->post('nama_lengkap');
        $this->tempat_lahir = $this->input->post('tempat_lahir');
        $this->tanggal_lahir = date('Y-m-d', strtotime($this->input->post('tanggal_lahir')));
        $this->jenis_kelamin = $this->input->post('jenis_kelamin');
        $this->urutan_anak = $this->clean_number($this->input->post('urutan_anak'));
        $this->jumlah_saudara = $this->clean_number($this->input->post('jumlah_saudara'));
        $this->golongan_darah = $this->input->post('golongan_darah');
        $this->nik = $this->input->post('nik');
        $this->npsn = $this->input->post('npsn');
        $this->nim = $this->input->post('nim');
        $this->nisn = $this->input->post('nisn');
        $this->alamat_santri = $this->br2nl($this->input->post('alamat_santri'));
        $this->kelurahan = $this->input->post('kelurahan');
        $this->kecamatan = $this->input->post('kecamatan');
        $this->kota = $this->input->post('kota');
        $this->provinsi = $this->input->post('provinsi');
        $this->kode_pos = $this->input->post('kode_pos');
        $this->telepon = $this->input->post('telepon');
        $this->handphone = $this->input->post('handphone');
        $this->tempat_tinggal = $this->input->post('tempat_tinggal');
        $this->tempat_tinggal_lainnya = $this->input->post('tempat_tinggal_lainnya');
        $this->ayah_no_kk = $this->input->post('ayah_no_kk');
        $this->ayah_nik = $this->input->post('ayah_nik');
        $this->ayah_nama_lengkap = $this->input->post('ayah_nama_lengkap');
        $this->ayah_tempat_lahir = $this->input->post('ayah_tempat_lahir');
        $this->ayah_tanggal_lahir = !is_null($this->input->post('ayah_tanggal_lahir')) ? date('Y-m-d', strtotime($this->input->post('ayah_tanggal_lahir'))) : null;
        $this->ayah_status = $this->input->post('ayah_status');
        $this->ayah_pendidikan = $this->input->post('ayah_pendidikan');
        $this->ayah_pekerjaan = $this->input->post('ayah_pekerjaan');
        $this->ayah_pekerjaan_jenis = $this->input->post('ayah_pekerjaan_jenis');
        $this->ayah_pekerjaan_pangkat = $this->input->post('ayah_pekerjaan_pangkat');
        $this->ayah_alamat = $this->br2nl($this->input->post('ayah_alamat'));
        $this->ayah_kelurahan = $this->input->post('ayah_kelurahan');
        $this->ayah_kecamatan = $this->input->post('ayah_kecamatan');
        $this->ayah_kota = $this->input->post('ayah_kota');
        $this->ayah_provinsi = $this->input->post('ayah_provinsi');
        $this->ayah_telepon = $this->input->post('ayah_telepon');
        $this->ayah_handphone = $this->input->post('ayah_handphone');
        $this->ayah_penghasilan = $this->input->post('ayah_penghasilan');
        $this->ibu_nik = $this->input->post('ibu_nik');
        $this->ibu_nama_lengkap = $this->input->post('ibu_nama_lengkap');
        $this->ibu_tempat_lahir = $this->input->post('ibu_tempat_lahir');
        $this->ibu_tanggal_lahir = !is_null($this->input->post('ibu_tanggal_lahir')) ? date('Y-m-d', strtotime($this->input->post('ibu_tanggal_lahir'))) : null;
        $this->ibu_status = $this->input->post('ibu_status');
        $this->ibu_pendidikan = $this->input->post('ibu_pendidikan');
        $this->ibu_pekerjaan = $this->input->post('ibu_pekerjaan');
        $this->ibu_pekerjaan_jenis = $this->input->post('ibu_pekerjaan_jenis');
        $this->ibu_pekerjaan_pangkat = $this->input->post('ibu_pekerjaan_pangkat');
        $this->ibu_alamat = $this->br2nl($this->input->post('ibu_alamat'));
        $this->ibu_kelurahan = $this->input->post('ibu_kelurahan');
        $this->ibu_kecamatan = $this->input->post('ibu_kecamatan');
        $this->ibu_kota = $this->input->post('ibu_kota');
        $this->ibu_provinsi = $this->input->post('ibu_provinsi');
        $this->ibu_telepon = $this->input->post('ibu_telepon');
        $this->ibu_handphone = $this->input->post('ibu_handphone');
        $this->ibu_penghasilan = $this->input->post('ibu_penghasilan');
        $this->wali_no_kk = $this->input->post('wali_no_kk');
        $this->wali_nik = $this->input->post('wali_nik');
        $this->wali_nama_lengkap = $this->input->post('wali_nama_lengkap');
        $this->wali_tempat_lahir = $this->input->post('wali_tempat_lahir');
        $this->wali_tanggal_lahir = !is_null($this->input->post('wali_tanggal_lahir')) ? date('Y-m-d', strtotime($this->input->post('wali_tanggal_lahir'))) : null;
        $this->wali_status = $this->input->post('wali_status');
        $this->wali_pendidikan = $this->input->post('wali_pendidikan');
        $this->wali_pekerjaan = $this->input->post('wali_pekerjaan');
        $this->wali_pekerjaan_jenis = $this->input->post('wali_pekerjaan_jenis');
        $this->wali_pekerjaan_pangkat = $this->input->post('wali_pekerjaan_pangkat');
        $this->wali_alamat = $this->br2nl($this->input->post('wali_alamat'));
        $this->wali_kelurahan = $this->input->post('wali_kelurahan');
        $this->wali_kecamatan = $this->input->post('wali_kecamatan');
        $this->wali_kota = $this->input->post('wali_kota');
        $this->wali_provinsi = $this->input->post('wali_provinsi');
        $this->wali_telepon = $this->input->post('wali_telepon');
        $this->wali_handphone = $this->input->post('wali_handphone');
        $this->wali_penghasilan = $this->input->post('wali_penghasilan');
        $this->ukuran_baju = $this->input->post('ukuran_baju');
        $this->ukuran_celana = $this->input->post('ukuran_celana');
        $this->sekolah_asal = strtoupper($this->input->post('sekolah_asal'));
        $this->sekolah_asal_alamat = $this->input->post('sekolah_asal_alamat');
        $this->komitmen = $this->input->post('komitmen');
        $this->komitmen_nominal = $this->input->post('komitmen_nominal');
        $this->surat_1 = $this->input->post('surat_1');
        $this->surat_2 = $this->input->post('surat_2');
        $this->tahun = $this->input->post('active_tahun');
        $this->db->insert($this->_table, $this);

        $response = array('status' => true, 'data' => 'Data has been saved.', 'psb_id' => $this->db->insert_id());
      } else {
        $response = array('status' => false, 'data' => 'Failed to create user data.', 'psb_id' => null);
      };
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.', 'psb_id' => null);
    };

    return $response;
  }

  public function insertBatch($data)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->insert_batch($this->_table, $data);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function update($id, $resetStatus = false)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $temp = $this->getDetail(['id' => $id]);
      $surat_1 = !is_null($this->input->post('surat_1')) ? $this->input->post('surat_1') : $temp->surat_1;
      $surat_2 = !is_null($this->input->post('surat_2')) ? $this->input->post('surat_2') : $temp->surat_2;

      $this->email = $this->input->post('email');
      $this->nama_lengkap = $this->input->post('nama_lengkap');
      $this->tempat_lahir = $this->input->post('tempat_lahir');
      $this->tanggal_lahir = date('Y-m-d', strtotime($this->input->post('tanggal_lahir')));
      $this->jenis_kelamin = $this->input->post('jenis_kelamin');
      $this->urutan_anak = $this->clean_number($this->input->post('urutan_anak'));
      $this->jumlah_saudara = $this->clean_number($this->input->post('jumlah_saudara'));
      $this->golongan_darah = $this->input->post('golongan_darah');
      $this->nik = $this->input->post('nik');
      $this->npsn = $this->input->post('npsn');
      $this->nim = $this->input->post('nim');
      $this->nisn = $this->input->post('nisn');
      $this->alamat_santri = $this->br2nl($this->input->post('alamat_santri'));
      $this->kelurahan = $this->input->post('kelurahan');
      $this->kecamatan = $this->input->post('kecamatan');
      $this->kota = $this->input->post('kota');
      $this->provinsi = $this->input->post('provinsi');
      $this->kode_pos = $this->input->post('kode_pos');
      $this->telepon = $this->input->post('telepon');
      $this->handphone = $this->input->post('handphone');
      $this->tempat_tinggal = $this->input->post('tempat_tinggal');
      $this->tempat_tinggal_lainnya = $this->input->post('tempat_tinggal_lainnya');
      $this->ayah_no_kk = $this->input->post('ayah_no_kk');
      $this->ayah_nik = $this->input->post('ayah_nik');
      $this->ayah_nama_lengkap = $this->input->post('ayah_nama_lengkap');
      $this->ayah_tempat_lahir = $this->input->post('ayah_tempat_lahir');
      $this->ayah_tanggal_lahir = !is_null($this->input->post('ayah_tanggal_lahir')) ? date('Y-m-d', strtotime($this->input->post('ayah_tanggal_lahir'))) : null;
      $this->ayah_status = $this->input->post('ayah_status');
      $this->ayah_pendidikan = $this->input->post('ayah_pendidikan');
      $this->ayah_pekerjaan = $this->input->post('ayah_pekerjaan');
      $this->ayah_pekerjaan_jenis = $this->input->post('ayah_pekerjaan_jenis');
      $this->ayah_pekerjaan_pangkat = $this->input->post('ayah_pekerjaan_pangkat');
      $this->ayah_alamat = $this->br2nl($this->input->post('ayah_alamat'));
      $this->ayah_kelurahan = $this->input->post('ayah_kelurahan');
      $this->ayah_kecamatan = $this->input->post('ayah_kecamatan');
      $this->ayah_kota = $this->input->post('ayah_kota');
      $this->ayah_provinsi = $this->input->post('ayah_provinsi');
      $this->ayah_telepon = $this->input->post('ayah_telepon');
      $this->ayah_handphone = $this->input->post('ayah_handphone');
      $this->ayah_penghasilan = $this->input->post('ayah_penghasilan');
      $this->ibu_nik = $this->input->post('ibu_nik');
      $this->ibu_nama_lengkap = $this->input->post('ibu_nama_lengkap');
      $this->ibu_tempat_lahir = $this->input->post('ibu_tempat_lahir');
      $this->ibu_tanggal_lahir = !is_null($this->input->post('ibu_tanggal_lahir')) ? date('Y-m-d', strtotime($this->input->post('ibu_tanggal_lahir'))) : null;
      $this->ibu_status = $this->input->post('ibu_status');
      $this->ibu_pendidikan = $this->input->post('ibu_pendidikan');
      $this->ibu_pekerjaan = $this->input->post('ibu_pekerjaan');
      $this->ibu_pekerjaan_jenis = $this->input->post('ibu_pekerjaan_jenis');
      $this->ibu_pekerjaan_pangkat = $this->input->post('ibu_pekerjaan_pangkat');
      $this->ibu_alamat = $this->br2nl($this->input->post('ibu_alamat'));
      $this->ibu_kelurahan = $this->input->post('ibu_kelurahan');
      $this->ibu_kecamatan = $this->input->post('ibu_kecamatan');
      $this->ibu_kota = $this->input->post('ibu_kota');
      $this->ibu_provinsi = $this->input->post('ibu_provinsi');
      $this->ibu_telepon = $this->input->post('ibu_telepon');
      $this->ibu_handphone = $this->input->post('ibu_handphone');
      $this->ibu_penghasilan = $this->input->post('ibu_penghasilan');
      $this->wali_no_kk = $this->input->post('wali_no_kk');
      $this->wali_nik = $this->input->post('wali_nik');
      $this->wali_nama_lengkap = $this->input->post('wali_nama_lengkap');
      $this->wali_tempat_lahir = $this->input->post('wali_tempat_lahir');
      $this->wali_tanggal_lahir = !is_null($this->input->post('wali_tanggal_lahir')) ? date('Y-m-d', strtotime($this->input->post('wali_tanggal_lahir'))) : null;
      $this->wali_status = $this->input->post('wali_status');
      $this->wali_pendidikan = $this->input->post('wali_pendidikan');
      $this->wali_pekerjaan = $this->input->post('wali_pekerjaan');
      $this->wali_pekerjaan_jenis = $this->input->post('wali_pekerjaan_jenis');
      $this->wali_pekerjaan_pangkat = $this->input->post('wali_pekerjaan_pangkat');
      $this->wali_alamat = $this->br2nl($this->input->post('wali_alamat'));
      $this->wali_kelurahan = $this->input->post('wali_kelurahan');
      $this->wali_kecamatan = $this->input->post('wali_kecamatan');
      $this->wali_kota = $this->input->post('wali_kota');
      $this->wali_provinsi = $this->input->post('wali_provinsi');
      $this->wali_telepon = $this->input->post('wali_telepon');
      $this->wali_handphone = $this->input->post('wali_handphone');
      $this->wali_penghasilan = $this->input->post('wali_penghasilan');
      $this->ukuran_baju = $this->input->post('ukuran_baju');
      $this->ukuran_celana = $this->input->post('ukuran_celana');
      $this->sekolah_asal = strtoupper($this->input->post('sekolah_asal'));
      $this->sekolah_asal_alamat = $this->input->post('sekolah_asal_alamat');
      $this->komitmen = $this->input->post('komitmen');
      $this->komitmen_nominal = $this->input->post('komitmen_nominal');
      $this->surat_1 = $surat_1;
      $this->surat_2 = $surat_2;

      if ($resetStatus === true) {
        $this->status = 0;
        $this->tahun = $this->input->post('active_tahun');
      };

      $this->updated_at = date('Y-m-d H:i:s');
      $this->db->update($this->_table, $this, ['id' => $id]);

      // Update in user
      if ($resetStatus === true) {
        $user = [
          'email' => $this->input->post('email'),
          'username' => $this->input->post('nisn'),
          'password' => md5($this->input->post('password')),
          'nama_lengkap' => $this->input->post('nama_lengkap'),
          'profile_photo' => $this->input->post('profile_photo'),
          'learndash_user_ref' => $this->input->post('password')
        ];
      } else {
        $user = [
          'email' => $this->email,
          'username' => $this->nisn,
          'nama_lengkap' => $this->nama_lengkap
        ];
      };
      $this->db->where(['id' => $id])->update('user', $user);

      $response = array('status' => true, 'data' => 'Data has been saved.', 'psb_id' => $id, 'psb_status' => $temp->status);
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.', 'psb_id' => $id, 'psb_status' => null);
    };

    return $response;
  }

  public function setStatus($id, $status)
  {
    $response = array('status' => false, 'data' => 'No operation.');
    $detail = $this->getDetail(['id' => $id]);

    try {
      // Update in calon santri
      $this->status = $status;
      $this->updated_at = date('Y-m-d H:i:s');
      $setStatus = $this->db->update($this->_table, $this, ['id' => $id]);

      if ($setStatus) {
        if ((int) $status === 4) {
          $role = 'Santri';

          // Update in user
          $updateUser = $this->db->update('user', ['role' => $role], ['id' => $id]);

          if ($updateUser) {
            // Copy to santri
            $santriInsertStatus = true;
            $this->db->delete('santri', ['id' => $id]);

            $santri = array(
              'id' => $id,
              'nisn' => $detail->nisn,
              'nomor_induk_lokal' => null,
              'nomor_absen' => null,
              'nama_lengkap' => $detail->nama_lengkap,
              'jenis_kelamin' => $detail->jenis_kelamin,
              'kelas' => null,
              'sub_kelas' => null,
              'asrama' => null,
              'wali' => null,
              'pembina' => null,
              'tempat_lahir' => $detail->tempat_lahir,
              'tanggal_lahir' => $detail->tanggal_lahir,
              'alamat' => $detail->alamat_santri,
              'provinsi' => $detail->provinsi,
              'kabupaten_kota' => $detail->kota,
              'tanggal_masuk' => null,
              'golongan_darah' => $detail->golongan_darah,
              'berat_badan' => null,
              'tinggi_badan' => null,
              'ayah_nama' => $detail->ayah_nama_lengkap,
              'ayah_alamat' => $detail->ayah_alamat,
              'ayah_provinsi' => $detail->ayah_provinsi,
              'ayah_kabupaten_kota' => $detail->ayah_kota,
              'ayah_hp' => $detail->ayah_handphone,
              'ibu_nama' => $detail->ibu_nama_lengkap,
              'ibu_alamat' => $detail->ibu_alamat,
              'ibu_provinsi' => $detail->ibu_provinsi,
              'ibu_kabupaten_kota' => $detail->ibu_kota,
              'ibu_hp' => $detail->ibu_handphone,
              'wali_nama' => $detail->wali_nama_lengkap,
              'wali_hubungan' => null,
              'wali_provinsi' => $detail->wali_provinsi,
              'wali_kabupaten_kota' => $detail->wali_kota,
              'wali_hp' => $detail->wali_handphone,
              'created_by' => $this->session->userdata('user')['id']
            );
            $insertSantri = $this->db->insert('santri', $santri);

            if (!$insertSantri) {
              $santriInsertStatus = false;
            };

            if ($santriInsertStatus === true) {
              $response = array('status' => true, 'data' => 'Data has been saved.');
            } else {
              // Rollback when insert santri failed
              $this->db->update($this->_table, ['status' => $detail->status], ['id' => $id]);
              $this->db->update('user', ['role' => 'Calon Santri'], ['id' => $id]);

              $response = array('status' => false, 'data' => 'Failed to save your data.');
            };
          } else {
            // Rollback when update user failed
            $this->db->update($this->_table, ['status' => $detail->status], ['id' => $id]);

            $response = array('status' => false, 'data' => 'Failed to save your data.');
          };
        } else {
          $response = array('status' => true, 'data' => 'Data has been saved.');
        };
      } else {
        $response = array('status' => false, 'data' => 'Failed to save your data.');
      };
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function delete($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->delete($this->_table, ['id' => $id]);
      $this->db->delete('psb_prestasi', ['psb_id' => $id]);
      $this->db->delete('user', ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  public function truncate()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->truncate($this->_table);

      $response = array('status' => true, 'data' => 'Data has been truncated.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to truncate your data.');
    };

    return $response;
  }

  public function getStatus($status = 0)
  {
    switch ($status) {
      case 0:
        return 'Belum Bayar, Nonaktif';
        break;
      case 1:
        return 'Sudah Bayar, Aktif';
        break;
      case 2:
        return 'Tidak Lulus';
        break;
      case 3:
        return 'Lulus';
        break;
      case 4:
        return 'Santri Aktif';
        break;
      default:
        return 'Undefined';
        break;
    };
  }

  function br2nl($text)
  {
    return str_replace("\r\n", '<br/>', htmlspecialchars_decode($text));
  }

  function clean_number($number)
  {
    return preg_replace('/[^0-9]/', '', $number);
  }

  function re_arrange($arr)
  {
    if (count($arr) > 0) {
      foreach ($arr as $key => $all) {
        foreach ($all as $i => $val) {
          $new[$i][$key] = $val;
        };
      };
      return $new;
    } else {
      return false;
    };
  }
}
