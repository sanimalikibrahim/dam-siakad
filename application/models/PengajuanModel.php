<?php
defined('BASEPATH') or exit('No direct script access allowed');

class PengajuanModel extends CI_Model
{
  private $_table = 'pengajuan';
  private $_tableView = '';
  private $_permittedChars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';

  public function rules($id)
  {
    return array(
      [
        'field' => 'nomor',
        'label' => 'Nomor',
        'rules' => [
          'required',
          'trim',
          [
            'nomor_exist',
            function ($nomor) use ($id) {
              return $this->_nomor_exist($nomor, $id);
            }
          ]
        ]
      ],
      [
        'field' => 'tanggal',
        'label' => 'Tanggal',
        'rules' => 'required|trim'
      ]
    );
  }

  private function _nomor_exist($nomor, $id)
  {
    $id = (!IS_NULL($id)) ? $id : 0;
    $temp = $this->db->where(['id !=' => $id, 'nomor' => $nomor])->get($this->_table);

    if ($temp->num_rows() > 0) {
      $this->form_validation->set_message('nomor_exist', 'Nomor "' . $nomor . '" already used.');
      return false;
    } else {
      return true;
    };
  }

  public function checkTtd($id, $status, $role)
  {
    $query = "SELECT t.*
              FROM view_pengajuan_history t
              JOIN pengajuan p ON p.id = t.pengajuan_id and p.revisi = t.revisi
              WHERE t.pengajuan_id = '" . $id . "' AND t.action = '" . $status . "' AND t.role = '" . $role . "'
              ORDER BY t.id DESC
              LIMIT 1";
    $result = $this->db->query($query)->row();

    if (!is_null($result)) {
      return $result->nama_lengkap;
    } else {
      return null;
    };
  }

  public function getAll($params = [], $orderField = null, $orderBy = 'asc')
  {
    $this->db->where($params);

    if (!is_null($orderField)) {
      $this->db->order_by($orderField, $orderBy);
    };

    return $this->db->get($this->_table)->result();
  }

  public function getDetail($params = [], $paramsIn = [])
  {
    $this->db->where($params);

    if (count($paramsIn) > 0) {
      foreach ($paramsIn as $key => $item) {
        $this->db->where_in($key, $item);
      };
    };

    return $this->db->get($this->_table)->row();
  }

  public function insert()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->nomor = $this->input->post('nomor');
      $this->tanggal = $this->input->post('tanggal');
      $this->jenis = 'Buku Perpustakaan';
      $this->revisi = 0;
      $this->status = 0;
      $this->created_by = $this->session->userdata('user')['id'];
      $this->db->insert($this->_table, $this);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function update($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->nomor = $this->input->post('nomor');
      $this->tanggal = $this->input->post('tanggal');
      $this->updated_at = date('Y-m-d H:i:s');
      $this->updated_by = $this->session->userdata('user')['id'];
      $this->db->update($this->_table, $this, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function setStatus($id, $status)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $temp = $this->getDetail(['id' => $id]);
      $revisi = ((int) $status === 1) ? (int) $temp->revisi + 1 : (int) $temp->revisi;

      if ((int) $status === 6) {
        $this->finish_at = date('Y-m-d H:i:s');
      };

      $this->revisi = $revisi;
      $this->status = $status;
      $this->updated_at = date('Y-m-d H:i:s');
      $this->updated_by = $this->session->userdata('user')['id'];
      $this->db->update($this->_table, $this, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Successfully update status.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to update status.');
    };

    return $response;
  }

  public function delete($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      // Delete pengajuan
      $this->db->delete($this->_table, ['id' => $id]);

      // Delete pengajuan item
      $this->db->delete('pengajuan_item', ['pengajuan_id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  public function truncate()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->truncate($this->_table);

      $response = array('status' => true, 'data' => 'Data has been truncated.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to truncate your data.');
    };

    return $response;
  }

  function br2nl($text)
  {
    return str_replace("\r\n", '<br/>', htmlspecialchars_decode($text));
  }

  function clean_number($number)
  {
    return preg_replace('/[^0-9]/', '', $number);
  }

  function generateRandom($input, $strength = 16)
  {
    $input_length = strlen($input);
    $random_string = '';

    for ($i = 0; $i < $strength; $i++) {
      $random_character = $input[mt_rand(0, $input_length - 1)];
      $random_string .= $random_character;
    };

    return $random_string;
  }
}
