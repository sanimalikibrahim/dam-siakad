<?php
defined('BASEPATH') or exit('No direct script access allowed');

class JadwalPelajaranModel extends CI_Model
{
  private $_table = 'jadwal_pelajaran';
  private $_tableView = '';

  public function rules()
  {
    return array(
      [
        'field' => 'semester',
        'label' => 'Semester',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'kelas',
        'label' => 'Kelas',
        'rules' => [
          [
            'kelas_validate',
            function () {
              return $this->_kelas_validate();
            }
          ]
        ]
      ],
    );
  }

  private function _kelas_validate()
  {
    $kelas = $this->input->post('kelas');

    if (count($kelas) === 0) {
      $this->form_validation->set_message('kelas_validate', 'The Kelas field is required.');
      return false;
    } else {
      return true;
    };
  }

  public function getAll($params = [])
  {
    return $this->db->where($params)->get($this->_table)->result();
  }

  public function getDetail($params = [])
  {
    return $this->db->where($params)->get($this->_table)->row();
  }

  public function insert()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->semester = $this->input->post('semester');
      $this->kelas = json_encode($this->input->post('kelas'));
      $this->created_by = $this->session->userdata('user')['id'];
      $this->db->insert($this->_table, $this);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function insertBatch($data)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->insert_batch($this->_table, $data);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function update($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->semester = $this->input->post('semester');
      $this->kelas = json_encode($this->input->post('kelas'));
      $this->updated_at = date('Y-m-d H:i:s');
      $this->updated_by = $this->session->userdata('user')['id'];
      $this->db->update($this->_table, $this, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function delete($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->delete($this->_table, ['id' => $id]);
      $this->db->delete('jadwal_pelajaran_item', ['jadwal_pelajaran_id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  public function truncate()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->truncate($this->_table);

      $response = array('status' => true, 'data' => 'Data has been truncated.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to truncate your data.');
    };

    return $response;
  }

  function br2nl($text)
  {
    return str_replace("\r\n", '<br/>', htmlspecialchars_decode($text));
  }

  function clean_number($number)
  {
    return preg_replace('/[^0-9]/', '', $number);
  }
}
