<?php
defined('BASEPATH') or exit('No direct script access allowed');

class AbsenHarianModel extends CI_Model
{
  private $_table = 'absen_harian';
  private $_tableView = 'view_absen_harian';

  public function rules()
  {
    return array(
      [
        'field' => 'kelas',
        'label' => 'Kelas',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'santri_id',
        'label' => 'Santri',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'mata_pelajaran_id',
        'label' => 'Mata Pelajaran',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'pengajar_id',
        'label' => 'Pengajar',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'status',
        'label' => 'Status',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'tanggal',
        'label' => 'Tanggal',
        'rules' => 'required|trim'
      ],
    );
  }

  public function getAll($params = [])
  {
    return $this->db->where($params)->get($this->_tableView)->result();
  }

  public function getAll_withSantri($kelas = null, $subKelas = null, $tanggal = null, $mapelId = null)
  {
    $query = "
      SELECT
      t.id,
      t.nomor_induk_lokal,
      t.nisn,
      t.nama_lengkap,
      CONCAT(t.kelas, '#', t.sub_kelas) AS kelas,
      a.status,
      a.catatan,
      a.mata_pelajaran_id,
      m.nama AS nama_mapel,
      a.pengajar_id,
      u.nama_lengkap AS nama_pengajar,
      a.created_at,
      a.created_by
    FROM santri t
    LEFT JOIN absen_harian a ON a.santri_id = t.id AND a.tanggal = '$tanggal' AND a.mata_pelajaran_id = '$mapelId'
    LEFT JOIN mata_pelajaran m ON m.id = a.mata_pelajaran_id
    LEFT JOIN `user` u ON u.id = a.pengajar_id
    WHERE t.kelas = '$kelas' AND t.sub_kelas = '$subKelas'
    ";
    return $this->db->query($query)->result();
  }

  public function getAll_rekap($kelas = null, $santri_id = null, $mapel_id = null, $tanggal = null)
  {
    $query = "
      SELECT *
      FROM $this->_tableView
      WHERE kelas = '$kelas' AND santri_id = '$santri_id' AND mata_pelajaran_id = '$mapel_id' AND tanggal = '$tanggal'
    ";
    return $this->db->query($query)->row();
  }

  public function getAll_rekapByDate($kelas = null, $santri_id = null, $tanggal = null)
  {
    $query = "
      SELECT
      (
        SELECT COUNT(*)
        FROM view_absen_harian
        WHERE kelas = '$kelas' AND santri_id = '$santri_id' AND tanggal = '$tanggal'
      ) AS jumlah_sesi,
      (
        SELECT COUNT(*)
        FROM view_absen_harian
        WHERE kelas = '$kelas' AND santri_id = '$santri_id' AND tanggal = '$tanggal' AND status = 'Hadir'
      ) AS jumlah_hadir,
      (
        SELECT COUNT(*)
        FROM view_absen_harian
        WHERE kelas = '$kelas' AND santri_id = '$santri_id' AND tanggal = '$tanggal' AND status = 'Izin'
      ) AS jumlah_izin,
      (
        SELECT COUNT(*)
        FROM view_absen_harian
        WHERE kelas = '$kelas' AND santri_id = '$santri_id' AND tanggal = '$tanggal' AND status = 'Sakit'
      ) AS jumlah_sakit,
      (
        SELECT COUNT(*)
        FROM view_absen_harian
        WHERE kelas = '$kelas' AND santri_id = '$santri_id' AND tanggal = '$tanggal' AND status = 'Alpa'
      ) AS jumlah_alpa,
      (
        SELECT COUNT(id)
        FROM view_jadwal_pelajaran_item
        WHERE jadwal_pelajaran_kelas LIKE '%$kelas%' AND hari_en = DAYNAME('$tanggal')
      ) AS jumlah_mapel
    ";
    return $this->db->query($query)->row();
  }

  public function getDetail($params = [])
  {
    return $this->db->where($params)->get($this->_tableView)->row();
  }

  public function insert()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->kelas = $this->input->post('kelas');
      $this->santri_id = $this->input->post('santri_id');
      $this->mata_pelajaran_id = $this->input->post('mata_pelajaran_id');
      $this->pengajar_id = $this->input->post('pengajar_id');
      $this->status = $this->input->post('status');
      $this->catatan = $this->input->post('catatan');
      $this->tanggal = $this->input->post('tanggal');
      $this->created_by = $this->session->userdata('user')['id'];
      $this->db->insert($this->_table, $this);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function insertBatch($data)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->insert_batch($this->_table, $data);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function update($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->kelas = $this->input->post('kelas');
      $this->santri_id = $this->input->post('santri_id');
      $this->mata_pelajaran_id = $this->input->post('mata_pelajaran_id');
      $this->pengajar_id = $this->input->post('pengajar_id');
      $this->status = $this->input->post('status');
      $this->catatan = $this->input->post('catatan');
      $this->tanggal = $this->input->post('tanggal');
      $this->updated_at = date('Y-m-d H:i:s');
      $this->updated_by = $this->session->userdata('user')['id'];
      $this->db->update($this->_table, $this, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function delete($params = [])
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->delete($this->_table, $params);

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  public function delete_by_id($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->delete($this->_table, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  public function truncate()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->truncate($this->_table);

      $response = array('status' => true, 'data' => 'Data has been truncated.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to truncate your data.');
    };

    return $response;
  }

  function br2nl($text)
  {
    return str_replace("\r\n", '<br/>', htmlspecialchars_decode($text));
  }

  function clean_number($number)
  {
    return preg_replace('/[^0-9]/', '', $number);
  }
}
