<?php
defined('BASEPATH') or exit('No direct script access allowed');

class UserModel extends CI_Model
{
  private $_table = 'user';
  private $_tableView = '';
  private $_columns = array(
    'email',
    'username',
    'nama_lengkap',
    'role'
  ); // Urutan (index) harus sama dengan template excel, dan penamaan harus sama dengan tabel (case-sensitive)
  private $_permittedChars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ~!@#$%^&*()_+';

  public function getColumnName($columnIndex)
  {
    $temp = array_combine(range(1, count($this->_columns)), array_values($this->_columns)); // Reset index to 1
    $result = (isset($temp[$columnIndex])) ? $temp[$columnIndex] : 0; // Get value
    return $result;
  }

  public function rules($id)
  {
    return array(
      [
        'field' => 'role',
        'label' => 'Role',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'nama_lengkap',
        'label' => 'Nama Lengkap',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'email',
        'label' => 'Email',
        'rules' => [
          'required',
          'trim',
          'valid_email',
          [
            'email_exist',
            function ($email) use ($id) {
              return $this->_email_exist($email, $id);
            }
          ]
        ]
      ],
      [
        'field' => 'username',
        'label' => 'Username',
        'rules' => [
          'required',
          'trim',
          'min_length[5]',
          'max_length[12]',
          'alpha_numeric',
          [
            'username_exist',
            function ($username) use ($id) {
              return $this->_username_exist($username, $id);
            }
          ]
        ]
      ]
    );
  }

  public function rules_register()
  {
    return array(
      [
        'field' => 'role',
        'label' => 'Role',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'nama_lengkap',
        'label' => 'Nama Lengkap',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'email',
        'label' => 'Email',
        'rules' => 'required|trim|valid_email|is_unique[user.email]'
      ],
      [
        'field' => 'username',
        'label' => 'Username',
        'rules' => 'required|trim|min_length[5]|max_length[12]|is_unique[user.username]|alpha_numeric'
      ],
      [
        'field' => 'password',
        'label' => 'Password',
        'rules' => 'required|matches[password_confirm]'
      ],
      [
        'field' => 'password_confirm',
        'label' => 'Password Confirm',
        'rules' => 'required'
      ]
    );
  }

  private function _email_exist($email, $id)
  {
    $id = (!IS_NULL($id)) ? $id : 0;
    $temp = $this->db->where(['id !=' => $id, 'email' => $email])->get($this->_table);

    if ($temp->num_rows() > 0) {
      $this->form_validation->set_message('email_exist', 'Email "' . $email . '" has been used by other user.');
      return false;
    } else {
      return true;
    };
  }

  private function _username_exist($username, $id)
  {
    $id = (!IS_NULL($id)) ? $id : 0;
    $temp = $this->db->where(['id !=' => $id, 'username' => $username])->get($this->_table);

    if ($temp->num_rows() > 0) {
      $this->form_validation->set_message('username_exist', 'Username "' . $username . '" has been used by other user.');
      return false;
    } else {
      return true;
    };
  }

  public function getAllCombo($params = [])
  {
    $this->db->select("t.id, if(s.kelas is not null, concat(t.`role`, ' &nbsp;|&nbsp;  ', t.nama_lengkap, ' (', s.kelas, ' ', s.sub_kelas, ')'), concat(t.`role`, ' &nbsp;|&nbsp; ', t.nama_lengkap)) as label");
    $this->db->from($this->_table . ' t');
    $this->db->join('santri s', 's.id = t.id', 'left');
    $this->db->where($params);
    return $this->db->get()->result();
  }

  public function getAllByRole($role = [], $orderField = null, $orderBy = 'asc')
  {
    $this->db->where_in('role', $role);

    if (!is_null($orderField)) {
      $this->db->order_by($orderField, $orderBy);
    };

    return $this->db->get($this->_table)->result();
  }

  public function getAll($params = [], $orderField = null, $orderBy = 'asc')
  {
    $this->db->where($params);

    if (!is_null($orderField)) {
      $this->db->order_by($orderField, $orderBy);
    };

    return $this->db->get($this->_table)->result();
  }

  public function getDetail($params = [])
  {
    return $this->db->where($params)->get($this->_table)->row();
  }

  public function insert()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $post = $this->input->post();
      $post['password'] = (isset($post['password']) && !empty($post['password'])) ? md5($this->input->post('password')) : null;

      $this->email = $this->input->post('email');
      $this->username = $this->input->post('username');
      $this->password = $post['password'];
      $this->nama_lengkap = $this->input->post('nama_lengkap');
      $this->role = $this->input->post('role');
      $this->profile_photo = $this->input->post('profile_photo');
      $this->db->insert($this->_table, $this);

      if ($this->input->post('role') === 'Santri') {
        // Insert to santri
        $santri = [
          'id' => $this->db->insert_id(),
          'nisn' => $this->input->post('username'),
          'nama_lengkap' => $this->input->post('nama_lengkap')
        ];
        $this->db->insert('santri', $santri);
      };

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function insertBatch($data)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->insert_batch($this->_table, $data);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function update($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $temp = $this->getDetail(['id' => $id]);
      $post = $this->input->post();
      $post['password'] = (isset($post['password']) && !empty($post['password'])) ? md5($this->input->post('password')) : $temp->password;
      $post['profile_photo'] = (isset($post['profile_photo']) && !is_null($post['profile_photo'])) ? $this->input->post('profile_photo') : $temp->profile_photo;

      $this->email = $this->input->post('email');
      $this->username = $this->input->post('username');
      $this->password = $post['password'];
      $this->nama_lengkap = $this->input->post('nama_lengkap');
      $this->role = $this->input->post('role');
      $this->profile_photo = $post['profile_photo'];
      $this->db->update($this->_table, $this, ['id' => $id]);

      // Update in santri
      $santri = [
        'nisn' => $this->username,
        'nama_lengkap' => $this->nama_lengkap
      ];
      $this->db->where(['id' => $id])->update('santri', $santri);

      // Update in psb / calon santri
      $calonSantri = [
        'email' => $this->email,
        'nisn' => $this->username,
        'nama_lengkap' => $this->nama_lengkap
      ];
      $this->db->where(['id' => $id])->update('psb', $calonSantri);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function setLearndashId($id, $learndashUserId)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->where(['id' => $id]);
      $this->db->set('learndash_user_id', $learndashUserId);
      $this->db->update($this->_table);

      $response = array('status' => true, 'data' => 'Password has been changed.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to change password.');
    };

    return $response;
  }

  public function setLearndashRef($id, $learndashUserRef)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->where(['id' => $id]);
      $this->db->set('learndash_user_ref', $learndashUserRef);
      $this->db->update($this->_table);

      $response = array('status' => true, 'data' => 'Password has been changed.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to change password.');
    };

    return $response;
  }

  public function setPassword($username, $data)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->where(['username' => $username]);
      $this->db->set('password', $data);
      $this->db->update($this->_table);

      $response = array('status' => true, 'data' => 'Password has been changed.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to change password.');
    };

    return $response;
  }

  public function generateToken($params = [])
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $token = $this->generateRandom($this->_permittedChars, 255);

      $this->db->where($params);
      $this->db->set('token', $token);
      $this->db->update($this->_table);

      $response = array('status' => true, 'data' => 'Token has been generated.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to generate token.');
    };

    return $response;
  }

  public function generatePassword($params = [])
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->where($params);
      $this->db->set('password', 'md5(concat(id, username))', false);
      $this->db->update($this->_table);

      $response = array('status' => true, 'data' => 'Password has been generated.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to generate password.');
    };

    return $response;
  }

  public function delete($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->delete($this->_table, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  public function truncate()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->truncate($this->_table);

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  function generateRandom($input, $strength = 16)
  {
    $input_length = strlen($input);
    $random_string = '';

    for ($i = 0; $i < $strength; $i++) {
      $random_character = $input[mt_rand(0, $input_length - 1)];
      $random_string .= $random_character;
    };

    return $random_string;
  }
}
