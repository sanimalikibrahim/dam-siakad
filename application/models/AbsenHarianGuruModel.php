<?php
defined('BASEPATH') or exit('No direct script access allowed');

class AbsenHarianGuruModel extends CI_Model
{
  private $_table = 'absen_harian_guru';
  private $_tableView = 'view_absen_harian_guru';

  public function rules()
  {
    return array(
      [
        'field' => 'pengajar_id',
        'label' => 'Pengajar',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'status',
        'label' => 'Status',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'tanggal',
        'label' => 'Tanggal',
        'rules' => 'required|trim'
      ],
    );
  }

  public function getAll($params = [])
  {
    return $this->db->where($params)->get($this->_tableView)->result();
  }

  public function getAll_withPengajar($tanggal = null)
  {
    $query = "
      SELECT
        t.id,
        a.pengajar_id,
        a.kelas,
        a.mata_pelajaran_id,
        m.nama AS mata_pelajaran_nama,
        t.nama_lengkap,
        a.status,
        a.catatan,
        a.created_at,
        a.created_by
      FROM `user`t
      LEFT JOIN absen_harian_guru a ON a.pengajar_id = t.id AND a.tanggal = '$tanggal'
      LEFT JOIN mata_pelajaran m on m.id = a.mata_pelajaran_id
      WHERE t.role = 'Guru'
    ";
    return $this->db->query($query)->result();
  }

  public function getAll_rekap($kelas = null, $tanggal = null, $sesi = null)
  {
    $query = "
      SELECT id, pengajar_id, mata_pelajaran_id, status, catatan, tanggal, nama_lengkap AS nama_pengajar
      FROM $this->_tableView
      WHERE kelas = '$kelas' AND tanggal = '$tanggal' AND sesi = '$sesi' AND status = 'Hadir'
    ";
    return $this->db->query($query)->row();
  }

  public function getAll_rekapByDate($kelas = null, $startDate = null, $endDate = null, $jumlahMinggu = 4)
  {
    $jumlahMinggu = (int) $jumlahMinggu;
    $query = "
      SELECT
        *,
        ((x.kehadiran_jumlah / x.jtm) * 100) AS kehadiran_persen,
        (((x.sakit + x.izin + x.tanpa_keterangan) / x.jtm) * 100) AS ketidakhadiran_persen
      FROM (
        SELECT DISTINCT
          pengajar_id,
          pengajar_nama_lengkap,
          mata_pelajaran_id,
          kode AS mata_pelajaran_kode,
          mata_pelajaran_jenis,
          mata_pelajaran_nama,
          (
            SELECT COUNT(id) FROM hari_libur WHERE MONTH(tanggal) = MONTH('$startDate')
          ) AS jumlah_libur,
          (
            SELECT DISTINCT COUNT(id)
            FROM view_jadwal_pelajaran_item
            WHERE jadwal_pelajaran_kelas LIKE '%$kelas%' AND pengajar_id = t.pengajar_id
          ) AS jumlah_ngajar,
          (
            SELECT DISTINCT ((COUNT(id) * $jumlahMinggu) - (SELECT COUNT(id) FROM hari_libur WHERE MONTH(tanggal) = MONTH('$startDate')))
            FROM view_jadwal_pelajaran_item
            WHERE jadwal_pelajaran_kelas LIKE '%$kelas%' AND pengajar_id = t.pengajar_id
          ) AS jtm,
          (
            SELECT COUNT(id)
            FROM absen_harian_guru
            WHERE pengajar_id = t.pengajar_id AND mata_pelajaran_id = t.mata_pelajaran_id AND tanggal between '$startDate' and '$endDate' AND status = 'Hadir'
          ) AS kehadiran_jumlah,
          (
            SELECT COUNT(id)
            FROM absen_harian_guru
            WHERE pengajar_id = t.pengajar_id AND mata_pelajaran_id = t.mata_pelajaran_id AND tanggal between '$startDate' and '$endDate' AND status = 'Sakit'
          ) AS sakit,
          (
            SELECT COUNT(id)
            FROM absen_harian_guru
            WHERE pengajar_id = t.pengajar_id AND mata_pelajaran_id = t.mata_pelajaran_id AND tanggal between '$startDate' and '$endDate' AND status = 'Izin'
          ) AS izin,
          (
            SELECT COUNT(id)
            FROM absen_harian_guru
            WHERE pengajar_id = t.pengajar_id AND mata_pelajaran_id = t.mata_pelajaran_id AND tanggal between '$startDate' and '$endDate' AND status = 'Alpa'
          ) AS tanpa_keterangan
        FROM view_jadwal_pelajaran_item t
        WHERE jadwal_pelajaran_kelas LIKE '%$kelas%'
      ) x
    ";
    return $this->db->query($query)->result();
  }

  public function getDetail($params = [])
  {
    return $this->db->where($params)->get($this->_tableView)->row();
  }

  public function insert()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->pengajar_id = $this->input->post('pengajar_id');
      $this->status = $this->input->post('status');
      $this->catatan = $this->input->post('catatan');
      $this->tanggal = $this->input->post('tanggal');
      $this->created_by = $this->session->userdata('user')['id'];
      $this->db->insert($this->_table, $this);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function insertBatch($data)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->insert_batch($this->_table, $data);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function update($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->pengajar_id = $this->input->post('pengajar_id');
      $this->status = $this->input->post('status');
      $this->catatan = $this->input->post('catatan');
      $this->tanggal = $this->input->post('tanggal');
      $this->updated_at = date('Y-m-d H:i:s');
      $this->updated_by = $this->session->userdata('user')['id'];
      $this->db->update($this->_table, $this, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function delete($params = [])
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->delete($this->_table, $params);

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  public function delete_by_id($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->delete($this->_table, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  public function truncate()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->truncate($this->_table);

      $response = array('status' => true, 'data' => 'Data has been truncated.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to truncate your data.');
    };

    return $response;
  }

  function br2nl($text)
  {
    return str_replace("\r\n", '<br/>', htmlspecialchars_decode($text));
  }

  function clean_number($number)
  {
    return preg_replace('/[^0-9]/', '', $number);
  }
}
