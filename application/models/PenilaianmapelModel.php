<?php
defined('BASEPATH') or exit('No direct script access allowed');

class PenilaianmapelModel extends CI_Model
{
  private $_table = 'penilaian_mapel';
  private $_tableView = 'view_penilaian_mapel';

  public function rules()
  {
    return array(
      [
        'field' => 'kelas',
        'label' => 'Kelas',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'santri_id',
        'label' => 'Santri',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'mata_pelajaran_id',
        'label' => 'Mata Pelajaran',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'pengajar_id',
        'label' => 'Pengajar',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'nilai',
        'label' => 'Nilai',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'tanggal',
        'label' => 'Tanggal',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'jenis',
        'label' => 'Jenis',
        'rules' => 'required|trim'
      ],
    );
  }

  public function rules_sync()
  {
    return array(
      [
        'field' => 'tanggal',
        'label' => 'Tanggal',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'jenis',
        'label' => 'Jenis',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'learndash_group_id',
        'label' => 'Kelas',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'learndash_course_id',
        'label' => 'Program',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'learndash_lesson_id',
        'label' => 'Pelajaran',
        'rules' => [
          'required', 'trim',
          [
            'check_lesson',
            function ($learndash_lesson_id) {
              return $this->_check_lesson($learndash_lesson_id);
            }
          ]
        ]
      ],
      [
        'field' => 'learndash_topic_id',
        'label' => 'Topic',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'learndash_quiz_id',
        'label' => 'Ulangan/Ujian',
        'rules' => 'required|trim'
      ],
    );
  }

  private function _check_lesson($learndash_lesson_id)
  {
    $temp = $this->db->where(['learndash_lesson_id' => $learndash_lesson_id])->get('mata_pelajaran');

    if ($temp->num_rows() === 0) {
      $this->form_validation->set_message('check_lesson', 'Pelajaran belum di set.');
      return false;
    } else {
      return true;
    };
  }

  public function getAll($params = [])
  {
    return $this->db->where($params)->get($this->_tableView)->result();
  }

  public function getAll_withSantri($kelas = null, $subKelas = null, $tanggal = null, $mapelId = null)
  {
    $query = "
      SELECT
        t.id,
        t.nomor_induk_lokal,
        t.nisn,
        t.nama_lengkap,
        CONCAT(t.kelas, '#', t.sub_kelas) AS kelas,
        a.nilai,
        a.catatan,
        a.mata_pelajaran_id,
        m.nama AS nama_mapel,
        a.pengajar_id,
        u.nama_lengkap AS nama_pengajar,
        a.jenis,
        a.created_at,
        a.created_by
      FROM santri t
      LEFT JOIN penilaian_mapel a ON a.santri_id = t.id AND a.tanggal = '$tanggal' AND a.mata_pelajaran_id = '$mapelId'
      LEFT JOIN mata_pelajaran m ON m.id = a.mata_pelajaran_id
      LEFT JOIN `user` u ON u.id = a.pengajar_id
      WHERE t.kelas = '$kelas' AND t.sub_kelas = '$subKelas'
      ORDER BY t.nama_lengkap ASC
    ";
    return $this->db->query($query)->result();
  }

  public function getDetail($params = [])
  {
    return $this->db->where($params)->get($this->_tableView)->row();
  }

  public function insert()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->kelas = $this->input->post('kelas');
      $this->santri_id = $this->input->post('santri_id');
      $this->mata_pelajaran_id = $this->input->post('mata_pelajaran_id');
      $this->pengajar_id = $this->input->post('pengajar_id');
      $this->nilai = $this->input->post('nilai');
      $this->catatan = $this->input->post('catatan');
      $this->tanggal = $this->input->post('tanggal');
      $this->jenis = $this->input->post('jenis');
      $this->created_by = $this->session->userdata('user')['id'];
      $this->db->insert($this->_table, $this);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function insertBatch($data)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->insert_batch($this->_table, $data);

      $response = array('status' => true, 'data' => 'Data has been saved (Total: ' . count($data) . ')');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function update($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->kelas = $this->input->post('kelas');
      $this->santri_id = $this->input->post('santri_id');
      $this->mata_pelajaran_id = $this->input->post('mata_pelajaran_id');
      $this->pengajar_id = $this->input->post('pengajar_id');
      $this->nilai = $this->input->post('nilai');
      $this->catatan = $this->input->post('catatan');
      $this->tanggal = $this->input->post('tanggal');
      $this->jenis = $this->input->post('jenis');
      $this->updated_at = date('Y-m-d H:i:s');
      $this->updated_by = $this->session->userdata('user')['id'];
      $this->db->update($this->_table, $this, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function delete($params = [])
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->delete($this->_table, $params);

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  public function delete_by_id($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->delete($this->_table, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  public function deleteByKelasDate($tanggal, $groupId, $courseId, $lessonId, $topicId, $quizId, $jenis)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->delete($this->_table, [
        'tanggal' => $tanggal,
        'jenis' => $jenis,
        'learndash_group_id' => $groupId,
        'learndash_course_id' => $courseId,
        'learndash_lesson_id' => $lessonId,
        'learndash_topic_id' => $topicId,
        'learndash_quiz_id' => $quizId,
      ]);

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  public function truncate()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->truncate($this->_table);

      $response = array('status' => true, 'data' => 'Data has been truncated.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to truncate your data.');
    };

    return $response;
  }

  function br2nl($text)
  {
    return str_replace("\r\n", '<br/>', htmlspecialchars_decode($text));
  }

  function clean_number($number)
  {
    return preg_replace('/[^0-9]/', '', $number);
  }
}
