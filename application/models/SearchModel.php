<?php
defined('BASEPATH') or exit('No direct script access allowed');

class SearchModel extends CI_Model
{
  public function santri($keyword)
  {
    $role = strtolower($this->session->userdata('user')['role']);

    $this->db->group_start();
    $this->db->like('nisn', $keyword, 'both');
    $this->db->or_like('nomor_induk_lokal', $keyword, 'both');
    $this->db->or_like('nama_lengkap', $keyword, 'both');
    $this->db->group_end();

    $this->db->order_by('nama_lengkap', 'asc');
    $result = $this->db->get('santri')->result();

    return $result;
  }
}
