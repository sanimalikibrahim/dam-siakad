<?php
defined('BASEPATH') or exit('No direct script access allowed');

class TransaksibukuModel extends CI_Model
{
  private $_table = 'transaksi_buku';
  private $_tableView = 'view_transaksi_buku';
  private $_permittedChars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';

  public function rules_pinjam()
  {
    return array(
      [
        'field' => 'user_id',
        'label' => 'Peminjam',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'tanggal_pinjam',
        'label' => 'Tanggal Pinjam',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'tanggal_kembali',
        'label' => 'Tanggal Kembali',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'buku_id',
        'label' => 'Buku',
        'rules' => [
          [
            'buku_assigned',
            function () {
              return $this->_buku_assigned();
            }
          ]
        ]
      ],
    );
  }

  private function _buku_assigned()
  {
    $buku = $this->input->post('buku_id');

    if (count($buku) === 0) {
      $this->form_validation->set_message('buku_assigned', 'At least one book must be added.');
      return false;
    } else {
      return true;
    };
  }

  public function getAll($params = [], $orderField = null, $orderBy = 'asc')
  {
    $this->db->where($params);

    if (!is_null($orderField)) {
      $this->db->order_by($orderField, $orderBy);
    };

    return $this->db->get($this->_tableView)->result();
  }

  public function getDetail($params = [])
  {
    return $this->db->where($params)->get($this->_tableView)->row();
  }

  public function getDetailFull($kode = null)
  {
    try {
      $header = $this->db->where(['kode' => $kode])->get('view_transaksi_buku_list')->row();

      if ($header) {
        $body = $this->db->where(['kode' => $kode])->get($this->_tableView)->result();
        $response = array(
          'status' => true,
          'data' => [
            'header' => $header,
            'body' => $body,
          ],
        );
      } else {
        $response = array('status' => false, 'data' => 'No data found.');
      };
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to get detail data.');
    };

    return $response;
  }

  public function insert()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->kode = $this->input->post('kode');
      $this->user_id = $this->input->post('user_id');
      $this->buku_id = $this->input->post('buku_id');
      $this->tanggal_pinjam = $this->input->post('tanggal_pinjam');
      $this->tanggal_kembali = $this->input->post('tanggal_kembali');
      $this->tanggal_pengembalian = $this->input->post('tanggal_pengembalian');
      $this->denda = $this->input->post('denda');
      $this->keterangan_pinjam = $this->br2nl($this->input->post('keterangan_pinjam'));
      $this->keterangan_pengembalian = $this->br2nl($this->input->post('keterangan_pengembalian'));
      $this->created_by = $this->session->userdata('user')['id'];
      $this->db->insert($this->_table, $this);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function insertPinjam()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $buku = $this->input->post('buku_id');
      $kode = date('Ymd') . $this->generateRandom($this->_permittedChars, 7);
      $data = array();

      foreach ($buku as $key => $item) {
        $data[] = array(
          'kode' => $kode,
          'user_id' => $this->input->post('user_id'),
          'buku_id' => $item,
          'tanggal_pinjam' => $this->input->post('tanggal_pinjam'),
          'tanggal_kembali' => $this->input->post('tanggal_kembali'),
          'keterangan_pinjam' => $this->br2nl($this->input->post('keterangan_pinjam')),
          'created_by' => $this->session->userdata('user')['id']
        );
      };

      $this->db->insert_batch($this->_table, $data);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function insertBatch($data)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->insert_batch($this->_table, $data);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function update($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->kode = $this->input->post('kode');
      $this->user_id = $this->input->post('user_id');
      $this->buku_id = $this->input->post('buku_id');
      $this->tanggal_pinjam = $this->input->post('tanggal_pinjam');
      $this->tanggal_kembali = $this->input->post('tanggal_kembali');
      $this->tanggal_pengembalian = $this->input->post('tanggal_pengembalian');
      $this->denda = $this->input->post('denda');
      $this->keterangan_pinjam = $this->br2nl($this->input->post('keterangan_pinjam'));
      $this->keterangan_pengembalian = $this->br2nl($this->input->post('keterangan_pengembalian'));
      $this->updated_at = date('Y-m-d H:i:s');
      $this->updated_by = $this->session->userdata('user')['id'];
      $this->db->update($this->_table, $this, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function updatePengembalian($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->tanggal_pengembalian = $this->input->post('tanggal_pengembalian');
      $this->keterangan_pengembalian = $this->br2nl($this->input->post('keterangan_pengembalian'));
      $this->updated_at = date('Y-m-d H:i:s');
      $this->updated_by = $this->session->userdata('user')['id'];
      $this->db->update($this->_table, $this, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function setStatus()
  {
    $response = array('status' => false, 'data' => 'No operation.');
    $id = $this->input->post('id');
    $status = $this->input->post('status');

    try {
      $this->status = $status;
      $this->tanggal_pengembalian = ($status === 'Selesai') ? date('Y-m-d H:i:s') : null;
      $this->updated_at = date('Y-m-d H:i:s');
      $this->updated_by = $this->session->userdata('user')['id'];
      $this->db->update($this->_table, $this, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Successfully save status as ' . $status . '.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save status as ' . $status . '.');
    };

    return $response;
  }

  public function delete($kode)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->delete($this->_table, ['kode' => $kode]);

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  public function truncate()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->truncate($this->_table);

      $response = array('status' => true, 'data' => 'Data has been truncated.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to truncate your data.');
    };

    return $response;
  }

  function br2nl($text)
  {
    return str_replace("\r\n", '<br/>', htmlspecialchars_decode($text));
  }

  function clean_number($number)
  {
    return preg_replace('/[^0-9]/', '', $number);
  }

  function generateRandom($input, $strength = 16)
  {
    $input_length = strlen($input);
    $random_string = '';

    for ($i = 0; $i < $strength; $i++) {
      $random_character = $input[mt_rand(0, $input_length - 1)];
      $random_string .= $random_character;
    };

    return $random_string;
  }
}
