<?php
defined('BASEPATH') or exit('No direct script access allowed');

class PengajuanitemModel extends CI_Model
{
  private $_table = 'pengajuan_item';
  private $_tableView = '';
  private $_permittedChars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';

  public function rules()
  {
    return array(
      [
        'field' => 'pengajuan_id',
        'label' => 'Pengajuan ID',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'item',
        'label' => 'Item',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'quantity',
        'label' => 'Quantity',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'unit',
        'label' => 'Unit',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'unit_price',
        'label' => 'Unit Price',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'amount',
        'label' => 'Amount',
        'rules' => 'required|trim'
      ]
    );
  }

  public function getAll($params = [], $orderField = null, $orderBy = 'asc')
  {
    $this->db->where($params);

    if (!is_null($orderField)) {
      $this->db->order_by($orderField, $orderBy);
    };

    return $this->db->get($this->_table)->result();
  }

  public function getDetail($params = [])
  {
    return $this->db->where($params)->get($this->_table)->row();
  }

  public function insert()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->pengajuan_id = $this->input->post('pengajuan_id');
      $this->item = $this->br2nl($this->input->post('item'));
      $this->quantity = $this->clean_number($this->input->post('quantity'));
      $this->unit = $this->input->post('unit');
      $this->unit_price = $this->clean_number($this->input->post('unit_price'));
      $this->amount = $this->clean_number($this->input->post('amount'));
      $this->created_by = $this->session->userdata('user')['id'];
      $this->db->insert($this->_table, $this);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function update($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->item = $this->br2nl($this->input->post('item'));
      $this->quantity = $this->clean_number($this->input->post('quantity'));
      $this->unit = $this->input->post('unit');
      $this->unit_price = $this->clean_number($this->input->post('unit_price'));
      $this->amount = $this->clean_number($this->input->post('amount'));
      $this->updated_at = date('Y-m-d H:i:s');
      $this->updated_by = $this->session->userdata('user')['id'];
      $this->db->update($this->_table, $this, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function delete($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->delete($this->_table, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  public function truncate()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->truncate($this->_table);

      $response = array('status' => true, 'data' => 'Data has been truncated.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to truncate your data.');
    };

    return $response;
  }

  function br2nl($text)
  {
    return str_replace("\r\n", '<br/>', htmlspecialchars_decode($text));
  }

  function clean_number($number)
  {
    return preg_replace('/[^0-9]/', '', $number);
  }

  function generateRandom($input, $strength = 16)
  {
    $input_length = strlen($input);
    $random_string = '';

    for ($i = 0; $i < $strength; $i++) {
      $random_character = $input[mt_rand(0, $input_length - 1)];
      $random_string .= $random_character;
    };

    return $random_string;
  }
}
