<?php
defined('BASEPATH') or exit('No direct script access allowed');

class RaportChbSikapResultModel extends CI_Model
{
  private $_table = 'raport_chb_sikap_result';
  private $_tableView = '';

  public function rules()
  {
    return array(
      [
        'field' => 'semester',
        'label' => 'Semester',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'tahun_ajar',
        'label' => 'Tahun Ajar',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'kelas',
        'label' => 'Kelas',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'santri_id',
        'label' => 'Santri ID',
        'rules' => 'required|trim'
      ]
    );
  }

  public function getAll($params = [], $orderField = null, $orderBy = 'asc')
  {
    $this->db->where($params);

    if (!is_null($orderField)) {
      $this->db->order_by($orderField, $orderBy);
    };

    return $this->db->get($this->_table)->result();
  }

  public function getDetail($params = [])
  {
    return $this->db->where($params)->get($this->_table)->row();
  }

  public function insert()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->semester = $this->input->post('semester');
      $this->tahun_ajar = $this->input->post('tahun_ajar');
      $this->kelas = $this->input->post('kelas');
      $this->santri_id = $this->input->post('santri_id');
      $this->sikap_spiritual_predikat = $this->input->post('sikap_spiritual_predikat');
      $this->sikap_spiritual_deskripsi = $this->input->post('sikap_spiritual_deskripsi');
      $this->sikap_sosial_predikat = $this->input->post('sikap_sosial_predikat');
      $this->sikap_sosial_deskripsi = $this->input->post('sikap_sosial_deskripsi');
      $this->created_by = $this->session->userdata('user')['id'];
      $this->db->insert($this->_table, $this);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function insertBatch($data)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->insert_batch($this->_table, $data);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function insertBatchDuplicateUpdate($semester = null, $tahunAjar = null, $kelas = null, $data = null)
  {
    $response = array('status' => false, 'data' => 'No operation.');
    $payload = array();

    try {
      if (!is_null($data)) {
        if (count($data) > 0) {
          foreach ($data as $santri_id => $item) {
            $payload[] = array(
              'semester' => $semester,
              'tahun_ajar' => $tahunAjar,
              'kelas' => $kelas,
              'santri_id' => $santri_id,
              'sikap_spiritual_predikat' => (isset($item['sikap_spiritual_predikat'])) ? $item['sikap_spiritual_predikat'] : null,
              'sikap_spiritual_deskripsi' => (isset($item['sikap_spiritual_deskripsi'])) ? $item['sikap_spiritual_deskripsi'] : null,
              'sikap_sosial_predikat' => (isset($item['sikap_sosial_predikat'])) ? $item['sikap_sosial_predikat'] : null,
              'sikap_sosial_deskripsi' => (isset($item['sikap_sosial_deskripsi'])) ? $item['sikap_sosial_deskripsi'] : null,
              'created_by' => $this->session->userdata('user')['id'],
            );
          };
        };

        if (count($payload) > 0) {
          $this->db->insert_batch_duplicate_update($this->_table, $payload);

          $response = array('status' => true, 'data' => 'Data has been saved.');
        };
      };
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function update($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->semester = $this->input->post('semester');
      $this->tahun_ajar = $this->input->post('tahun_ajar');
      $this->kelas = $this->input->post('kelas');
      $this->santri_id = $this->input->post('santri_id');
      $this->sikap_spiritual_predikat = $this->input->post('sikap_spiritual_predikat');
      $this->sikap_spiritual_deskripsi = $this->input->post('sikap_spiritual_deskripsi');
      $this->sikap_sosial_predikat = $this->input->post('sikap_sosial_predikat');
      $this->sikap_sosial_deskripsi = $this->input->post('sikap_sosial_deskripsi');
      $this->updated_at = date('Y-m-d H:i:s');
      $this->updated_by = $this->session->userdata('user')['id'];
      $this->db->update($this->_table, $this, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function delete($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->delete($this->_table, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  public function truncate()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->truncate($this->_table);

      $response = array('status' => true, 'data' => 'Data has been truncated.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to truncate your data.');
    };

    return $response;
  }

  function br2nl($text)
  {
    return str_replace("\r\n", '<br/>', htmlspecialchars_decode($text));
  }

  function clean_number($number)
  {
    return preg_replace('/[^0-9]/', '', $number);
  }
}
