<?php
defined('BASEPATH') or exit('No direct script access allowed');

class PsbprestasiModel extends CI_Model
{
  private $_table = 'psb_prestasi';
  private $_tableView = '';

  public function rules()
  {
    return array(
      [
        'field' => 'nama_lomba',
        'label' => 'Nama Lomba',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'peringkat',
        'label' => 'Peringkat',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'tahun',
        'label' => 'Tahun',
        'rules' => 'required|trim'
      ]
    );
  }

  public function getAll_list_count($params = [])
  {
    return $this->db->where($params)->count_all_results($this->_table);
  }

  public function getAll_list($params = [], $per_page = 16, $offset = 0)
  {
    return $this->db->where($params)->limit($per_page, $offset)->get($this->_table)->result();
  }

  public function getAll($params = [])
  {
    return $this->db->where($params)->get($this->_table)->result();
  }

  public function getDetail($params = [])
  {
    return $this->db->where($params)->get($this->_table)->row();
  }

  public function insert()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->psb_id = $this->input->post('psb_id');
      $this->nama_lomba = $this->input->post('nama_lomba');
      $this->jenis_lomba = $this->input->post('jenis_lomba');
      $this->penyelenggara = $this->input->post('penyelenggara');
      $this->tingkat = $this->input->post('tingkat');
      $this->peringkat = $this->input->post('peringkat');
      $this->tahun = $this->input->post('tahun');
      $this->file_raw_name = $this->input->post('file_raw_name');
      $this->file_raw_name_thumb = $this->input->post('file_raw_name_thumb');
      $this->file_name = $this->input->post('file_name');
      $this->file_name_thumb = $this->input->post('file_name_thumb');
      $this->file_size = $this->input->post('file_size');
      $this->db->insert($this->_table, $this);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function insertBatch($data)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->insert_batch($this->_table, $data);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function delete($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $document = $this->getDetail(['id' => $id]);

      if (!empty($document) && !is_null($document)) {
        // Delete data
        $this->db->delete($this->_table, ['id' => $id]);

        if (!is_null($document->file_name) && !empty($document->file_name)) {
          unlink(FCPATH . $document->file_name);
        };

        if (!is_null($document->file_name_thumb) && !empty($document->file_name_thumb)) {
          unlink(FCPATH . $document->file_name_thumb);
        };
      };

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  public function truncate()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->truncate($this->_table);

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  function br2nl($text)
  {
    return str_replace("\r\n", '<br/>', htmlspecialchars_decode($text));
  }
}
