<?php
defined('BASEPATH') or exit('No direct script access allowed');

class PenilaianModel extends CI_Model
{
  private $_table = 'penilaian';
  private $_tableView = '';

  public function rules()
  {
    return array(
      [
        'field' => 'user_id',
        'label' => 'User',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'kriteria_id',
        'label' => 'Kriteria',
        'rules' => 'required|trim'
      ]
    );
  }

  public function getPivot()
  {
    $query1 = "SET @sql = NULL;";
    $query2 = "
      SELECT
        GROUP_CONCAT(
          DISTINCT(
            CONCAT(
              'max(case when nama = ''',
              nama,
              ''' then t2.nilai else null end) AS ',
              replace(nama, ' ', '_')
            )
          )
        ORDER BY id ASC
        ) INTO @sql
      FROM kriteria;
    ";
    $query3 = "
      SET @sql = CONCAT('
        SELECT 
          p.id,
          p.nisn,
          p.email,
          p.nama_lengkap,
          p.jenis_kelamin,
          p.kota,
          p.telepon,
          p.tahun,
          p.status,
          ', @sql, ' 
        FROM psb p 
        LEFT JOIN penilaian t2 ON t2.user_id = p.id
        LEFT JOIN kriteria t3 ON t3.id = t2.kriteria_id
        WHERE p.status = 1 -- AND p.tahun = 2020
        GROUP BY p.id
      ');
    ";
    $query4 = "PREPARE stmt FROM @sql;";
    $query5 = "EXECUTE stmt;";
    $query6 = "DEALLOCATE PREPARE stmt;";

    $this->db->query($query1);
    $this->db->query($query2);
    $this->db->query($query3);
    $this->db->query($query4);
    $transaction = $this->db->query($query5);

    if ($transaction !== false) {
      $result = $transaction->result_array();
      $columns = [];
      $columnsLabel = [];
      $columnsExcept = ['id', 'email', 'kota', 'telepon', 'tahun', 'status'];

      if (count($result) > 0) {
        foreach ($result as $index => $item) {
          foreach ($item as $field => $value) {
            if (!in_array($field, $columns) && !in_array($field, $columnsExcept)) {
              $tempLabel = $field;

              switch ($tempLabel) {
                case 'nisn':
                  $tempLabel = 'NISN';
                  break;
                case 'nama_lengkap':
                  $tempLabel = 'Nama Lengkap';
                  break;
                case 'jenis_kelamin':
                  $tempLabel = 'Jenis Kelamin';
                  break;
                default:
                  $tempLabel = $tempLabel;
                  break;
              };

              $columns[] = $field;
              $columnsLabel[] = str_replace('_', ' ', $tempLabel);
            };
          };
        };

        $response = [
          'column' => (object) [
            'field' => $columns,
            'label' => $columnsLabel
          ],
          'data' => $result
        ];
      } else {
        $response = [
          'column' => [],
          'data' => $result
        ];
      };
    } else {
      $response = $transaction;
    };

    $this->db->query($query6);

    return $response;
  }

  public function getMaxNilai()
  {
    $kriteria = $this->db->get('kriteria')->result();
    $response = [];

    if (count($kriteria) > 0) {
      foreach ($kriteria as $index => $item) {
        $kriteriaName = str_replace(' ', '_', $item->nama);
        $maxNilai = $this->db->select_max('nilai')->where(['kriteria_id' => $item->id])->get('penilaian')->row();

        if (!is_null($maxNilai)) {
          $response[$kriteriaName] = $maxNilai->nilai;
        } else {
          $response[$kriteriaName] = 1;
        };
      };
    };

    return $response;
  }

  public function getAll($params = [])
  {
    return $this->db->where($params)->get($this->_table)->result();
  }

  public function getDetail($params = [])
  {
    return $this->db->where($params)->get($this->_table)->row();
  }

  public function insert()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->user_id = $this->input->post('user_id');
      $this->kriteria_id = $this->input->post('kriteria_id');
      $this->nilai = $this->input->post('nilai');
      $this->created_by = $this->session->userdata('user')['id'];
      $this->db->insert($this->_table, $this);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function insertBatch($data)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->insert_batch($this->_table, $data);

      $response = array('status' => true, 'data' => 'Data has been saved. (' . count($data) . ' rows)');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function update($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->user_id = $this->input->post('user_id');
      $this->kriteria_id = $this->input->post('kriteria_id');
      $this->nilai = $this->input->post('nilai');
      $this->updated_at = date('Y-m-d H:i:s');
      $this->updated_by = $this->session->userdata('user')['id'];
      $this->db->update($this->_table, $this, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function delete($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->delete($this->_table, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  public function truncate()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->truncate($this->_table);

      $response = array('status' => true, 'data' => 'Data has been truncated.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to truncate your data.');
    };

    return $response;
  }

  function br2nl($text)
  {
    return str_replace("\r\n", '<br/>', htmlspecialchars_decode($text));
  }

  function clean_number($number)
  {
    return preg_replace('/[^0-9]/', '', $number);
  }
}
