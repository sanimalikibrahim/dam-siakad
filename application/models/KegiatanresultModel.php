<?php
defined('BASEPATH') or exit('No direct script access allowed');

class KegiatanresultModel extends CI_Model
{
  private $_table = 'kegiatan_result';
  private $_tableView = 'view_kegiatan_resultˇ';

  public function rules()
  {
    return array(
      [
        'field' => 'kegiatan_item_id',
        'label' => 'Kegiatan Item',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'santri_id',
        'label' => 'Santri',
        'rules' => 'required|trim'
      ]
    );
  }

  public function getAll_list_count($params = [])
  {
    return $this->db->where($params)->count_all_results('view_kegiatan_result_list');
  }

  public function getAll_list($params = [], $per_page = 16, $offset = 0)
  {
    return $this->db->where($params)->limit($per_page, $offset)->get('view_kegiatan_result_list')->result();
  }

  public function getDetail_list($params = [])
  {
    return $this->db->where($params)->get('view_kegiatan_result_list')->row();
  }

  public function getAll($params = [])
  {
    return $this->db->where($params)->get($this->_tableView)->result();
  }

  public function getDetail($params = [])
  {
    return $this->db->where($params)->get($this->_tableView)->row();
  }

  public function insert()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->kegiatan_item_id = $this->input->post('kegiatan_item_id');
      $this->santri_id = $this->input->post('santri_id');
      $this->jawaban = $this->br2nl($this->input->post('jawaban'));
      $this->created_by = $this->session->userdata('user')['id'];
      $this->db->insert($this->_table, $this);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function insertBatch($data)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->insert_batch($this->_table, $data);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function update($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->kegiatan_item_id = $this->input->post('kegiatan_item_id');
      $this->santri_id = $this->input->post('santri_id');
      $this->jawaban = $this->br2nl($this->input->post('jawaban'));
      $this->updated_at = date('Y-m-d H:i:s');
      $this->updated_by = $this->session->userdata('user')['id'];
      $this->db->update($this->_table, $this, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function delete($params = [])
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->delete($this->_table, $params);

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  public function delete_by_id($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->delete($this->_table, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  public function truncate()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->truncate($this->_table);

      $response = array('status' => true, 'data' => 'Data has been truncated.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to truncate your data.');
    };

    return $response;
  }

  function br2nl($text)
  {
    return str_replace("\r\n", '<br/>', htmlspecialchars_decode($text));
  }

  function clean_number($number)
  {
    return preg_replace('/[^0-9]/', '', $number);
  }
}
