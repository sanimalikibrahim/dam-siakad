<?php
defined('BASEPATH') or exit('No direct script access allowed');

class SettingMoreModel extends CI_Model
{
  private $_table = 'setting';

  public function rules()
  {
    return array(
      [
        'field' => 'psb_status',
        'label' => 'Status',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'psb_selesai',
        'label' => 'Tanggal Selesai',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'psb_tahun_mulai',
        'label' => 'Tahun Mulai',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'psb_tahun_selesai',
        'label' => 'Tahun Selesai',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'psb_test_type',
        'label' => 'Tipe Test',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'idcard_psb_foto_top',
        'label' => 'Foto: Top',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'idcard_psb_foto_left',
        'label' => 'Foto: Left',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'idcard_psb_nomor_top',
        'label' => 'No. Peserta: Top',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'idcard_psb_nomor_left',
        'label' => 'No. Peserta: Left',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'idcard_psb_nomor_font_size',
        'label' => 'No. Peserta: Size',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'idcard_psb_nomor_color',
        'label' => 'No. Peserta: Color',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'idcard_psb_nama_top',
        'label' => 'Nama: Top',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'idcard_psb_nama_left',
        'label' => 'Nama: Left',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'idcard_psb_nama_font_size',
        'label' => 'Nama: Size',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'idcard_psb_nama_color',
        'label' => 'Nama: Color',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'idcard_psb_alamat_top',
        'label' => 'Alamat: Top',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'idcard_psb_alamat_left',
        'label' => 'Alamat: Left',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'idcard_psb_alamat_font_size',
        'label' => 'Alamat: Size',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'idcard_psb_alamat_color',
        'label' => 'Alamat: Color',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'idcard_psb_ruangan1_top',
        'label' => 'Ruangan 1: Top',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'idcard_psb_ruangan1_left',
        'label' => 'Ruangan 1: Left',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'idcard_psb_ruangan1_font_size',
        'label' => 'Ruangan 1: Size',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'idcard_psb_ruangan1_color',
        'label' => 'Ruangan 1: Color',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'idcard_psb_ruangan2_top',
        'label' => 'Ruangan 2: Top',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'idcard_psb_ruangan2_left',
        'label' => 'Ruangan 2: Left',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'idcard_psb_ruangan2_font_size',
        'label' => 'Ruangan 2: Size',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'idcard_psb_ruangan2_color',
        'label' => 'Ruangan 2: Color',
        'rules' => 'required|trim'
      ],
    );
  }

  public function update()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $post = $this->input->post();

      foreach ($post as $key => $value) {
        $this->content = $value;
        $this->db->update($this->_table, $this, ['data' => $key]);
      };

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }
}
