<?php
defined('BASEPATH') or exit('No direct script access allowed');

class KegiatanModel extends CI_Model
{
  private $_table = 'kegiatan';
  private $_tableView = '';

  public function rules($id = null)
  {
    return array(
      [
        'field' => 'judul',
        'label' => 'Judul',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'slug',
        'label' => 'Slug',
        'rules' => [
          'required',
          'trim',
          [
            'slug_exist',
            function ($slug) use ($id) {
              return $this->_slug_exist($slug, $id);
            }
          ]
        ]
      ]
    );
  }

  private function _slug_exist($slug, $id)
  {
    $temp = $this->db->where(['id !=' => $id, 'slug' => $slug])->get($this->_table);

    if ($temp->num_rows() > 0) {
      $this->form_validation->set_message('slug_exist', 'Slug "' . $slug . '" already in use.');
      return false;
    } else {
      return true;
    };
  }

  public function getAll($params = [])
  {
    return $this->db->where($params)->get($this->_table)->result();
  }

  public function getAll_fullItem($date_result = null, $santri_id = null, $slugs = null)
  {
    $response = [];

    if (is_null($slugs)) {
      $kegiatan = $this->db->get($this->_table)->result();
    } else {
      $kegiatan = $this->db->where_in('slug', $slugs)->get($this->_table)->result();
    };

    if (count($kegiatan) > 0) {
      foreach ($kegiatan as $key => $parent) {
        // Get all kegiatan_item
        $kegiatan_item = $this->db->where(['slug' => $parent->slug])->get('kegiatan_item')->result();

        $temp_kegiatan_item = [];
        if (count($kegiatan_item) > 0) {
          foreach ($kegiatan_item as $index => $item) {
            // Get kegiatan result
            if (!is_null($santri_id)) {
              $kegiatan_result = $this->db->where(['kegiatan_item_id' => $item->id, 'DATE(created_at)' => $date_result, 'santri_id' => $santri_id])->get('kegiatan_result')->row();
            } else {
              $kegiatan_result = $this->db->where(['kegiatan_item_id' => $item->id, 'DATE(created_at)' => $date_result])->get('kegiatan_result')->row();
            };

            // Assign kegiatan_result to kegiatan_item
            if (!is_null($kegiatan_result)) {
              $item->jawaban = $kegiatan_result->jawaban;
            } else {
              $item->jawaban = '';
            };

            // Store in kegiatan_item
            $temp_kegiatan_item[] = $item;
          };
        };

        // Assign kegiatan_item to parent
        $parent->kegiatan_item = $temp_kegiatan_item;

        // Store in parent
        $response[] = $parent;
      };
    };

    return $response;
  }

  public function getAll_fullItemWithSantri($date_result = null, $kelas = null, $sub_kelas = null, $slugs = null)
  {
    $response = [];
    $data_santri = $this->db->where(['kelas' => $kelas, 'sub_kelas' => $sub_kelas])->get('santri')->result();

    if (count($data_santri) > 0) {
      foreach ($data_santri as $santri_key => $santri) {
        // Asign santri for parent
        $response[$santri_key] = (object) [
          'santri_id' => $santri->id,
          'nisn' => $santri->nisn,
          'nama_lengkap' => $santri->nama_lengkap
        ];

        if (is_null($slugs)) {
          $kegiatan = $this->db->get($this->_table)->result();
        } else {
          $kegiatan = $this->db->where_in('slug', $slugs)->get($this->_table)->result();
        };

        if (count($kegiatan) > 0) {
          foreach ($kegiatan as $key => $parent) {
            // Get all kegiatan_item
            $kegiatan_item = $this->db->where(['slug' => $parent->slug])->get('kegiatan_item')->result();

            $temp_kegiatan_item = [];
            if (count($kegiatan_item) > 0) {
              foreach ($kegiatan_item as $index => $item) {
                // Get kegiatan result
                $kegiatan_result = $this->db->where(['kegiatan_item_id' => $item->id, 'DATE(created_at)' => $date_result, 'santri_id' => $santri->id])->get('kegiatan_result')->row();

                // Assign kegiatan_result to kegiatan_item
                if (!is_null($kegiatan_result)) {
                  $item->jawaban = $kegiatan_result->jawaban;
                } else {
                  $item->jawaban = '';
                };

                // Store in kegiatan_item
                $temp_kegiatan_item[] = $item;
              };
            };

            // Assign kegiatan_item to parent
            $parent->kegiatan_item = $temp_kegiatan_item;

            // Store in parent
            $response[$santri_key]->kegiatan[] = $parent;
          };
        };
      };
    };

    return $response;
  }

  public function getDetail($params = [])
  {
    return $this->db->where($params)->get($this->_table)->row();
  }

  public function insert()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->judul = $this->input->post('judul');
      $this->keterangan = $this->br2nl($this->input->post('keterangan'));
      $this->slug = $this->input->post('slug');
      $this->created_by = $this->session->userdata('user')['id'];
      $this->db->insert($this->_table, $this);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function insertBatch($data)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->insert_batch($this->_table, $data);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function update($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');
    $temp = $this->getDetail(['id' => $id]);

    try {
      $this->judul = $this->input->post('judul');
      $this->keterangan = $this->br2nl($this->input->post('keterangan'));
      $this->slug = $this->input->post('slug');
      $this->updated_at = date('Y-m-d H:i:s');
      $this->updated_by = $this->session->userdata('user')['id'];
      $this->db->update($this->_table, $this, ['id' => $id]);

      // Update slug in kegiatan_item
      if ($this->input->post('slug') != $temp->slug) {
        $this->db->where(['slug' => $temp->slug])->update('kegiatan_item', ['slug' => $this->input->post('slug')]);
      };

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function delete($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->delete('kegiatan_item', ['kegiatan_id' => $id]);
      $this->db->delete($this->_table, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  public function truncate()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->truncate($this->_table);

      $response = array('status' => true, 'data' => 'Data has been truncated.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to truncate your data.');
    };

    return $response;
  }

  function br2nl($text)
  {
    return str_replace("\r\n", '<br/>', htmlspecialchars_decode($text));
  }

  function clean_number($number)
  {
    return preg_replace('/[^0-9]/', '', $number);
  }
}
