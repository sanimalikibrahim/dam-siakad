<?php
defined('BASEPATH') or exit('No direct script access allowed');

class RekapModel extends CI_Model
{
  public function getBerjamaah($start_date = null, $end_date = null, $kelas = null, $sub_kelas = null, $standar_kehadiran = 30)
  {
    return $this->db->query("CALL get_rekap_berjamaah($standar_kehadiran, '$start_date', '$end_date', $kelas, '$sub_kelas')")->result();
  }

  public function getBerjamaahBySantri($start_date = null, $end_date = null, $kelas = null, $sub_kelas = null, $standar_kehadiran = 30, $santri_id = null)
  {
    return $this->db->query("CALL get_rekap_berjamaah_by_santri($standar_kehadiran, '$start_date', '$end_date', $kelas, '$sub_kelas', $santri_id)")->result();
  }
}
