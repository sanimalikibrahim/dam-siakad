<?php
defined('BASEPATH') or exit('No direct script access allowed');

class PsbdokumenModel extends CI_Model
{
  private $_table = 'psb_dokumen';
  private $_tableView = '';

  public function rules()
  {
    return array(
      [
        'field' => 'psb_id',
        'label' => 'User ID',
        'rules' => 'required|trim'
      ]
    );
  }

  public function getAll_list_count($params = [])
  {
    return $this->db->where($params)->count_all_results($this->_table);
  }

  public function getAll_list($params = [], $per_page = 16, $offset = 0)
  {
    return $this->db->where($params)->limit($per_page, $offset)->get($this->_table)->result();
  }

  public function getAll($where = [], $whereInKey = null, $whereInValues = [])
  {
    if (count($where) > 0) {
      $this->db->where($where);
    };

    if (!is_null($whereInKey) && count($whereInValues) > 0) {
      $this->db->where_in($whereInKey, $whereInValues);
    };

    return $this->db->get($this->_table)->result();
  }

  public function getDetail($params = [])
  {
    return $this->db->where($params)->get($this->_table)->row();
  }

  public function insert($post)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->psb_id = $post['psb_id'];
      $this->nisn = $post['nisn'];
      $this->akte_kelahiran = $post['akte_kelahiran'];
      $this->kartu_keluarga = $post['kartu_keluarga'];
      $this->ijazah_sd = $post['ijazah_sd'];
      $this->skhun = $post['skhun'];
      $this->buku_laporan = $post['buku_laporan'];
      $this->kip = $post['kip'];
      $this->accept_aturan = $post['accept_aturan'];
      $this->accept_ortu = $post['accept_ortu'];
      $this->created_by = $this->session->userdata('user')['id'];
      $this->db->insert($this->_table, $this);

      $response = array('status' => true, 'data' => 'File has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your file.');
    };

    return $response;
  }

  public function insertBatch($data)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->insert_batch($this->_table, $data);

      $response = array('status' => true, 'data' => 'File has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your file.');
    };

    return $response;
  }

  public function update($post)
  {
    $response = array('status' => false, 'data' => 'No operation.');
    $detail = $this->getDetail(['psb_id' => $post['psb_id']]);

    try {
      $this->nisn = (!is_null($post['nisn']) && !empty($post['nisn'])) ? $post['nisn'] : $detail->nisn;
      $this->akte_kelahiran = (!is_null($post['akte_kelahiran']) && !empty($post['akte_kelahiran'])) ? $post['akte_kelahiran'] : $detail->akte_kelahiran;
      $this->kartu_keluarga = (!is_null($post['kartu_keluarga']) && !empty($post['kartu_keluarga'])) ? $post['kartu_keluarga'] : $detail->kartu_keluarga;
      $this->ijazah_sd = (!is_null($post['ijazah_sd']) && !empty($post['ijazah_sd'])) ? $post['ijazah_sd'] : $detail->ijazah_sd;
      $this->skhun = (!is_null($post['skhun']) && !empty($post['skhun'])) ? $post['skhun'] : $detail->skhun;
      $this->buku_laporan = (!is_null($post['buku_laporan']) && !empty($post['buku_laporan'])) ? $post['buku_laporan'] : $detail->buku_laporan;
      $this->kip = (!is_null($post['kip']) && !empty($post['kip'])) ? $post['kip'] : $detail->kip;
      $this->accept_aturan = (!is_null($post['accept_aturan']) && !empty($post['accept_aturan'])) ? $post['accept_aturan'] : $detail->accept_aturan;
      $this->accept_ortu = (!is_null($post['accept_ortu']) && !empty($post['accept_ortu'])) ? $post['accept_ortu'] : $detail->accept_ortu;
      $this->db->update($this->_table, $this, ['psb_id' => $post['psb_id']]);

      $response = array('status' => true, 'data' => 'File has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your file.');
    };

    return $response;
  }

  public function delete($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $document = $this->getDetail(['id' => $id]);

      if (!empty($document) && !is_null($document)) {
        // Delete data
        $this->db->delete($this->_table, ['id' => $id]);

        // Delete file
        @unlink(FCPATH . $document->nisn);
        @unlink(FCPATH . $document->akte_kelahiran);
        @unlink(FCPATH . $document->kartu_keluarga);
        @unlink(FCPATH . $document->ijazah_sd);
        @unlink(FCPATH . $document->skhun);
        @unlink(FCPATH . $document->buku_laporan);
        @unlink(FCPATH . $document->kip);
      };

      $response = array('status' => true, 'data' => 'File has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your file.');
    };

    return $response;
  }

  public function truncate()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->truncate($this->_table);

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  function br2nl($text)
  {
    return str_replace("\r\n", '<br/>', htmlspecialchars_decode($text));
  }
}
