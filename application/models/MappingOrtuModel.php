<?php
defined('BASEPATH') or exit('No direct script access allowed');

class MappingOrtuModel extends CI_Model
{
  private $_table = 'mapping_ortu';
  private $_tableView = 'view_mapping_ortu';

  public function rules()
  {
    return array(
      [
        'field' => 'ortu_id',
        'label' => 'Orang Tua',
        'rules' => 'required|trim|integer'
      ],
      [
        'field' => 'santri_id',
        'label' => 'Santri',
        'rules' => 'required|trim|integer'
      ],
    );
  }

  public function getAll($params = [], $order = [])
  {
    if (count($order) === 2) {
      $this->db->order_by($order[0], $order[1]);
    };
    $this->db->where($params);
    return $this->db->get($this->_tableView)->result();
  }

  public function getSantriIdFromOrtu($ortuId = null)
  {
    $list = $this->getAll(['ortu_id' => $ortuId]);
    $response = array();

    if (count($list)) {
      foreach ($list as $index => $item) {
        $response[] = $item->santri_id;
      };
    };

    return $response;
  }

  public function getDetail($params = [])
  {
    return $this->db->where($params)->get($this->_tableView)->row();
  }

  public function insert()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->ortu_id = $this->input->post('ortu_id');
      $this->santri_id = json_encode($this->input->post('santri_id'));
      $this->created_by = $this->session->userdata('user')['id'];
      $this->db->insert($this->_table, $this);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function insertBatch($data)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->insert_batch($this->_table, $data);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function update($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->ortu_id = $this->input->post('ortu_id');
      $this->santri_id = json_encode($this->input->post('santri_id'));
      $this->updated_at = date('Y-m-d H:i:s');
      $this->updated_by = $this->session->userdata('user')['id'];
      $this->db->update($this->_table, $this, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function publishByOrtu($ortuId = null)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      // Set status
      $this->status = 'Publish';
      $this->updated_at = date('Y-m-d H:i:s');
      $this->updated_by = $this->session->userdata('user')['id'];
      $this->db->update($this->_table, $this, ['ortu_id' => $ortuId]);

      $response = array('status' => true, 'data' => 'Data has been published.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to publish your data.');
    };

    return $response;
  }

  public function delete($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->delete($this->_table, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  public function deleteByOrtu($ortuId = null)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->delete($this->_table, ['ortu_id' => $ortuId]);

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  public function truncate()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->truncate($this->_table);

      $response = array('status' => true, 'data' => 'Data has been truncated.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to truncate your data.');
    };

    return $response;
  }

  function br2nl($text)
  {
    return str_replace("\r\n", '<br/>', htmlspecialchars_decode($text));
  }

  function clean_number($number)
  {
    return preg_replace('/[^0-9]/', '', $number);
  }
}
