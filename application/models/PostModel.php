<?php
defined('BASEPATH') or exit('No direct script access allowed');

class PostModel extends CI_Model
{
  private $_table = 'post';
  private $_tableView = '';

  public function rules()
  {
    return array(
      [
        'field' => 'judul',
        'label' => 'Judul',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'konten',
        'label' => 'Konten',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'url',
        'label' => 'URL',
        'rules' => 'required|trim|regex_match[/^[a-z0-9-]+$/]'
      ],
      [
        'field' => 'status',
        'label' => 'Status',
        'rules' => 'required|trim'
      ]
    );
  }

  public function getAll($params = [])
  {
    return $this->db->where($params)->get($this->_table)->result();
  }

  public function getDetail($params = [])
  {
    return $this->db->where($params)->get($this->_table)->row();
  }

  public function insert()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->judul = $this->input->post('judul');
      $this->url = $this->input->post('url');
      $this->konten = $this->input->post('konten');
      $this->status = $this->input->post('status');
      $this->created_by = $this->session->userdata('user')['id'];
      $this->db->insert($this->_table, $this);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function insertBatch($data)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->insert_batch($this->_table, $data);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function update($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->judul = $this->input->post('judul');
      $this->url = $this->input->post('url');
      $this->konten = $this->input->post('konten');
      $this->status = $this->input->post('status');
      $this->updated_at = date('Y-m-d H:i:s');
      $this->updated_by = $this->session->userdata('user')['id'];
      $this->db->update($this->_table, $this, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function delete($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      if (in_array((int) $id, [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13])) {
        $response = array('status' => false, 'data' => 'This data is related to other modules, you are not allowed to delete it.');
      } else {
        $this->db->delete($this->_table, ['id' => $id]);

        $response = array('status' => true, 'data' => 'Data has been deleted.');
      };
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  public function truncate()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->truncate($this->_table);

      $response = array('status' => true, 'data' => 'Data has been truncated.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to truncate your data.');
    };

    return $response;
  }

  function br2nl($text)
  {
    return str_replace("\r\n", '<br/>', htmlspecialchars_decode($text));
  }

  function clean_number($number)
  {
    return preg_replace('/[^0-9]/', '', $number);
  }
}
