<?php
defined('BASEPATH') or exit('No direct script access allowed');

class BukuModel extends CI_Model
{
  private $_table = 'buku';
  private $_tableView = 'view_buku';

  public function rules($id)
  {
    return array(
      [
        'field' => 'buku_kategori_id',
        'label' => 'Kategori',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'judul',
        'label' => 'Judul',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'kode',
        'label' => 'Kode',
        'rules' => [
          'required',
          'trim',
          [
            'kode_exist',
            function ($kode) use ($id) {
              return $this->_kode_exist($kode, $id);
            }
          ]
        ]
      ],
    );
  }

  private function _kode_exist($kode, $id)
  {
    $id = (!IS_NULL($id)) ? $id : 0;
    $temp = $this->db->where(['id !=' => $id, 'kode' => $kode])->get($this->_table);

    if ($temp->num_rows() > 0) {
      $this->form_validation->set_message('kode_exist', 'Kode "' . $kode . '" already used.');
      return false;
    } else {
      return true;
    };
  }

  public function getAll_list_count($params = [])
  {
    return $this->db->where($params)->count_all_results($this->_table);
  }

  public function getAll_list($params = [], $per_page = 16, $offset = 0)
  {
    return $this->db->where($params)->order_by('id', 'desc')->limit($per_page, $offset)->get($this->_tableView)->result();
  }

  public function getAll($params = [], $orderField = null, $orderBy = 'asc')
  {
    $this->db->where($params);

    if (!is_null($orderField)) {
      $this->db->order_by($orderField, $orderBy);
    };

    return $this->db->get($this->_tableView)->result();
  }

  public function getDetail($params = [])
  {
    return $this->db->where($params)->get($this->_tableView)->row();
  }

  public function getSearch($keyword = null, $jenis = 'Fisik', $status = 'Publish')
  {
    $this->db->group_start();
    $this->db->like('kategori_nama', $keyword);
    $this->db->or_like('kode', $keyword);
    $this->db->or_like('kode_rak', $keyword);
    $this->db->or_like('judul', $keyword);
    $this->db->or_like('pengarang', $keyword);
    $this->db->or_like('penerbit', $keyword);
    $this->db->or_like('tahun_terbit', $keyword);
    $this->db->or_like('barcode', $keyword);
    $this->db->group_end();
    $this->db->where('jenis', $jenis);
    $this->db->where('status', $status);
    $result = $this->db->get($this->_tableView)->result();

    return $result;
  }

  public function insert()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->buku_kategori_id = $this->input->post('buku_kategori_id');
      $this->kode = $this->input->post('kode');
      $this->kode_rak = $this->input->post('kode_rak');
      $this->judul = $this->input->post('judul');
      $this->pengarang = $this->input->post('pengarang');
      $this->penerbit = $this->input->post('penerbit');
      $this->tahun_terbit = $this->input->post('tahun_terbit');
      $this->barcode = $this->input->post('barcode');
      $this->jenis = 'Fisik';
      $this->status = 'Publish';
      $this->created_by = $this->session->userdata('user')['id'];
      $this->db->insert($this->_table, $this);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function insertDigital()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->buku_kategori_id = $this->input->post('buku_kategori_id');
      $this->kode = $this->input->post('kode');
      $this->judul = $this->input->post('judul');
      $this->pengarang = $this->input->post('pengarang');
      $this->penerbit = $this->input->post('penerbit');
      $this->tahun_terbit = $this->input->post('tahun_terbit');
      $this->jenis = 'Digital';
      $this->status = $this->input->post('status');
      $this->cover_img = $this->input->post('cover_img');
      $this->created_by = $this->session->userdata('user')['id'];
      $this->db->insert($this->_table, $this);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function insertBatch($data)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->insert_batch($this->_table, $data);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function update($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->buku_kategori_id = $this->input->post('buku_kategori_id');
      $this->kode = $this->input->post('kode');
      $this->kode_rak = $this->input->post('kode_rak');
      $this->judul = $this->input->post('judul');
      $this->pengarang = $this->input->post('pengarang');
      $this->penerbit = $this->input->post('penerbit');
      $this->tahun_terbit = $this->input->post('tahun_terbit');
      $this->jenis = 'Fisik';
      $this->status = 'Publish';
      $this->updated_at = date('Y-m-d H:i:s');
      $this->updated_by = $this->session->userdata('user')['id'];
      $this->db->update($this->_table, $this, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function updateDigital($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');
    $temp = $this->getDetail(['id' => $id, 'jenis' => 'Digital']);

    try {
      $_POST['cover_img'] = (!empty($_POST['cover_img'])) ? $_POST['cover_img'] : $temp->cover_img;

      $this->buku_kategori_id = $this->input->post('buku_kategori_id');
      $this->kode = $this->input->post('kode');
      $this->judul = $this->input->post('judul');
      $this->pengarang = $this->input->post('pengarang');
      $this->penerbit = $this->input->post('penerbit');
      $this->tahun_terbit = $this->input->post('tahun_terbit');
      $this->jenis = 'Digital';
      $this->status = $this->input->post('status');
      $this->cover_img = $this->input->post('cover_img');
      $this->updated_at = date('Y-m-d H:i:s');
      $this->updated_by = $this->session->userdata('user')['id'];
      $this->db->update($this->_table, $this, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function setBarcode($id, $file_path)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->barcode = $file_path;
      $this->updated_at = date('Y-m-d H:i:s');
      $this->updated_by = $this->session->userdata('user')['id'];
      $this->db->update($this->_table, $this, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function delete($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->delete($this->_table, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  public function deleteDigital($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $buku = $this->getDetail(['id' => $id, 'jenis' => 'Digital']);
      $document = $this->db->where(['ref' => 'bukudigital', 'ref_id' => $id])->get('document')->result();

      // Delete buku
      $this->db->delete($this->_table, ['id' => $id, 'jenis' => 'Digital']);

      // Delete buku cover file
      if (!empty($buku->cover_img) && !is_null($buku->cover_img)) {
        unlink(FCPATH . $buku->cover_img);
      };

      if (count($document) > 0) {
        // Delete document
        $this->db->delete('document', [
          'ref' => 'bukudigital',
          'ref_id' => $id
        ]);

        // Delete document file
        foreach ($document as $index => $item) {
          unlink(FCPATH . $item->file_name);
        };
      };

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  public function truncate()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->truncate($this->_table);

      $response = array('status' => true, 'data' => 'Data has been truncated.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to truncate your data.');
    };

    return $response;
  }

  function br2nl($text)
  {
    return str_replace("\r\n", '<br/>', htmlspecialchars_decode($text));
  }

  function clean_number($number)
  {
    return preg_replace('/[^0-9]/', '', $number);
  }
}
