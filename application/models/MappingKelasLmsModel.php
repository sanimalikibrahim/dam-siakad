<?php
defined('BASEPATH') or exit('No direct script access allowed');

class MappingKelasLmsModel extends CI_Model
{
  private $_table = 'mapping_kelas_lms';
  private $_tableView = '';

  public function rules()
  {
    return array(
      [
        'field' => 'kelas_id',
        'label' => 'Kelas',
        'rules' => 'required|trim|integer'
      ],
      [
        'field' => 'sub_kelas_id',
        'label' => 'Sub Kelas',
        'rules' => 'required|trim|integer'
      ],
      [
        'field' => 'learndash_group_id',
        'label' => 'Santri',
        'rules' => 'required|trim|integer'
      ],
      [
        'field' => 'learndash_group_name',
        'label' => 'Nomor Induk Lokal',
        'rules' => 'required|trim'
      ],
    );
  }

  public function getAll($params = [], $order = [])
  {
    if (count($order) === 2) {
      $this->db->order_by($order[0], $order[1]);
    };
    $this->db->where($params);
    return $this->db->get($this->_table)->result();
  }

  public function getAllList()
  {
    $query = "
      SELECT
        t.*,
        CONCAT(t.kelas_nama, ' ', t.sub_kelas_nama) AS kelas_concated
      FROM view_mapping_kelas_head t
      WHERE t.learndash_group_id IS NOT NULL AND t.learndash_group_id != ''
    ";
    return $this->db->query($query)->result();
  }

  public function getAllSantriKelas($groupId = null)
  {
    $query = "
      SELECT
        t.id,
        t.nisn,
        t.nomor_induk_lokal,
        t.nama_lengkap,
        t.jenis_kelamin,
        t.kelas,
        t.sub_kelas,
        mk.kelas_id,
        mk.sub_kelas_id,
        mk.learndash_group_id,
        mk.learndash_group_name,
        u.learndash_user_id
      FROM santri t
      LEFT JOIN `user` u ON u.id = t.id
      LEFT JOIN view_mapping_kelas_head mk ON LOWER(mk.kelas_nama) = LOWER(t.kelas) AND LOWER(mk.sub_kelas_nama) = LOWER(t.sub_kelas)
      WHERE mk.learndash_group_id = '$groupId'
    ";
    return $this->db->query($query)->result();
  }

  public function getDetail($params = [])
  {
    return $this->db->where($params)->get($this->_table)->row();
  }

  public function insert()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $kelasId = $this->input->post('kelas_id');
      $subKelasId = $this->input->post('sub_kelas_id');
      $temp = $this->getDetail(array('kelas_id' => $kelasId, 'sub_kelas_id' => $subKelasId));

      if (!is_null($temp)) {
        $this->deleteByKelas($kelasId, $subKelasId);
      };

      $this->kelas_id = $kelasId;
      $this->sub_kelas_id = $subKelasId;
      $this->learndash_group_id = $this->input->post('learndash_group_id');
      $this->learndash_group_name = $this->input->post('learndash_group_name');
      $this->created_by = $this->session->userdata('user')['id'];
      $this->db->insert($this->_table, $this);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function insertBatch($data)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->insert_batch($this->_table, $data);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function update($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->kelas_id = $this->input->post('kelas_id');
      $this->sub_kelas_id = $this->input->post('sub_kelas_id');
      $this->learndash_group_id = $this->input->post('learndash_group_id');
      $this->learndash_group_name = $this->input->post('learndash_group_name');
      $this->db->update($this->_table, $this, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function delete($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->delete($this->_table, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  public function deleteByKelas($kelas = null, $subKelas = null)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->delete($this->_table, ['kelas_id' => $kelas, 'sub_kelas_id' => $subKelas]);

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  public function truncate()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->truncate($this->_table);

      $response = array('status' => true, 'data' => 'Data has been truncated.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to truncate your data.');
    };

    return $response;
  }

  function br2nl($text)
  {
    return str_replace("\r\n", '<br/>', htmlspecialchars_decode($text));
  }

  function clean_number($number)
  {
    return preg_replace('/[^0-9]/', '', $number);
  }
}
