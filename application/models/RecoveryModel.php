<?php
defined('BASEPATH') or exit('No direct script access allowed');

class RecoveryModel extends CI_Model
{
  private $_table = 'user';

  public function rules()
  {
    return array(
      [
        'field' => 'username',
        'label' => 'NISN / Email',
        'rules' => 'required|trim'
      ]
    );
  }

  public function rulesReset()
  {
    return array(
      [
        'field' => 'token',
        'label' => 'Token',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'password',
        'label' => 'Kata Sandi Baru',
        'rules' => 'required|trim'
      ]
    );
  }

  public function getDetail($username = null)
  {
    $this->db->where('username', $username);
    $this->db->or_where('email', $username);
    $query = $this->db->get($this->_table);

    return $query->row();
  }

  public function checkToken($token)
  {
    $this->db->where('token', $token);
    $query = $this->db->get($this->_table);
    $data = $query->row();

    if (!is_null($data)) {
      return $data;
    } else {
      return false;
    };
  }

  public function setToken($id, $token)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->where(['id' => $id]);
      $this->db->set('token', $token);
      $this->db->update($this->_table);

      $response = array('status' => true, 'data' => 'Token has been set.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to set token, try again later.');
    };

    return $response;
  }

  public function setPassword($token, $password)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->where(['token' => $token]);
      $this->db->set('password', md5($password));
      $this->db->set('token', '');
      $this->db->update($this->_table);

      $response = array('status' => true, 'data' => 'Password has been set.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to update your password, try again later.');
    };

    return $response;
  }
}
